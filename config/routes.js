//Routing of all the menu(s) is defined here using $stateProvider service.
//.state('state_name',{
//URL:prefered URL name
//views with aliasname EG: topnav@dashboard indicates load template at ui-view whose name is topnav & parent state is dashboard.
//templateurl: path to htmlpage
//})
try {
    app.config(['$urlMatcherFactoryProvider', '$stateProvider', '$urlRouterProvider', '$logProvider',
        function($urlMatcherFactoryProvider, $stateProvider, $urlRouterProvider) {

            $urlMatcherFactoryProvider.caseInsensitive(true);
            $urlMatcherFactoryProvider.strictMode(false);
            $stateProvider
                .state('Dashboard', {
                    url: '/',
                    views: {
                        '@': {
                            templateUrl: './views/home.view.html',
                        }
                    }
                })
                .state('Dashboard.Report', {
                    url: 'Reports',
                    abstract: true
                })
                .state('Dashboard.Report.overview', {
                    url: '/Overview',
                    views: {
                        'topnav@Dashboard': {
                            templateUrl: './views/defaultDropdown.html',

                        },
                        'content@Dashboard': {
                            templateUrl: "./views/overview.html",


                        },

                    }
                })
                .state('Dashboard.Report.AverageDaysToBill', {
                    url: "/AverageDaysToBill",
                    views: {
                        'topnav@Dashboard': {
                            templateUrl: './views/defaultDropdown.html',

                        },
                        'content@Dashboard': {
                            templateUrl: "./views/AverageDaysToBill.html",


                        }
                    }
                })
                .state('Dashboard.Report.WaitingToBeBilled', {
                    url: "/WaitingToBeBilled",
                    views: {
                        'topnav@Dashboard': {
                            templateUrl: './views/defaultDropdown.html',

                        },
                        'content@Dashboard': {
                            templateUrl: "./views/WaitingToBeBilled.html",

                        }
                    }
                })
                .state('Dashboard.Report.DateOfServiceVsDateOfUpload', {
                    url: '/DateOfServiceVsDateOfUpload',
                    views: {
                        'topnav@Dashboard': {
                            templateUrl: './views/averageDropdown.html',

                        },
                        'content@Dashboard': {
                            templateUrl: "./views/DateOfServiceVsDateOfUpload.html",

                        }
                    }
                })
                .state('Dashboard.Report.ARTotalOutstandingClaims', {
                    url: '/ARTotalOutstandingClaims',

                    views: {
                        'topnav@Dashboard': {
                            templateUrl: './views/ARTotalDropdown.html',

                        },
                        'content@Dashboard': {
                            templateUrl: "./views/ARTotalOutstandingClaims.html",


                        }
                    }
                })

            .state('Dashboard.Report.ARPercentOutstandingClaims', {
                url: '/ARPercentOutstandingClaims',

                views: {
                    'topnav@Dashboard': {
                        templateUrl: './views/payerDropdown.html',

                    },
                    'content@Dashboard': {
                        templateUrl: "./views/ARPercentOutstandingClaims.html",


                    }
                }
            })

            .state('Dashboard.Report.AverageDaysOutstandingTending', {
                url: '/AverageDaysOutstandingTending',

                views: {
                    'topnav@Dashboard': {
                        templateUrl: './views/defaultDropdown.html',

                    },
                    'content@Dashboard': {
                        templateUrl: "./views/AverageDaysOutstandingTending.html",


                    }
                }
            })

            .state('Dashboard.Report.ClaimsBilledPerMonth', {
                url: '/ClaimsBilledMonthPerClient',

                views: {
                    'topnav@Dashboard': {
                        templateUrl: './views/defaultDropdown.html',

                    },
                    'content@Dashboard': {
                        templateUrl: "./views/ClaimsBilledMonthPerClient.html",


                    }
                }
            })

            .state('Dashboard.Report.AverageCashCollectedPerTrip', {
                url: '/AverageCashCollectedPerTrip',

                views: {
                    'topnav@Dashboard': {
                        templateUrl: './views/defaultDropdown.html',

                    },
                    'content@Dashboard': {
                        templateUrl: "./views/AverageCashCollectedPerTrip.html",


                    }
                }
            })

            .state('Dashboard.Report.MedicareLastPayment', {
                url: '/MedicareLastPayment',

                views: {
                    'topnav@Dashboard': {
                        templateUrl: './views/defaultDropdown.html',

                    },
                    'content@Dashboard': {
                        templateUrl: "./views/MedicareLastPayment.html",

                    }
                }
            })

            .state('Dashboard.Report.VolumeMonthlyTrending', {
                url: '/VolumeMonthlyTrending',

                views: {
                    'topnav@Dashboard': {
                        templateUrl: './views/defaultDropdown.html',

                    },
                    'content@Dashboard': {
                        templateUrl: "./views/VolumeMonthlyTrending.html",
                    }
                }
            })

            .state('login', {
                url: '/login',

                views: {
                    '@': {
                        templateUrl: "./views/login.html",
                        controller: "LoginControl"
                    }
                }
            })

            //Note : For setting default view through $urlRouterProvider, first give url of parent then slash'/'than state name of template which you want as default.
           // $urlRouterProvider.otherwise('/Reports/Overview');
            $urlRouterProvider.otherwise('/');
        }
    ])
} catch (exception) {
    console.log("Exception: " + exception);
}
//abstract: true indicates but you want to set this since you'd never go to this state directly, you'd 
//always go to one of it's child states.
