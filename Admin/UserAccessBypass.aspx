﻿<%@ Page Title="" Language="C#" MasterPageFile="TriTechMaster.master" AutoEventWireup="true"
    CodeFile="UserAccessBypass.aspx.cs" Inherits="Admin_UserAccessBypass" %>

<asp:Content ID="Content1" ContentPlaceHolderID="pagehead" runat="Server">
    <link href="css/jefbar/facilities.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-2.1.4.min.js"></script>
    <link href="css/bootstrap.min.css" rel="stylesheet" />
     <script src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.nicescroll.340/jquery.nicescroll.js"></script>
    <link href="css/jquery.dataTables.min.css" rel="stylesheet" />
    <script src="js/jquery.dataTables.min.js"></script>
    <script src="js/UserAccessBypass.js"></script>
    <script type="text/javascript">
       
    </script>
    <style>
        #mpusername {
            display: inline-table;
        }

        lstUnselect > option, lstUnselect > option {
            height: 20px;
        }

        #lium, #lihrum {
            display: none;
        }

        .mask {
            background: #666;
            opacity: 0.3;
            position: fixed;
            top: 0;
        }

        .modal {
            border: solid 6px rgba(0,0,0,0.5);
            width: auto;
            height: auto;
            display: none;
            position: fixed;
            left: 40%;
            top: 35%;
            z-index: 10001;
            margin: -25px 0 0 -50px;
            border-radius: 8px 8px 8px 8px;
            background-color: #ffffff;
            -moz-background-clip: padding;
            -webkit-background-clip: padding;
            background-clip: padding-box;
        }

        .overlay-style {
            background: none repeat scroll 0 0 #444444; /*background: rgba(40, 40, 40, .7);*/
            height: 100%;
            left: 0;
            opacity: 0.7;
            position: fixed;
            top: 0;
            width: 100%;
            height: 100%;
            z-index: 10000;
            display: none;
        }

        .MainTable_Ex a:hover, .MainTable_Ex #tbody tr:hover {
            color: #fff;
            background: rgb(18,68,4);
            text-decoration: underline;
            cursor: pointer;
        }

        .active td, .active th {
            color: #fff;
            background: rgb(18,68,4);
        }

        #changefacility {
            display: none;
        }

        .savingPopup {
            position: fixed;
            top: 15%;
            max-width: 600px;
            height: auto;
            z-index: 10;
            width: 80%;
        }

        .savingPopupIpad {
            position: relative;
            top: 20px;
            max-width: 94%;
            height: auto;
            z-index: 10;
            width: 90%;
        }

        #tbSelect td {
            border-bottom: 1px solid #ccc;
            height: 20px;
            line-height: 20px;
        }

        #divError br {
            display: block !important;
        }

        .divselectArea {
            height: 200px;
            overflow: auto;
            cursor: pointer;
            width: 100%;
            padding: 0;
            margin: 0;
        }

        @media (max-width:640px) {

            .savingPopupIpad input[type="text"], .savingPopupIpad input[type="password"] {
                width: 70%;
                max-width: 175px;
            }
        }

        @media (max-width:420px) {
            .savingPopupIpad {
                position: relative;
                top: -2px;
                max-width: 94%;
                height: auto;
                z-index: 10;
                width: 90%;
            }
        }

               .dataTable {
    color: #777 !important;
    font-size: 12px !important;
}
    </style>
     <script type="text/javascript">
       
         $(document).ready(function () {
             $('.innerView').css({'position':'relative','overflow-y':'auto'});
             //  alert(DecryptData());
         });
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CustomTopHeadHolder" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CustomHeadHolder" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentHolder" runat="Server">
    <div align="center">
        <div id="lfrom" style="padding: 15px;">
            <div style="float: left; margin-left: 2%; margin-bottom:10px;">
                <asp:LinkButton Text="Back" CssClass="cancgo" ID="lnkBack" runat="server" style="display:none;" />
            </div>
            <div style="float: right; margin-right: 2%; margin-bottom:10px;">
               <%-- <input type="button" value="Add" class="cancgo" onclick="ShowFacilityPopup('-1', '', '', -1, '0');" />--%>
            </div>

            <div class="panel-heading" style="    border-color: #e9e9e9 !important;text-align:left;">
                                <span id="span_report_heading2">
                                    <strong>
                                        <span id="div_management_header" style="    font-weight: bold;    font-size: 14px;">User Login By-Pass</span>
                                    </strong>
                                   
                               
                                     </span>

                            </div>
            <div>
               <div  style="padding-top: 5px; overflow:auto">
                    <table cellpadding="0" cellspacing="0" border="0" class="display dataTable no-footer" style="width: 100%;font-weight: normal;">
                        <thead>
                            <tr>
                                <th style="text-align:left;">User
                                </th>
                                <%-- <th>
                                    Facilities
                                </th>--%>
                                <th style="text-align:left;">Role Name
                                </th>
                                <th style="text-align:left;">Status
                                </th>
                                <th style="text-align:left;">Created By
                                </th>
                                
                                <th style="text-align:left;"> Login
                                </th>
                            </tr>
                        </thead>
                        <tbody id="tbody">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div id="dialog" class="savingPopup" style="display: none;">
        <div class="reportschoose" style="padding: 10px;">
            <div id="divError" style="display: none; color: Red; margin-bottom: 5px; text-align: left;">
            </div>
            <div id="divSuccess" style="display: none; color: #3f934f; margin-bottom: 5px;">
                Seved Successfully.
            </div>
            <div style="width: 100%">
                <table width="100%">
                    <tr>
                        <td>
                            <table width="100%" id="userDetail" cellpadding="2" cellspacing="2">
                                <tr>
                                    <td>User Name
                                    </td>
                                    <td>
                                        <input type="text" id="txtUserName" title="Please Enter User Name" />
                                        <img id="imgCheck" src="images/Check-icon1.png" onclick="checkUserName();" style="width: 18px; vertical-align: sub; cursor: pointer"
                                            title="Click to Verify User Name" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>Password
                                    </td>
                                    <td>
                                        <input type="password" id="txtPassword" title="Please Enter Password" />
                                        <span title="Generate Password" id="btngenerator" style="cursor: pointer; font-size: 20px;">
                                            <i class="icon-key"></i></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Confirm Password
                                    </td>
                                    <td>
                                        <input type="password" id="txtRePassword" title="Please Enter Password Again" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>Active
                                    </td>
                                    <td>
                                        <input type="checkbox" id="chkActive" value="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>Visible
                                    </td>
                                    <td>
                                        <input type="checkbox" id="chkGlobal" value="" />
                                    </td>
                                </tr>

                                <tr>
                                    <td>Role
                                    </td>
                                    <td>
                                        <select id="ddlRole"></select>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div id="divWeb" style="display: none; width: 100%; padding: 0">
                                <table width="100%" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td colspan="3">
                                            <%--<h2>
                                                Facilities</h2>--%>
                                                &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <th style="padding-bottom: 5px;" align="left">Available Facilities
                                        </th>
                                        <th></th>
                                        <th style="padding-bottom: 5px;" align="left">Assigned Facilities
                                        </th>
                                    </tr>
                                    <tr>
                                        <td style="width: 42%">
                                            <select size="10" multiple="multiple" style="width: 100%; height: auto;" id="lstUnselect">
                                            </select>
                                        </td>
                                        <td style="width: 10%" align="center">
                                            <input type="button" id="btnRight" value=">>" /><br />
                                            <input type="button" id="btnLeft" value="<<" />
                                        </td>
                                        <td style="width: 42%">
                                            <select size="10" multiple="multiple" style="width: 100%; height: auto;" id="lstSelect">
                                            </select>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div id="divMob" style="width: 100%; padding: 0">
                                <table width="100%" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <th style="padding-bottom: 5px;" align="left">Select Facilities
                                        </th>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div style="text-align: center; background: #ccc; height: 20px; line-height: 20px; font-weight: bold; width: 100%; cursor: pointer; padding: 0"
                                                id="divAll">
                                                <table width="100%">
                                                    <tr>
                                                        <td>ALL
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div style="height: 200px; overflow: auto; cursor: pointer; width: 100%; padding: 0;">
                                                <table id="tbSelect" style="width: 100%" cellpadding="0" cellspacing="0">
                                                </table>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input type="button" id="btnSave" class="cancgo" value="Save" onclick="SaveUserFacility();" />
                            <input type="button" id="btnCancel" class="cancgo" value="Cancel" onclick="resetAndClose();"
                                style="width: 55px;" />
                            <%--  <input type="button" id="btnInactive" class="cancgo" value="Inactive" onclick="ActiveInactiveUser();"
                                style="float: right; width: 60px;" />--%>
                            <input type="hidden" name="" value="" id="hdnClintID" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <div id="divpwd" class="modal" style="display: none; width: 400px;">
        <div style="z-index: 610; border-radius: 8px 8px 8px 8px;">
            <div style="background: none repeat scroll 0 0 #296CBA; z-index: 1000; position: relative; padding: 2px; border-bottom: 1px solid rgb(194, 122, 0); height: 35px; border-radius: 3px 3px 0px 0px;">
                <table width="100%" height="100%">
                    <tbody>
                        <tr>
                            <td valign="middle" style="vertical-align: middle; font-weight: bold; color: rgb(255, 255, 255); text-shadow: 0px 1px 1px rgba(0, 0, 0, 0.5); padding-left: 10px; font-size: 13px; cursor: move;">Password Generator
                            </td>
                            <td width="20" align="center" style="vertical-align: middle; font-family: Arial,Helvetica,sans-serif; color: rgb(170, 170, 170); cursor: pointer;">
                                <div title="Close Window" class="close-wizs" onclick="closegenrator();">
                                    <span style="color: #000000;"><i class="icon-remove-circle" style="font-size: 20px; margin-right: 0;"></i></span>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="window-content-wrapper" style="padding: 15px;">
                <table width="100%" cellpadding="0" cellspacing="10">
                    <tr>
                        <td>
                            <input type="text" id="txtpwd" style="width: 100px" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input type="button" id="btngenrate" value="Generate Password" class="canccompare"
                                style="margin-left: 0;" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input type="checkbox" id="chk" />
                            I have copied this password in a safe place.
                        </td>
                    </tr>
                </table>
            </div>
            <div class="window-buttons-wrapper" style="padding: 0px; display: inline-block; width: 100%; border-top: 1px solid rgb(255, 255, 255); background: none repeat scroll 0px 0px rgb(222, 222, 222); z-index: 999; position: relative; text-align: right; border-radius: 0px 0px 3px 3px;">
                <div style="padding: 12px; height: 33px;">
                    <input type="button" id="btnusepass" value="Use Password" class="canccompare" />
                    <input type="button" id="btncancelpwd" value="Cancel" class="canccompare" />
                </div>
            </div>
        </div>
    </div>
    <div id="divReset" class="reportschoose" style="width: 175px; padding: 5px; display: none; position: absolute; z-index: 10;">
        <h3 style="margin: 2px;">Reset Password</h3>
        <div id="divResetError" style="color: Red; display: none;">
        </div>
        <div style="width: 100%">
            <table style="width: 100%; display: block;">
                <tr>
                    <td style="display: block;">New Password
                    </td>
                    <td style="display: block;">
                        <input type="password" id="txtNpassword" />
                    </td>
                    <td style="display: block; float: right;">
                        <img src="images/save_icon.png" alt="Save" onclick="ResetPassword();" style="width: 20px; vertical-align: sub; cursor: pointer;" />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <img src="images/themes/icons/cancel.png" alt="Cancle" onclick="HideResetPopUp();"
                            style="width: 16px; vertical-align: sub; cursor: pointer;" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div align="center" style="display: none;" class="divloading" id="divLoading">
        <table cellspacing="0" cellpadding="0" style="height: 100%; "
            class="">
            <tbody>
                <tr>
                    <td style="" class="dx">
                        <%--<img align="middle" alt="Loading..." src="/CCAMedicalC/DXR.axd?r=0_622-kqAE6">--%>
                        <img align="middle" alt="Loading..." src="images/Loader_Circular.gif">
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div id="divoverlay" class="overlay-style">
    </div>
    <div id="divMask" class="mask" style="display: none">
    </div>
</asp:Content>

