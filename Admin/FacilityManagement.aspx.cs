﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

public partial class Admin_FacilityManagement : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.Cookies["SM_USER"] == null)
            redirectOnLogin();

        if (Request.Cookies["reportval"] != null)
        {
            HttpCookie myCookie = Request.Cookies["reportval"];
            myCookie.Expires = DateTime.Now.AddDays(-1d);
            Response.Cookies.Add(myCookie);
        }


        if (!IsPostBack)
        {
            string rediredUrl = "../facilities/facilities.aspx";
            if (Request.UrlReferrer != null )
                if (!Request.UrlReferrer.ToString().ToLower().Contains("changepassword.aspx") && !Request.UrlReferrer.ToString().ToLower().Contains("usermanagement.aspx") && !Request.UrlReferrer.ToString().ToLower().Contains("rolemanagement.aspx"))
                rediredUrl = Request.UrlReferrer.ToString();
            lnkBack.PostBackUrl = rediredUrl;
           

        }
    }
    private void clearcookies()
    {
        HttpCookie myCookie = new HttpCookie("SM_FACILITY");
        HttpCookie myuserCookie = new HttpCookie("SM_USER");
        HttpCookie UrlCookie = new HttpCookie("SM_URL");
        HttpCookie valcookie = new HttpCookie("reportval");
        HttpCookie nooffacility = new HttpCookie("nooffacility");
        HttpCookie CAutoID = new HttpCookie("CAutoID");
        myCookie.Expires = DateTime.Now.AddDays(-1d);
        myuserCookie.Expires = DateTime.Now.AddDays(-1d);
        UrlCookie.Expires = DateTime.Now.AddDays(-1d);
        valcookie.Expires = DateTime.Now.AddDays(-1d);
        nooffacility.Expires = DateTime.Now.AddDays(-1d);
        CAutoID.Expires = DateTime.Now.AddDays(-1d);
        //myuserCookie.Domain = ".eonbi.com";
        // UrlCookie.Domain = ".eonbi.com";
        Response.Cookies.Add(myCookie);
        Response.Cookies.Add(myuserCookie);
        Response.Cookies.Add(UrlCookie);
        Response.Cookies.Add(valcookie);
        Response.Cookies.Add(nooffacility);
        Response.Cookies.Add(CAutoID);
    }

    private void redirectOnLogin()
    {
        clearcookies();
        string RedirecUrl = ConfigurationManager.AppSettings["LoginUrl"].ToString();
        string userAgent = Request.UserAgent;
        Uri test = new Uri(Request.Url.ToString());
        if (test.Host != "localhost" && !userAgent.Contains("iPad") && !userAgent.Contains("iPhone"))
            Response.Redirect(RedirecUrl);
        else
            Response.Redirect("../Login/login.aspx");
    }
}