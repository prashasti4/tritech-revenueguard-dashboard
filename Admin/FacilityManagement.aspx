﻿<%@ Page Title="" Language="C#" MasterPageFile="TriTechMaster.master" AutoEventWireup="true"
    CodeFile="FacilityManagement.aspx.cs" Inherits="Admin_FacilityManagement" %>

<asp:Content ID="Content1" ContentPlaceHolderID="pagehead" runat="Server">
    <link rel="stylesheet" type="text/css" href="../tree/themes/default/easyui.css" />
    <link rel="stylesheet" type="text/css" href="../js/Jcrop-v0.9.12/css/jquery.Jcrop.css" />
    <link rel="stylesheet" type="text/css" href="../js/Jcrop-v0.9.12/css/jquery.Jcrop.min.css" />
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.8.2.js"></script>
    <script type="text/javascript" src="../js/dxchart/1.9.2/jquery-ui.min.js"></script>
    <script type="text/javascript" src="../tree/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="../js/jquery.nicescroll.340/jquery.nicescroll.js"></script>
    <script type="text/javascript" src="../js/Jcrop-v0.9.12/js/jquery.Jcrop.js"></script>
    <%--<script type="text/javascript" src="../js/jquery.resizecrop.js"></script>--%>
    <script src="../js/FacilitiesManagement.js" type="text/javascript"></script>
    <style type="text/css">
        /* Apply these styles only when #preview-pane has
   been placed within the Jcrop widget */
        .preview-pane
        {
            display: block; /* position: absolute;*/
            z-index: 2000;
            top: 20px; /* right: -280px;*/
            padding: 6px;
            border: 3px solid #666;
            background-color: #ccc;
            -webkit-border-radius: 6px;
            -moz-border-radius: 6px;
            border-radius: 6px;
            -webkit-box-shadow: 1px 1px 5px 2px rgba(0, 0, 0, 0.2);
            -moz-box-shadow: 1px 1px 5px 2px rgba(0, 0, 0, 0.2);
            box-shadow: 1px 1px 5px 2px rgba(0, 0, 0, 0.2);
            margin: 20px;
        }
        /* The Javascript code will set the aspect ratio of the crop
   area based on the size of the thumbnail preview,
   specified here */.preview-pane .preview-container
        {
            width: 190px;
            height: 125px;
            overflow: hidden;
        }
        #tbody a:hover
        {
            color: #940006;
            text-decoration: underline;
            cursor: pointer;
        }
        #imgCropper
        {
            position: fixed;
            left: 10%;
            top: 10%;
            width: 80%;
            height: 75%;
            background: #fff;
            display: none;
            border: 1px solid #808080;
            border-radius: 3px;
            z-index:100;
        }
        .viewport
        {
            float: left;
            width: 75%;
            height: 85%;
            overflow: hidden;
            border: 3px solid #666;
            border-radius: 5px;
            opacity: 0.8;
            margin: 20px 10px 5px 10px;
        }
        .zoomer
        {
            /* position:absolute;*/
            clear: both;
            margin: 10px 20px 10px 10px;
        }
        .zoomer i
        {
            font-size: 20px;
        }
        .zoomer i:hover
        {
            color: #940006;
            cursor: pointer;
        }
        .close
        {
            float: right;
            position: absolute;
            right: 5px;
            top: 5px;
            background: url('../tree/themes/icons/cancel.png');
            cursor: pointer;
        }
        .slider-pane
        {
            width: 70%;
            margin: 10px;
            float: none;
            border: 3px solid #ccc;
            border-radius: 6px;
            background: #fff;
        }
        #changefacility
        {
            display: none;
        }
        .noimg
        {
            width: 100%;
            margin: 43px auto;
            font-size: 20px;
            color: #665;
            text-align: center;
        }
        
        .popupbottom
        {
            width: 100%;
            margin: 0 auto;
            border-top: 1px solid #000;
            background-color: #ccc;
            height: 29px;
        }
        .mask
        {
            width:100%;
            height:100%;
            position:absolute;
            top:0;
            left:0;
            bottom:0;
            right:0;
            background:#fff;
            opacity:0;
            display:none;
            
            
        }
        @media(max-width:1024px)
        {
            #imgCropper
           {
            position: fixed;
            left: 5%;
            top: 5%;
            width: 90%;
            height: 80%;
            background: #fff;
            display: none;
            border: 1px solid #808080;
            border-radius: 3px;
           }
           .viewport
           {
               width:73%;
               height:84%;
           }
           .zoomer i
           {
               padding-top:10px;
           }
        }
        @media(max-width:768px)
        {
            #imgCropper
            {
                left: 1%;
                top: 21%;
                width: 98%;
                height: 70%;
           }
           .viewport {
           width: 67%;
           height: 85%;
           }
            .reportschoose
            {
                margin-left:1%
            }
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CustomTopHeadHolder" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CustomHeadHolder" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentHolder" runat="Server">
    <div align="center">
        <div id="lfrom" style="margin: 5px 0;">
            <div style="float: left; margin-left: 2%">
                <asp:LinkButton Text="Back" CssClass="cancgo" ID="lnkBack" runat="server" />
            </div>
            <div>
                <div class="reportschoose" style="padding: 5px; 0">
                    <table cellpadding="0" cellspacing="0" border="0" class="MainTable_Ex" style="width: 98%">
                        <thead>
                            <tr>
                                <th>
                                    Facilities
                                </th>
                                <th style="width: 20%;">
                                    Image
                                </th>
                                <th style="width: 20%">
                                    Edit
                                </th>
                                <th style="width: 20%">
                                    Remove
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td colspan="4" style="padding: 0;">
                                    <div id="tbody" style="width: 100%; margin: 0; padding: 0;">
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div id="imgCropper" style="">
        <%-- <div class="close">
           </div>--%>
        <div id="divviewPort" class="viewport">
        </div>
        <div id="divimgPreview" class="preview-pane" style="width: 190px; height: 125px;
            overflow: hidden;">
            <div class="preview-container">
                <img id="imgPreview" />
            </div>
        </div>
        <div class="zoomer">
            <a id="plus" title="Zoom In"><i class="icon-zoom-in"></i></a>&nbsp;&nbsp;&nbsp;
            <a id="minus" title="Zoom In"><i class="icon-zoom-out"></i></a>
            <input type="button" class="cancgo" value="Close" id="close" title="Close" onclick="CloseCroper();"
                style="float: right;" />&nbsp;&nbsp;&nbsp;
            <input type="button" class="cancgo" value="Crop & Save" onclick="cropImage();" style="float: right;
                width: 100px;" />
        </div>
        <%-- <div class="slider-pane"></div>--%>
    </div>
    <div align="center" style="display: none;" class="divloading" id="divLoading">
        <table cellspacing="0" cellpadding="0" style="height: 100%; border-collapse: collapse;"
            class="dxchartsuiLoadingPanel_DevEx">
            <tbody>
                <tr>
                    <td style="" class="dx">
                        <img align="middle" alt="Loading..." src="../images/jefbar_Loadingdemo3.gif">
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div style="display: none">
        <input type="hidden" name="name" value="" id="hdnPrecticID" />
        <canvas id="canvasCroppingImage"></canvas>
        <canvas id="canvasTracker"></canvas>
        <input type="file" id="fuFile" />
        <div class="coords">
            <label>
                X1
                <input type="text" id="x1" name="x1" /></label>
            <label>
                Y1
                <input type="text" id="y1" name="y1" /></label>
            <label>
                X2
                <input type="text" id="x2" name="x2" /></label>
            <label>
                Y2
                <input type="text" id="y2" name="y2" /></label>
            <label>
                W
                <input type="text" id="w" name="w" /></label>
            <label>
                H
                <input type="text" id="h" name="h" /></label>
        </div>
    </div>
    <div id="divAlert" class="divmsg" style="display: none;">
        <div style="width: 87%; padding: 15px; min-height: 25px;">
            <span id="AlertSpan"></span>
        </div>
        <div class="popupbottom" style="height: 33px;">
            <a href="javascript:;" id="btnAlertok" class="cancgo" style="margin-left: 40%;">OK</a>
        </div>
    </div>
    <div id="divConfirm" class="divmsg" style="display: none;">
        <div style="width: 87%; padding: 15px; min-height: 25px;">
            <span id="ConfirmSpan"></span>
        </div>
        <div class="popupbottom" style="height: 33px;">
            <a href="javascript:;" id="btnConfirmOK" class="cancgo" style="margin-left: 20%;">OK</a>
            <a href="javascript:;" id="btnConfirmCancel" class="cancgo" style="margin-left: 15%;">
                Cancel</a>
        </div>
    </div>
    <div class="mask"></div>
</asp:Content>
