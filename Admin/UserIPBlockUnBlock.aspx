﻿<%@ Page Title="" Language="C#" MasterPageFile="TriTechMaster.master" AutoEventWireup="true" CodeFile="UserIPBlockUnBlock.aspx.cs" Inherits="admin_UserIPBlockUnBlock" %>

<asp:Content ID="Content1" ContentPlaceHolderID="pagehead" Runat="Server">
    <%--<link rel="stylesheet" type="text/css" href="css/jquery-ui-1.11.3.css" />--%>
    <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <%--<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />--%>
    <link href="css/jquery-ui-1.7.1.custom.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="css/jquery.timepicker.css" />
 <%--   <script type="text/javascript" src="js/jquery-1.8.2.js"></script>
    <script src="js/bootstrap.min.js" type="text/javascript"></script>--%>
    <script src="js/jquery-2.1.4.min.js"></script>
    <link href="css/bootstrap.min.css" rel="stylesheet" />
     <script src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui-1.11.3.js"></script>
    <link href="css/jquery.alerts.css" rel="stylesheet" />
    <script src="js/jquery.alerts.js"></script>
    <script type="text/javascript" src="js/jquery.timepicker.js"></script>
    <script src="js/dxchart/extjs/ext-base.js" type="text/javascript"></script>
    <script src="js/dxchart/extjs/ext-all.js" type="text/javascript"></script>
    <link href="css/jquery.dataTables.min.css" rel="stylesheet" />
    <script src="js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="js/dxchart/extjs/JefbarReportgrid.js"></script>
    <script type="text/javascript" src="js/UserBlock.js"></script>
    <style>
        .no-print, #changefacility {
            display: none;
        }

        .ep-rec-dow {
            padding: 0px;
            margin-left: 8px;
        }

        .time {
            width: 80px;
        }

        #ui-datepicker-div {
            display: none;
        }

        .x-grid3-hd-row {
            background-color: #f6f6f6;
        }

        .modal-dialog {
            width: 450px;
        }

        .trleft {
            width: 30%;
        }

        .trright {
            width: 70%;
        }
        body {
         font-size:12px !important;
        }
        .divmask {
            z-index: 1001;
            background: rgba(255, 255, 255, 0.4);
            position: absolute;
            top: 0;
            width: 100%;
            height: 100%;
        }
        input, button, select, textarea
{
    color: #555;
    
    }

   .x-grid-empty{
       text-align:center;
   }
         .dataTable {
    color: #777 !important;
    font-size: 12px !important;
}
         .x-grid3-hd-inner{
             font-weight:bold;
         }
    </style>
    <%--<script type="text/javascript">
        function getReportName() {
            var sp = '';

            sp = 'getAllReprotName';

            $.get('../reportdata.aspx?sp=' + sp, function (data) {
                if (data.length > 0) {


                    $("#ddlReprot").find('option').remove().end();

                    var data = JSON.parse(data);


                    $.each(data.Table, function (key, value) {
                        $("#ddlReprot").append($("<option></option>").val
            (value.type).html(value.DisplayName));
                    });

                }

            });
        }
       
        $(document).ready(function () {
            getReportName();
        });


    </script>--%>
     <script type="text/javascript">
       
         $(document).ready(function () {
             $('.innerView').css({'position':'relative','overflow-y':'auto'});
             //  alert(DecryptData());
         });
         </script>
    <script>
        function shownodata() {

            $('#divCharts').css('width', '100%');
            $('#divnodata').show();
            $('#divReportDashboard').hide();
            $('#divinnerchartContainerCover, #divinnerchartContainerCover1, #divinnerchartContainerCover2, #divfinancialsummaryCover').hide();
            $('#divcomingsoon').hide();
            $('#divGridPrint').hide();
            $('#divChartPrint').hide();
            HideLoading();
            if (!iPadAgent && !iPhoneAgent && !AndroidAgent) {
                //$('.nano').perfectScrollbar().perfectScrollbar('destroy');
                //$('.nano').perfectScrollbar();
            }
            $('#divNoNetwork').hide();
        }
    </script>
    
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentHolder" Runat="Server">
    <div class="wrapper row-offcanvas row-offcanvas-left" style="margin-left:10px;margin-right:15px;">
        <!-- Right side column. Contains the navbar and content of the page -->
        <aside class="right-side strech" style="margin-left:6px;">
            <!-- Content Header (Page header) -->
            <section class="content-header">

                <%--<h1 style="font-size: 24px !important;">User/IP Block
                   
                </h1>--%>

                <div class="panel-heading" style="    border-color: #e9e9e9 !important;text-align:left;">
                                <span id="span_report_heading2">
                                    <strong>
                                        <span id="div_management_header" style="    font-weight: bold;    font-size: 14px;">User/IP Block</span>
                                    </strong>
                                   
                                <asp:LinkButton ID="lnkCancel" runat="server" CssClass="canccompare"  style="float:right;margin-right:6px;display:none;" Text="Back" ></asp:LinkButton>
                   
                                     </span>

                            </div>
               
            </section>
            <section class="content">
               
                    <div class="row">
                        <div class="col-xs-12">
                            <!-- interactive chart -->
                            <div class="box box-primary">
                                <div class="box-header">
                                    </div>
                                <div class="box-body">
                                    <h5 id="h_schedule" style="font-weight:bold;margin-left: 5px;">&nbsp;</h5>
                                    <table cellpadding="5" cellspacing="5" width="43%">

                                        <tr>
                                            <td class="trleft" >Filter
                                            </td>
                                            <td class="trright">
                                                <select id="ddlBlock" class="form-control">
                                                    <%--<option value="xls">All</option>
                                                    <option value="pdf">User</option>
                                                    <option value="pdf">User</option>--%>
                                                </select>
                                            </td>
                                        </tr>
                                       
                                    </table>
                                    <hr />
                                     <section class="content-header"><div style="width:100%;">
                                         <table style="width:99.5%;">
                                             <tr>
                                                 <td style="width:68%">
<h5 style="font-size:14px; margin-left: 5px;"><b>User/IP Block Details</b>
                 
                </h5>
                                                 </td>
                                                 <td style="width:9%;">
<input type="radio" id="rbActive" name="active" value="never" checked="checked"> Active 
                    <input type="radio" id="rbInActive" name="active" value="never" > Inactive 

                                                 </td>
                                             </tr>
                                         </table>
               
                                        
            </section>
                                    <div id="divfinancialsummaryCover12" class="AlignCenter TopLargeMargin"
                                        style="height: auto;margin-bottom: 10px; border-top:1px solid #f3f3f3; border-left:1px solid #f3f3f3;margin-left: 5px;font-weight:normal;">
                                       
                                        <div id="divfinancialsummary12" class="table" style="margin: 0 auto;">
                                        </div>
                                        <table style="width:100%;">

                                        <tr>
                                            
                                            <td >
                                                
                                                <input id="btn_clear" type="button" name="" value="Clear All" class="canccompare filterButton" style="margin-right:12px;float:right;    
    border: 1px solid rgb(18,68,4);
        background-color: rgb(18,68,4);

    height: 26px;
    width: 85px;

    border-radius: 4px !important;"  onclick="return clearcontrol();"  />
                                                <input id="btn_save" type="button" name="" value="Save" class="canccompare filterButton" style="margin-right:5px;   float: right;
    border: 1px solid rgb(18,68,4);
        background-color: rgb(18,68,4);

    height: 26px;
    width: 40px;

    border-radius: 4px !important;"  onclick="return update_block_details();" />
                                            </td>
                                        </tr>
                                            </table>
                                    </div>

                                    </div>
                                
                            </div>
                        </div>
                    </div>
              </section>
            
        </aside>
        <!-- /.right-side -->
    </div>

   <div align="center" class="divloading" id="divLoading">
                    <table cellspacing="0" cellpadding="0" style="height: 100%; border-collapse: collapse;"
                        class="dxchartsuiLoadingPanel_DevEx">
                        <tbody>
                            <tr>
                                <td style="" class="dx">
                                    <%--<img align="middle" alt="Loading..." src="/CCAMedicalC/DXR.axd?r=0_622-kqAE6">--%>
                                    <img id="imgLoadingjefbar" align="middle" alt="Loading..." src="images/Loader_Circular.gif"
                                         />
                                   <%-- <img id="imgLoadingCCA" align="middle" alt="Loading..." src="images/Loading.gif"
                                        style="display: none;" />--%>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>

    <div class="divmask" style="display: none;">
                </div>
</asp:Content>

