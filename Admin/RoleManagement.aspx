﻿<%@ Page Title="" Language="C#" MasterPageFile="TriTechMaster.master" AutoEventWireup="true"
    CodeFile="RoleManagement.aspx.cs" Inherits="Admin_RoleManagement" %>

<asp:Content ID="Content1" ContentPlaceHolderID="pagehead" runat="Server">
     <%--<script type="text/javascript" src="js/jquery.js"></script>--%>
         <script src="js/jquery-2.1.4.min.js"></script>
    <link href="css/bootstrap.min.css" rel="stylesheet" />
     <script src="js/bootstrap.min.js"></script>
    <link href="css/bootstrap-dialog.min.css" rel="stylesheet" />
        <script src="js/bootstrap-dialog.min.js"></script>
   <link href="css/jquery.dataTables.min.css" rel="stylesheet" />
    <script src="js/jquery.dataTables.min.js"></script>
<%--  <script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.2/rollups/aes.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.2/rollups/sha256.js" type="text/javascript"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.2/rollups/pbkdf2.js" type="text/javascript"></script>--%>
     <script type="text/javascript" src="js/RoleManagement.js"></script>
   
  
    <style>
        #mpusername
        {
            display: inline-table;
        }
        lstUnselect > option, lstUnselect > option
        {
            height: 20px;
        }
        
         #lirm, #lihrrm
        {
            display: none;
        }
        .mask
        {
            background: #666;
            opacity: 0.3;
            position: fixed;
            top: 0;
            left:0;
        }
        .modal
        {
            border: solid 6px rgba(0,0,0,0.5);
            width: auto;
            height: auto;
            display: none;
            position: absolute;
            left: 40%;
            top: 35%;
            z-index: 10001;
            margin: -25px 0 0 -50px;
            border-radius: 8px 8px 8px 8px;
            background-color: #ffffff;
            -moz-background-clip: padding;
            -webkit-background-clip: padding;
            background-clip: padding-box;
        }
        
        .overlay-style
        {
            background: none repeat scroll 0 0 #444444; /*background: rgba(40, 40, 40, .7);*/
            height: 100%;
            left: 0;
            opacity: 0.7;
            position: fixed;
            top: 0;
            width: 100%;
            height: 100%;
            z-index: 10000;
            display: none;
        }
        .MainTable_Ex a:hover, .MainTable_Ex #tbody tr:hover
        {
            color: #fff;
            background: rgb(18,68,4);
            text-decoration: underline;
            cursor: pointer;
        }
        
        .active td
        {
            color: #fff;
            background: rgb(18,68,4);
        }
        #changefacility
        {
            display: none;
        }
        
        .savingPopup
        {
            position: fixed;
            top: 10%;
            max-width: 600px;
            height: auto;
            z-index: 10;
            width: 80%;
        }
        
        .savingPopupIpad
        {
            position: relative;
            top: 20px;
            max-width: 94%;
            height: auto;
            z-index: 10;
            width: 90%;
        }
        
        #tbSelect td
        {
            border-bottom: 1px solid #ccc;
            height: 20px;
            line-height: 20px;
        }
        .class_0
        {
           /* color:#47d620;*/
           color:rgb(18,68,4);
            cursor:pointer;
           
        }
        .class_1
        {
           /*color:#ce0808;*/
            color:#666;
            cursor:pointer;
            
        }
        .class_2
        {
          /* color:#666;*/
          color: #ce0808;
          cursor:default;
            
        }
        
        .report
        {
            width:100%;
            height:25px;
            padding:0 !important;
            margin:0 auto;
            text-align:center
            
         }
        
        .tabPanel
        {
            float: left;
            left: -4%;
            margin: 2.5% 0 -1px;
            padding: 0;
            position: relative;
            z-index: 10;
        }
        .tabmenu
        {
            border: 1px solid #808080;
            line-height: 30px;
            float: left;
            display: inline;
            min-width: 55px;
            background: #cacaca;
            font-weight: bold;
            border-radius: 4px;
        }
        .active
        {
            /*color: #fff;*/
            /*background: rgb(18,68,4);*/
            /*border-bottom: 1px solid #fff;*/
        }
        
        @media (max-width:640px)
        {
        
            .savingPopupIpad input[type="text"], .savingPopupIpad input[type="password"]
            {
                width: 70%;
                max-width: 175px;
            }
        }
        .bootstrap-dialog{
            background: transparent;
                border: none;
    overflow: hidden;
    left:0 !important;
        }
        .bootstrap-dialog .bootstrap-dialog-message {
    font-size: 12px;
}

        .dataTable {
    color: #777 !important;
    font-size: 12px !important;
}

        .btn_submit{
                
    border: 1px solid rgb(18,68,4);
    background-color: rgb(18,68,4);
    height: 26px;
    width: 70px;
    
    border-radius: 4px !important;
        }

       
    </style>
    <script type="text/javascript">
       
        $(document).ready(function () {
            $('.innerView').css({'position':'relative','overflow-y':'auto'});
          //  alert(DecryptData());
        });

       

        function DecryptData() {
            var encryptData = "7u/eG2jCixy6RrlWVZ6F/Q==";
          

            try {
                //Creating the Vector Key
                var iv = CryptoJS.enc.Hex.parse('e84ad660c4721ae0e84ad660c4721ae0');
                //Encoding the Password in from UTF8 to byte array
                var Pass = CryptoJS.enc.Utf8.parse('MAKV2SPBNI99212');
                //var Pass = "MAKV2SPBNI99212";
                //Encoding the Salt in from UTF8 to byte array
                var Salt = CryptoJS.enc.Utf8.parse("insight123resultxyz");
                //Creating the key in PBKDF2 format to be used during the decryption
                var key128Bits1000Iterations = CryptoJS.PBKDF2(Pass.toString(CryptoJS.enc.Utf8), Salt, { keySize: 128 / 32, iterations: 1000 });
                //Enclosing the test to be decrypted in a CipherParams object as supported by the CryptoJS libarary
                var cipherParams = CryptoJS.lib.CipherParams.create({
                    ciphertext: CryptoJS.enc.Base64.parse(encryptData)
                });

                //Decrypting the string contained in cipherParams using the PBKDF2 key
               // var decrypted = CryptoJS.AES.decrypt(cipherParams, "", { mode: CryptoJS.mode.CBC, iv: iv, padding: CryptoJS.pad.Pkcs7 });
                var decrypted = CryptoJS.AES.decrypt(cipherParams, key128Bits1000Iterations, { mode: CryptoJS.mode.CBC, iv: iv, padding: CryptoJS.pad.Pkcs7 });
                var decryptElement = decrypted.toString(CryptoJS.enc.Utf8);
                return decryptElement;
            }
            //Malformed UTF Data due to incorrect password
            catch (err) {
                return "";
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CustomTopHeadHolder" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CustomHeadHolder" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentHolder" runat="Server">
    <div align="center">
        <div id="lfrom" style="padding: 15px;">
            <div style="float: left; margin-left: 2%">
                <asp:LinkButton Text="Back" CssClass="cancgo" ID="lnkBack" runat="server" style="display:none;" />
            </div>
            <div class="panel-heading" style="    border-color: #e9e9e9 !important;text-align:left;">
                                <span id="span_report_heading2">
                                    <strong>
                                        <span id="div_management_header" style="    font-weight: bold;    font-size: 14px;">Role Management</span>
                                    </strong>
                                    <input type="button" value="Add" class="cancgo filterButton" style="float: right;    float: right;
    border: 1px solid rgb(18,68,4);
    background-color: rgb(18,68,4);
    height: 26px;
    width: 40px;
    border-radius: 30px;
    border-radius: 4px !important;" onclick="ShowPopup('-1', '', '', 0);" />
                                </span>

                            </div>
           <%-- <div style="float: right; margin-right: 2%;margin-bottom: 5px;">
                <input type="button" value="Add" class="cancgo filterButton" style="background-color: rgb(18,68,4);" onclick="ShowPopup('-1','','',0);" />
            </div>--%>
            <div>
                <div  style="padding-top: 5px; overflow:auto">
                    <table cellpadding="0" cellspacing="0" border="0" class="display dataTable no-footer" style="width: 100%;font-weight: normal;">
                        <thead>
                            <tr>
                                <th style="text-align:left;">
                                    Role
                                </th>
                              <th style="text-align:left;">
                                    Status
                                </th>
                                 <th style="text-align:left;">
                                   Visible
                                </th>  
                                <th style="text-align:left;">
                                    Created By
                                </th>
                                <th style="text-align:left;">
                                    Modified By
                                </th>
                            </tr>
                        </thead>
                        <tbody id="tbody">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div id="dialog" class="savingPopup" style="display: none;">
        <div class="reportschoose" style="padding: 10px;">
            <div id="divError" style="display: none; color: Red; margin-bottom: 5px;">
            </div>
            <div id="divSuccess" style="display: none; color: #3f934f; margin-bottom: 5px; font-weight:bold;">
                Seved Successfully.
            </div>
            <div style="width: 100%">
                 <table width="100%">
                        <tr>
                            <td>
                                <table width="100%" id="userDetail" cellpadding="0" cellspacing="0" style="font-weight:normal;">
                                    <tr>
                                        <td>
                                           Role Name
                                        </td>
                                        <td>
                                            <input type="text" id="txtRoleName" title="Please Enter Role Name" onblur="checkRoleName();" />
                                          <%--  <img src="images/Check-icon1.png" onclick="checkRoleName();" style="width: 18px;
                                                vertical-align: sub; cursor: pointer" title="Click to Verify Role Name" />--%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Active
                                        </td>
                                        <td>
                                            <input type="checkbox" id="chkActive" value="" />
                                        </td>
                                    </tr>
                                     <tr>
                                        <td>
                                           <%-- Globally Show--%>Visible
                                        </td>
                                        <td>
                                            <input type="checkbox" id="chkGlobal" value="" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div id="divWeb" style="display: none; width: 100%; padding: 0;max-height: 390px; overflow: auto;">
                                    <table width="100%" cellpadding="0" cellspacing="0" class="MainTable_Ex">
                                        <thead>
                                        <tr>
                                        
                                        <th>Report Name</th>
                                       <%-- <th style="width:80px">Physician</th>--%>
                                        <th style="width:80px">Status</th>
                                        </tr>
                                        </thead>
                                        <tbody id="tbodyMenu"></tbody>
                                    </table>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="button" id="btnSave" style="    float: left;
    border: 1px solid rgb(18,68,4);
    background-color: rgb(18,68,4);
    height: 26px;
    width: 40px;
    border-radius: 30px;
    border-radius: 4px !important;" class="cancgo filterButton" value="Save" onclick="SaveRole();" />
                                <input type="button" id="btnCancel" style="    float: left;
    border: 1px solid rgb(18,68,4);
    background-color: rgb(18,68,4);
    height: 26px;
    width: 70px;
    border-radius: 30px;
    border-radius: 4px !important;" class="cancgo filterButton" value="Cancel" onclick="resetAndClose();"
                                     />
                                <%--  <input type="button" id="btnInactive" class="cancgo" value="Inactive" onclick="ActiveInactiveUser();"
                                style="float: right; width: 60px;" />--%>
                                <input type="hidden" name="" value="" id="hdnRoleID" />
                            </td>
                        </tr>
                    </table>
            </div>
        </div>
    </div>
   
    <div align="center" style="display: none;" class="divloading" id="divLoading">
        <table cellspacing="0" cellpadding="0" style="height: 100%; border-collapse: collapse;"
            class="dxchartsuiLoadingPanel_DevEx">
            <tbody>
                <tr>
                    <td style="" class="dx">
                        <img align="middle" alt="Loading..." src="images/Loader_Circular.gif">
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div id="divoverlay" class="overlay-style">
    </div>
    <div id="divMask" class="mask" style="display: none">
    </div>
</asp:Content>
