﻿/// Facilities Management
/**** this is Used for Managing Facilities****/
/*****Created Date  June 11 2014****/

var jcrop_api, boundx, boundy, displayWidth, displayHeight, AspectRatio, pic_real_width, pic_real_height;
var origionalImage;


$(document).ready(function () {
    
    $('.divloading').show();
    SetFacilityGridHeight();
    getFacilitiesForUser();
    ResizeButtonClickEvent();



    $("#fuFile").change(function (e) {
        var file = e.target.files[0];
        var type = 'image/*';

        if (file.type.indexOf('image') > -1) {
            var reader = new FileReader();
            reader.onload = fileOnload;
            reader.readAsDataURL(file);
        }
        else
            CreateAlertBox('Invalid File Extension.');
    });
    function fileOnload(e) {

        var $img = $('<img>', {
            src: e.target.result
        }).load(function () {
            pic_real_width = this.width;   // Note: $(this).width() will not
            pic_real_height = this.height; // work for in memory images.
        });
        origionalImage = $img;
        AspectRatio = $img[0].width / $img[0].height;

        if ($img.width > $('#divviewPort').width())
            $img.css('width', $('#divviewPort').width());

        if ($img.height > $('#divviewPort').height())
            $img.css('height', $('#divviewPort').height());

        $('#imgCropper').show();
        $('.mask').show();
        $('#divviewPort').append($img);
        //$img.attr({ width: $img.width(), heigth: $img.height() });
        $('#imgPreview').attr({ src: e.target.result });
         $($('#imgPreview')[0]).css({ width: '', height: '','margin-left':'', 'margin-top':''});
       //$('#imgPreview').css('margin-left', '0px').css('margin-top', '0px')
        InitJCrop($img);
        ScrollBarResize('#divviewPort',false);
        //        $('.slider-pane').slider();

    }

});



// JCorp Initialize on Image
function InitJCrop(img) {
    var $img = img;
    $img.Jcrop({
        onChange: updateCoords,
        onSelect: setCoords,
        bgFade: true,
        bgOpacity: 0.5,
        bgColor: '#ccc',
        addClass: 'jcrop-dark'
    }, function () {
        jcrop_api = this;
        jcrop_api.setOptions({
            aspectRatio: 190 / 125,
            maxSize: [190, 125],
            minSize: [190, 125],
            setSelect: [0, 0, 190, 125]
        });
        jcrop_api.focus()
    });

    $(".jcrop-holder").css({
        margin: "0 auto",
        "vertical-align": "middle"
    })
}




function updateCoords(c) {
    $('#x1').val(c.x);
    $('#y1').val(c.y);
    $('#x2').val(c.x2);
    $('#y2').val(c.y2);
    $('#w').val(c.w);
    $('#h').val(c.h);
    $('.noimg').remove();
    $('#imgPreview').show();
    if (c.w > 0 && c.h > 0) {
        var rx = 190 / c.w; // 190*125 - preview box size
        var ry = 125 / c.h;

        var $img = origionalImage
        var widthFactor = pic_real_width / $img.width();  //imgW / $img.width();
        var heightFactor = pic_real_height / $img.height(); //imgH / $img.height();

        //   $('#divimgPreview').css({ width: c.w * widthFactor, height: c.h * heightFactor });

        $('#imgPreview').css({
            marginLeft: '-' + c.x + 'px',
            marginTop: '-' + c.y + 'px'
        });
    }
    else {
        $('#imgPreview').hide();
        var $div = $('<div>').html('Not Available').addClass('noimg');
        $('.preview-container').append($div);
    }

}



function setCoords(c) {
    $('#x1').val(c.x);
    $('#y1').val(c.y);
    $('#x2').val(c.x2);
    $('#y2').val(c.y2);
    $('#w').val(c.w);
    $('#h').val(c.h);
}

function getFacilitiesForUser() {
$.get('../reportdata.aspx?con=1', function (conMsg) {
    $.getJSON('../reportdata.aspx?proc=getFacilitiesForSAutoID', function (data) {
        if (data != undefined && data != null) {
            var data1 = data;
            BindFacilitiesForUser(data1.Table);
        }
    }).done(function () {
        $('.divloading').hide();
    }).fail(function () {
        $('.divloading').hide();
    });
    }).fail(function () {
        $('.divloading').hide();
         $('#divnoConnect').show();
            CreateTimer();
    });
}

var guid = (function() {
  function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
               .toString(16)
               .substring(1);
  }
  return function() {
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
           s4() + '-' + s4() + s4() + s4();
  };
})();

var arr = ['logo_Blue.jpg', 'logo_green.jpg', 'logo_orange.jpg', 'logo_red.jpg', 'logo_RoyalBlue.jpg']

count = 0;
function BindFacilitiesForUser(data) {
    var table = "<table width='100%' cellpadding='0' cellspacing='0' border='0' class='MainTable_Ex'>";
    $('#tbody').empty();
    for (var key in data) {
        if (data[key].name != undefined) {
            table += '<tr><td>' + data[key].name + '</td>';
            if (data[key].imagurl.indexOf('facility2.jpg') > -1) {
                table += '<td align="center"><img src="../images/facilitylogos/jefbar/' + arr[count] + '" width="70px"/></td>';
                table += '<td align="center" style="width:20%"><a onclick="performClick(document.getElementById(\'fuFile\')); setPracticeID(\'' + data[key].ImagePracticeID + '\')">Change Image</a></td>';
                table += '<td align="center" style="width:20%">&nbsp</td></tr>';
                count++;
                if (count == 5)
                    count = 0;
            }
            else {
                table += '<td align="center" style="width:20%"><img src="' + data[key].imagurl +'?'+guid()+ '" width="70px"/></td>';
                table += '<td align="center" style="width:20%"><a onclick="performClick(document.getElementById(\'fuFile\')); setPracticeID(\'' + data[key].ImagePracticeID + '\')">Change Image</a></td>';
                table += '<td align="center" style="width:20%"><a onclick="removeImage(\'' + data[key].ImagePracticeID + '\')">Remove Image</a></td></tr>';

            }
        }
    }
    $('#tbody').append(table);
    ScrollBarResize('#tbody',false);
}


function setPracticeID(pId) {
    $('#hdnPrecticID').val(pId);

}

function performClick(node) {
    var evt = document.createEvent("MouseEvents");
    evt.initEvent("click", true, false);
    node.dispatchEvent(evt);
}



function cropImage() {
    try {
        if ($('#w').val() > 0 && $('#h').val() > 0) {
            $('.divloading').show();
            var dataURL = getTrakerAreaImage();
             var img = document.getElementById("imgPreview");
            dataURL = dataURL.replace('data:image/jpeg;base64,', '');
          $.get('../reportdata.aspx?con=1', function (conMsg) {
            $.post('../Reportdata.aspx', {
                'file': dataURL,
                'pid': $('#hdnPrecticID').val(),
                'tW':img.width,
                'tH':img.height,
                'x':$('#x1').val(),
                'x1':$('#x2').val(),
                'y':$('#y1').val(),
                'y1':$('#y2').val()
            }).done(function () {
                $('.divloading').hide();
                CloseCroper();
                getFacilitiesForUser();
            }).fail(function () {
                $('.divloading').hide();
            }).always(function () {
                $('.divloading').hide();
            });
          }).fail(function () {
                $('.divloading').hide();
                 $('#divnoConnect').show();
                CreateTimer();
                CloseCroper();
          });
        }
        else {
            CreateAlertBox('Please Select Area to Crop.')  
        }
    }
    catch (e) {
        $('.divloading').hide();
        CloseCroper();
    }

}



//Function to get the cropped image starts here
function getTrakerAreaImage() {
    var x, y, x2, y2, w, h;

    x = $('#x1').val();
    y = $('#y1').val();
    x2 = $('#x2').val();
    y2 = $('#y2').val();
    w = $('#w').val();
    h = $('#h').val();


    var SelectImageCropper = document.getElementById('imgPreview')
    var originalCroppedWidth, originalCroppedHeight, trackerWidth, trackerHeight, originalImageWidth, originalImageHeight;
      var ext = "image/jpeg";
    originalImageWidth = SelectImageCropper.naturalWidth;
    originalImageHeight = SelectImageCropper.naturalHeight;

    trackerWidth = w;
    trackerHeight = h;

    var img = document.getElementById("imgPreview"),
        $img = $(img),
        imgW = img.width,
        imgH = img.height;



//        var canvasCroppingImage =document.getElementById("canvasCroppingImage");
//        canvasCroppingImage.width =img.width;     //width after Resize
//        canvasCroppingImage.height =img.height;   //height after Resize
//         var CroppingImage = canvasCroppingImage.getContext("2d");
//         CroppingImage.drawImage(img,0,0,imgW,imgH);
         

    var originalimage = new Image();
    originalimage.src = SelectImageCropper.src;
    originalimage.height = originalImageHeight;
    originalimage.width = originalImageWidth;
    originalimage.onload = function () {
    }
    if (imgW == 0 && imgH == 0) {
        imgW = originalImageWidth;
        imgH = originalImageHeight;
    }

    var widthFactor, heightFactor;
    widthFactor = (originalImageWidth / SelectImageCropper.naturalWidth);
    heightFactor = (originalImageHeight / SelectImageCropper.naturalHeight);

    originalCroppedWidth = (trackerWidth * widthFactor);
    originalCroppedHeight = (trackerHeight * heightFactor);

    if (parseFloat(originalCroppedWidth) > parseFloat(originalImageWidth)) {
        originalCroppedWidth = originalImageWidth;
    }
    if (parseFloat(originalCroppedHeight) > parseFloat(originalImageHeight)) {
        originalCroppedHeight = originalImageHeight;
    }

    var canvas = document.getElementById("canvasTracker");
    canvas.width =originalImageWidth;    
    canvas.height =originalImageHeight;  

    var context = canvas.getContext("2d");

   

    originalCroppedWidth = parseInt(originalCroppedWidth);
    originalCroppedHeight = parseInt(originalCroppedHeight);

    context.drawImage(originalimage, 0, 0, originalImageWidth, originalImageHeight, 0, 0, originalImageWidth, originalImageHeight);

  
    var dataURL = canvas.toDataURL(ext);

    //alert(dataURL);
    return dataURL;

}

//Function to get the cropped image Ends here


function ResizeButtonClickEvent() {

    $('#plus').on('click', function () {

        jcrop_api.destroy();
        var width_viewport, height_viewport, width_preview, height_preview


        width_viewport = $('#divviewPort img').width() + parseInt($('#divviewPort img').width() / 10);
        height_viewport = $('#divviewPort img').height() + parseInt($('#divviewPort img').height() / 10);
        if (width_viewport > 190 && height_viewport > 125) {
            $('#divviewPort img').css({ width: width_viewport, height: height_viewport })
            $($('#imgPreview')[0]).css({ width: width_viewport, height: height_viewport })
        }
        InitJCrop($('#divviewPort img'));
        ScrollBarResize('#divviewPort',true);
    });

    $('#minus').on('click', function () {
        var width_viewport, height_viewport
        jcrop_api.destroy();


        width_viewport = $('#divviewPort img').width() - parseInt($('#divviewPort img').width() / 10);
        height_viewport = $('#divviewPort img').height() - parseInt($('#divviewPort img').height() / 10);
        if (width_viewport > 190 && height_viewport > 125) {
            $('#divviewPort img').css({ width: width_viewport, height: height_viewport })
            $($('#imgPreview')[0]).css({ width: width_viewport, height: height_viewport })
        }

        InitJCrop($('#divviewPort img'));
        ScrollBarResize('#divviewPort',true);

    });

}


function CloseCroper() {
    jcrop_api.destroy();
    $('#divviewPort').empty();
    $('#imgCropper').hide();
    $('.mask').hide();
    $('.coords input').val('');
    $('#fuFile').val('');
}

function SetFacilityGridHeight() {
    var win = $(window).outerHeight();
    var head = $('.header').outerHeight()
    var foot = $('.footer').outerHeight();
    var back = $('#ContentHolder_lnkBack').outerHeight();

    $('#tbody').height(win - (head + foot + back + foot));
}

function removeImage(precticeID) {
   
    var Okfunction = function () {
     $.get('../reportdata.aspx?con=1', function (conMsg) {
        $('.divloading').show();
        $.get('../reportdata.aspx?del=1&pid=' + precticeID, function (data) {
           try
           {
            var data = JSON.parse(data);
            if (data.result[0].msg != "")
                alert(data.result[0].msg);
            else {
                getFacilitiesForUser();
            }
            }
            catch(e)
            {
            $('.divloading').hide();
            }
        }).done(function () {
            $('.divloading').hide();
        }).fail(function () {
            $('.divloading').hide();
        });
     }).fail(function () {
            $('.divloading').hide();
            $('#divnoConnect').show();
            CreateTimer();
        });
    }

    var Cancelfunction = function () {
        //console.log("Run Cancel");
    }

    var msg = "Are you sure, you want to remove it?";

    CreateConfirmBox(msg, Okfunction, Cancelfunction);
}

function CreateAlertBox(msg) {
    $('#divAlert').dialog({
        autoOpen: false,
        draggable: false,
        resizable: false,
        autoResize: true,
        width: 250,
        modal: true,
        height: 'auto',
        minHeight: 100,
        title: '&nbsp;',
        //dialogClass: 'noTitleStuff'
    });

   // $(".ui-dialog-titlebar").hide();
   $('.panel-header,.panel-body').show();
    //$('.panel-body').show();

    $('#AlertSpan').text(msg);
    $("#divAlert").dialog('open');
    $('#btnAlertok').off('click').on('click', function () {
        $("#divAlert").dialog('close');
    });

}

function CreateConfirmBox(msg, OKfunc, Cancelfunc) {
    $('#divConfirm').dialog({
        autoOpen: false,
        draggable: false,
        resizable: false,
        autoResize: true,
        width: 250,
        modal: true,
        height: 'auto',
        minHeight: 100,
        title: '&nbsp;'
       // dialogClass: 'noTitleStuff'
    });

   // $(".ui-dialog-titlebar").hide();
   // $('.panel-body').show();
   
     $('.panel-header,.panel-body').show();

    $('#ConfirmSpan').text(msg);
    $('#divConfirm').dialog('open');
    $('#btnConfirmOK').off('click').on('click', function () {
        $("#divConfirm").dialog('close');
        OKfunc();
    });

    $('#btnConfirmCancel').off('click').on('click', function () {
        $("#divConfirm").dialog('close');
        Cancelfunc();
    });

}

$(window).resize(function(){
    ScrollBarResize('#divviewPort',true);
    ScrollBarResize('#tbody',true);
    $('.mask').width($(window).width()).height($(window).height());
    SetFacilityGridHeight();
})

function ScrollBarResize(pane, isResize)
{
    if (navigator.userAgent.match(/iPhone/) == null && navigator.userAgent.match(/iPad/) == null || navigator.userAgent.match(/Android/i) != null) {
        if(isResize)
            $(pane).getNiceScroll().resize();
         else
            $(pane).niceScroll();
    }
    else
    {
        $(pane).css({'overflow':'auto'});
    }
   if (navigator.userAgent.match(/iPhone/) != null || navigator.userAgent.match(/iPad/) != null ) 
            objc_msgsend('hideStatusBar');
}

$(document).click(function(e){
 if (navigator.userAgent.match(/iPhone/) != null || navigator.userAgent.match(/iPad/) != null ) 
            objc_msgsend('hideStatusBar');

});