﻿$(document).ready(function () {
    $('#sidebar').show();
    $('.mask').width($(window).width()).height($(window).height());
    $('.divloading').show();
    $('#txtPassword').val('');
    $('#txtUserName').val('');

    //if (navigator.userAgent.match(/iPhone/) != null || navigator.userAgent.match(/iPad/) != null || navigator.userAgent.match(/Android/i) != null) {
    //    $('#dialog').addClass('savingPopupIpad');
    //    $('#divMob').show();
    //    $('#divWeb').hide();

    //}
    //else {
        $('#divMob').hide();
        $('#divWeb').show();

   // }
    getRole();

});




function getRole() {
    $.get('../reportdata.aspx?con=1', function (conMsg) {
        $.getJSON('../reportdata.aspx?proc=RoleManagement_getMenuRole&isGlobal=-1&rid=-1&status=-1', function (data) {
            var data1 = data;
            BindRole(data1.Table);
        }).done(function () {
            $('.divloading').hide();
        }).fail(function () {
            $('.divloading').hide();
        });
    }).fail(function () {
        $('.divloading').hide();
        callIos();
    });
}

function BindRole(data) {
    var table = "";
    $('#tbody').empty();
    for (var key in data) {
        if (data[key].roleid != undefined) {
            table = '<tr id="' + key + '" onclick="ShowPopup(\'' + data[key].roleid + '\',\'' + data[key].IsActive + '\',\'' + data[key].rolename + '\',\'' + data[key].isgloballyvisible + '\');"><td>' + data[key].rolename + '</td>';
            table += '<td >' + data[key].IsActive + '</td>';
            table += '<td >' + (data[key].isgloballyvisible==0?'YES':'NO') + '</td>';
            table += '<td >' + data[key].Createdby + '</td>';
            table += '<td >' + data[key].modifiedby + '</td></tr>';
            $('#tbody').append(table);
        }
    }
}

function ShowPopup(RoleId, status, RoleName, globalStatus) {
    var win = $(window).width();
    var diawidth = $('#dialog').width();
  

    $('#dialog').css('left', (win - diawidth) / 2 - 10);
    $('#txtRoleName').val(RoleName);
    $('.divloading').show();
    $('#divError').hide();

    //check is Active
    if (status == "Active" || RoleId == -1)
        $('#chkActive').prop('checked', true);
    else
        $('#chkActive').prop('checked', false);

    //check is Global
    if (globalStatus == 0 || RoleId == -1)
        $('#chkGlobal').prop('checked', true);
    else
        $('#chkGlobal').prop('checked', false);
    var isGlobal = $('#chkGlobal').is(':checked') ? 0 : 1;

    if (RoleId != -1)
        $('#txtRoleName').prop('disabled', true);
    else
        $('#txtRoleName').prop('disabled', false);

    $('#hdnRoleID').val(RoleId);
    $.get('../reportdata.aspx?con=1', function (conMsg) {
        $.getJSON('../reportdata.aspx?proc=RoleManagement_getMenuRoleDetails&status=0&isGlobal=' + isGlobal + '&rid=' + RoleId, function (data) {
            var data1 = data;
            BindMenuTable(data1.Table)
        }).done(function () {
            $('.divloading').hide();
        }).fail(function () {
            $('.divloading').hide();
        });
    }).fail(function () {
        $('.divloading').hide();
        callIos();
    });

    $('#chkActive').unbind("click").bind("click", function () {
        //if (!$('#chkActive').is(':checked'))
        //    if (confirm('Are You sure, you want to Inactive this.'))
        //        $('#chkActive').attr('checked', '');
        //    else
        //        $('#chkActive').attr('checked', 'checked');
        //});
        if (!$('#chkActive').is(':checked'))
        BootstrapDialog.confirm({
            title: 'Confirmation',
            message: 'Are You sure, you want to Inactive this?',
            btnCancelLabel: 'OK', // <-- Default value is 'Cancel',
            btnOKLabel: 'Cancel', // <-- Default value is 'OK',
            btnOKClass: 'btn-default',
            btnCancelClass: 'btn-primary',

            callback: function (result) {
                // result will be true if button was click, while it will be false if users close the dialog directly.
                if (result) {
                    $('#chkActive').prop('checked', true);

                } else {
                    $('#chkActive').prop('checked', false);
                }
            }
        });

        setTimeout('$($(".btn-default")[0]).addClass("btn-primary filterButton btn_submit").removeClass("btn-default")', 400);
        setTimeout('$($(".btn-default")[0]).addClass("filterButton btn_submit")', 400);
      
   

    });

      
    $('#dialog').fadeIn(500);
    if (navigator.userAgent.match(/iPhone/) != null || navigator.userAgent.match(/iPad/) != null || navigator.userAgent.match(/Android/i) != null)
    {
        $('#lfrom').fadeOut(500);
        $('#dialog').css({ 'position': 'static', 'top': '0', 'max-width': '100%', 'width': '94%' });
    }
    else
        $('.mask').show();
}

function resetAndClose() {
    $('#tbody tr').removeClass('active');

    $('#txtRoleName').val('');
    $('#dialog').fadeOut(500);
    if (navigator.userAgent.match(/iPhone/) != null || navigator.userAgent.match(/iPad/) != null || navigator.userAgent.match(/Android/i) != null)
        $('#lfrom').fadeIn(500);
    $('.mask').hide();
    flag = 0;
}

function BindMenuTable(data) {
    $('#tbodyMenu').empty();
    //$('#tbodyMenu').html('');
    var imgcheck = '<i class="icon-ok" style="font-size:20px"></i>';  //'<img src="../images/check-black.png" alt="Checked" width="18px" style="margin:5px"/>';
    var imgUncheck = '<i class="icon-remove" style="font-size:20px"></i>' //'<img src="../images/cancel_black.png" alt="Checked" style="margin:5px"/>';
    var imgnotAccess = '<i class="icon-ban-circle" style="font-size:20px"></i>';
    for (var key in data) {
        if (data[key].ID != undefined) {
            /* 0= checked; 1: Unchecked; 2= disabled  */
            var isPhysician = data[key].isPhysician;
            var isAmbulance = data[key].isAmbulance;
                     table = '<tr id="' + data[key].ID + '"><td>' + data[key].Reportname + '</td>';
            //table += '<td align="center" class="phy"><input type="checkbox" ' + (isPhysician == 0 ? 'checked' : isPhysician == 2 ? 'disabled' : '') + '/></td>';
            //table += '<td align="center" class="amb"><input type="checkbox" ' + (isAmbulance == 0 ? 'checked' : isAmbulance == 2 ? 'disabled' : '') + '/></td>';
                  //   table += '<td><div class="phy report class_' + isPhysician + '">' + (isPhysician == 0 ? imgcheck : isPhysician == 1 ? imgUncheck : imgnotAccess) + '</div></td>'
                     table += '<td><div class="amb report class_' + isAmbulance + '">' + (isAmbulance == 0 ? imgcheck : isAmbulance == 1 ? imgUncheck : imgnotAccess) + '</div></td>'

            $('#tbodyMenu').append(table);
        } 
    }

    $('.report').unbind('click').bind('click', function () {

        if ($(this).hasClass('class_0')) {
            $(this).removeClass('class_0');
            $(this).addClass('class_1');
            $(this).html(imgUncheck);
        }
        else if ($(this).hasClass('class_1')) {
            $(this).removeClass('class_1');
            $(this).addClass('class_0');
            $(this).html(imgcheck);
        }
    });

   
}

$(document).keydown(function (e) {
    var code = e.keyCode ? e.keyCode : e.which;
    
});


function ValidateField() {
    var RoleName = $('#txtRoleName').val();
    var strError = "";
    if (RoleName == "")
        strError += "-Please Enter Role Name.<br/>";
    return strError;
}


function checkRoleName() {
    var roleName = $('#txtRoleName').val();
    var rid = $('#hdnRoleID').val();
    if (roleName != "") {
        $.get('../reportdata.aspx?con=1', function (conMsg) {
            $.getJSON('../reportdata.aspx?proc=CheckRoleName&rName=' + roleName+'&rid='+rid, function (data) {
                var data1 = data;
                if (data.result != undefined) {
                    if (data.result[0].msg != "") {
                        $('#divError').show();
                        $('#divError').empty();
                        $('#divError').html("-" + data.result[0].msg);
                    }
                    else {
                        $('#divError').hide();
                        $('#divError').empty();
                        $('#divSuccess').show();
                        $('#divSuccess').html('Verified.');
                        setTimeout("$('#divSuccess').fadeOut(500);", 2000);
                    }
                }
            });
        }).fail(function () {
            $('.divloading').hide();
            callIos();
        });
    }
    else {
        $('#divError').show();
        $('#divError').empty();
        $('#divError').html("-Please enter Role Name.");
    }
}


function SaveRole() {
    try {
        var strError = ValidateField();
        var rid = $('#hdnRoleID').val();
        var rolename = $('#txtRoleName').val();
        var isactive = $('#chkActive').is(':checked') ? 0 : 1;
        var isGlobal = $('#chkGlobal').is(':checked') ? 0 : 1;
        var amb = '';
        var phy = '';
        var errorflag = true;
        if (rid == -1) {
            if (strError == "")
                errorflag = false;
        }
        else {
            if (strError == "" || flag == 0)
                errorflag = false;
        }
        if (!errorflag) {
            $.get('../reportdata.aspx?con=1', function (conMsg) {
                $('.divloading').show();


                $.each($('#tbodyMenu tr'), function (index, item) {
                    var ambtd = $(item).find('.amb')
                    if ($(ambtd).hasClass('class_0'))
                        amb += item.id + ',';
                    //console.log(amb);
                    var phytd = $(item).find('.phy')
                    if ($(phytd).hasClass('class_0'))
                        phy += item.id + ',';
                    //console.log(phy);
                });



                $.getJSON('../reportdata.aspx?proc=SetMenuRole&rid=' + rid + '&isGlobal=' + isGlobal + '&isactive=' + isactive + '&amb=' + amb + '&phy=' + phy + '&rName=' + encodeURIComponent(rolename), function (data) {
                    var data1 = data;
                    if (data.result != undefined) {
                        if (data.result[0].status != 0) {
                            $('#divError').show();
                            $('#divError').empty();
                            $('#divError').html(data.result[0].msg);
                        }
                        else {
                            resetAndClose();
                            getRole();
                        }
                    }
                }).done(function () {
                    $('.divloading').hide();
                    //resetAndClose();
                }).fail(function () {
                    $('#divError').show();
                    $('#divError').empty();
                    $('#divError').html("-Network Connection Error.");
                    $('.divloading').hide();

                });
            }).fail(function () {
                $('.divloading').hide();
                callIos();
            });
        }
        else {
            $('#divError').show();
            $('#divError').empty();
            $('#divError').html(strError);
        }
    }
    catch (e) {
        $('.divloading').hide();
        $('#divError').show();
        $('#divError').empty();
        $('#divError').html(e);
    }
}



