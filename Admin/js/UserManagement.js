﻿
$(document).ready(function () {
    $('#sidebar').show();
    $('.mask').width($(window).width()).height($(window).height());
    $('.divloading').show();
    $('#txtPassword').val('');
    $('#txtUserName').val('');

    if (navigator.userAgent.match(/iPhone/) != null || navigator.userAgent.match(/iPad/) != null || navigator.userAgent.match(/Android/i) != null) {
        $('#dialog').addClass('savingPopupIpad');
        $('#divMob').show();
        $('#divWeb').hide();

    }
    else {
        $('#divMob').hide();
        $('#divWeb').show();

    }

    getLoginUser();

    $('#btngenerator').click(function () {
        $('#divpwd').fadeIn(500);
        $('#divoverlay').show();
        populateform();
        return false;

    });

    $('#btngenrate').click(function () {
        populateform();
    });

    $('#btnusepass').click(function () {
        Usepassword();
    });

    $('#btncancelpwd').click(function () {
        closegenrator();
    });
});



function getLoginUser() {
    $.get('../reportdata.aspx?con=1', function (conMsg) {
        $.getJSON('../reportdata.aspx?proc=GetUsersForCAutoId', function (data) {
            if (data != undefined && data != null) {
                var data1 = data;
                BindLogiNUser(data1.Table);
            }
        }).done(function () {
            $('.divloading').hide();
        }).fail(function () {
            $('.divloading').hide();
        });
    }).fail(function () {
        $('.divloading').hide();
        callIos();
    });
}

function BindLogiNUser(data) {
    var table = "";
    $('#tbody').empty();
    for (var key in data) {
        if (data[key].username != undefined) {
            table = '<tr id="' + key + '" onclick="ShowFacilityPopup(\'' + data[key].CAutoID + '\',\'' + data[key].Isactive + '\',\'' + data[key].username + '\',\'' + data[key].RoleID + '\',\'' + data[key].isgloballyvisible + '\');activeline(' + key + ')"><td>' + data[key].username + '</td>';
            // table += '<td align="center"> <a id="' + key + '"  href="javascript:void(0);" onclick="ShowResetPopUp(' + data[key].CAutoID + ');" >Reset</a> </td>'
            // table += '<td><a href="javascript:void(0);" onclick="ShowFacilityPopup(\'' + data[key].CAutoID + '\',\'' + data[key].Isactive + '\');">';
            table += '<td>' + data[key].Facilities + '</td>';
            table += '<td>' + data[key].Rolename + '</td>';
            table += '<td >' + data[key].Isactive + '</td>';
            table += '<td >' + data[key].createdby + '</td>';
            table += '<td >' + data[key].modifiedby + '</td></tr>';
            $('#tbody').append(table);
        }
    }
}


function activeline(id) {
    $("#" + id).addClass('active');
}

function ShowFacilityPopup(cid, status, username, roleid, isglobal) {
    var win = $(window).width();
    var diawidth = $('#dialog').width();


    $('#dialog').css('left', (win - diawidth) / 2-10);
    $('#txtUserName').val(username);
    $('#txtPassword').val('');
    $('.divloading').show();
    $('#divError').hide();

    if (cid != '-1') {
        $('#txtUserName').prop('disabled', true);
        $('#imgCheck').hide();
    }
    else {
        $('#txtUserName').prop('disabled', false);
        $('#imgCheck').show();
    }
    if (status == "Active" || cid == -1)
        $('#chkActive').prop('checked', true);
    else
        $('#chkActive').prop('checked', false);

    if (isglobal == 0 || cid == -1) {
        $('#chkGlobal').prop('checked', true);
        isglobal = 0;
    }
    else {
        $('#chkGlobal').prop('checked', false);
        isglobal = 1;
    }
    getRole(isglobal, roleid);

    $('#hdnClintID').val(cid);
    $.get('../reportdata.aspx?con=1', function (conMsg) {
        $.getJSON('../reportdata.aspx?proc=GetFacilitiesNameForCAutoId&cid=' + cid, function (data) {
            var data1 = data;
            BindListBoxes(data1.Table);
        }).done(function () {
            $('.divloading').hide();
        }).fail(function () {
            $('.divloading').hide();
        });
    }).fail(function () {
        $('.divloading').hide();
        callIos();
    });

    $("#btnRight").unbind("click").bind("click", function () {
        $('#lstSelect').append($("#lstUnselect").find(":selected"));

        EnableDisableAllFacility();
    });

    $("#btnLeft").unbind("click").bind("click", function () {
        $('#lstUnselect').append($("#lstSelect").find(":selected"));

        EnableDisableAllFacility();
    });

    $('#chkActive').unbind("click").bind("click", function () {
        //if (!$('#chkActive').is(':checked'))
        //    if (confirm('Are You sure, you want to Inactive this.'))
        //        $('#chkActive').prop('checked', false);
        //    else
        //        $('#chkActive').prop('checked', true);
        if (!$('#chkActive').is(':checked'))
            BootstrapDialog.confirm({
                title: 'Confirmation',
                message: 'Are You sure, you want to Inactive this?',
                btnCancelLabel: 'OK', // <-- Default value is 'Cancel',
                btnOKLabel: 'Cancel', // <-- Default value is 'OK',
                btnOKClass: 'btn-default',
                btnCancelClass: 'btn-primary',

                callback: function (result) {
                    // result will be true if button was click, while it will be false if users close the dialog directly.
                    if (result) {
                        $('#chkActive').prop('checked', true);

                    } else {
                        $('#chkActive').prop('checked', false);
                    }
                }
            });

        setTimeout('$($(".btn-default")[0]).addClass("btn-primary filterButton btn_submit").removeClass("btn-default")', 250);
        setTimeout('$($(".btn-default")[0]).addClass("filterButton btn_submit")', 250);
    })

    $('#chkGlobal').unbind("click").bind("click", function () {
        var isGlobal = ($('#chkGlobal').is(':checked') ? 0 : 1);
        getRole(isGlobal, -1);

    });



    $('#txtPassword,#txtRePassword').keypress(function () {
        flag = 1;

    });

    $('#txtPassword,#txtRePassword').blur(function () {
        if (flag == 1) {
            var strError = ValidateField();
            if (strError != "") {
                $('#divError').show();
                $('#divError').empty();
                $('#divError').html(strError);
            }
            else {
                $('#divError').hide();
            }
        }
    });


    $('#dialog').fadeIn(500);
    if (navigator.userAgent.match(/iPhone/) != null || navigator.userAgent.match(/iPad/) != null || navigator.userAgent.match(/Android/i) != null)
        $('#lfrom').fadeOut(500);
    else
        $('.mask').show();
}

var flag = 0;


function BindListBoxes(data) {
    $('#lstSelect').find('option').remove();
    $('#lstUnselect').find('option').remove();

    var tr = "";
    $('#chkAllFacility').prop({ 'checked': false });
    for (var key in data) {
        if (data[key].Name != undefined) {
            if (data[key].practiceid != "allfacilities"){
            if (data[key].isassigned == 0) {
                $('#lstSelect').append($('<option></option>').val(data[key].practiceid).html(data[key].Name).attr('title', data[key].Name));
                tr += '<tr class="active" id="' + data[key].practiceid + '" title= "' + data[key].Name + '"><td>' + data[key].Name + '</td><tr>';
            }
            else if (data[key].isassigned == 1) {
                $('#lstUnselect').append($('<option></option>').val(data[key].practiceid).html(data[key].Name).attr('title', data[key].Name));
                tr += '<tr class="" id="' + data[key].practiceid + '" title= "' + data[key].Name + '"><td>' + data[key].Name + '</td><tr>';
            }
            }

            if (data[key].practiceid == "allfacilities" && data[key].isassigned == 0)
                $('#chkAllFacility').prop({ 'disabled': false, 'checked': true });
                               
        }
    }
    $('#tbSelect').html(tr);

    //EnableDisableAllFacility();

    $('#tbSelect tr').click(function () {
        if ($(this).hasClass('active'))
            $(this).removeClass('active');
        else
            $(this).addClass('active');

        if ($('#tbSelect tr').length == $('#tbSelect').find('.active').length)
            $('#divAll').addClass('active');
        else
            $('#divAll').removeClass('active');

        EnableDisableAllFacility();
    });


    $('#divAll').click(function () {
        if ($('#divAll').hasClass('active')) {
            $('#divAll').removeClass('active');
            $('#tbSelect tr').removeClass('active');
        }
        else {
            $('#divAll').addClass('active');
            $('#tbSelect tr').addClass('active');
        }
    });
}

$('#lstUnselect').change(function () {

    $(this).css('background', '#940006');
});

function checkUserName() {
    var uname = $('#txtUserName').val();
    if (uname != "") {
        $.get('../reportdata.aspx?con=1', function (conMsg) {
            $.getJSON('../reportdata.aspx?proc=CheckUsername&uname=' + uname, function (data) {
                var data1 = data;
                if (data.result != undefined) {
                    if (data.result[0].msg != "") {
                        $('#divError').show();
                        $('#divError').empty();
                        $('#divError').html("-" + data.result[0].msg);
                    }
                    else {
                        $('#divError').hide();
                        $('#divError').empty();
                        $('#divSuccess').show();
                        $('#divSuccess').html('Verified.');
                        setTimeout("$('#divSuccess').fadeOut(500);", 2000);
                    }
                }
            });
        }).fail(function () {
            $('.divloading').hide();
            callIos();
        });
    }
    else {
        $('#divError').show();
        $('#divError').empty();
        $('#divError').html("-Please enter User Name.");
    }
}

function resetAndClose() {
    $('#tbody tr').removeClass('active');
    $('#txtUserName').val('');
    $('#txtPassword').val('');
    $('#hdnClintID').val('');
    $('#txtRePassword').val('');
    $('#lstSelect').find('option').remove();
    $('#lstUnselect').find('option').remove();
    $('#dialog').fadeOut(500);
    if (navigator.userAgent.match(/iPhone/) != null || navigator.userAgent.match(/iPad/) != null || navigator.userAgent.match(/Android/i) != null)
        $('#lfrom').fadeIn(500);
    $('.mask').hide();
    flag = 0;
}


function ValidateField() {
    var cid = $('#hdnClintID').val();
    var username = $('#txtUserName').val();
    var pwd = $('#txtPassword').val();
    var repwd = $('#txtRePassword').val();
    var strError = "";
    // var isactive = $('#btnInactive').val() == "Active" ? 0 : 1;
    //    if (cid == -1) {

    if (username == "")
        strError += "-Please Enter User Name.<br/>";
    if (pwd == "")
        strError += "-Please Enter Password.<br/>";
    if (repwd == "")
        strError += "-Please Enter Confirm Password.<br/>";
    if (pwd != "" && repwd != "") {
        if (pwd != repwd)
            strError += "-Password not Match.<br/>";
    }

    //    if ($('#ddlRole').val() == -1) {
    //        strError += "-Please Select Role.<br/>";
    //    }

    //    }
    return strError;
}


////////////// Save User Facility //////////////////////
function SaveUserFacility() {
    try {
        var strError = ValidateField();
        var cid = $('#hdnClintID').val();
        var username = $('#txtUserName').val();
        var pwd = $('#txtPassword').val();
        var repwd = $('#txtRePassword').val();
        var isactive = $('#chkActive').is(':checked') ? 0 : 1;
        var roleid = $('#ddlRole').val();

        var allfsc = $('#chkAllFacility').is(':checked') ? 0 : 1;

        var errorflag = true;
        if (cid == -1) {
            if (strError == "")
                errorflag = false;
        }
        else {
            if (strError == "" || flag == 0)
                errorflag = false;
        }
        if (!errorflag) {
            $.get('../reportdata.aspx?con=1', function (conMsg) {
                $('.divloading').show();
                var autoid = '';
                if (navigator.userAgent.match(/iPhone/) != null || navigator.userAgent.match(/Android/i) != null) {
                    if ($(window).height() < 650 && $(window).he() < 650) {
                        $.each($('#tbSelect .active'), function (index, item) {
                            if (index == 0)
                                autoid = $(item).attr('id');
                            else
                                autoid += ',' + $(item).attr('id');

                        });
                    }
                }
                else {
                    $.each($("#lstSelect").find("option"), function (index, item) {
                        if (index == 0)
                            autoid = $(item).val();
                        else
                            autoid += ',' + $(item).val();
                    });
                }

                $.getJSON('../reportdata.aspx?proc=setFacilitiesNameForCAutoId&autoid=' + autoid + '&username=' + encodeURIComponent(username) + '&cautoid=' + cid + '&roleid=' + roleid + '&isactive=' + isactive + '&pwd=' + encodeURIComponent(pwd) + '&allfsc=' + allfsc, function (data) {
                    var data1 = data;
                    if (data.result != undefined) {
                        if (data.result[0].status != 0) {
                            $('#divError').show();
                            $('#divError').empty();
                            $('#divError').html(data.result[0].msg);
                        }
                        else {
                            resetAndClose();
                            getLoginUser();
                        }
                    }
                }).done(function () {
                    $('.divloading').hide();
                    //resetAndClose();
                }).fail(function () {
                    $('#divError').show();
                    $('#divError').empty();
                    $('#divError').html("-Network Connection Error.");
                    $('.divloading').hide();

                });
            }).fail(function () {
                $('.divloading').hide();
                callIos();
            });
        }
        else {
            $('#divError').show();
            $('#divError').empty();
            $('#divError').html(strError);
        }
        //    }
        //    else {
        //        $('#divError').show();
        //        $('#divError').empty();
        //        $('#divError').html("-User Not Active.");
        //    }
    }
    catch (e) {
        $('.divloading').hide();
        $('#divError').show();
        $('#divError').empty();
        $('#divError').html(e);
    }
}


var keylist = "abcdefghijklmnopqrstuvwxyz0123456789"
var temp = ''

function generatepass() {
    temp = ''
    for (i = 0; i < 10; i++)
        temp += keylist.charAt(Math.floor(Math.random() * keylist.length))
    return temp
}

function populateform() {
    $('#txtpwd').val(generatepass());
    return false;
}

function openpassowrdgenerator() {
    //document.getElementById('divpwd').style.display = 'block';
    $('#divpwd').fadeIn(500);
    $('#divoverlay').show();
    populateform();
    return false;
}

function closegenrator() {
    //document.getElementById('divpwd').style.display = 'none';
    $('#divpwd').fadeOut(500);
    $('#divoverlay').hide();
    return false;
}
function Usepassword() {
    if ($('#chk').is(":checked")) {
        var pwd = $('#txtpwd').val();
        $('#txtPassword').val(pwd);
        $('#txtRePassword').val(pwd);
        $('#divpwd').fadeOut(500);
        $('#divoverlay').hide();
    }
    return false;
}


function ShowResetPopUp(id) {
    $('#hdnClintID').val(id);
    $('#divResetError').hide();
    // var position = $("#"+id).offset();
    $('#divReset').css('left', '40%');
    $('#divReset').css('top', '30%');
    $('#divReset').show();
    $('.mask').show();

}

function HideResetPopUp() {
    $('#divReset').hide();
    $('.mask').hide();
    $('#hdnClintID').val('');
    $("#txtNpassword").val('');
    $('#divResetError').hide();
}
function ResetPassword(cid) {
    var strerror = "";
    if ($("#txtNpassword").val() == "")
        strerror += "-Please enter New Password.";
    if (strerror == "") {
        $('.divloading').show();

        var cid = $('#hdnClintID').val();
        var pwd = $('#txtNpassword').val();
        $.getJSON('../reportdata.aspx?proc=ResetPassWordForCAutoId&cid=' + cid + '&pwd=' + pwd, function (data) {
            var data1 = data;
            if (data.result != undefined) {
                if (data.result[0].msg != 0) {
                    $('#divResetError').show();
                    $('#divResetError').empty();
                    $('#divResetError').html("-Error Occure.");
                }
                else {
                    $('#divResetError').hide();
                    $('#divResetError').empty();
                    HideResetPopUp();
                }
            }
        }).done(function () {
            $('.divloading').hide();
        }).fail(function () {
            $('.divloading').hide();

        });
    }
    else {
        $('#divResetError').show();
        $('#divResetError').empty();
        $('#divResetError').html(strerror);
    }
}

function ActiveInactiveUser() {
    var x = confirm("Do you want to " + $('#btnInactive').val() + " this?");
    if (x) {
        var isactive = $('#btnInactive').val() == "Active" ? 0 : 1;
        var cid = $('#hdnClintID').val();
        var autoid = '';
        //        $.each($("#lstSelect").find("option"), function (index, item) {
        //            if (index == 0)
        //                autoid = $(item).val();
        //            else
        //                autoid += ',' + $(item).val();
        //        });
        var username = '';
        var pwd = '';
        $.getJSON('../reportdata.aspx?proc=setFacilitiesNameForCAutoId&autoid=' + autoid + '&username=' + username + '&pwd=' + pwd + '&cautoid=' + cid + '&isactive=' + isactive, function (data) {
            var data1 = data;
            if (data.result != undefined) {
                if (data.result[0].status != 0) {
                    $('#divError').show();
                    $('#divError').empty();
                    $('#divError').html(data.result[0].msg);
                }
                else {
                    resetAndClose();
                    getLoginUser();
                }
            }
        }).done(function () {
            //resetAndClose();
        }).fail(function () {
            $('#divError').show();
            $('#divError').empty();
            $('#divError').html("-Connection Error.");
        });
    }
    else {
        // resetAndClose();
    }
}

///////////////////////// ROLE MANAGEMENT //////////////////////////////////////////////////////////////////

function getRole(isGlobal, roleId) {
    $.get('../reportdata.aspx?con=1', function (conMsg) {
        $.getJSON('../reportdata.aspx?proc=RoleManagement_getMenuRole&isGlobal=' + isGlobal + '&rid=-1&status=0', function (data) {
            var data1 = data;
            BindRole(data1.Table, roleId);
        }).done(function () {
            $('.divloading').hide();
        }).fail(function () {
            $('.divloading').hide();
        });
    }).fail(function () {
        $('.divloading').hide();
        callIos();
    });
}

function BindRole(data, roleId) {
    $('#ddlRole').find('option').remove();

    $("#ddlRole").append($("<option></option>").val
                (-1).html("Select Role"));

    for (var key in data) {
        if (data[key].roleid != undefined) {
            $("#ddlRole").append($("<option></option>").val
                (data[key].roleid).html(data[key].rolename));
        }
    }
    $("#ddlRole").val(roleId);
}


function EnableDisableAllFacility() {
    if (navigator.userAgent.match(/iPhone/) != null || navigator.userAgent.match(/Android/i) != null) {
        if ($(window).height() < 650 && $(window).he() < 650) {
            if ($('#tbSelect').find('.active').length == 1)
                $('#chkAllFacility').prop({ 'disabled': true, 'checked': false });
            else
                $('#chkAllFacility').prop('disabled', false);
        }
        else {
            if ($('#lstSelect').find('option').length < 2)
                $('#chkAllFacility').prop({ 'disabled': true, 'checked': false });
            else
                $('#chkAllFacility').prop('disabled', false);
        }
    }
    else {
        if ($('#lstSelect').find('option').length < 2)
            $('#chkAllFacility').prop({ 'disabled': true, 'checked': false });
        else
            $('#chkAllFacility').prop('disabled', false);
    }
}