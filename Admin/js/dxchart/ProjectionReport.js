﻿//////  Project Report //////////////////

$(document).ready(function () {
    Showloading();

    if ($('#ContentHolder_hdnclientfolder').val() == "jefbar") {
        $('.ccalogo').attr('alt', 'Jefbar');
        $('.marslogo').attr('alt', 'AMB');
        $('#reportname').css('color', '#780100');
        $('#imgLoadingCCA').hide();
        $('#imgLoadingjefbar').show();
    }
    else {
        $('#imgLoadingjefbar').hide();
        $('#imgLoadingCCA').show();
    }
    IPhoneStyle();
    $("#divdatascroll").tooltip({ track: true });
    setscrollheight();
    SetjqTransform()
    bindreportmenu();

    changereportchart(0);
    CheckComparebasedonvalue();
});



function SetjqTransform() {
    $("#lfrom").removeClass("jqtransformdone");
    $("#lfrom").jqTransform();

   
    $(".jqTransformInputWrapper").on("click", function () {
        setoffset();
    });

 }


////////////////////  Set Scroll divs Height///////////////////////////////////////
function setscrollheight() {

    try {
        var mainhead = $('#tabs-container').outerHeight();
        var greenhead = $('#nav-container').outerHeight();
        var rpthead = $('.reportschooseblack').outerHeight();
        var footer = $('.footer').outerHeight();
        var winheight = $(window).height();
        $('#divmenuscroll').height(winheight - (mainhead + greenhead + rpthead + footer));
        $('#divdatascroll').height(winheight - (mainhead + greenhead + rpthead + footer));
        $('#divReportDashboard').width($(window).width() * 84 / 100);
        if (navigator.userAgent.match(/iPhone/i) == null && navigator.userAgent.match(/iPad/i) == null) {

            //            if (!$('.x-grid3-scroller').hasClass('nano'))
            //                $('.x-grid3-scroller').addClass('nano')
            //            $('.x-grid3-scroller').css('overflow', 'hidden');
            $('.nano').perfectScrollbar().perfectScrollbar('destroy');
            $('.nano').perfectScrollbar();
            setTimeout("$('.nano').perfectScrollbar('update');", 1000);
        }
        else {
            if (navigator.userAgent.match(/iPad/i) != null)
                $('#divdatascroll').width($(window).width() * 84 / 100);
            else
                $('#divdatascroll').width($(window).width() * 99 / 100);
            $('.nano').css('overflow', 'auto');
        }
    }
    catch (e) {
        //alert(e);
    }


}


$(window).resize(function () {
    setscrollheight();


});


function Showloading() {
    //$('.divloading').css('z-index', '3000');
    $('.divloading').show();
    $('.divmask').show();
}
function HideLoading() {
    //$('.divloading').css('z-index', '-1');
    $('.divloading').hide();
    $('.divmask').hide();
}





function bindreportmenu() {

   // $.get('../reportdata.aspx?sp=reportmenu', function (data) {
        var data ; //JSON.parse(data);

      //  if (!data.hasOwnProperty('error')) {

            getreportmenu(data);

       // }
    //    else {
     //       if (data.error[0].error == "error")
    //            shownodata();
   //         else
    //            document.write(data.error[0].error);
    //    }

  //  });
}



/////////////////Reporting Menu///////////////////////////////////////////////////////////////////////////
function getreportmenu(data) {
    var imageurl = "../images/reportsicon/" + $('#ContentHolder_hdnclientfolder').val();
    var imgoverview = "../images/reportsicon/" + $('#ContentHolder_hdnclientfolder').val() + "/overview_active.png";



    var table = '<div class="grayrctext" style="text-align:left;"><ul>';

    table += '<li class="active"><a onclick="return changereportchart(0);" href="javascript:;" > <table><tr><td style="width:20%">';
    table += '<img id="0" style="height:50%; width:50%" src="' + imgoverview + '"/> </td>';
    table += '<td align="left">OVERVIEW';
    table += '</td></tr><tr><td colspan="2"><div class="graysp" style="width:96%"></div></td></tr></table></a></li>';

    table += '<li class="active"><a onclick="return changereportchart(1);" href="javascript:;" > <table><tr><td style="width:20%">';
    table += '<img id="1" style="height:50%; width:50%" src=""/> </td>';
    table += '<td align="left">CHARGES';
    table += '</td></tr><tr><td colspan="2"><div class="graysp" style="width:96%"></div></td></tr></table></a></li>';

    table += '<li class="active"><a onclick="return changereportchart(2);" href="javascript:;" > <table><tr><td style="width:20%">';
    table += '<img id="2" style="height:50%; width:50%" src=""/> </td>';
    table += '<td align="left">PAYMENTS';
    table += '</td></tr><tr><td colspan="2"><div class="graysp" style="width:96%"></div></td></tr></table></a></li>';

    table += '<li class="active"><a onclick="return changereportchart(3);" href="javascript:;" > <table><tr><td style="width:20%">';
    table += '<img id="3" style="height:50%; width:50%" src=""/> </td>';
    table += '<td align="left">Clients >=5% of proj';
    table += '</td></tr><tr><td colspan="2"><div class="graysp" style="width:96%"></div></td></tr></table></a></li>';
   
   
   
   
   
   
   
   
   
    var html = "";
    html += "<div class='grayrctext1'><p class='active report'><a href='javascript:;' onclick='return changereportchart(0);'>"
    html += "<img id='reportdashboardimg' alt='' src='" + imgoverview + "'><br></a><a href='javascript:;' class='report' onclick='changereportchart(0);'> OVERVIEW";
    html += "</a></p><div class='graysp'>   </div></div><div class='grayrctext'>";

//    for (var key in data) {
//        if (data[key].ID != undefined) {

//            /// For Mobile Menu
//            if (data[key].ID == 0) {
//                table += '<li class="active"><a onclick="return changereportchart(0);" href="javascript:;" > <table><tr><td style="width:20%">';
//                table += '<img id="0" style="height:50%; width:50%" src="' + imgoverview + '"/> </td>';
//                table += '<td align="left">OVERVIEW';
//                table += '</td></tr><tr><td colspan="2"><div class="graysp" style="width:96%"></div></td></tr></table></a></li>';
//            }
//            else {
//                table += '<li><a  onclick="return changereportchart(' + data[key].ID + ');" href="javascript:;" > <table><tr><td style="width:20%">';
//                table += '<img id="' + data[key].ID + '" style="height:50%; width:50%" src="' + imageurl + '/' + data[key].reportname.trim() + '.png"/> </td>';
//                table += '<td align="left">' + data[key].reportname; ;
//                table += '</td></tr><tr><td colspan="2"><div class="graysp" style="width:96%"></div></td></tr></table></a></li>';
//            }

//            // For Web Menu
//            if (data[key].ID != 0 && data[key].ID != undefined) {
//                html += "<p class='report'><a href='javascript:;' onclick='return changereportchart(" + data[key].ID + ");'>"
//                html += "<img id='" + data[key].ID + "' alt='' src='" + imageurl + "/" + data[key].reportname.trim() + ".png' />";
//                html += "<br></a><a href='javascript:;' class='report' onclick=' changereportchart(" + data[key].ID + ");' style='margin:5px'>" + data[key].reportname;
//                html += "</a></p><div class='graysp'>   </div>";
//            }


//        }
//    }
    table += '</ul></div>'

    $('#divselectmenu').html(table);

    $('.reportcenter').off('click').on('click', function () {
        if ($('#divmenuscroll').is(':hidden')) {
            $('#divselectmenu').toggle("slow");
            var position = $(this).position();
            $('#divselectmenu').css('top', position.top + 20);
            $('#divselectmenu').css('left', position.left + 5);
        }
    });


    html += "<p class='report'><a href='javascript:;' onclick='return changereportchart(1);'>"
                    html += "<img id='1' alt='' src='" + imageurl + "/ + data[key].reportname.trim() + .png' />";
                    html += "<br></a><a href='javascript:;' class='report' onclick='return changereportchart(1);' style='margin:5px'>CHARGES"
                    html += "</a></p><div class='graysp'>   </div>";

                    html += "<p class='report'><a href='javascript:;' onclick='return changereportchart(2);'>"
                    html += "<img id='2' alt='' src='" + imageurl + "/ + data[key].reportname.trim() + .png' />";
                    html += "<br></a><a href='javascript:;' class='report' onclick='return changereportchart(2);' style='margin:5px'>PAYMENTS"
                    html += "</a></p><div class='graysp'>   </div>";


                    html += "<p class='report'><a href='javascript:;' onclick='return changereportchart(3);'>"
                    html += "<img id='3' alt='' src='" + imageurl + "/ + data[key].reportname.trim() + .png' />";
                    html += "<br></a><a href='javascript:;' class='report' onclick='return changereportchart(3);' style='margin:5px'>Clients >=5% of proj"
                    html += "</a></p><div class='graysp'>   </div>";

   
    html += "</div>";

    $("#divReportmenu").html(html);
    if (navigator.userAgent.match(/iPhone/i) == null && navigator.userAgent.match(/iPad/i) == null) {
        $('.nano').perfectScrollbar().perfectScrollbar('destroy');
                $('.nano').perfectScrollbar();

    }
    else {
        $('.nano').css('overflow', 'auto');
       // $('.nano').perfectScrollbar('destroy');
    }
}


//////////////////Report Click Event ///////////////////////////////////////////////////

function changereportchart(id) {

    Showloading();

    if (navigator.userAgent.match(/iPhone/i) != null)
        $('#txtDateRange').daterangepicker({ arrows: false });

    if (!$('#divselectmenu').is(':hidden'))
        $('#divselectmenu').toggle("slow");

    $('#hdnreportid').val(id);

    $('#divComapareforSelection').hide();
    $('#divtreeCarrier').hide();
    $('#txtSelectedSearhfield').val('');
    CheckComparebasedonvalue();
    ExportAll();
    if (id > 0) {
      
       
        $('.content').css('top', '0px');

        $('.ps-scrollbar-y-rail').css('top', '0px');
        $('.ps-scrollbar-y').css('top', '0px');
        $('#divnodata').hide();
        $('#divReportDashboard').hide();
        $('#Divchart').show();


        BindGrid(id);
       
    }
    else {
        $('#divReportDashboard').show();
        $('#Divchart').hide();
       
        $('#divnodata').hide();
        $('#divcomingsoon').hide();
        $('#divfinancialsummaryCover').hide();
      
      
        BindGrid(0);
    }
    ApplyCssActiveReport();
   
    if (navigator.userAgent.match(/iPhone/i) == null && navigator.userAgent.match(/iPad/i) == null) {
//        if (!$('.x-grid3-scroller').hasClass('nano'))
//            $('.x-grid3-scroller').addClass('nano')
//        $('.x-grid3-scroller').css('overflow', 'hidden');
        $('.nano').perfectScrollbar().perfectScrollbar('destroy');
        $('.nano').perfectScrollbar();
        setTimeout("$('.nano').perfectScrollbar('update');", 500);
        

    }
    else {
        $('.nano').css('overflow', 'auto');
        $('.nano').perfectScrollbar('destroy');
    }
    return false;
}


///
function Onclick_Go() {
    Showloading();
    if (navigator.userAgent.match(/iPhone/i) != null)
        $('#txtDateRange').daterangepicker({ arrows: false });

    if (!$('#divselectmenu').is(':hidden'))
        $('#divselectmenu').toggle("slow");

    var id= $('#hdnreportid').val();

    $('#divComapareforSelection').hide();
    $('#divtreeCarrier').hide();
    $('#txtSelectedSearhfield').val('');
    CheckComparebasedonvalue();
    ExportAll();
    if (id > 0) {


        $('.content').css('top', '0px');

        $('.ps-scrollbar-y-rail').css('top', '0px');
        $('.ps-scrollbar-y').css('top', '0px');

        $('#divReportDashboard').hide();
        $('#Divchart').show();
        
        $('#divnodata').hide();
        $('#divGridPrint').show();
        $('#divDashboardPrint').hide();
        BindGrid(id);
    }
    else {
        $('#divReportDashboard').show();
        $('#Divchart').hide();
       
        $('#divnodata').hide();
        $('#divcomingsoon').hide();
        $('#divfinancialsummaryCover').hide();
        $('#divGridPrint').hide();
        $('#divDashboardPrint').show();
        BindGrid(0);
    }
   
    ApplyCssActiveReport();

    if (navigator.userAgent.match(/iPhone/i) == null && navigator.userAgent.match(/iPad/i) == null) {
//        if (!$('.x-grid3-scroller').hasClass('nano'))
//            $('.x-grid3-scroller').addClass('nano')
//        $('.x-grid3-scroller').css('overflow', 'hidden');
        $('.nano').perfectScrollbar().perfectScrollbar('destroy');
        $('.nano').perfectScrollbar();
        setTimeout("$('.nano').perfectScrollbar('update');", 500);


    }
    else {
        $('.nano').css('overflow', 'auto');
        $('.nano').perfectScrollbar('destroy');
    }
    return false;
}





////////// Show No data /////////////////////////////////////////////
function shownodata() {
    $('#divnodata').show();
    $('#divReportDashboard').hide();
    $('#Divchart').hide();
    $('#divcomingsoon').hide();
    HideLoading();
    $('.nano').perfectScrollbar().perfectScrollbar('destroy');
    $('.nano').perfectScrollbar();
}



function BindGrid(id) {
    try {
        
        $('#divProjectionGrid, #divProjectionGrid1, #divProjectionGrid2').empty();
        var date = "";
        if ($('#txtDate').val() != "" && $('#txtDate').val().trim() != "SELECT DATE")
            date = $('#txtDate').val();

        if (id > 0) {
            if (id == 1)
                BindChargesGrid(date);
            else if (id == 2)
                BindPaymentsGrid(date);
            else if (id == 3)
                BindClientsGreaterFivePercentProjectionGrid(date);
        }
        else {
            getActualVsBenchmark("", date);
        }
    }
    catch (e) {
        HideLoading();
        shownodata();
 }
}


function BindChargesGrid(date) {
    $.get('../ProjectionReportdata.aspx?sp=getProjectionCharges&date=' + date, function (data) {
        var data = JSON.parse(data);
        if (data['error'] == undefined) {
            getActiveClientsChargesGrid(data.Table, 'divProjectionGrid');
            getConversionClientsChargesGrid(data.Table1, 'divProjectionGrid1');
            getTotalChargesGrid(data.Table2, 'divProjectionGrid2');

            setTimeout("getGridJsScrollBar();", 1000);
        }
        else {
            HideLoading();
            shownodata();
        }

    }).done(function () {
        HideLoading();
    }).fail(function () {
        HideLoading();
        shownodata();
    });
}

function BindPaymentsGrid(date) {
    $.get('../ProjectionReportdata.aspx?sp=getProjectionPayments&date=' + date, function (data) {
        var data = JSON.parse(data);
        if (data['error'] == undefined) {
            getActiveClientsPaymentsGrid(data.Table, 'divProjectionGrid');
            getConversionClientsPaymentsGrid(data.Table1, 'divProjectionGrid1');
            getTotalPaymentsGrid(data.Table2, 'divProjectionGrid2');

            setTimeout("getGridJsScrollBar();", 1000);
        }
        else {
            HideLoading();
            shownodata();
        }

    }).done(function () {
        HideLoading();
    }).fail(function () {
        HideLoading();
        shownodata();
    });
}


function BindClientsGreaterFivePercentProjectionGrid(date) {
    $.get('../ProjectionReportdata.aspx?sp=getClientsGreaterFivePercentProjection&date=' + date, function (data) {
        var data = JSON.parse(data);
        if (data['error'] == undefined) {
            getActiveClientsChargesGrid(data.Table, 'divProjectionGrid');
            setTimeout("getGridJsScrollBar();", 1000);
        }
        else {
            HideLoading();
            shownodata();
        }

    }).done(function () {
        HideLoading();
    }).fail(function () {
        HideLoading();
        shownodata();
    });
}


//////////////// Export Grid //////////////////////////////////////////////////////////
function ExportAll() {
    $('.exportXls').unbind('click').bind("click", function () {
        try {

            var id;
            id = $('#hdnreportid').val();
            var comcol = "";
            var grpcol = "";
            var zcol = "";
            if (id == 0) {
                comcol = "";
                grpcol = "rowgrp";
                zcol = "value";
            }

            document.getElementById('iframeExport').src = "Exporter.aspx?type=xls&id=" + id + "&name=" + $('#divmenuscroll .active a')[1].innerHTML+"&comcol="+comcol+"&grpcol="+grpcol+"&zcol="+zcol;
            HideLoading();
            return false;

        }
        catch (e) {
            // alert(e);
            return false;
        }
    });
}



function IPhoneStyle() {
    if ($(window).width() < 650) {
        $('#dgFilter').append($('#divddlFilter'));
        $('#divfilterbtn').show();
        $('#dgFilter').hide();
        $('#help').addClass('helpIphone');
        $('#dgFilter').addClass('filter');

        var iteg = $('#help').find('.icon-question-sign');
        $('#help').empty();
        $('#help').append(iteg);
        var iteg1 = $('.reportcenter').find('.icon-list');
        $('.reportcenter').empty();
        $('.reportcenter').append(iteg1);

        $('.reportschooseblack i').css('font-size', '24px').css('color', '#fff');

        // $('#dgFilter').dialog("close");
        $('.panel-body').show();
        $('#divfilterbtn').on('click', function () {
            setoffset();
            ShowddlSearchInBox();
            $('#dgFilter').css('left', 5).css('top', $(this).position().top + 31);

            if ($('#dgFilter').is(':visible'))
                $('#divddlFilter').hide();
            else
                $('#divddlFilter').show();
            $('#dgFilter').slideToggle('fast');



        });

    }



}


function ShowddlSearchInBox() {
    if ($(window).width() < 650) {
        $('#divddlFilter').addClass('ddlFilter');
        var id = $('#hdnreportid').val();
        if (id == '18' || id == '20' || id == '22' || id == '23' || id == '25' || id == '30')
            $('#hrFilter').hide();
        else
            $('#hrFilter').show();

        if (id == '21' || id == '24' || id == '26') {
            if ($('#divnodata').is(':hidden')) {
                $('#dgFilter').css('height', '120px');
                $('#divGobtn').css('margin-top', '35px');
                $('#divGobtn').css('margin-left', '-15px');

            }
            else
                $('#dgFilter').css('height', '40px');
        }
        else if (id == '28' || id == '27') {
            $('#dgFilter').css('height', '85px');
            $('#divcarrierTreePanel').css('margin-right', '10px');
            $('#divGobtn').css('margin-top', '0px');
            $('#divGobtn').css('margin-left', '-20px');

        }
        else
            $('#dgFilter').css('height', '40px');

    }
}