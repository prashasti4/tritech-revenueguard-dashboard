﻿
var AndroidAgent = navigator.userAgent.match(/Android/i) != null;
var iPadAgent = navigator.userAgent.match(/iPad/i) != null;
var iPhoneAgent = navigator.userAgent.match(/iPhone/i) != null;

var isAllFacility = false;

$(document).ready(function () {
    $('#sidebar').show();
    isAllFacility = document.getElementById("ContentHolder_hdnFacility").value.toString().split('-')[0].split(',').length > 1 ? true : false;

    Showloading();
    $('#txtDateRange').daterangepicker({ arrows: false });
    if ($('#ContentHolder_hdnclientfolder').val() == "jefbar") {
        $('.ccalogo').attr('alt', 'Jefbar');
        $('.marslogo').attr('alt', 'AMB');
        $('#reportname').css('color', '#780100');
        $('#imgLoadingCCA').hide();
        $('#imgLoadingjefbar').show();

        if (location.href.toLowerCase().indexOf('mydashboard.ambulancerevenue') == -1) {
            $('#imgLoadingjefbar').attr('src', '../images/loading_Dashboard.gif');
            $('.divloading').css('border', '0px none transparent');
        }

    }
    else {
        $('#imgLoadingjefbar').hide();
        $('#imgLoadingCCA').show();
    }

    var nooffacility = getCookie("nooffacility");
    if (isAllFacility) {
        // if ($('#ContentHolder_hdnCAutoId').val() == '2096192288') {
        $('#divOverView').show();
        $('#lblFacilityName').hide();
        // }
    }
    else
        $('#divOverView').hide();

    IPhoneStyle();
    $('#Nodatatext').html('');
    setscrollheight();
    if (showspeed()) {
        bindOverview();
        //TreeURLOverView();
        getOnreadyfunc();
    }
    var height = $(window).height() * 6 / 10;
    $('#divinnerchartContainer').height(height)
    $('#divinnerchartContainer1').height(height)
    $('#divinnerchartContainer2').height(height)


    $('#imgHtml').hide();
    $('#imgHtmlChart').hide();
    // $('#imgExcel').hide();
    $('#imgExcelchart').hide();


});

function SetjqTransform() {
    $("#lfrom").removeClass("jqtransformdone");
    $("#lfrom").jqTransform();


    $('#txtDateRange').on('click', function () {
        $('.ms-drop').hide();
    });


    $(".jqTransformInputWrapper").on("click", function () {
        setoffset();

    });

    $("#divddlBranch .jqTransformSelectWrapper").on("click", function () {
        $("#divComapareforSelection").hide();
        $("#divtreeProvider").hide();
        $("#divtreeCarrier").hide();

    });

    $("#divddlBranch .jqTransformSelectWrapper li a").on("click", function () {
        document.getElementById('txtProviderSearch').value = '';
    });

}
function getOnreadyfunc() {
    setDateNCompny();
    BindYear();
    bindBranch();

    CheckComparebasedonvalue();
    $('.lnkDeshboard').on("click", function () {
        $('#hdnreportid').val(0);
        changereportchart(0);
        ApplyCssActiveReport();
        return false;
    });

    $('#divCompareShow').addClass('canccompareDisable');


    $('#btnClear').on('click', function () {
        $('#txtDateRange').val("SELECT PERIOD");
        $('#txtDate').val("SELECT DATE");
        $('#txtfrom').val("FROM DATE");
        $('#txtto').val("TO DATE");
        $('#txtMonth').val("TO MONTH");
        CheckComparebasedonvalue();
        var date = new Date().getFullYear();
        $('.mtz-monthpicker-year').val(date);
        $('.mtz-monthpicker').removeClass('ui-state-active');
        BindYear();
        Onclick_Go();
    });

    $('.tab').click(function () {
        $('#divLocationCharts,#divLocationGrid').empty();
        if (this.id == 'lichart') {
            $('#lichart').addClass('activetab');
            $('#litext').removeClass('activetab');
            getinternalCahrtdata($('#hdnreportid').val());
            $('#divLocationCharts').show();
            $('#divLocationGrid').hide();
        }
        else {
            $('#lichart').removeClass('activetab');
            $('#litext').addClass('activetab');
            $('#divLocationGrid').show();
            $('#divLocationCharts').hide();
            getLocationGrid();
        }
    });

    $("#divfinancialsummary").tooltip({ track: true });

    bindreportmenu();

    //setTimeout("dashboarddata();", 1000);
}


////

$(document).click(function (e) {
    var target = e.target;
    if (!$(target).is('.reportcenter') && !$(target).is('.icon-list'))
        $('#divselectmenu').hide();
    if (!$(target).is('.jqDropdown')) {
        $("#divComapareforSelection").hide();
        $("#divtreeProvider").hide();
        $("#divtreeCarrier, #divtreeviewOverview").hide();
    }
    if (!$(target).is('.icon-cog'))
        $('#divSettings').hide();
    if ($(window).width() < 650) {
        if (!$(target).is('.icon-filter') && $('#dgFilter').find(target).length == 0) {
            $('#dgFilter').hide();
        }
    }
});


function setDateNCompny() {
    $.get('../reportdata.aspx?sp=getDashboardDate', function (data) {
        var data = JSON.parse(data);
        $('#lblFacilityName').text(data.Table[0].Name);
        $('#lblDateString').text(data.Table[0].Date);
 isAllFacility = (data.Table[0].ShowFacilityDropDown == 0 ? true : false);

       if (isAllFacility) {
           // if ($('#ContentHolder_hdnCAutoId').val() == '2096192288') {
           $('#divOverView').show();
           $('#lblFacilityName').hide();
           // }
       }
       else{
           $('#divOverView,#divGobtn').hide();
$('#lblFacilityName').show();
}
    });


}


//bring data from server for Dashboard char & menu

function bindreportmenu() {
    var fid = $('#txtOverviewSearch').val();
    $.post('../reportdata.aspx', { 'sp': 'reportmenu', 'fid': fid }, function (data) {
        var data = JSON.parse(data);

        if (!data.hasOwnProperty('error')) {
            if (data.Table != undefined) {
                getreportmenu(data.Table);

                changereportchart(data.Table[0].ID);
            }

        }
        else {
            if (data.error[0].error == "error")
                shownodata();
            else
                document.write(data.error[0].error);
        }

    });
}



function Showloading() {
    //$('.divloading').css('z-index', '3000');
    $('.divloading').show();
    $('.divmask').show();
}
function HideLoading() {
    //$('.divloading').css('z-index', '-1');
    $('.divloading').hide();
    $('.divmask').hide();
    $('#divslow').hide();
}

//// BIND YEAR DROPDOWN //////////////////
function BindYear() {
    var year = new Date().getFullYear();
    $("#ddlYear").removeClass('jqTransformHidden');
    $("#ddlYear").removeClass('jqtransformdone');
    $("#ddlYear").removeClass('jqTransformSelectWrapper');
    $("#ddlYear").find('option').remove().end();
    var ddl = $('#divYear .jqTransformSelectWrapper').find("#ddlYear");
    $('#divYear .jqTransformSelectWrapper').remove();
    $('#divYear').append(ddl);
    ddl = null;
    $("#ddlYear").append($("<option></option>").val
                (0).html("SELECT YEAR"));
    for (var i = 0; i < 10; i++) {
        var y = year - i;
        $("#ddlYear").append($("<option></option>").val
                (y).html(y));
    }


    SetjqTransform();

    $("#divYear .jqTransformSelectWrapper ul li a").on("click", function () {

        var id = $('#hdnreportid').val();
        Onclick_Go();

        document.getElementById("divCompareShow").setAttribute("class", "canccompareDisable");
        CheckComparebasedonvalue();
    });
}

$(document).keyup(function (e) {
    var code = e.keyCode;

    if (code == 27) {
        $('#divChartPopup').dialog('close');
    }
});


function dashboarddata() {

    $('#divChartPopup').dialog({
        autoOpen: false,
        draggable: false,
        resizable: false,
        autoResize: true,
        modal: true,
        width: ($(window).width() * 4 / 5),
        height: ($(window).height() * 3 / 5),
        minheight: ($(window).height() * 3 / 5),
        closeOnEscape: true
    });
    $('#divChartPopup').dialog('close');

    var count = 5;

    var fid = $('#txtOverviewSearch').val();

    $('#archartContainer,#linechartContainer,#barchartContainer,#linechartContainer1,#tblhighlight,#PiechartContainer,#divfinancialsummaryKPI,#spanGridTitle').empty();
    $.get('../reportdata.aspx?sp=Dashboard0&fid=' + fid, function (data) {
        // $.post('../reportdata.aspx', { 'sp': 'Dashboard0', 'fid': fid }, function (data) {
        var data = JSON.parse(data);
        if (!data.hasOwnProperty('error')) {
            getDailyhighlight(data.Table, data.Table1, data.Table2);
        }
        else {
            if (data.error[0].error == "error")
                shownodata();
            else
                document.write(data.error[0].error);
        }

        count--;
        if (count == 0)
            HideLoading();

        data = null;

    });

    $.get('../reportdata.aspx?sp=Dashboard1&dtype=1&fid=' + fid, function (data) {
        //$.post('../reportdata.aspx', { 'sp': 'Dashboard1', 'fid': fid }, function (data) {
        var data = JSON.parse(data);
        getLineChart('archartContainer', data.Table, data.series, true, data.Table1, 'spline', false, 'currency', data.Table2);
        count--;
        if (count == 0)
            HideLoading();
        data = null;
    });

    //$.post('../reportdata.aspx', { 'sp': 'Dashboard2', 'fid': fid }, function (data) {
    $.get('../reportdata.aspx?sp=Dashboard2&fid=' + fid, function (data) {
        var data = JSON.parse(data);
        var legend = false;
        if (data.Table.length > 10) {
            legend = false
            $('#divpieGRM').show();
        }
        else {
            legend = true
            $('#divpieGRM').hide();
        }

        getpiechart('PiechartContainer', data.Table, data.Table3, legend, false, 'currency', true);
        getpiechart('divChartPopup', data.Table, data.Table3, true, false, 'currency', true);


        count--;
        if (count == 0)
            HideLoading();
        data = null;
    });

    //$.post('../reportdata.aspx', { 'sp': 'Dashboard3', 'fid': fid }, function (data) {
    $.get('../reportdata.aspx?sp=Dashboard3&fid=' + fid, function (data) {
        var data = JSON.parse(data);
        var legend = false;
        if (data.Table.length > 10) {
            legend = false
            $('#divpieDif').show();
        }
        else {
            legend = true
            $('#divpieDif').hide();
        }

        getpiechart('linechartContainer', data.Table, data.Table3, legend, false, 'number', true);

        getpiechart('divChartPopup', data.Table, data.Table3, true, false, 'currency', true);
        count--;
        if (count == 0)
            HideLoading();
        data = null;
    });

    //$.post('../reportdata.aspx', { 'sp': 'Dashboard5', 'fid': fid }, function (data) {
    $.get('../reportdata.aspx?sp=Dashboard5&fid=' + fid, function (data) {
        var data = JSON.parse(data);
        getLineChart('barchartContainer', data.Table, data.series, true, data.Table1, 'spline', false, 'number', data.Table2);
        count--;
        if (count == 0)
            HideLoading();
        data = null;
    });
}


function showbarinpopup(divid) {

    $('#divChartPopup').html($('#' + divid).html());

    var pieChart = $('#divChartPopup').dxPieChart('instance');
    pieChart.option({
        legend: { visible: true }
    });
    // pieChart.render();
    var option = pieChart.option();
    $('#divChartPopup').dialog({ title: option.title.text });

    $('#divChartPopup').dialog('open');
    $('.panel-header').show();
}

function getinternalCahrtdata(id) {

    try {
        setoffset();

        Showloading();


        $('#divinnerchartContainer').css('width', '98%');
        $('#divinnerchartContainer1').css('width', '98%');
        $('#divsummary').height('20px');
        $('#divsummary1').height('20px');
        $('#divinnerchartContainer,#divinnerchartContainer1,#divinnerchartContainer2,#divfinancialsummary, #divsummary, #divsummary1, #divfinancialsummaryKPI,#spanGridTitle, #divLocationCharts,#divLocationTitle, #divLocationGrid').empty();
        var fid = $('#txtOverviewSearch').val();
        var branch = -1;  //$('#ddlBranch').val() == 1 ? -1 : $('#ddlBranch').val().trim();
        var carrier = "";
        var arr = $('#txtCarrierSearch').val().split(',')

        if (arr[0] != "1" && ($('#txtcarrier').val() != "ALL CODES" && $('#txtcarrier').val() != "ALL PAYER GROUPS" && $('#txtcarrier').val() != "ALL CARRIERS" && $('#txtcarrier').val() != "ALL DEFICIENCIES"))
            carrier = $('#txtCarrierSearch').val();
        var fromdate = '';
        var todate = '';
        var predata = $('#txtSelectedSearhfield').val();
        var daterange = $('#txtDateRange').val().split('-');
        var date = $('#txtDate').val();
        //        if (id == 20 ) {
        //            if (($('#txtfrom').val() != "FROM DATE" && $('#txtfrom').val() != "") && ($('#txtto').val() != "TO DATE" && $('#txtto').val() != "")) {
        //                fromdate = $('#txtfrom').val();
        //                todate = $('#txtto').val();
        //            }
        //        }
        if (id == 20) {
            if ($('#txtMonth').val() != 'SELECT MONTH') {
                var monthdate = $('#txtMonth').val().split('-');
                if (monthdate.length > 1) {
                    fromdate = monthdate[0];
                    todate = monthdate[1];
                }
            }
        }
        else if (id == 22) {
            if ($('#ddlYear').val() > 0) {
                fromdate = '01/01/' + $('#ddlYear').val();
            }
        }
        else if (id == 6 || id == 15 || id == 10 || id == 28) {
            fromdate = "";
            if (date != 'SELECT DATE' && date != '')
                todate = date.trim();
        }
        else if (daterange.length > 0 && daterange[0] != "SELECT PERIOD" && daterange[0] != "") {
            if (daterange.length > 1) {
                fromdate = daterange[0].trim();
                todate = daterange[1].trim();
            }
            else {
                fromdate = daterange[0].trim();
                todate = daterange[0].trim();
            }
        }

        var deficiencycode = "";
        if (arr[0] != "1" && ($('#txtcarrier').val() != "ALL DEFICIENCIES"))
            deficiencycode = $('#txtCarrierSearch').val();
        var providercode = "";
        providercode = $('#txtProviderSearch').val();

        if (id == 30 || id == 27 || id == 31) {
            if ($('#ddlBranch').val() != null && $('#ddlBranch').val() != 1)
                predata = $('#ddlBranch').val();
        }
        if (id == 18) {     // Comment Out For Some Time till Data is Incorrect (Dana Evnas's Email )

            // if (window.location.href.indexOf('?FS') == -1)  // special query string for Testing it
            //     ShowUnderConstruction();
            getReportName("", -1);
            getfinancialsummary(fid, id, branch, carrier, fromdate, todate, predata);
        }
        else if (id == 23) {
            getReportName("", -1);
            getANSIDentalLogbyLine(fid, id, branch, carrier, fromdate, todate, predata, deficiencycode, providercode);
        }
        else if (id == 25) {
            getReportName("", -1);
            getTransactionSummarybyType(fid, id, branch, carrier, fromdate, todate, predata, deficiencycode, providercode);
        }
        else if (id == 31) {
            getReportName("", -1);
            getCPTCodesByState(fid, id, branch, carrier, fromdate, todate, predata, providercode);
        }

        //        else if (id == 27) {
        //            getReportName("", -1);
        //            getJBKPIReport(id, branch, carrier, fromdate, todate, predata, deficiencycode, providercode);
        //        }
        //        else if (id == 29) {
        //            getReportName("", -1);
        //            getInsuranceAgingReportByPayer(id, branch, carrier, fromdate, todate, predata, deficiencycode, providercode);
        //        }
        else {
            $.get('../reportdata.aspx?sp=' + 'getGraphdata' + '&fid=' + fid + '&id=' + id + '&branch=' + branch + '&carrier=' + carrier + '&FromDate=' + fromdate + '&toDate=' + todate + '&predata=' + predata + '&deficiencycode=' + deficiencycode + '&providercode=' + providercode, function (data) {
                //$.post('../reportdata.aspx', { 'sp': 'getGraphdata', 'fid': fid, 'id': id, 'branch': branch, 'carrier': carrier, 'FromDate': fromdate, 'toDate': todate, 'predata': predata, 'deficiencycode': deficiencycode, 'providercode': providercode }, function (data) {
                try {
                    var data = JSON.parse(data);

                    if (data['speed'] == undefined) {
                        if (data['error'] == undefined) {

                            //                    if (data.Table.hasOwnProperty('id')) {
                            if (id == 4 || id == 5) {
                                getpiechart('divinnerchartContainer', data.Table, data.Table3, true, false, 'currency', false);
                            }
                            else if (id == 6) {
                                if (data.Table1[0].ChartType == "Bar") {

                                    getLineChart('divinnerchartContainer', data.Table, data.series, true, data.Table1, 'bar', true, 'currency', data.Table2, 'divsummary');
                                }
                                else
                                    getpiechart('divinnerchartContainer', data.Table, data.Table3, true, true, 'currency', false);
                            }
                            else if (id == 21)
                                getpiechart('divinnerchartContainer', data.Table, data.Table3, true, true, 'number', false);

                            else if (id == 10)
                                getLineChart('divinnerchartContainer', data.Table, data.series, true, data.Table1, 'bar', false, 'currency', data.Table2, 'divsummary');
                            else if (id == 22) {
                                GetAnnualStaticArgumentCount(data.Table);   ///FOR CROLLING
                                getLineChart('divinnerchartContainer', data.Table, data.series1, true, data.Table1, 'bar', false, 'currency', data.Table2, 'divsummary'); //stackedBar
                                getLineChart('divinnerchartContainer1', data.Table3, data.series2, true, data.Table4, 'bar', false, 'currency', data.Table5, 'divsummary1');
                            }
                            else if (id == 24) {
                                getLineChart('divinnerchartContainer', data.Table, data.series1, true, data.Table1, 'spline', false, 'currency', data.Table2, 'divsummary');
                                getLineChart('divinnerchartContainer1', data.Table3, data.series2, true, data.Table4, 'spline', false, 'currency', data.Table5, 'divsummary1');
                            }
                            else if (id == 27) {
                                getReportName("", -1);
                                getJBKPIReport(data.Table, 'divfinancialsummary', id + '_1', $('#ddlBranch option:selected').text(), '#99CC20');
                                // getJBKPIReport(data.Table1, 'divfinancialsummaryKPI', id + '_2', 'Fully Adjudicated Runs', '#9966CC');

                            }
                            else if (id == 28 || id == 26 || id == 20) {
                                getLineChart('divinnerchartContainer', data.Table, data.series, true, data.Table1, 'spline', true, 'currency', data.Table2, 'divsummary');
                                if (id == 20)
                                    CreateAnalysisByTransactionDateTableForCrolling(data.Table); ///FOR CROLLING
                            }
                            else if (id == 30) {
                                noshowexport();
                                getReportName($('#ddlBranch option:selected').text(), -1);
                                getLoactionCharts(data);
                            }
                            else
                                getLineChart('divinnerchartContainer', data.Table, data.series, true, data.Table1, 'spline', false, 'currency', data.Table2, 'divsummary');
                            data = null;
                        }
                        else
                            if (data.error[0].error == "error")
                                shownodata();
                            else
                                document.write(data.error[0].error);
                    }
                    else
                        shownoNetwork();
                }
                catch (e) {
                    HideLoading();
                    shownodata();

                }

            }).done(function () {
                HideLoading();
                if (!$('.x-grid3-scroller').hasClass('nano'))
                    $('.x-grid3-scroller').addClass('nano')
                if (!iPadAgent && !iPhoneAgent && !AndroidAgent) {
                    $('.nano').perfectScrollbar().perfectScrollbar('destroy');
                    $('.nano').perfectScrollbar();
                    setTimeout("$('.nano').perfectScrollbar('update');", 500);
                }
                else {
                    $('.nano').css('overflow', 'auto');

                    $('.nano').perfectScrollbar('destroy');
                }
            }).fail(function () {
                HideLoading();
                //shownodata();
                shownoNetwork();
            }).always(function () {
                HideLoading();
            });
        }
    }
    catch (e) {
        HideLoading();
        shownodata();

    }


}

//////////////////Daily Highlight Table/////////////////////////////////////////////////////////////////////////////////////
function getDailyhighlight(data, display, ododata) {

    var heading = "";
    //for (var key in display) {
    heading = display[0].Reportheading;
    //  break;
    //}
    var html = "<table style='width:100%; '><tr><td align='center' class='ReportSummaryHeader higlightTitle'>"
    + heading + "</td></tr>";

    ///Highlight boxes ///
    html += "<tr><td>";
    for (var key in data) {
        if (data[key].currentvalue != undefined) {
            var table = "<div class='dxdvItem_DevEx highlightbox'>";
            table += "<table style='width:100%; backgroun-color:#ccc;' cellpadding='3' cellspacing='0' align='center'><tr><td align='left' class='tdsummeryhead' colspan='2'>";
            table += "<span class='tdfont' style='color:#666666; font-size:13px; font-weight:bold;'>" + data[key].name + "</span></td></tr>";
            table += "<tr><td align='right' class='tdsummerytext' colspan='2' style='padding-top: 25px; padding-right:5px'><span class='dashhighlighttext'>" + data[key].currentvalue;
            table += "</span></td></tr><tr><td>&nbsp;</td></tr></table>";
            // table += "<tr><td align='left' class='tdsummerytext'><span style='color:#477CC0; font-size:13px; font-weight:bold;'>MTD</span></td>";
            //  table += "<td align='right' valign='bottom' class='tdsummerytext'><span style='color:#477CC0; font-size:13px; font-weight:bold;'>" + data[key].yesterdayvalue+"</span></td>";
            //table += "</tr></table></td><td style='width:4px;'></td>";
            table += "</div>";
            html += table;
        }
    }

    html += "</td></tr></table>";

    $("#tblhighlight").html(html);



    var html1 = '<table style="width:100%;"><tr><td align="center"  class="higlightTitle"> AVERAGE HIGHLIGHTS</td></tr><tr><td>';
    for (var key in ododata) {
        if (ododata[key].minvalue != undefined)
            html1 += "<div id='divodometer_" + key + "' style='height:150px;' class='highlightbox'></div>";
    }

    html1 += '</td></tr></table>';

    $("#divGaugedata").html(html1);

    for (var key in ododata) {
        if (ododata[key].minvalue != undefined)
            getGauge('divodometer_' + key, ododata[key].minvalue, ododata[key].maxvalue, ododata[key].value, ododata[key].Targetvalue, ododata[key].name);
    }


    if ($('#ContentHolder_hdnclientfolder').val() == "jefbar") {
        $('.dashhighlighttext').css('color', '#940006');

    }
    if (!iPadAgent && !iPhoneAgent && !AndroidAgent) {
        $('.nano').perfectScrollbar().perfectScrollbar('destroy');
        $('.nano').perfectScrollbar();


    }
    else {
        $('.nano').css('overflow', 'auto');

        $('.nano').perfectScrollbar('destroy');
    }
}


/////////////// Gauge Control /////////////////
function getGauge(contrl, min, max, value, target, title) {

    try {
        var max = Math.round(max);
        $('#' + contrl).empty();
        if (value != undefined || value != null) {
            $('#' + contrl).dxCircularGauge({
                redrawOnResize: true,
                geometry: {
                    startAngle: 180, endAngle: 0
                },
                scale: {
                    startValue: min,
                    endValue: max,
                    majorTick: {
                        tickInterval: 20
                    }
                },
                rangeContainer: {
                    palette: '',
                    ranges: [
                  { startValue: min, endValue: target, color: 'green' },
			      { startValue: target, endValue: max, color: 'red' }
                    ]
                },

                valueIndicator: {
                    type: 'triangleNeedle',
                    color: '#9B870C'
                },
                title: {
                    text: title,
                    font: { color: '#666666', size: 13, family: 'trebuchet ms', weight: 900 }
                },
                animation: {
                    enabled: true
                },
                value: value,
                needles: [{ value: value}],

                markers: [{ value: value}]

            });
        }
        else {
            var EmptyDiv = '<div style="font-size: 13px; color: #666;  font-family: \'trebuchet ms\'; font-weight: 900; text-align: center; width: 100%">' + title + '</div>';
            EmptyDiv += '<div class="higlightTitle" style="width: 100%; text-align: center;margin-top: 20%">Not Available</div>';
            $('#' + contrl).html(EmptyDiv);
        }
    }
    catch (e) {
        var EmptyDiv = '<div style="font-size: 13px; color: #666;  font-family: \'trebuchet ms\'; font-weight: 900; text-align: center; width: 100%">' + title + '</div>';
        EmptyDiv += '<div class="higlightTitle" style="width: 100%; text-align: center;margin-top: 20%">Not Available</div>';
        $('#' + contrl).html(EmptyDiv);
    }
}

var windowWidth = $(window).width();
var windowHeight = $(window).height();
//////////////////Line Chart Control///////////////////////////////////////////////////////////////////////////////////////
function getLineChart(contrl, data, series, legend, dispaly, type, isClick, tooltiptype, summay, summarycontrl) {
    try {

        if (data.length > 0) {
            var myPalette = new Array();
            var ispalette = false;
            var palettename = '';
            //            if (colorarray.length > 0) {
            //                for (var key in colorarray) {
            //                    if (colorarray[key].colourcode != null && colorarray[key].colourcode != undefined) {
            //                        myPalette.push(colorarray[key].colourcode);
            //                        ispalette = false;
            //                    }

            //                }
            //            }



            var titlefont = 15;
            var tooltipfont = 15;
            var vertalign = 'top';
            var horizalign = 'right';
            var window = windowWidth;
            if ((window < 600) && (window > 400)) {
                titlefont = 13;
                tooltipfont = 13;
                vertalign = 'bottom';
                horizalign = 'center';
            }
            else if (window < 400) {
                titlefont = 12;
                tooltipfont = 12;
                vertalign = 'bottom';
                horizalign = 'center'
            }
            else {
                titlefont = 15;
                tooltipfont = 15;
                vertalign = 'top';
                horizalign = 'right';
            }




            if (ispalette) {
                DevExpress.viz.core.registerPalette('mySuperPalette', myPalette);
                palettename = 'mySuperPalette';
            }

            var legendLength = series.length;
            var height = (windowHeight * 6 / 10);
            if (windowWidth < 600) {
                $('#' + contrl).height(height + 20 * legendLength);
            }
            height = $('#' + contrl).height();

            $("#" + contrl).dxChart({
                dataSource: data,
                palette: palettename,
                redrawOnResize: true,
                commonSeriesSettings: {
                    argumentField: "month",
                    type: type,
                    hoverMode: "includePoints",
                    point: {
                        //                        hoverMode: "allArgumentPoints",
                        //                        selectionMode: "allArgumentPoints",
                        symbol: "polygon"
                    }

                },
                animation: {
                    enabled: true
                },
                series: series,
                argumentAxis: {
                    title: {
                        text: dispaly[0].Bottomheading,
                        font: {
                            color: '#868686', size: titlefont, family: 'trebuchet ms', weight: 900
                        }
                    },
                    format: 'largeNumber'
                },

                commonAxisSettings: {
                    label: {
                        overlappingBehavior: { mode: 'stagger', staggeringSpacing: 5 }
                    },
                    visible: true
                },
                tooltip: {
                    enabled: true,
                    point: {
                        hoverMode: "allArgumentPoints",
                        selectionMode: "allArgumentPoints"

                    },
                    format: tooltiptype,
                    shared: true,
                    customizeText: function () {
                        return this.argumentText + ' : ' + this.valueText;
                    },
                    font: { color: '#ffffff', size: tooltipfont, weight: 700 }
                },
                title: { text: dispaly[0].Reportheading, font: { color: '#868686', size: titlefont, family: 'trebuchet ms', weight: 900} },
                legend: {

                    verticalAlignment: vertalign.toString(),
                    horizontalAlignment: horizalign.toString(),

                    visible: legend,
                    rowItemSpacing: 1,
                    columnItemSpacing: 2,
                    //  rowCount: 14,
                    margin: 5,
                    paddingTopBottom: 5,
                    font: { size: tooltipfont - 2 },
                    markerSize: (tooltipfont - 4),
                    customizeText: function () {
                        var legendName = this.seriesName;
                        if (legendName.length > 20)
                            return legendName.substring(0, 20) + "....";
                        else
                            return legendName;
                    }

                },
                size: {
                    height: height
                },
                valueAxis: [{

                    position: 'left',
                    label: {
                        format: tooltiptype
                    },
                    font: { color: '#3D3D47' },
                    title: {
                        text: dispaly[0].leftheading,
                        font: { color: '#868686', size: titlefont, family: 'trebuchet ms', weight: 900 }

                    }
                }],
                seriesClick: function (seriesclick) {
                    if (isClick) {

                        var fromdate = '';
                        var todate = '';

                        var id = $('#hdnreportid').val();
                        var carrier = "";
                        if (id == "20") {

                            if ($('#txtMonth').val() != 'SELECT MONTH') {
                                var monthdate = $('#txtMonth').val().split('-');
                                if (monthdate.length > 1) {
                                    fromdate = monthdate[0];
                                    todate = monthdate[1];
                                }
                            }

                           // ShowAnalysisByTransactionDateDrillDownReport(id, carrier, fromdate, todate, seriesclick.name, -1);
                        }
                        else {
                            var daterange = $('#txtDateRange').val().split('-');
                            if (daterange.length > 1) {
                                fromdate = daterange[0].trim();
                                todate = daterange[1].trim();
                            }
                            showpayerpopup(seriesclick.name, fromdate, todate, -1);
                        }

                    }

                },
                pointClick: function (point) {
                    if (isClick) {


                        var fromdate = '';
                        var todate = '';

                        var carrier = "";
                        var id = $('#hdnreportid').val();
                        if (id == "20") {
                            if ($('#txtMonth').val() != 'SELECT MONTH') {
                                var monthdate = $('#txtMonth').val().split('-');
                                if (monthdate.length > 1) {
                                    fromdate = monthdate[0];
                                    todate = monthdate[1];
                                }
                            }

                            ShowAnalysisByTransactionDateDrillDownReport(id, carrier, fromdate, todate, point.series.name, point.argument);
                        }
                        else {
                            var daterange = $('#txtDateRange').val().split('-');
                            if (daterange.length > 1) {
                                fromdate = daterange[0].trim();
                                todate = daterange[1].trim();
                            }
                            point.select();
                        }
                    }
                    else
                        point.select();
                },
                placeholderSize: 5,
                done: function () {

                }
            });


            // $('#divChartTitle').html(dispaly[0].Reportheading)


            var summary = summay[0].Total
            if (summary != undefined) {
                var wdt = summary.split('<h3>');

                $('#' + summarycontrl).html('<div style="float:right; font-size:' + titlefont + 'px; font-weight:bold; font-family:trebuchet ms; color:#868686; text-align:right;">' + summary + '</div>');
                $('#' + summarycontrl).height((wdt.length) * 25);
            }

            var chart = $('#' + contrl).dxChart('instance');
            chart.option({
                size: {
                    height: $('#' + contrl).height(),
                    width: $('#' + contrl).width()
                }
            });
        }
        else {
            $("#" + contrl).empty();
            // $("#" + contrl).append('<div style="margin:0 auto; width:100%; text-align:center;"> <img src="../images/DataNotFound2.jpg" alt="No Data Available" style="vertical-align: middle; height:100%"/></div>')
            var marginTop = $("#" + contrl).height() / 2 - 20;
            $("#" + contrl).append('<div class="higlightTitle" style="margin:0 auto; width:100%; text-align:center; margin-top:' + marginTop + 'px">Not Available</div>')
        }




    }
    catch (e) {

        shownodata();
    }
    finally {
        data = null;
        dispaly = null;
        series = null;
    }



}

//////////////Pie Chart Control////////////////////////////////////////////////////////////////////
function getpiechart(contrl, data, dispaly, legend, isClick, tooltipType, isWrape) {
    try {

        if (data.length > 0) {
            var myPalette = new Array();
            var ispalette = true;
            var palettename = '';

            for (var key in data) {
                if (data[key].value != undefined) {
                    if (contrl == "divinnerchartContainer") {
                        if (key == 0)
                            $('#hdnCrollingValue').val(data[key].value)
                        else
                            $('#hdnCrollingValue').val($('#hdnCrollingValue').val() + ',' + data[key].value);
                    }
                    else if (contrl == "linechartContainer") {
                        if (key == 0)
                            $('#hdnOvervirewCrollingValue').val(data[key].value)
                        else
                            $('#hdnOvervirewCrollingValue').val($('#hdnOvervirewCrollingValue').val() + ',' + data[key].value);
                    }
                }
            }
            for (var key in data) {

                if (data[key].colourcode != "")
                    myPalette.push(data[key].colourcode);
                else {
                    ispalette = false;
                    break;
                }
            }

            var titlefont = 15;
            var tooltipfont = 15;
            var vertalign = 'top';
            var horizalign = 'right';
            var window = windowWidth;
            if ((window < 600) && (window > 400)) {
                titlefont = 13;
                tooltipfont = 13;
                vertalign = 'bottom';
                horizalign = 'center';
            }
            else if (window < 400) {
                titlefont = 12;
                tooltipfont = 12;
                vertalign = 'bottom';
                horizalign = 'center'
            }
            else {
                titlefont = 15;
                tooltipfont = 15;
                vertalign = 'top';
                horizalign = 'right';
            }

            //            if ($('#hdnreportid').val() == '30') {
            //                titlefont = 13;
            //                tooltipfont = 13;
            //                vertalign = 'bottom';
            //                horizalign = 'center'
            //            }

            if (ispalette) {
                DevExpress.viz.core.registerPalette('mySuperPalette', myPalette);
                palettename = 'mySuperPalette';
            }


            var dataSource = data;
            var chart = $("#" + contrl).dxPieChart({
                dataSource: dataSource,
                palette: palettename,
                redrawOnResize: true,
                margin: {
                    right: 5,
                    left: 5,
                    bottom: 10
                },
                title: { text: dispaly[0].Reportheading, font: { color: '#868686', size: titlefont, family: 'trebuchet ms', weight: 900} },

                tooltip: {
                    enabled: true,
                    percentPrecision: 2,
                    arrowLength: 20,

                    font: { color: '#ffffff', size: tooltipfont, weight: 700 },
                    customizeText: function () {
                        var text = this.argumentText;
                        if (isWrape) {
                            if (text.length > 30)
                                text = text.substring(0, 30) + "...";
                        }

                        if (tooltipType == 'currency')
                            return text + ' : ' + Math.abs(this.valueText).dollarFormat() + ' : ' + this.percentText;
                        else
                            return text + ' : ' + this.valueText + ' : ' + this.percentText;
                    }
                },
                legend: {
                    horizontalAlignment: horizalign.toString(),
                    verticalAlignment: vertalign.toString(),
                    font: { size: tooltipfont - 1 },
                    rowItemSpacing: 0,
                    columnItemSpacing: 1,
                    visible: legend,
                    margin: 10,
                    font: { size: tooltipfont },
                    markerSize: (tooltipfont - 4),
                    customizeText: function () {
                        if (isWrape) {
                            var legendName = this.seriesName;
                            if (legendName.length > 20)
                                return legendName.substring(0, 20) + "....";
                            else
                                return legendName;
                        }
                        else
                            return this.seriesName;
                    }

                },
                series: [{
                    //type: 'doughnut',
                    argumentField: 'name',
                    valueField: 'value',
                    label: {
                        visible: false,
                        font: { color: '#ffffff' },
                        connector: {
                            visible: true,
                            width: 0.5
                        },
                        customizeText: function () {
                            return this.argumentText + ' : ' + this.percentText;
                        }
            ,
                        position: "columns"
                    }
                }],
                pointClick: function (point) {
                    if (isClick) {

                        //alert(point);
                        // window.open("AgedTrialBalanceSummary.aspx?cb=" + point.argument.replace('&', '%26') + "&fd=" + frmdate + "&td=" + frmdate);
                        ShowDeficiencyDetailbyCode(point.argument);
                    }
                    else
                        point.select();
                },
                animation: {
                    enabled: false
                }
            });
        }
        else {
            $("#" + contrl).empty();
            // $("#" + contrl).append(' <img src="../images/DataNotFound2.jpg" alt="No Data Available" style="vertical-align: middle;  height:100%"/>')
            var marginTop = $("#" + contrl).height() / 2 - 20;
            $("#" + contrl).append('<div class="higlightTitle" style="margin:0 auto; width:100%; text-align:center; margin-top:' + marginTop + 'px">Not Available</div>')
        }
    }
    catch (e) {

        shownodata();
    }
    finally {
        data = null;
        dispaly = null;
    }
}


////////////////// Click function of Chart  //////////////////////////////////////////////////////////

function ShowDeficiencyDetailbyCode(deficiencycode) {

    if ($(window).width() < 650) {
        $('#dgFilter').hide();
        $('#divddlFilter').hide();
    }
    $.get('../reportdata.aspx?con=check', function (data) {

        var flag = true;
        var data = JSON.parse(data);
        var speed = data.speed[0].speed;
        if (speed == 0) {
            $('#divnoConnect').show();
            CreateTimer();
            flag = false;
        }
        else if (speed < 50) {
            $('#divslow').show();
            flag = true;
        }

        if (flag) {

            var fid = $('#txtOverviewSearch').val();

            if (fid.split(',').length == 1 || !isAllFacility) {
                $('#divfinancialsummary').empty();
                $('#divfinancialsummaryCover').show();
                $('#divinnerchartContainerCover').hide();
                $('#divGridPrint').show();
                $('#divChartPrint').hide();
                $('#divGraphToText').show();

                deftype = "Detail";

                $('#btnGraph').removeClass('activegraph').addClass('graph');
                $('#btnDetail').removeClass('detail').addClass('activedetail')
                $('#btnSummary').removeClass('activesummary').addClass('summary');

                ExportAll();
                if ((navigator.appName == "Microsoft Internet Explorer") || (navigator.userAgent.indexOf("rv:11.0") > -1) || iPhoneAgent || iPadAgent || AndroidAgent) {
                    noshowexport();
                }

                var fromdate = '';
                var todate = '';
                var daterange = $('#txtDateRange').val().split('-');
                if (daterange.length > 1) {
                    fromdate = daterange[0].trim();
                    todate = daterange[1].trim();
                }
                var providercode = "";
                providercode = $('#txtProviderSearch').val();
                var carrier = "";
                if ($('#txtcarrier').val() != "ALL DEFICIENCIES")
                    carrier = $('#txtCarrierSearch').val();
                Showloading();
                getReportName("", -1);

                getDeficiencyRunsDetail(fid, id, fromdate, todate, deficiencycode, providercode, carrier);
            }
            else {
                BindDialogBox();
                $("#divmsg").dialog('open');
                $('#msgSpan').text('Please select a single facility to view the detailed report.');
            }
        }
    }).fail(function () {
        $('#divnoConnect').show();
        if ($('#divnodata').is(':visible'))
            shownoNetwork();
        else
            CreateTimer();
    });
}

/////////////////Reporting Menu///////////////////////////////////////////////////////////////////////////
function getreportmenu(data) {
    var imageurl = "../images/reportsicon/" + $('#ContentHolder_hdnclientfolder').val();
    var imgoverview = "../images/reportsicon/" + $('#ContentHolder_hdnclientfolder').val() + "/overview_active.png";



    var table = '<div class="grayrctext" style="text-align:left;"><ul>';

    var html = "";
    //html += "<div class='grayrctext1'><p class='active report'><a href='javascript:;' onclick='return changereportchart(0);'>"
    //html += "<img id='reportdashboardimg' alt='' src='" + imgoverview + "'><br></a><a href='javascript:;' class='report' onclick='changereportchart(0);'> OVERVIEW";
    //html += "</a></p><div class='graysp'>   </div></div><div class='grayrctext'>";


    //table += '<li class="active"><a onclick="return changereportchart(0);" href="javascript:;" > <table><tr><td style="width:20%">';
    //table += '<img id="0" style="height:50%; width:50%" src="' + imgoverview + '"/> </td>';
    //table += '<td align="left">OVERVIEW';
    //table += '</td></tr></table></a></li>';
    //table += '<li><div class="graysp" style="width:96%"></div></li>'

    for (var key in data) {
        if (data[key].ID != undefined) {

            //            /// For Mobile Menu
            if (data[key].ID == 0) {
                table += '<li class="active"><a onclick="return changereportchart(0);" href="javascript:;" > <table><tr><td style="width:20%">';
                table += '<img id="0" style="height:50%; width:50%" src="' + imgoverview + '"/> </td>';
                table += '<td align="left">OVERVIEW';
                table += '</td></tr></table></a></li>';
                table += '<li><div class="graysp" style="width:96%"></div></li>'
            }
            else {
                table += '<li><a  onclick="return changereportchart(' + data[key].ID + ');" href="javascript:;" > <table><tr><td style="width:20%">';
                table += '<img id="' + data[key].ID + '" style="height:50%; width:50%" src="' + imageurl + '/' + data[key].reportname.trim() + '.png"/> </td>';
                table += '<td align="left">' + data[key].reportname; ;
                table += '</td></tr></table></a></li>';
                table += '<li><div class="graysp" style="width:96%"></div></li>'
            }

            // For Web Menu
            if (data[key].ID != 0 && data[key].ID != undefined) {
 		if (key == 0)
                    html += "<div class='grayrctext'>";

                html += "<p class='report'><a href='javascript:;' onclick='return changereportchart(" + data[key].ID + ");'>"
                html += "<img id='" + data[key].ID + "' alt='' src='" + imageurl + "/" + data[key].reportname.trim() + ".png' />";
                html += "<br></a><a href='javascript:;' class='report reportmenu' onclick=' changereportchart(" + data[key].ID + ");' style='margin:5px'>" + data[key].reportname;
                html += "</a></p><div class='graysp'>   </div>";
            }
            else {
                html += "<div class='grayrctext1'><p class='active report'><a href='javascript:;' onclick='return changereportchart(0);'>"
                html += "<img id='reportdashboardimg' alt='' src='" + imgoverview + "'><br></a><a href='javascript:;' class='report reportmenu' onclick='changereportchart(0);'> OVERVIEW";
                html += "</a></p><div class='graysp'>   </div></div><div class='grayrctext'>";
            }

	    
        }
    }


    table += '</ul></div>'

    $('#divselectmenu').html(table);

    $('.reportcenter').off('click').on('click', function () {
        if ($('#divmenuscroll').is(':hidden')) {
            $('#divselectmenu').toggle("slow");
            var position = $(this).position();
            $('#divselectmenu').css('top', position.top + 30);
            $('#divselectmenu').css('left', position.left + 5);
        }
    });
    // }
    //   else {

    html += "</div>";

    $("#divReportmenu").html(html);
    if (!iPadAgent && !iPhoneAgent && !AndroidAgent) {
        $('.nano').perfectScrollbar().perfectScrollbar('destroy');
        $('.nano').perfectScrollbar();
        setTimeout(" $('.nano').perfectScrollbar('update');", 500)
    }
    else {
        $('.nano').css('overflow', 'auto');

        $('.nano').perfectScrollbar('destroy');
    }


}



function SetPageDetailsForAnalytics(menu_name) {
    $.post("../Google_Analytics.aspx", { Servicetype: 'Page_View_Details' }, function (data) {
        var data = JSON.parse(data);

        var report_name = 'No Report';
        if (menu_name != '' && menu_name != undefined && menu_name != null)
            report_name = menu_name;
        var all_details = 'Uname=' + data[0].Uname + ',CAutoID=' + data[0].CAutoID + ',ClientMasterID=' + data[0].ClientMasterID + ',URL=' + window.location.href.toString() + ',SM_USER=' + data[0].SM_USER + ',FacilityName=' + data[0].facilityName + ',ReportName=' + menu_name;
        // ga('send', 'event', 'Page View', 'Page View Details', all_details);

        ga('send', 'event', 'Page View', 'Page View Details', all_details, { 'dimension1': data[0].Uname, 'dimension2': data[0].CAutoID, 'dimension3': data[0].ClientMasterID, 'dimension4': window.location.href.toString(), 'dimension5': data[0].SM_USER, 'dimension6': data[0].facilityName, 'dimension7': report_name });
        // ga('send', 'event', 'Page View', 'Page View Details', all_details, { 'dimension1': '1', 'dimension2': '2', 'dimension3': '3', 'dimension4': '4', 'dimension5': '5', 'dimension6': '6', 'dimension7': '7' });

    });
}


//////////////////Report Click Event ///////////////////////////////////////////////////

function changereportchart(id) {
 var menu_name = $('#' + id).parent().parent().children()[1].innerHTML;
    SetPageDetailsForAnalytics(menu_name);

    $('#Nodatatext').html('');
    if ($(window).width() < 650) {
        $('#dgFilter').hide();
        $('#divddlFilter').hide();
    }

   

    if (!$('#divselectmenu').is(':hidden'))
        $('#divselectmenu').toggle("slow");
   
    $('#divFilterSection').show();
    $('#divComapareforSelection').hide();
    $('#divtreeCarrier').hide();
    $('#txtSelectedSearhfield, #txtCarrierSearch, #txtProviderSearch').val('');
   

    if (id > 0) {
        if (ValidatecheckedFacilities(id)) {
            $('#hdnreportid').val(id);
            CheckComparebasedonvalue();
            ShowAllReportFilters();
            Showloading();
            bindBranch();
            comingsoon();
            $('#divNoNetwork').hide();
            $('.content').css('top', '0px');

            $('.ps-scrollbar-y-rail').css('top', '0px');
            $('.ps-scrollbar-y').css('top', '0px');

            if (id == "18") {
                $('#divreportsetting').show();
                $('#divreportsettingOnlyDashboard').hide();
                $('#divreportsettingEmpty').show();
                $('#divcarrierTreePanel').hide();
                $('#divProviderTreePanel').hide();
                $('#divddlBranch').hide();
                $('#divPayerFilter').hide();

                if (!isAllFacility)
                    $('#divGobtn').hide();
                $('#divreportcentermain').hide();
                $('#divLocationChartContainer').hide();
                $('#divOverViewPanel').show();
                $('#divPracticeOverview').hide();
            }

            else {
                $('#divreportsetting').hide();
                $('#divreportsettingOnlyDashboard').show();
                $('#divreportsettingEmpty').hide();
                $('#divPracticeOverview').hide();
                $('#divcarrierTreePanel').hide();
                $('#divProviderTreePanel').hide();
                $('#divddlBranch').hide();
                $('#divPayerFilter').hide();
                $('#imgPdf').show();
                if (!isAllFacility)
                    $('#divGobtn').hide();
                $('#divLocationChartContainer').hide();
                $('#divOverViewPanel').show();
            }
            $('#divFilterSection').show();
            $('#divPracticeOverview').hide();
            $('#divOverViewPanel').show();
            $('#divinnerchartContainerCover1').hide();
            $('#divinnerchartContainer2').hide();
            $('#divReportDashboard').hide();
            $('#divreportcentermain').show();
            $('#Divchart').show();
            $('#divnodata').hide();
            $('#divcomingsoon').hide();
            $('#divGraphToText').hide();
            $('#divComparePanel').hide();
            if (!isAllFacility)
                $('#divGobtn').hide();
            $('#divLocationChartContainer').hide();
            if (id == 18 || id == 25 || id == 23 || id == 27 || id == 29 || id == 31) {

                ExportAll();
                $('#divfinancialsummaryCover').show();
                $('#divinnerchartContainerCover1').hide();
                $('#divinnerchartContainer2').hide();
                $('#divinnerchartContainerCover').hide();
                $('#divGridPrint').show();
                $('#divChartPrint').hide();
                $('#divLocationChartContainer').hide();
                if (id == 27) {
                    $('#divddlBranch').show();
                    $('#divGobtn').show();
                }
                if (id == 31) {
                    $('#divddlBranch').show();
                    $('#divGobtn').show();
                    $('#divPayerFilter').hide();
                    $('#divcarrierTreePanel').hide();
                    $('#divComparePanel').hide();
                    $('#divProviderTreePanel').show();
                    $('#divreportsettingOnlyDashboard').hide();
                    $('#txtProvider').val('ALL LOCATIONS');
                    TreeURLProvider("getLocationsforDropDown", "", "");
                }
                if (id == 18)
                    $('#divreportcentermain').hide();
            }
            else {
                ExportChart(id);
                $('#divinnerchartContainerCover1').hide();
                $('#divinnerchartContainer2').hide();

                if (id == "2") {
                    $('#divddlBranch').show();
                    $('#divcarrierTreePanel').hide();
                    $('#divProviderTreePanel').hide();
                    $('#divComparePanel').hide();
                    $('#divPayerFilter').hide();
                    $('#divGobtn').show();
                    $('#divreportsettingOnlyDashboard').hide();

                }
                else if (id == "3" || id == "5" || id == "15" || id == "16" || id == "28" || id == "26" || id == "24" || id == "22" || id == "21" || id == "30") {
                    $('#divcarrierTreePanel').show();
                    $('#divddlBranch').hide();
                    $('#divPayerFilter').hide();
                    $('#divreportsettingOnlyDashboard').hide();
                    $('#txtCarrierSearch').val("");
                    $('#txtProviderSearch').val("");
                    $('#divProviderTreePanel').hide();
                    $('#divComparePanel').hide();
                    $('#divGobtn').show();


                    if (id == "21") {
                        $('#txtcarrier').val('ALL DEFICIENCIES');
                        TreeURLCarrier("getDeficiencies", "");
                        $('#txtProvider').val('ALL PROVIDERS');
                        TreeURLProvider("getProvider", "", "");
                        $('#divProviderTreePanel').show();
                        fordeficiencyPanel();
                        $('#divGraphToText').show();
                        deftype = "Graph";
                        $('#btnGraph').removeClass('graph').addClass('activegraph');
                        $('#btnDetail').removeClass('activedetail').addClass('detail')
                        $('#btnSummary').removeClass('activesummary').addClass('summary');
                    }
                    else if (id == "28" || id == "26") {
                        $('#txtcarrier').val('ALL PAYER GROUPS');
                        TreeURLCarrier("getPayerGroup", "");
                        if (id == "26")
                            $('#divComparePanel').show();
                    }
                    else if (id == "24" || id == 22) {
                        $('#divinnerchartContainerCover1').show();
                        if (id == 24) {
                            //TreeURLCarrier("getcodes", ""); // Comment for use new procedure (VK 19 june 2015) 
                            TreeURLCarrier("getcodes_OLAP", "");
                            $('#txtcarrier').val('ALL CODES');
                          //  $('#divComparePanel').show(); // Hide Compare dorpdown (VK 19 june 2015) 
                        }
                        else {
                            $('#divComparePanel').hide();
                            if (!isAllFacility)
                                $('#divGobtn').hide();
                            $('#divcarrierTreePanel').hide();
                            $('#divProviderTreePanel').hide();
                        }
                    }
                    else {
                        TreeURLCarrier("getCarrierfordropdown", "");
                        $('#txtcarrier').val('ALL CARRIERS');
                    }

                }
                $('#divfinancialsummaryCover').hide();
                $('#divinnerchartContainerCover').show();
                $('#divGridPrint').hide();
                $('#divChartPrint').show();

                if (id == '30') {
                    $('#divLocationGrid').hide();
                    $('#divLocationCharts').show();
                    $('#txtcarrier').val('ALL PAYER GROUPS');
                    TreeURLCarrier("getPayerGroup", "");
                    $('#divddlBranch').show();
                    $('#divfinancialsummaryCover').hide();
                    $('#divLocationChartContainer').show();
                    $('#divinnerchartContainerCover').hide();
                    locationType = "chart";
                }

            }
            if ((navigator.appName == "Microsoft Internet Explorer") || (navigator.userAgent.indexOf("rv:11.0") > -1) || iPadAgent || iPhoneAgent || AndroidAgent) {
                noshowexport();
            }
            if (id != 19) {
                //if (id != 18)
                if (id == 20) {

                    var daterange = $('#txtDateRange').val().split('-');
                    var daydiff = true;
                    if (daterange.length > 1) {
                        daydiff = getdaysdiff(daterange[0], daterange[1]);
                    }

                    if (daydiff)
                        getinternalCahrtdata(id);
                    else {

                        var width = windowWidth < 400 ? 250 : 300
                        $("#divmsg").dialog({
                            autoOpen: false,
                            draggable: false,
                            resizable: true,
                            autoResize: true,
                            width: width,
                            height: 'auto',
                            minHeight: 100,
                            title: 'Message',
                            dialogClass: 'noTitleStuff'
                        });
                        $(".ui-dialog-titlebar").hide();
                        $('.panel-body').show();


                        $('#btnok').off('click').on('click', function () {
                            $("#divmsg").dialog('close');
                        });

                        $('#msgSpan').text('Please select same month.'); //Please minimize your selection upto 31 days
                        shownodata();
                    }
                }
                else
                    getinternalCahrtdata(id);
            }
            else
                comingsoon();

        }
    }
    else {
        Showloading();
        $('#hdnreportid').val(id);
        $('#divReportDashboard').show();
        $('#Divchart').hide();
        $('#divreportcentermain').hide();
        $('#divnodata').hide();
        $('#divUnderConstruction').hide();
        $('#divcomingsoon').hide();
        $('#divUnderConstruction').hide();
        $('#divfinancialsummaryCover').hide();
        $('#divGridPrint').hide();
        $('#divChartPrint').hide();
        $('#divOverViewPanel').show();
        $('#divddlBranch').hide();
        $('#divPayerFilter').hide();
        $('#divcarrierTreePanel').hide();
        $('#divProviderTreePanel').hide();
        $('#divPracticeOverview').show();
        $('#divComparePanel').hide();
        $('#divGraphToText').hide();
        if (!isAllFacility)
            $('#divGobtn').hide();
        dashboarddata();
    }
    ApplyCssActiveReport();

    if (!iPadAgent && !iPhoneAgent && !AndroidAgent) {
        if (!$('.x-grid3-scroller').hasClass('nano'))
            $('.x-grid3-scroller').addClass('nano')
        $('.x-grid3-scroller').css('overflow', 'hidden');
        $('.nano').perfectScrollbar().perfectScrollbar('destroy');
        $('.nano').perfectScrollbar();
        setTimeout("$('.nano').perfectScrollbar('update');", 500);

    }
    else {
        $('.nano').css('overflow', 'auto');

        $('.nano').perfectScrollbar('destroy');
    }

    return false;
}

////////// Show No data /////////////////////////////////////////////

function ShowAllReportFilters() {
    $('#divcarrierTreePanel').show();
    $('#divProviderTreePanel').show();
    $('#divComparePanel').show();
    $('#divGraphToText').show();
    $('#divGridPrint').show();
    $('#divChartPrint').show();
    $('#divGobtn').show();
    $('#divddlBranch').show();
}
function HideAllReportFilters() {
    $('#divcarrierTreePanel').hide();
    $('#divProviderTreePanel').hide();
    $('#divComparePanel').hide();
    $('#divGraphToText').hide();
    $('#divGridPrint').hide();
    $('#divChartPrint').hide();
    $('#divddlBranch').hide();
    if (!isAllFacility)
        $('#divGobtn').hide();
}


function shownodata() {
    $('#divnodata').show();
    $('#divReportDashboard').hide();
    $('#Divchart').hide();
    $('#divcomingsoon').hide();
    $('#divUnderConstruction').hide();
    //$('#divFilterSection').hide();
    $('#Nodatatext').html('Not Available');
    HideAllReportFilters();
    HideLoading();
    if (!iPadAgent && !iPhoneAgent && !AndroidAgent) {
        $('.nano').perfectScrollbar().perfectScrollbar('destroy');
        $('.nano').perfectScrollbar();
    }
    $('#divNoNetwork').hide();
}

////////// Show No NetWork /////////////////////////////////////////////
function shownoNetwork() {
    $('#divnodata').hide();
    $('#divReportDashboard').hide();
    $('#Divchart').hide();
    $('#divcomingsoon').hide();
    $('#Nodatatext').html('');

    HideLoading();
    if (!iPadAgent && !iPhoneAgent && !AndroidAgent) {
        $('.nano').perfectScrollbar().perfectScrollbar('destroy');
        $('.nano').perfectScrollbar();
    }
    $('#divNoNetwork').show();
    $('#divnoConnect').show();
    CreateTimer();
}

////////Bind Branch Dropdown ///////////////////////////////////////////
function bindBranch() {


    var fromdate = '';
    var todate = '';
    var id = $('#hdnreportid').val() != "" ? $('#hdnreportid').val() : 0;

    var fid = $('#txtOverviewSearch').val();

    var daterange = $('#txtDateRange').val().split('-');
    if (daterange.length > 1) {
        fromdate = daterange[0];
        todate = daterange[1];
    }
    $("#ddlBranch").find('option').remove().end();
    if (id == 30 || id == 27 || id == 31) {
        $.post('../reportdata.aspx', { 'sp': 'getLocationFilter', 'fid': fid, 'FilterType': 'single', 'FromDate': fromdate, 'toDate': todate, 'id': id }, function (data) {
            if (data.length > 0) {

                $("#ddlBranch").removeClass('jqTransformHidden');
                $("#ddlBranch").removeClass('jqtransformdone');

                var ddl = $("#divddlBranch").find("#ddlBranch");
                $("#divddlBranch .jqTransformSelectWrapper").remove();
                $("#divddlBranch").append(ddl);
                ddl = null;
                var data = JSON.parse(data);
                $.each(data.Table, function (key, value) {
                    $("#ddlBranch").append($("<option></option>").val
                (value.Id).html(value.Name));
                });
            }




        }).done(function () {
            SetjqTransform();
        }).fail(function () {
            SetjqTransform();
        }).always(function () {
            SetjqTransform();
        });
    }
    else
        SetjqTransform();

}


function bindOverview() {

    var selectioncount = 100;

    $("#ddlOverview").find('option').remove().end();
    var id = document.getElementById("ContentHolder_hdnFacility").value.toString().split('-')[0];
    $.post('../GetDataPage.aspx', { 'overview': '1', 'insgrpid': -1, 'facilities': id }, function (data) {
        if (data.length > 0) {

            $("#ddlOverview").removeClass('jqTransformHidden');
            $("#ddlOverview").removeClass('jqtransformdone');

            var ddl = $("#divOverview").find("#ddlOverview");
            $("#ddlOverview .jqTransformSelectWrapper").remove();
            $("#ddlOverview").append(ddl);
            ddl = null;
            // var data = JSON.parse(data);
            $.each(data[0].children, function (key, value) {
                if (key < selectioncount) {
                    $("#ddlOverview").append($("<option></option>").val
                (value.id).html(value.text).prop('selected', value.checked));
                } else {
                    $("#ddlOverview").append($("<option></option>").val
                (value.id).html(value.text));
                }
            });
        }

        $("#ddlOverview").multipleSelect({
            filter: 'true',
            placeholder: 'ALL FACILITIES',
            onOpen: function () {
                //                var text = $('.ms-search').find('input');
                $('.ms-search div').removeClass('jqTransformInputInner jqTransformInputWrapper jqTransformInputWrapper_hover').css('width', '');
                $('.ms-drop').css({ 'width': 'auto', 'overflow-x': 'hidden' });
                $('.ms-drop ul').css({ 'padding-right': '20px', 'overflow-x': 'hidden' });
                $('.ms-search input').removeClass('jqtranformdone jqTransformInput');
                $("#divmsg").dialog('close');
                document.getElementById('txtOverviewSearch').value = "";
                document.getElementById('txtOverviewSearch').value = $("#ddlOverview").multipleSelect("getSelects");

            },
            onClose: function () {
                // $eventResult.text('Select closed!');
                $("#divmsg").dialog('close');
            },
            onCheckAll: function () {
                if (OverviewSelectionValidation(selectioncount)) {
                    document.getElementById('txtOverviewSearch').value = "";
                    document.getElementById('txtOverviewSearch').value = $("#ddlOverview").multipleSelect("getSelects");
                }

            },
            onUncheckAll: function () {
                if (OverviewSelectionValidation(selectioncount)) {
                    document.getElementById('txtOverviewSearch').value = "";
                    document.getElementById('txtOverviewSearch').value = $("#ddlOverview").multipleSelect("getSelects");
                }
            },
            onFocus: function () {
                // $eventResult.text('focus!');
            },
            onBlur: function () {
                // $eventResult.text('blur!');
            },
            onOptgroupClick: function (view) {
                if (OverviewSelectionValidation(selectioncount)) {
                    document.getElementById('txtOverviewSearch').value = "";
                    document.getElementById('txtOverviewSearch').value = $("#ddlOverview").multipleSelect("getSelects");
                }
            },
            onClick: function (view) {
                if (OverviewSelectionValidation(selectioncount)) {
                    document.getElementById('txtOverviewSearch').value = "";
                    document.getElementById('txtOverviewSearch').value = $("#ddlOverview").multipleSelect("getSelects");
                }

            }
        });

        document.getElementById('txtOverviewSearch').value = "";
        document.getElementById('txtOverviewSearch').value = $("#ddlOverview").multipleSelect("getSelects");

        if (!isAllFacility)
            $(".ms-parent").hide();
    }).done(function () {

    }).fail(function () {

    }).always(function () {

    });
    ;

}


function OverviewSelectionValidation(selectioncount) {
    var ids = $("#ddlOverview").multipleSelect("getSelects");

    BindDialogBox();

    if (ids.length > selectioncount) {
        $("#divmsg").dialog('open');
        $('#msgSpan').text('Maximum allowed selections are ' + selectioncount + '.');
        $("#divmsg").dialog('open');
        $('#ddlOverview').multipleSelect('setSelects', document.getElementById('txtOverviewSearch').value.split(','));
        return false;
    }
    else {
        $("#divmsg").dialog('close');
        return true;
    }
}



function BindDialogBox() {
    var width = windowWidth < 400 ? 250 : 300
    $("#divmsg").dialog({
        autoOpen: false,
        draggable: false,
        resizable: false,
        autoResize: true,
        modal: true,
        width: width,
        height: 'auto',
        minHeight: 100,
        title: 'Message',
        dialogClass: 'noTitleStuff'
    });
    $(".ui-dialog-titlebar").hide();
    $('.panel-body').show();

    $("#divmsg").dialog('close');

    $('#btnok').off('click').on('click', function () {
        $("#divmsg").dialog('close');

    });
}


function ValidatecheckedFacilities(id) {

    var count = document.getElementById('txtOverviewSearch').value.split(',').length;

    if (count > 1 && isAllFacility) {
        if (id == 23) {
            BindDialogBox();
            $("#divmsg").dialog('open');
            $('#msgSpan').text('Please select a single facility to view the detailed report.');
            return false;
        }
        else
            return true;
    }
    else if (document.getElementById('txtOverviewSearch').value == '') {
        BindDialogBox();
        $("#divmsg").dialog('open');
        $('#msgSpan').text('Please select atleast one facility');
       
        return false;
    }
    else
        return true;
}


///////// Go button Click Event /////////////////////////////////////////////////////
function Onclick_Go() {
    //    var crrier = $('#txtCarrierSearch').val();
    clearInterval(varinterval);
    $('#divnoConnect').hide();
    $('#Nodatatext').html('');
    var id = $('#hdnreportid').val();

    if (ValidatecheckedFacilities(id)) {

        if (id == "") {
            Showloading();
            $('#divReportDashboard').show();
            $('#Divchart').hide();
            $('#divreportcentermain').hide();
            $('#divnodata').hide();
            $('#divNoNetwork').hide();
            $('#divcomingsoon').hide();
            $('#divfinancialsummaryCover').hide();
            $('#divGridPrint').hide();
            $('#divChartPrint').hide();
            getOnreadyfunc();

        }
        if (id > 0) {
            ShowAllReportFilters();
            Showloading();
            if ($(window).width() < 650) {
                $('#dgFilter').hide();
                $('#divddlFilter').hide();
            }
            $('#divNoNetwork').hide();
            if (id == "18") {
                $('#divreportsetting').show();
                $('#divreportsettingOnlyDashboard').hide();
                $('#divreportsettingEmpty').show();
                $('#divcarrierTreePanel').hide();
                $('#divProviderTreePanel').hide();
                $('#divddlBranch').hide();
                $('#divPayerFilter').hide();
                if (!isAllFacility)
                    $('#divGobtn').hide();
            }
            else {
                $('#divreportsetting').hide();
                $('#divreportsettingOnlyDashboard').show();
                $('#divreportsettingEmpty').hide();
                $('#divcarrierTreePanel').hide();
                $('#divProviderTreePanel').hide();
                $('#divddlBranch').hide();
                $('#divPayerFilter').hide();
                $('#imgPdf').show();
                if (!isAllFacility)
                    $('#divGobtn').hide();
            }
            $('#divinnerchartContainerCover1').hide();
            $('#divinnerchartContainer2').hide();
            $('#divReportDashboard').hide();
            $('#divreportcentermain').show();
            $('#Divchart').show();
            $('#divnodata').hide();
            $('#divcomingsoon').hide();
            $('#divGraphToText').hide();
            $('#divcarrierTreePanel').hide();
            $('#divProviderTreePanel').hide();
            $('#divddlBranch').hide();
            $('#divComparePanel').hide();

            if (id == 18)
                $('#divreportcentermain').hide();

            if (id == 18 || id == 25 || id == 23 || id == 27 || id == 29 || id == 31) {
                $('#divfinancialsummaryCover').show();
                $('#divinnerchartContainerCover1').hide();
                $('#divinnerchartContainer2').hide();
                $('#divinnerchartContainerCover').hide();
                $('#divGridPrint').show();
                $('#divChartPrint').hide();

                if (id == 27) {
                    //bindBranch();
                    $('#divddlBranch').show();
                    $('#divGobtn').show();
                }

                if (id == 31) {
                    $('#divddlBranch').show();
                    $('#divGobtn').show();
                    $('#divPayerFilter').hide();
                    $('#divcarrierTreePanel').hide();
                    $('#divComparePanel').hide();
                    $('#divProviderTreePanel').show();
                    $('#divreportsettingOnlyDashboard').hide();
                    var selectedPID = document.getElementById('txtProviderSearch').value;
                    var filterid = $("#ddlBranch").val()
                    TreeURLProvider("getLocationsforDropDown", selectedPID, filterid);
                }
            }
            else {
                $('#divinnerchartContainerCover1').hide();
                $('#divinnerchartContainer2').hide();
                if (id == "2") {
                    $('#divddlBranch').show();
                    $('#divcarrierTreePanel').hide();
                    $('#divProviderTreePanel').hide();
                    $('#divreportsettingOnlyDashboard').hide();
                }
                else if (id == "3" || id == "5" || id == "15" || id == "16" || id == "28" || id == "26" || id == "24" || id == "22" || id == "21" || id == "30") {
                    $('#divcarrierTreePanel').show();
                    $('#divProviderTreePanel').show();
                    $('#divComparePanel').hide();
                    $('#divddlBranch').hide();
                    $('#divPayerFilter').hide();
                    $('#divreportsettingOnlyDashboard').hide();
                    var selectedID = document.getElementById('txtCarrierSearch').value;
                    var selectedPID = document.getElementById('txtProviderSearch').value;

                    if (!isAllFacility) {
                        if (id == "24" || id == "26" || id == "28" || id == "21" || id == "30")
                            $('#divGobtn').show();
                        else
                            $('#divGobtn').hide();
                    }
                    else
                        $('#divGobtn').show();

                    if (id == "21") {
                        $('#divGraphToText').show();
                        TreeURLCarrier("getDeficiencies", selectedID);
                        TreeURLProvider("getProvider", selectedPID, "");
                        fordeficiencyPanel();
                    }
                    else if (id == "28" || id == "26") {
                        if (id == "26")
                            $('#divComparePanel').show();
                        TreeURLCarrier("getPayerGroup", selectedID);
                        $('#divProviderTreePanel').hide();
                    }
                    else if (id == "24" || id == 22) {

                        if (id == 24) {
                            // Commented for use new procedure and hide compare dropdown
                            //$('#divComparePanel').show();
                            //TreeURLCarrier("getcodes", selectedID);
                            TreeURLCarrier("getcodes_OLAP", selectedID);
                            $('#divProviderTreePanel').hide();
                        }
                        else {
                            $('#divcarrierTreePanel').hide();
                            $('#divProviderTreePanel').hide();
                        }
                        $('#divinnerchartContainerCover1').show();
                    }
                    else {
                        TreeURLCarrier("getCarrierfordropdown", selectedID);

                    }
                }

                $('#divfinancialsummaryCover').hide();
                $('#divinnerchartContainerCover').show();
                $('#divGridPrint').hide();
                $('#divChartPrint').show();
                if (id == '30') {
                    $('#divComparePanel').hide();
                    TreeURLCarrier("getPayerGroup", selectedID);
                    $('#divddlBranch').show();
                    $('#divfinancialsummaryCover').hide();
                    $('#divLocationChartContainer').show();
                    $('#divinnerchartContainerCover').hide();
                    $('#divLocationGrid').hide();
                    $('#divLocationCharts').show();
                    //bindBranch();

                }
            }




            if (id == 20) {
                var daterange = $('#txtDateRange').val().split('-');
                var daydiff = true;
                if (daterange.length > 1) {
                    daydiff = getdaysdiff(daterange[0], daterange[1]);
                }

                if (daydiff)
                    getinternalCahrtdata(id);
                else {
                    var width = windowWidth < 400 ? 250 : 300
                    $("#divmsg").dialog({
                        autoOpen: false,
                        draggable: false,
                        resizable: true,
                        autoResize: true,
                        width: width,
                        height: 'auto',
                        minHeight: 100,
                        title: 'Message',
                        dialogClass: 'noTitleStuff'
                    });
                    $(".ui-dialog-titlebar").hide();
                    $('.panel-body').show();
                    HideLoading();

                    $('#btnok').off('click').on('click', function () {
                        $("#divmsg").dialog('close');
                    });

                    $('#msgSpan').text('Please select same month.');
                }
            }
            else {
                if (id == "26" || id == "24") {
                    if (showErrormsgPopup())
                        getinternalCahrtdata(id);
                }
                else if (id == "21") {
                    if (deftype == "Graph")
                        showDeficiencyGraph();
                    else if (deftype == "Summary")
                        showDeficiencySummary();
                    else
                        showDeficiencyDetail();
                }
                else if (id == 30) {
                    if (locationType == "chart") {
                        $('#divLocationCharts').show();
                        $('#divLocationGrid').hide();
                        getinternalCahrtdata(id);
                    }
                    else {
                        getLocationGrid();
                        $('#divLocationCharts').hide();
                        $('#divLocationGrid').show();
                    }
                }

                else
                    getinternalCahrtdata(id);
            }

            if (id != "21") {
                if (!$('.x-grid3-scroller').hasClass('nano'))
                    $('.x-grid3-scroller').addClass('nano')
                if (!iPadAgent && !iPhoneAgent && !AndroidAgent) {
                    $('.x-grid3-scroller').css('overflow', 'hidden');
                }
                else
                    $('.nano').perfectScrollbar('destroy');
            }
            $('#divComapareforSelection').hide();
            $('#divtreeCarrier').hide();
            if ((navigator.appName == "Microsoft Internet Explorer") || (navigator.userAgent.indexOf("rv:11.0") > -1) || iPadAgent || iPhoneAgent || AndroidAgent) {
                noshowexport();
            }
        }
        else {
            Showloading();
            $('#divReportDashboard').show();
            $('#Divchart').hide();
            $('#divreportcentermain').hide();
            $('#divnodata').hide();
            $('#divcomingsoon').hide();
            $('#divfinancialsummaryCover').hide();
            $('#divGridPrint').hide();
            $('#divChartPrint').hide();
            $('#divNoNetwork').hide();
            dashboarddata();
        }
    }
    return false;
}



////////////////////  Set Scroll divs Height///////////////////////////////////////
function setscrollheight() {

    try {
        var mainhead = $('#tabs-container').outerHeight();
        var greenhead = $('#nav-container').outerHeight();
        var rpthead = $('.reportschooseblack').outerHeight();
        var footer = $('.footer').outerHeight();
        var winheight = $(window).height();




        $('#divmenuscroll,#divdatascroll').height(winheight - (mainhead + greenhead + rpthead + footer));
        // $('#divdatascroll').height(winheight - (mainhead + greenhead + rpthead + footer));

        $('#divnodata, #divNoNetwork,#divUnderConstruction').height(winheight - (mainhead + greenhead + rpthead + footer + footer));

        $('#Nodatatext,#DivUCtext').css('margin-top', $('#divnodata').height() / 2 - 20);

        // $('#divNoNetwork').height(winheight - (mainhead + greenhead + rpthead + footer + footer));

        $('#divmenuscrollpc,#divdatascrollpc').height(winheight - (mainhead + greenhead + rpthead + footer));

        // $('#divdatascrollpc').height(winheight - (mainhead + greenhead + rpthead + footer));


        if (!iPadAgent && !iPhoneAgent && !AndroidAgent) {

            if (!$('.x-grid3-scroller').hasClass('nano'))
                $('.x-grid3-scroller').addClass('nano')
            $('.x-grid3-scroller').css('overflow', 'hidden');
            $('.nano').perfectScrollbar().perfectScrollbar('destroy');
            $('.nano').perfectScrollbar();
            setTimeout("$('.nano').perfectScrollbar('update');", 1000);

            //            $('.x-grid3-scroller').perfectScrollbar().perfectScrollbar('destroy');
            //            $('.x-grid3-scroller').perfectScrollbar();

            // setTimeout("$('.x-grid3-scroller').perfectScrollbar('update');", 1000);
        }
        else {

            if (iPadAgent)
                $('#divdatascroll').width($(window).width() * 84 / 100);
            else if (iPhoneAgent)
                $('#divdatascroll').width($(window).width() * 99 / 100);
            else {
                if ($(window).width() > 640 && $(window).height() > 640)
                    $('#divdatascroll').width($(window).width() * 84 / 100);
                else
                    $('#divdatascroll').width($(window).width() * 99 / 100);
            }
            $('.nano').css('overflow', 'auto');

            $('.nano').perfectScrollbar('destroy');


            IPhoneStyle();
            if (AndroidAgent) {
                if ($(window).width() > 650 || $(window).height() > 650) {
                    if ($(window).width() < 650 || $(window).height() < 650) {
                        if ($(window).width() > $(window).height()) {
                            var iteg = $('#help').find('.icon-question-sign');
                            $('#help').empty();
                            $('#help').append(iteg);
                            $('.help').append("Help/Feedback");
                        }
                        else {
                            var iteg = $('#help').find('.icon-question-sign');
                            $('#help').empty();
                            $('#help').append(iteg);
                        }
                    }
                }
            }
        }
    }
    catch (e) {
        //alert(e);
    }


}


$(window).resize(function () {
    setscrollheight();
    setTimeout(" resizecharts();", 1000);
    //setTimeout("$('.nano').perfectScrollbar('update');", 1000);


});




function getReportName(type, date) {
    var id = $('#hdnreportid').val();
    var fromdate = '';
    var todate = '';
    if (id == 20) {
        var daterange = $('#txtMonth').val().split('-');
        if (daterange.length > 1) {
            fromdate = daterange[0].trim();
            todate = daterange[1].trim();
        }
    }
    else {
        var daterange = $('#txtDateRange').val().split('-');
        if (daterange.length > 1) {
            fromdate = daterange[0].trim();
            todate = daterange[1].trim();
        }
    }

    var fid = $('#txtOverviewSearch').val();
    $.get('../reportdata.aspx?sp=getReportName&FromDate=' + fromdate + '&toDate=' + todate + '&rpt=' + id + '&type=' + type + '&date=' + date + '&fid=' + fid, function (data) {
        // $.post('../reportdata.aspx', { 'sp': 'getReportName', 'FromDate': fromdate, 'toDate': todate, 'rpt': id, 'type': type, 'date': date, 'fid': fid }, function (data) {
        var data = JSON.parse(data);
        try {
            if (id != 30)
                $('#spanGridTitle').html(data.Table[0].Reportheading);
            else
                $('#divLocationTitle').html(data.Table[0].Reportheading);
        }
        catch (e) {
            //shownoNetwork();
        }
    }).done(function () {
    }).fail(function () {
        // shownoNetwork();
    });

}



////////////////// Coming Soon Image Show //////////////////////////////////////////////
function comingsoon() {
    $('#divcomingsoon').show();
    $('#divnodata').hide();
    $('#divReportDashboard').hide();
    $('#divUnderConstruction').hide();
    $('#divFilterSection').hide();
    //$('#Divchart').hide();
    HideLoading();
}


function ShowUnderConstruction() {
    shownodata();
    $('#divnodata').hide();
    $('#divUnderConstruction').show();
}

//////////////// Export Grid //////////////////////////////////////////////////////////
function ExportAll() {
    $('#imgExcel').unbind('click').bind("click", function () {

        try {
            // alert(grid.getExcelXml());
            //            if ($('#hdnreportid').val() == 18) {
            //                $('#datatable tr td').append($('#divfinancialsummary').clone());
            //                return ExcellentExport.excel(this, 'datatable', 'mydata');
            //            }
            //            else {
            //            grid.store.commitChanges();
            //                            window.open('data:application/vnd.ms-excel;charset=utf-8;base64,' +
            //                                     Base64.encode(grid.getExcelXml()));
            //                            return false;

            var id;
            id = $('#hdnreportid').val();
            var fstcol = " ";
            var ordrcol = "";
            var grpcol = "";
            var zcolum = "";
            var image = $('.marslogo').attr('src');
            var title =encodeURIComponent($('#spanGridTitle').html());
            if (id == "27" || id == "18" || id == "31") {
                grpcol = "grp";
                zcolum = "value";
                if (id == "27") {
                    fstcol = $('#ContentHolder_hdnFacility').val().split('-')[0];
                    ordrcol = $('#ddlBranch').val()
                    grpcol = "rowgrp";
                    zcolum = "amount"
                }
                if (id == 31) {
                    grpcol = "colgrp";
                    zcolum = "amount";
                }
                else
                    ordrcol = "Morder";
            }
            else {
                if (id == "20") {
                    fstcol = "";
                    ordrcol = "ordr";
                }
                else if (id == "23")
                    fstcol = "Run Number,DOS,Charges,Payer";
                else if (id == "25")
                    fstcol = "Code,Description";
                else if (id == '29')
                    ordrcol = "ordr";

            }
            document.getElementById('iframeExport').src = "Export.aspx?type=xls&id=" + id + "&name=" + $('#divmenuscroll .active a')[1].innerHTML + "&ocol=" + ordrcol + "&comuncol=" + fstcol + "&grpcol=" + grpcol + "&zcolum=" + zcolum + '&image=' + image + '&title=' + title;
            HideLoading();
            return false;

        }
        catch (e) {
            // alert(e);
            return false;
        }
    });

    $('#imgPdf').unbind('click').bind("click", function () {
        try {
            // alert(grid.getExcelXml());
            //            window.open('data:application/pdf;charset=utf-8;base64,' +
            //                         Base64.encode(grid.getExcelXml()));

            var id;
            id = $('#hdnreportid').val();
            var fstcol = " ";
            var ordrcol = "";
            var grpcol = "";
            var zcolum = "";
            var image = $('.marslogo').attr('src');
            var title = encodeURIComponent($('#spanGridTitle').html());
            if (id == "27" || id == "18" || id == "31") {
                grpcol = "grp";
                zcolum = "value";
                if (id == "27") {
                    fstcol = $('#ContentHolder_hdnFacility').val().split('-')[0];
                    ordrcol = ordrcol = $('#ddlBranch').val();
                    grpcol = "rowgrp";
                    zcolum = "amount";
                }
                if (id == 31) {
                    grpcol = "colgrp";
                    zcolum = "amount";
                    ordrcol = "rowgrp";
                }
                else
                    ordrcol = "Morder";
            }
            else {
                if (id == "20") {
                    fstcol = "";
                    ordrcol = "ordr";
                }
                else if (id == "23")
                    fstcol = "Run Number,DOS,Charges,Payer";
                else if (id == "25")
                    fstcol = "Code,Description";
                else if (id == '29')
                    ordrcol = "ordr";

            }

            document.getElementById('iframeExport').src = "Export.aspx?type=pdf&id=" + id + "&name=" + $('#divmenuscroll .active a')[1].innerHTML + "&ocol=" + ordrcol + "&comuncol=" + fstcol + "&grpcol=" + grpcol + "&zcolum=" + zcolum + '&image=' + image+'&title='+title;
            HideLoading();
            return false;
        }
        catch (e) {
            //alert(e);
            return false;
        }
    });

    $('#imgHtml').unbind('click').bind("click", function () {
        try {

            //            if ($('#hdnreportid').val() == 18) {
            //                var data = document.getElementById("divfinancialsummary").innerHTML;
            //                ChartPrint(data);
            //            }
            //            else
            Ext.ux.Printer.print(Ext.getCmp('id'));
            return false;
        }
        catch (e) {
            //alert(e);
            return false;
        }
    });

    // $('.x-grid3-body').perfectScrollbar();

}

///////////////////// Export Charts //////////////////////////////////////////////////////////////
function ExportChart(id) {
    $('#imgHtmlChart').unbind('click').bind("click", function () {


        var divElements = document.getElementById("divinnerchartContainer").innerHTML;
        if (id == 24 || id == 26) {
            divElements += document.getElementById("divinnerchartContainer1").innerHTML;
            divElements += document.getElementById("divinnerchartContainer2").innerHTML;
        }
        ChartPrint(divElements);

    });

    $('#imgExcelchart').unbind('click').bind("click", function () {


        //        var svg = $('#divinnerchartContainer svg').css('background-color', '#ffffff');
        //        svg[0].toDataURL("image/png", {
        //            callback: function (data2) {
        //                var img = new Image();
        //                img.setAttribute('src', data2);
        //                $('#datatable tr td').append(img);

        //                return ExcellentExport.excel(this, 'tableright', 'mydata');
        //            }
        //        });

    });

    $('#imgPdfChart').unbind('click').bind("click", function () {
        // alert("Searching........");
        printPdf();
    });
}

function ChartPrint(data) {

    //Get the HTML of whole page
    var mywindow = window.open('', 'my div', 'height=600,width=800');
    mywindow.document.write('<html><head><title></title>');
    /*optional stylesheet*/ //mywindow.document.write('<link rel="stylesheet" href="main.css" type="text/css" />');
    mywindow.document.write('</head><body>');
    mywindow.document.write(data);
    mywindow.document.write('</body></html>');
    //mywindow.style.display = 'none';
    mywindow.document.close();
    mywindow.print();
    mywindow.close();
    //disable postback on print button
    return false;
}


function printPdfdata(base64img) {

    var id = $('#hdnreportid').val();
    var svg = $('#divinnerchartContainer svg').clone().css('background-color', '#ffffff');
    svg[0].toDataURL("image/jpeg", {
        callback: function (data1) {
            if (id == 24 || id == 22) {    // if (id == 24 || id == 26) {
                var svg1 = $('#divinnerchartContainer1 svg').clone().css('background-color', '#ffffff');
                svg1[0].toDataURL("image/jpeg", {
                    callback: function (data2) {
                        var doc = new jsPDF('landscape');

                        doc.addImage(base64img, 'JPEG', 10, 5, 60, 15);
                        doc.setLineWidth(0.1);
                        doc.line(10, 25, 280, 25);
                        doc.setFontSize(12);

                        doc.addImage(data1, 'JPEG', 20, 40, 250, 100);
                        $.each($("#divsummary h3"), function (index, item) {
                            doc.text(170, (145 + 5 * index), $(item).text());
                        });

                        doc.line(10, 195, 280, 195);
                        doc.setFontSize(11);
                        doc.setTextColor(100);
                        doc.setFontType("normal");
                        doc.text(0, 200, $('.footer .left').text());

                        var footer = $('.footer .right span').text() + " Global RCP";
                        doc.text(215, 204, footer);

                        doc.addPage();

                        doc.addImage(base64img, 'JPEG', 10, 5, 60, 15);
                        doc.setLineWidth(0.1);
                        doc.line(10, 25, 280, 25);
                        doc.setFontSize(12);

                        doc.addImage(data2, 'JPEG', 20, 40, 250, 100);
                        $.each($("#divsummary1 h3"), function (index, item) {
                            doc.text(170, (145 + 5 * index), $(item).text());
                        });

                        doc.line(10, 195, 280, 195);
                        doc.setFontSize(11);
                        doc.setTextColor(100);
                        doc.setFontType("normal");
                        doc.text(0, 200, $('.footer .left').text());

                        var footer = $('.footer .right span').text() + " Global RCP";
                        doc.text(215, 204, footer);
                        var out = doc.output('save', $('#divmenuscroll .active a')[1].innerHTML + '.pdf');
                        //                       }
                    }
                });
            }
            else {
                var doc = new jsPDF('landscape');

                doc.addImage(base64img, 'JPEG', 10, 5, 60, 15);
                doc.setLineWidth(0.1);
                doc.line(10, 25, 280, 25);
                doc.setFontSize(12);

                doc.addImage(data1, 'JPEG', 20, 40, 250, 100);
                $.each($("#divsummary h3"), function (index, item) {
                    doc.text(170, (145 + 5 * index), $(item).text());
                });

                doc.line(10, 195, 280, 195);
                doc.setFontSize(11);
                doc.setTextColor(100);
                doc.setFontType("normal");
                doc.text(0, 200, $('.footer .left').text());

                var footer = $('.footer .right span').text() + " Global RCP";
                doc.text(215, 204, footer);

                var out = doc.output('save', $('#divmenuscroll .active a')[1].innerHTML + '.pdf');
            }
        }
    });
}


function printPdf() {
    convertImgToBase64(function (base64img) { printPdfdata(base64img) }, "image/jpeg");
    return false;
}

function convertImgToBase64(callback, outputFormat) {
    var canvas = document.createElement('CANVAS');
    var ctx = canvas.getContext('2d');
    var img = new Image;
    img.crossOrigin = 'Anonymous';
    $(img).css('background', '#fff');
    img.onload = function () {
        canvas.height = img.height;
        canvas.width = img.width;
        ctx.fillStyle = '#fff';
        ctx.fillRect(0, 0, img.width, img.height);
        ctx.drawImage(img, 0, 0);
        var dataURL = canvas.toDataURL(outputFormat || 'image/png');
        callback.call(this, dataURL);
        // Clean up
        canvas = null;
    };
    img.src = $('.marslogo').attr('src'); //url;
}


//////////////Hide All Export/////////////////////////////////////////

function noshowexport() {
    $('#divGridPrint').hide();
    $('#divChartPrint').hide();
}



if (!Number.prototype.dollarFormat) {
    // Simple U.S Dollar Formatter - By Doug Jones (MIT Licensed)
    Number.prototype.dollarFormat = function () {
        if (!isNaN(this)) {
            var n = this < 0 ? true : false,
             a = (n ? this * -1 : this).toFixed(2).toString().split("."),
             b = a[0].split("").reverse().join("").replace(/.{3,3}/g, "$&,").replace(/\,$/, "").split("").reverse().join("");
            return ((n ? "(" : "") + "$" + b + "." + a[1] + (n ? ")" : ""));
        }
    };
}


////////////// Popup window for Payers ////////////////////////////////////
var payergroup;
function showpayerpopup(payergrp, frmdate, todate, rpt) {
    $.get('../reportdata.aspx?con=check', function (data) {

        var flag = true;
        var data = JSON.parse(data);
        var speed = data.speed[0].speed;
        if (speed == 0) {
            $('#divnoConnect').show();
            CreateTimer();
            flag = false;
        }
        else if (speed < 50) {
            $('#divslow').show();
            flag = true;
        }

        if (flag) {
            if ($(window).width() < 650) {
                $('#divpayertable').height(209);
            }

            var width = windowWidth < 400 ? 250 : 300
            $("#dialog1").dialog({
                autoOpen: true,
                draggable: false,
                resizable: false,
                autoResize: true,
                modal: true,
                width: width,
                height: 'auto',
                minheight: 100,
                title: 'Payers (' + payergrp + ')'
            });
            payergroup = payergrp;
            $('.panel-body').show();
            $('#divpayertable').empty();
            var id = $('#hdnreportid').val();

            var predata = $('#txtSelectedSearhfield').val();

            var fid = $('#txtOverviewSearch').val();
            $.get('../reportdata.aspx?sp=getPayers&payergrp=' + payergrp + '&FromDate=' + frmdate + '&toDate=' + todate + '&rpt=' + id + '&fid=' + fid + '&predata=' + predata, function (data) {
                //$.post('../reportdata.aspx', { 'sp': 'getPayers', 'payergrp': payergrp, 'FromDate': frmdate, 'toDate': todate, 'rpt': id, 'fid': fid }, function (data) {
                var data = JSON.parse(data);
                getpayerlist(data.Table)
            });

        }
    }).fail(function () {
        $('#divnoConnect').show();
        if ($('#divnodata').is(':visible'))
            shownoNetwork();
        else
            CreateTimer();
    });

}


function getpayerlist(data) {
    $('#divpayertable').empty();
    var table1 = '<table cellpadding="0" cellspacing="0" style="width:100%;line-height:30px;">';
    var table = '<table cellpadding="0" cellspacing="0" style="width:100%;line-height:20px;">';
    for (var key in data) {
        if (data[key].insgrpid != undefined) {

            if (data[key].insgrpid == 1)
                table1 += '<tr><th style=" text-align:center; width:10%"><input type="checkbox" id="checkall"  /></th><th style=" padding-left:20px">' + data[key].name + '</th><th align="right"><div class="imgPopupClose"><img id="imgPopupClose" src="../tree/themes/icons/cancel.png" /></div><th></tr></table>';
            else
                table += '<tr><td style="border-bottom:1px solid #808080; text-align:center; width:10%"><input type="checkbox" class="chkchild" value="' + data[key].insgrpid + '" /></td><td style="border-bottom:1px solid #808080; padding-left:20px">' + data[key].name + '</td></tr>';
        }
    }
    table += '</table>';

    $('#tablehead').html(table1);
    $('#divpayertable').html(table);
    if (data.length < 2) {
        $('#divpayertable').html('<div style="margin:0 auto;"><img src="../images/DataNotFound2.jpg" alt="No Data Available" style="vertical-align: middle; max-width:100%; height:100%"/><div>')
        $('#btngopopup').hide();
    }
    else {
        $('#btngopopup').show();
    }



    if (!iPadAgent && !iPhoneAgent && !AndroidAgent) {
        $('#divpayertable').perfectScrollbar('destroy');
        $('#divpayertable').perfectScrollbar();
    }
    else {
        $('#divpayertable').css('overflow', 'auto');
        $('#divpayertable').perfectScrollbar('destroy');

    }

    $('#imgPopupClose').off('click').on('click', function () {
        $("#dialog1").dialog('close');

    });


    $('#checkall').change(function () {
        if ($(this).is(":checked")) {
            $('.chkchild').attr('checked', true);
        }
        else
            $('.chkchild').attr('checked', false);

    });



    $('.chkchild').change(function () {
        var count = $('.chkchild');
        var countcheck = $('.chkchild:checked');
        if (count.length == countcheck.length)
            $('#checkall').attr('checked', true);
        else
            $('#checkall').attr('checked', false);

    });



    $('#btngopopup').off('click').on('click', function () {
        if ($(window).width() < 650) {
            $('#dgFilter').hide();
            $('#divddlFilter').hide();
        }
        var lst = $('.chkchild:checked');
        var width = windowWidth < 400 ? 250 : 300
        $("#divmsg").dialog({
            autoOpen: false,
            draggable: false,
            resizable: false,
            autoResize: true,
            modal: true,
            width: width,
            height: 'auto',
            minHeight: 100,
            title: 'Message',
            dialogClass: 'noTitleStuff'
        });
        $(".ui-dialog-titlebar").hide();
        $('.panel-body').show();


        $('#btnok').off('click').on('click', function () {
            $("#divmsg").dialog('close');

        });



        if (lst.length > 0) {
            if (lst.length > 20) {
                $("#divmsg").dialog('open');
                $('#msgSpan').text('Maximum allowed selections are 20.');
            }

            else {
                $("#divmsg").dialog('close');
                var payers = '';
                for (var i = 0; i < lst.length; i++) {
                    payers = lst[i].value + ',' + payers;
                }
                $('#divsummary').empty();
                if ($('#hdnreportid').val() == "28")
                    getInsuranceAgingReportByPayer(payers.substring(0, payers.length - 1));
                else
                    getTransactionSummaryByTypePayer(payers.substring(0, payers.length - 1), payergroup);
            }
        }
        else {
            $('#msgSpan').text('Please select atleast one Payer.');
            $("#divmsg").dialog('open');
        }
    });
}


function getInsuranceAgingReportByPayer(payers) {
    $.get('../reportdata.aspx?con=check', function (data) {

        var flag = true;
        var data = JSON.parse(data);
        var speed = data.speed[0].speed;
        if (speed == 0) {
            $('#divnoConnect').show();
            CreateTimer();
            flag = false;
        }
        else if (speed < 50) {
            $('#divslow').show();
            flag = true;
        }

        if (flag) {
            $("#dialog1").dialog('close');
            $('#divGobtn, #divOverViewPanel').hide();
            Showloading();
            $('#divddlFilter').show();
            $('#divcarrierTreePanel').hide();
            $('#divProviderTreePanel').hide();
            $('#divPayerFilter').show();
            $('#divinnerchartContainer').empty();
            if ((navigator.appName == "Microsoft Internet Explorer") || (navigator.userAgent.indexOf("rv:11.0") > -1) || iPhoneAgent || iPadAgent || AndroidAgent) {
                noshowexport();
            }
            var fromdate = '';
            var todate = '';
            var date = $("#txtDate").val();
            if (date != 'SELECT DATE' && date != '')
                todate = date.trim();

            var fid = $('#txtOverviewSearch').val();
            var id = $('#hdnreportid').val();
            $.get('../reportdata.aspx?sp=getAgedTrialBalanceByPayer&payer=payer&id=' + id + '&branch=' + -1 + '&carrier=' + payers + '&FromDate=' + fromdate + '&toDate=' + todate + '&predata=&fid=' + fid, function (data) {
                //$.post('../reportdata.aspx', { 'sp': 'getAgedTrialBalanceByPayer', 'payer': 'payer', 'id': id, 'branch': -1, 'carrier': payers, 'FromDate': fromdate, 'toDate': todate, 'predata': '', 'fid': fid }, function (data) {
                var data = JSON.parse(data);
                getLineChart('divinnerchartContainer', data.Table, data.series, true, data.Table1, 'spline', false, 'currency', data.Table2, 'divsummary');
            }).done(function () {
                HideLoading();
                if (!iPadAgent && !iPhoneAgent && !AndroidAgent) {
                    if (!$('.x-grid3-scroller').hasClass('nano'))
                        $('.x-grid3-scroller').addClass('nano')
                    $('.x-grid3-scroller').css('overflow', 'hidden');

                    $('.nano').perfectScrollbar().perfectScrollbar('destroy');
                    $('.nano').perfectScrollbar();
                    setTimeout("$('.nano').perfectScrollbar('update');", 300);
                }
                else
                    $('.nano').perfectScrollbar('destroy');
            }).fail(function () {
                HideLoading();
                // shownodata();
                shownoNetwork();
            }).always(function () {
                HideLoading();
            });


            $('#divPayerFilter').off('click').on('click', function () {
                $("#dialog1").dialog('open');


            });
        }
    }).fail(function () {
        $('#divnoConnect').show();
        if ($('#divnodata').is(':visible'))
            shownoNetwork();
        else
            CreateTimer();
    });

}


function getTransactionSummaryByTypePayer(payers, type) {
    $.get('../reportdata.aspx?con=check', function (data) {

        var flag = true;
        var data = JSON.parse(data);
        var speed = data.speed[0].speed;
        if (speed == 0) {
            $('#divnoConnect').show();
            CreateTimer();
            flag = false;
        }
        else if (speed < 50) {
            $('#divslow').show();
            flag = true;
        }

        if (flag) {
            Showloading();
            $("#dialog1").dialog('close');
            $('#divGobtn, #divOverViewPanel, #divComparePanel').hide();
            $('.divloading').css('z-index', '3000');
            $('#divcarrierTreePanel').hide();
            $('#divProviderTreePanel').hide();
            $('#divddlFilter').show();
            $('#divPayerFilter').show();
            $('#divinnerchartContainer').empty();
            if ((navigator.appName == "Microsoft Internet Explorer") || (navigator.userAgent.indexOf("rv:11.0") > -1) || iPhoneAgent || iPadAgent || AndroidAgent) {
                noshowexport();
            }
            var fromdate = '';
            var todate = '';
            var daterange = $('#txtDateRange').val().split('-');
            if (daterange.length > 1) {
                fromdate = daterange[0].trim();
                todate = daterange[1].trim();
            }

            var fid = $('#txtOverviewSearch').val();
            var id = $('#hdnreportid').val();

            // $.post('../reportdata.aspx', { 'sp': 'getTransactionSummaryByTypePayer', 'payer': 'payer', 'id': id, 'branch': -1, 'carrier': payers, 'FromDate': fromdate, 'toDate': todate, 'predata': type, 'fid': fid }, function (data) {

            $.get('../reportdata.aspx?sp=getTransactionSummaryByTypePayer&payer=payer&id=' + id + '&branch=' + -1 + '&carrier=' + payers + '&FromDate=' + fromdate + '&toDate=' + todate + '&predata=' + type + '&fid=' + fid, function (data) {
                var data = JSON.parse(data);
                getLineChart('divinnerchartContainer', data.Table, data.series, true, data.Table1, 'spline', false, 'currency', data.Table2, 'divsummary');
            }).done(function () {
                HideLoading();
                if (!iPadAgent && !iPhoneAgent && !AndroidAgent) {
                    if (!$('.x-grid3-scroller').hasClass('nano'))
                        $('.x-grid3-scroller').addClass('nano')
                    $('.x-grid3-scroller').css('overflow', 'hidden');
                    $('.nano').perfectScrollbar().perfectScrollbar('destroy');
                    $('.nano').perfectScrollbar();
                    setTimeout("$('.nano').perfectScrollbar('update');", 300);

                    //            $('.x-grid3-scroller').perfectScrollbar().perfectScrollbar('destroy');
                    //            $('.x-grid3-scroller').perfectScrollbar();


                }
                else
                    $('.nano').perfectScrollbar('destroy');
            }).fail(function () {
                HideLoading();
                //shownodata();
                shownoNetwork();
            }).always(function () {
                HideLoading();
            });


            $('#divPayerFilter').off('click').on('click', function () {
                $("#dialog1").dialog('open');


            });
        }
    }).fail(function () {
        $('#divnoConnect').show();
        if ($('#divnodata').is(':visible'))
            shownoNetwork();
        else
            CreateTimer();
    });


}



function resizecharts() {
    var id = $('#hdnreportid').val();

    windowWidth = $(window).width();
    if (id > 0) {
        if (id != "23" && id != "27" && id != "18") {
            if (id == "21" && deftype == "Graph") {

                pieChartInstanceMethod('divinnerchartContainer', $(window).width());
            }
            else if (id == '22' || id == '24') {
                if (iPhoneAgent) {
                    $('#divinnerchartContainer').width($(window).width() * 96 / 100 - 3);
                    $('#divinnerchartContainer1').width($(window).width() * 96 / 100 - 3);
                }
                else if (iPadAgent) {
                    $('#divinnerchartContainer').width($(window).width() * 82 / 100 - 5);
                    $('#divinnerchartContainer1').width($(window).width() * 82 / 100 - 5);
                }
                else if (AndroidAgent) {
                    if ($(window).width() > 640) {
                        $('#divinnerchartContainer').width($(window).width() * 82 / 100 - 5);
                        $('#divinnerchartContainer1').width($(window).width() * 82 / 100 - 5);
                    }
                    else {
                        $('#divinnerchartContainer').width($(window).width() * 96 / 100 - 3);
                        $('#divinnerchartContainer1').width($(window).width() * 96 / 100 - 3);
                    }
                }
                setscrollheight();
                lineChartInstanceMethod('divinnerchartContainer', $(window).width());
                lineChartInstanceMethod('divinnerchartContainer1', $(window).width());

            }
            else if (id != "21") {
                $('#divinnerchartContainer').css('width', '98%');
                $('#divinnerchartContainer1').css('width', '98%');
                lineChartInstanceMethod('divinnerchartContainer', $(window).width());
            }
        }
    }
    else {
        lineChartInstanceMethod('archartContainer', $(window).width());
        lineChartInstanceMethod('barchartContainer', $(window).width());
        pieChartInstanceMethod('PiechartContainer', $(window).width());
        pieChartInstanceMethod('linechartContainer', $(window).width());

        $.each($('#divGaugedata div'), function (index, item) {
            gaugeInstanceMethod($(item)[0].id);

        });

    }
}




function gaugeInstanceMethod(contrl) {
    try {
        var width = $('#tblhighlight .highlightbox').width();
        var gauge = $('#' + contrl).dxCircularGauge('instance');
        var gaugesize = { size: { height: $('#' + contrl).height(), width: width} };
        gauge.option(gaugesize);
    }
    catch (e) {
        gauge = null;
    }
}

function lineChartInstanceMethod(contrl, window) {
    var chart;
    try {
        var titlefont = 15;
        var tooltipfont = 15;
        var vertalign = 'top';
        var horizalign = 'right';
        var window = windowWidth;
        if ((window < 600) && (window > 400)) {
            titlefont = 13;
            tooltipfont = 13;
            vertalign = 'bottom';
            horizalign = 'center';
        }
        else if (window < 400) {
            titlefont = 12;
            tooltipfont = 12;
            vertalign = 'bottom';
            horizalign = 'center'
        }
        else {
            titlefont = 15;
            tooltipfont = 15;
            vertalign = 'top';
            horizalign = 'right';
        }
        chart = $('#' + contrl).dxChart('instance');
        chart.option({
            size: {
                height: $('#' + contrl).height(),
                width: $('#' + contrl).width() > window ? window : $('#' + contrl).width()
            },
            animation: {
                enabled: false
            },
            title: { font: { color: '#868686', size: titlefont, family: 'trebuchet ms', weight: 900} },
            tooltip: { font: { size: tooltipfont} },
            // valueAxis: { font: { size: titlefont} },
            argumentAxis: { font: { size: titlefont} },
            legend: {
                font: { size: tooltipfont - 2 },
                verticalAlignment: vertalign.toString(),
                horizontalAlignment: horizalign.toString(),
                markerSize: (tooltipfont - 4)
            }
        });
    }
    catch (e) {
        chart = undefined;
    }
    finally {
        chart = undefined;
    }
}

function pieChartInstanceMethod(contrl, window) {


    var titlefont = 15;
    var tooltipfont = 15;
    var vertalign = 'top';
    var horizalign = 'right';
    var window = windowWidth;
    if ((window < 600) && (window > 400)) {
        titlefont = 13;
        tooltipfont = 13;
        vertalign = 'bottom';
        horizalign = 'center';
    }
    else if (window < 400) {
        titlefont = 12;
        tooltipfont = 12;
        vertalign = 'bottom';
        horizalign = 'center'
    }
    else {
        titlefont = 15;
        tooltipfont = 15;
        vertalign = 'top';
        horizalign = 'right';
    }



    var piechart = $('#' + contrl).dxPieChart('instance');
    piechart.option({
        size: {
            height: $('#' + contrl).height(),
            width: $('#' + contrl).width()
        },
        animation: {
            enabled: false
        },
        title: { font: { color: '#868686', size: titlefont, family: 'trebuchet ms', weight: 900} },
        tooltip: { font: { size: tooltipfont} },
        legend: {
            verticalAlignment: vertalign.toString(),
            horizontalAlignment: horizalign.toString(),
            font: { size: tooltipfont },
            markerSize: (tooltipfont - 4)
        }
    });
    piechart = null;
}



function getdaysdiff(startdate, enddate) {
    return true;

    if (startdate != "" && enddate != "") {
        if ((startdate.split('/')[0].toString().trim() == enddate.split('/')[0].toString().trim()) && (startdate.split('/')[2].toString().trim() == enddate.split('/')[2].toString().trim()))
            return true;
        else
            return false
    }




}


// Show Error Popup when Select exceed from 20 
function showErrormsgPopup() {
    var id = $('#hdnreportid').val();
    var comVal = $('#txtSelectedSearhfield').val();
    var carVal = $('#txtCarrierSearch').val();

    var splitVComVal = comVal.split(',');
    var splitVCarrVal = carVal.split(',');

    if (splitVCarrVal.length > 0, splitVComVal.length > 0) {
        var carrNum = 0;
        var comNum = 0;
        if (splitVCarrVal[0] == "1")
            carrNum = splitVCarrVal.length - 1;
        else
            carrNum = splitVCarrVal.length;

        for (var i = 0; i < splitVComVal.length; i++) {
            if (splitVComVal[i] != "1")
                comNum++;
        }


        if ((carrNum * comNum > 20 && id != 24) || (carrNum > 20)) {
            $("#divmsg").dialog({
                autoOpen: false,
                draggable: false,
                resizable: false,
                autoResize: true,
                width: 300,
                height: 'auto',
                minHeight: 100,
                title: 'Message',
                dialogClass: 'noTitleStuff'
            });
            $(".ui-dialog-titlebar").hide();
            $('.panel-body').show();


            $('#btnok').off('click').on('click', function () {
                $("#divmsg").dialog('close');

            });

            HideLoading();
            $('#msgSpan').text('Your selection will result in plotting more than 20 items. Please change criteria & try again.');
            $("#divmsg").dialog('open');


            return false

        }
        else
            return true;
    }

}


var deftype = "";
function fordeficiencyPanel() {

    $('#btnGraph').off('click').on('click', function () {

        showDeficiencyGraph();
    });


    $('#btnSummary').off('click').on('click', function () {

        showDeficiencySummary();
    });

    $('#btnDetail').off('click').on('click', function () {

        showDeficiencyDetail();
    });


}



function showDeficiencyGraph() {
    $.get('../reportdata.aspx?con=check', function (data) {

        var flag = true;
        var data = JSON.parse(data);
        var speed = data.speed[0].speed;
        if (speed == 0) {
            $('#divnoConnect').show();
            CreateTimer();
            flag = false;
        }
        else if (speed < 50) {
            $('#divslow').show();
            flag = true;
        }

        if (flag) {
            Showloading();
            var id = $('#hdnreportid').val();
            var fromdate = '';
            var todate = '';
            var predata = $('#txtSelectedSearhfield').val();
            var daterange = $('#txtDateRange').val().split('-');
            if (daterange.length > 1) {
                fromdate = daterange[0].trim();
                todate = daterange[1].trim();
            }

            var deficiencycode = "";
            if (($('#txtcarrier').val() != "ALL DEFICIENCIES"))
                deficiencycode = $('#txtCarrierSearch').val();
            var providercode = "";
            providercode = $('#txtProviderSearch').val();

            deftype = "Graph";
            $('#divinnerchartContainer').empty();
            getinternalCahrtdata(id);
            $('#divnodata').hide();
            $('#divcomingsoon').hide();
            $('#divfinancialsummaryCover').hide();
            $('#divinnerchartContainerCover1').hide();
            $('#divinnerchartContainer2').hide();
            $('#divinnerchartContainerCover').show();
            $('#divGridPrint').hide();
            $('#divChartPrint').show();
            $('#Divchart').show();
            ExportChart(id);
            $('#divGraphToText').show();

            $('#btnGraph').removeClass('graph').addClass('activegraph');
            $('#btnDetail').removeClass('activedetail').addClass('detail')
            $('#btnSummary').removeClass('activesummary').addClass('summary');
            if (!iPadAgent && !iPhoneAgent && !AndroidAgent) {
                if (!$('.x-grid3-scroller').hasClass('nano'))
                    $('.x-grid3-scroller').addClass('nano')
                $('.x-grid3-scroller').css('overflow', 'hidden');
                $('.nano').perfectScrollbar().perfectScrollbar('destroy');
                $('.nano').perfectScrollbar();
                setTimeout("$('.nano').perfectScrollbar('update');", 300);
            }
            else
                $('.nano').perfectScrollbar('destroy');
            if ((navigator.appName == "Microsoft Internet Explorer") || (navigator.userAgent.indexOf("rv:11.0") > -1) || iPhoneAgent || iPadAgent || AndroidAgent) {
                noshowexport();
            }
        }
    }).fail(function () {
        $('#divnoConnect').show();
        if ($('#divnodata').is(':visible'))
            shownoNetwork();
        else
            CreateTimer();
    });
}


function showDeficiencySummary() {
    $.get('../reportdata.aspx?con=check', function (data) {

        var flag = true;
        var data = JSON.parse(data);
        var speed = data.speed[0].speed;
        if (speed == 0) {
            $('#divnoConnect').show();
            CreateTimer();
            flag = false;
        }
        else if (speed < 50) {
            $('#divslow').show();
            flag = true;
        }

        if (flag) {
            Showloading();
            var id = $('#hdnreportid').val();
            var fromdate = '';
            var todate = '';
            var predata = $('#txtSelectedSearhfield').val();
            var daterange = $('#txtDateRange').val().split('-');
            if (daterange.length > 1) {
                fromdate = daterange[0].trim();
                todate = daterange[1].trim();
            }
            var carrier = "";
            var deficiencycode = "";
            if (($('#txtcarrier').val() != "ALL DEFICIENCIES"))
                deficiencycode = $('#txtCarrierSearch').val();
            carrier = deficiencycode;
            var providercode = "";
            providercode = $('#txtProviderSearch').val();


            $('#btnGraph').removeClass('activegraph').addClass('graph');
            $('#btnDetail').removeClass('activedetail').addClass('detail')
            $('#btnSummary').removeClass('summary').addClass('activesummary');

            deftype = "Summary";
            $('#divfinancialsummary').empty();
            $('#divnodata').hide();
            $('#divcomingsoon').hide();
            $('#Divchart').show();
            $('#divfinancialsummary').html('');
            $('#divfinancialsummaryCover').show();
            $('#divinnerchartContainerCover').hide();
            $('#divGridPrint').show();
            $('#divChartPrint').hide();
            ExportAll();
            getReportName("", -1);

            var fid = $('#txtOverviewSearch').val();
            getDeficiencyRunsSummary(fid, id, fromdate, todate, deficiencycode, providercode, carrier);
            $('#divGraphToText').show();
            if (!iPadAgent && !iPhoneAgent && !AndroidAgent) {
                $('.nano').perfectScrollbar().perfectScrollbar('destroy');
                $('.nano').perfectScrollbar();
                setTimeout("$('.nano').perfectScrollbar('update');", 300);
            }
            else
                $('.nano').perfectScrollbar('destroy');

            if ((navigator.appName == "Microsoft Internet Explorer") || (navigator.userAgent.indexOf("rv:11.0") > -1) || iPadAgent || iPhoneAgent || AndroidAgent) {
                noshowexport();
            }
        }
    }).fail(function () {
        $('#divnoConnect').show();
        if ($('#divnodata').is(':visible'))
            shownoNetwork();
        else
            CreateTimer();
    });
}





function showDeficiencyDetail() {
    $.get('../reportdata.aspx?con=check', function (data) {

        var flag = true;
        var data = JSON.parse(data);
        var speed = data.speed[0].speed;
        if (speed == 0) {
            $('#divnoConnect').show();
            CreateTimer();
            flag = false;
        }
        else if (speed < 50) {
            $('#divslow').show();
            flag = true;
        }

        if (flag) {
            var fid = $('#txtOverviewSearch').val();

            if (fid.split(',').length == 1 || !isAllFacility) {
                Showloading();
                var id = $('#hdnreportid').val();
                var fromdate = '';
                var todate = '';
                var predata = $('#txtSelectedSearhfield').val();
                var daterange = $('#txtDateRange').val().split('-');
                if (daterange.length > 1) {
                    fromdate = daterange[0].trim();
                    todate = daterange[1].trim();
                }

                var deficiencycode = "";
                var carrier = "";
                if (($('#txtcarrier').val() != "ALL DEFICIENCIES"))
                    carrier = $('#txtCarrierSearch').val();

                var providercode = "";
                providercode = $('#txtProviderSearch').val();
                $('#btnGraph').removeClass('activegraph').addClass('graph');
                $('#btnDetail').removeClass('detail').addClass('activedetail')
                $('#btnSummary').removeClass('activesummary').addClass('summary');

                deftype = "Detail";
                $('#divfinancialsummary').empty();
                $('#divfinancialsummary').html('');
                $('#divnodata').hide();
                $('#divcomingsoon').hide();
                $('#Divchart').show();
                $('#divfinancialsummaryCover').show();
                $('#divinnerchartContainerCover1').hide();
                $('#divinnerchartContainer2').hide();
                $('#divinnerchartContainerCover').hide();
                $('#divGridPrint').show();
                $('#divChartPrint').hide();
                ExportAll();
                getReportName("", -1);


                getDeficiencyRunsDetail(fid, id, fromdate, todate, deficiencycode, providercode, carrier)
                $('#divGraphToText').show();
                if (!iPadAgent && !iPhoneAgent && !AndroidAgent) {
                    $('.nano').perfectScrollbar().perfectScrollbar('destroy');
                    $('.nano').perfectScrollbar();
                    setTimeout("$('.nano').perfectScrollbar('update');", 300);
                }
                else
                    $('.nano').perfectScrollbar('destroy');
                if ((navigator.appName == "Microsoft Internet Explorer") || (navigator.userAgent.indexOf("rv:11.0") > -1) || iPhoneAgent || iPadAgent || AndroidAgent) {
                    noshowexport();
                }
            }
            else {
                BindDialogBox();
                $("#divmsg").dialog('open');
                $('#msgSpan').text('Please select a single facility to view the detailed report.');
            }
        }
    }).fail(function () {
        $('#divnoConnect').show();
        if ($('#divnodata').is(':visible'))
            shownoNetwork();
        else
            CreateTimer();
    });
}



function ShowAnalysisByTransactionDateDrillDownReport(id, carrier, fromdate, todate, type, date) {
    $.get('../reportdata.aspx?con=check', function (data) {

        var flag = true;
        var data = JSON.parse(data);
        var speed = data.speed[0].speed;
        if (speed == 0) {
            $('#divnoConnect').show();
            CreateTimer();
            flag = false;
        }
        else if (speed < 50) {
            $('#divslow').show();
            flag = true;
        }

        if (flag) {


            var fid = $('#txtOverviewSearch').val();

            if (fid.split(',').length == 1 || !isAllFacility) {
                Showloading();
                if ($(window).width() < 650) {
                    $('#dgFilter').hide();
                    $('#divddlFilter').hide();
                }

                $('#divfinancialsummary').empty();
                $('#divfinancialsummaryCover').show();
                $('#divinnerchartContainerCover1').hide();
                $('#divinnerchartContainer2').hide();
                $('#divinnerchartContainerCover').hide();
                $('#divGridPrint').show();
                $('#divChartPrint').hide();
                ExportAll();
                getReportName(type, date);

                getAnalysisbyTransactionDate(fid, id, carrier, fromdate, todate, type, date);
                if (!iPadAgent && !iPhoneAgent && !AndroidAgent) {

                    if (!$('.x-grid3-scroller').hasClass('nano'))
                        $('.x-grid3-scroller').addClass('nano')
                    $('.x-grid3-scroller').css('overflow', 'hidden');
                    $('.nano').perfectScrollbar().perfectScrollbar('destroy');
                    $('.nano').perfectScrollbar();
                    setTimeout("$('.nano').perfectScrollbar('update');", 300);
                }
                else
                    $('.nano').perfectScrollbar('destroy');
                if ((navigator.appName == "Microsoft Internet Explorer") || (navigator.userAgent.indexOf("rv:11.0") > -1) || iPadAgent || iPhoneAgent || AndroidAgent) {
                    noshowexport();
                }
            }
            else {
                BindDialogBox();
                $("#divmsg").dialog('open');
                $('#msgSpan').text('Please select a single facility to view the detailed report.');
            }
        }
    }).fail(function () {
        $('#divnoConnect').show();
        if ($('#divnodata').is(':visible'))
            shownoNetwork();
        else
            CreateTimer();
    });
}



var locationType;
/////////// LOCATION CHARTS ////////////////    
function getLoactionCharts(data) {
    locationType = "chart";
    $('#lichart').addClass('activetab');
    $('#litext').removeClass('activetab');
    $('#divLocationCharts').empty();
    var divhtml = '';
    var dataarray = [];
    for (var key in data) {
        if (key != 'series')
            dataarray.push(key);
    }
    for (var i = 0; i < dataarray.length; i += 3) {
        if (i == dataarray.length - 3)
            divhtml += '<div class="locationchart" style="width:100%" id="divloc_' + i + '"></div>';
        else
            divhtml += '<div class="locationchart" id="divloc_' + i + '"></div>';
    }

    $('#divLocationCharts').html(divhtml);

    for (var i = 0; i < dataarray.length; i += 3) {
        var contrl = 'divloc_' + i;
        getpiechart(contrl, data[dataarray[i]], data[dataarray[i + 2]], true, false, 'number');
    }

    var height = 0;

    var lst = $('.locationchart');

    for (var i = 0; i < lst.length; i++) {
        if (i < lst.length - 1) {
            if (i % 2 == 0)
                height += $('.locationchart').height();
        }
    }
    $('#divLocationChartContainer').height(height + $('.locationchart').height() + 20 + $('#divLocationTitle').height());
}

////////// LOCATION GRID //////////////////////
function getLocationGrid() {
    locationType = "text";
    getReportName($('#ddlBranch option:selected').text(), -1);
    $('#divLocationGrid').empty();
    Showloading();
    $('#lichart').removeClass('activetab');
    $('#litext').addClass('activetab');
    var id = $('#hdnreportid').val();
    var fromdate = '';
    var todate = '';
    var predata = $('#ddlBranch').val() != null ? $('#ddlBranch').val() : "";
    var daterange = $('#txtDateRange').val().split('-');
    if (daterange.length > 1) {
        fromdate = daterange[0].trim();
        todate = daterange[1].trim();
    }

    var carrier = "";
    var arr = $('#txtCarrierSearch').val().split(',')

    if (arr[0] != "1" && ($('#txtcarrier').val() != "ALL CODES" && $('#txtcarrier').val() != "ALL PAYER GROUPS" && $('#txtcarrier').val() != "ALL CARRIERS"))
        carrier = $('#txtCarrierSearch').val();
    getLocationReport(id, carrier, fromdate, todate, predata);
}

////////////////////////////

function IPhoneStyle() {
    if (iPhoneAgent || AndroidAgent) {
        if ($(window).width() < 650 || $(window).height() < 650) {

            $('#dgFilter').append($('#divddlFilter'));
            $('#divfilterbtn').show();
            $('#dgFilter').hide();
            $('#help').addClass('helpIphone');
            $('#dgFilter').addClass('filter');

            var iteg = $('#help').find('.icon-question-sign');
            $('#help').empty();
            $('#help').append(iteg);
            var iteg1 = $('.reportcenter').find('.icon-list');
            $('.reportcenter').empty();
            $('.reportcenter').append(iteg1);

            $('.reportschooseblack i').css('font-size', '24px').css('color', '#fff');

            if ($(window).width() > 650) {
                $('.cancgo i').css('font-size', '16px');
            }

            // $('#dgFilter').dialog("close");
            $('.panel-body').show();
            $('#divfilterbtn').off('click').on('click', function () {
                setoffset();
                ShowddlSearchInBox();
                $('#dgFilter').css('left', 5).css('top', $(this).position().top + 31);

                if ($('#dgFilter').is(':visible'))
                    $('#divddlFilter').hide();
                else
                    $('#divddlFilter').show();

                if ($('#divnodata').is(':hidden')) {
                    $('#divddlFilter').show();
                }
                else
                    $('#divddlFilter').hide();
                $('#dgFilter').slideToggle('fast');
            });


            if (AndroidAgent) {
                if ($(window).width() > 650 || $(window).height() > 650) {
                    $('.reportcenter').width(0);
                    $('#divfilterbtn').css('margin-left', '100%');
                    $('.grayrc').hide();
                    if ($(window).width() > $(window).height()) {
                        iteg = $('#help').find('.icon-question-sign');
                        $('#help').empty();
                        $('#help').append(iteg).append('Help/Feedback');
                    }
                    else {
                        iteg = $('#help').find('.icon-question-sign');
                        $('#help').empty();
                        $('#help').append(iteg);
                    }
                }
            }

        }
    }
}





function ShowddlSearchInBox() {
    if (iPhoneAgent || AndroidAgent) {
        if ($(window).width() < 650 || $(window).height() < 650) {

	

            $('#divddlFilter').addClass('ddlFilter');
            var id = $('#hdnreportid').val();
            if (id == '18' || id == '20' || id == '22' || id == '23' || id == '25' || id == '30')
                $('#hrFilter').hide();
            else
                $('#hrFilter').show();

            if (id == '21' || id == '24' || id == '26'|| id == '31') {
                if ($('#divnodata').is(':hidden')) {
                    $('#dgFilter').css('height', ( isAllFacility==true?  '160px': '120px'));
                    if ($(window).width() < 650) {
                        $('#divGobtn').css('margin-top', ( isAllFacility==true?  '75px': '35px'));
                        $('#divGobtn').css('margin-left', '-15px');

			if (id == 31)
                        {
			    $('.ddlFilter > div:first').css('width', '220px');
                            $('#divProviderTreePanel').css('margin-top','5px')
		            $('#divGobtn').css('margin-left', '-15px');
                        }
                    }
                    else {
                        $('#divGobtn').css('margin-left', ( isAllFacility==true?  '40px': '0px'));
                    }
                }
                else {
                    $('#dgFilter').css('height', ( isAllFacility==true?  '80px': '40px'));

                }
            }
            else if (id == '28' || id == '27') {
                if ($('#divnodata').is(':hidden')) {
                    $('#dgFilter').css('height', '90px');
                    $('#divcarrierTreePanel').css('margin-right', '10px');
                    if ($(window).width() < 650) {
                        $('#divGobtn').css('margin-top', '0px');
                        $('#divGobtn').css('margin-left', '-20px');
                    }
                    else {
                        $('#divGobtn').css('margin-left', '0px');
                    }
                }
                else
                    $('#dgFilter').css('height', ( isAllFacility==true?  '80px': '40px'));
            }
            else
                $('#dgFilter').css('height',( isAllFacility==true?  '80px': '40px'));


	if(isAllFacility){
		$('.ddlFilter > div:first').css('width', '220px');
$('#divOverView').css('margin-bottom','5px')	;
}
            if (AndroidAgent) {
                if ($(window).height() > 650 || $(window).width() > 650) {
                    $('#txtcarrier').css({ 'margin-left': '', 'margin-right': '' });
                    $('#divCarrier .jqTransformInputWrapper').css('margin-left', '-5px').end().css('margin-left', '5px');
                }
            }

        }
    }
}

function showspeed() {
    var flag = true;
    $.get('../reportdata.aspx?con=check', function (data) {
        var data = JSON.parse(data);
        var speed = data.speed[0].speed;
        if (speed == 0) {
            $('#divnoConnect').show();
            CreateTimer();
            shownoNetwork();
            flag = false;
        }
        else if (speed < 50) {
            $('#divslow').show();
            flag = true;
        }

    }).done(function () {
    }).fail(function () {
        $('#divnoConnect').show();
        CreateTimer();
        shownoNetwork();
        flag = false;
    });
    return flag;
}


function FiscalClick() {
    $.get('../reportdata.aspx?con=check', function (data) {
        var flag = true;
        var data = JSON.parse(data);
        var speed = data.speed[0].speed;
        if (speed == 0) {
            $('#divnoConnect').show();
            CreateTimer();
            flag = false;
        }
        else if (speed < 50) {
            $('#divslow').show();
            flag = true;
        }

        if (flag) {
            window.location = "FiscalSetting.aspx";
        }
        else
            $('#aFiscal').attr("href", "javascript:;");
        return flag;
    }).done(function () {
    }).fail(function () {
        $('#divnoConnect').show();

        if ($('#divnodata').is(':visible'))
            shownoNetwork();
        else
            CreateTimer();
        $('#aFiscal').attr("href", "javascript:;");
        return false;
    });
}

function TargetClick() {
    $.get('../reportdata.aspx?con=check', function (data) {
        var flag = true;
        var data = JSON.parse(data);
        var speed = data.speed[0].speed;
        if (speed == 0) {
            $('#divnoConnect').show();
            CreateTimer();
            flag = false;
        }
        else if (speed < 50) {
            $('#divslow').show();
            flag = true;

        }

        if (flag) {
            window.location = "TargetSetting.aspx";

        }
        else
            $('#aTraget').attr("href", "javascript:;");
        return flag;
    }).done(function () {
    }).fail(function () {
        $('#divnoConnect').show();
        if ($('#divnodata').is(':visible'))
            shownoNetwork();
        else
            CreateTimer();
        $('#aTraget').attr("href", "javascript:;");
        return false;
    });
}


var varinterval;
function CreateTimer() {

    timecount = 60;
    clearInterval(varinterval);
    varinterval = setInterval("showtime()", 1000);


}
var timecount;
function showtime() {
    var count = timecount--;
    var hr;
    var min;
    var sec = 60;
    if (count > 3600) {
        hr = count / 3600;
        min = hr / 60;
        sec = hr % 60;
    }
    else if (count > 60) {
        min = count / 60;
        sec = count % 60;
    }
    else if (count < 60)
        sec = count;

    $('#spantimer').text(sec + 's...');
    if (count == 0) {
        Onclick_Go();
    }
}




////////////******* Function for Crolling *************////////////////////////////////////

function RemoveReadOnly() {
    $('#txtDateRange').prop('readonly', false);
    $('#txtDate').prop('readonly', false);
    $('#txtfrom').prop('readonly', false);
    $('#txtto').prop('readonly', false);
    $('#txtMonth').prop('readonly', false);
}

function ClearSearchTextbox() {
    $('#txtCarrierSearch').val("");
    $('#txtProviderSearch').val("");
}

function CreateAnalysisByTransactionDateTableForCrolling(data) {
    var fromdate = '';
    var todate = '';

    var carrier = "";
    var id = $('#hdnreportid').val();
    if (id == "20") {
        if ($('#txtMonth').val() != 'SELECT MONTH') {
            var monthdate = $('#txtMonth').val().split('-');
            if (monthdate.length > 1) {
                fromdate = monthdate[0];
                todate = monthdate[1];
            }
        }
    }

    try {

        for (var key in data) {
            if (data[key].month != undefined) {
                var tr = "<tr><td>" + data[key].month + "</td><td onclick='ShowAnalysisByTransactionDateDrillDownReport(" + id + ",\"" + carrier + "\",\"" + fromdate + "\",\"" + todate + "\",\"Charges\",\"" + data[key].month + "\");'>" + data[key].Charges + "</td>";
                tr += "<td onclick='ShowAnalysisByTransactionDateDrillDownReport(" + id + ",\"" + carrier + "\",\"" + fromdate + "\",\"" + todate + "\",\"Payments\",\"" + data[key].month + "\");'>" + data[key].Payments + "</td>";
                tr += "<td onclick='ShowAnalysisByTransactionDateDrillDownReport(" + id + ",\"" + carrier + "\",\"" + fromdate + "\",\"" + todate + "\",\"Adjustments\",\"" + data[key].month + "\");'>" + data[key].Adjustments + "</td></tr>";
                $("#tbodyATDCrolling").append(tr);
            }
        }
    }
    catch (e) {
        alert(e);
    }

}

function GetAnnualStaticArgumentCount(data) {
    var str = "";
    for (var key in data) {
        if (data[key].month != undefined) {
            if (key == 0)
                str = data[key].month
            if (str.indexOf(data[key].month) == -1)
                str = str + ',' + data[key].month

        }
    }

    var strSplit = str.split(',');
    $('#hdnASCAgrumentCount').val(strSplit.length);
}