﻿/*
This file is part of Ext JS 3.4

Copyright (c) 2011-2013 Sencha Inc

Contact:  http://www.sencha.com/contact

GNU General Public License Usage
This file may be used under the terms of the GNU General Public License version 3.0 as
published by the Free Software Foundation and appearing in the file LICENSE included in the
packaging of this file.

Please review the following information to ensure the GNU General Public License version 3.0
requirements will be met: http://www.gnu.org/copyleft/gpl.html.

If you are unsure which license is appropriate for your use, please contact the sales department
at http://www.sencha.com/contact.

Build date: 2013-04-03 15:07:25
*/
var grid;
var dlg;
var AndroidAgent = navigator.userAgent.match(/Android/i) != null;
var iPadAgent = navigator.userAgent.match(/iPad/i) != null;
var iPhoneAgent = navigator.userAgent.match(/iPhone/i) != null;
////////////////////////// Financial Summary ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function getfinancialsummary(fid, id, branch, carrier, fromdate, todate, predata) {
    var width = windowWidth < 400 ? 250 : 300
    dlg = $("#dialog").dialog({
        autoOpen: false,
        draggable: false,
        resizable: false,
        autoResize: true,
        width: width,
        height: 270,
        modal: true,
        show: {
            effect: "blind",
            duration: 1000
        },
        hide: {
            effect: "explode",
            duration: 1000
        }
    });



    //    Ext.onReady(function () {
    var SaleRecord = Ext.data.Record.create([
        { name: 'name', type: 'string' },
        { name: 'Value', type: 'string' },
        { name: 'month', type: 'string' },
        { name: 'Morder', type: 'int' },
        { name: 'grp', type: 'float' },
        { name: 'Colour', type: 'string' },
        { name: 'ForeColour', type: 'string' }
    //        { name: 'quantity', type: 'int' },
    //        { name: 'value', type: 'int' }
    ]);

    var myStore = new Ext.data.Store({
        proxy: new Ext.data.HttpProxy({
            method: 'GET',
            url: '../reportdata.aspx?sp=getGraphdata&fid=' + fid + '&id=' + id + '&branch=' + branch + '&carrier=' + carrier + '&FromDate=' + fromdate + '&toDate=' + todate + '&predata=' + predata,
            timeout: 90000
        }),
        autoLoad: true,
        reader: new Ext.data.JsonReader({
            root: 'Table',
            idProperty: id
        }, SaleRecord),
        listeners: {
            load: {
                fn: function () {
                    if (myStore.getCount() > 0) {
                        if (myStore.data.items[0].data.name.toLowerCase() != "under construction") {
                            $('.x-grid3-row-headers table tr').find('td:first').css('display', 'none');
                            $('.x-grid3-header-offset table').find('tr:first').css('display', 'none');
                            $('.x-grid3-scroller').addClass('nano');
                            HideLoading();
                            $('.x-grid3-header-title').height($('.x-grid3-header-inner').height());
                            if (!iPadAgent && !iPhoneAgent && !AndroidAgent) {
                                $('.nano').perfectScrollbar().perfectScrollbar('destroy');
                                $('.nano').perfectScrollbar();
                                setTimeout("$('.nano').perfectScrollbar('update');", 1000);
                            }
                            else
                                $('.nano').perfectScrollbar('destroy');

                            $('.x-grid3-cell-inner').css('padding', '0px');
                            $('.x-grid3-row-headers').css('font-weight', 'bold').css('text-align', 'left');



                            $.each($('.x-grid3-row-table'), function (index, item) {
                                $.each($('.x-grid3-row-headers table tr'), function (ind, itm) {
                                    if (index == ind) {
                                        $(item).find('.divfs').height($(itm).height() - 2);
                                    }

                                });

                            });

                            setTimeout("$('.x-pivotgrid').css('height', '100%');", 500);
                            // $('.x-grid3-scroller').css('height', GetInnerHeightAndWidthofClientBrowser(1, 58))
                            getGridJsScrollBar();
                        }
                        else
                            ShowUnderConstruction();

                    }
                    else
                        shownodata();
                }

            },
            exception: function (misc) {
                shownodata();
                //shownoNetwork();
            }

        }
    });

    var grid = new Ext.grid.PivotGrid({
        title: 'Financial Summary',
        header: false,
        id: 'FinancialSummary',
        width: document.getElementById('divfinancialsummary').style.width,
        height: GetInnerHeightAndWidthofClientBrowser(1, 58),
        renderTo: 'divfinancialsummary',
        store: myStore,
        aggregator: 'sum',
        measure: 'Value',
        viewConfig: {
            title: ''
        },
        aggregator: function (records, measure) {
            for (var i = 0; i < records.length; i++) {
                var func = "";
                var aach = "";
                var span = "";
                var height = 19;

                if (records[i].get("grp") <= 31 && records[i].get(measure) != "") {
                    func = "drildownreport('" + records[i].get("month") + "','" + records[i].get("grp") + "','" + records[i].get("name") + " (" + records[i].get("month") + ")','" + fromdate + "','" + todate + "');";
                    aach = '<a href="javascript:;" style="width:100%; cursor:pointer; color:' + records[i].get("ForeColour") + ';"  >' + records[i].get(measure) + '</a>';
                    var span = '<div class="divfs" title="' + records[i].get(measure) + '" style="font-weight:bold; width:100%; text-align:right; height:100%; color:' + records[i].get("ForeColour") + ' !important; background-color:' + records[i].get("Colour") + '; cursor:pointer;" onclick="' + func + '">' + aach + '</div>';
                }
                else {
                    aach = '<a href="javascript:;" style="width:100%; "  onclick="' + func + '">' + records[i].get(measure) + '</a>';
                    var span = '<div class="divfs" title="' + records[i].get(measure) + '"  style="font-weight:bold; width:100%; text-align:right; height:100%; color:' + records[i].get("ForeColour") + ' !important; background-color:' + records[i].get("Colour") + '; " onclick="' + func + '">' + aach + '</div>';
                }

                return span;

            }
        },



        leftAxis: [
            {
                dataIndex: 'grp',
                width: 0,
                direction: 'ASC',
                hidden: true


            },

            {
                dataIndex: 'name',
                width: GetInnerHeightAndWidthofClientBrowser(0, 15),

                sortable: false,
                renderer: function (value) {
                    return "<a href='#' onclick='alert(121);'>" + value + "</a>";
                    // return '<div style"font-weight:bold">'+ value+'</div>';
                }
            }

        ]
            ,

        topAxis: [
                        {
                            dataIndex: 'Morder',
                            hidden: true
                        },
                        {
                            dataIndex: 'month'
                            //width: GetInnerHeightAndWidthofClientBrowser(0, 15)
                        }]
    });
    grid.view.refresh(true);
    dlg.dialog("close");

}



////////////  for Set Height and width of the browser  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function GetInnerHeightAndWidthofClientBrowser(Reqtype, confPersent) {
    var myWidth;
    var myHeight;
    try {
        if (typeof (window.innerWidth) == 'number') {
            //Non-IE 
            myWidth = window.innerWidth;
            myHeight = window.innerHeight;
        } else if (document.documentElement && (document.documentElement.clientWidth || document.documentElement.clientHeight)) {
            //IE 6+ in 'standards compliant mode' 
            myWidth = document.documentElement.clientWidth;
            myHeight = document.documentElement.clientHeight;
        } else if (document.body && (document.body.clientWidth || document.body.clientHeight)) {
            //IE 4 compatible 
            myWidth = document.body.clientWidth;
            myHeight = document.body.clientHeight;
        }

        if (Reqtype == 0) {
            myWidth = Math.round((myWidth * confPersent) / 100);
            return myWidth;
        }
        else {
            myHeight = Math.round((myHeight * confPersent) / 100);
            return myHeight;
        }

    }
    catch (ex) {
        if (Reqtype == 0)
            return '585';
        else
            return '540';

    }
    finally {
        myWidth = null;
        myHeight = null;
    }


}



//Ext.EventManager.onWindowResize(function () {
//    // grid.setWidth(GetInnerHeightAndWidthofClientBrowser(0,80));
//});

////////////////// Dril-down Report of Financial Summary ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function drildownreport(month, grp, header, fromdate, todate) {

    $("#dialog").dialog({ title: header });
    dlg.dialog("open");
    $('.panel-header').show();
    $('#dialog').empty();
    var fid = $('#txtOverviewSearch').val();
    var store = new Ext.data.JsonStore({
        root: 'Table',
        autoLoad: false,
        fields: ['name', 'value'],
        proxy: new Ext.data.HttpProxy({
            method: 'GET',
            url: document.getElementById("ContentHolder_hdnProxyUrlTest").value + 'reportdata.aspx?sp=getDrilldownPmtReport&month=' + month + '&grp=' + grp + '&FromDate=' + fromdate + '&toDate=' + todate + '&fid=' + fid,
            timeout: 90000
        }),
        listeners: {
            load: {
                fn: function () {
                    //                    $('.x-grid3-scroller').perfectScrollbar('destroy');
                    //                    $('.x-grid3-scroller').perfectScrollbar();
                    //                    $('.x-grid3-scroller').css('overflow', 'hidden');
                    //  
                    if (!iPadAgent && !iPhoneAgent && !AndroidAgent) {
                        $('.nano').perfectScrollbar('destroy');
                        $('.nano').perfectScrollbar();
                        setTimeout("$('.nano').perfectScrollbar('update');", 1000);
                    }
                    else
                        $('.nano').perfectScrollbar('destroy');
                    if (store.getCount() == 0) {
                        $('#dialog').empty();
                        $('#dialog').append(' <img src="../images/DataNotFound2.jpg" alt="No Data Available" style="vertical-align: middle; margin:0 auto; display:table; height:100%"/>')
                    }
                }
            },
            exception: function (misc) {
                $('#dialog').empty();
                $('#dialog').append(' <img src="../images/DataNotFound2.jpg" alt="No Data Available" style="vertical-align: middle; margin:0 auto; display:table; height:100%"/>')
                //dlg.dialog("close");
                //shownodata();
            }
        }
    });

    // create the grid
    var popupgrid = new Ext.grid.GridPanel({
        store: store,
        id: header,
        columns: [
            {
                header: "Name",
                width: 150,
                dataIndex: 'name',
                sortable: false,
                renderer: function (value, metadata, record, rowIndex, colIndex, store) {
                    if (value.toString().indexOf('||') > -1)
                        metadata.style = 'text-align:right';
                    return value.replace('||', '');
                }
            },
            {
                header: "Value",
                width: 150,
                dataIndex: 'value',
                sortable: false,
                renderer: function (value, metadata, record, rowIndex, colIndex, store) {
                    if (value.toString().indexOf('||') > -1)
                        metadata.style = 'text-align:right';
                    return value.replace('||', '');
                }
            }

        ],

        renderTo: 'dialog',
        autoWidth: true,
        height: $('#dialog').height(),
        viewConfig: {
            forceFit: true,
            autoScroll: true
        },
        // width: $('#divfinancialsummary').width(),//GetInnerHeightAndWidthofClientBrowser(0,80),
        title: ''
        // plugins: [new Ext.ux.FitToParent("divfinancialsummary")]

    });

    store.load();
}