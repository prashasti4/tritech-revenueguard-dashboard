﻿/*
Extjs Grid for jefbar
*/
var grid;
var AndroidAgent = navigator.userAgent.match(/Android/i) != null;
var iPadAgent = navigator.userAgent.match(/iPad/i) != null;
var iPhoneAgent = navigator.userAgent.match(/iPhone/i) != null;
/////////////// DEFICIENCY RUNS (21)//////////////////////////////////////////////////////////////



// Show block details in grid according to block type from block type dropdown
function set_block_details(bid, isactive) {

    try {


        $('#divfinancialsummary12').html('');
        var store = new Ext.data.JsonStore({
            root: 'Table',
            autoLoad: false,
            fields: [{ name: 'id', type: 'int' }, { name: 'Loginid', type: 'string' }, { name: 'UserName', type: 'string' }, { name: 'IpAddress', type: 'string' }, { name: 'Attempt', type: 'int' }, { name: 'IsBlock', type: 'int' }, { name: 'IsBlockType', type: 'int' },
          { name: 'Createdon', type: 'string' }, { name: 'lastattempton', type: 'date', dateFormat: 'm/d/Y' }, { name: 'BlockType', type: 'string' }, { name: 'lastattempton1', type: 'string' }],

            proxy: new Ext.data.HttpProxy({
                method: 'GET',
                url: '../reportdata.aspx?UB=login_block_details&bid=' + bid + '&isactive=' + isactive
                , timeout: 90000
            }),
            listeners: {
                load: {
                    fn: function () {

                        if (store.getCount() == 0)
                            shownodata();

                        HideLoading();



                        $('.x-grid3-scroller, .x-grid3, .x-panel-body, .x-panel').height('auto');
                        $('#divfinancialsummary12 table').addClass('table');
                        //$('#id').css("width", ($('#id').width()) - 10 + "px");
                        //  $('#id').css("width", "1304px");
                        //getGridJsScrollBar();
                        $('.x-grid3-hd div').css("min-height", "");
                        $('.x-grid3-cell-inner img').css("cursor", "pointer");
                       


                    }
                },
                exception: function (misc) {
                    shownodata();
                    //shownoNetwork();
                }
            }

        });



        grid12 = new Ext.grid.GridPanel({
            store: store,
            id: 'id',
            columns: [
                {
                    header: "Username",
                    //  width: 150,
                    width: GetInnerHeightAndWidthofClientBrowser(0, 9),
                    dataIndex: 'UserName',
                    sortable: false,
                    align: 'left',
                    renderer: function (value, metadata, record, rowIndex, colIndex, store) {
                        metadata.attr = 'title="' + value + '"';
                        return value;


                    }

                },
                {
                    header: "IP Address",
                    // width: 200,
                    width: GetInnerHeightAndWidthofClientBrowser(0, 8),
                    dataIndex: 'IpAddress',
                    sortable: false,
                    align: 'left',
                    renderer: function (value, metadata, record, rowIndex, colIndex, store) {
                        metadata.attr = 'title="' + value + '"';
                        return value;

                    }

                },
                {
                    header: "Login Attempt",
                    // width: 200,
                    width: GetInnerHeightAndWidthofClientBrowser(0, 5),
                    dataIndex: 'Attempt',
                    sortable: false,
                    align: 'left',
                    renderer: function (value, metadata, record, rowIndex, colIndex, store) {
                        metadata.attr = 'title="' + value + '"';
                        return value;

                    }

                },
                {
                    header: "User Or IP Block",
                    //   width: 90,
                    width: GetInnerHeightAndWidthofClientBrowser(0, 7),
                    dataIndex: 'BlockType',
                    sortable: false,
                    align: 'left',
                    //fixed:true,
                    renderer: function (value, metadata, record, rowIndex, colIndex, store) {


                        metadata.attr = 'title="' + value + '"';
                        return value;


                    }
                },


                   {
                       header: "Attempt On",
                       //   width: 90,
                       width: GetInnerHeightAndWidthofClientBrowser(0, 15),
                       dataIndex: 'lastattempton1',
                       sortable: false,
                       align: 'left',

                       renderer: function (value, metadata, record, rowIndex, colIndex, store) {

                           metadata.attr = 'title="' + value + '"';
                           return value;

                       }

                   },

            {

                header: 'Block or Unblock',
                width: GetInnerHeightAndWidthofClientBrowser(0, 5),
                dataIndex: 'IsBlock',
                renderer: function (value, metadata, record, rowIndex, colIndex, store) {


                    if (value == 1) {
                        var anchor = '<input type="checkbox" class="chkbox" id="' + store.getAt(rowIndex).get("id") + '" name="' + "chk" + store.getAt(rowIndex).get("id") + '" checked>';
                        return anchor;
                    }
                    else {
                        var anchor = '<input type="checkbox" class="chkbox" id="' + store.getAt(rowIndex).get("id") + '" name="' + "chk" + store.getAt(rowIndex).get("id") + '">';
                        return anchor;

                    }


                }


            }

            ],



            renderTo: 'divfinancialsummary12',
            //autoWidth: true,
            overflowY: 'auto',
            viewConfig: {
                forceFit: true,
                scrollOffset: 0,
                emptyText: 'No Block Details.'

            },

            width: $('#divfinancialsummary12').width(), //GetInnerHeightAndWidthofClientBrowser(0,80),
            height: 300,
            //    title: modestr + ' Summary - ' + (Year == "" ? "2015" : Year),
            //header: true

        });

        store.load();
        $('.x-grid3-hd div').css("min-height", "31px");

    }
    catch (e) {

    }
}

function ScheduleReport() {

    $('#divfinancialsummary11').html('');
    var store = new Ext.data.JsonStore({
        root: 'Table',
        totalProperty: 'totalCount',
        autoLoad: false,
        fields: [{ name: 'report_id', type: 'int' }, { name: 'report_name', type: 'string' }, { name: 'emailid', type: 'string' }, { name: 'reportType', type: 'string' }, { name: 'emailto', type: 'string' },
      { name: 'createdon', type: 'string' }, { name: 'eventid', type: 'string' }, { name: 'startdate', type: 'string' }, { name: 'startdate1', type: 'string' }, { name: 'startime', type: 'string' }
      , { name: 'enddate', type: 'string' }, { name: 'enddate1', type: 'string' }, { name: 'endtime', type: 'string' }, { name: 'eventid', type: 'string' }, { name: 'timezone', type: 'string' }, { name: 'time_zone_name', type: 'string' }, { name: 'repeat', type: 'string' }, , { name: 'reportType1', type: 'string' }, { name: 'repeattypeid', type: 'string' }, { name: 'recursionrule', type: 'string' }, { name: 'summary', type: 'string' }, { name: 'emailto1', type: 'string' }, { name: 'rid', type: 'int' }, { name: 'isrepeat', type: 'int' }],

        proxy: new Ext.data.HttpProxy({
            method: 'GET',
            url: '../reportdata.aspx?sp=fetch_all_schedules'
            , timeout: 90000
        }),
        listeners: {
            load: {
                fn: function () {

                    if (store.getCount() == 0)
                        shownodata();

                    HideLoading();



                    $('.x-grid3-scroller, .x-grid3, .x-panel-body, .x-panel').height('auto');
                    $('#divfinancialsummary11 table').addClass('table');
                    //$('#id').css("width", ($('#id').width()) - 10 + "px");
                    //  $('#id').css("width", "1304px");
                    //getGridJsScrollBar();
                    $('.x-grid3-hd div').css("min-height", "");
                    $('.x-grid3-cell-inner img').css("cursor", "pointer");
                    $('.x-grid3-header-offset').css('width', ($('.x-grid3-header-inner').width()-3));
                    $('.x-grid3-header-offset table').css('width', ($('.x-grid3-header-inner').width()-3));
                    $('.x-grid3-body').css('width', ($('.x-grid3-header-inner').width()-3));
                    $('.x-grid3-row').css('width', ($('.x-grid3-header-inner').width()-3));
                    $('.x-grid3-row table').css('width', ($('.x-grid3-header-inner').width()-3));

                }
            },
            exception: function (misc) {
                shownodata();
                //shownoNetwork();
            }
        }

    });

    // create the grid
    grid = new Ext.grid.GridPanel({
        store: store,
        id: 'id',
        columns: [
            {
                header: "Report Name",
                //  width: 150,
                width: GetInnerHeightAndWidthofClientBrowser(0, 7),
                dataIndex: 'report_name',
                sortable: false,
                align: 'left',
                renderer: function (value, metadata, record, rowIndex, colIndex, store) {
                    metadata.attr = 'title="' + value + '"';
                    return value;

                    ////                    $.each($(".x-grid3-header thead td"), function (index, item) {
                    ////                        if (index == colIndex) {
                    ////                            if (!$.browser.chrome) {
                    ////                                metadata.style = 'text-align:right; width:' + ($(item).width() + 1) + 'px';
                    ////                            }
                    ////                            else {
                    ////                                //metadata.style = 'text-align:right;';
                    ////                            }
                    ////                        }
                    ////                    });

                    ////                    metadata.attr = 'title="' + value + '"';
                    ////                    return value;
                }
                //fixed:true 
            },
            {
                header: "Email To",
                // width: 200,
                width: GetInnerHeightAndWidthofClientBrowser(0, 16),
                dataIndex: 'emailto1',
                sortable: false,
                align: 'left',
                renderer: function (value, metadata, record, rowIndex, colIndex, store) {
                    metadata.attr = 'title="' + value + '"';
                    return value;
                    $.each($(".x-grid3-header thead td"), function (index, item) {
                        if (index == colIndex) {
                            if (!$.browser.chrome) {
                                metadata.style = 'text-align:right; width:' + ($(item).width()) + 'px';
                            }
                            else {
                                //metadata.style = 'text-align:right;';
                            }
                        }
                    });



                    //                    var val = format2(value);
                    //                    if (mode != 3) {
                    //                        val = '$' + Comma(formatcurrency(value));

                    //                    }

                    metadata.attr = 'title="' + value + '"';
                    return value;


                }

            },
            {
                header: "Report Type",
                //   width: 90,
                width: GetInnerHeightAndWidthofClientBrowser(0, 6),
                dataIndex: 'reportType1',
                sortable: false,
                align: 'left',
                //fixed:true,
                renderer: function (value, metadata, record, rowIndex, colIndex, store) {

                    //metadata.attr = 'title="' + "Excel" + '"';
                    //return "Excel";

                    metadata.attr = 'title="' + value + '"';
                    return value;

                    //                    $.each($(".x-grid3-header thead td"), function (index, item) {
                    //                        if (index == colIndex) {
                    //                            if (!$.browser.chrome) {
                    //                                metadata.style = ' width:' + ($(item).width() - 2) + 'px';
                    //                            }
                    //                            else {
                    //                                //metadata.style = 'text-align:right;';
                    //                            }
                    //                        }
                    //                    });
                    metadata.attr = 'title="' + value + '"';
                    return value;
                }
            },


               {
                   header: "Start Date",
                   //   width: 90,
                   width: GetInnerHeightAndWidthofClientBrowser(0, 5),
                   dataIndex: 'startdate1',
                   sortable: false,
                   align: 'left',
                   renderer: function (value, metadata, record, rowIndex, colIndex, store) {
                       metadata.attr = 'title="' + value + '"';
                       return value;
                       $.each($(".x-grid3-header thead td"), function (index, item) {
                           if (index == colIndex) {
                               if (!$.browser.chrome) {
                                   metadata.style = 'text-align:right; width:' + ($(item).width()) + 'px';
                               }
                               else {
                                   //metadata.style = 'text-align:right;';
                               }
                           }
                       });
                       metadata.attr = 'title="' + value + '"';


                       //                       var val = format2(value);
                       //                       if (mode != 3) {
                       //                           val = '$' + Comma(formatcurrency(value));
                       //                       }

                       metadata.attr = 'title="' + val + '"';
                       return value;
                   }

               },
                 {
                     header: "End Date",
                     //   width: 90,
                     width: GetInnerHeightAndWidthofClientBrowser(0, 5),
                     dataIndex: 'summary',
                     sortable: false,
                     align: 'left',
                     renderer: function (value, metadata, record, rowIndex, colIndex, store) {
                     var isrepeat = store.getAt(rowIndex).get('isrepeat');
                         var enddate = store.getAt(rowIndex).get('enddate1');

                         //    sp = 'delete_schedule&emailid=' + rec.get('emailid') + "&eventid=" + rec.get('eventid');
                         if (isrepeat == 0)
                             value = enddate;
                         metadata.attr = 'title="' + value + '"';
                         return value;
                         $.each($(".x-grid3-header thead td"), function (index, item) {
                             if (index == colIndex) {
                                 if (!$.browser.chrome) {
                                     metadata.style = 'text-align:right; width:' + ($(item).width()) + 'px';
                                 }
                                 else {
                                     //metadata.style = 'text-align:right;';
                                 }
                             }
                         });
                         metadata.attr = 'title="' + value + '"';


                         //                         var val = format2(value);
                         //                         if (mode != 3) {

                         //                             val = '$' + Comma(formatcurrency(value));
                         //                         }

                         //                         metadata.attr = 'title="' + val + '"';
                         return value;
                     }

                 },
                 {
                     header: "Timing",
                     //    width: 90,
                     width: GetInnerHeightAndWidthofClientBrowser(0, 5),
                     dataIndex: 'startime',
                     sortable: false,
                     align: 'left',
                     renderer: function (value, metadata, record, rowIndex, colIndex, store) {
                         metadata.attr = 'title="' + value + '"';
                         return value;
                         $.each($(".x-grid3-header thead td"), function (index, item) {
                             if (index == colIndex) {
                                 if (!$.browser.chrome) {
                                     metadata.style = 'text-align:right; width:' + ($(item).width()) + 'px';
                                 }
                                 else {
                                     //metadata.style = 'text-align:right;';
                                 }
                             }
                         });
                         metadata.attr = 'title="' + value + '"';


                         //                         var val = format2(value);
                         //                         if (mode != 3) {

                         //                             val = '$' + Comma(formatcurrency(value));
                         //                         }

                         //                         metadata.attr = 'title="' + val + '"';
                         return value;
                     }

                 },
                 {
                     header: "Repeats",
                     //   width: 100,
                     width: GetInnerHeightAndWidthofClientBrowser(0, 6),
                     dataIndex: 'repeat',
                     sortable: false,
                     align: 'left',
                     renderer: function (value, metadata, record, rowIndex, colIndex, store) {
                      var isrepeat = store.getAt(rowIndex).get('isrepeat');


                         //    sp = 'delete_schedule&emailid=' + rec.get('emailid') + "&eventid=" + rec.get('eventid');
                         if (isrepeat == 0)
                             value = "No Repeat";
                         metadata.attr = 'title="' + value + '"';
                         return value;
                         $.each($(".x-grid3-header thead td"), function (index, item) {
                             if (index == colIndex) {
                                 if (!$.browser.chrome) {
                                     metadata.style = 'text-align:right; width:' + ($(item).width()) + 'px';
                                 }
                                 else {
                                     //metadata.style = 'text-align:right;';
                                 }
                             }
                         });
                         metadata.attr = 'title="' + value + '"';


                         //                         var val = format2(value);
                         //                         if (mode != 3) {

                         //                             val = '$' + Comma(formatcurrency(value));
                         //                         }

                         //                         metadata.attr = 'title="' + val + '"';
                         return value;
                     }

                 },

             {
                 header: "Time Zone",
                 //     width: 100,
                 width: GetInnerHeightAndWidthofClientBrowser(0, 10),
                 dataIndex: 'time_zone_name',
                 sortable: false,
                 align: 'left',
                 renderer: function (value, metadata, record, rowIndex, colIndex, store) {
                     metadata.attr = 'title="' + value + '"';
                     return value;
                     $.each($(".x-grid3-header thead td"), function (index, item) {
                         if (index == colIndex) {
                             if (!$.browser.chrome) {
                                 metadata.style = 'text-align:right; width:' + ($(item).width() - 1) + 'px';
                             }
                             else {
                                 //metadata.style = 'text-align:right;';
                             }
                         }
                     });
                     metadata.attr = 'title="' + value + '"';


                     ////                     var val = format2(value);
                     ////                     if (mode != 3) {
                     ////                         val = '$' + Comma(formatcurrency(value));
                     ////                     }

                     //    metadata.attr = 'title="' + val + '"';
                     return value;
                 }

             },
             {
                 header: "Created Date",
                 //   width: 100,
                 width: GetInnerHeightAndWidthofClientBrowser(0, 5),
                 dataIndex: 'createdon',
                 sortable: false,
                 align: 'left',
                 renderer: function (value, metadata, record, rowIndex, colIndex, store) {
                     metadata.attr = 'title="' + value + '"';
                     return value;
                     $.each($(".x-grid3-header thead td"), function (index, item) {
                         if (index == colIndex) {
                             if (!$.browser.chrome) {
                                 metadata.style = 'text-align:right; width:' + ($(item).width() - 1) + 'px';
                             }
                             else {
                                 //metadata.style = 'text-align:right;';
                             }
                         }
                     });
                     metadata.attr = 'title="' + value + '"';


                     ////                     var val = format2(value);
                     ////                     if (mode != 3) {
                     ////                         val = '$' + Comma(formatcurrency(value));
                     ////                     }

                     //    metadata.attr = 'title="' + val + '"';
                     return value;
                 }

             },
        {

            header: "Edit",
            xtype: 'actioncolumn',
            //  width: 40,
            width: GetInnerHeightAndWidthofClientBrowser(0, 4),
            tdCls: 'edit',
            items: [{
                icon: '../images/Edit.png',  // Use a URL in the icon config
                tooltip: 'Edit',
                handler: function (grid, rowIndex, colIndex) {

                    grid_edit(grid, rowIndex);

                }
            }]
        },
        {

            header: "Delete",
            xtype: 'actioncolumn',
            //   width: 40,
            width: GetInnerHeightAndWidthofClientBrowser(0, 4),
            tdCls: 'delete',
            items: [{
                icon: '../images/Delete.png',  // Use a URL in the icon config
                tooltip: 'Delete',
                handler: function (grid, rowIndex, colIndex) {


                    jConfirm('Are you sure you want to delete this schedule?', 'Confirmation', function (r) {
                        if (r) {
                            var rec = grid.getStore().getAt(rowIndex);

                            sp = 'delete_schedule&emailid=' + rec.get('emailid') + "&eventid=" + rec.get('eventid');

                            $.get('../reportdata.aspx?sp=' + sp, function (data) {

                            });
                            grid.getStore().remove(rec);
                            clearcontrol();
                        }

                    });

                }
            }]
        }


        ],



        renderTo: 'divfinancialsummary11',
        //autoWidth: true,
        overflowY: 'auto',
        viewConfig: {
            forceFit: true,
            scrollOffset: 0,
            emptyText: 'No schedules available.'

        },
        listeners: {
            // Grid Row Double Click Event
            'rowdblclick': function (grid, rowIndex, e) {

                grid_edit(grid, rowIndex);


            }
        },
        width: $('#divfinancialsummary11').width() - 10, //GetInnerHeightAndWidthofClientBrowser(0,80),
        height: 300,
        //    title: modestr + ' Summary - ' + (Year == "" ? "2015" : Year),
        //header: true
        bbar: new Ext.PagingToolbar({
                pageSize: 10,
                store: store,
                displayInfo: true,
                displayMsg: 'Displaying {0} - {1} of {2}',
                emptyMsg: "",
                items: [
                    '-', {
                        pressed: true,
                        enableToggle: true,
                        //text: 'Show Preview',
                        cls: 'x-btn-text-icon details',
                        toggleHandler: function (btn, pressed) {
                            var view = grid.getView();
                            view.showPreview = pressed;
                            view.refresh();
                        }
                    }]
            }),

            tbar: new Ext.PagingToolbar({
                pageSize: 10,
                store: store,
              //  displayInfo: true,
              //  displayMsg: 'Displaying {0} - {1} of {2}',
               // emptyMsg: "",
                items: [

                   'Quick Search :',
            {
                pressed: true,
                enableToggle: true,
                text: 'search',
                id: 'searchdashboard',
                xtype: 'textfield',
                fieldStyle: 'text-align: right;',
                cls: 'x-btn-text-icon details',
                enableKeyEvents: true,
                listeners: {
                    'keyup': function (field, event) {
                        var searchval = Ext.getCmp('searchdashboard').getValue();
                        searchval = searchval.toString().toLowerCase();
                        searchTable(searchval);

                    }
                }

            }
                ]
            })

    });

    grid.render('divfinancialsummary11');
        store.load({ params: { start: 0, limit: 10 } });
    $('.x-grid3-hd div').css("min-height", "31px");


}
// Call from double click event,Edit and delete button click of Grid
// Confirmation Dialog
function grid_edit(grid, rowIndex) {
    try {

        if ($('#txtEmail').val() != '') {
            // Confirmation Message when email field is not blank
            jConfirm('You may lose changes by navigating away. Please press OK to continue or Cancel to keep editing.', 'Confirmation', function (r) {
                if (r) {
                    fillDataonEditgrid(grid, rowIndex);
                }
            });
        }
        else {
            fillDataonEditgrid(grid, rowIndex);
            //                    // when email field is blank
            //                    $('#ddlReprot').val(report_id);
            //                    $('#txtEmail').val(emailto);
            //                    $('#ddlextension').val(reportType.toString().toLowerCase());
            //                    $('#startdate').val(startdate);
            //                    $('#starttime').val(startime);
            //                    $('#enddate').val(enddate);
            //                    $('#endtime').val(endtime);
            //                    $('#hdnEventid').val(eventid);
            //                    $('#ddlTimeZone').val(timezone);

            //                    $('#lbl_id').val(emailid);
            //                    $('#btn_save').val('Update');
        }
    }
    catch (e) {

    }
}

var ismatchdata;
function searchTable(inputVal) {
    try {


        var table = $('.x-grid3-row-table');
        table.find('tr').each(function (index, row) {
            var allCells = $(row).find('td');
            if (allCells.length > 0) {
                var found = false;
                allCells.each(function (index, td) {
                    var regExp = new RegExp(inputVal, 'i');
                    if (regExp.test($(td).text())) {
                        found = true;
                        return false;
                    }

                });
                if (found == true) {

                    $(row).closest('div').show();
                    // $(row).show();
                    //   alert("1");

                    if ($('#divv').length > 0) {
                        $('#divv').remove();
                    }

                    //                        $('#divv').remove();


                }
                else {
                    // $(row).hide();
                    $(row).closest('div').hide();
                    if ($('.x-grid3-row').css('display') == 'block') {
                        if ($('#divv').length > 0) {
                            $('#divv').remove();
                        }


                    }
                    else {
                        if ($('#divv').length > 0) {
                            $('#divv').remove();
                        }

                        //                        $('#divv').remove();

                    }
                    //$('.x-grid3-body').append('<div id="divv" style="text-align:center;">No Matching Details</div>');

                }
            }
        });

        $('.x-grid3-row').each(function (index, row) {

            if ($('#divv').length > 0) {
                $('#divv').remove();
            }

            if ($('.x-grid3-row')[index].style.display == 'none') {
                ismatchdata = false;
            }
            else {
                ismatchdata = true;
                return false;
            }

        });
        if (!ismatchdata) {
            $('.x-grid3-body').append('<div id="divv" style="text-align:center;  border: 1px solid #ddd;">No Matching Details</div>');
        }
        else {
            if ($('#divv').length > 0) {
                $('#divv').remove();
            }
        }

    }
    catch (e) {
        alert(e);
    }
}

// Add grid data to form controls
function fillDataonEditgrid(grid, rowIndex) {
    try {
        var store = grid.getStore();
        var value = store.getAt(rowIndex).get('emailid');
        // alert(value);
        var report_id = store.getAt(rowIndex).get('report_id');
        var emailto = store.getAt(rowIndex).get('emailto');
        var reportType = store.getAt(rowIndex).get('reportType1');
        var startdate = store.getAt(rowIndex).get('startdate1');
        var startime = store.getAt(rowIndex).get('startime');
        var enddate = store.getAt(rowIndex).get('enddate1');
        var endtime = store.getAt(rowIndex).get('endtime');
        var emailid = store.getAt(rowIndex).get('emailid');
        var eventid = store.getAt(rowIndex).get('eventid');
        var timezone = store.getAt(rowIndex).get('timezone');
        var rid = store.getAt(rowIndex).get('rid');


        $('#ddlextension').empty();
        $.get('../reportdata.aspx?sp=fetch_master_details&rid=' + report_id, function (data) {
            var data = JSON.parse(data);

            report_type_master(data.Table1);
            $('#ddlextension').val(rid);


        });

        $('#ddlReprot').val(report_id);
        $('#txtEmail').val(emailto);

        $('#startdate').val(startdate);
        $('#starttime').val(startime);
        $('#enddate').val(enddate);
        $('#endtime').val(endtime);
        $('#hdnEventid').val(eventid);
        $('#ddlTimeZone').val(timezone);

        $('#lbl_id').val(emailid);
        $('#btn_save').val('Update');
        $('#btn_clear').val('Cancel');
        if (store.getAt(rowIndex).get('repeattypeid') != "-1") {
            $('#ddlrepeat').val(store.getAt(rowIndex).get('repeattypeid'));
            SetRepeatDropdownonready($('#ddlrepeat'));
            SetValuebasedonRecursionRule(store.getAt(rowIndex).get('recursionrule'));
            setTimeout("SetSummary()", 2000);
            setTimeout("setRepeatSummary()", 2000);

            
        }
    }
    catch (ex) {

    }
}

function formatcurrency(total) {

    try {
        if (total == "") {
            return "0.00"
        }
        var neg = false;
        if (total < 0) {
            neg = true;
            total = Math.abs(total);
        }
        return (neg ? "" : '') + parseFloat(total, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString();
    }
    catch (e) {
        return '';
    }
}
function format2(nStr) {
    //return "" + " " + n.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,");
    if (nStr == "") {
        return "0"
    }
    nStr += '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}

// Adjust grid columns height and width according to screen resolution
function GetInnerHeightAndWidthofClientBrowser(Reqtype, confPersent) {
    var myWidth;
    var myHeight;
    try {
        if (typeof (window.innerWidth) == 'number') {
            //Non-IE 
            myWidth = window.innerWidth;
            myHeight = window.innerHeight;
        } else if (document.documentElement && (document.documentElement.clientWidth || document.documentElement.clientHeight)) {
            //IE 6+ in 'standards compliant mode' 
            myWidth = document.documentElement.clientWidth;
            myHeight = document.documentElement.clientHeight;
        } else if (document.body && (document.body.clientWidth || document.body.clientHeight)) {
            //IE 4 compatible 
            myWidth = document.body.clientWidth;
            myHeight = document.body.clientHeight;
        }

        // alert(myWidth);
        if (Reqtype == 0) {
            myWidth = Math.round((myWidth * confPersent) / 100);
            return myWidth;
        }
        else {
            myHeight = Math.round((myHeight * confPersent) / 100);
            return myHeight;
        }

    }
    catch (ex) {
        if (Reqtype == 0)
            return '585';
        else
            return '540';

    }
    finally {
        myWidth = null;
        myHeight = null;
    }


}


function getDeficiencyRunsSummary(fid, id, fromdate, todate, deficiencycode, providercode, carrier) {
    var store = new Ext.data.JsonStore({
        root: 'Table',
        autoLoad: false,
        fields: [{ name: 'Deficiency Code', type: 'string' }, { name: 'Deficiency', type: 'string' }, { name: 'Deficiency Count', type: 'string' },
        { name: 'Charges', type: 'string' }, { name: 'Provider', type: 'string' }],
        proxy: new Ext.data.HttpProxy({
            method: 'GET',
            url: document.getElementById("ContentHolder_hdnProxyUrlTest").value + 'reportdata.aspx?sp=getGrossDeficiencyTextSummary&defType=Summary&fid=' + fid + '&id=' + id + '&FromDate=' + fromdate + '&toDate=' + todate + '&carrier=' + carrier + '&deficiencycode=' + deficiencycode + '&providercode=' + providercode,
            timeout: 90000
        }),
        listeners: {
            load: {
                fn: function () {

                    if (store.getCount() == 0)
                        shownodata();

                    HideLoading();
                    getGridJsScrollBar();

                }
            },
            exception: function (misc) {
                shownodata();
                //shownoNetwork();
            }
        }

    });

    // create the grid
    grid = new Ext.grid.GridPanel({
        store: store,
        id: 'id',
        columns: [
        //            {
        //                header: "Deficiency Code",
        //                width: GetInnerHeightAndWidthofClientBrowser(0, 20),
        //                dataIndex: 'Deficiency Code',
        //                sortable: false,
        //                renderer: function (value, metadata, record, rowIndex, colIndex, store) {
        //                    $.each($(".x-grid3-header thead td"), function (index, item) {
        //                        if (index == colIndex) {
        //                            metadata.style = ' width:' + $(item).width() + 'px';
        //                        }
        //                    });
        //                    metadata.attr = 'title="' + value + '"';
        //                    var func = "ShowDeficiencyDetailbyCode('" + store.getAt(rowIndex).get('Deficiency') + "')";

        //                    var anchor = '<div style="width:100%; font-weight:bold; cursor:pointer;" onclick="' + func + '"  >' + value + '</div>';

        //                    return anchor;
        //                }
        //                //fixed:true
        //            },
            {
                header: "Deficiency",
                width: GetInnerHeightAndWidthofClientBrowser(0, 30),
                dataIndex: 'Deficiency',
                sortable: false,
                renderer: function (value, metadata, record, rowIndex, colIndex, store) {
                    $.each($(".x-grid3-header thead td"), function (index, item) {
                        if (index == colIndex) {
                            metadata.style = ' width:' + $(item).width() + 'px';
                        }
                    });
                    metadata.attr = 'title="' + value + '"';
                    var func = "ShowDeficiencyDetailbyCode('" + value + "')";

                    var anchor = '<div style="width:100%; font-weight:bold; cursor:pointer;" onclick="' + func + '"  >' + value + '</div>';

                    return anchor;
                }
                //fixed:true 
            },
            {
                header: "Deficiency Count",
                width: GetInnerHeightAndWidthofClientBrowser(0, 10),
                dataIndex: 'Deficiency Count',
                sortable: false,
                //fixed:true,
                renderer: function (value, metadata, record, rowIndex, colIndex, store) {
                    $.each($(".x-grid3-header thead td"), function (index, item) {
                        if (index == colIndex) {
                            metadata.style = 'width:' + $(item).width() + 'px';
                        }
                    });
                    metadata.attr = 'title="' + value + '"';
                    return value;
                }
            },
            {
                header: "Charges",
                width: GetInnerHeightAndWidthofClientBrowser(0, 20),
                dataIndex: 'Charges',
                sortable: false,
                renderer: function (value, metadata, record, rowIndex, colIndex, store) {
                    $.each($(".x-grid3-header thead td"), function (index, item) {
                        if (index == colIndex) {
                            metadata.style = 'text-align:right; width:' + $(item).width() + 'px';
                        }
                    });
                    metadata.attr = 'title="' + value + '"';
                    return value;
                }
                //                fixed:true
            }
        //            ,{
        //                header: "Provider",
        //                width: GetInnerHeightAndWidthofClientBrowser(0, 20),
        //                dataIndex: 'Provider',
        //                sortable: false,
        //                renderer: function (value, metadata, record, rowIndex, colIndex, store) {
        //                    $.each($(".x-grid3-header thead td"), function (index, item) {
        //                        if (index == colIndex) {
        //                            metadata.style = 'text-align:right; width:' + $(item).width() + 'px';
        //                        }
        //                    });
        //                    metadata.attr = 'title="' + value + '"';
        //                    return value;
        //                }
        //                //                fixed:true
        //            }
        ],



        renderTo: 'divfinancialsummary',
        autoWidth: true,
        viewConfig: { forceFit: true },
        // width: $('#divfinancialsummary').width(),//GetInnerHeightAndWidthofClientBrowser(0,80),
        height: GetInnerHeightAndWidthofClientBrowser(1, 60),
        title: '',
        header: false
        // plugins: [new Ext.ux.FitToParent("divfinancialsummary")]

    });
    store.load();


}

function getDeficiencyRunsDetail(fid, id, fromdate, todate, deficiencycode, providercode, carrier) {

    try {
        var store = new Ext.data.JsonStore({
            root: 'Table',
            autoLoad: false,
            fields: [{ name: 'Run Number', type: 'string' }, { name: 'DOS', type: 'string' }, { name: 'Charges', type: 'string' }, { name: 'Provider', type: 'string' },
        { name: 'Denial Code-Reason', type: 'string' }, { name: 'Deficiency', type: 'string' }, { name: 'ordr', type: 'int' }, { name: 'MrgCell', type: 'int' }],
            proxy: new Ext.data.HttpProxy({
                method: 'GET',
                url: document.getElementById("ContentHolder_hdnProxyUrlTest").value + 'reportdata.aspx?sp=getDeficiencyRuns&defType=detail&fid=' + fid + '&id=' + id + '&FromDate=' + fromdate + '&toDate=' + todate + '&carrier=' + carrier + '&deficiencycode=' + deficiencycode + '&providercode=' + providercode,
                timeout: 90000
            }),
            listeners: {
                load: {
                    fn: function () {
                        if (store.getCount() == 0)
                            shownodata();
                        HideLoading();
                        getGridJsScrollBar()
                    }
                },
                exception: function (misc) {
                    shownodata();
                    // shownoNetwork();
                }
            }

        });

        // create the grid
        grid = new Ext.grid.GridPanel({
            store: store,
            id: 'id',
            columns: [

            {
                header: "Run Number",
                width: GetInnerHeightAndWidthofClientBrowser(0, 20),
                dataIndex: 'Run Number',
                sortable: false,
                renderer: function (value, meta, record, rowIndex, colIndex, store) {
                    $.each($(".x-grid3-header thead td"), function (index, item) {
                        if (index == colIndex) {
                            meta.style = 'width:' + $(item).width() + 'px';
                        }
                    });
                    if (store.getAt(rowIndex).get('MrgCell') == 0 && store.getAt(rowIndex).get('ordr') > 1) {
                        if (colIndex == 0) {
                            width = '';
                            $.each($(".x-grid3-header thead td"), function (index, item) {
                                if (index > colIndex)
                                    width += $(item).width();

                            });


                            if (store.getAt(rowIndex).get('ordr') == 2) {
                                meta.style = 'border-top:1px solid #000; width:100%; font-weight:bold;';
                                meta.id = 'deficiencytotal';
                            }
                            else {
                                meta.style = 'width:100%;';
                                meta.id = 'deficiencypertotal';
                            }
                        }
                        else if (colIndex > 1) {
                            meta.style = 'width:0px; display:none;';
                        }

                    }
                    meta.attr = 'title="' + value + '"';
                    return value;
                }
                //fixed:true
            },
            {
                header: "DOS",
                width: GetInnerHeightAndWidthofClientBrowser(0, 10),
                dataIndex: 'DOS',
                sortable: false,
                renderer: function (value, meta, record, rowIndex, colIndex, store) {

                    $.each($(".x-grid3-header thead td"), function (index, item) {
                        if (index == colIndex) {
                            meta.style = 'width:' + $(item).width() + 'px';
                        }
                    });
                    if (store.getAt(rowIndex).get('MrgCell') == 0 && store.getAt(rowIndex).get('ordr') > 1) {
                        if (colIndex == 0) {
                            width = '';
                            $.each($(".x-grid3-header thead td"), function (index, item) {
                                if (index > colIndex)
                                    width += $(item).width();

                            });
                            meta.style = 'width:' + width + 'px;'
                        }
                        else if (colIndex > 0) {
                            meta.style = 'width:0px; display:none; '
                        }
                    }
                    meta.attr = 'title="' + value + '"';
                    return value;
                }
                //fixed:true 
            },
            {
                header: "Charges",
                width: GetInnerHeightAndWidthofClientBrowser(0, 10),
                dataIndex: 'Charges',
                sortable: false,
                //fixed:true,
                renderer: function (value, meta, record, rowIndex, colIndex, store) {
                    $.each($(".x-grid3-header thead td"), function (index, item) {
                        if (index == colIndex) {
                            meta.style = 'text-align:right; width:' + $(item).width() + 'px';
                        }
                    });

                    if (store.getAt(rowIndex).get('MrgCell') == 0 && store.getAt(rowIndex).get('ordr') > 1) {
                        if (colIndex == 0) {
                            width = '';
                            $.each($(".x-grid3-header thead td"), function (index, item) {
                                if (index > colIndex)
                                    width += $(item).width();
                            });
                            if (rowIndex > 0) {
                                if (store.getAt(rowIndex - 1).get('ordr') != store.getAt(rowIndex).get('ordr'))
                                    meta.style = 'border-top:1px solid #000; width:' + width + 'px;'
                                else
                                    meta.style = 'width:' + width + 'px;'
                            }
                        }
                        else if (colIndex > 0) {
                            meta.style = 'width:0px; display:none;'
                        }
                    }
                    meta.attr = 'title="' + value + '"';
                    return value;
                }
            },
            {
                header: "Denial Code-Reason",
                width: GetInnerHeightAndWidthofClientBrowser(0, 30),
                dataIndex: 'Denial Code-Reason',
                sortable: false,
                renderer: function (value, meta, record, rowIndex, colIndex, store) {
                    $.each($(".x-grid3-header thead td"), function (index, item) {
                        if (index == colIndex) {
                            meta.style = 'width:' + $(item).width() + 'px';
                        }
                    });
                    if (store.getAt(rowIndex).get('MrgCell') == 0 && store.getAt(rowIndex).get('ordr') > 1) {
                        if (colIndex == 0) {
                            width = '';
                            $.each($(".x-grid3-header thead td"), function (index, item) {
                                if (index > colIndex)
                                    width += $(item).width();

                            });



                            meta.style = 'width:' + width + 'px;'
                        }
                        else if (colIndex > 0) {
                            meta.style = 'width:0px; display:none;'
                        }
                    }

                    meta.attr = 'title="' + value + '"';
                    return value;
                }
                //                fixed:true
            },
            //             {
            //                 header: "Deficiency",
            //                 width: GetInnerHeightAndWidthofClientBrowser(0, 10),
            //                 dataIndex: 'Deficiency',
            //                 sortable: false,
            //                 hidden: false,
            //                 renderer: function (value, meta, record, rowIndex, colIndex, store) {
            //                     $.each($(".x-grid3-header thead td"), function (index, item) {
            //                         if (index == colIndex) {
            //                             meta.style = 'width:' + $(item).width() + 'px';
            //                         }
            //                     });
            //                     if (store.getAt(rowIndex).get('MrgCell') == 0 && store.getAt(rowIndex).get('ordr') > 1) {
            //                         if (colIndex == 0) {
            //                             width = '';
            //                             $.each($(".x-grid3-header thead td"), function (index, item) {
            //                                 if (index > colIndex)
            //                                     width += $(item).width();
            //                                 
            //                             });
            //                             meta.style = 'width:' + width + 'px;'
            //                         }
            //                         else if (colIndex > 0) {
            //                             meta.style = 'display:none; width:0px'
            //                         }
            //                     }

            //                     meta.attr = 'title="' + value + '"';
            //                     return value;
            //                 }
            //             },
            {
                header: "Provider",
                width: GetInnerHeightAndWidthofClientBrowser(0, 10),
                dataIndex: 'Provider',
                sortable: false,
                hidden: false,
                renderer: function (value, meta, record, rowIndex, colIndex, store) {
                    $.each($(".x-grid3-header thead td"), function (index, item) {
                        if (index == colIndex) {
                            meta.style = 'width:' + $(item).width() + 'px';
                        }
                    });
                    if (store.getAt(rowIndex).get('MrgCell') == 0 && store.getAt(rowIndex).get('ordr') > 1) {
                        if (colIndex == 0) {
                            width = '';
                            $.each($(".x-grid3-header thead td"), function (index, item) {
                                if (index > colIndex)
                                    width += $(item).width();
                            });
                            meta.style = 'width:100%;'
                        }
                        else if (colIndex > 0) {
                            meta.style = 'display:none; width:0px'
                        }
                    }
                    meta.attr = 'title="' + value + '"';
                    return value;
                }
            }
            ],



            renderTo: 'divfinancialsummary',
            autoWidth: true,
            viewConfig: { forceFit: true },
            // width: $('#divfinancialsummary').width(),//GetInnerHeightAndWidthofClientBrowser(0,80),
            height: GetInnerHeightAndWidthofClientBrowser(1, 60),
            title: 'DEFICIENCY RUNS',
            header: false
            // plugins: [new Ext.ux.FitToParent("divfinancialsummary")]

        });
        store.load();

    }
    catch (e) {
        shownodata();
    }
}

/////////////// ANSI DENIAL LOG BY LINE (23)//////////////////////////////////////////////////////////////
function getANSIDentalLogbyLine(fid, id, branch, carrier, fromdate, todate, predata, deficiencycode, providercode) {

    try {

        var store = new Ext.data.JsonStore({
            root: 'Table',
            autoLoad: false,
            fields: [{ name: 'Run Number', type: 'string' }, { name: 'DOS', type: 'string' }, { name: 'Payer', type: 'string' }, { name: 'Charges', type: 'string' }, { name: 'Provider', type: 'string' },
        { name: 'Denial Code-Reason', type: 'string' }, { name: 'Denial Code', type: 'string' }, { name: 'HCPCS Code', type: 'string' }, { name: 'ordr', type: 'int' }, { name: 'MrgCell', type: 'int' }],
            proxy: new Ext.data.HttpProxy({
                method: 'GET',
                url: document.getElementById("ContentHolder_hdnProxyUrlTest").value + 'reportdata.aspx?sp=getGraphdata&fid=' + fid + '&id=' + id + '&branch=' + branch + '&carrier=' + carrier + '&FromDate=' + fromdate + '&toDate=' + todate + '&predata=' + predata + '&deficiencycode=' + deficiencycode + '&providercode=' + providercode,
                timeout: 90000
            }),
            listeners: {
                load: {
                    fn: function () {
                        if (store.getCount() == 0)
                            shownodata();

                        HideLoading();
                        getGridJsScrollBar()


                    }
                },
                exception: function (proxy, response, operation) {
                    HideLoading();

                    shownodata();
                    //shownoNetwork();
                }
            }
        });

        // create the grid
        grid = new Ext.grid.GridPanel({
            store: store,
            id: 'id',
            columns: [
            {
                header: "Run Number",
                width: GetInnerHeightAndWidthofClientBrowser(0, 10),
                dataIndex: 'Run Number',
                sortable: false,
                renderer: groupingADLBLdata
            },
            {
                header: "DOS",
                width: GetInnerHeightAndWidthofClientBrowser(0, 8),
                dataIndex: 'DOS',
                sortable: false,
                // fixed:true,
                renderer: groupingADLBLdata
            },
            {
                header: "Charges",
                width: GetInnerHeightAndWidthofClientBrowser(0, 10),
                dataIndex: 'Charges',
                sortable: false,
                // fixed:true,
                renderer: function (value, meta, record, rowIndex, colIndex, store) {
                    if (store.getAt(rowIndex).get('ordr') == 1) {
                        $.each($(".x-grid3-header thead td"), function (index, item) {
                            if (index == colIndex)
                                meta.style = 'text-align:right; width:' + $(item).width() + 'px;'
                        });
                    }
                    else if (store.getAt(rowIndex).get('MrgCell') == 0 && store.getAt(rowIndex).get('ordr') > 1) {
                        if (colIndex == 1) {
                            width = '';
                            $.each($(".x-grid3-header thead td"), function (index, item) {
                                if (index > colIndex)
                                    width += $(item).width();
                            });
                            meta.style = 'width:' + width + 'px;'
                        }
                        else if (colIndex > 1) {
                            if (rowIndex > 0) {
                                if (store.getAt(rowIndex - 1).get('ordr') != store.getAt(rowIndex).get('ordr'))
                                    meta.style = 'display:none; border-top:1px solid; #000; width:0px'
                                else
                                    meta.style = 'display:none; width:0px'
                            }
                        }
                    }
                    if (rowIndex > 0) {
                        if ((store.getAt(rowIndex - 1).get('Run Number') == store.getAt(rowIndex).get('Run Number')) && (store.getAt(rowIndex - 1).get('DOS') == store.getAt(rowIndex).get('DOS')) && (store.getAt(rowIndex - 1).get('Charges') == store.getAt(rowIndex).get('Charges')) && (store.getAt(rowIndex - 1).get('Payer') == store.getAt(rowIndex).get('Payer'))) {
                            value = "";
                        }
                    }
                    meta.attr = 'title="' + value + '"';
                    return value;
                }

            },
             {
                 header: "Payer",
                 width: GetInnerHeightAndWidthofClientBrowser(0, 15),
                 dataIndex: 'Payer',
                 sortable: false,
                 // fixed:true,
                 renderer: groupingADLBLdata
             },

            {
                header: "HCPCS Code",
                width: GetInnerHeightAndWidthofClientBrowser(0, 10),
                dataIndex: 'HCPCS Code',
                sortable: false,
                renderer: function (val, meta, record, rowIndex, colIndex, store) {
                    if (store.getAt(rowIndex).get('ordr') == 1) {
                        $.each($(".x-grid3-header thead td"), function (index, item) {
                            if (index == colIndex)
                                meta.style = 'width:' + $(item).width() + 'px;'
                        });
                    }
                    else if (store.getAt(rowIndex).get('MrgCell') == 0 && store.getAt(rowIndex).get('ordr') > 1) {
                        if (colIndex == 1) {
                            width = '';
                            $.each($(".x-grid3-header thead td"), function (index, item) {
                                if (index > colIndex)
                                    width += $(item).width();
                            });
                            meta.style = 'width:' + width + 'px;'
                        }
                        else if (colIndex > 1) {
                            if (rowIndex > 0) {
                                if (store.getAt(rowIndex - 1).get('ordr') != store.getAt(rowIndex).get('ordr'))
                                    meta.style = 'display:none; border-top:1px solid; #000; width:0px'
                                else
                                    meta.style = 'display:none; width:0px'
                            }
                        }
                    }
                    meta.attr = 'title="' + val + '"';
                    return val;
                }
            },

            {
                header: "Denial Code-Reason",
                width: GetInnerHeightAndWidthofClientBrowser(0, 20),
                dataIndex: 'Denial Code-Reason',
                //  fixed:true,
                sortable: false,
                renderer: function (val, meta, record, rowIndex, colIndex, store) {
                    if (store.getAt(rowIndex).get('ordr') == 1) {
                        $.each($(".x-grid3-header thead td"), function (index, item) {
                            if (index == colIndex)
                                meta.style = 'width:' + $(item).width() + 'px;'
                        });
                    }
                    else if (store.getAt(rowIndex).get('MrgCell') == 0 && store.getAt(rowIndex).get('ordr') > 1) {
                        if (colIndex == 1) {
                            width = '';
                            $.each($(".x-grid3-header thead td"), function (index, item) {
                                if (index > colIndex)
                                    width += $(item).width();
                            });
                            meta.style = 'width:' + width + 'px;'
                        }
                        else if (colIndex > 1) {
                            if (rowIndex > 0) {
                                if (store.getAt(rowIndex - 1).get('ordr') != store.getAt(rowIndex).get('ordr'))
                                    meta.style = 'display:none; border-top:1px solid; #000; width:0px'
                                else
                                    meta.style = 'display:none; width:0px'
                            }
                        }
                    }
                    meta.attr = 'title="' + val + '"';
                    return val;
                }
            }
           , {
               header: "Provider",
               width: GetInnerHeightAndWidthofClientBrowser(0, 10),
               dataIndex: 'Provider',
               //  fixed:true,
               sortable: false,
               renderer: function (val, meta, record, rowIndex, colIndex, store) {
                   if (store.getAt(rowIndex).get('ordr') == 1) {
                       $.each($(".x-grid3-header thead td"), function (index, item) {
                           if (index == colIndex)
                               meta.style = 'width:' + $(item).width() + 'px;'
                       });
                   }
                   else if (store.getAt(rowIndex).get('MrgCell') == 0 && store.getAt(rowIndex).get('ordr') > 1) {
                       if (colIndex == 1) {
                           width = '';
                           $.each($(".x-grid3-header thead td"), function (index, item) {
                               if (index > colIndex)
                                   width += $(item).width();
                           });
                           meta.style = 'width:' + width + 'px;'
                       }
                       else if (colIndex > 1) {
                           if (rowIndex > 0) {
                               if (store.getAt(rowIndex - 1).get('ordr') != store.getAt(rowIndex).get('ordr'))
                                   meta.style = 'display:none; border-top:1px solid; #000; width:0px'
                               else
                                   meta.style = 'display:none; width:0px'
                           }
                       }
                   }
                   meta.attr = 'title="' + val + '"';
                   return val;
               }
           }
            ],
            viewConfig: { forceFit: true },
            renderTo: 'divfinancialsummary',
            width: $('#divfinancialsummary').width(), //GetInnerHeightAndWidthofClientBrowser(0,80),
            height: GetInnerHeightAndWidthofClientBrowser(1, 60),
            title: 'ANSI DENIAL LOG BY LINE',
            header: false



        });
        store.load();
    }
    catch (e) {
        shownodata();
    }
}


function groupingADLBLdata(val, meta, record, rowIndex, colIndex, store) {
    try {
        var value = val;
        if (rowIndex > 0) {
            if ((store.getAt(rowIndex - 1).get('Run Number') == store.getAt(rowIndex).get('Run Number')) && (store.getAt(rowIndex - 1).get('DOS') == store.getAt(rowIndex).get('DOS')) && (store.getAt(rowIndex - 1).get('Charges') == store.getAt(rowIndex).get('Charges')) && (store.getAt(rowIndex - 1).get('Payer') == store.getAt(rowIndex).get('Payer'))) {
                value = "";
            }
        }
        if (store.getAt(rowIndex).get('ordr') == 1) {
            $.each($(".x-grid3-header thead td"), function (index, item) {
                if (index == colIndex)
                    meta.style = 'width:' + $(item).width() + 'px;'
            });
        }
        else if (store.getAt(rowIndex).get('MrgCell') == 0 && store.getAt(rowIndex).get('ordr') > 1) {
            if (colIndex == 1) {
                width = '';
                if (rowIndex > 0) {
                    if (store.getAt(rowIndex - 1).get('ordr') != store.getAt(rowIndex).get('ordr'))
                        meta.style = 'border-top:1px solid; #000; width:100%;'
                    else
                        meta.style = ' width:100%'
                }
                meta.attr = 'colspan="6"'
            }
            else if (colIndex > 1) {
                if (rowIndex > 0) {
                    if (store.getAt(rowIndex - 1).get('ordr') != store.getAt(rowIndex).get('ordr'))
                        meta.style = 'border-top:1px solid; #000; width:0px'
                    else
                        meta.style = 'display:none; width:0px'
                }
            }
            else if (colIndex == 0) {
                if (rowIndex > 0) {
                    if (store.getAt(rowIndex - 1).get('Run Number') == store.getAt(rowIndex).get('Run Number')) {
                        value = "";
                    }
                }
                $.each($(".x-grid3-header thead td"), function (index, item) {
                    if (index == 0) {
                        if (rowIndex > 0) {
                            if (store.getAt(rowIndex - 1).get('ordr') != store.getAt(rowIndex).get('ordr'))
                                meta.style = 'border-top:1px solid; #000; width:' + $(item).width() + 'px'
                            else
                                meta.style = ' width:' + $(item).width() + 'px'
                        }
                    }
                });

            }

        }
        meta.attr = 'title="' + value + '"';
        return value;
    }
    catch (e) {
        shownodata();
        return e;
    }
}
/////////////// TRANSACTION SUMMARY BY TYPE (25)//////////////////////////////////////////////////////////////

function getTransactionSummarybyType(fid, id, branch, carrier, fromdate, todate, predata, deficiencycode, providercode) {
    try {
        var store = new Ext.data.JsonStore({
            root: 'Table',
            autoLoad: false,
            fields: [{ name: 'Code', type: 'string' },
                 { name: 'Description', type: 'string' },
                 { name: 'MTD/Total', type: 'string' },
                 { name: 'Miles', type: 'string' },
                 { name: 'Payments(#)', type: 'int' },
                 { name: 'Adjustments(#)', type: 'int' },
                 { name: 'Total(#)', type: 'int' },
                 { name: 'Payments($)', type: 'string' },
                 { name: 'Adjustments($)', type: 'string' },
                 { name: 'Total($)', type: 'string' },
                 { name: 'ordr', type: 'int' }

            ],
            proxy: new Ext.data.HttpProxy({
                method: 'GET',
                url: document.getElementById("ContentHolder_hdnProxyUrlTest").value + 'reportdata.aspx?sp=getGraphdata&fid=' + fid + '&id=' + id + '&branch=' + branch + '&carrier=' + carrier + '&FromDate=' + fromdate + '&toDate=' + todate + '&predata=' + predata + '&deficiencycode=' + deficiencycode + '&providercode=' + providercode,
                timeout: 90000
            }),
            listeners: {
                load: {
                    fn: function () {

                        if (store.getCount() == 0)
                            shownodata();
                        HideLoading();
                        getGridJsScrollBar()
                    }
                },
                exception: function (misc) {
                    shownodata();
                    //shownoNetwork();
                }
            },
            sortInfo: this.sortInfo,
            groupField: 'Code',
            getGroupState: Ext.emptyFn
        });


        // create the grid
        grid = new Ext.grid.GridPanel({
            store: store,
            id: 'id',
            columns: [
            {
                header: "Code",
                width: GetInnerHeightAndWidthofClientBrowser(0, 8),
                dataIndex: 'Code',
                sortable: false,
                //fixed:true,
                renderer: function (val, metadata, record, rowIndex, colIndex, store) {

                    $.each($(".x-grid3-header thead td"), function (index, item) {
                        if (index == colIndex) {
                            if (store.getAt(rowIndex).get('ordr') == 2) {
                                if (rowIndex == store.getCount() - 2)
                                    metadata.style = 'border-top:1px solid #000; font-weight:bold; width:' + $(item).width() + 'px;';
                                else
                                    metadata.style = 'font-weight:bold; width:' + $(item).width() + 'px;';
                            }
                            else
                                metadata.style = 'width:' + $(item).width() + 'px;';
                        }
                    });
                    var value = val;
                    if (rowIndex > 0) {
                        if ((store.getAt(rowIndex - 1).get('Description') == store.getAt(rowIndex).get('Description')) && (store.getAt(rowIndex - 1).get('Code') == store.getAt(rowIndex).get('Code'))) {
                            value = "";
                        }
                    }
                    metadata.attr = 'title="' + value + '"';
                    return value;
                }
            },
            {
                header: "Description",
                width: GetInnerHeightAndWidthofClientBrowser(0, 20),
                dataIndex: 'Description',
                sortable: false,
                //fixed:true,
                renderer: function (val, metadata, record, rowIndex, colIndex, store) {

                    $.each($(".x-grid3-header thead td"), function (index, item) {
                        if (index == colIndex)
                            if (store.getAt(rowIndex).get('ordr') == 2) {
                                if (rowIndex == store.getCount() - 2)
                                    metadata.style = 'border-top:1px solid #000; font-weight:bold; width:' + $(item).width() + 'px;';
                                else
                                    metadata.style = 'font-weight:bold; width:' + $(item).width() + 'px;';
                            }
                            else
                                metadata.style = 'width:' + $(item).width() + 'px;';


                    });
                    var value = val;
                    if (rowIndex > 0) {
                        if ((store.getAt(rowIndex - 1).get('Description') == store.getAt(rowIndex).get('Description')) && (store.getAt(rowIndex - 1).get('Code') == store.getAt(rowIndex).get('Code'))) {
                            value = "";
                        }
                    }
                    metadata.attr = 'title="' + value + '"';
                    return value;
                }
            },
            {
                header: "MTD/Total",
                width: GetInnerHeightAndWidthofClientBrowser(0, 8),
                dataIndex: 'MTD/Total',
                //fixed:true,
                sortable: false,
                renderer: function (value, metadata, record, rowIndex, colIndex, store) {

                    $.each($(".x-grid3-header thead td"), function (index, item) {
                        if (index == colIndex)
                            if (store.getAt(rowIndex).get('ordr') == 2) {
                                if (rowIndex == store.getCount() - 2)
                                    metadata.style = 'border-top:1px solid #000; font-weight:bold; width:' + $(item).width() + 'px;';
                                else
                                    metadata.style = 'font-weight:bold; width:' + $(item).width() + 'px;';
                            }
                            else
                                metadata.style = 'width:' + $(item).width() + 'px;';

                    });
                    metadata.attr = 'title="' + value + '"';
                    return value;
                }
            },

            {
                header: "Miles",
                width: GetInnerHeightAndWidthofClientBrowser(0, 8),
                dataIndex: 'Miles',
                //fixed:true,
                sortable: false,
                renderer: function (value, metadata, record, rowIndex, colIndex, store) {

                    $.each($(".x-grid3-header thead td"), function (index, item) {
                        if (index == colIndex)
                            if (store.getAt(rowIndex).get('ordr') == 2)
                                if (rowIndex == store.getCount() - 2)
                                    metadata.style = 'border-top:1px solid #000; font-weight:bold; width:' + $(item).width() + 'px;';
                                else
                                    metadata.style = 'font-weight:bold; width:' + $(item).width() + 'px;';
                            else
                                metadata.style = 'width:' + $(item).width() + 'px;';

                    });
                    metadata.attr = 'title="' + value + '"';
                    return value;
                }
            },



            //            {
            //                header: "Payments(#)",
            //                width: GetInnerHeightAndWidthofClientBrowser(0, 7),
            //                dataIndex: 'Payments(#)',
            //                fixed:true,
            //                sortable: false
            //            },
            //            {
            //                header: "Adjustments(#)",
            //                width: GetInnerHeightAndWidthofClientBrowser(0, 8),
            //                dataIndex: 'Adjustments(#)',
            //                //fixed:true,
            //                sortable: false
            //            },
            //            {
            //                header: "Total(#)",
            //                width: GetInnerHeightAndWidthofClientBrowser(0, 5),
            //                dataIndex: 'Total(#)',
            //                //fixed:true,
            //                sortable: false
            //            },
            {
                header: "Payments($)",
                width: GetInnerHeightAndWidthofClientBrowser(0, 10),
                dataIndex: 'Payments($)',
                sortable: false,
                //fixed:true,
                renderer: function (value, metadata, record, rowIndex, colIndex, store) {

                    $.each($(".x-grid3-header thead td"), function (index, item) {
                        if (index == colIndex)
                            if (store.getAt(rowIndex).get('ordr') == 2) {
                                if (rowIndex == store.getCount() - 2)
                                    metadata.style = 'border-top:1px solid #000; font-weight:bold; text-align:right; width:' + $(item).width() + 'px;';
                                else
                                    metadata.style = 'font-weight:bold; text-align:right; width:' + $(item).width() + 'px;';
                            }
                            else
                                metadata.style = 'text-align:right; width:' + $(item).width() + 'px;';

                    });
                    metadata.attr = 'title="' + value + '"';
                    return value;
                }


            },
            {
                header: "Adjustments($)",
                width: GetInnerHeightAndWidthofClientBrowser(0, 10),
                dataIndex: 'Adjustments($)',
                sortable: false,
                //fixed:true,
                //                renderer: function(value, metadata, record, rowIndex, colIndex, store){
                //                    metadata.style='text-align:right; padding-right:15px;';
                //                    
                //                          return value;
                //                 }
                renderer: function (value, metadata, record, rowIndex, colIndex, store) {

                    $.each($(".x-grid3-header thead td"), function (index, item) {
                        if (index == colIndex)
                            if (store.getAt(rowIndex).get('ordr') == 2) {
                                if (rowIndex == store.getCount() - 2)
                                    metadata.style = 'border-top:1px solid #000; font-weight:bold; text-align:right; width:' + $(item).width() + 'px;';
                                else
                                    metadata.style = 'font-weight:bold; text-align:right; width:' + $(item).width() + 'px;';
                            }
                            else
                                metadata.style = 'text-align:right; width:' + $(item).width() + 'px;';

                    });
                    metadata.attr = 'title="' + value + '"';
                    return value;
                }
            },
            {
                header: "Total($)",
                width: GetInnerHeightAndWidthofClientBrowser(0, 10),
                dataIndex: 'Total($)',
                sortable: false,
                //fixed:true,
                //                renderer: function(value, metadata, record, rowIndex, colIndex, store){
                //                    metadata.style='text-align:right';
                //                    
                //                          return value;
                //                 }
                renderer: function (value, metadata, record, rowIndex, colIndex, store) {

                    $.each($(".x-grid3-header thead td"), function (index, item) {
                        if (index == colIndex)
                            if (store.getAt(rowIndex).get('ordr') == 2) {
                                if (rowIndex == store.getCount() - 2)
                                    metadata.style = 'border-top:1px solid #000; font-weight:bold; text-align:right; width:' + $(item).width() + 'px;';
                                else
                                    metadata.style = 'font-weight:bold; text-align:right; width:' + $(item).width() + 'px;';
                            }
                            else
                                metadata.style = 'text-align:right; width:' + $(item).width() + 'px;';

                    });
                    metadata.attr = 'title="' + value + '"';
                    return value;
                }
            }

            ],


            viewConfig: {
                forceFit: true,
                autoScroll: true
            },
            autoScroll: true,
            renderTo: 'divfinancialsummary',
            width: $('#divfinancialsummary').width(), //GetInnerHeightAndWidthofClientBrowser(0,80),
            height: GetInnerHeightAndWidthofClientBrowser(1, 60),
            title: 'TRANSACTION SUMMARY BY TYPE',
            header: false

        });
        store.load();
    }
    catch (e) {
        shownodata();
    }



}




/////////////// ANALYSIS BY TRANSACTION DATE (20)//////////////////////////////////////////////////////////////

function getAnalysisbyTransactionDate(fid, id, carrier, fromdate, todate, type, date) {



    var store = new Ext.data.JsonStore({
        root: 'Table',
        autoLoad: false,
        fields: [{ name: 'Run Number', type: 'string' }, { name: 'Transaction Date', type: 'string' }, { name: 'Date Of Service', type: 'string' },
        { name: 'Amount', type: 'string' }, { name: 'Payer Group', type: 'string' }, { name: 'Patient Name', type: 'string' }, { name: 'ordr', type: 'int' }, { name: 'MrgCell', type: 'int' }],
        proxy: new Ext.data.HttpProxy({
            method: 'GET',
            url: document.getElementById("ContentHolder_hdnProxyUrlTest").value + 'reportdata.aspx?sp=getAnalysisByTransactionDateDrillDown&fid=' + fid + '&id=' + id + '&carrier=' + carrier + '&FromDate=' + fromdate + '&toDate=' + todate + '&type=' + type + '&date=' + date,
            timeout: 90000
        }),
        listeners: {
            load: {
                fn: function () {
                    if (store.getCount() == 0)
                        shownodata();

                    HideLoading();
                    getGridJsScrollBar()
                }
            },

            exception: function (misc) {
                shownodata();
                //shownoNetwork();
            }
        },
        sortInfo: this.sortInfo,
        groupField: 'Run Number'
        //   getGroupState: Ext.emptyFn
    });


    // create the grid
    grid = new Ext.grid.GridPanel({
        store: store,
        id: 'id',
        columns: [
            {
                header: "Run Number",
                width: GetInnerHeightAndWidthofClientBrowser(0, 10),
                dataIndex: 'Run Number',
                sortable: false,
                // fixed:true,
                renderer: showUpdated

            },
            {
                header: "Transaction Date",
                width: GetInnerHeightAndWidthofClientBrowser(0, 10),
                dataIndex: 'Transaction Date',
                //fixed:true,
                sortable: false,
                renderer: showUpdated
            },
            {
                header: "Date Of Service",
                width: GetInnerHeightAndWidthofClientBrowser(0, 15),
                dataIndex: 'Date Of Service',
                sortable: false,
                //fixed:true,
                renderer: showUpdated


            },
            {
                header: "Amount",
                width: GetInnerHeightAndWidthofClientBrowser(0, 15),
                dataIndex: 'Amount',
                sortable: false,
                //fixed:true,
                renderer: function (value, metadata, record, rowIndex, colIndex, store) {
                    $.each($(".x-grid3-header thead td"), function (index, item) {
                        if (index == colIndex) {
                            metadata.style = 'text-align:right; width:' + $(item).width();
                        }
                    });

                    if (store.getAt(rowIndex).get('ordr') == 2) {
                        if (colIndex == 0)
                            metadata.style = 'width:100%';
                        else
                            metadata.style = 'width:0pc; display:none';
                    }
                    metadata.attr = 'title="' + value + '"';
                    return value;
                }
            },
            {
                header: "Payer Group",
                width: GetInnerHeightAndWidthofClientBrowser(0, 15),
                dataIndex: 'Payer Group',
                sortable: false,
                //fixed:true,
                renderer: showUpdated
            },
            {
                header: "Patient Name",
                width: GetInnerHeightAndWidthofClientBrowser(0, 15),
                dataIndex: 'Patient Name',
                sortable: false,
                //fixed:true,
                renderer: showUpdated
            }

        ],

        viewConfig: {
            forceFit: true,
            autoScroll: true
        },
        renderTo: 'divfinancialsummary',
        width: $('#divfinancialsummary').width(), //GetInnerHeightAndWidthofClientBrowser(0,80),
        height: GetInnerHeightAndWidthofClientBrowser(1, 60),
        title: 'ANALYSIS BY TRANSACTION DATE',
        header: false


    });

    store.load();


}



function showUpdated(val, meta, record, rowIndex, colIndex, store) {
    var value = val;
    //    if(rowIndex>0){
    //     if (store.getAt(rowIndex - 1).get('Date') == store.getAt(rowIndex).get('Date')) {
    //         value = "";
    //     }
    // }
    if (store.getAt(rowIndex).get('ordr') == 1) {
        $.each($(".x-grid3-header thead td"), function (index, item) {
            if (index == colIndex)
                meta.style = 'width:' + $(item).width() + 'px'
        });
    }


    if (store.getAt(rowIndex).get('ordr') == 2) {
        if (colIndex == 0)
            meta.style = 'width:100%; border-top:1px solid #000; font-weight:bold';
        else
            meta.style = 'width:0px; display:none';
    }
    meta.attr = 'title="' + value + '"';
    return value;
}





/////////////////////////////WINDOW RESIZE EVENT///////////////////////////////////////////////////////////////
Ext.EventManager.onWindowResize(function () {

    if ($(window).width() > 640) {
        if (grid) {
            if (!iPhoneAgent && !AndroidAgent) {
                grid.setWidth(GetInnerHeightAndWidthofClientBrowser(0, 79.5));
                //alert(GetInnerHeightAndWidthofClientBrowser(0,80));
                $('#divfinancialsummary').css('width', GetInnerHeightAndWidthofClientBrowser(0, 80));
            }
            else if (!AndroidAgent) {
                grid.setWidth(GetInnerHeightAndWidthofClientBrowser(0, 97.5));
                //alert(GetInnerHeightAndWidthofClientBrowser(0,80));
                $('#divfinancialsummary').css('width', GetInnerHeightAndWidthofClientBrowser(0, 98));
            }
            else {
                if ($(window).width() < 650 || $(window).height() < 650) {
                    grid.setWidth(GetInnerHeightAndWidthofClientBrowser(0, 97.5));
                    //alert(GetInnerHeightAndWidthofClientBrowser(0,80));
                    $('#divfinancialsummary').css('width', GetInnerHeightAndWidthofClientBrowser(0, 98));
                }
                else {
                    grid.setWidth(GetInnerHeightAndWidthofClientBrowser(0, 79.5));
                    //alert(GetInnerHeightAndWidthofClientBrowser(0,80));
                    $('#divfinancialsummary').css('width', GetInnerHeightAndWidthofClientBrowser(0, 80));
                }
            }
            Ext.getCmp('id').getView().refresh();
        }
        if (iPadAgent || AndroidAgent)
            getGridJsScrollBar();
    }
    else {
        getGridJsScrollBar();
    }
    //setTimeout("setscrollheight();", 300);
});


/////////////// INSURANCE AGING REPORT BY PAYER (29)//////////////////////////////////////////////////////////////
function getInsuranceAgingReportByPayer(fid, id, branch, carrier, fromdate, todate, predata, deficiencycode, providercode) {
    var store = new Ext.data.JsonStore({
        root: 'Table',
        autoLoad: false,
        fields: [{ name: 'ordr', type: 'int' }, { name: 'Group', type: 'string' }, { name: 'Cur', type: 'string' }, { name: '31-60', type: 'string' },
       { name: '61-90', type: 'string' }, { name: '91-120', type: 'string' }, { name: '121-150', type: 'string' },
       { name: '151-180', type: 'string' }, { name: '>180', type: 'string' }, { name: 'Total', type: 'string' }],
        proxy: new Ext.data.HttpProxy({
            method: 'GET',
            url: document.getElementById("ContentHolder_hdnProxyUrlTest").value + 'reportdata.aspx?sp=getGraphdata&fid=' + fid + '&id=' + id + '&branch=' + branch + '&carrier=' + carrier + '&FromDate=' + fromdate + '&toDate=' + todate + '&predata=' + predata + '&deficiencycode=' + deficiencycode + '&providercode=' + providercode,
            timeout: 90000
        }),
        listeners: {
            load: {
                fn: function () {

                    if (store.getCount() == 0)
                        shownodata();
                    HideLoading();
                    getGridJsScrollBar()
                }
            },
            exception: function (misc) {
                shownodata();
                //shownoNetwork();
            }
        }
    });

    // create the grid
    grid = new Ext.grid.GridPanel({
        store: store,
        id: 'id',
        columns: [
            {
                header: "Group",
                width: GetInnerHeightAndWidthofClientBrowser(0, 13),
                dataIndex: 'Group',
                sortable: false,
                renderer: function (value, metadata, record, rowIndex, colIndex, store) {

                    if (record.get('ordr') == 2)
                        metadata.style = 'text-align:right; font-weight:bold;';
                    else
                        metadata.style = 'font-weight:bold;';
                    return value;
                }
                //fixed:true,
                //renderer:groupingIARBPdata
            },
            {
                header: "Cur",
                width: GetInnerHeightAndWidthofClientBrowser(0, 7),
                dataIndex: 'Cur',
                sortable: false,
                renderer: function (value, metadata, record, rowIndex, colIndex, store) {

                    if (record.get('ordr') == 2)
                        metadata.style = 'text-align:right; font-weight:bold;';
                    else
                        metadata.style = 'text-align:right;';
                    return value;
                }
                // fixed:true
                //renderer:groupingIARBPdata 
            },
            {
                header: "31-60",
                width: GetInnerHeightAndWidthofClientBrowser(0, 7),
                dataIndex: '31-60',
                sortable: false,
                // fixed:true,
                renderer: function (value, metadata, record, rowIndex, colIndex, store) {
                    if (record.get('ordr') == 2)
                        metadata.style = 'text-align:right; font-weight:bold;';
                    else
                        metadata.style = 'text-align:right; ';

                    return value;
                }

            },
             {
                 header: "61-90",
                 width: GetInnerHeightAndWidthofClientBrowser(0, 7),
                 dataIndex: '61-90',
                 sortable: false,
                 renderer: function (val, metadata, record, rowIndex, colIndex, store) {
                     if (record.get('ordr') == 2)
                         metadata.style = 'text-align:right; font-weight:bold;';
                     else
                         metadata.style = 'text-align:right; ';
                     return val;
                 }
                 // fixed:true,
                 //  renderer:groupingIARBPdata 
             },

            {
                header: "91-120",
                width: GetInnerHeightAndWidthofClientBrowser(0, 7),
                dataIndex: '91-120',
                sortable: false,
                renderer: function (val, metadata, record, rowIndex, colIndex, store) {
                    if (record.get('ordr') == 2)
                        metadata.style = 'text-align:right; font-weight:bold;';
                    else
                        metadata.style = 'text-align:right; ';
                    return val;
                }
            },

            {
                header: "121-150",
                width: GetInnerHeightAndWidthofClientBrowser(0, 7),
                dataIndex: '121-150',
                //  fixed:true,
                sortable: false,
                renderer: function (val, metadata, record, rowIndex, colIndex, store) {
                    if (record.get('ordr') == 2)
                        metadata.style = 'text-align:right; font-weight:bold;';
                    else
                        metadata.style = 'text-align:right;';
                    return val;
                }
            },
            {
                header: "151-180",
                width: GetInnerHeightAndWidthofClientBrowser(0, 7),
                dataIndex: '151-180',
                // fixed:true, 
                sortable: false,
                renderer: function (val, metadata, record, rowIndex, colIndex, store) {
                    if (record.get('ordr') == 2)
                        metadata.style = 'text-align:right; font-weight:bold;';
                    else
                        metadata.style = 'text-align:right;';

                    return val;
                }
            },
             {
                 header: ">180",
                 width: GetInnerHeightAndWidthofClientBrowser(0, 7),
                 dataIndex: '>180',
                 // fixed:true, 
                 sortable: false,
                 renderer: function (val, metadata, record, rowIndex, colIndex, store) {
                     if (record.get('ordr') == 2)
                         metadata.style = 'text-align:right; font-weight:bold;';
                     else
                         metadata.style = 'text-align:right; padding-right:5px';
                     return val;
                 }
             },
             {
                 header: "Total",
                 width: GetInnerHeightAndWidthofClientBrowser(0, 8),
                 dataIndex: 'Total',
                 // fixed:true, 
                 sortable: false,
                 renderer: function (val, metadata, record, rowIndex, colIndex, store) {
                     metadata.style = 'text-align:right; font-weight:bold;';

                     return val;
                 }
             }

        ],
        viewConfig: { forceFit: true },
        renderTo: 'divfinancialsummary',
        width: $('#divfinancialsummary').width(), //GetInnerHeightAndWidthofClientBrowser(0,80),
        height: GetInnerHeightAndWidthofClientBrowser(1, 58),
        title: 'INSURANCE AGING REPORT BY PAYER'



    });
    store.load();
}


function groupingIARBPdata(val, meta, record, rowIndex, colIndex, store) {
    var value = val;
    if (rowIndex > 0) {
        if ((store.getAt(rowIndex - 1).get('Run Number') == store.getAt(rowIndex).get('Run Number')) && (store.getAt(rowIndex - 1).get('DOS') == store.getAt(rowIndex).get('DOS')) && (store.getAt(rowIndex - 1).get('Charges') == store.getAt(rowIndex).get('Charges')) && (store.getAt(rowIndex - 1).get('Payer') == store.getAt(rowIndex).get('Payer'))) {
            value = "";
        }
    }
    return value;
}


///////////////////////////////// JB KPI REPORT (27)////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


function getJBKPIReport(data, contrl, id, title, color) {

    if (data.length > 0) {
        var SaleRecord = Ext.data.Record.create([
        { name: 'name', type: 'string' },
        { name: 'amount', type: 'string' },
        { name: 'month', type: 'string' },
        { name: 'rowgrp', type: 'int' },
        { name: 'colgrp', type: 'int' },
        { name: 'fullsort', type: 'int' }
        // { name: 'Colour', type: 'string' },
        //  { name: 'ForeColour', type: 'string' }
        //        { name: 'quantity', type: 'int' },
        //        { name: 'value', type: 'int' }
        ]);

        var myStore = new Ext.data.Store({
            data: data,
            autoLoad: true,
            reader: new Ext.data.JsonReader({
                root: '',
                idProperty: 'JBKPI'
            }, SaleRecord)

        });

        var grid = new Ext.grid.PivotGrid({
            title: title,
            id: id,
            width: document.getElementById(contrl).style.width,
            height: GetInnerHeightAndWidthofClientBrowser(1, 60),
            renderTo: contrl,
            store: myStore,
            // aggregator: 'sum',
            measure: 'amount',

            viewConfig: {
                title: ($('#ContentHolder_hdnFacility').val().split('-')[0].split(',').length > 1 ? '' : $('#ContentHolder_hdnFacility').val().split('-')[0]),
                forceFit: true
            },

            aggregator: function (records, measure) {
                for (var i = 0; i < records.length; i++) {
                    return '<div style="text-align:right; width:100%" title="' + records[i].get(measure) + '">' + records[i].get(measure) + '</div>';
                }

            },


            //            aggregator: function (records, measure) {
            //                for (var i = 0; i < records.length; i++) {
            //                    return '<a href="#" onclick="alert( 123456 );">' + records[i].get(measure) + '</a>';
            //                    return "<span style='font-weight:bold;color:" + records[i].get("ForeColour") + ", background-color:'" + records[i].get("Colour") + ">" + measure + "</span>";
            //                }
            //            },


            leftAxis: [{
                dataIndex: 'fullsort',
                sortable: false
            },
                 {
                     dataIndex: 'rowgrp',
                     sortable: false
                 },
                 {
                     dataIndex: 'name',
                     width: GetInnerHeightAndWidthofClientBrowser(0, 5),
                     sortable: false
                 }],

            topAxis: [{
                dataIndex: 'colgrp',
                sortable: false
            },
                        {
                            dataIndex: 'month',
                            width: 100// GetInnerHeightAndWidthofClientBrowser(0, 10)
                        }]
        });

        grid.view.refresh(true);

        $.each($('.x-grid3-row-headers table tr'), function (index, item) {

            var lst = $(item).find('td');
            if (lst.length == 3) {
                $(lst[0]).css('display', 'none');
                $(lst[1]).css('display', 'none');
            }
            else if (lst.length == 2)
                $(lst[0]).css('display', 'none');

        });




        $('.x-grid3-header-offset table').find('tr:first').css('display', 'none');


        //                   $('#' + contrl + ' #' + id + ' .x-grid3-row-headers table td').remove('background')
        //                   $('#'+contrl+' #' + id + ' .x-grid3-row-headers table td').css('background', color);

        //                   $('#'+contrl+' #' + id + ' .x-grid3-header').css('background-color', color)
        $('.x-grid3-header-title').height($('.x-grid3-header-inner').height());

        setTimeout("$('.x-pivotgrid').css('height', '100%');", 500);
        // $('.x-grid3-scroller').css('height', GetInnerHeightAndWidthofClientBrowser(1, 60))
        getGridJsScrollBar();
    }
}

////////////////////////////////////////// Location Report 30 ///////////////////////////////////////////


function getLocationReport(id, carrier, fromdate, todate, predata) {


    var SaleRecord = Ext.data.Record.create([
        { name: 'name', type: 'string' },
        { name: 'amount', type: 'string' },
        { name: 'month', type: 'string' },
        { name: 'rowgrp', type: 'int' },
        { name: 'colgrp', type: 'int' },
        { name: 'fullsort', type: 'int' }
    // { name: 'Colour', type: 'string' },
    //  { name: 'ForeColour', type: 'string' }
    //        { name: 'quantity', type: 'int' },
    //        { name: 'value', type: 'int' }
    ]);

    var myStore = new Ext.data.Store({

        proxy: new Ext.data.HttpProxy({
            method: 'GET',
            url: '../reportdata.aspx?sp=getlocationsBasedTextReport&id=' + id + '&carrier=' + carrier + '&FromDate=' + fromdate + '&toDate=' + todate + '&predata=' + predata,
            timeout: 90000
        }),
        autoLoad: true,
        reader: new Ext.data.JsonReader({
            root: 'Table',
            idProperty: id
        }, SaleRecord),
        listeners: {
            load: {
                fn: function () {

                    if (myStore.getCount() > 0) {
                        $.each($('.x-grid3-row-headers table tr'), function (index, item) {

                            var lst = $(item).find('td');
                            if (lst.length == 3) {
                                $(lst[0]).css('display', 'none');
                                $(lst[1]).css('display', 'none');
                            }
                            else if (lst.length == 2)
                                $(lst[0]).css('display', 'none');

                        });
                        $('.x-grid3-header-offset table').find('tr:first').css('display', 'none');

                        HideLoading();
                        $('.x-grid3-header-title').height($('.x-grid3-header-inner').height());

                        $('.x-grid3-cell-inner').css('padding', '0px');
                        // $('.x-grid3-row-headers').css('font-weight', 'bold').css('text-align', 'left');



                        $.each($('.x-grid3-row-table'), function (index, item) {
                            $.each($('.x-grid3-row-headers table tr'), function (ind, itm) {
                                if (index == ind) {
                                    $(item).find('.divfs').height($(itm).height() - 2);
                                }

                            });

                        });



                        setTimeout("$('.x-pivotgrid').css('height', '100%');", 500);
                        //  $('.x-grid3-scroller').css('height', GetInnerHeightAndWidthofClientBrowser(1, 60))
                        getGridJsScrollBar();
                        setTimeout("$('#divLocationChartContainer').css('height', '100%');", 500);

                    }
                    else
                        shownodata();
                }

            },
            exception: function (misc) {
                //shownodata();
                shownoNetwork();
            }

        }
    });

    var grid = new Ext.grid.PivotGrid({
        title: '',

        id: id,
        header: false,
        width: document.getElementById('divLocationGrid').style.width,
        height: GetInnerHeightAndWidthofClientBrowser(1, 60),
        renderTo: 'divLocationGrid',
        store: myStore,
        // aggregator: 'sum',
        measure: 'amount',

        viewConfig: {
            title: '',
            forceFit: true
        },

        aggregator: function (records, measure) {
            for (var i = 0; i < records.length; i++) {
                return '<div class="divfs" style="text-align:right; width:100%" title="' + records[i].get(measure) + '">' + records[i].get(measure) + '</div>';
            }

        },


        //            aggregator: function (records, measure) {
        //                for (var i = 0; i < records.length; i++) {
        //                    return '<a href="#" onclick="alert( 123456 );">' + records[i].get(measure) + '</a>';
        //                    return "<span style='font-weight:bold;color:" + records[i].get("ForeColour") + ", background-color:'" + records[i].get("Colour") + ">" + measure + "</span>";
        //                }
        //            },


        leftAxis: [
            {
                dataIndex: 'fullsort',
                sortable: false
            },
                 {
                     dataIndex: 'rowgrp',
                     sortable: false
                 },
                 {
                     dataIndex: 'name',
                     width: GetInnerHeightAndWidthofClientBrowser(0, 5),
                     sortable: false
                 }],

        topAxis: [
            {
                dataIndex: 'colgrp',
                sortable: false
            },
                        {
                            dataIndex: 'month'
                            // width: GetInnerHeightAndWidthofClientBrowser(0, 10)
                        }]
    });

    grid.view.refresh(true);



}



///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function getCPTCodesByState(fid, id, branch, carrier, fromdate, todate, predata, location) {
    try {
        var SaleRecord = Ext.data.Record.create([
        { name: 'name', type: 'string' },
        { name: 'amount', type: 'string' },
        { name: 'month', type: 'string' },
        { name: 'rowgrp', type: 'int' },
        { name: 'colgrp', type: 'string' },
        { name: 'fullsort', type: 'int' }
        // { name: 'Colour', type: 'string' },
        //  { name: 'ForeColour', type: 'string' }
        //        { name: 'quantity', type: 'int' },
        //        { name: 'value', type: 'int' }
        ]);

        var myStore = new Ext.data.Store({
            proxy: new Ext.data.HttpProxy({
                method: 'GET',
                url: '../reportdata.aspx?sp=getCPTCodesByState&fid=' + fid + '&id=' + id + '&branch=' + branch + '&carrier=' + carrier + '&FromDate=' + fromdate + '&toDate=' + todate + '&predata=' + predata + '&location=' + location,
                timeout: 90000
            }),
            autoLoad: true,
            reader: new Ext.data.JsonReader({
                root: 'Table',
                idProperty: id
            }, SaleRecord),
            listeners: {
                load: {
                    fn: function () {

                        if (myStore.getCount() > 0) {
                            HideLoading();
                            $('.x-grid3-header-offset table').find('tr:first').css('border-bottom', '1px solid #d0d0d0');
                            $('.x-grid3-header-offset table').find('tr:last').css('border-bottom', '1px solid #EEE');
                            $('.x-grid3-row-headers table tr').find('td:first').css('display', 'none');

                            //$.each($('.x-grid3-row-table'), function (index, item) {
                            //    $.each($('.x-grid3-row-headers table tr'), function (ind, itm) {
                            //        if (index == ind) {
                            //            $(item).find('.divfs').height($(itm).height() - 2);
                            //        }
                            //    });
                            //});
                            setTimeout("$('.x-pivotgrid').css('height', '100%');", 500);
                            getGridJsScrollBar();
                        }
                        else
                            shownodata();
                    }

                },
                exception: function (misc) {
                    shownodata();
                }

            }
        });

        var grid = new Ext.grid.PivotGrid({
            title: '',
            id: id,
            width: document.getElementById('divfinancialsummary').style.width,
            height: GetInnerHeightAndWidthofClientBrowser(1, 60),
            renderTo: divfinancialsummary,
            store: myStore,
            aggregator: 'sum',
            measure: 'amount',

            viewConfig: {
                title: '',
                forceFit: true
            },

            aggregator: function (records, measure) {
                for (var i = 0; i < records.length; i++) {
                    return '<div style="text-align:right; width:100%" title="' + records[i].get(measure) + '">' + records[i].get(measure) + '</div>';
                }

            },


            leftAxis: [
            //    {
            //    dataIndex: 'fullsort',
            //    sortable: false
            //},
                 {
                     dataIndex: 'rowgrp',
                     sortable: false
                 },
                 {
                     dataIndex: 'name',
                     width: GetInnerHeightAndWidthofClientBrowser(0, 5),
                     sortable: false
                 }],

            topAxis: [{
                dataIndex: 'colgrp',
                sortable: false
            },
                        {
                            dataIndex: 'month',
                            width: 100// GetInnerHeightAndWidthofClientBrowser(0, 10)
                        }]
        });

        grid.view.refresh(true);
    }
    catch (e) {
        alert(e);
    }
}



///////////////////////////////////////////////////////////////////////////////////////////////////////
function getGridJsScrollBar() {

    if ((iPhoneAgent || AndroidAgent) && ($(window).width() < 650 || $(window).height() < 650)) {
        if ($(window).width() < 650 || $(window).height() < 650) {
            var lst1 = $('.x-grid3-header-offset table').find('tr:first').find('td');
            if ($('#hdnreportid').val() == '31')
                lst1 = $('.x-grid3-header-offset table').find('tr:last').find('td');
            $('#divfinancialsummaryCover').width($(window).width() * 95 / 100);
            $('.footer').css('position', 'fixed');
            $('#divfinancialsummaryCover').addClass('nano');
            $('#divfinancialsummaryCover').css('position', 'relative').css('overflow', 'hidden');
            if (lst1.length * 80 > $(window).width() || lst1.length > 5) {
                if ($('#divfinancialsummary').find('.x-pivotgrid').length > 0) {
                    $('#divfinancialsummary').css('width', lst1.length * 80 + $('.x-grid3-header-title').width()).css('height', 'auto');
                    $('.x-grid3-header-inner').css('width', lst1.length * 80);
                    $('.x-grid3-body').css('width', lst1.length * 80).css('float', 'none');   //.css('height', 'auto');
                    $('.x-grid3-row').css('width', lst1.length * 80);
                    $('.x-grid3-scroller').css('width', lst1.length * 80 + $('.x-grid3-header-title').width()).css('height', GetInnerHeightAndWidthofClientBrowser(1, 60));
                    $('.x-pivotgrid').css('width', lst1.length * 80 + $('.x-grid3-header-title').width());
                    $('.x-grid3-header-offset').css('width', '100%');
                    $('.x-grid3-header-offset table').css('width', '100%');
                    $('.x-grid3-row table').css('width', '100%');
                    $('.x-pivotgrid').css('height', 'auto');
                    $('.x-grid-panel').css('width', '100%').css('height', 'auto');
                    $('.x-panel-body').css('width', lst1.length * 80 + $('.x-grid3-header-title').width()).css('height', 'auto');
                }
                else {

                    $('#divfinancialsummary').css('width', lst1.length * 100).css('height', 'auto');
                    $('.x-grid3-body').css('width', lst1.length * 100).css('float', 'none');
                    $('.x-grid3-header-inner').css('width', lst1.length * 100);
                    $('.x-grid-panel').css('width', lst1.length * 100).css('height', 'auto');
                    $('.x-grid3-row').css('width', lst1.length * 100);
                    $('.x-grid3-row table').css('width', '100%');
                    $('.x-grid3-scroller').css('width', 'auto').css('height', GetInnerHeightAndWidthofClientBrowser(1, 60)).css('font-size', '10px !important');
                    $('.x-grid3').css('width', lst1.length * 100).css('height', 'auto');
                    $('.x-grid3-header-offset').css('width', '100%');
                    $('.x-grid3-header-offset table').css('width', '100%');
                    $('.x-panel-body').css('width', lst1.length * 100).css('height', 'auto');
                    if ($('#hdnreportid').val() != '25') {
                        $.each($('.x-grid3-header-offset table').find('td'), function (index, item) {
                            $(item).css('width', '');
                        });

                        $.each($('.x-grid3-row'), function (index, item) {
                            var lst = $(item).find('td');
                            for (var i = 0; i < lst.length; i++) {
                                $(lst[i]).width(Math.round($(lst1[i]).width()));

                            }
                        });
                    }
                }
            }
            else if ($('#hdnreportid').val() == '18' || $('#hdnreportid').val() == '27' || $('#hdnreportid').val() == '30' || $('#hdnreportid').val() == '31') {
                $('#divfinancialsummary').css('height', 'auto');
                $('#divfinancialsummary').css('width', $('#divfinancialsummaryCover').width());
                $('.x-grid3-header-inner').css('width', $('#divfinancialsummaryCover').width() - $('.x-grid3-header-title').width());
                $('.x-grid3-body').css('width', $('#divfinancialsummaryCover').width() - $('.x-grid3-header-title').width()).css('float', 'none').css('height', 'auto');
                $('.x-grid3-row').css('width', $('#divfinancialsummaryCover').width() - $('.x-grid3-header-title').width());
                $('.x-grid3-scroller').css('width', $('#divfinancialsummaryCover').width()).css('font-size', '10px !important').css('height', GetInnerHeightAndWidthofClientBrowser(1, 60) - $('.x-grid3-header-title').height());
                $('.x-pivotgrid').css('width', $('#divfinancialsummaryCover').width());
                $('.x-grid3-header-offset').css('width', '100%');
                $('.x-grid3-header-offset table').css('width', '100%');
                $('.x-grid3-row table').css('width', '100%');
                $('.x-pivotgrid').css('height', 'auto');
                $('.x-grid-panel').css('width', '100%');
                $('.x-panel-body').css('width', $('#divfinancialsummaryCover').width()).css('height', 'auto');;
            }
            else {
                //  $('#divfinancialsummary').css('max-height', GetInnerHeightAndWidthofClientBrowser(1, 60) - $('#spanGridTitle').height());


                var width = Math.round(lst1.length * 100 > $(window).width() * 96 / 100 ? lst1.length * 100 : $(window).width() * 96 / 100);

                $('#divfinancialsummary').css('height', 'auto');
                $('#divfinancialsummary').css('width', width);
                $('.x-grid3-body').css('width', '100%').css('float', 'none');
                $('.x-grid3-header-inner').css('width', width);
                $('.x-grid-panel').css('width', width).css('height', 'auto');
                $('.x-grid3-row').css('width', '100%');
                $('.x-grid3-row table').css('width', '100%');
                $('.x-grid3-scroller').css('width', width).css('height', GetInnerHeightAndWidthofClientBrowser(1, 60) - $('.x-grid3-header-title').height());
                $('.x-grid3').css('width', width).css('height', 'auto');
                $('.x-grid3-header-offset').css('width', '100%');
                $('.x-grid3-header-offset table').css('width', width);
                $('.x-panel-body').css('width', width).css('height', 'auto');
                $.each($('.x-grid3-header-offset table').find('td'), function (index, item) {
                    $(item).css('width', '');
                });


                $.each($('.x-grid3-row'), function (index, item) {
                    var lst = $(item).find('td');
                    for (var i = 0; i < lst.length; i++) {
                        $(lst[i]).width(Math.round($(lst1[i]).width()));

                    }


                    // $('.x-grid3-hd').css('width', 100);



                });


            }
        }
    } else if (iPadAgent || AndroidAgent) {
        if ($(window).width() > 650 && $(window).height() > 650) {
            $('#divfinancialsummaryCover').width($(window).width() * 82 / 100);
            if ($('#hdnreportid').val() == '18' || $('#hdnreportid').val() == '27' || $('#hdnreportid').val() == '30') {
                $('#divfinancialsummary').css('height', 'auto');
                $('#divfinancialsummary').css('width', $('#divfinancialsummaryCover').width());
                $('.x-grid3-header-inner').css('width', $('#divfinancialsummaryCover').width() - $('.x-grid3-header-title').width());
                $('.x-grid3-body').css('width', $('#divfinancialsummaryCover').width() - $('.x-grid3-header-title').width()).css('float', 'none').css('height', 'auto');
                $('.x-grid3-row').css('width', $('#divfinancialsummaryCover').width() - $('.x-grid3-header-title').width());
                $('.x-grid3-scroller').css('width', $('#divfinancialsummaryCover').width()).css('font-size', '10px !important').css('height', GetInnerHeightAndWidthofClientBrowser(1, 60) - $('.x-grid3-header-title').height());
                $('.x-pivotgrid').css('width', $('#divfinancialsummaryCover').width());
                $('.x-grid3-header-offset').css('width', '100%');
                $('.x-grid3-header-offset table').css('width', '100%');
                $('.x-grid3-row table').css('width', '100%');
                $('.x-pivotgrid').css('height', 'auto');
                $('.x-grid-panel').css('width', '100%');
                $('.x-panel-body').css('width', $('#divfinancialsummaryCover').width()).css('height', 'auto');;
            }
            else {
                //  $('#divfinancialsummary').css('max-height', GetInnerHeightAndWidthofClientBrowser(1, 60) - $('#spanGridTitle').height());


                var width = Math.round(lst1.length * 100 > $(window).width() * 82 / 100 ? lst1.length * 100 : $(window).width() * 82 / 100);

                $('#divfinancialsummary').css('height', 'auto');
                $('#divfinancialsummary').css('width', width);
                $('.x-grid3-body').css('width', '100%').css('float', 'none');
                $('.x-grid3-header-inner').css('width', width);
                $('.x-grid-panel').css('width', width).css('height', 'auto');
                $('.x-grid3-row').css('width', '100%');
                $('.x-grid3-row table').css('width', '100%');
                $('.x-grid3-scroller').css('width', width).css('height', GetInnerHeightAndWidthofClientBrowser(1, 60) - $('.x-grid3-header-title').height());
                $('.x-grid3').css('width', width).css('height', 'auto');
                $('.x-grid3-header-offset').css('width', '100%');
                $('.x-grid3-header-offset table').css('width', width);
                $('.x-panel-body').css('width', width).css('height', 'auto');
                $.each($('.x-grid3-header-offset table').find('td'), function (index, item) {
                    $(item).css('width', '');
                });


                $.each($('.x-grid3-row'), function (index, item) {
                    var lst = $(item).find('td');
                    for (var i = 0; i < lst.length; i++) {
                        $(lst[i]).width(Math.round($(lst1[i]).width()));

                    }
                });
            }
        }
    }

    if (!$('.x-grid3-scroller').hasClass('nano'))
        $('.x-grid3-scroller').addClass('nano')

    if (!iPhoneAgent && !iPadAgent && !AndroidAgent) {
        $('.x-grid3-scroller').css('overflow', 'hidden');
        $('.nano').perfectScrollbar('destroy');
        $('.nano').perfectScrollbar();
        setTimeout("$('.nano').perfectScrollbar('update');", 1500);
    }
    else {
        $('.nano').css('overflow', 'auto');

        $('.nano').perfectScrollbar('destroy');
    }
}

