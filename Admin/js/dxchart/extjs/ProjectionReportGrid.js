﻿///////////// Global Variables /////////////////////

var grid1, grid2, grid3;

/////////////////////////////////////////// ACTIVE CLIENTS CHARGES ////////////////////////////////////
function getActiveClientsChargesGrid(data, randeredDiv) {
    var store = new Ext.data.JsonStore({
        data: data,
        root: '',
        autoLoad: true,
        fields: [{ name: 'Active Clients', type: 'string' }, { name: 'Current Month Projection Charges', type: 'string' },
         { name: 'Where We Should Be As Of Today', type: 'string' }, { name: 'Where We Are As Of Today - PracticeExpress', type: 'string' },
         { name: 'Where We Are As Of Today - OtherSystem', type: 'string' }, { name: 'Where We Are Today', type: 'string' },
          { name: 'Difference', type: 'string' }, { name: 'Percentage', type: 'string'}]
    });

    // create the grid
    grid1 = new Ext.grid.GridPanel({
        store: store,
        id: 'id',
        columns: [
                    {
                        header: "Active Clients",
                        width: GetInnerHeightAndWidthofClientBrowser(0, 15),
                        dataIndex: 'Active Clients',
                        sortable: false,
                        renderer: function (value, metadata, record, rowIndex, colIndex, store) {
                            $.each($(".x-grid3-header thead td"), function (index, item) {
                                if (index == colIndex) {
                                    metadata.style = ' width:' + $(item).width() + 'px';
                                }
                            });
                            metadata.attr = 'title="' + value + '"';
                            return value;
                        }
                        //fixed:true
                    },
            {
                header: "Current Month Projection Charges",
                width: GetInnerHeightAndWidthofClientBrowser(0, 25),
                dataIndex: 'Current Month Projection Charges',
                sortable: false,
                renderer: function (value, metadata, record, rowIndex, colIndex, store) {
                    $.each($(".x-grid3-header thead td"), function (index, item) {
                        if (index == colIndex) {
                            metadata.style = 'text-align:right; width:' + $(item).width() + 'px';
                        }
                    });
                    metadata.attr = 'text-align:right; title="' + value + '"';
                    return value;
                }
                //fixed:true 
            },
            {
                header: "Where We Should Be As Of Today",
                width: GetInnerHeightAndWidthofClientBrowser(0, 25),
                dataIndex: 'Where We Should Be As Of Today',
                sortable: false,
                //fixed:true,
                renderer: function (value, metadata, record, rowIndex, colIndex, store) {
                    $.each($(".x-grid3-header thead td"), function (index, item) {
                        if (index == colIndex) {
                            metadata.style = 'text-align:right; width:' + $(item).width() + 'px';
                        }
                    });
                    metadata.attr = 'title="' + value + '"';
                    return value;
                }
            },
            {
                header: "Where We Are As Of Today - PracticeExpress",
                width: GetInnerHeightAndWidthofClientBrowser(0, 25),
                dataIndex: 'Where We Are As Of Today - PracticeExpress',
                sortable: false,
                renderer: function (value, metadata, record, rowIndex, colIndex, store) {
                    $.each($(".x-grid3-header thead td"), function (index, item) {
                        if (index == colIndex) {
                            metadata.style = 'text-align:right; width:' + $(item).width() + 'px';
                        }
                    });
                    metadata.attr = 'title="' + value + '"';
                    return value;
                }
                //                fixed:true
            },
            {
                header: "Where We Are As Of Today - OtherSystem",
                width: GetInnerHeightAndWidthofClientBrowser(0, 25),
                dataIndex: 'Where We Are As Of Today - OtherSystem',
                sortable: false,
                renderer: function (value, metadata, record, rowIndex, colIndex, store) {
                    $.each($(".x-grid3-header thead td"), function (index, item) {
                        if (index == colIndex) {
                            metadata.style = 'text-align:right; width:' + $(item).width() + 'px';
                        }
                    });
                    metadata.attr = 'title="' + value + '"';
                    return value;
                }
                //                fixed:true
            },
            {
                header: "Where We Are Today",
                width: GetInnerHeightAndWidthofClientBrowser(0, 20),
                dataIndex: 'Where We Are Today',
                sortable: false,
                renderer: function (value, metadata, record, rowIndex, colIndex, store) {
                    $.each($(".x-grid3-header thead td"), function (index, item) {
                        if (index == colIndex) {
                            metadata.style = 'text-align:right; width:' + $(item).width() + 'px';
                        }
                    });
                    metadata.attr = 'title="' + value + '"';
                    return value;
                }
                //                fixed:true
            },
            {
                header: "Difference",
                width: GetInnerHeightAndWidthofClientBrowser(0, 12),
                dataIndex: 'Difference',
                sortable: false,
                renderer: function (value, metadata, record, rowIndex, colIndex, store) {
                    $.each($(".x-grid3-header thead td"), function (index, item) {
                        if (index == colIndex) {
                            metadata.style = 'text-align:right; width:' + $(item).width() + 'px';
                        }
                    });
                    metadata.attr = 'title="' + value + '"';
                    return value;
                }
                //                fixed:true
            },
            {
                header: "Percentage",
                width: GetInnerHeightAndWidthofClientBrowser(0, 12),
                dataIndex: 'Percentage',
                sortable: false,
                renderer: function (value, metadata, record, rowIndex, colIndex, store) {
                    $.each($(".x-grid3-header thead td"), function (index, item) {
                        if (index == colIndex) {
                            metadata.style = 'text-align:right; width:' + $(item).width() + 'px';
                        }
                    });
                    metadata.attr = 'title="' + value + '"';
                    return value;
                }
                //                fixed:true
            }
        ],



        renderTo: randeredDiv,
        autoWidth: true,
        viewConfig: { forceFit: true },
        height: GetInnerHeightAndWidthofClientBrowser(1, 60),
        title: '',
        header: false


    });
    // store.load();

    setTimeout("$('.x-grid3-scroller').css('height', '100%');", 500);
    setTimeout("$('.x-panel-body').css('height', '100%');", 500);
    setTimeout("$('.x-grid3').css('height', '100%');", 500);
    setTimeout("$('#'+randeredDiv).css('height', '100%');", 500);


}


function getConversionClientsChargesGrid(data, randeredDiv) {
    var store = new Ext.data.JsonStore({
        data: data,
        root: '',
        autoLoad: true,
        fields: [{ name: 'Conversion Clients', type: 'string' }, { name: 'Current Month Projection Charges', type: 'string' },
         { name: 'Where We Should Be As Of Today', type: 'string' }, { name: 'Where We Are As Of Today - PracticeExpress', type: 'string' },
         { name: 'Where We Are As Of Today - OtherSystem', type: 'string' }, { name: 'Where We Are Today', type: 'string' },
          { name: 'Difference', type: 'string' }, { name: 'Percentage', type: 'string'}]
    });

    // create the grid
    grid2 = new Ext.grid.GridPanel({
        store: store,
        id: 'id',
        columns: [
                    {
                        header: "Conversion Clients",
                        width: GetInnerHeightAndWidthofClientBrowser(0, 15),
                        dataIndex: 'Conversion Clients',
                        sortable: false,
                        renderer: function (value, metadata, record, rowIndex, colIndex, store) {
                            $.each($(".x-grid3-header thead td"), function (index, item) {
                                if (index == colIndex) {
                                    metadata.style = 'min-width:100px; width:' + $(item).width() + 'px';
                                }
                            });
                            metadata.attr = 'title="' + value + '"';
                            return value;
                        }
                        //fixed:true
                    },
            {
                header: "Current Month Projection Charges",
                width: GetInnerHeightAndWidthofClientBrowser(0, 25),
                dataIndex: 'Current Month Projection Charges',
                sortable: false,
                renderer: function (value, metadata, record, rowIndex, colIndex, store) {
                    $.each($(".x-grid3-header thead td"), function (index, item) {
                        if (index == colIndex) {
                            metadata.style = 'text-align:right; min-width:200px; width:' + $(item).width() + 'px';
                        }
                    });
                    metadata.attr = 'text-align:right; title="' + value + '"';
                    return value;
                }
                //fixed:true 
            },
            {
                header: "Where We Should Be As Of Today",
                width: GetInnerHeightAndWidthofClientBrowser(0, 25),
                dataIndex: 'Where We Should Be As Of Today',
                sortable: false,
                //fixed:true,
                renderer: function (value, metadata, record, rowIndex, colIndex, store) {
                    $.each($(".x-grid3-header thead td"), function (index, item) {
                        if (index == colIndex) {
                            metadata.style = 'text-align:right; min-width:200px; width:' + $(item).width() + 'px';
                        }
                    });
                    metadata.attr = 'title="' + value + '"';
                    return value;
                }
            },
            {
                header: "Where We Are As Of Today - PracticeExpress",
                width: GetInnerHeightAndWidthofClientBrowser(0, 25),
                dataIndex: 'Where We Are As Of Today - PracticeExpress',
                sortable: false,
                renderer: function (value, metadata, record, rowIndex, colIndex, store) {
                    $.each($(".x-grid3-header thead td"), function (index, item) {
                        if (index == colIndex) {
                            metadata.style = 'text-align:right; min-width:200px; width:' + $(item).width() + 'px';
                        }
                    });
                    metadata.attr = 'title="' + value + '"';
                    return value;
                }
                //                fixed:true
            },
            {
                header: "Where We Are As Of Today - OtherSystem",
                width: GetInnerHeightAndWidthofClientBrowser(0, 25),
                dataIndex: 'Where We Are As Of Today - OtherSystem',
                sortable: false,
                renderer: function (value, metadata, record, rowIndex, colIndex, store) {
                    $.each($(".x-grid3-header thead td"), function (index, item) {
                        if (index == colIndex) {
                            metadata.style = 'text-align:right; min-width:200px; width:' + $(item).width() + 'px';
                        }
                    });
                    metadata.attr = 'title="' + value + '"';
                    return value;
                }
                //                fixed:true
            },
            {
                header: "Where We Are Today",
                width: GetInnerHeightAndWidthofClientBrowser(0, 20),
                dataIndex: 'Where We Are Today',
                sortable: false,
                renderer: function (value, metadata, record, rowIndex, colIndex, store) {
                    $.each($(".x-grid3-header thead td"), function (index, item) {
                        if (index == colIndex) {
                            metadata.style = 'text-align:right; min-width:200px; width:' + $(item).width() + 'px';
                        }
                    });
                    metadata.attr = 'title="' + value + '"';
                    return value;
                }
                //                fixed:true
            },
            {
                header: "Difference",
                width: GetInnerHeightAndWidthofClientBrowser(0, 12),
                dataIndex: 'Difference',
                sortable: false,
                renderer: function (value, metadata, record, rowIndex, colIndex, store) {
                    $.each($(".x-grid3-header thead td"), function (index, item) {
                        if (index == colIndex) {
                            metadata.style = 'text-align:right; min-width:100px; width:' + $(item).width() + 'px';
                        }
                    });
                    metadata.attr = 'title="' + value + '"';
                    return value;
                }
                //                fixed:true
            },
            {
                header: "Percentage",
                width: GetInnerHeightAndWidthofClientBrowser(0, 12),
                dataIndex: 'Percentage',
                sortable: false,
                renderer: function (value, metadata, record, rowIndex, colIndex, store) {
                    $.each($(".x-grid3-header thead td"), function (index, item) {
                        if (index == colIndex) {
                            metadata.style = 'text-align:right; min-width:100px; width:' + $(item).width() + 'px';
                        }
                    });
                    metadata.attr = 'title="' + value + '"';
                    return value;
                }
                //                fixed:true
            }
        ],



        renderTo: randeredDiv,
        autoWidth: true,
        viewConfig: { forceFit: true },
        height: GetInnerHeightAndWidthofClientBrowser(1, 60),
        title: '',
        header: false


    });
    // store.load();

    setTimeout("$('.x-grid3-scroller').css('height', '100%');", 500);
    setTimeout("$('.x-panel-body').css('height', '100%');", 500);
    setTimeout("$('.x-grid3').css('height', '100%');", 500);
    setTimeout("$('#'+randeredDiv).css('height', '100%');", 500);


}

function getTotalChargesGrid(data, randeredDiv) {
    var store = new Ext.data.JsonStore({
        data: data,
        root: '',
        autoLoad: true,
        fields: [{ name: 'Active Clients', type: 'string' }, { name: 'Current Month Projection Charges', type: 'string' },
         { name: 'Where We Should Be As Of Today', type: 'string' }, { name: 'Where We Are As Of Today - PracticeExpress', type: 'string' },
         { name: 'Where We Are As Of Today - OtherSystem', type: 'string' }, { name: 'Where We Are Today', type: 'string' },
          { name: 'Difference', type: 'string' }, { name: 'Percentage', type: 'string'}]
    });

    // create the grid
    grid3 = new Ext.grid.GridPanel({
        store: store,
        id: 'id',
        columns: [
                    {
                        header: "Active Clients",
                        width: GetInnerHeightAndWidthofClientBrowser(0, 15),
                        dataIndex: 'Active Clients',
                        sortable: false,
                        renderer: function (value, metadata, record, rowIndex, colIndex, store) {
                            $.each($(".x-grid3-header thead td"), function (index, item) {
                                if (index == colIndex) {
                                    metadata.style = ' width:' + $(item).width() + 'px';
                                }
                            });
                            metadata.attr = 'title="' + value + '"';
                            return value;
                        }
                        //fixed:true
                    },
            {
                header: "Current Month Projection Charges",
                width: GetInnerHeightAndWidthofClientBrowser(0, 25),
                dataIndex: 'Current Month Projection Charges',
                sortable: false,
                renderer: function (value, metadata, record, rowIndex, colIndex, store) {
                    $.each($(".x-grid3-header thead td"), function (index, item) {
                        if (index == colIndex) {
                            metadata.style = 'text-align:right; width:' + $(item).width() + 'px';
                        }
                    });
                    metadata.attr = 'text-align:right; title="' + value + '"';
                    return value;
                }
                //fixed:true 
            },
            {
                header: "Where We Should Be As Of Today",
                width: GetInnerHeightAndWidthofClientBrowser(0, 25),
                dataIndex: 'Where We Should Be As Of Today',
                sortable: false,
                //fixed:true,
                renderer: function (value, metadata, record, rowIndex, colIndex, store) {
                    $.each($(".x-grid3-header thead td"), function (index, item) {
                        if (index == colIndex) {
                            metadata.style = 'text-align:right; width:' + $(item).width() + 'px';
                        }
                    });
                    metadata.attr = 'title="' + value + '"';
                    return value;
                }
            },
            {
                header: "Where We Are As Of Today - PracticeExpress",
                width: GetInnerHeightAndWidthofClientBrowser(0, 25),
                dataIndex: 'Where We Are As Of Today - PracticeExpress',
                sortable: false,
                renderer: function (value, metadata, record, rowIndex, colIndex, store) {
                    $.each($(".x-grid3-header thead td"), function (index, item) {
                        if (index == colIndex) {
                            metadata.style = 'text-align:right; width:' + $(item).width() + 'px';
                        }
                    });
                    metadata.attr = 'title="' + value + '"';
                    return value;
                }
                //                fixed:true
            },
            {
                header: "Where We Are As Of Today - OtherSystem",
                width: GetInnerHeightAndWidthofClientBrowser(0, 25),
                dataIndex: 'Where We Are As Of Today - OtherSystem',
                sortable: false,
                renderer: function (value, metadata, record, rowIndex, colIndex, store) {
                    $.each($(".x-grid3-header thead td"), function (index, item) {
                        if (index == colIndex) {
                            metadata.style = 'text-align:right; width:' + $(item).width() + 'px';
                        }
                    });
                    metadata.attr = 'title="' + value + '"';
                    return value;
                }
                //                fixed:true
            },
            {
                header: "Where We Are Today",
                width: GetInnerHeightAndWidthofClientBrowser(0, 20),
                dataIndex: 'Where We Are Today',
                sortable: false,
                renderer: function (value, metadata, record, rowIndex, colIndex, store) {
                    $.each($(".x-grid3-header thead td"), function (index, item) {
                        if (index == colIndex) {
                            metadata.style = 'text-align:right; width:' + $(item).width() + 'px';
                        }
                    });
                    metadata.attr = 'title="' + value + '"';
                    return value;
                }
                //                fixed:true
            },
            {
                header: "Difference",
                width: GetInnerHeightAndWidthofClientBrowser(0, 12),
                dataIndex: 'Difference',
                sortable: false,
                renderer: function (value, metadata, record, rowIndex, colIndex, store) {
                    $.each($(".x-grid3-header thead td"), function (index, item) {
                        if (index == colIndex) {
                            metadata.style = 'text-align:right; width:' + $(item).width() + 'px';
                        }
                    });
                    metadata.attr = 'title="' + value + '"';
                    return value;
                }
                //                fixed:true
            },
            {
                header: "Percentage",
                width: GetInnerHeightAndWidthofClientBrowser(0, 12),
                dataIndex: 'Percentage',
                sortable: false,
                renderer: function (value, metadata, record, rowIndex, colIndex, store) {
                    $.each($(".x-grid3-header thead td"), function (index, item) {
                        if (index == colIndex) {
                            metadata.style = 'text-align:right; width:' + $(item).width() + 'px';
                        }
                    });
                    metadata.attr = 'title="' + value + '"';
                    return value;
                }
                //                fixed:true
            }
        ],



        renderTo: randeredDiv,
        autoWidth: true,
        viewConfig: { forceFit: true },
        height: GetInnerHeightAndWidthofClientBrowser(1, 58),
        title: '',
        header: false


    });
    // store.load();

    setTimeout("$('.x-grid3-scroller').css('height', '100%');", 500);
    setTimeout("$('.x-panel-body').css('height', '100%');", 500);
    setTimeout("$('.x-grid3').css('height', '100%');", 500);
    setTimeout("$('#'+randeredDiv).css('height', '100%');", 500);


}



////////////////////// DASHBOARD GRID ////////////////////////////////////////////////////////////////
function getActualVsBenchmark(fromdate, todate) {
    //    Ext.onReady(function () {

    $('#divDashboardGridContainer').empty();
    var SaleRecord = Ext.data.Record.create([
        { name: 'name', type: 'string' },
        { name: 'value', type: 'string' },
        { name: 'month', type: 'string' },
        { name: 'rowgrp', type: 'int' },
        { name: 'colgrp', type: 'float' },
        { name: 'fullsort', type: 'int' }
    ]);

        var myStore = new Ext.data.Store({
            url: '../ProjectionReportdata.aspx?sp=getActualVsBenchmark&fromdate=' + fromdate + '&todate=' + todate,
            autoLoad: true,
            reader: new Ext.data.JsonReader({
                root: 'Table',
                idProperty: id
            }, SaleRecord),
            listeners: {
                load: {
                    fn: function () {

                        if (myStore.getCount() > 0) {
                            //$('.x-grid3-row-headers table tr').find('td:first').css('display', 'none');
                             $('.x-grid3-header-offset table').find('tr:first').css('display', 'none');
                          
                            $.each($('.x-grid3-row-headers table tr'), function (index, item) {

                                var lst = $(item).find('td');
                                if (lst.length == 3) {
                                    $(lst[0]).css('display', 'none');
                                    $(lst[1]).css('display', 'none');
                                }
                                else if (lst.length == 2)
                                    $(lst[0]).css('display', 'none');

                            });
                            $('.x-grid3-cell-inner').css('padding', '0px');
                            $('.x-grid3-row-headers').css('font-weight', 'bold').css('text-align', 'left');

                            $.each($('.x-grid3-row-table'), function (index, item) {
                                $.each($('.x-grid3-row-headers table tr'), function (ind, itm) {
                                    if (index == ind) {
                                        $(item).find('.divfs').height($(itm).height() - 2);
                                    }

                                });

                            });

                            var lst1 = $('.x-grid3-header-offset table').find('tr:first').find('td');
                            $('.x-grid3-header-inner').css('width', lst1.length * 100);
                            $('.x-grid3-body').css('width', lst1.length * 100).css('float', 'none').css('height', 'auto');
                            $('.x-grid3-row').css('width', lst1.length * 100);
                            $('.x-grid3-scroller').css('width', lst1.length * 100 + $('.x-grid3-header-title').width()).css('height', 'auto');
                            $('.x-pivotgrid').css('width', lst1.length * 100 + $('.x-grid3-header-title').width());
                            $('.x-grid3-header-offset').css('width', '100%');
                            $('.x-grid3-header-offset table').css('width', '100%');
                            $('.x-grid3-row table').css('width', '100%');
                            $('.x-pivotgrid').css('height', 'auto');
                            HideLoading();
                            setTimeout("getGridJsScrollBar();", 1000);
                            //setTimeout("$('.x-grid3-scroller').perfectScrollbar('destroy');", 2000);
                        }
                        else
                            shownodata();
                    }

                },
                exception: function (misc) {
                    shownodata();
                }

            }
        });

    var grid = new Ext.grid.PivotGrid({
        title: '',
        header: false,
        id: 'FinancialSummary',
        //width: document.getElementById('divfinancialsummary').style.width,
        //height: GetInnerHeightAndWidthofClientBrowser(1, 60),
        renderTo: 'divDashboardGridContainer',
        store: myStore,
        aggregator: 'sum',
        measure: 'value',
        viewConfig: {
            title: ''
        },
        aggregator: function (records, measure) {
            for (var i = 0; i < records.length; i++) {
                return '<div class="divfs" style="text-align:right;" title="' + records[i].get(measure) + '">' + records[i].get(measure) + '</div>';
            }

        },


        leftAxis: [
            {
                dataIndex: 'fullsort',
                sortable: false
            },
                 {
                     dataIndex: 'rowgrp',
                     sortable: false
                 },
                 {
                     dataIndex: 'name',
                     width: GetInnerHeightAndWidthofClientBrowser(0, 5),
                     sortable: false
                 }],

        topAxis: [
            {
                dataIndex: 'colgrp',
                sortable: false
            },
             {
                 dataIndex: 'month'
           }]
    });
    grid.view.refresh(true);

}



/////////////////////////////////////////// ACTIVE CLIENTS CHARGES ////////////////////////////////////
function getActiveClientsPaymentsGrid(data, randeredDiv) {
    var store = new Ext.data.JsonStore({
        data: data,
        root: '',
        autoLoad: true,
        fields: [{ name: 'Active Clients', type: 'string' }, { name: 'Current Month Projection Collections', type: 'string' },
         { name: 'Where We Should Be As Of Today', type: 'string' }, { name: 'Where We Are As Of Today - PracticeExpress', type: 'string' },
         { name: 'Where We Are As Of Today - OtherSystem', type: 'string' }, { name: 'Where We Are Today', type: 'string' },
          { name: 'Difference', type: 'string' }, { name: 'Percentage', type: 'string'}]
    });

    // create the grid
    grid1 = new Ext.grid.GridPanel({
        store: store,
        id: 'id',
        columns: [
                    {
                        header: "Active Clients",
                        width: GetInnerHeightAndWidthofClientBrowser(0, 15),
                        dataIndex: 'Active Clients',
                        sortable: false,
                        renderer: function (value, metadata, record, rowIndex, colIndex, store) {
                            $.each($(".x-grid3-header thead td"), function (index, item) {
                                if (index == colIndex) {
                                    metadata.style = ' width:' + $(item).width() + 'px';
                                }
                            });
                            metadata.attr = 'title="' + value + '"';
                            return value;
                        }
                        //fixed:true
                    },
            {
                header: "Current Month Projection Collections",
                width: GetInnerHeightAndWidthofClientBrowser(0, 25),
                dataIndex: 'Current Month Projection Collections',
                sortable: false,
                renderer: function (value, metadata, record, rowIndex, colIndex, store) {
                    $.each($(".x-grid3-header thead td"), function (index, item) {
                        if (index == colIndex) {
                            metadata.style = 'text-align:right; width:' + $(item).width() + 'px';
                        }
                    });
                    metadata.attr = 'text-align:right; title="' + value + '"';
                    return value;
                }
                //fixed:true 
            },
            {
                header: "Where We Should Be As Of Today",
                width: GetInnerHeightAndWidthofClientBrowser(0, 25),
                dataIndex: 'Where We Should Be As Of Today',
                sortable: false,
                //fixed:true,
                renderer: function (value, metadata, record, rowIndex, colIndex, store) {
                    $.each($(".x-grid3-header thead td"), function (index, item) {
                        if (index == colIndex) {
                            metadata.style = 'text-align:right; width:' + $(item).width() + 'px';
                        }
                    });
                    metadata.attr = 'title="' + value + '"';
                    return value;
                }
            },
            {
                header: "Where We Are As Of Today - PracticeExpress",
                width: GetInnerHeightAndWidthofClientBrowser(0, 25),
                dataIndex: 'Where We Are As Of Today - PracticeExpress',
                sortable: false,
                renderer: function (value, metadata, record, rowIndex, colIndex, store) {
                    $.each($(".x-grid3-header thead td"), function (index, item) {
                        if (index == colIndex) {
                            metadata.style = 'text-align:right; width:' + $(item).width() + 'px';
                        }
                    });
                    metadata.attr = 'title="' + value + '"';
                    return value;
                }
                //                fixed:true
            },
            {
                header: "Where We Are As Of Today - OtherSystem",
                width: GetInnerHeightAndWidthofClientBrowser(0, 25),
                dataIndex: 'Where We Are As Of Today - OtherSystem',
                sortable: false,
                renderer: function (value, metadata, record, rowIndex, colIndex, store) {
                    $.each($(".x-grid3-header thead td"), function (index, item) {
                        if (index == colIndex) {
                            metadata.style = 'text-align:right; width:' + $(item).width() + 'px';
                        }
                    });
                    metadata.attr = 'title="' + value + '"';
                    return value;
                }
                //                fixed:true
            },
            {
                header: "Where We Are Today",
                width: GetInnerHeightAndWidthofClientBrowser(0, 20),
                dataIndex: 'Where We Are Today',
                sortable: false,
                renderer: function (value, metadata, record, rowIndex, colIndex, store) {
                    $.each($(".x-grid3-header thead td"), function (index, item) {
                        if (index == colIndex) {
                            metadata.style = 'text-align:right; width:' + $(item).width() + 'px';
                        }
                    });
                    metadata.attr = 'title="' + value + '"';
                    return value;
                }
                //                fixed:true
            },
            {
                header: "Difference",
                width: GetInnerHeightAndWidthofClientBrowser(0, 12),
                dataIndex: 'Difference',
                sortable: false,
                renderer: function (value, metadata, record, rowIndex, colIndex, store) {
                    $.each($(".x-grid3-header thead td"), function (index, item) {
                        if (index == colIndex) {
                            metadata.style = 'text-align:right; width:' + $(item).width() + 'px';
                        }
                    });
                    metadata.attr = 'title="' + value + '"';
                    return value;
                }
                //                fixed:true
            },
            {
                header: "Percentage",
                width: GetInnerHeightAndWidthofClientBrowser(0, 12),
                dataIndex: 'Percentage',
                sortable: false,
                renderer: function (value, metadata, record, rowIndex, colIndex, store) {
                    $.each($(".x-grid3-header thead td"), function (index, item) {
                        if (index == colIndex) {
                            metadata.style = 'text-align:right; width:' + $(item).width() + 'px';
                        }
                    });
                    metadata.attr = 'title="' + value + '"';
                    return value;
                }
                //                fixed:true
            }
        ],



        renderTo: randeredDiv,
        autoWidth: true,
        viewConfig: { forceFit: true },
        height: GetInnerHeightAndWidthofClientBrowser(1, 60),
        title: '',
        header: false


    });
    // store.load();

    setTimeout("$('.x-grid3-scroller').css('height', '100%');", 500);
    setTimeout("$('.x-panel-body').css('height', '100%');", 500);
    setTimeout("$('.x-grid3').css('height', '100%');", 500);
    setTimeout("$('#'+randeredDiv).css('height', '100%');", 500);


}


function getConversionClientsPaymentsGrid(data, randeredDiv) {
    var store = new Ext.data.JsonStore({
        data: data,
        root: '',
        autoLoad: true,
        fields: [{ name: 'Conversion Clients', type: 'string' }, { name: 'Current Month Projection Collections', type: 'string' },
         { name: 'Where We Should Be As Of Today', type: 'string' }, { name: 'Where We Are As Of Today - PracticeExpress', type: 'string' },
         { name: 'Where We Are As Of Today - OtherSystem', type: 'string' }, { name: 'Where We Are Today', type: 'string' },
          { name: 'Difference', type: 'string' }, { name: 'Percentage', type: 'string'}]
    });

    // create the grid
    grid2 = new Ext.grid.GridPanel({
        store: store,
        id: 'id',
        columns: [
                    {
                        header: "Conversion Clients",
                        width: GetInnerHeightAndWidthofClientBrowser(0, 15),
                        dataIndex: 'Conversion Clients',
                        sortable: false,
                        renderer: function (value, metadata, record, rowIndex, colIndex, store) {
                            $.each($(".x-grid3-header thead td"), function (index, item) {
                                if (index == colIndex) {
                                    metadata.style = 'min-width:100px; width:' + $(item).width() + 'px';
                                }
                            });
                            metadata.attr = 'title="' + value + '"';
                            return value;
                        }
                        //fixed:true
                    },
            {
                header: "Current Month Projection Collections",
                width: GetInnerHeightAndWidthofClientBrowser(0, 25),
                dataIndex: 'Current Month Projection Collections',
                sortable: false,
                renderer: function (value, metadata, record, rowIndex, colIndex, store) {
                    $.each($(".x-grid3-header thead td"), function (index, item) {
                        if (index == colIndex) {
                            metadata.style = 'text-align:right; min-width:200px; width:' + $(item).width() + 'px';
                        }
                    });
                    metadata.attr = 'text-align:right; title="' + value + '"';
                    return value;
                }
                //fixed:true 
            },
            {
                header: "Where We Should Be As Of Today",
                width: GetInnerHeightAndWidthofClientBrowser(0, 25),
                dataIndex: 'Where We Should Be As Of Today',
                sortable: false,
                //fixed:true,
                renderer: function (value, metadata, record, rowIndex, colIndex, store) {
                    $.each($(".x-grid3-header thead td"), function (index, item) {
                        if (index == colIndex) {
                            metadata.style = 'text-align:right; min-width:200px; width:' + $(item).width() + 'px';
                        }
                    });
                    metadata.attr = 'title="' + value + '"';
                    return value;
                }
            },
            {
                header: "Where We Are As Of Today - PracticeExpress",
                width: GetInnerHeightAndWidthofClientBrowser(0, 25),
                dataIndex: 'Where We Are As Of Today - PracticeExpress',
                sortable: false,
                renderer: function (value, metadata, record, rowIndex, colIndex, store) {
                    $.each($(".x-grid3-header thead td"), function (index, item) {
                        if (index == colIndex) {
                            metadata.style = 'text-align:right; min-width:200px; width:' + $(item).width() + 'px';
                        }
                    });
                    metadata.attr = 'title="' + value + '"';
                    return value;
                }
                //                fixed:true
            },
            {
                header: "Where We Are As Of Today - OtherSystem",
                width: GetInnerHeightAndWidthofClientBrowser(0, 25),
                dataIndex: 'Where We Are As Of Today - OtherSystem',
                sortable: false,
                renderer: function (value, metadata, record, rowIndex, colIndex, store) {
                    $.each($(".x-grid3-header thead td"), function (index, item) {
                        if (index == colIndex) {
                            metadata.style = 'text-align:right; min-width:200px; width:' + $(item).width() + 'px';
                        }
                    });
                    metadata.attr = 'title="' + value + '"';
                    return value;
                }
                //                fixed:true
            },
            {
                header: "Where We Are Today",
                width: GetInnerHeightAndWidthofClientBrowser(0, 20),
                dataIndex: 'Where We Are Today',
                sortable: false,
                renderer: function (value, metadata, record, rowIndex, colIndex, store) {
                    $.each($(".x-grid3-header thead td"), function (index, item) {
                        if (index == colIndex) {
                            metadata.style = 'text-align:right; min-width:200px; width:' + $(item).width() + 'px';
                        }
                    });
                    metadata.attr = 'title="' + value + '"';
                    return value;
                }
                //                fixed:true
            },
            {
                header: "Difference",
                width: GetInnerHeightAndWidthofClientBrowser(0, 12),
                dataIndex: 'Difference',
                sortable: false,
                renderer: function (value, metadata, record, rowIndex, colIndex, store) {
                    $.each($(".x-grid3-header thead td"), function (index, item) {
                        if (index == colIndex) {
                            metadata.style = 'text-align:right; min-width:100px; width:' + $(item).width() + 'px';
                        }
                    });
                    metadata.attr = 'title="' + value + '"';
                    return value;
                }
                //                fixed:true
            },
            {
                header: "Percentage",
                width: GetInnerHeightAndWidthofClientBrowser(0, 12),
                dataIndex: 'Percentage',
                sortable: false,
                renderer: function (value, metadata, record, rowIndex, colIndex, store) {
                    $.each($(".x-grid3-header thead td"), function (index, item) {
                        if (index == colIndex) {
                            metadata.style = 'text-align:right; min-width:100px; width:' + $(item).width() + 'px';
                        }
                    });
                    metadata.attr = 'title="' + value + '"';
                    return value;
                }
                //                fixed:true
            }
        ],



        renderTo: randeredDiv,
        autoWidth: true,
        viewConfig: { forceFit: true },
        height: GetInnerHeightAndWidthofClientBrowser(1, 60),
        title: '',
        header: false


    });
    // store.load();

    setTimeout("$('.x-grid3-scroller').css('height', '100%');", 500);
    setTimeout("$('.x-panel-body').css('height', '100%');", 500);
    setTimeout("$('.x-grid3').css('height', '100%');", 500);
    setTimeout("$('#'+randeredDiv).css('height', '100%');", 500);


}

function getTotalPaymentsGrid(data, randeredDiv) {
    var store = new Ext.data.JsonStore({
        data: data,
        root: '',
        autoLoad: true,
        fields: [{ name: 'Active Clients', type: 'string' }, { name: 'Current Month Projection Collections', type: 'string' },
         { name: 'Where We Should Be As Of Today', type: 'string' }, { name: 'Where We Are As Of Today - PracticeExpress', type: 'string' },
         { name: 'Where We Are As Of Today - OtherSystem', type: 'string' }, { name: 'Where We Are Today', type: 'string' },
          { name: 'Difference', type: 'string' }, { name: 'Percentage', type: 'string'}]
    });

    // create the grid
    grid3 = new Ext.grid.GridPanel({
        store: store,
        id: 'id',
        columns: [
                    {
                        header: "Active Clients",
                        width: GetInnerHeightAndWidthofClientBrowser(0, 15),
                        dataIndex: 'Active Clients',
                        sortable: false,
                        renderer: function (value, metadata, record, rowIndex, colIndex, store) {
                            $.each($(".x-grid3-header thead td"), function (index, item) {
                                if (index == colIndex) {
                                    metadata.style = ' width:' + $(item).width() + 'px';
                                }
                            });
                            metadata.attr = 'title="' + value + '"';
                            return value;
                        }
                        //fixed:true
                    },
            {
                header: "Current Month Projection Collections",
                width: GetInnerHeightAndWidthofClientBrowser(0, 25),
                dataIndex: 'Current Month Projection Collections',
                sortable: false,
                renderer: function (value, metadata, record, rowIndex, colIndex, store) {
                    $.each($(".x-grid3-header thead td"), function (index, item) {
                        if (index == colIndex) {
                            metadata.style = 'text-align:right; width:' + $(item).width() + 'px';
                        }
                    });
                    metadata.attr = 'text-align:right; title="' + value + '"';
                    return value;
                }
                //fixed:true 
            },
            {
                header: "Where We Should Be As Of Today",
                width: GetInnerHeightAndWidthofClientBrowser(0, 25),
                dataIndex: 'Where We Should Be As Of Today',
                sortable: false,
                //fixed:true,
                renderer: function (value, metadata, record, rowIndex, colIndex, store) {
                    $.each($(".x-grid3-header thead td"), function (index, item) {
                        if (index == colIndex) {
                            metadata.style = 'text-align:right; width:' + $(item).width() + 'px';
                        }
                    });
                    metadata.attr = 'title="' + value + '"';
                    return value;
                }
            },
            {
                header: "Where We Are As Of Today - PracticeExpress",
                width: GetInnerHeightAndWidthofClientBrowser(0, 25),
                dataIndex: 'Where We Are As Of Today - PracticeExpress',
                sortable: false,
                renderer: function (value, metadata, record, rowIndex, colIndex, store) {
                    $.each($(".x-grid3-header thead td"), function (index, item) {
                        if (index == colIndex) {
                            metadata.style = 'text-align:right; width:' + $(item).width() + 'px';
                        }
                    });
                    metadata.attr = 'title="' + value + '"';
                    return value;
                }
                //                fixed:true
            },
            {
                header: "Where We Are As Of Today - OtherSystem",
                width: GetInnerHeightAndWidthofClientBrowser(0, 25),
                dataIndex: 'Where We Are As Of Today - OtherSystem',
                sortable: false,
                renderer: function (value, metadata, record, rowIndex, colIndex, store) {
                    $.each($(".x-grid3-header thead td"), function (index, item) {
                        if (index == colIndex) {
                            metadata.style = 'text-align:right; width:' + $(item).width() + 'px';
                        }
                    });
                    metadata.attr = 'title="' + value + '"';
                    return value;
                }
                //                fixed:true
            },
            {
                header: "Where We Are Today",
                width: GetInnerHeightAndWidthofClientBrowser(0, 20),
                dataIndex: 'Where We Are Today',
                sortable: false,
                renderer: function (value, metadata, record, rowIndex, colIndex, store) {
                    $.each($(".x-grid3-header thead td"), function (index, item) {
                        if (index == colIndex) {
                            metadata.style = 'text-align:right; width:' + $(item).width() + 'px';
                        }
                    });
                    metadata.attr = 'title="' + value + '"';
                    return value;
                }
                //                fixed:true
            },
            {
                header: "Difference",
                width: GetInnerHeightAndWidthofClientBrowser(0, 12),
                dataIndex: 'Difference',
                sortable: false,
                renderer: function (value, metadata, record, rowIndex, colIndex, store) {
                    $.each($(".x-grid3-header thead td"), function (index, item) {
                        if (index == colIndex) {
                            metadata.style = 'text-align:right; width:' + $(item).width() + 'px';
                        }
                    });
                    metadata.attr = 'title="' + value + '"';
                    return value;
                }
                //                fixed:true
            },
            {
                header: "Percentage",
                width: GetInnerHeightAndWidthofClientBrowser(0, 12),
                dataIndex: 'Percentage',
                sortable: false,
                renderer: function (value, metadata, record, rowIndex, colIndex, store) {
                    $.each($(".x-grid3-header thead td"), function (index, item) {
                        if (index == colIndex) {
                            metadata.style = 'text-align:right; width:' + $(item).width() + 'px';
                        }
                    });
                    metadata.attr = 'title="' + value + '"';
                    return value;
                }
                //                fixed:true
            }
        ],



        renderTo: randeredDiv,
        autoWidth: true,
        viewConfig: { forceFit: true },
        height: GetInnerHeightAndWidthofClientBrowser(1, 58),
        title: '',
        header: false


    });
    // store.load();

    setTimeout("$('.x-grid3-scroller').css('height', '100%');", 500);
    setTimeout("$('.x-panel-body').css('height', '100%');", 500);
    setTimeout("$('.x-grid3').css('height', '100%');", 500);
    setTimeout("$('#'+randeredDiv).css('height', '100%');", 500);


}








///////////////////////////////////////////////////////////////////////////////////////////////////////
function getGridJsScrollBar() {
   /// if (!$('.x-grid3-scroller').hasClass('nano'))
      //  $('.x-grid3-scroller').addClass('nano')

    if (navigator.userAgent.match(/iPhone/i) == null && navigator.userAgent.match(/iPad/i) == null) {
       //  $('.x-grid3-scroller').css('overflow', 'hidden');
         $('.nano').perfectScrollbar('destroy');
         $('.nano').perfectScrollbar();
         setTimeout("$('.nano').perfectScrollbar('update');", 500);
    }
    else {
        $('.nano').css('overflow', 'auto');
        $('.nano').perfectScrollbar('destroy');
        }
  
}

/////////////////////////////WINDOW RESIZE EVENT///////////////////////////////////////////////////////////////
Ext.EventManager.onWindowResize(function () {
    if (grid1 && grid2 && grid3) {
        if (navigator.userAgent.match(/iPhone/i) == null && navigator.userAgent.match(/iPad/i) == null) {
            grid1.setWidth(GetInnerHeightAndWidthofClientBrowser(0, 80));
            grid2.setWidth(GetInnerHeightAndWidthofClientBrowser(0, 80));
            grid3.setWidth(GetInnerHeightAndWidthofClientBrowser(0, 80));
            //alert(GetInnerHeightAndWidthofClientBrowser(0,80));
            $('#divfinancialsummary').css('width', GetInnerHeightAndWidthofClientBrowser(0, 80));
        }
        else {
            grid1.setWidth(GetInnerHeightAndWidthofClientBrowser(0, 98));
            grid2.setWidth(GetInnerHeightAndWidthofClientBrowser(0, 98));
            grid3.setWidth(GetInnerHeightAndWidthofClientBrowser(0, 98));
            //alert(GetInnerHeightAndWidthofClientBrowser(0,80));
            $('#divfinancialsummary').css('width', GetInnerHeightAndWidthofClientBrowser(0, 98));
        }
        Ext.getCmp('id').getView().refresh();
    }
    setTimeout("setscrollheight();", 1000);
    if (navigator.userAgent.match(/iPhone/i) == null && navigator.userAgent.match(/iPad/i) == null) {
        //if ($('#hdnreportid').val() == '18' && $('#hdnreportid').val() == '27')
        //$('.x-grid3-scroller').perfectScrollbar().perfectScrollbar('destroy');

        //        $('.x-panel-body').perfectScrollbar().perfectScrollbar('destroy');
        //        $('.x-panel-body').perfectScrollbar();
        setTimeout("$('#FinancialSummary .x-grid3-scroller').perfectScrollbar('destroy');", 1000);
    }
});