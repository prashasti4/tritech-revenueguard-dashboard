﻿$(document).ready(function () {
    $('.date').datepicker({
        changeMonth: true,
        changeYear: true
    });
    Showloading();
    //  BindReportType();
    //   $('#dtend').datepicker();




    EmailAutoComplete();
    bindreportmenu();
    // getReportName();
    var date = new Date().getDate();
    var month = new Date().getMonth();
    var year = new Date().getFullYear();
    bind_master_details();
    ScheduleReport();
    SetDaysOnCheckbox();
    BindDays();
    $('#ddlrepeat').val('0');
    SetRepeatDropdownonready($('#ddlrepeat'));

    SetSummary();
    summary = 'Daily';
    $('#spansumary').text(summary);

    $('.date').datepicker('setDate', new Date());
    //  $('.date').val(month + 1 + '/' + date + '/' + year);

    $('.time').timepicker({
        'showDuration': true,
        'timeFormat': 'g:ia'
    });

    $('.time').timepicker('setTime', new Date());

    $('#chkAlldays').change(function () {
        if ($(this).is(':checked')) {
            $('.time').hide();
        }
        else {
            $('.time').show();
        }
    });

    $('#chkRepeats').change(function () {
        if ($(this).is(':checked')) {
            RepeatcheckboxClick();
            $('#repeatsummary').text('Repeat : ' + $('#spansumary').text());

        }
        else {
            $('#repeatModal').modal('hide');
            $('#repeatsummary').text('');
            $('#EditRepeatschedule').hide();

        }
    });

   

    $('#ddlrepeat').change(function () {
        SetRepeatDropdownonready($(this));
        SetSummary();
    });


    $('input:radio[name="repeatby"]').change(function () {
        SetSummary();
    });

    $('#txtafter').blur(function () {
        SetSummary();
    });

    $('input:radio[name="end"]').change(function () {
        setEndtimeonRepeatpopup(this);
        SetSummary();
    });

    $('#trRptOn :checkbox').click(function () {
        SetSummary();
    });

    $("#ddlReprot").change(function () {


        fetch_report_type_master($("#ddlReprot").val());
        HideLoading();

    });



    $.mask.definitions['H'] = "[0-1]";
    $.mask.definitions['h'] = "[0-9]";
    $.mask.definitions['M'] = "[0-5]";
    $.mask.definitions['m'] = "[0-9]";
    $.mask.definitions['P'] = "[AaPp]";
    $.mask.definitions['p'] = "[Mm]";

    $(".timepicker").mask("Hh:Mm Pp");


});

function fetch_report_type_master(id) {
    //var n = $("#ddlReprot").val();
    $.get('../reportdata.aspx?sp=fetch_master_details&rid=' + id, function (data) {
        var data = JSON.parse(data);

        report_type_master(data.Table1);



    });
}

//function for on ready set end radio
function setEndtimeonRepeatpopup(ctrol) {
    if (ctrol.id == 'rbNever') {
        $('#txtafter, #txtendon').val('');
        $('#txtafter, #txtendon').prop('disabled', true);
    }
    if (ctrol.id == 'rbAfter') {
        $('#txtendon').val('');
        $('#txtendon').prop('disabled', true);
        $('#txtafter').prop('disabled', false).val(35);
    }
    if (ctrol.id == 'rbOn') {
        $('#txtafter').val('');
        $('#txtafter').prop('disabled', true);
        $('#txtendon').prop('disabled', false);
        $('#txtendon').datepicker({
            changeMonth: true,
            changeYear: true,
            onSelect: function () {
                SetSummary();
            }
        });

        //var onDate = new Date(new Date().getFullYear() + 20, new Date().getDate(), new Date().getMonth() + 1)
        //$('#txtendon').datepicker('setDate', onDate);
        $('#txtendon').val($('#startdate').val())
    }
}


// Fetch Report Menu DropDown Data
function bind_master_details() {
    try {

        $.get('../reportdata.aspx?sp=fetch_master_details', function (data) {
            var data = JSON.parse(data);

            repeat_type_master(data.Table);
            report_type_master(data.Table1);
            HideLoading();


        });
    }
    catch (e) {

    }
}

// ADD Report Type Master Details To DropDown
function repeat_type_master(data) {
    try {
        for (var key in data) {
            if (data[key].repeat_id != undefined)
                $('#ddlrepeat').append($("<option></option>").val
                           (data[key].repeat_id).html(data[key].repeat));
        }
    }
    catch (e) {

    }
}


// ADD Report Type Master Details To DropDown
function report_type_master(data) {
    try {
        $('#ddlextension').empty();
        for (var key in data) {
            if (data[key].Reporttypeid != undefined)
                $('#ddlextension').append($("<option></option>").val
                           (data[key].Reporttypeid).html(data[key].ReportType));
        }
    }
    catch (e) {

    }
}

//function for on ready set Repeat Dropdown
function SetRepeatDropdownonready(ctrl) {
    var id = ctrl.val();
    if (id == null) {
        $('#trRepeatEvery').show();
    }
    if (id == 0 || id == 6) {
        $('#trRepeatEvery').show();
        $('#trRptOn, #trRptBy, #trYealymonth').hide();


        if (id == 0)
            $('#spanrpt').text('day(s)');
        else {
            $('#spanrpt').text('year(s)');
            $('#trYealymonth').show();
        }
    }
    else if (id >= 1 && id <= 3) {
        $('#trRptOn, #trRptBy, #trRepeatEvery, #trYealymonth').hide();
    }
    else if (id == 4) {
        $('#trRepeatEvery, #trRptOn').show();
        $('#trRptBy, #trYealymonth').hide();
        $('#trRepeatEvery').show();
        $('#spanrpt').text('week(s)');
        SetDaysOnCheckbox();

    }
    if (id == 5) {
        $('#trRepeatEvery,#trRptBy').show();
        $('#trRptOn, #trYealymonth').hide();
        $('#trRepeatEvery').show();
        $('#spanrpt').text('month(s)');
        $('#rbdom').prop('checked', true);
    }
}

function reposition() {
    try {
        var modal = $(this),
        dialog = modal.find('.modal-dialog');
        modal.css('display', 'block');
        // Dividing by two centers the modal exactly, but dividing by three
        // or four works better for larger screens.
        dialog.css("margin-top", Math.max(0, ($(window).height() - dialog.height()) / 2));
        alert(Math.max(0, ($(window).height() - dialog.height()) / 2));
    }
    catch (ex) {
        alert(ex.toString());
    }
}
// function for get and bind repeat schedule
function RepeatcheckboxClick() {
    var modal = $('#repeatModal'),
        dialog = modal.find('.modal-dialog');
    modal.css('display', 'block');
    // Dividing by two centers the modal exactly, but dividing by three
    // or four works better for larger screens.
    dialog.css("margin-top", Math.max(0, ($(window).height() - dialog.height()) / 2));

    $("#repeatModal").modal('show').css({
        'margin-top': function () { //vertical centering
            // return Math.max(0, ($(window).height() - dialog.height()) / 2);
        }
    });
    //$('#repeatModal').modal('show');
    //BindDays();
    $('#txtStartsOn').val($('#startdate').val())
}



function clearall() {
    $('.date').datepicker('setDate', new Date());
    //  $('.date').val(month + 1 + '/' + date + '/' + year);

    $('.time').timepicker({
        'showDuration': true,
        'timeFormat': 'g:ia'
    });

    $('.time').timepicker('setTime', new Date());

    $('#txtEmail').val('');
    $('#ddlextension').val('xls');
    $('#ddlTimeZone').val('cst');

    //  ddlextension
}

function SetDaysOnCheckbox() {
    var cnt = new Date().getDay();
    $('#trRptOn :checkbox').each(function (index, item) {
        if (index == cnt)
            $(item).prop('checked', true);
        else
            $(item).prop('checked', false);
    });

}

function BindDays() {
    $("#ddlrepeatevery").html("");
    $('#ddlMonthDay').html("");
    $('#ddlmonthonforYear').html("");

    for (var i = 1; i <= 30; i++) {

        $("#ddlrepeatevery").append($("<option></option>").val(i).html(i));
        $("#ddlMonthDay").append($("<option></option>").val(i).html(i));
    }

    for (var k = 1; k <= 12; k++) {
        $("#ddlmonthonforYear").append($("<option></option>").val(k).html(k));
    }


    $("#ddlrepeatevery").change(function () {
        SetSummary();
    });
    $("#ddlMonthDay").change(function () {
        SetSummary();
    });
    $("#ddlmonthonforYear").change(function () {
        SetSummary();
    });


}

function SetSummary() {
    var summary = '';
    var ddlrpt = $('#ddlrepeat').val();
    if (ddlrpt == 0) {
        if ($("#ddlrepeatevery").val() > 1)
            summary = 'Every ' + $("#ddlrepeatevery").val() + ' days'
        else
            summary = 'Daily';
    }
    else if (ddlrpt >= 1 && ddlrpt <= 4) {
        if ($("#ddlrepeatevery").val() > 1)
            if (ddlrpt == 4)
                summary = 'Every ' + $("#ddlrepeatevery").val() + ' weeks'
            else
                summary = 'Every weeks'
            else
                summary = 'Weekly';
            if (ddlrpt == 1)
                summary += ' on weekdays';
            else if (ddlrpt == 2)
                summary += ' on Monday, Wednesday, Friday';
            else if (ddlrpt == 3)
                summary += ' on Tuesday, Thursday';
            else {
                summary += ' on ';
                if ($('#trRptOn :checkbox:checked').length < 7) {
                    $('#trRptOn :checkbox:checked').each(function (index, item) {
                        if (index == 0)
                            summary += $(item).attr('title');
                        else
                            summary += ', ' + $(item).attr('title');
                    });
                }
                else
                    summary += 'all days'
            }
        }
        else if (ddlrpt == 5) {

            if ($("#ddlrepeatevery").val() > 1) {
                summary = summary + "Every " + $("#ddlrepeatevery").val() + " months on" + " " + "day " + $('select#ddlMonthDay option:selected').val();
            }
            else {
                summary = summary + "Every months on" + " " + "day " + $('select#ddlMonthDay option:selected').val();
            }

            //        if ($('#rbdom').is(':checked'))
            //            summary = 'Monthly on date ' + new Date().getDate();
            //        else
            //            summary = 'Monthly on day ' + nums[new Date().getMonthWeek()] + ' ' + Weeks[new Date().getDay()];
        }
        else if (ddlrpt == 6) {
            if ($("#ddlrepeatevery").val() > 1)
                summary = 'Every ' + $("#ddlrepeatevery").val() + ' years on ' + Months[$('#ddlmonthonforYear').val()] + ' ' + new Date($('#txtStartsOn').val()).getDate();
            else {
                summary = 'Annually on ' + Months[$('#ddlmonthonforYear').val() - 1] + ' ' + new Date($('#txtStartsOn').val()).getDate();
            }
            //summary = 'Annually on ' + Months[new Date($('#txtStartsOn').val()).getMonth()] + ' ' + new Date($('#txtStartsOn').val()).getDate();
        }


        if ($('#rbOn').is(':checked'))
            summary += ', until ' + $('#txtendon').val();
        if ($('#rbAfter').is(':checked')) {
            if ($('#txtafter').val() > 1)
                summary += ', ' + $('#txtafter').val() + ' times';
            else if ($('#txtafter').val() == 1)
                summary = 'Once';
        }
        $('#spansumary').text(summary);

    }

var Weeks = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];

var nums = ['zero', 'first', 'second', 'third', 'fourth', 'fifth'];
var Months = ["January", "February", "March", "April", "May", "Jun", "July", "August", "September", "October", "November", "December"];
Date.prototype.getMonthWeek = function () {
    var firstDay = new Date(this.getFullYear(), this.getMonth(), 1).getDay();
    return Math.ceil((this.getDate() + firstDay) / 7);
}

// Fetch Report Menu DropDown Data
function bindreportmenu() {

    $.get('../reportdata.aspx?sp=reportmenu', function (data) {
        var data = JSON.parse(data);

        if (!data.hasOwnProperty('error')) {
            reportmenu_fetch(data.Table);
            HideLoading();
        }
        else {
            if (data.error[0].error == "error")
                shownodata();
            else
                console.log(data.error[0].error);
        }

    });
}

// ADD Report Menu DropDown Data
function reportmenu_fetch(data) {
    for (var key in data) {
        if (data[key].ID != undefined)
            $('#ddlReprot').append($("<option></option>").val
                       (data[key].ID).html(data[key].reportname));
    }
    fetch_report_type_master(data[0].ID);
}


var flag = true;

// Bind AutoComplete DropDown for Email ID
function EmailAutoComplete() {
    try {
        $("#txtEmail").bind("keydown", function (event) {

            if (event.keyCode === $.ui.keyCode.TAB &&
            $(this).autocomplete("instance").menu.active) {
                flag = true;
                event.preventDefault();
            }
            if (event.keyCode == 188 || event.keyCode == 9) {
                terms = extractLast(this.value);
                if (IsEmail(terms.trim())) {
                    $.get("../reportdata.aspx?sp=setschedulingemail&email=" + terms, function (data) {
                        if (data.result[0].id != -1) {
                            $("#txtEmalIDs").val(',' + data.result[0].id);
                        }
                    });
                }
                else {
                    //terms = split(this.value);
                    //terms.pop();
                    //if (terms.length > 1)
                    //    this.value = terms.join(", ");
                    //else
                    //    this.value = '';

                }
            }
        }).autocomplete({
            autoFocus: true,
            selectFirst: true,
            source: function (request, response) {

                $.getJSON("../reportdata.aspx?sp=getschedulingemail", {
                    term: extractLast(request.term)
                }, response);

                response;
            },
            search: function () {
                $(".ui-autocomplete").css({ "z-index": "10003", "font-size": "13px" });
                // custom minLength
                var term = extractLast(this.value);
                if (term.length < 2) {
                    return false;
                }
            },
            focus: function () {
                // prevent value inserted on focus
                return false;
            },
            select: function (event, ui) {
                var terms = split(this.value);
                // remove the current input
                terms.pop();
                // add the selected item
                terms.push(ui.item.value);
                // add placeholder to get the comma-and-space at the end

                terms.push("");
                this.value = terms.join(", ");

                var oldDtring = $("#txtEmalIDs").val();
                if (oldDtring == "")
                    oldDtring = ui.item.id + ',';
                else
                    oldDtring = oldDtring + ui.item.id + ',';

                $("#txtEmalIDs").val(oldDtring);
                flag = !flag;
                return false;
            }
        });
    }
    catch (e) {
        return false;
    }
}

function split(val) {
    try {
        return val.split(/,\s*/);
    }
    catch (e) {
        return '';
    }
}

function extractLast(term) {
    return split(term).pop();
}

function CloseModal() {
  

    $('#repeatModal').modal('hide');
    $('#repeatsummary').text('Repeat : ' + $('#spansumary').text());
    if (isRepeatEdit != true) {
        $('#chkRepeats').prop('checked', false);
        $('#repeatsummary').text('');
    }
}

function Showloading() {
    //$('.divloading').css('z-index', '3000');
    $('.divloading').show();
    $('.divmask').show();
}
function HideLoading() {
    //$('.divloading').css('z-index', '-1');
    $('.divloading').hide();
    $('.divmask').hide();
    $('#divslow').hide();
}
// Vlidate Email ID
function IsEmail(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}


// validation for save schedule
function isvalide() {

    var stat = 'true';
    if ($('#txtEmail').val() == '') {
        stat = 'false';
        jAlert('Please enter your email', 'Validation Error');
        return false;

    }
    if (stat == 'true') {
        if ($('#txtEmail').val() != '') {
            var emailsplit = $('#txtEmail').val().split(",");
            for (var i = 0; i < emailsplit.length; i++) {
                if ($.trim(emailsplit[i]) != '') {
                    if (!IsEmail($.trim(emailsplit[i]))) {
                        //   jAlert('Please enter valid Email', 'Validation');
                        stat = 'false';
                    }
                }
            }

            // check all split emails

        }
    }

    if (stat == 'true') {

        if ($('#startdate').val() == '') {
            stat = 'false';
            jAlert('Please enter start date.', 'Validation Error');

        }
            //        else if ($('#enddate').val() == '') {
            //            stat = 'false';
            //            jAlert('Please enter end date.', 'Validation');

            //        }
        else if ($('#startdate').val() == '') {
            stat = 'false';
            jAlert('Please enter start date.', 'Validation Error');


        }
        else if ($('#txtendon').val() != '') {
            if (new Date($('#startdate').val()).getTime() > new Date($('#txtendon').val()).getTime()) {
                stat = 'false';
                jAlert('Start date should be anterior to End date.', 'Validation Error');

            }
        }
        else if ($('#ddlextension').val() == null) {

            stat = 'false';
            jAlert('No report type is available for this report.', 'Validation Error');


        }
        else if ($('#starttime').val() == '') {

            stat = 'false';
            jAlert('Please enter schedule time.', 'Validation Error');


        }
        else if ($('#startdate').val() != '') {
            if (!isDate($('#startdate').val())) {
                stat = 'false';
                jAlert('Start date should be anterior to End date.', 'Validation Error');
            }
        }
        else {
            stat = 'true';
        }
    }
    else {
        jAlert('Please enter a valid email.', 'Validation Error');
    }

    if (stat == 'true') {
        return true;
    }
    else {

        return false;
    }



}

// Add and Update Schedule Details
function AddSchedule() {
    try {
        if (isvalide()) {
            Showloading();
            var sp = '';
            var endsummary = "";
            var email_id = '-1';
            if ($('#lbl_id').val() != '') {
                email_id = $('#lbl_id').val();
            }
            if ($('#rbNever').is(':checked')) {
                endsummary = "Never";
            }
            else if ($('#rbAfter').is(':checked')) {
                endsummary = "";
            }
            else if ($('#rbOn').is(':checked')) {
                endsummary = $('#txtendon').val();
            }

            var end_date = $('#startdate').val();
            if ($('#txtendon').val() != '') {
                end_date = $('#txtendon').val();
            }

            var isrepeat = 0;
            if ($("#chkRepeats").is(':checked')) {
                isrepeat = 1;
            }

            sp = 'AddScheduleDetails&report_id=' + $('#ddlReprot').val() + '&name=' + $("#ddlReprot option:selected").text() + '&eventid=' + $('#hdnEventid').val() + '&email_to=' + $('#txtEmail').val() + '&email_type=' + $('#ddlextension').val() + '&startdate=' + $('#startdate').val() + '&starttime=' + $('#starttime').val().toUpperCase() + '&enddate=' + end_date + '&endtime=' + $('#endtime').val() + "&emailid=" + email_id + '&timezone=' + $('#ddlTimeZone').val() + '&repeat=' + $('#spansumary').html() + '&recursionrule=' + CreateCalendarRules() + '&Repeattypeid=' + $('#ddlrepeat').val() + '&endsummary=' + endsummary + '&Timezonedisplay=' + $("#ddlTimeZone option:selected").text().replace("&", "||") + '&isrepeat=' + isrepeat;


            $.get('../reportdata.aspx?sp=' + sp, function (data) {
                if (data.toString().indexOf('generateerror') > -1) {
                    jAlert("This schedule already exists. Please check and try again.");
                }
                if (data.toString().indexOf('repeat') > -1) {
                    jAlert("This schedule already exists. Please check and try again.", 'Validation Error');
                    HideLoading();
                }
                else {
                    HideLoading();
                    ScheduleReport();
                    clearcontrol();
                    $('#btn_save').val('Add');
                }
            });

        }


    }
    catch (e) {

    }
}

// Date Validation for mm/dd/yyyy format
function isDate(txtDate) {

    var currVal = txtDate;

    if (currVal == '')

        return false;



    //Declare Regex 

    var rxDatePattern = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/;

    var dtArray = currVal.match(rxDatePattern); // is format OK?

    if (dtArray == null)

        return false;



    //Checks for mm/dd/yyyy format.

    dtMonth = dtArray[1];

    dtDay = dtArray[3];

    dtYear = dtArray[5];

    if (dtMonth < 1 || dtMonth > 12)

        return false;

    else if (dtDay < 1 || dtDay > 31)

        return false;

    else if ((dtMonth == 4 || dtMonth == 6 || dtMonth == 9 || dtMonth == 11) && dtDay == 31)

        return false;

    else if (dtMonth == 2) {

        var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));

        if (dtDay > 29 || (dtDay == 29 && !isleap))

            return false;

    }

    return true;

}

// function for clear all control in previous state

function clearcontrol() {
    $('.time').timepicker({
        'showDuration': true,
        'timeFormat': 'g:ia'
    });
    $('.time').timepicker('setTime', new Date());
    $('#txtEmail').val('');
    $('#txtEmalIDs').val('');
    $('#lbl_id').val('');
    $('#hdnEventid').val('');
    $('#btn_save').val('Add');
    $('.date').datepicker('setDate', new Date());
    //  $('.date').val(month + 1 + '/' + date + '/' + year);

    $('.time').timepicker({
        'showDuration': true,
        'timeFormat': 'g:ia'
    });

    $('.time').timepicker('setTime', new Date());

    $('#ddlextension').val('xls');
    $('#ddlrepeat').val('0');
    $('#ddlReprot').val('0');


    
    Showloading();


    //
    isRepeatEdit = false;
    $("#EditRepeatschedule").css("display", "none");
    $('#chkRepeats').prop('checked', false);
    SetDaysOnCheckbox();
    SetRepeatDropdownonready($('#ddlrepeat'));
    SetSummary();
    $('#ddlTimeZone').val('America/Chicago');
    $('#ddlextension').empty();
    $('#rbNever').prop('checked', true);
    $('#txtafter, #txtendon').val('');
    $('#txtafter, #txtendon').prop('disabled', true);
    summary = 'Daily';
    $('#spansumary').text(summary);
    $('#btn_clear').val('Clear All');
    BindDays();
    HideLoading();
    $('#repeatsummary').text('');
}

// Fetch report name for 'Report Name' Dropdown
function getReportName() {
    var sp = '';

    sp = 'getAllReprotName';

    $.get('../reportdata.aspx?sp=' + sp, function (data) {
        if (data.length > 0) {


            $("#ddlReprot").find('option').remove().end();

            var data = JSON.parse(data);


            $.each(data.Table, function (key, value) {
                $("#ddlReprot").append($("<option></option>").val
            (value.report_id).html(value.report_name));
            });

        }

    });
}

// function get Recursion rules based on setting for google api 
function CreateCalendarRules() {
    var recursionRule = "";
    // daily=0,Every weekday (Monday to Friday)=1,Every Monday, Wednesday, and Friday=2,Every Tuesday and Thursday=3,Weekly=4,Monthly=5,Yearly=6
    //Repeats every: Interval
    //Ends: until

    try {

        if ($("#chkRepeats").is(':checked')) {
            //for Daily
            if ($('select#ddlrepeat option:selected').val() == "0") {
                //=RRULE:FREQ=DAILY;UNTIL=" + Request["enddate"] + " " + Request["starttime"]
                recursionRule = "RRULE:FREQ=DAILY";
                // for interval
                recursionRule = recursionRule + ";INTERVAL=" + $('select#ddlrepeatevery option:selected').val();


                // Until
                var check = $("#rbAfter").prop("checked");
                if (check) {
                    recursionRule = recursionRule + ";COUNT=" + $('#txtafter').val();
                }
                check = null;

                //rbOn
                var check = $("#rbOn").prop("checked");
                if (check) {
                    if ($('#txtendon').val() != "") {
                        var untildate = returnvaliduntilDate(new Date($('#txtendon').val() + ' ' + $('#starttime').val()));
                        // var untildate = returnvaliduntilDate(new Date($('#txtendon').val()));
                        recursionRule = recursionRule + ";UNTIL=" + untildate;
                        //recursionRule = recursionRule + ";UNTIL=" + $('#txtendon').val();
                    }
                }
                check = null;
            }
            // for week days
            else if ($('select#ddlrepeat option:selected').val() == "1") {
                // RRULE:FREQ=WEEKLY;COUNT=4;BYDAY=TU,SU;WKST=SU
                recursionRule = "RRULE:FREQ=WEEKLY";
                // Until
                var check = $("#rbAfter").prop("checked");
                if (check) {
                    recursionRule = recursionRule + ";COUNT=" + $('#txtafter').val();
                }
                check = null;

                //rbOn
                var check = $("#rbOn").prop("checked");
                if (check) {
                    if ($('#txtendon').val() != "") {
                        var untildate = returnvaliduntilDate(new Date($('#txtendon').val() + ' ' + $('#starttime').val()));
                        recursionRule = recursionRule + ";UNTIL=" + untildate;
                        //recursionRule = recursionRule + ";UNTIL=" + $('#txtendon').val();
                    }
                    // recursionRule = recursionRule + ";UNTIL=" + $('#txtendon').val();
                }
                check = null;
                recursionRule = recursionRule + ";BYDAY=MO,TU,WE,TH,FR";
            }

            // For Every Monday, Wednesday, and Friday
            else if ($('select#ddlrepeat option:selected').val() == "2") {
                // RRULE:FREQ=WEEKLY;INTERVAL=2;COUNT=4;BYDAY=TU,SU;WKST=SU
                recursionRule = "RRULE:FREQ=WEEKLY";
                // Until
                var check = $("#rbAfter").prop("checked");
                if (check) {
                    recursionRule = recursionRule + ";COUNT=" + $('#txtafter').val();
                }
                check = null;

                //rbOn
                var check = $("#rbOn").prop("checked");
                if (check) {
                    if ($('#txtendon').val() != "") {
                        var untildate = returnvaliduntilDate(new Date($('#txtendon').val() + ' ' + $('#starttime').val()));
                        recursionRule = recursionRule + ";UNTIL=" + untildate;
                        //recursionRule = recursionRule + ";UNTIL=" + $('#txtendon').val();
                    }
                    //recursionRule = recursionRule + ";UNTIL=" + $('#txtendon').val();
                }
                check = null;
                recursionRule = recursionRule + ";BYDAY=MO,WE,FR";
            }

            // For Every Tuesday and Thursday
            else if ($('select#ddlrepeat option:selected').val() == "3") {
                // RRULE:FREQ=WEEKLY;INTERVAL=2;COUNT=4;BYDAY=TU,SU;WKST=SU
                recursionRule = "RRULE:FREQ=WEEKLY";
                // Until
                var check = $("#rbAfter").prop("checked");
                if (check) {
                    recursionRule = recursionRule + ";COUNT=" + $('#txtafter').val();
                }
                check = null;

                //rbOn
                var check = $("#rbOn").prop("checked");
                if (check) {
                    //recursionRule = recursionRule + ";UNTIL=" + $('#txtendon').val();
                    if ($('#txtendon').val() != "") {
                        var untildate = returnvaliduntilDate(new Date($('#txtendon').val() + ' ' + $('#starttime').val()));
                        recursionRule = recursionRule + ";UNTIL=" + untildate;
                        //recursionRule = recursionRule + ";UNTIL=" + $('#txtendon').val();
                    }
                }
                check = null;
                recursionRule = recursionRule + ";BYDAY=TU,TH";
            }

            // For Weekly
            else if ($('select#ddlrepeat option:selected').val() == "4") {
                // RRULE:FREQ=WEEKLY;INTERVAL=2;COUNT=4;BYDAY=TU,SU;WKST=SU
                recursionRule = "RRULE:FREQ=WEEKLY";
                // for interval
                recursionRule = recursionRule + ";INTERVAL=" + $('select#ddlrepeatevery option:selected').val();

                // Until
                var check = $("#rbAfter").prop("checked");
                if (check) {
                    recursionRule = recursionRule + ";COUNT=" + $('#txtafter').val();
                }
                check = null;

                //rbOn
                var check = $("#rbOn").prop("checked");
                if (check) {
                    if ($('#txtendon').val() != "") {
                        var untildate = returnvaliduntilDate(new Date($('#txtendon').val() + ' ' + $('#starttime').val()));
                        recursionRule = recursionRule + ";UNTIL=" + untildate;
                        //recursionRule = recursionRule + ";UNTIL=" + $('#txtendon').val();
                    }
                    //recursionRule = recursionRule + ";UNTIL=" + $('#txtendon').val();
                }
                check = null;

                var Days = "";
                if ($("#chkSU").prop("checked") == true || $("#chkMO").prop("checked") == true || $("#chkTE").prop("checked") == true || $("#chkWE").prop("checked") == true || $("#chkTH").prop("checked") == true || $("#chkFR").prop("checked") == true || $("#chkSA").prop("checked") == true) {
                    //recursionRule = recursionRule + ";BYDAY=TU,TH";
                    if ($("#chkSU").prop("checked") == true) { if (Days == "") { Days = "SU"; } else { Days = Days + ",SU"; } }
                    if ($("#chkMO").prop("checked") == true) { if (Days == "") { Days = "MO"; } else { Days = Days + ",MO"; } }
                    if ($("#chkTE").prop("checked") == true) { if (Days == "") { Days = "TU"; } else { Days = Days + ",TU"; } }
                    if ($("#chkWE").prop("checked") == true) { if (Days == "") { Days = "WE"; } else { Days = Days + ",WE"; } }
                    if ($("#chkTH").prop("checked") == true) { if (Days == "") { Days = "TH"; } else { Days = Days + ",TH"; } }
                    if ($("#chkFR").prop("checked") == true) { if (Days == "") { Days = "FR"; } else { Days = Days + ",FR"; } }
                    if ($("#chkSA").prop("checked") == true) { if (Days == "") { Days = "SA"; } else { Days = Days + ",SA"; } }
                    recursionRule = recursionRule + ";BYDAY=" + Days;
                }
                Days = null;
            }
            // For Monthly
            else if ($('select#ddlrepeat option:selected').val() == "5") {
                // RRULE:FREQ=MONTHLY;INTERVAL=1;COUNT=1;UNTIL=1;BYMONTHDAY=1
                recursionRule = "RRULE:FREQ=MONTHLY";

                // interval
                recursionRule = recursionRule + ";INTERVAL=" + $('select#ddlrepeatevery option:selected').val();



                // Until
                var check = $("#rbAfter").prop("checked");
                if (check) {
                    recursionRule = recursionRule + ";COUNT=" + $('#txtafter').val();
                }
                check = null;

                //rbOn
                var check = $("#rbOn").prop("checked");
                if (check) {
                    if ($('#txtendon').val() != "") {
                        var untildate = returnvaliduntilDate(new Date($('#txtendon').val()));
                        recursionRule = recursionRule + ";UNTIL=" + untildate;
                        //recursionRule = recursionRule + ";UNTIL=" + $('#txtendon').val();
                    }
                    //recursionRule = recursionRule + ";UNTIL=" + $('#txtendon').val();
                }
                check = null;


                // Date of the Reoprt
                recursionRule = recursionRule + ";BYMONTHDAY=" + $('select#ddlMonthDay option:selected').val();
            }
            // For Yealy
            else if ($('select#ddlrepeat option:selected').val() == "6") {
                // RRULE:FREQ=YEARLY;INTERVAL=1;BYMONTH=1;COUNT=1;UNTIL=1;BYYEARDAY=1
                recursionRule = "RRULE:FREQ=YEARLY";

                // interval
                recursionRule = recursionRule + ";INTERVAL=" + $('select#ddlrepeatevery option:selected').val();


                // Until
                var check = $("#rbAfter").prop("checked");
                if (check) {
                    recursionRule = recursionRule + ";COUNT=" + $('#txtafter').val();
                }
                check = null;

                //rbOn
                var check = $("#rbOn").prop("checked");
                if (check) {
                    if ($('#txtendon').val() != "") {
                        var untildate = returnvaliduntilDate(new Date($('#txtendon').val()));
                        recursionRule = recursionRule + ";UNTIL=" + untildate;
                        //recursionRule = recursionRule + ";UNTIL=" + $('#txtendon').val();
                    }
                    //recursionRule = recursionRule + ";UNTIL=" + $('#txtendon').val();
                }

                // Month of the Reoprt
                recursionRule = recursionRule + ";BYMONTH=" + $('select#ddlmonthonforYear option:selected').val();
                // Date of the Year
                recursionRule = recursionRule + ";BYMONTHDAY=" + new Date($('#txtStartsOn').val()).getDate();

                check = null;
            }


        }
        else {


            var untildate = returnvaliduntilDate(new Date($('#startdate').val()));
            recursionRule = recursionRule + ";UNTIL=" + untildate;
            recursionRule = "RRULE:FREQ=DAILY;UNTIL=" + untildate;
            return recursionRule;
        }
    }
    catch (ex) {
        alert(ex.toString());
    }
    //alert(recursionRule);

    return recursionRule;
}
// function for format until date
function returnvaliduntilDate(date) {
    var today = date;

    var offset = (date.getTimezoneOffset() * 60000)  ///date.toString().split('GMT')[1].split(' ')[0];
    today = new Date(date).addTimes(offset);
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!

    var yyyy = today.getFullYear();
    if (dd < 10) {
        dd = '0' + dd;
    }
    if (mm < 10) {
        mm = '0' + mm;
    }
    var arr = today.toString().split(yyyy + " ");

    var TZtime = arr[1].toString().split(':')[0] + arr[1].toString().split(':')[1];
    var today = yyyy + mm + dd + "T" + TZtime + "00Z";
    return today;


}

Date.prototype.addTimes = function (offset) {
    var copiedDate = new Date();
    copiedDate.setTime(this.getTime() + (offset));
    return copiedDate;
}


// Veriable for check repeat Edit mode 
var isRepeatEdit = false;
//function for done repeat schedule set
function setRepeatSchedule() {
    $("#EditRepeatschedule").css("display", "block");
    $('#repeatModal').modal('hide');
    isRepeatEdit = true;
    $('#repeatsummary').text('Repeat : ' + $('#spansumary').text());
}


// function for set values based on Recursion rules
function SetValuebasedonRecursionRule(rule) {
    //split rule string and get interval or count and other param in rule
    var rulesplit = rule.split(";");

    var UNTILindex;
    var Countindex;
    var intervalindex;
    var BYYEARDAYindex;
    var Bydayindex;
    var BYMONTHDAYindex;
    var BYMONTHYEARindex;

    for (var i = 0; i < rulesplit.length; i++) {
        if (rulesplit[i].toString().indexOf("BYYEARDAY=") > -1) {
            BYYEARDAYindex = i;
        }
        if (rulesplit[i].toString().indexOf("BYMONTHDAYindex=") > -1) {
            BYMONTHDAYindex = i;
        }
        else if (rulesplit[i].toString().indexOf("UNTIL=") > -1) {
            UNTILindex = i;
        }
        else if (rulesplit[i].toString().indexOf("COUNT=") > -1) {
            Countindex = i;
        }
        else if (rulesplit[i].toString().indexOf("INTERVAL=") > -1) {
            intervalindex = i;
        }
        else if (rulesplit[i].toString().indexOf("BYDAY=") > -1) {
            Bydayindex = i;
        }
        if (rulesplit[i].toString().indexOf("BYMONTH=") > -1) {
            BYMONTHYEARindex = i;
        }

    }


    // Code for Daily setting
    if ($('select#ddlrepeat option:selected').val() == "0") {
        //="RRULE:FREQ=DAILY;INTERVAL=1;COUNT=2;UNTIL=10/01/2015"
        $('#ddlrepeatevery').val(rulesplit[intervalindex].toString().replace("INTERVAL=", ""));
        if (UNTILindex) {
            if (rulesplit[UNTILindex].toString().indexOf("UNTIL=") > -1) {
                SetEndtimeON(rulesplit[UNTILindex].toString().replace("UNTIL=", "").substring(0, 4) + "/" + rulesplit[UNTILindex].toString().replace("UNTIL=", "").substring(4, 6) + "/" + rulesplit[UNTILindex].toString().replace("UNTIL=", "").substring(6, 8), rulesplit[UNTILindex].toString().replace("UNTIL=", "").substring(4, 6) + "/" + rulesplit[UNTILindex].toString().replace("UNTIL=", "").substring(6, 8) + "/" + rulesplit[UNTILindex].toString().replace("UNTIL=", "").substring(0, 4));
            }
        }
        else if (Countindex) {
            if (rulesplit[Countindex].toString().indexOf("COUNT=") > -1) {
                $('#txtafter').val(rulesplit[Countindex].toString().replace("COUNT=", ""));
                $('#rbAfter').prop('checked', true);
                setEndtimeonRepeatpopup($('#rbAfter'));
            }
        }
        else if (intervalindex) {
            if (rulesplit[intervalindex].toString().indexOf("INTERVAL=") > -1) {
                $('#ddlrepeatevery').val(rulesplit[intervalindex].toString().replace("INTERVAL=", ""));
                $('#rbNever').prop('checked', true);
                setEndtimeonRepeatpopup($('#rbNever'));
            }
        }
    }

    // Code for Every weekday (Monday to Friday) or  Every Monday, Wednesday, and Friday or For Every Tuesday and Thursdaysetting
    else if ($('select#ddlrepeat option:selected').val() == "1" || $('select#ddlrepeat option:selected').val() == "2" || $('select#ddlrepeat option:selected').val() == "3") {
        //"RRULE:FREQ=WEEKLY;COUNT=1;UNTIL=1"
        if (UNTILindex) {
            if (rulesplit[UNTILindex].toString().indexOf("UNTIL=") > -1) {
                SetEndtimeON(rulesplit[UNTILindex].toString().replace("UNTIL=", "").substring(0, 4) + "/" + rulesplit[UNTILindex].toString().replace("UNTIL=", "").substring(4, 6) + "/" + rulesplit[UNTILindex].toString().replace("UNTIL=", "").substring(6, 8), rulesplit[UNTILindex].toString().replace("UNTIL=", "").substring(4, 6) + "/" + rulesplit[UNTILindex].toString().replace("UNTIL=", "").substring(6, 8) + "/" + rulesplit[UNTILindex].toString().replace("UNTIL=", "").substring(0, 4));
            }
        }
        else if (Countindex) {
            if (rulesplit[Countindex].toString().indexOf("COUNT=") > -1) {
                $('#txtafter').val(rulesplit[Countindex].toString().replace("COUNT=", ""));
                $('#rbAfter').prop('checked', true);
                setEndtimeonRepeatpopup($('#rbAfter'));
            }
        }
    }

    // For Weekly 
    else if ($('select#ddlrepeat option:selected').val() == "4") {
        //"RRULE:FREQ=WEEKLY;INTERVAL=10;COUNT=1;UNTIL=1;BYDAY="


        if (Bydayindex) {
            if (rulesplit[Bydayindex].toString().indexOf("BYDAY=") > -1) {
                var onday = rulesplit[Bydayindex].toString().replace("BYDAY=", "").split(",");
                for (var i = 0; i < onday.length; i++) {
                    if (onday[i].toString().trim() == "TU") {
                        $('#chkTE').prop('checked', true);

                    }
                    else {
                        $('#chk' + onday[i].toString().trim()).prop('checked', true);
                    }
                }
            }
        }
        $('#ddlrepeatevery').val(rulesplit[intervalindex].toString().replace("INTERVAL=", ""));
        if (UNTILindex) {
            if (rulesplit[UNTILindex].toString().indexOf("UNTIL=") > -1) {
                //var ondate = new Date(rulesplit[UNTILindex].toString().replace("UNTIL=", ""))
                SetEndtimeON(rulesplit[UNTILindex].toString().replace("UNTIL=", "").substring(0, 4) + "/" + rulesplit[UNTILindex].toString().replace("UNTIL=", "").substring(4, 6) + "/" + rulesplit[UNTILindex].toString().replace("UNTIL=", "").substring(6, 8), rulesplit[UNTILindex].toString().replace("UNTIL=", "").substring(4, 6) + "/" + rulesplit[UNTILindex].toString().replace("UNTIL=", "").substring(6, 8) + "/" + rulesplit[UNTILindex].toString().replace("UNTIL=", "").substring(0, 4));
            }
        }
        else if (Countindex) {
            if (rulesplit[Countindex].toString().indexOf("COUNT=") > -1) {
                $('#txtafter').val(rulesplit[Countindex].toString().replace("COUNT=", ""));
                $('#rbAfter').prop('checked', true);
                setEndtimeonRepeatpopup($('#rbAfter'));
            }
        }
        else if (intervalindex) {
            if (rulesplit[intervalindex].toString().indexOf("INTERVAL=") > -1) {
                $('#ddlrepeatevery').val(rulesplit[intervalindex].toString().replace("INTERVAL=", ""));
                $('#rbNever').prop('checked', true);
                setEndtimeonRepeatpopup($('#rbNever'));
            }
        }
    }
    else if ($('select#ddlrepeat option:selected').val() == "5") {
        // RRULE:FREQ=MONTHLY;INTERVAL=1;COUNT=1;UNTIL=1;BYMONTHDAY=1
        $('#ddlrepeatevery').val(rulesplit[intervalindex].toString().replace("INTERVAL=", ""));
        if (BYMONTHDAYindex) {
            if (rulesplit[BYMONTHDAYindex].toString().indexOf("BYMONTHDAY=") > -1) {
                $('#ddlMonthDay').val(rulesplit[BYMONTHDAYindex].toString().replace("BYMONTHDAY=", ""))
            }
        }
        if (UNTILindex) {
            if (rulesplit[UNTILindex].toString().indexOf("UNTIL=") > -1) {
                //var ondate = new Date(rulesplit[UNTILindex].toString().replace("UNTIL=", ""))
                SetEndtimeON(rulesplit[UNTILindex].toString().replace("UNTIL=", "").substring(0, 4) + "/" + rulesplit[UNTILindex].toString().replace("UNTIL=", "").substring(4, 6) + "/" + rulesplit[UNTILindex].toString().replace("UNTIL=", "").substring(6, 8), rulesplit[UNTILindex].toString().replace("UNTIL=", "").substring(4, 6) + "/" + rulesplit[UNTILindex].toString().replace("UNTIL=", "").substring(6, 8) + "/" + rulesplit[UNTILindex].toString().replace("UNTIL=", "").substring(0, 4));
            }
        }
        else if (Countindex) {
            if (rulesplit[Countindex].toString().indexOf("COUNT=") > -1) {
                $('#txtafter').val(rulesplit[Countindex].toString().replace("COUNT=", ""));
                $('#rbAfter').prop('checked', true);
                setEndtimeonRepeatpopup($('#rbAfter'));
            }
        }
        else if (intervalindex) {
            if (rulesplit[intervalindex].toString().indexOf("INTERVAL=") > -1) {
                $('#ddlrepeatevery').val(rulesplit[intervalindex].toString().replace("INTERVAL=", ""));
                $('#rbNever').prop('checked', true);
                setEndtimeonRepeatpopup($('#rbNever'));
            }
        }

    }
    else if ($('select#ddlrepeat option:selected').val() == "6") {
        // RRULE:FREQ=YEARLY;INTERVAL=1;COUNT=1;UNTIL=1;BYYEARDAY=1;BYMONTH=1

        if (BYYEARDAYindex) {
            if (rulesplit[BYYEARDAYindex].toString().indexOf("BYYEARDAY=") > -1) {
                $('#ddlMonthDay').val(rulesplit[BYYEARDAYindex].toString().replace("BYYEARDAY=", ""))
            }
        }
        if (BYMONTHYEARindex) {
            if (rulesplit[BYMONTHYEARindex].toString().indexOf("BYMONTH=") > -1) {
                $('#ddlmonthonforYear').val(rulesplit[BYMONTHYEARindex].toString().replace("BYMONTH=", ""))
            }
        }
        if (UNTILindex) {
            if (rulesplit[UNTILindex].toString().indexOf("UNTIL=") > -1) {
                //var ondate = new Date(rulesplit[UNTILindex].toString().replace("UNTIL=", ""))
                SetEndtimeON(rulesplit[UNTILindex].toString().replace("UNTIL=", "").substring(0, 4) + "/" + rulesplit[UNTILindex].toString().replace("UNTIL=", "").substring(4, 6) + "/" + rulesplit[UNTILindex].toString().replace("UNTIL=", "").substring(6, 8), rulesplit[UNTILindex].toString().replace("UNTIL=", "").substring(4, 6) + "/" + rulesplit[UNTILindex].toString().replace("UNTIL=", "").substring(6, 8) + "/" + rulesplit[UNTILindex].toString().replace("UNTIL=", "").substring(0, 4));
            }
        }
        else if (Countindex) {
            if (rulesplit[Countindex].toString().indexOf("COUNT=") > -1) {
                $('#txtafter').val(rulesplit[Countindex].toString().replace("COUNT=", ""));
                $('#rbAfter').prop('checked', true);
                setEndtimeonRepeatpopup($('#rbAfter'));
            }
        }
        else if (intervalindex) {
            if (rulesplit[intervalindex].toString().indexOf("INTERVAL=") > -1) {
                $('#ddlrepeatevery').val(rulesplit[intervalindex].toString().replace("INTERVAL=", ""));
                $('#rbNever').prop('checked', true);
                setEndtimeonRepeatpopup($('#rbNever'));
            }
        }
    }
    setRepeatSchedule();
    $('#chkRepeats').prop('checked', true);
}
// function for set end time on date with Calendar
function SetEndtimeON(firstdate, seconddate) {
    var untildate = firstdate;
    var ondate = new Date(untildate)
    $('#rbOn').prop('checked', true);
    setEndtimeonRepeatpopup($('#rbOn'));
    $('#txtendon').prop('disabled', false);
    $('#txtendon').datepicker({
        changeMonth: true,
        changeYear: true,
        onSelect: function () {
            SetSummary();
        }
    });
    $('#txtendon').val(seconddate)

}


function deleteemail(emailid) {
    jConfirm('Are you sure you want to delete \'' + emailid + '\' ?', 'Confirmation', function (r) {
        if (r) {


            sp = 'DeleteSchedulingEmails&emailid=' + emailid;

            $.get('../reportdata.aspx?sp=' + sp, function (data) {
                $('#txtEmail').val('');
            });

        }

    });
}


function setRepeatSummary() {
    
    $('#repeatsummary').text('Repeat : ' + $('#spansumary').text());

}

