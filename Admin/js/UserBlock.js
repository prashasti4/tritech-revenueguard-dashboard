﻿$(document).ready(function () {
    $('#sidebar').show();
    fetch_block_type_master();

    // -1 mean all block type
    set_block_details('-1', '1');
    $('#ddlBlock').change(function () {
        try {
            if ($('#rbActive').is(':checked')) {
                set_block_details($(this).val(), '1');
            }
            else if ($('#rbInActive').is(':checked')) {
                set_block_details($(this).val(), '0');
            }


        }
        catch (e) {

        }

    });


    $('#rbActive').prop('checked', true);

    $('input:radio[name="active"]').change(function () {
        try {
            if (this.id == 'rbActive') {
                set_block_details('-1', '1');
            }
            else if (this.id == 'rbInActive') {
                set_block_details('-1', '0');
            }
        }
        catch (e) {

        }


    });

    
});

// Fetch block type master details for dropdown
function fetch_block_type_master() {
    try {

        $.get('../reportdata.aspx?UB=fetch_block_type_master_details', function (data) {
            var data = JSON.parse(data);

            block_type_master(data.Table);



        });
    }
    catch (e) {

    }
}

function Showloading() {
    //$('.divloading').css('z-index', '3000');
    $('.divloading').show();
    $('.divmask').show();
}
function HideLoading() {
    //$('.divloading').css('z-index', '-1');
    $('.divloading').hide();
    $('.divmask').hide();
    $('#divslow').hide();
}


// ADD Report Type Master Details To DropDown
function block_type_master(data) {
    try {
        for (var key in data) {
            if (data[key].id != undefined)
                $('#ddlBlock').append($("<option></option>").val
                           (data[key].id).html(data[key].BlockType));
        }
    }
    catch (e) {

    }
}
// Update block details and change status
// 1 for Block
// 0 for UnBlock
function update_block_details() {
    try {
        Showloading();
        var data = '';
        var grid_data = '';
        $.each($('.chkbox'), function (index, item) {
            grid_data = 'avail';
            if ($(item).is(':checked')) {
                if (data != '') {
                    data = data + ',' + $(item).attr('id') + '|' + '1';
                }
                else {
                    data = $(item).attr('id') + '|' + '1';
                }
            }
            else {
                if (data != '') {
                    data = data + ',' + $(item).attr('id') + '|' + '0';
                }
                else {
                    data = $(item).attr('id') + '|' + '0';
                }
            }
        });

        if (grid_data == '') {
            jAlert('No block data is available for update', 'Validation');
        }

        $.get('../reportdata.aspx?UB=update_block_details&block_data=' + data, function (data) {
            var data = JSON.parse(data);

            block_type_master(data.Table);



        });
        clearcontrol();
        HideLoading();

    }
    catch (e) {

    }


}


// Clear All
function clearcontrol() {
    Showloading();
    $('#ddlBlock').val('-1');

    $('#rbActive').prop('checked', true);
    set_block_details('-1', '1');
    HideLoading();

}


