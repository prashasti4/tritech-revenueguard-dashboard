﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_UserIPBlockUnBlock : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.Cookies["SM_USER"] == null)
        {
            clearcookies();
            Response.Redirect("../Login/Login.aspx");
        }


        // Check whether current user is Admin or Not
        AppearenceSettingRepo objrepo = new AppearenceSettingRepo();
        if (objrepo.Decryptdata(Request.Cookies["isAdmincheck"].Value.ToString()).ToUpper() == "1")
        {
            
        }
        else
        {
            Response.Redirect("AccessDenied.aspx");
        }

    }

    private void clearcookies()
    {
        if (Request.Cookies["SM_USER"] != null)
        {
            HttpCookie SM_USER = Request.Cookies["SM_USER"];
            SM_USER.Expires = DateTime.Now.AddDays(-1d);
            Response.Cookies.Add(SM_USER);
        }
        if (Request.Cookies["isAdmincheck"] != null)
        {
            HttpCookie isAdmincheck = Request.Cookies["isAdmincheck"];
            isAdmincheck.Expires = DateTime.Now.AddDays(-1d);
            Response.Cookies.Add(isAdmincheck);
        }
        if (Request.Cookies["SM_URL"] != null)
        {
            HttpCookie SM_URL = Request.Cookies["SM_URL"];
            SM_URL.Expires = DateTime.Now.AddDays(-1d);
            Response.Cookies.Add(SM_URL);
        }
        if (Request.Cookies["CAutoID"] != null)
        {
            HttpCookie CAutoID = Request.Cookies["CAutoID"];
            CAutoID.Expires = DateTime.Now.AddDays(-1d);
            Response.Cookies.Add(CAutoID);
        }
        if (Request.Cookies["SAuto_ID"] != null)
        {
            HttpCookie SAuto_ID = Request.Cookies["SAuto_ID"];
            SAuto_ID.Expires = DateTime.Now.AddDays(-1d);
            Response.Cookies.Add(SAuto_ID);
        }
        if (Request.Cookies["Role_ID"] != null)
        {
            HttpCookie Role_ID = Request.Cookies["Role_ID"];
            Role_ID.Expires = DateTime.Now.AddDays(-1d);
            Response.Cookies.Add(Role_ID);
        }
        if (Request.Cookies["Uname"] != null)
        {
            HttpCookie Uname = Request.Cookies["Uname"];
            Uname.Expires = DateTime.Now.AddDays(-1d);
            Response.Cookies.Add(Uname);
        }
        if (Request.Cookies["active_menu"] != null)
        {
            HttpCookie active_menu = Request.Cookies["active_menu"];
            active_menu.Expires = DateTime.Now.AddDays(-1d);
            Response.Cookies.Add(active_menu);
        }
        if (Request.Cookies["UserID"] != null)
        {
            HttpCookie UserID = Request.Cookies["UserID"];
            UserID.Expires = DateTime.Now.AddDays(-1d);
            Response.Cookies.Add(UserID);
        }
        if (Request.Cookies["ClientsCount"] != null)
        {
            HttpCookie ClientsCount = Request.Cookies["ClientsCount"];
            ClientsCount.Expires = DateTime.Now.AddDays(-1d);
            Response.Cookies.Add(ClientsCount);
        }
        if (Request.Cookies["Unm"] != null)
        {
            HttpCookie Unm = Request.Cookies["Unm"];
            Unm.Expires = DateTime.Now.AddDays(-1d);
            Response.Cookies.Add(Unm);
        }

        if (Request.Cookies["reportid"] != null)
        {
            HttpCookie reportid = Request.Cookies["reportid"];
            reportid.Expires = DateTime.Now.AddDays(-1d);
            Response.Cookies.Add(reportid);
        }

        if (Request.Cookies["service"] != null)
        {
            HttpCookie service = Request.Cookies["service"];
            service.Expires = DateTime.Now.AddDays(-1d);
            Response.Cookies.Add(service);
        }

        if (Request.Cookies["active_menu"] != null)
        {
            HttpCookie active_menu = Request.Cookies["active_menu"];
            active_menu.Expires = DateTime.Now.AddDays(-1d);
            Response.Cookies.Add(active_menu);
        }
    }
}