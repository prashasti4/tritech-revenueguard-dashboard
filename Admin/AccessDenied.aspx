﻿<%@ Page Title="" Language="C#" MasterPageFile="TriTechMaster.master" AutoEventWireup="true" CodeFile="AccessDenied.aspx.cs" Inherits="admin_AccessDenied" %>

<asp:Content ID="Content1" ContentPlaceHolderID="pagehead" Runat="Server">

    
    <script type="text/javascript" src="js/jquery-1.8.2.js"></script>
    <script src="js/bootstrap.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/jquery-ui-1.11.3.js"></script>
   
    <style>
       
        #divReport a, #divHome a, #css3menu1 {
            display: none !important;
        }

        #logintab {
            display: block !important;
        }
        .login-wrapper {
        width: 30%;
        left: 35%;
        top: 35%;
    }
        .login {
    background: none repeat scroll 0 0 transparent;
    border: 10px solid #c7c6c6 !important;
    border-radius: 5px;
    padding: 10px;
}

        table{
            border-collapse:inherit !important;
            border-spacing:2px !important;
        }
        body{
            font-size:12px !important;
        }
        .right{
            margin:0 !important;
        }
        #changefacility{
            display:none !important;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentHolder" Runat="Server">
    <center>
    <div class="login-wrapper">
<table style="width:100%;margin-top:25%;" class="login">
        <tr>
        <td colspan="3">
        <h2>Access Denied</h2>
        </td>
        </tr>
        <tr>
            <td style="width: 12%;vertical-align: middle;" align="left" valign="top">
               <%-- <asp:Image ID="imgError" runat="server" ImageUrl="~/Images/alerts.png" />--%>
                 <i id="iconfail" style="font-size:30px; color: #CA0808;" class="icon-warning-sign"></i>
            </td>
            <td style="width: 90%; border-bottom: lightgrey thin solid;">
                 <div class="clear:both;">
                    &nbsp;</div>
                    You do not have permissions to view this page.
                  <br />      
                  

                <div class="clear:both;">
                    &nbsp;</div>
            </td>
            <td style="width: 3%">
            </td>
        </tr>
        <tr>
            <td style="width: 12%">
            </td>
            <td style="width: 90%">
                <br />
                <asp:Button ID="btnBack" CssClass="canccompare"  runat="server" Text="Try Again" OnClick="btnBack_Click" />
                <div class="clear:both;">
                    &nbsp;</div>
                      <div class="clear:both;">
                    &nbsp;</div>
            </td>
            <td style="width: 3%">
            </td>
        </tr>
    </table>
    <asp:HiddenField ID="hdnUrl" runat="server" />
    </div>
        </center>
</asp:Content>

