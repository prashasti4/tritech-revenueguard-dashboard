﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections;
using System.Text;
using System.Configuration;
using System.Security.Cryptography;
using System.IO;

public partial class Admin_TriTechMaster : System.Web.UI.MasterPage
{
    AppearenceSettingRepo objrepo = new AppearenceSettingRepo();
    protected void Page_Init(object sender, EventArgs e)
    {
        Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
        Response.Cache.SetNoStore();

        if(!IsPostBack)
        {
            
            //string a =tst.Encrypt("test");
            //AppearenceSettingRepo ap = new AppearenceSettingRepo();
            //string d = Decrypt(a);
            if (Request.Cookies["Uname"] != null)
            {
                AppearenceSettingRepo objrepo = new AppearenceSettingRepo();

                lblUser.Text = objrepo.Decryptdata(Request.Cookies["Uname"].Value);
                //string menu_html=
                if (objrepo.Decryptdata(Request.Cookies["isAdmincheck"].Value.ToString()).ToUpper() != "1")
                {
                    lblIsAdmin.Text = "0";
                   
                }
                else
                    lblIsAdmin.Text = "1";
            }
        }

       // string config =objrepo.Decryptdata( ConfigurationManager.AppSettings["clientcss"]);
      //  hdnProxyUrl.Value = ConfigurationManager.AppSettings["proxyurl"].ToString();
      //  hdnProxyUrlTest.Value = ConfigurationManager.AppSettings["proxyurltest"].ToString();
        //if (config == "jefbar")
        //{
        //   // Head1.Title = "AMB (Beta)";
        //    stylecss.Attributes.Add("href", "css/" + config + "/style_jefbar.css?"+DateTime.Now.ToString("MM/dd/yyyy").Replace("/",""));
        //}
        //else
        //{
        //   // Head1.Title = "CCA Medical (Beta)";
        //    stylecss.Attributes.Add("href", "css/" + config + "/style_green.css?" + DateTime.Now.ToString("MM/dd/yyyy").Replace("/", ""));
        //}

        //if (Request.Cookies["SM_USER"] != null)
        //{
        //    string new_pass = objrepo.Decryptdata(Request.Cookies["SM_USER"].Value.ToString());
        //    DataTable login = objrepo.GetNewLogins(new_pass);
        //  //  if (login.Rows.Count>0)
        //      //  loginname.InnerHtml = Convert.ToString(login.Rows[0][4].ToString());
        //    if (Request.Cookies["SM_FACILITY"] == null)
        //    {
        //        HttpCookie myCookie = new HttpCookie("SM_FACILITY");
        //        myCookie["Uname"] = objrepo.Encryptdata(login.Rows[0][4].ToString());
        //        Response.Cookies.Add(myCookie);
        //    }

        //    if (System.Configuration.ConfigurationManager.AppSettings["RedirectMethod"].ToString().ToLower() == "get")
        //    {
        //        if (Request.Cookies["SM_FACILITY"] != null)
        //        {
        //            if (login.Rows.Count > 0)
        //                Request.Cookies["SM_FACILITY"]["Uname"] = objrepo.Encryptdata(login.Rows[0][4].ToString());
        //            string s = objrepo.Decryptdata(Request.Cookies["SM_FACILITY"]["Uname"]); //Convert.ToString(Session["Uname"].ToString().Trim());
        //            string[] LoginName = s.Split('|');
                  
        //           // lblServerDate.Text = DateTime.Now.ToShortDateString();
        //        }//
        //        else
        //        {
        //            redirectOnLogin();
        //        }

        //    }
        //}
        //else
        //{
        //    string userAgent = Request.UserAgent;
        //    Uri test = new Uri(Request.Url.ToString());
        //  //  if (test.Host != "localhost" && !userAgent.Contains("iPad") && !userAgent.Contains("iPhone"))
        //     //   Response.Redirect(ConfigurationManager.AppSettings["LoginUrl"].ToString());
        //   // Response.Redirect("../Login/login.aspx");

        //}

      // lblyear.InnerText = " "+DateTime.Now.Year.ToString()+", All Rights Reserved"; 
        lblyear1.InnerText = " " + DateTime.Now.Year.ToString();
    }


 
    protected void Page_Load(object sender, EventArgs e)
    {
        //HttpCookie OldSM_USER = Request.Cookies["OldSM_USER"];
        //if (OldSM_USER != null)
        //{
        //    lnkLogout.Text = "Exit";
        //    lnkLogout.CommandName = "Exit";
        //}
        //else
        //{
        //    lnkLogout.Text = "Logout";
        //    lnkLogout.CommandName = "Logout";
        //}
        
    }


    protected void lnkLogout_click(object sender, EventArgs e)
    {
         LinkButton lnk = (LinkButton)sender;

         if (lnk.CommandName == "Exit")
         {
             HttpCookie OldSM_USER = Request.Cookies["OldSM_USER"];

             HttpCookie myCookie = new HttpCookie("SM_USER");
             myCookie.Value = OldSM_USER["SM_USER"];
             Response.Cookies.Add(myCookie);

             HttpCookie isAdmincheck = new HttpCookie("isAdmincheck");
             isAdmincheck.Value = OldSM_USER["isAdmincheck"];
             Response.Cookies.Add(isAdmincheck);

             OldSM_USER.Expires = DateTime.Now.AddDays(-1d);
             Response.Cookies.Add(OldSM_USER);


             string error = "Error";
             string sAutoId = objrepo.Decryptdata(myCookie.Value);

             DataSet ds = objrepo.getFacilitiesForSAutoID(sAutoId, out error);
             if (ds.Tables.Count > 1)
             {
                 HttpCookie CAutoID = new HttpCookie("CAutoID");
                 CAutoID.Value = objrepo.Encryptdata(ds.Tables[1].Rows[0]["CAutoID"].ToString());
                 Response.Cookies.Add(CAutoID);
                 Response.Redirect("../admin/useraccessbypass.aspx");
             }

         }
         else
         {
             HttpCookie myCookie = new HttpCookie("SM_USER");
             myCookie.Expires = DateTime.Now.AddDays(-1d);
             // myCookie.Domain = ".eonbi.com";
             Response.Cookies.Add(myCookie);
             myCookie = new HttpCookie("SM_URL");
             myCookie.Expires = DateTime.Now.AddDays(-1d);
             // myCookie.Domain = ".eonbi.com";
             Response.Cookies.Add(myCookie);
             HttpCookie valcookie = new HttpCookie("reportval");
             HttpCookie nooffacility = new HttpCookie("nooffacility");
             valcookie.Expires = DateTime.Now.AddDays(-1d);
             nooffacility.Expires = DateTime.Now.AddDays(-1d);
             HttpCookie CAutoID = new HttpCookie("CAutoID");
             CAutoID.Expires = DateTime.Now.AddDays(-1d);
             Response.Cookies.Add(CAutoID);

             Response.Cookies.Add(valcookie);
             Response.Cookies.Add(nooffacility);
             if (Request.Cookies["SM_FACILITY"] != null)
             {
                 string s = objrepo.Decryptdata(Request.Cookies["SM_FACILITY"]["Uname"]);
                 string[] LoginName = s.Split('|');
                 //   Session.Abandon();
                 myCookie = new HttpCookie("SM_FACILITY");
                 myCookie.Expires = DateTime.Now.AddDays(-1d);
                 // myCookie.Domain = ".eonbi.com";
                 Response.Cookies.Add(myCookie);
                 if (LoginName.Length > 1)
                 {
                     // Response.Redirect("../login/login.aspx");
                 }
                 else
                 {
                     //Response.Redirect("../login/login.aspx");
                     //Response.Redirect("http://mydashboard.eonbi.com/");
                     redirectOnLogin();
                 }
             }
             else
             {
                 //Response.Redirect("../login/login.aspx");
                 // Response.Redirect("http://mydashboard.eonbi.com/");
                 redirectOnLogin();
             }

         }

    }


    private void redirectOnLogin()
    {
        string RedirecUrl = ConfigurationManager.AppSettings["LoginUrl"].ToString();
        string userAgent = Request.UserAgent;
        Uri test = new Uri(Request.Url.ToString());
        if (test.Host != "localhost" && !userAgent.Contains("iPad") && !userAgent.Contains("iPhone"))
            Response.Redirect(RedirecUrl);
        else
            Response.Redirect("../Login/login.aspx");
    }


    protected void lnk_logout_Click(object sender, EventArgs e)
    {
        if (Request.Cookies["SM_USER"] != null)
        {
            HttpCookie SM_USER = Request.Cookies["SM_USER"];
            SM_USER.Expires = DateTime.Now.AddDays(-1d);
            Response.Cookies.Add(SM_USER);
        }
        if (Request.Cookies["isAdmincheck"] != null)
        {
            HttpCookie isAdmincheck = Request.Cookies["isAdmincheck"];
            isAdmincheck.Expires = DateTime.Now.AddDays(-1d);
            Response.Cookies.Add(isAdmincheck);
        }
        if (Request.Cookies["SM_URL"] != null)
        {
            HttpCookie SM_URL = Request.Cookies["SM_URL"];
            SM_URL.Expires = DateTime.Now.AddDays(-1d);
            Response.Cookies.Add(SM_URL);
        }
        if (Request.Cookies["CAutoID"] != null)
        {
            HttpCookie CAutoID = Request.Cookies["CAutoID"];
            CAutoID.Expires = DateTime.Now.AddDays(-1d);
            Response.Cookies.Add(CAutoID);
        }
        if (Request.Cookies["SAuto_ID"] != null)
        {
            HttpCookie SAuto_ID = Request.Cookies["SAuto_ID"];
            SAuto_ID.Expires = DateTime.Now.AddDays(-1d);
            Response.Cookies.Add(SAuto_ID);
        }
        if (Request.Cookies["Role_ID"] != null)
        {
            HttpCookie Role_ID = Request.Cookies["Role_ID"];
            Role_ID.Expires = DateTime.Now.AddDays(-1d);
            Response.Cookies.Add(Role_ID);
        }
        if (Request.Cookies["Uname"] != null)
        {
            HttpCookie Uname = Request.Cookies["Uname"];
            Uname.Expires = DateTime.Now.AddDays(-1d);
            Response.Cookies.Add(Uname);
        }
        if (Request.Cookies["active_menu"] != null)
        {
            HttpCookie active_menu = Request.Cookies["active_menu"];
            active_menu.Expires = DateTime.Now.AddDays(-1d);
            Response.Cookies.Add(active_menu);
        }
        if (Request.Cookies["UserID"] != null)
        {
            HttpCookie UserID = Request.Cookies["UserID"];
            UserID.Expires = DateTime.Now.AddDays(-1d);
            Response.Cookies.Add(UserID);
        }
        if (Request.Cookies["ClientsCount"] != null)
        {
            HttpCookie ClientsCount = Request.Cookies["ClientsCount"];
            ClientsCount.Expires = DateTime.Now.AddDays(-1d);
            Response.Cookies.Add(ClientsCount);
        }

        if (Request.Cookies["Unm"] != null)
        {
            HttpCookie Unm = Request.Cookies["Unm"];
            Unm.Expires = DateTime.Now.AddDays(-1d);
            Response.Cookies.Add(Unm);
        }


        if (Request.Cookies["reportid"] != null)
        {
            HttpCookie reportid = Request.Cookies["reportid"];
            reportid.Expires = DateTime.Now.AddDays(-1d);
            Response.Cookies.Add(reportid);
        }

        if (Request.Cookies["service"] != null)
        {
            HttpCookie service = Request.Cookies["service"];
            service.Expires = DateTime.Now.AddDays(-1d);
            Response.Cookies.Add(service);
        }

        if (Request.Cookies["active_menu"] != null)
        {
            HttpCookie active_menu = Request.Cookies["active_menu"];
            active_menu.Expires = DateTime.Now.AddDays(-1d);
            Response.Cookies.Add(active_menu);
        }

        Response.Redirect("../Login/Login.aspx");
    }

    
}

