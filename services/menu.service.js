try {
    //SERVICE TO MAKE HTTP CALLS FOR CHARTS
    app.factory('httpService', ['$http', 'chartFilterParameters', 'appSettings', '$q', '$rootScope', 'ErrorLog', function($http, chartFilterParameters, appSettings, $q, $rootScope, ErrorLog) {
        try {
            
            var httpService = {
                async: function(canceller) {            
                    // $http returns a promise, which has a then function, which also returns a promise
                    var config = {
                        method: 'POST',
                        url: appSettings.proxyPage,
                        timeout: canceller.promise,
                        data: {
                            requester: "ChartCtrl",
                            requestedUrl: appSettings.requestedUrl,
                            ServiceType: chartFilterParameters.ServiceType,
                            companyid: chartFilterParameters.companyid, //companyid: 
                            reporttype: chartFilterParameters.reporttype,
                            fromdate: chartFilterParameters.fromdate,
                            todate: chartFilterParameters.todate,
                            reportid: chartFilterParameters.reportid,
                            userid: chartFilterParameters.userid,
                            avgormax: chartFilterParameters.avgormax,
                            numberofdays: chartFilterParameters.numberofdays,
                            insuranceid: chartFilterParameters.insuranceid,
                            isreport: 0
                        }
                    };
                  
                   
                    var request = $http(config); //created request
                    var promise = request.then(function(response) {                                           
                        return response;
                    }, function errorCallback(response) {
                     //if http request encounered some error
                        var time = new Date();
                        var httpErrorObject = {
                            statusCode: response.status,
                            Message: response.statusText,
                            time: time
                        }
                        //pushing error message in error object to display relative error message
                        ErrorLog.httpError.push(httpErrorObject);                      
                    });
                    // Return the promise to the controller
                    return promise;
                }
            };
            return httpService;
        } catch (exception) {
            //if some exception occurs
            var caughtException = exception;
            var exceptionObject = {
                Error: caughtException,
                time: new Date()
            }
            //push exception in error object to displaye relative error message
            ErrorLog.exceptions.push(exceptionObject);
        }
    }]);


    //SERVICE TO MAKE HTTP CALLS FOR FETCHING DATA FOR FILTERS (i.e. DROPDOWNS)

    app.factory('getFilterDataService', ['$http', 'chartFilterParameters', 'appSettings', 'ErrorLog', function($http, chartFilterParameters, appSettings, ErrorLog) {
        try {
            var getFilterDataService = {

                async: function(sp) {
                   
                    var config = {
                        method: 'POST',
                        url: appSettings.proxyPage,                       
                        data: {
                            requestedUrl: appSettings.requestedUrl,
                            ServiceType: sp,
                            companyid: chartFilterParameters.companyid,
                            insuranceid: chartFilterParameters.insuranceid,
                            reporttype: chartFilterParameters.reporttype,
                            fromdate: chartFilterParameters.fromdate,
                            todate: chartFilterParameters.todate,
                            reportid: chartFilterParameters.reportid,
                            userid: chartFilterParameters.userid,
                            avgormax: chartFilterParameters.avgormax,
                            numberofdays: chartFilterParameters.numberofdays
                        }
                    };                    
                    var request = $http(config); //created request
                    var promise = request.then(function(response) { //call to 'then' function                       
                            return response.data;
                        }, function errorCallback(response) {                          
                            var time = new Date();
                            var httpErrorObject = {
                                statusCode: response.status,
                                Message: response.statusText,
                                time: time
                            }
                            //pushing error message in error object to display relative error message
                            ErrorLog.httpError.push(httpErrorObject);                           
                        })
                        // Return the promise to the controller
                    return promise;
                }
            };
            return getFilterDataService;
        } catch (exception) {
            var caughtException = exception;
            var exceptionObject = {
                Error: caughtException,
                time: new Date()
            }
            //push exception in error object to displaye relative error message
            ErrorLog.exceptions.push(exceptionObject);
        }
    }]);

    //SERVICE TO USE A COMMON VARIABLE IN ALL CONTROLLERS
    app.factory("isMobileFilter", function() {

        return {
            isMobileFilter: false, //variable used to show hide filter icon
            open: "hide", //variable used to show hide calander
            clicked: 1, //to restrict function calling more than once on click of clear button
            scope: "", //scope to be passed for charts in service
            displayChart: false   

        };

    });


    //SERVICE FOR RETURNING JSON DATA FOR CHARTS ON FILTER SELECTION
    app.factory("filterCharts", function() {
        return {
            section: "home.report.overview",
            id: 1,
            xAxesLabel: "", //to store chart x-axis label
            yAxesLabel: "", //to store chart y-axis label
            ChartType: "",  //to store charttype
            BoldCount: "",  //to store no. of rows to bold
            gridScope: "", //to call gridcontrollers grid bind function wherever required
            isChart: true, //default view is chart view
            currencyFormat: "", //currecny format to be displayed at y-axis/tooltips
            scope: "", //getValue function's scope to be called from login's call webservice function
            freezeColumnCount: "",
            isMenu:false, //to avoid double function call on menu click -> go button call
            isinit:true, //to check if html is loaded for first time or not to avoid double chart function call
            gridControllerScope:"",//for calling canceller of grid controller,
            isRequestCancelled:false, //for cancelling request
            canceller:"",//global cancelller variable to store object so that can be accessed anywhere
            selectedParentClientId:"", //used in angular-multi-select.js as global flag variable
            disableClientDropdownClick:false
        };
    });

    //SET FILTERPARAMETERS FOR PLOTTING CHARTS (these parameters are sent in http request)
    app.factory("chartFilterParameters", function() {
        var service = getCookies("service");
        var reportid = getCookies("reportid");
       
        return {
            requestedUrl: "",
            ServiceType: (service != null && service != "") ? FetchCookieData('service') : "", //overview             
            companyid: "",
            insuranceid: "",
            reporttype: 1,
            fromdate: "",
            todate: "",
            reportid: (reportid != null && reportid != "") ? reportid : 0,          
            userid: FetchCookieData('UserID'),
            avgormax: "",
            numberofdays: 1,
            companyname: "",
            avgormaxname: "",
            insurancename: "",
            numberofdaysname: "",
            dateRange: "",
            proxyPage: ""

        };
    });


//Error object in which http errors and exceptions are pushed 
    app.factory("ErrorLog", function() {
        return {
            httpError: [],
            exceptions: []
        };

    });

} catch (exception) {
    console.log(exception);
}
