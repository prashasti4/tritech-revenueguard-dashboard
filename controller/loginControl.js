//=========================LOGIN CONTROL====================//
try {
    app.controller('LoginControl', function($rootScope, $scope, $localStorage, $window, $interval, $state, isMobileFilter, chartFilterParameters, ErrorLog) {
        $rootScope.userAgent = navigator.userAgent; //get current browser's info      
        //Prepopulated username/ password.
        $scope.form = { username: 'dashboardlib@virim.com', password: 'admin@123' };
        $scope.Guestname = "Hi " + FetchCookieData('Unm'); //display username in side menu header below icon
        chartFilterParameters.userid = FetchCookieData('UserID'); //get logged in user's id

        //"Hi, User"; 
        //Login function (firebase)
        $scope.login = function() {
            showLoader();
            firebase.auth().signInWithEmailAndPassword($scope.form.username, $scope.form.password).then(function() {
                var user = firebase.auth().currentUser;
                if (user) {
                    $scope.Guestname = user.email;
                    $rootScope.base64encodedUser = btoa($scope.Guestname);
                    //sessionService.setUserAuthenticated(true);
                    $state.go('Dashboard.Report.overview');
                    showFilter();
                } else {
                    // No user is signed in.
                    console.log('Signin Unsuccessful')
                }

            }).catch(function(error) {
                // Handle Errors here.
                var errorCode = error.code;
                var errorMessage = error.message;
                if (errorCode === 'auth/wrong-password') {
                    console.log('Invalid Username or Password.');
                    hideLoader();
                } else {
                    console.log(errorMessage);
                    hideLoader();
                }
                console.log(error);
            });

        };

        //Logout function
        $scope.logout = function() {
            //deleting all cookies
            setCookie('UserID', '', -1);
            setCookie('reportid', '', -1);
            setCookie('service', '', -1);
            //window.location.href = 'login/login.aspx';               
            $state.go('login');
        }
    });

} catch (exception) {
    console.log("Exception " + exception);
}

//==================APPLICATION CONTROLLER(General settings)======================//

app.controller('AppCtrl', ['$scope', '$mdSidenav', 'isMobileFilter', '$http', '$state', 'filterCharts', 'chartFilterParameters', '$timeout', 'appSettings', 'ErrorLog', '$mdDialog', '$rootScope', function($scope, $mdSidenav, isMobileFilter, $http, $state, filterCharts, chartFilterParameters, $timeout, appSettings, ErrorLog, $mdDialog, $rootScope) {
    //Following function is responsible for toggling the left side navigation menu bar

    $scope.toggleSidenav = function(menuId) {
        $mdSidenav(menuId).toggle(); //toggle sidenavigation menu bar

    };

    //Following function determines whether to open right side nav (filters) depending on screen size
    $scope.showfilter = function() {
        isMobileFilter.isMobileFilter = true;
        if (window.innerWidth >= 729) {} else {
            $scope.toggleSidenav('right');
        }

    }

    //Get current year to display on footer
    $scope.getYear = function() {
        var date = new Date();
        $scope.year = date.getFullYear();
    }

    //setting ng-view=innerview's top margin to avoid cross browser UI difficulties 
    $scope.setMargin = function() {
        setHomeViewMargin();
    }

    //=========SIDE NAVIGATION MENU=============//

    //Get list of menu 
    $scope.getMenu = function() {
        try {

            $scope.menuOptions; //binding menu from json at app initialization
            //Preparing request parameters to be sent with HTTP request
            var config = {
                method: 'POST',
                url: appSettings.proxyPage,
                data: {
                    requester: "ChartCtrl",
                    requestedUrl: appSettings.requestedUrl,
                    ServiceType: 'GetReports',
                    companyid: "",
                    reporttype: 1,
                    fromdate: '',
                    todate: '',
                    reportid: 1,
                    userid: FetchCookieData('UserID'),
                    avgormax: "",
                    numberofdays: "",
                    insuranceid: ""
                }
            };
            var request = $http(config); //request object
            //now make request
            request.then(function(response) {
                if (response.hasOwnProperty('errortag')) {
                    //if webservice encountered some error
                    ErrorLog.exceptions.push(response.data.errortag);
                    console.log("Menu Exception " + response.data.errortag);
                    hideLoader();
                } else {
                    if (response.data.Table[0] != undefined && response.data.Table[0] != null) {
                        if ((response.data.Table).length != 0) {
                            if (response.data.Table[0].Database_Error.trim() == "") {
                                //Database Error column is blank
                                if (response.data != null && response.data != undefined && response.data != '' && response.data != '\n\n') {
                                    //response is not null
                                    $scope.menuOptions = response.data.Table; //assigne menus
                                    $timeout(function() {
                                        UpdateFilterParameter(); //calling function to setcookies to avoid blank excel/pdf export on page reload
                                    }, 200);
                                    $timeout(function() {
                                        document.getElementsByClassName('icon-bg')[0].style.backgroundImage = "url('" + $scope.menuOptions[0].Icon + "_click.png')"; //set image path
                                        document.getElementsByClassName('icon-bg')[0].style.backgroundRepeat = "no-repeat"; //set background image property  
                                        document.getElementById('nav').style.height = (window.innerHeight - 153) + "px"; //setting heigt of side navigation bar
                                    }, 800);

                                } else {
                                    console.log("Menu: webservice returned null data");
                                }
                            } else {
                                //when webservice encountered some error
                                if (document.getElementById('reportContent') != null && document.getElementById('reportContent') != undefined) { document.getElementById('reportContent').style.display = "none"; }
                                document.getElementById('noClients').style.display = "block";
                                document.getElementById('overviewReport7Nodatatext').style.display = "block";
                                document.getElementById('overviewReport7Nodatatext').innerText = response.data.Table[0].Database_Error;
                                hideLoader();
                            }
                        }
                    } else {
                        if (document.getElementById('reportContent') != null && document.getElementById('reportContent') != undefined) { document.getElementById('reportContent').style.display = "none"; }
                        document.getElementById('noClients').style.display = "block";
                        document.getElementById('overviewReport7Nodatatext').style.display = "block";
                        document.getElementById('overviewReport7Nodatatext').innerText = "Hi, no menu(s) assigned to you yet. Please contact your administrator.";
                        hideLoader();
                    }
                }
            }, function errorCallback(response) {
                //if HTTP request encounters some error
                var time = new Date();
                if (response.statusText != undefined) {
                    var httpErrorObject = {

                        statusCode: response.status,
                        Message2: response.statusText,
                        time: time
                    }
                } else {
                    var httpErrorObject = {
                        Message2: response,
                        time: time
                    }
                }
                //pushing error message in error object to display relative error message
                ErrorLog.httpError.push(httpErrorObject);
                console.log(ErrorLog.httpErrorObject);
                hideLoader();
            })
        } catch (exception) {
            var caughtException = exception;
            var exceptionObject = {
                    Error: caughtException,
                    time: new Date()
                }
                //pushing exception in exception object to display relative error message.
            ErrorLog.exceptions.push(exceptionObject);
            console.log(ErrorLog.exceptions);
        }

    }

    //=================TOGGLE MENU ON MENU ICON CLICK====================//

    //variable for toggle behaviour
    $scope.noneStyle = true; //flag variable 
    try {
        //Toggle the side menu : function called when user clicks menu icon
        $scope.toggleStyle = function() {
            //display uncollapsed view
            if ($scope.noneStyle == true) {
                document.getElementById('headerSidenavLogo').style.display = "block";
                document.getElementById('headerImage').style.display = "block";
                document.getElementById('filterControls').style.marginLeft = "242px";
                document.getElementById('innerviewMargin').style.left = "258px";
                document.getElementById('innerviewMargin').style.width = "80%";

                for (var i = 0; i < document.getElementsByClassName('textMenu').length; i++) {
                    document.getElementsByClassName('textMenu')[i].style.left = "5px";

                }

                document.getElementById('nav-wrapper').style.width = "240px";
                document.getElementById('nav-container').style.width = "240px";
                document.getElementById('nav').style.width = "265px";
                document.getElementById('nav').style.height = (window.innerHeight - 230) + "px";
                $scope.noneStyle = false;      
               
              
            } else {
                //display collapsed view (default view)
                document.getElementById('headerSidenavLogo').style.display = "none";
                document.getElementById('headerImage').style.display = "none";
                document.getElementById('filterControls').style.marginLeft = "55px";
                document.getElementById('innerviewMargin').style.left = "80px";
                document.getElementById('innerviewMargin').style.width = "93%";
                for (var i = 0; i < document.getElementsByClassName('textMenu').length; i++) {
                    document.getElementsByClassName('textMenu')[i].style.left = "-400px";

                }
                var menuBarHeight = window.innerHeight - 160;
                document.getElementById('nav-wrapper').style.width = "54px";
                document.getElementById('nav-container').style.width = "54px";
                document.getElementById('nav').style.height = menuBarHeight;
                document.getElementById('nav').style.height = (window.innerHeight - 135) + "px";
                $scope.noneStyle = true;
                
      
            }
        }

    } catch (exception) {
        var caughtException = exception;
        var exceptionObject = {
                Error: caughtException,
                time: new Date()
            }
            //pushing exception in exception object to display relative error message.
        ErrorLog.exceptions.push(exceptionObject);
        console.log(ErrorLog.exceptions);
    }


    //on hovering menu icons
    $scope.mouseHover = function(img, index) {
        var img = img; //icon image name 
        var index = index; //selected menu icon's index        
        if (index != '-1') { //if not logout
            document.getElementsByClassName('icon-bg')[index].style.backgroundImage = "url('" + img + "_click.png')"; //set image path
            document.getElementsByClassName('icon-bg')[index].style.backgroundRepeat = "no-repeat"; //set background image property         
        }
    }

    //function called on menu click to load corresponding page
    $scope.callWebService = function(url, id) {
        showLoader();
        setHomeViewMargin(); //function to set margin of home view
        filterCharts.section = url; //selected menu's redirection url state
        chartFilterParameters.reportid = id; //set selected report's id 
        filterCharts.isChart = true; //display chart view by default 
        var url = url; //selected menu's redirection url state
        if (url == 'login') {
            //do nothing, because login is written in .net
        } else {
            document.getElementsByClassName('innerView').innerHTML = "";
            //Get Properties from master json , this properties are used in excel export 
            $http.get('chartSettings/config.json').success(function(res) {
                //loop applied on length of json object received from config.json               
                for (i = 0; i <= res.length; i++) {
                    //if current selection's id is equal to one of the ids in json object
                    if (res[i].reportid == chartFilterParameters.reportid) {
                        chartFilterParameters.ServiceType = res[i].ServiceType; //get current report's servive type                         
                        chartFilterParameters.avgormax = res[i].avgormax; //get avgormax dropdown availability                        
                        chartFilterParameters.CompanyidDisplayName = res[i].CompanyidDisplayName; //get name to be displayed for client dropdown
                        chartFilterParameters.DateDisplayName = res[i].DateDisplayName; //get name to be displayed for daterange picker
                        chartFilterParameters.InsuranceidDisplayName = res[i].InsuranceidDisplayName; //get name to be displayed for payer dropdown
                        chartFilterParameters.NumberOfDaysDisplayName = res[i].NumberOfDaysDisplayName; //get name to be displayed for # of days dropdown
                        chartFilterParameters.AvgormaxDisplayName = res[i].AvgormaxDisplayName; // get name to be displayed for avg or max dropdown
                        //if report id 4 or 5 then default value to be displayed in pdf for number of days dropdown
                        if (chartFilterParameters.reportid == 4 || chartFilterParameters.reportid == 5) {
                            chartFilterParameters.numberofdaysname = res[i].Numberofdayspdfvaluename;
                        }
                        setCookie("service", res[i].ServiceType, 1); //to avoid page reload reset behaviour
                        setCookie("reportid", id, 1); //to avoid page reload reset behaviour
                        break;
                    }
                }
                $timeout(function() {
                    filterCharts.isMenu = true;
                    var linkPath = url;
                    if (filterCharts.scope != "") {
                        filterCharts.scope.getValue(); //calling go button function (initially data didn't get refreshed on menu click therefore we directly call go button)
                        $state.go(linkPath); //load mentioned URL
                    }
                });
            })
        }

    };

}])
