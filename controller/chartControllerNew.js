//===chart controller===//
app.controller("ChartCtrl", ['$scope', '$http', 'chartFilterParameters', 'isMobileFilter', 'filterCharts', 'getFilterDataService', 'appSettings', '$timeout', 'getReportHeading', 'httpService', 'getOverviewData', '$rootScope', 'ErrorLog', '$mdDialog', '$q', function($scope, $http, chartFilterParameters, isMobileFilter, filterCharts, getFilterDataService, appSettings, $timeout, getReportHeading, httpService, getOverviewData, $rootScope, ErrorLog, $mdDialog, $q) {
        //getting current controllers scope to call this controller's function in other controller (for cancelling request mainly).
        filterCharts.scope = $scope;
        //define variables and functions for later use for cancelling ongoing request
        filterCharts.canceler = $q.defer();
        var resolved = false; //variable used for cancelling chart request
        var cancel = function() { //sub function for cancelling chart request
            filterCharts.canceler.resolve("http call aborted");
        };

        //==========================================DROPDOWN BINDING===================================================//
        //variables and functions for various dropdown binding.
        $scope.dropdown = {}; //stores client dropdown variables       
        $selectedClientIds = []; //store id's of selected clients
        $selectedAvgOrMax = []; //store selected avg or max
        $selectedPayerIds = []; //store selected payer id's
        $selectedDaysId = []; //store selected number of days
        $scope.showhoursworked = false; //show total hours worked report or not
        $scope.showClientDropdown; //show client dropdown or not  
        var set = 0; //flag variable to avoid displaying dialog again & again when no client/menu present 

        //Get data for Client dropdown
        $scope.getFilterData = function() {
            
            try {
                //display date in datepicker
                var DisplayDate = '';
                if (chartFilterParameters.fromdate.toString() != '' && chartFilterParameters.todate.toString() != '') {

                    DisplayDate = chartFilterParameters.fromdate.toString() + ' - ' + chartFilterParameters.todate.toString();

                }
                DatePickerInitialize(DisplayDate);

                //reading client count from cookie
                var ClientsCount = FetchCookieData('ClientsCount');

                //if clientcount null then don't show dropdown else show dropdown
                if (ClientsCount != '' && ClientsCount != null) {                  
                   
                    //get clients
                    getFilterDataService.async("getClientsWithRestrictedSelection").then(function(response) {
                        if (response.hasOwnProperty('errortag')) {
                            //if webservice encounters some error
                            document.getElementsByClassName('ams-items')[0].innerHTML = "<div id='noData'>" + response.errortag + "</div>";
                        } else {
                            //if response returned by webservice is other than error tag.                     
                            if (response != null && response != undefined && response != "") {
                                //if response is not null and undefined or blank
                                alert(ClientsCount);
                                if (ClientsCount > 1) {                                   
                                     filterCharts.disableClientDropdownClick = false;
                                    $scope.dropdown.clients = response;
                                } else {
                                    //alert("else");
                                    //when only one client is assigned to a user then disable client dropdown click
                                    //$scope.showClientDropdown = true;
                                    filterCharts.disableClientDropdownClick = true;
                                    $scope.dropdown.clients =  response;
                                }
                            } else {
                                //if response is null/undefined/blank then display following message in dropdown container
                                document.getElementsByClassName('ams-items')[0].innerHTML = "<div id='noData'>Something went wrong.Please refresh.</div>";
                            }

                        }
                    }, function errorCallback(response) {
                        //if some error occurs during http request 
                        var time = new Date();
                        if (response.statusText != undefined) {
                            var httpErrorObject = {
                                statusCode: response.status,
                                Message2: response.statusText,
                                time: time
                            }
                        } else {
                            var httpErrorObject = {
                                Message2: response,
                                time: time
                            }
                        }
                        //pushing errormessage in an object, to be displayed as message
                        ErrorLog.httpError.push(httpErrorObject);
                        hideLoader();
                    });

                } else {
                    if (ClientsCount <= 0 && set != 1) {
                        //uncomment following code to display a message when no clients assigned.
                        /*       alert = $mdDialog.alert()
                                   .title('Hi! ')
                                   .content('You have no clients assigned. Please contact your Administrator!')
                                   .ok('CLOSE');

                               $mdDialog
                                   .show(alert)
                                   .finally(function() {
                                       alert = undefined;
                                   });
                               set = 1;*/
                    }
                    $scope.showClientDropdown = false;
                    //hiding jqtransform border
                    document.getElementById('clientFilter').style.display = "none";
                } //visible true else false
            } catch (exception) {
                var caughtException = exception;
                var exceptionObject = {
                    Error: caughtException,
                    time: new Date()
                }
                ErrorLog.exceptions.push(exceptionObject);
                hideLoader();
            }
        }

        //Get data for Average/Max dropdown
        $scope.getAverageDropdownData = function() {
            try {
                getFilterDataService.async("Getavgormax").then(function(response) {

                    if (response.hasOwnProperty('errortag')) {
                        //if webservie encountered error it returns errortag
                        document.getElementsByClassName('ams-items')[0].innerHTML = "<div id='noData'>" + response.errortag + "</div>";
                    } else {
                        //if webservice returned response other than error tag
                        if (response != null && response != undefined && response != "") {
                            //if response not null/undefined /blank then bind data with dropdown
                            $scope.dropdown.avgMax = response;
                        } else {
                            //if response is null/undefined/ blank then display following error message in dropdown container
                            document.getElementsByClassName('ams-items')[0].innerHTML = "<div id='noData'>Something went wrong,Please refresh the page</div>";

                        }
                    }
                }, function errorCallback(response) {
                    // if some error occurs during http request
                    var time = new Date();
                    if (response.statusText != undefined) {
                        var httpErrorObject = {

                            statusCode: response.status,
                            Message2: response.statusText,
                            time: time
                        }
                    } else {
                        var httpErrorObject = {
                            Message2: response,
                            time: time
                        }
                    }
                    //pushing error message in an object, to be used to display relative error message
                    ErrorLog.httpError.push(httpErrorObject);
                    hideLoader();
                });
            } catch (exception) {
                var caughtException = exception;
                var exceptionObject = {
                    Error: caughtException,
                    time: new Date()
                }
                ErrorLog.exceptions.push(exceptionObject);
                hideLoader();
            }
        }

        //Get data for Payer dropdown
        $scope.getPayers = function() {
            try {
                getFilterDataService.async("GetInsurances").then(function(response) {

                    if (response.hasOwnProperty('errortag')) {
                        document.getElementsByClassName('ams-items')[0].innerHTML = "<div id='noData'>" + response.errortag + "</div>";
                    } else {
                        if (response != null && response != undefined && response != "") {
                            $scope.dropdown.payers = response;

                        } else {
                            document.getElementsByClassName('ams-items')[0].innerHTML = "<div id='noData'>Something went wrong,Please refresh the page</div>";

                        }
                    }
                }, function errorCallback(response) {

                    var time = new Date();
                    if (response.statusText != undefined) {
                        var httpErrorObject = {

                            statusCode: response.status,
                            Message2: response.statusText,
                            time: time
                        }
                    } else {
                        var httpErrorObject = {
                            Message2: response,
                            time: time
                        }
                    }
                    ErrorLog.httpError.push(httpErrorObject);
                    hideLoader();
                });
            } catch (exception) {
                var caughtException = exception;
                var exceptionObject = {
                    Error: caughtException,
                    time: new Date()
                }
                ErrorLog.exceptions.push(exceptionObject);
                hideLoader();
            }
        }

        //Get data for Number of days dropdown
        $scope.getDays = function() {
            getFilterDataService.async("GetNumberOfDays").then(function(response) {

                    if (response.hasOwnProperty('errortag')) {
                        //if webservice encounter some error
                        document.getElementsByClassName('ams-items')[0].innerHTML = "<div id='noData'>" + response.errortag + "</div>";
                    } else {
                        // if webservice returns response error other than error tag
                        if (response != null && response != undefined && response != "") {
                            //if returned response is not null/undefined/blank
                            $scope.dropdown.days = response;
                            //by default keep a value selected so that, this value will be sent in http request.
                            angular.forEach($scope.dropdown.days, function(value, key) {
                                if (value.checked == true) {
                                    chartFilterParameters.numberofdays = value.id;
                                    chartFilterParameters.numberofdaysname = value.value;
                                }
                            })
                        } else {
                            //if returned response is null/undefined/blank
                            document.getElementsByClassName('ams-items')[0].innerHTML = "<div id='noData'>Something went wrong,Please refresh the page</div>";

                        }
                    }
                },
                function errorCallback(response) {
                    //if http request encounters some errors.
                    var time = new Date();
                    if (response.statusText != undefined) {
                        var httpErrorObject = {

                            statusCode: response.status,
                            Message2: response.statusText,
                            time: time
                        }
                    } else {
                        var httpErrorObject = {
                            Message2: response,
                            time: time
                        }
                    }
                    //push error in error object, to be displayed later as relative error message.
                    ErrorLog.httpError.push(httpErrorObject);
                    hideLoader();
                });
        }

        //========Dropdown binding code finished==========//


        //=====================================SETTING DEFAULT PROPERTIES FOR CHARTS=================================//

        //checking platform(desktop/mobile) to plot charts(size) accordingly        
        $scope.isWebpage = isWeb();
        if ($scope.isWebpage != true) {
            $scope.showLegend = false;
            $scope.responsive = false;
        } else {
            $scope.showLegend = true;
            $scope.responsive = true;
        }

        //set GLOBAL CHART PROPERTIES(same for all charts)
        Chart.defaults.global.defaultFontColor = appSettings.defaultFontColor;
        Chart.defaults.global.defaultFontSize = appSettings.defaultFontSize;
        Chart.defaults.global.defaultFontFamily = appSettings.defaultFontFamily;

        //=================================================REPORTS SECTION===========================================//

        //function to plot all types of chart except pie chart
        $scope.plotLineChart = function() {
            try {
                showLoader();
                CheckLogin(); //check if user logged in or not
                chartFilterParameters.reporttype = 1; //1=chart 0=grid report
                //variable to store report title and date range.
                $scope.ReportTitle = "";
                $scope.DateRange = "";
                //if resolved variable is true then cancel the request from following if condition
                if (resolved) {
                    cancel();
                }
                //This request's canceller object to be passed to the service
                filterCharts.canceler = $q.defer();
                resolved = true;
                filterCharts.isChart = true; //for toggle between chart and grid
                filterCharts.isMenu = false; //to avoid function calling twice on repot click 
                filterCharts.isRequestCancelled = false; //variable used for cancelling grid request on the basis of this variable's true/false value
                $scope.totalRevenue = ""; //total displayed at end of each report
                //clear chart data on new request
                $scope.labels = "";
                $scope.series = "";
                $scope.data = "";
                //canvas = document.getElementById('canvas'); //remove if not needed 

                //keeping date range selected in date dropdown
                var DisplayDate = '';
                if (chartFilterParameters.fromdate.toString() != '' && chartFilterParameters.todate.toString() != '') {

                    DisplayDate = chartFilterParameters.fromdate.toString() + ' - ' + chartFilterParameters.todate.toString();

                }
                DatePickerInitialize(DisplayDate);

                //Get Report Heading
                var promise = getReportHeading.getReport();
                promise.then(function(response) {
                    if (response.data != null && response.data != undefined && response.data != "\n\n" && response.data != '') {
                        //if received response is not null/undefined/blank then Storing response received from report heading request in different variables

                        $scope.ReportTitle = response.data.Table[0].ReportTitle;
                        $scope.DateRange = '( ' + response.data.Table[0].DateRange + ' )';
                        filterCharts.dateRange = response.data.Table[0].DateRange; //to use this date range in pdf
                        chartFilterParameters.dateRange = response.data.Table[0].DateRange;
                        filterCharts.xAxesLabel = response.data.Table[0].XAxisLabel; //chart x-axis label
                        filterCharts.yAxesLabel = response.data.Table[0].YAxisLabel; //chart y-axis label
                        filterCharts.ChartType = response.data.Table[0].ChartType.toLowerCase(); //chart type pie/line/bar etc
                        filterCharts.BoldCount = response.data.Table[0].BoldCount; //if 0 grid last row bold else not bold
                        filterCharts.currencyFormat = response.data.Table[0].ToolTipFormat; //currency to be displayed on yaxis and tooltips
                        filterCharts.freezeColumnCount = response.data.Table[0].freezeColumnCount; //number of columns to be freezed in grid report
                    } else {
                        //if webservice returned null data
                        console.log("webservice returned null data");
                    }


                    //get currency format to be displayed
                    if (filterCharts.currencyFormat != "" && filterCharts.currencyFormat != null && filterCharts.currencyFormat != undefined) {
                        var currency = getCurrencySign(filterCharts.currencyFormat);
                    } else {
                        var currency = "";
                    }

                    try {

                        //fetching line chart/bar chart property from chart settings
                        $scope.options = appSettings.lineChartProperty;
                        try {
                            //if selected payers is 1 or 0 then display stacked bar else display normal bar
                            if (filterCharts.ChartType == 'bar') {
                                var payerLength = ((chartFilterParameters.insuranceid).split(",")).length;
                                if ((payerLength == 1 || payerLength == 0) && (chartFilterParameters.insuranceid == -1 || chartFilterParameters.insuranceid == "")) {

                                    $scope.options.scales.xAxes[0].stacked = true;

                                } else {

                                    $scope.options.scales.xAxes[0].stacked = false;

                                }
                            }

                            //if currency format is percent
                            if (currency == '%') {
                                //y-axis labels callback function
                                $scope.options.scales.yAxes[0].ticks.callback = function(value, index, values) {
                                    try {
                                        //if value b/w 0 and 100 then display values as it is otherwise round of 
                                        if (parseInt(value) > 0 && parseInt(value) <= 100) {
                                            if (value == 100) {
                                                return (Number(value)) + currency;
                                            } else {
                                                return (Number(value.toFixed(2))).toString().replace(/\B(?=(\d{2})+(?!\d))/g, ".") + currency;
                                            }
                                        } else {
                                            return (Number(value.toFixed(2))) + currency;
                                        }
                                    } catch (exception) {
                                        console.log(exception);
                                    }
                                }

                                //tooltips callback function
                                //chart binidng code
                                var n = 0; //for calculating mod
                                $scope.options.tooltips.callbacks = {
                                    label: function(tooltipItem, data) {
                                        var datasetLabel = data.datasets[tooltipItem.datasetIndex].label || '';
                                        var allData = data.datasets[tooltipItem.datasetIndex].data;
                                        var tooltipLabel = data.labels[tooltipItem.index];
                                        if ((tooltipItem.yLabel).toFixed(0) == 100) {
                                            return datasetLabel + ': ' + Number(tooltipItem.yLabel).toFixed(0) + currency;
                                        } else {
                                            return datasetLabel + ': ' + Number(tooltipItem.yLabel).toFixed(2) + currency;
                                        }
                                    }
                                }

                            } else {
                                //if currency format other than percent
                                $scope.options.scales.yAxes[0].ticks.callback = function(value, index, values) {
                                        //detecting if number is perfect or decimal
                                        if ((value % 1) != 0) {
                                            n = 2; //if decimal display y-axis label to 2 decimal places
                                        }

                                        if (parseInt(value) > 0) {
                                            return currency + (value.toFixed(n)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                        } else if (parseInt(value) < 0) {
                                            return '(' + currency + Math.abs(value.toFixed(n)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + ')';
                                        } else {
                                            return currency + (value.toFixed(n));
                                        }
                                    }
                                    //tooltips callback function
                                $scope.options.tooltips.callbacks = {
                                    title: function() {
                                        // Title doesn't make sense for scatter since we format the data as a point
                                        return '';
                                    },
                                    label: function(tooltipItem, data) {
                                        var datasetLabel = data.datasets[tooltipItem.datasetIndex].label || '';
                                        var allData = data.datasets[tooltipItem.datasetIndex].data;
                                        var tooltipLabel = data.labels[tooltipItem.index];
                                        //if received y-axis label is a number and not not a number
                                        if (!isNaN(tooltipItem.yLabel)) {
                                            //if positive number
                                            if (tooltipItem.yLabel >= 0) {
                                                return datasetLabel + ': ' + currency + Number(tooltipItem.yLabel).toFixed(n).replace(/./g, function(c, i, a) {
                                                    return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "," + c : c;
                                                });
                                            } else {
                                                //if negative number then display within brackets
                                                if (tooltipItem.yLabel < 0) {
                                                    return datasetLabel + ': (' + currency + Math.abs(tooltipItem.yLabel).toFixed(n).replace(/./g, function(c, i, a) {
                                                        return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "," + c : c;
                                                    }) + ')';
                                                }
                                            }
                                        }
                                    }

                                }

                            }

                            //Legend Callback function
                            var cancelledLegend = []; //store legends cancelled by user
                            $scope.options.legend.onClick = function(event, legendItem) {
                                var index = legendItem.datasetIndex;
                                var chart = this.chart;
                                if (!cancelledLegend.includes(index)) {
                                    //if cancelledLegend does not contain selected index then push it 
                                    cancelledLegend.push(index);
                                } else {
                                    //else remove it
                                    var cancelledLegendIndex = cancelledLegend.indexOf(index);
                                    if (cancelledLegendIndex >= 0) {
                                        cancelledLegend.splice(cancelledLegendIndex, 1);
                                    }

                                }
                                var totalLabels = (chart.legend.legendItems).length; //total no. of legends currently displayed on screen      
                                // to avoid blank chart when all legends get cancelled
                                if (cancelledLegend.length != totalLabels) {
                                    //hide legend only if cancelled legend length and total legends is not equal
                                    var meta = chart.getDatasetMeta(index);

                                    // See controller.isDatasetVisible comment
                                    meta.hidden = meta.hidden === null ? !chart.data.datasets[index].hidden : null;
                                } else {
                                    //if cancelled Legend lenght equal to total legend length then remove last legend's index from cancelled legend
                                    cancelledLegend.pop();
                                }
                                // We hid a dataset ... rerender the chart
                                chart.update();
                            }
                            $scope.options.responsive = $scope.responsive; //if true chart will be responsive
                            $scope.options.legend.display = $scope.showLegend; //if true legends will be displayed
                        } catch (exception) {
                            var caughtException = exception;
                            var exceptionObject = {
                                    Error: caughtException,
                                    time: new Date()
                                }
                                //push exceptions in error message object to display it as relative error message
                            ErrorLog.exceptions.push(exceptionObject);
                            hideLoader();
                        }
                    } catch (exception) {
                        var caughtException = exception;
                        var exceptionObject = {
                            Error: caughtException,
                            time: new Date()
                        }
                        ErrorLog.exceptions.push(exceptionObject);
                        hideLoader();
                    }

                    // Make http request to get chart data

                    httpService.async(filterCharts.canceler).then(function(response) {
                        resolved = false;
                        if (response != undefined) {
                            if (response.data.hasOwnProperty('errortag')) {
                                //if webservice encountered some error 
                                document.getElementById('displayChart').style.display = "none";
                                document.getElementById('displayGrid').style.display = "none";
                                document.getElementById('Nodatatext').innerHTML = response.data.errortag;
                                document.getElementById('GridErrorDiv').style.display = "block";
                                hideLoader();
                            } else {
                                if (response.data.Database_Error != undefined) {
                                    if ((response.data.Database_Error).trim() === "") {
                                        //If there is no database error
                                        if (response.data != null && response.data != undefined && response.data != "" && response.data != "\n\n") {
                                            //If returned response is not null/undefined/blank
                                            if (filterCharts.ChartType != "" && filterCharts.ChartType != null) {
                                                //If chart type is not null/undefined/blank
                                                document.getElementById('GridErrorDiv').style.display = "none";
                                                document.getElementById('displayGrid').style.display = "none";
                                                document.getElementById('displayChart').style.display = "block";
                                                try {
                                                    //binding chart data
                                                    $scope.labels = response.data.labels;

                                                    $scope.series = response.data.series;

                                                    $scope.data = response.data.data;

                                                    if ($scope.labels.length == 1) {
                                                        //To display single label at center of x-axis
                                                        $scope.options.scales.xAxes[0].gridLines.offsetGridLines = true;
                                                    }
                                                    $scope.chartType = filterCharts.ChartType; //type of chart to be plotted
                                                    $scope.totalRevenue = response.data.TotalRevenue; //total to be displayed on chart

                                                } catch (exception) {
                                                    console.log(exception);
                                                }

                                                //Set headings of x,y axis                 
                                                $scope.options.scales.yAxes[0].scaleLabel.labelString = filterCharts.yAxesLabel;
                                                $scope.options.scales.xAxes[0].scaleLabel.labelString = filterCharts.xAxesLabel;
                                                hideLoader();


                                            } else {
                                                //If chart type is null/blank
                                                document.getElementById('displayGrid').style.display = "none";
                                                document.getElementById('displayChart').style.display = "none";
                                                document.getElementById('Nodatatext').innerHTML = "Something Went Wrong. Please Refresh!";
                                                document.getElementById('GridErrorDiv').style.display = "block";
                                                $scope.totalRevenue = "";
                                                hideLoader();
                                            }
                                        } else {
                                            //If returned response is null/undefined/blank
                                            document.getElementById('displayGrid').style.display = "none";
                                            document.getElementById('displayChart').style.display = "none";
                                            document.getElementById('Nodatatext').innerHTML = "Something Went Wrong. Please Refresh!";
                                            document.getElementById('GridErrorDiv').style.display = "block";
                                            $scope.totalRevenue = "";
                                            hideLoader();
                                        }
                                    } else {
                                        //if not received Database_Error column in response.                                 
                                        document.getElementById('displayGrid').style.display = "none";
                                        document.getElementById('displayChart').style.display = "none";
                                        document.getElementById('Nodatatext').innerHTML = response.data.Database_Error;
                                        document.getElementById('GridErrorDiv').style.display = "block";
                                        $scope.totalRevenue = "";
                                        hideLoader();
                                    }
                                } else {
                                    document.getElementById('displayGrid').style.display = "none";
                                    document.getElementById('displayChart').style.display = "none";
                                    document.getElementById('Nodatatext').innerHTML = "Something Went Wrong. Please Refresh!";
                                    document.getElementById('GridErrorDiv').style.display = "block";
                                    $scope.totalRevenue = "";
                                    hideLoader();
                                }
                            }
                        } else {
                            if (filterCharts.isRequestCancelled == true) {
                                //If http request cancelled 
                                document.getElementById('displayGrid').style.display = "none";
                                document.getElementById('displayChart').style.display = "none";
                                document.getElementById('Nodatatext').innerHTML = "Loading Cancelled..";
                                document.getElementById('GridErrorDiv').style.display = "block";
                                hideLoader();
                            } else {
                                //if http request not cancelled
                                document.getElementById('displayGrid').style.display = "none";
                                document.getElementById('displayChart').style.display = "none";
                                document.getElementById('GridErrorDiv').style.display = "none";
                                showLoader();
                            }
                            $scope.totalRevenue = "";

                        }
                    }, function errorCallback(response) {
                        //If http request for chart encountered some error
                        var time = new Date();
                        if (response.statusText != undefined) {
                            var httpErrorObject = {

                                statusCode: response.status,
                                Message2: response.statusText,
                                time: time
                            }
                        } else {
                            var httpErrorObject = {
                                Message2: response,
                                time: time
                            }
                        }
                        //pushing error message in error object to display as relative error message.
                        ErrorLog.httpError.push(httpErrorObject);
                        hideLoader();
                    })

                }, function errorCallback(response) {
                    //if http request for report name caught some error 
                    var time = new Date();
                    if (response.statusText != undefined) {
                        var httpErrorObject = {

                            statusCode: response.status,
                            Message2: response.statusText,
                            time: time
                        }
                    } else {
                        var httpErrorObject = {
                            Message2: response,
                            time: time
                        }
                    }
                    //pushing error message in error object to display as relative error message.
                    ErrorLog.httpError.push(httpErrorObject);
                    hideLoader();
                })
            } catch (exception) {
                var caughtException = exception;
                var exceptionObject = {
                    Error: caughtException,
                    time: new Date()
                }
                ErrorLog.exceptions.push(exceptionObject);
                hideLoader();
            }
            //canceller.resolve();

        }


        //=======================================OVERVIEW PAGE SECTION============================================//

        //default overviewpage parameters for showing report heading
        $scope.overviewReport1;
        $scope.overviewReport2;
        $scope.overviewReport3;
        $scope.overviewReport4;
        $scope.overviewReport5;
        $scope.overviewReport6;
        //variable to display each odometer's heading
        $scope.odometer1;
        $scope.odometer2;
        $scope.odometer3;
        //show individual loaders on each report of overview page    
        $scope.overviewReport2Activated = true;
        $scope.overviewReport3Activated = true;
        $scope.overviewReport4Activated = true;
        $scope.overviewReport5Activated = true;
        $scope.overviewReport6Activated = true;

        //Global Averages Overview Report
        $scope.plotOdometer = function() {
            CheckLogin(); //check if user logged in or not
            $scope.ReportTitle = "";
            $scope.DateRange = "";
            filterCharts.isChart = true;

            //Get Report Heading
            var promise = getReportHeading.getReport();
            promise.then(function(response) {
                    try {
                        if (response.data != null && response.data != undefined && response.data != "\n\n" && response.data != '') {
                            //if response not null/undefined/blank
                            $scope.ReportTitle = response.data.Table[0].ReportTitle;
                            $scope.DateRange = '( ' + response.data.Table[0].DateRange + ' )';
                            filterCharts.xAxesLabel = response.data.Table[0].XAxisLabel;
                            filterCharts.yAxesLabel = response.data.Table[0].YAxisLabel;
                            filterCharts.ChartType = response.data.Table[0].ChartType;
                            filterCharts.BoldCount = response.data.Table[0].BoldCount;
                            $scope.currencyFormat = response.data.Table[0].ToolTipFormat;
                            //hideLoader();
                        } else {
                            //if returned response is null/blank/undefined
                            hideLoader();
                        }
                    } catch (exception) {

                        hideLoader();
                    }
                }, function errorCallback(response) {
                    //if http request encountered some error
                    var time = new Date();
                    if (response.statusText != undefined) {
                        var httpErrorObject = {

                            statusCode: response.status,
                            Message2: response.statusText,
                            time: time
                        }
                    } else {
                        var httpErrorObject = {
                            Message2: response,
                            time: time
                        }
                    }
                    //pushing error message in error object
                    ErrorLog.httpError.push(httpErrorObject);
                    hideLoader();
                })
                //variables required to plot ododmeters 
            $scope.daysunit = "";
            $scope.precision = 2;
            //charges/run
            $scope.chargeValue;
            $scope.chargeupperLimit;
            $scope.chargelowerLimit;
            $scope.chargeRanges;
            //daysinar
            $scope.arValue;
            $scope.arupperLimit;
            $scope.arlowerLimit;
            $scope.arRanges;
            //Revenue/run
            $scope.revenueValue;
            $scope.revenueupperLimit;
            $scope.revenuelowerLimit;
            $scope.revenueRanges;
            var config = {
                method: 'POST',
                url: appSettings.proxyPage,
                data: {
                    requester: "ChartCtrl",
                    requestedUrl: appSettings.requestedUrl,
                    ServiceType: 'getOdometerValues',
                    companyid: chartFilterParameters.companyid,
                    reporttype: 1,
                    fromdate: chartFilterParameters.fromdate,
                    todate: chartFilterParameters.todate,
                    reportid: 0,
                    userid: chartFilterParameters.userid,
                    avgormax: "",
                    numberofdays: "",
                    isreport: 1,
                    insuranceid: ""

                }
            };
            try {
                //making http request             
                var request = $http(config);
                request.then(function(response) {
                    if (response.data.hasOwnProperty('errortag')) {
                        //if webservice encountered some error
                        document.getElementById('overviewReport1ErrorDiv').style.display = "block";
                        hideLoader();
                    } else {
                        if (response.data.Database_Error.trim() === "") {
                            //if Database_Error column is blank ("does not contain any error")
                            if (response.data != null && response.data != undefined && response.data != "\n\n" && response.data != '') {
                                //if response is not null/undefined/blank
                                document.getElementById('overviewReport1ErrorDiv').style.display = "none";
                                //binding headings
                                $scope.overviewReport1 = response.data.ReportTitle;
                                $scope.odometer1 = response.data.ChargesPerRun.title;
                                $scope.odometer2 = response.data.DaysinAR.title;
                                $scope.odometer3 = response.data.RevenuePerRun.title;
                                //displaying currency sign in odometer values.
                                var currency = getCurrencySign($scope.currencyFormat);
                                if (currency != undefined) {
                                    $scope.unit = currency;
                                } else {
                                    $scope.unit = '';
                                }
                                //Binding odometer values
                                //charges/run                  
                                if (response.data.ChargesPerRun.ranges[0].min != null && response.data.ChargesPerRun.ranges[0].max != null && response.data.ChargesPerRun.ranges[0].min != undefined && response.data.ChargesPerRun.ranges[0].max != undefined && response.data.ChargesPerRun.ranges[1].min != null && response.data.ChargesPerRun.ranges[1].max != null && response.data.ChargesPerRun.ranges[1].min != undefined && response.data.ChargesPerRun.ranges[1].max != undefined && response.data.ChargesPerRun.value != null) {
                                    document.getElementById('ChargesPerRun').style.display = "block";
                                    document.getElementById('odometerErrorDiv1').style.display = "none";
                                    $scope.chargeValue = response.data.ChargesPerRun.value;
                                    $scope.chargeupperLimit = response.data.ChargesPerRun.upperLimit;
                                    $scope.chargelowerLimit = response.data.ChargesPerRun.lowerLimit;
                                    $scope.chargeRanges = response.data.ChargesPerRun.ranges;
                                    $scope.chargeRanges[0].color = 'green';
                                    $scope.chargeRanges[1].color = 'green';
                                } else {
                                    document.getElementById('ChargesPerRun').style.display = "none";
                                    document.getElementById('odometerErrorDiv1').innerHTML = "Not Available"
                                    document.getElementById('odometerErrorDiv1').style.display = "block";
                                    // $scope.overviewReport1 = "";
                                }
                                //daysinar
                                if (response.data.DaysinAR.ranges[0].min != null && response.data.DaysinAR.ranges[0].max != null && response.data.DaysinAR.ranges[0].min != undefined && response.data.DaysinAR.ranges[0].max != undefined && response.data.DaysinAR.ranges[1].min != null && response.data.DaysinAR.ranges[1].max != null && response.data.DaysinAR.ranges[1].min != undefined && response.data.DaysinAR.ranges[1].max != undefined && response.data.DaysinAR.value != null) {
                                    document.getElementById('DaysInAr').style.display = "block";
                                    document.getElementById('odometerErrorDiv2').style.display = "none";
                                    $scope.arValue = response.data.DaysinAR.value;
                                    $scope.arupperLimit = response.data.DaysinAR.upperLimit;
                                    $scope.arlowerLimit = response.data.DaysinAR.lowerLimit;
                                    $scope.arRanges = response.data.DaysinAR.ranges;
                                    $scope.arRanges[0].color = 'green';
                                    $scope.arRanges[1].color = 'red';
                                } else {
                                    document.getElementById('DaysInAr').style.display = "none";
                                    document.getElementById('odometerErrorDiv2').innerHTML = "Not Available"
                                    document.getElementById('odometerErrorDiv2').style.display = "block";
                                    //$scope.overviewReport1 = "";
                                }
                                //revenue
                                if (response.data.RevenuePerRun.ranges[0].min != null && response.data.RevenuePerRun.ranges[0].max != null && response.data.RevenuePerRun.ranges[0].min != undefined && response.data.RevenuePerRun.ranges[0].max != undefined && response.data.RevenuePerRun.ranges[1].min != null && response.data.RevenuePerRun.ranges[1].max != null && response.data.RevenuePerRun.ranges[1].min != undefined && response.data.RevenuePerRun.ranges[1].max != undefined && response.data.RevenuePerRun.value != null) {
                                    document.getElementById('RevenuePerRun').style.display = "block";
                                    document.getElementById('odometerErrorDiv3').style.display = "none";
                                    $scope.revenueValue = response.data.RevenuePerRun.value;
                                    $scope.revenueupperLimit = response.data.RevenuePerRun.upperLimit;
                                    $scope.revenuelowerLimit = response.data.RevenuePerRun.lowerLimit;
                                    $scope.revenueRanges = response.data.RevenuePerRun.ranges;
                                    $scope.revenueRanges[0].color = 'green';
                                    $scope.revenueRanges[1].color = 'green';
                                    $scope.showDropdown = true;
                                } else {
                                    document.getElementById('RevenuePerRun').style.display = "none";
                                    document.getElementById('odometerErrorDiv3').innerHTML = "Not Available"
                                    document.getElementById('odometerErrorDiv3').style.display = "block";
                                    // $scope.overviewReport1 = "";
                                }
                                hideLoader();
                            } else {
                                //if response is null/undefined/blank
                                document.getElementById('overviewReport1ErrorDiv').style.display = "block";
                                document.getElementById('overviewReport1ErrorDiv').innerHTML = "Something went wrong,Please refresh the page";

                                hideLoader();
                            }
                        } //end of dberror if
                        else {
                            //if Database_Error column contains some value
                            document.getElementById('overviewReport1ErrorDiv').style.display = "block";
                            document.getElementById('overviewReport1ErrorDiv').innerHTML = response.data.Database_Error;
                            hideLoader();
                        }
                    } //end of webservice else
                }, function errorCallback(response) {
                    //if http request encountered some error
                    var time = new Date();
                    if (response.statusText != undefined) {
                        var httpErrorObject = {

                            statusCode: response.status,
                            Message2: response.statusText,
                            time: time
                        }
                    } else {
                        var httpErrorObject = {
                            Message2: response,
                            time: time
                        }
                    }
                    //pushing error message in error object
                    ErrorLog.httpError.push(httpErrorObject);
                    hideLoader();
                })
            } catch (exception) {

                var caughtException = exception;
                var exceptionObject = {
                    Error: caughtException,
                    time: new Date()
                }
                ErrorLog.exceptions.push(exceptionObject);
                hideLoader();

            }
        }

        //Total Claims Waiting to be Billed Overview report
        $scope.totalClaimsWaitingToBeBilled = function() {
            try {
                document.getElementById('canvas2').style.display = "none";
                document.getElementById('LoaderReport2').style.display = "block";
                $scope.overviewReport2Activated = true; //show individual loader
                filterCharts.isChart = true; //is not grid report
                $scope.waitingClaimoptions = appSettings.pieChartProperty; //get chart properties
                var claimsPromise = getOverviewData.getOverviewChartData('getClaimsWaitingToBeBilled'); //ger report heading
                var selectedLegendsData = []; //for removing cancelled legends data from percentage calculation
                claimsPromise.then(function(response) {

                            if (response.data.hasOwnProperty('errortag')) {
                                //If webservice encountered error
                                document.getElementById('overviewReport2').style.display = "none";
                                document.getElementById('overviewReport2Nodatatext').innerHTML = response.data.errortag;
                                document.getElementById('overviewReport2ErrorDiv').style.display = "block";
                                $scope.overviewReport2 = ""; //clearig old data
                                //hiding individual loader
                                $scope.overviewReport2Activated = false;
                                document.getElementById('LoaderReport2').style.display = "none";
                            } else {
                                if (document.getElementById('overviewReport2') != null && document.getElementById('overviewReport2') != undefined) {
                                    if (response.data.Database_Error === "") {
                                        //if Database error has no value
                                        if (response.data != null && response.data != undefined && response.data != "" && response.data != "\n\n") {
                                            //if response is not null
                                            if (response.data.ChartType != "" && response.data.ChartType != null) {
                                                //if charttype is not null
                                                document.getElementById('overviewReport2ErrorDiv').style.display = "none";
                                                document.getElementById('overviewReport2').style.display = "block";
                                                document.getElementById('canvas2').style.display = "block";
                                                //bind chart data
                                                $scope.overviewReport2 = response.data.ReportTitle;
                                                $scope.waitingChartType = response.data.ChartType.toLowerCase();
                                                $scope.waitingClaimlabels = response.data.labels;
                                                $scope.waitingClaimseries = response.data.series;
                                                $scope.waitingClaimdata = response.data.data;
                                                $scope.waitingClaimTotal = response.data.TotalRevenue;
                                                //get currency format
                                                var currency = response.data.ToolTipFormat;
                                                currency = getCurrencySign(currency);
                                                //hiding individual loader
                                                $scope.overviewReport2Activated = false;
                                                document.getElementById('LoaderReport2').style.display = "none";
                                                //tooltip callback function
                                                $scope.waitingClaimoptions.tooltips.callbacks = {
                                                    label: function(tooltipItem, data) {
                                                        var allData = data.datasets[tooltipItem.datasetIndex].data;

                                                        var tooltipLabel = data.labels[tooltipItem.index];

                                                        var tooltipData = allData[tooltipItem.index];

                                                        var total = 0;
                                                        //calculate sum of all the legends available
                                                        for (var i in allData) {
                                                            total = total + parseInt(allData[i]);

                                                        }
                                                        //if no legend is cancelled then calculate percentage from total
                                                        if (selectedLegendsData.length == 0) {
                                                            var tooltipPercentage = ((tooltipData / total) * 100).toFixed(2);
                                                            return tooltipLabel + ': ' + tooltipData + ' (' + tooltipPercentage + '%)';
                                                        } else {
                                                            //if some legends are cancelled then exclude them from percentage calculation
                                                            var eval = 0;
                                                            for (var i in selectedLegendsData) {
                                                                eval = eval + parseInt(selectedLegendsData[i]);
                                                            }

                                                            var tooltipPercentage = ((tooltipData / (total - eval)) * 100).toFixed(2);

                                                            if (tooltipPercentage <= 100) {
                                                                //tooltip percent is not greater than 100
                                                                return tooltipLabel + ': ' + tooltipData + ' (' + tooltipPercentage + '%)';
                                                            }
                                                        }
                                                    }

                                                }

                                                //Legend Callback function
                                                var cancelledLegend = [];
                                                $scope.waitingClaimoptions.legend.onClick = function(event, legendItem) {
                                                    var index = legendItem.index;
                                                    var chart = this.chart;

                                                    var i, ilen, meta;
                                                    //if cancelled legend does not exist in array then push it in array
                                                    if (!cancelledLegend.includes(index)) {

                                                        cancelledLegend.push(index);
                                                        selectedLegendsData.push(chart.data.datasets[0].data[index]); //push selected legend's data

                                                    } else {
                                                        //if cancelled legend already exist in array then remove it from array
                                                        var cancelledLegendIndex = cancelledLegend.indexOf(index);
                                                        if (cancelledLegendIndex >= 0) {

                                                            cancelledLegend.splice(cancelledLegendIndex, 1);

                                                        }
                                                        selectedLegendsData.splice(cancelledLegendIndex, 1); //remove selected legend's data from selectedLegendsData


                                                    }
                                                    var totalLabels = (chart.data.labels).length; //total no. of legends currently displayed on screen      
                                                    if (cancelledLegend.length != totalLabels) {
                                                        //hide legend only if cancelledLegend length is not equal to total legend length
                                                        for (i = 0, ilen = (chart.data.datasets || []).length; i < ilen; ++i) {

                                                            meta = chart.getDatasetMeta(i);
                                                            // toggle visibility of index if exists
                                                            if (meta.data[index]) {
                                                                meta.data[index].hidden = !meta.data[index].hidden;
                                                            }
                                                        }
                                                    } else {
                                                        // if cancelled legend length is equal to total then remove last cancelled to avoid blank chart
                                                        cancelledLegend.pop();
                                                        selectedLegendsData.pop();

                                                    }

                                                    chart.update();


                                                }

                                            } else {
                                                //if chart type is null
                                                document.getElementById('overviewReport2').style.display = "none";
                                                document.getElementById('overviewReport2Nodatatext').innerHTML = "Something Went Wrong. Please Refresh!";
                                                document.getElementById('overviewReport2ErrorDiv').style.display = "block";
                                                $scope.waitingClaimlabels = "";
                                                $scope.waitingClaimseries = "";
                                                $scope.waitingClaimdata = "";
                                                $scope.overviewReport2 = "";
                                                //hiding individual loader
                                                $scope.overviewReport2Activated = false;
                                                document.getElementById('LoaderReport2').style.display = "none";
                                            }
                                        } else {
                                            //if response is null 
                                            document.getElementById('overviewReport2').style.display = "none";
                                            document.getElementById('overviewReport2Nodatatext').innerHTML = "Something went wrong,Please refresh the page.";
                                            document.getElementById('overviewReport2ErrorDiv').style.display = "block";
                                            $scope.waitingClaimlabels = "";
                                            $scope.waitingClaimseries = "";
                                            $scope.waitingClaimdata = "";
                                            $scope.overviewReport2 = "";
                                            //hiding individual report loader
                                            $scope.overviewReport2Activated = false;
                                            document.getElementById('LoaderReport2').style.display = "none";
                                        }
                                    } //database error if
                                    else {
                                        //If Database_Error column is not null
                                        document.getElementById('overviewReport2').style.display = "none";
                                        document.getElementById('overviewReport2ErrorDiv').style.display = "block";
                                        document.getElementById('overviewReport2Nodatatext').innerHTML = response.data.Database_Error;
                                        $scope.overviewReport2 = "";
                                        $scope.waitingClaimlabels = "";
                                        $scope.waitingClaimseries = "";
                                        $scope.waitingClaimdata = "";
                                        //hiding individual loader
                                        $scope.overviewReport2Activated = false;
                                        document.getElementById('LoaderReport2').style.display = "none";
                                    }
                                } //if overview report not avaiable
                            } //else

                        }, //http
                        function errorCallback(response) {
                            // If http request encountered some error
                            var time = new Date();
                            if (response.statusText != undefined) {
                                var httpErrorObject = {

                                    statusCode: response.status,
                                    Message2: response.statusText,
                                    time: time
                                }
                            } else {
                                var httpErrorObject = {
                                    Message2: response,
                                    time: time
                                }
                            }
                            //push error message in error object
                            ErrorLog.httpError.push(httpErrorObject);
                            //hiding individual loader
                            $scope.overviewReport2Activated = false;
                            document.getElementById('LoaderReport2').style.display = "none";
                        })
                    // $scope.callOverviewPageServices(2, 'getClaimsWaitingToBeBilled');
            } catch (exception) {

                var caughtException = exception;
                var exceptionObject = {
                    Error: caughtException,
                    time: new Date()
                }
                ErrorLog.exceptions.push(exceptionObject);
                //hiding individual loader
                $scope.overviewReport2Activated = false;
                document.getElementById('LoaderReport2').style.display = "none";

            }
        }

        /*Medicare days over 90 report*/
        $scope.medicareDays = function() {
            try {
                document.getElementById('canvas3').style.display = "none";
                document.getElementById('LoaderReport3').style.display = "block";
                $scope.overviewReport3Activated = true; //show individual loader               
                filterCharts.isChart = false;
                $scope.medicareoptions = appSettings.overviewMedicareOverReportChartProperty; //Get chart properties
                var medicarePromise = getOverviewData.getOverviewChartData('getPercentOutstandingClaims'); //Get report heading
                medicarePromise.then(function(response) {

                    if (response.data.hasOwnProperty('errortag')) {
                        //if webservice encountered some error
                        document.getElementById('overviewReport3').style.display = "none";
                        document.getElementById('overviewReport3Nodatatext').innerHTML = response.data.errortag;
                        document.getElementById('overviewReport3ErrorDiv').style.display = "block";
                        $scope.overviewReport3 = "";
                        //hiding individual loader
                        $scope.overviewReport3Activated = false;
                        document.getElementById('LoaderReport3').style.display = "none";
                    } else {
                        if (document.getElementById('overviewReport3') != null && document.getElementById('overviewReport3') != undefined) { //checking to avoid style erros
                            if (response.data.Database_Error.trim() === "") {
                                //if Databse_Error column has no value
                                if (response.data != null && response.data != undefined && response.data != "" && response.data != "\n\n") {
                                    //if response is not null
                                    if (response.data.ChartType != "" && response.data.ChartType != null) {
                                        //if chart type is not null
                                        document.getElementById('overviewReport3ErrorDiv').style.display = "none";
                                        document.getElementById('overviewReport3').style.display = "block";
                                        document.getElementById('canvas3').style.display = "block";

                                        //bind chart data
                                        $scope.overviewReport3 = response.data.ReportTitle;
                                        $scope.medicareChartType = (response.data.ChartType).toLowerCase();
                                        $scope.medicarelabels = response.data.labels;
                                        $scope.medicareseries = response.data.series;
                                        $scope.medicaredata = response.data.data;

                                        //set x axis/yaxis label
                                        $scope.medicareoptions.scales.yAxes[0].scaleLabel.labelString = response.data.YAxisLabel;
                                        $scope.medicareoptions.scales.xAxes[0].scaleLabel.labelString = response.data.XAxisLabel;

                                        //hiding individual loader
                                        $scope.overviewReport3Activated = false;
                                        document.getElementById('LoaderReport3').style.display = "none";

                                        //set currency format
                                        if (response.data.ToolTipFormat != "" && response.data.ToolTipFormat != null && response.data.ToolTipFormat != undefined) {
                                            var currency = response.data.ToolTipFormat;
                                            currency = getCurrencySign(currency);
                                        } else {
                                            var currency = '';
                                        }
                                        var n = 0; // for setting decimal places on basis of mod calculation

                                        //if currenct type is percent
                                        if (currency == '%') {
                                            //y -axis label callback function
                                            $scope.medicareoptions.scales.yAxes[0].ticks.callback = function(value, index, values) {
                                                try {

                                                    if (parseInt(value) > 0) {

                                                        return (Number(value.toFixed(0))).toString().replace(/\B(?=(\d{2})+(?!\d))/g, ".") + currency;
                                                    } else {
                                                        return (Number(value.toFixed(0))) + currency;
                                                    }
                                                } catch (exception) {
                                                    console.log(exception);
                                                }
                                            }

                                            // Tooltips callback function
                                            $scope.medicareoptions.tooltips.callbacks = {
                                                label: function(tooltipItem, data) {
                                                    var datasetLabel = data.datasets[tooltipItem.datasetIndex].label || '';
                                                    var allData = data.datasets[tooltipItem.datasetIndex].data;
                                                    var tooltipLabel = data.labels[tooltipItem.index];
                                                    return datasetLabel + ': ' + Number(tooltipItem.yLabel).toFixed(2) + currency;
                                                }
                                            }

                                        } else {
                                            // If currency type is other than percent
                                            //yaxis label callback function
                                            $scope.medicareoptions.scales.yAxes[0].ticks.callback = function(value, index, values) {
                                                //taking mod to decide whether to display label or not on y axis
                                                if ((value % 1) != 0) {
                                                    n = 2;
                                                }
                                                if (parseInt(value) > 0) {
                                                    //if value is positive
                                                    return currency + value.toFixed(n).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                                } else if (parseInt(value) < 0) {
                                                    //if value is negative
                                                    return '(' + currency + Math.abs(value.toFixed(n)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + ')';
                                                } else {
                                                    return currency + (value.toFixed(n));
                                                }
                                            }

                                            //medicare tooltips callback function
                                            $scope.medicareoptions.tooltips.callbacks = {
                                                label: function(tooltipItem, data) {
                                                    var datasetLabel = data.datasets[tooltipItem.datasetIndex].label || '';
                                                    var allData = data.datasets[tooltipItem.datasetIndex].data;
                                                    var tooltipLabel = data.labels[tooltipItem.index];
                                                    if (tooltipItem.yLabel > 0) {
                                                        return datasetLabel + ': ' + currency + Number(tooltipItem.yLabel).toFixed(n).replace(/./g, function(c, i, a) {
                                                            return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "," + c : c;
                                                        });
                                                    } else {
                                                        if (tooltipItem.yLabel < 0) {
                                                            return datasetLabel + ': (' + currency + Math.abs(tooltipItem.yLabel).toFixed(n).replace(/./g, function(c, i, a) {
                                                                return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "," + c : c;
                                                            }) + ')';
                                                        }
                                                    }
                                                }
                                            }

                                        }
                                        //Legend callback function
                                        var cancelledLegend = [];
                                        $scope.medicareoptions.legend.onClick = function(event, legendItem) {
                                            var index = legendItem.datasetIndex;
                                            var chart = this.chart;
                                            //if cancelledLegend array does not contain selected legend then add it
                                            if (!cancelledLegend.includes(index)) {
                                                cancelledLegend.push(index);

                                            } else {
                                                //if cancelledLegend array does contain selected legend then remove it
                                                var cancelledLegendIndex = cancelledLegend.indexOf(index);
                                                if (cancelledLegendIndex >= 0) {
                                                    cancelledLegend.splice(cancelledLegendIndex, 1);
                                                }

                                            }
                                            var totalLabels = (chart.legend.legendItems).length; //total no. of legends currently displayed on screen      

                                            //hide legend only if cancelled legend array length not equal to total labels
                                            if (cancelledLegend.length != totalLabels) {
                                                var meta = chart.getDatasetMeta(index);
                                                // See controller.isDatasetVisible comment
                                                meta.hidden = meta.hidden === null ? !chart.data.datasets[index].hidden : null;
                                            } else {
                                                // if cancelled legend length is equal to total then remove last cancelled to avoid blank chart
                                                cancelledLegend.pop();
                                            }
                                            // We hid a dataset ... rerender the chart
                                            chart.update();
                                        }
                                        $scope.medicareoptions.responsive = $scope.responsive; //if true chart will be responsive
                                        $scope.medicareoptions.legend.display = $scope.showLegend; //if true legends will be displayed
                                        //hide individual Loader
                                        $scope.overviewReport3Activated = false;
                                        document.getElementById('LoaderReport3').style.display = "none";
                                    } else {
                                        //if chart type is null
                                        document.getElementById('overviewReport3').style.display = "none";
                                        document.getElementById('overviewReport3Nodatatext').innerHTML = "Something Went Wrong. Please Refresh!";
                                        document.getElementById('overviewReport3ErrorDiv').style.display = "block";
                                        //clear old data (following variables are used in html)
                                        $scope.medicarelabels = "";
                                        $scope.medicareseries = "";
                                        $scope.medicaredata = "";
                                        $scope.overviewReport3 = "";
                                        //hide inidividual Loader
                                        $scope.overviewReport3Activated = false;
                                        document.getElementById('LoaderReport3').style.display = "none";
                                    }
                                } else {
                                    //if response is null
                                    document.getElementById('overviewReport3').style.display = "none";
                                    document.getElementById('overviewReport3Nodatatext').innerHTML = "Something Went Wrong,Please Refresh!";
                                    document.getElementById('overviewReport3ErrorDiv').style.display = "block";
                                    //clear old data (following variables are used in html)
                                    $scope.overviewReport3 = "";
                                    $scope.medicarelabels = "";
                                    $scope.medicareseries = "";
                                    $scope.medicaredata = "";
                                    //hide individual Loader
                                    $scope.overviewReport3Activated = false;
                                    document.getElementById('LoaderReport3').style.display = "none";
                                }
                            } else {
                                //if Database_Error column is not null
                                document.getElementById('overviewReport3').style.display = "none";
                                document.getElementById('overviewReport3Nodatatext').innerHTML = response.data.Database_Error;
                                document.getElementById('overviewReport3ErrorDiv').style.display = "block";
                                //clear old data (following variables are used in html)
                                $scope.overviewReport3 = "";
                                $scope.medicarelabels = "";
                                $scope.medicareseries = "";
                                $scope.medicaredata = "";
                                //hide individual Loader
                                $scope.overviewReport3Activated = false;
                                document.getElementById('LoaderReport3').style.display = "none";
                            } //if database error
                        }
                    } //else

                }, function errorCallback(response) {
                    //if http request encountered some error
                    var time = new Date();
                    if (response.statusText != undefined) {
                        var httpErrorObject = {

                            statusCode: response.status,
                            Message2: response.statusText,
                            time: time
                        }
                    } else {
                        var httpErrorObject = {
                            Message2: response,
                            time: time
                        }
                    }
                    ErrorLog.httpError.push(httpErrorObject);
                    //hide individual Loader
                    $scope.overviewReport3Activated = false;
                    document.getElementById('LoaderReport3').style.display = "none";
                })

            } catch (exception) {

                var caughtException = exception;
                var exceptionObject = {
                    Error: caughtException,
                    time: new Date()
                }
                ErrorLog.exceptions.push(exceptionObject);
                //hide individual Loader
                $scope.overviewReport3Activated = false;
                document.getElementById('LoaderReport3').style.display = "none";
            }
        }

        /*TOTAL HOURS WORKED REPORT*/
        $scope.totalHoursWorked = function() {
            try {
                document.getElementById('canvas4').style.display = "none";
                document.getElementById('LoaderReport4').style.display = "block";
                $scope.overviewReport4Activated = true; //show individual loader
                $timeout(function() {
                    $scope.hoursworkedoptions = appSettings.overviewTotalHoursReportChartProperty; //get chart properties
                    var hoursworkedPromise = getOverviewData.getOverviewChartData('getTotalHoursWorked'); //get report heading
                    hoursworkedPromise.then(function(response) {

                        if (response.data.hasOwnProperty('errortag')) {
                            //if webservice encountered error
                            document.getElementById('overviewReport4').style.display = "none";
                            document.getElementById('overviewReport4Nodatatext').innerHTML = response.data.errortag;
                            document.getElementById('overviewReport4ErrorDiv').style.display = "block";
                            $scope.overviewReport4 = "";
                            //hide individual Loader
                            $scope.overviewReport4Activated = false;
                            document.getElementById('LoaderReport4').style.display = "none";
                        } else {
                            if (document.getElementById('overviewReport4') != null && document.getElementById('overviewReport4') != undefined) { //check to avoid style errors
                                if (response.data.Database_Error.trim() === "") {
                                    //If database_error column has no value
                                    if (response.data != null && response.data != undefined && response.data != "" && response.data != "\n\n") {
                                        //if response is not null
                                        if (response.data.ChartType != "" && response.data.ChartType != null) {
                                            //if charttype is not null
                                            document.getElementById('overviewReport4ErrorDiv').style.display = "none";
                                            document.getElementById('overviewReport4').style.display = "block";
                                            document.getElementById('canvas4').style.display = "block";
                                            //bind chart data
                                            $scope.overviewReport4 = response.data.ReportTitle;
                                            $scope.totalhoursChartType = (response.data.ChartType).toLowerCase();
                                            $scope.totalhourslabels = response.data.labels;
                                            $scope.totalhoursseries = response.data.series;
                                            $scope.totalhoursdata = response.data.data;
                                            //set x/y axis labels
                                            $scope.hoursworkedoptions.scales.yAxes[0].scaleLabel.labelString = response.data.YAxisLabel;
                                            $scope.hoursworkedoptions.scales.xAxes[0].scaleLabel.labelString = response.data.XAxisLabel;
                                            //hide individual Loader
                                            $scope.overviewReport4Activated = false;
                                            document.getElementById('LoaderReport4').style.display = "none";
                                            //set currency format
                                            if (response.data.ToolTipFormat != "" && response.data.ToolTipFormat != null && response.data.ToolTipFormat != undefined) {
                                                var currency = response.data.ToolTipFormat;

                                                currency = getCurrencySign(currency);
                                            } else {
                                                var currency = "";
                                            }
                                            var n = 0;
                                            //If currency format is percent
                                            if (currency == '%') {
                                                //y-axis scale callback functions
                                                $scope.hoursworkedoptions.scales.yAxes[0].ticks.callback = function(value, index, values) {
                                                        if (parseInt(value) > 0) {
                                                            return currency + (value.toFixed(0)).toString().replace(/\B(?=(\d{2})+(?!\d))/g, ".");
                                                        } else {
                                                            return (value.toFixed(0)) + currency;
                                                        }
                                                    }
                                                    //tooltips callback function
                                                $scope.hoursworkedoptions.tooltips.callbacks = {
                                                    label: function(tooltipItem, data) {
                                                        var datasetLabel = data.datasets[tooltipItem.datasetIndex].label || '';
                                                        var allData = data.datasets[tooltipItem.datasetIndex].data;
                                                        var tooltipLabel = data.labels[tooltipItem.index];
                                                        return datasetLabel + ': ' + Number(tooltipItem.yLabel).toFixed(2) + currency;
                                                    }
                                                }

                                            } else {
                                                //if currency format is other than percent
                                                //scales callback function
                                                $scope.hoursworkedoptions.scales.yAxes[0].ticks.callback = function(value, index, values) {
                                                        //display number to two decimal places if number is not a perfect number
                                                        if ((value % 1) != 0) {
                                                            n = 2;
                                                        }
                                                        if (parseInt(value) > 0) {
                                                            //If no. is positive
                                                            return currency + (value.toFixed(n)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                                        } else if (parseInt(value) < 0) {
                                                            //if number is negative
                                                            return '(' + currency + Math.abs(value.toFixed(n)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + ')';
                                                        } else {
                                                            return currency + (value.toFixed(n));
                                                        }
                                                        //tooltip callback function
                                                        $scope.hoursworkedoptions.tooltips.callbacks = {
                                                            label: function(tooltipItem, data) {
                                                                var datasetLabel = data.datasets[tooltipItem.datasetIndex].label || '';
                                                                var allData = data.datasets[tooltipItem.datasetIndex].data;
                                                                var tooltipLabel = data.labels[tooltipItem.index];
                                                                if (tooltipItem.yLabel > 0) {
                                                                    //if value is positive
                                                                    return datasetLabel + ': ' + currency + Number(tooltipItem.yLabel).toFixed(n).replace(/./g, function(c, i, a) {
                                                                        return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "," + c : c;
                                                                    });
                                                                } else {

                                                                    //if value negative then display it within brackets
                                                                    if (tooltipItem.yLabel < 0) {
                                                                        return datasetLabel + ': (' + currency + Math.abs(tooltipItem.yLabel).toFixed(n).replace(/./g, function(c, i, a) {
                                                                            return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "," + c : c;
                                                                        }) + ')';
                                                                    }
                                                                }
                                                            }
                                                        }

                                                    }
                                                    //Legend Callback function
                                                var cancelledLegend = []; //store legends cancelled by user                                        
                                                $scope.hoursworkedoptions.legend.onClick = function(event, legendItem) {
                                                    var index = legendItem.datasetIndex; //selected legend's index
                                                    var chart = this.chart; //all chart properties,data
                                                    //if cancelled legend array doesn't have selected index addit else remove it from array
                                                    if (!cancelledLegend.includes(index)) {
                                                        cancelledLegend.push(index);

                                                    } else {
                                                        var cancelledLegendIndex = cancelledLegend.indexOf(index);
                                                        if (cancelledLegendIndex >= 0) {
                                                            cancelledLegend.splice(cancelledLegendIndex, 1);
                                                        }

                                                    }
                                                    var totalLabels = (chart.legend.legendItems).length; //total no. of legends currently displayed on screen      
                                                    //if cancelled legend array length not equal to totlalegned then hide selected legend else don't hide legend
                                                    if (cancelledLegend.length != totalLabels) {
                                                        var meta = chart.getDatasetMeta(index);

                                                        // See controller.isDatasetVisible comment
                                                        meta.hidden = meta.hidden === null ? !chart.data.datasets[index].hidden : null;
                                                    } else {
                                                        cancelledLegend.pop();
                                                    }
                                                    // We hid a dataset ... rerender the chart
                                                    chart.update();
                                                }
                                                $scope.hoursworkedoptions.responsive = $scope.responsive; //if true chart will be responsive
                                                $scope.hoursworkedoptions.legend.display = $scope.showLegend; //if true legends will be displayed
                                                //hide indivdual loader 
                                                $scope.overviewReport4Activated = false;
                                                document.getElementById('LoaderReport4').style.display = "none";
                                            }
                                        } else {
                                            //if chart type null
                                            document.getElementById('overviewReport3').style.display = "none";
                                            document.getElementById('overviewReport3Nodatatext').innerHTML = "Something Went Wrong. Please Refresh!";
                                            document.getElementById('overviewReport3ErrorDiv').style.display = "block";
                                            //clear old data (following variables are used in html)
                                            $scope.totalhourslabels = "";
                                            $scope.totalhoursseries = "";
                                            $scope.totalhoursdata = "";
                                            $scope.overviewReport4 = "";
                                            //hideLoader
                                            $scope.overviewReport4Activated = false;
                                            document.getElementById('LoaderReport4').style.display = "none";
                                        }
                                    } else {
                                        //if response is null
                                        document.getElementById('overviewReport4').style.display = "none";
                                        document.getElementById('overviewReport4Nodatatext').innerHTML = "Something Went Wrong,Please Refresh";
                                        document.getElementById('overviewReport4ErrorDiv').style.display = "block";
                                        //clear old data (following variables are used in html)
                                        $scope.overviewReport4 = "";
                                        $scope.totalhourslabels = "";
                                        $scope.totalhoursseries = "";
                                        $scope.totalhoursdata = "";
                                        //hideLoader
                                        $scope.overviewReport4Activated = false;
                                        document.getElementById('LoaderReport4').style.display = "none";
                                    }
                                } else {
                                    //if database error column is not null
                                    document.getElementById('overviewReport4').style.display = "none";
                                    document.getElementById('overviewReport4Nodatatext').innerHTML = response.data.Database_Error;
                                    document.getElementById('overviewReport4ErrorDiv').style.display = "block";
                                    $scope.overviewReport4 = "";
                                    $scope.totalhourslabels = "";
                                    $scope.totalhoursseries = "";
                                    $scope.totalhoursdata = "";
                                    //hideLoader
                                    $scope.overviewReport4Activated = false;
                                    document.getElementById('LoaderReport4').style.display = "none";
                                } //if-else database error end
                            } //if overview html available or not
                        } //else end

                    }, function errorCallback(response) {
                        // if http error encountered some error
                        var time = new Date();
                        if (response.statusText != undefined) {
                            var httpErrorObject = {

                                statusCode: response.status,
                                Message2: response.statusText,
                                time: time
                            }
                        } else {
                            var httpErrorObject = {
                                Message2: response,
                                time: time
                            }
                        }
                        ErrorLog.httpError.push(httpErrorObject); //push error message in error object
                        //hideLoader
                        $scope.overviewReport4Activated = false;
                        document.getElementById('LoaderReport4').style.display = "none";
                    })

                }, 1000);
            } catch (exception) {
                var caughtException = exception;
                var exceptionObject = {
                    Error: caughtException,
                    time: new Date()
                }
                ErrorLog.exceptions.push(exceptionObject);
                //hideLoader
                $scope.overviewReport4Activated = false;
                document.getElementById('LoaderReport4').style.display = "none";
            }
        }

        /*AVERAGE DAYS TO BILL*/
        $scope.averageDaysToBill = function() {
            try {
                //hiding chart canvas and displaying laoder 
                document.getElementById('canvas5').style.display = "none";
                document.getElementById('LoaderReport5').style.display = "block";
                $scope.overviewReport5Activated = true;
                filterCharts.isChart = true; // ischart or grid
                $scope.avgoptions = appSettings.overviewAverageReport; //get chart settings
                var avgPromise = getOverviewData.getOverviewChartData('getAverageDaysToBill'); // get report heading
                avgPromise.then(function(response) {

                    if (response.data.hasOwnProperty('errortag')) {
                        //if webservice encountered some error
                        document.getElementById('overviewReport5').style.display = "none";
                        document.getElementById('overviewReport5Nodatatext').innerHTML = response.data.errortag;
                        document.getElementById('overviewReport5ErrorDiv').style.display = "block";
                        $scope.overviewReport5 = "";
                        //hide individual loader
                        $scope.overviewReport5Activated = false;
                        document.getElementById('LoaderReport5').style.display = "none";

                    } else {
                        if (document.getElementById('overviewReport5') != null && document.getElementById('overviewReport5') != undefined) { //check to avoid sytle error
                            if (response.data.Database_Error.trim() === "") {
                                //if Database_Error is blank
                                if (response.data != null && response.data != undefined && response.data != "" && response.data != "\n\n") {
                                    //if response is not null
                                    if (response.data.ChartType != "" && response.data.ChartType != null) {
                                        //if chart type is not null
                                        document.getElementById('overviewReport5ErrorDiv').style.display = "none";
                                        document.getElementById('overviewReport5').style.display = "block";
                                        document.getElementById('canvas5').style.display = "block";
                                        //bind chart data & report heading
                                        $scope.overviewReport5 = response.data.ReportTitle;
                                        $scope.avgdaysChartType = (response.data.ChartType).toLowerCase();
                                        $scope.avgdayslabels = response.data.labels;
                                        $scope.avgdaysseries = response.data.series;
                                        $scope.avgdaysdata = response.data.data;
                                        //set x/y axis labels
                                        $scope.avgoptions.scales.yAxes[0].scaleLabel.labelString = response.data.YAxisLabel;
                                        $scope.avgoptions.scales.xAxes[0].scaleLabel.labelString = response.data.XAxisLabel;
                                        //hide individual Loader
                                        $scope.overviewReport5Activated = false;
                                        document.getElementById('LoaderReport5').style.display = "none";
                                        //Setting Currency format
                                        if (response.data.ToolTipFormat != "" && response.data.ToolTipFormat != null && response.data.ToolTipFormat != undefined) {
                                            var currency = response.data.ToolTipFormat;
                                            currency = getCurrencySign(currency);
                                        } else {
                                            var currency = "";
                                        }
                                        var n = 0; //to calculate mod for deciding whether to display number in decimal format or not
                                        //if currency format is percent
                                        if (currency == '%') {
                                            //y-axis scale callback function
                                            $scope.avgoptions.scales.yAxes[0].ticks.callback = function(value, index, values) {
                                                    if (parseInt(value) > 0) {
                                                        //if value is positive
                                                        return currency + (value.toFixed(0)).toString().replace(/\B(?=(\d{2})+(?!\d))/g, ".");
                                                    } else {
                                                        return (value.toFixed(0)) + currency; //didn't use bracket because percent can't be negative
                                                    }
                                                }
                                                //Tooltip callback function
                                            $scope.avgoptions.tooltips.callbacks = {
                                                label: function(tooltipItem, data) {
                                                    var datasetLabel = data.datasets[tooltipItem.datasetIndex].label || '';
                                                    var allData = data.datasets[tooltipItem.datasetIndex].data;
                                                    var tooltipLabel = data.labels[tooltipItem.index];
                                                    return datasetLabel + ': ' + Number(tooltipItem.yLabel).toFixed(2) + currency;
                                                }
                                            }
                                            var cancelledLegend = []; //store legends cancelled by user


                                        } else {
                                            //if currency format is other than percent
                                            //y-axis scales callback function
                                            $scope.avgoptions.scales.yAxes[0].ticks.callback = function(value, index, values) {
                                                if ((value % 1) != 0) {
                                                    n = 2;
                                                }
                                                if (parseInt(value) > 0) {
                                                    //if value is positive
                                                    return currency + (value.toFixed(n)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                                } else {
                                                    if (parseInt(value) < 0) {
                                                        //if value is negative
                                                        return '(' + currency + Math.abs(value.toFixed(n)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + ')';
                                                    } else {
                                                        return currency + (value.toFixed(n));
                                                    }
                                                }

                                                //Tooltips callback function 
                                                $scope.avgoptions.tooltips.callbacks = {
                                                    label: function(tooltipItem, data) {
                                                        var datasetLabel = data.datasets[tooltipItem.datasetIndex].label || ''; //get label of selected point
                                                        var allData = data.datasets[tooltipItem.datasetIndex].data; //get all the data plotted on current chart in array
                                                        var tooltipLabel = data.labels[tooltipItem.index]; //get legend name
                                                        if (tooltipItem.yLabel > 0) { //if value positive
                                                            return datasetLabel + ': ' + currency + Number(tooltipItem.yLabel).toFixed(n).replace(/./g, function(c, i, a) {
                                                                return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "," + c : c;
                                                            });
                                                        } else {
                                                            if (tooltipItem.yLabel < 0) { //if value negative
                                                                return datasetLabel + ': (' + currency + Math.abs(tooltipItem.yLabel).toFixed(n).replace(/./g, function(c, i, a) {
                                                                    return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "," + c : c;
                                                                }) + ')';
                                                            }
                                                        }
                                                    }
                                                }
                                            }

                                            //Legend callback function
                                            $scope.avgoptions.legend.onClick = function(event, legendItem) {
                                                var index = legendItem.datasetIndex; //get index of cancelled legend
                                                var chart = this.chart; //all chart properties,data
                                                if (!cancelledLegend.includes(index)) { //push index of cancelled legend if doesn't already exist in array
                                                    cancelledLegend.push(index);

                                                } else { //if index already exist in array then remove it from array
                                                    var cancelledLegendIndex = cancelledLegend.indexOf(index);
                                                    if (cancelledLegendIndex >= 0) {
                                                        cancelledLegend.splice(cancelledLegendIndex, 1);
                                                    }

                                                }
                                                var totalLabels = (chart.legend.legendItems).length; //total no. of legends currently displayed on screen      

                                                if (cancelledLegend.length != totalLabels) {
                                                    //if no. of cancelled legends not equal total no. of legends than hide the selected legend else don't hide to avoid blank chart display
                                                    var meta = chart.getDatasetMeta(index);

                                                    // See controller.isDatasetVisible comment
                                                    meta.hidden = meta.hidden === null ? !chart.data.datasets[index].hidden : null;
                                                } else {
                                                    cancelledLegend.pop(); // if no.of cancelled legend equal to total no. of legend than remove the legend from cancelled legend array.
                                                }
                                                // We hid a dataset ... rerender the chart
                                                chart.update();
                                            }
                                            $scope.avgoptions.responsive = $scope.responsive; //if true chart will be responsive
                                            $scope.avgoptions.legend.display = $scope.showLegend; //if true legends will be displayed
                                            //hide individual report loader
                                            $scope.overviewReport5Activated = false;
                                            document.getElementById('LoaderReport5').style.display = "none";
                                        }
                                    } else {
                                        //if charttype is null
                                        document.getElementById('overviewReport3').style.display = "none";
                                        document.getElementById('overviewReport3Nodatatext').innerHTML = "Something Went Wrong. Please Refresh!";
                                        document.getElementById('overviewReport3ErrorDiv').style.display = "block";
                                        $scope.avgdayslabels = "";
                                        $scope.avgdaysseries = "";
                                        $scope.avgdaysdata = "";
                                        $scope.overviewReport5 = "";
                                        //hideLoader
                                        $scope.overviewReport5Activated = false;
                                        document.getElementById('LoaderReport5').style.display = "none";
                                    }
                                } else {
                                    //if response is null
                                    document.getElementById('overviewReport5').style.display = "none";
                                    document.getElementById('overviewReport5Nodatatext').innerHTML = "Something Went Wrong. Please Refresh!";
                                    document.getElementById('overviewReport5ErrorDiv').style.display = "block";
                                    $scope.overviewReport5 = "";
                                    $scope.avgdayslabels = "";
                                    $scope.avgdaysseries = "";
                                    $scope.avgdaysdata = "";
                                    //hideLoader
                                    $scope.overviewReport5Activated = false;
                                    document.getElementById('LoaderReport5').style.display = "none";
                                }
                            } else {
                                //if Database_Error column is has some value 
                                document.getElementById('overviewReport5').style.display = "none";
                                document.getElementById('overviewReport5Nodatatext').innerHTML = response.data.Database_Error;
                                document.getElementById('overviewReport5ErrorDiv').style.display = "block";
                                //clear old data (following variables are used in html)
                                $scope.avgdayslabels = "";
                                $scope.avgdaysseries = "";
                                $scope.avgdaysdata = "";
                                $scope.overviewReport5 = "";
                                //hide individual Loader
                                $scope.overviewReport5Activated = false;
                                document.getElementById('LoaderReport5').style.display = "none";
                            } //if-else database error end
                        } //if overview html available or not 
                    } //else

                }, function errorCallback(response) {
                    //if http request encounters some error
                    var time = new Date();
                    if (response.statusText != undefined) {
                        var httpErrorObject = {

                            statusCode: response.status,
                            Message2: response.statusText,
                            time: time
                        }
                    } else {
                        var httpErrorObject = {
                            Message2: response,
                            time: time
                        }
                    }
                    //push error message in error object to display relative error message to user
                    ErrorLog.httpError.push(httpErrorObject);
                    //hide individual loader
                    $scope.overviewReport5Activated = false;
                    document.getElementById('LoaderReport5').style.display = "none";
                })
            } catch (exception) {
                var caughtException = exception;
                var exceptionObject = {
                    Error: caughtException,
                    time: new Date()
                }
                ErrorLog.exceptions.push(exceptionObject);
                //hide individual loader
                $scope.overviewReport5Activated = false;
                document.getElementById('LoaderReport5').style.display = "none";
            }
        }

        /*TOTAL CLAIMS WAITING TO BE BILLED VS CLAIMS BILLED*/

        $scope.totalClaimsWatitingToBeBilled_claimsBilled = function() {
            try {
                //hide canvas to display loader
                document.getElementById('canvas6').style.display = "none";
                document.getElementById('LoaderReport6').style.display = "block";
                $scope.overviewReport6Activated = true;
                filterCharts.isChart = true;
                $scope.vsoptions = appSettings.overviewClaimsDifferenceReport; //get Chart properties
                var claimsVsPromise = getOverviewData.getOverviewChartData('getTotalClaimsWaitingToBeBilledVsTotalClaimsBilled'); //get report heading
                claimsVsPromise.then(function(response) {

                        if (response.data.hasOwnProperty('errortag')) {
                            //if webservice encounters some error
                            document.getElementById('overviewReport6').style.display = "none";
                            document.getElementById('overviewReport6Nodatatext').innerHTML = response.data.errortag;
                            document.getElementById('overviewReport6ErrorDiv').style.display = "block";
                            $scope.overviewReport6 = "";
                            //hide individual Loader
                            $scope.overviewReport6Activated = false;
                            document.getElementById('LoaderReport6').style.display = "none";
                        } else {
                            if (document.getElementById('overviewReport6') != null && document.getElementById('overviewReport6') != undefined) { //a check to avoid style errors
                                if (response.data.Database_Error === "") {
                                    if (response.data != null && response.data != undefined && response.data != "" && response.data != "\n\n") {
                                        if (response.data.ChartType != "" && response.data.ChartType != null) { //if chart type not null
                                            document.getElementById('overviewReport6ErrorDiv').style.display = "none";
                                            document.getElementById('overviewReport6').style.display = "block";
                                            document.getElementById('canvas6').style.display = "block";

                                            //bind report title and chart data
                                            $scope.overviewReport6 = response.data.ReportTitle;
                                            $scope.claimswaiting_claimsbilledChartType = (response.data.ChartType).toLowerCase();
                                            $scope.claimswaiting_claimsbilledlabels = response.data.labels;
                                            $scope.claimswaiting_claimsbilledseries = response.data.series;
                                            $scope.claimswaiting_claimsbilleddata = response.data.data;
                                            //set x/y-axis headings
                                            $scope.vsoptions.scales.yAxes[0].scaleLabel.labelString = response.data.YAxisLabel;
                                            $scope.vsoptions.scales.xAxes[0].scaleLabel.labelString = response.data.XAxisLabel;
                                            //hide individual report loader
                                            $scope.overviewReport6Activated = false;
                                            document.getElementById('LoaderReport6').style.display = "none";
                                            //set currency format
                                            if (response.data.ToolTipFormat != "" && response.data.ToolTipFormat != null && response.data.ToolTipFormat != undefined) {
                                                var currency = response.data.ToolTipFormat;
                                                currency = getCurrencySign(currency);
                                            } else {
                                                var currency = "";
                                            }
                                            var n = 0; //to calculate mod for deciding whether to display number in decimal format or not

                                            if (currency == '%') { //if currency format is percent
                                                //y-axis scale callback function
                                                $scope.vsoptions.scales.yAxes[0].ticks.callback = function(value, index, values) {
                                                    if (parseInt(value) > 0) {
                                                        //if value positive
                                                        return currency + (value.toFixed(0)).toString().replace(/\B(?=(\d{2})+(?!\d))/g, ".");
                                                    } else {
                                                        return value.toFixed(0) + currency;
                                                    }
                                                }

                                                $scope.vsoptions.tooltips.callbacks = { //tooltip callback function
                                                    label: function(tooltipItem, data) {
                                                        var datasetLabel = data.datasets[tooltipItem.datasetIndex].label || ''; //get label of selected point
                                                        var allData = data.datasets[tooltipItem.datasetIndex].data; //get all the data plotted on current chart in array
                                                        var tooltipLabel = data.labels[tooltipItem.index]; //get legend name
                                                        return datasetLabel + ': ' + Number(tooltipItem.yLabel).toFixed(2) + currency;
                                                    }
                                                }

                                            } else { //if currency format is other than percent
                                                $scope.vsoptions.scales.yAxes[0].ticks.callback = function(value, index, values) {
                                                        if ((value % 1) != 0) { //calculating mod to decide whether to show perfect number or decimal
                                                            n = 2;
                                                        }
                                                        if (parseInt(value) > 0) { //if number is positive
                                                            return currency + (value.toFixed(n)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                                        } else if (parseInt(value) < 0) { //if number is negative
                                                            return '(' + currency + Math.abs(value.toFixed(n)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + ')';
                                                        } else {
                                                            return currency + value.toFixed(n);
                                                        }
                                                    }
                                                    //tooltips callback function
                                                $scope.vsoptions.tooltips.callbacks = {
                                                    label: function(tooltipItem, data) {
                                                        var datasetLabel = data.datasets[tooltipItem.datasetIndex].label || ''; //get label of selected point
                                                        var allData = data.datasets[tooltipItem.datasetIndex].data; //get all the data plotted on current chart in array
                                                        var tooltipLabel = data.labels[tooltipItem.index]; //get legend name
                                                        if (tooltipItem.yLabel > 0) { //if value positive
                                                            return datasetLabel + ': ' + currency + Number(tooltipItem.yLabel).toFixed(n).replace(/./g, function(c, i, a) {
                                                                return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "," + c : c;
                                                            });
                                                        } else {
                                                            if (tooltipItem.yLabel < 0) { //if value negative
                                                                return datasetLabel + ': (' + currency + Math.abs(tooltipItem.yLabel).toFixed(n).replace(/./g, function(c, i, a) {
                                                                    return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "," + c : c;
                                                                }) + ')';
                                                            }
                                                        }
                                                    }
                                                }

                                            }
                                            var cancelledLegend = []; //stores legends cancelled by user

                                            //Legned callback function
                                            $scope.vsoptions.legend.onClick = function(event, legendItem) {
                                                var index = legendItem.datasetIndex; //get index of cancelled legend
                                                var chart = this.chart; //all chart properties,data
                                                if (!cancelledLegend.includes(index)) { //push index of cancelled legend if doesn't already exist in array
                                                    cancelledLegend.push(index);

                                                } else {
                                                    var cancelledLegendIndex = cancelledLegend.indexOf(index);
                                                    if (cancelledLegendIndex >= 0) {
                                                        cancelledLegend.splice(cancelledLegendIndex, 1);
                                                    }

                                                }
                                                var totalLabels = (chart.legend.legendItems).length; //total no. of legends currently displayed on screen      

                                                if (cancelledLegend.length != totalLabels) {
                                                    //if no. of cancelled legends not equal total no. of legends than hide the selected legend else don't hide to avoid blank chart display
                                                    var meta = chart.getDatasetMeta(index);


                                                    meta.hidden = meta.hidden === null ? !chart.data.datasets[index].hidden : null;
                                                } else {
                                                    cancelledLegend.pop();
                                                }
                                                // We hid a dataset ... rerender the chart
                                                chart.update();
                                            }
                                            $scope.vsoptions.responsive = $scope.responsive; //if true chart will be responsive
                                            $scope.vsoptions.legend.display = $scope.showLegend; //if true legends will be displayed
                                            //hide individual Loader
                                            $scope.overviewReport6Activated = false;
                                            document.getElementById('LoaderReport6').style.display = "none";
                                        } else {
                                            //if chart type is null
                                            document.getElementById('overviewReport6').style.display = "none";
                                            document.getElementById('overviewReport6Nodatatext').innerHTML = "Something Went Wrong. Please Refresh!";
                                            document.getElementById('overviewReport6ErrorDiv').style.display = "block";
                                            //clear old data (following variables are used in html) 
                                            $scope.claimswaiting_claimsbilledlabels = "";
                                            $scope.claimswaiting_claimsbilledseries = "";
                                            $scope.claimswaiting_claimsbilleddata = "";
                                            $scope.overviewReport6 = "";
                                            //hide individual loader
                                            $scope.overviewReport6Activated = false;
                                            document.getElementById('LoaderReport6').style.display = "none";
                                        }

                                    } else { //if resonse is not null
                                        document.getElementById('overviewReport6').style.display = "none";
                                        document.getElementById('overviewReport6Nodatatext').innerHTML = "Something Went Wrong. Please Refresh!";
                                        document.getElementById('overviewReport6ErrorDiv').style.display = "block";
                                        //clear old data (following variables are used in html) 
                                        $scope.overviewReport6 = "";
                                        $scope.claimswaiting_claimsbilledlabels = "";
                                        $scope.claimswaiting_claimsbilledseries = "";
                                        $scope.claimswaiting_claimsbilleddata = "";
                                        //hide individual report loader
                                        $scope.overviewReport6Activated = false;
                                        document.getElementById('LoaderReport6').style.display = "none";
                                    }
                                } else { //if Database_Error column has some value
                                    document.getElementById('overviewReport6').style.display = "none";
                                    document.getElementById('overviewReport6Nodatatext').innerHTML = response.data.Database_Error;
                                    document.getElementById('overviewReport6ErrorDiv').style.display = "block";
                                    //clear old data (following variables are used in html) 
                                    $scope.claimswaiting_claimsbilledlabels = "";
                                    $scope.claimswaiting_claimsbilledseries = "";
                                    $scope.claimswaiting_claimsbilleddata = "";
                                    $scope.overviewReport6 = "";
                                    //hide individual report loader
                                    $scope.overviewReport6Activated = false;
                                    document.getElementById('LoaderReport6').style.display = "none";
                                } // if-else database error
                            } //if overview html available or not
                        } //else end

                    },
                    function errorCallback(response) {
                        //if http request encountered some error
                        var time = new Date();
                        if (response.statusText != undefined) {
                            var httpErrorObject = {

                                statusCode: response.status,
                                Message2: response.statusText,
                                time: time
                            }
                        } else {
                            var httpErrorObject = {
                                Message2: response,
                                time: time
                            }
                        }
                        //push error message in error object to display relative error message
                        ErrorLog.httpError.push(httpErrorObject);
                        //hide individual report loader
                        $scope.overviewReport6Activated = false;
                        document.getElementById('LoaderReport6').style.display = "none";
                    })
            } catch (exception) {
                var caughtException = exception;
                var exceptionObject = {
                    Error: caughtException,
                    time: new Date()
                }
                ErrorLog.exceptions.push(exceptionObject);
                //hide individual report loader
                $scope.overviewReport6Activated = false;
                document.getElementById('LoaderReport6').style.display = "none";
            }
        }


        //===========================================GO, CLEAR & DATERANGE ===========================================//

        //function called on click of go button 
        $scope.getValue = function() {
            showLoader();
            //get selected CLIENTS from client DROPDOWN

            $selectedClientIds = []; //store selected client's id
            $selectedClientNames = []; //store selected client's names (for displaying in pdf)

            try {
                angular.forEach($scope.dropdown.output_data, function(value, key) { //for each value in output_data

                    if ($selectedClientIds.length < $scope.dropdown.output_data.length) {
                        //if no. of values in selectedclients is less than no. of values in output _data then keep extracting data and push it in $selectedClientIds,$selectedClientNames
                        var ids = value.id;
                        $selectedClientIds.push(ids);
                        $selectedClientNames.push(value.text);

                    }
                });
                //store comma separated values in service parameters which are used for sending request
                chartFilterParameters.companyname = $selectedClientNames.join(",");
                chartFilterParameters.companyid = $selectedClientIds.join(",");

            } catch (exception) {

                console.log("clientdropdown " + exception);
            }

            //get selected option from AVG OR MAX DROPDOWN

            $selectedAvgOrMax = []; // to store id of average/max or all
            $selectedAvgOptionName = []; //to store name of selected option

            try {

                if ($scope.dropdown.selectedAvgMax != undefined) {

                    if ($scope.dropdown.selectedAvgMax.length == $scope.dropdown.avgMax[0].children.length) {
                        //no. of value in selecetedAvgMax is equal to no. of children all has then send -1 in request parameter
                        $selectedAvgOrMax.push('-1');

                    }
                }
            } catch (exception) {

                console.log(exception);
            }
            angular.forEach($scope.dropdown.selectedAvgMax, function(value, key) {
                //for each value in selectedAvgMax array if selectedAvgOrMax length is less than or eqal to length of selected options then extract data and push it in $selectedAvgOrMax, $selectedAvgOptionName arrays
                if ($selectedAvgOrMax.length <= $scope.dropdown.selectedAvgMax.length) {

                    var ids = value.id;
                    $selectedAvgOrMax.push(ids);
                    $selectedAvgOptionName.push(value.text);

                }
            });
            //store comma separated values in service parameters which are used for sending request
            chartFilterParameters.avgormaxname = $selectedAvgOptionName.join(",");
            chartFilterParameters.avgormax = $selectedAvgOrMax.join(",");

            // get selected payers from PAYER DROPDOWN
            $selectedPayerIds = []; //to store selected payer's id
            $selectedPayersName = []; //to store selected payer's name (for displaying in PDF)
            try {

                angular.forEach($scope.dropdown.selectedPayers, function(value, key) {
                    //for each value in selectedPayers, if no. of values in selectedPayerIds is less than values in selectedPayers then extract data and push it in $selectedPayerIds, $selectedPayersName arrays
                    if ($selectedPayerIds.length < $scope.dropdown.selectedPayers.length) {

                        var ids = value.id;
                        $selectedPayerIds.push(ids);
                        $selectedPayersName.push(value.text);

                    }
                });
            } catch (exception) {

                console.log("payerdropdown " + exception);
            }
            //store comma separated values in service parameters which are used for sending request
            chartFilterParameters.insurancename = $selectedPayersName.join(",");
            chartFilterParameters.insuranceid = $selectedPayerIds.join(",");

            //get selected option from NUMBER OF DAYS DROPDOWN
            $selectedDaysId = []; // to store selected option's id
            $selectedDaysValue = []; //to store selected option's name
            try {

                angular.forEach($scope.dropdown.selectedDays, function(value, key) {
                    //for each value in selectedDays, if no. of values in selectedDaysIds is less than values in selectedDays then extract data and push it in $selectedDaysId,  $selectedDaysValue arrays
                    if ($selectedDaysId.length < $scope.dropdown.selectedDays.length) {

                        var ids = value.id;
                        $selectedDaysId.push(ids);
                        $selectedDaysValue.push(value.text);

                    }
                });
            } catch (exception) {

                console.log("noofdaydropdoawn " + exception);
            }
            //store comma separated values in service parameters which are used for sending request
            chartFilterParameters.numberofdaysname = $selectedDaysValue.join(",");
            chartFilterParameters.numberofdays = $selectedDaysId.join(",");

            //call all functions to update data on charts/grid reports
            var currentpage = window.location.href; //get current page URL
            currentpage = currentpage.toLowerCase(); //convert value to lowercase for comparision

            if (currentpage.indexOf("overview") >= 0 && chartFilterParameters.reportid == 0) {
                //if current page URL is of overview page then call functions of all the reports of overview page
                $scope.plotOdometer();
                $scope.totalClaimsWaitingToBeBilled();
                $scope.medicareDays();
                $scope.totalHoursWorked();
                $scope.averageDaysToBill();
                $scope.totalClaimsWatitingToBeBilled_claimsBilled();
            } else { //if not overview page 
                //hide display of old data
                if (document.getElementById('displayChart') != undefined && document.getElementById('displayGrid') != undefined) {
                    document.getElementById('displayChart').style.display = "none";
                    document.getElementById('displayGrid').style.display = "none";
                }
                if (filterCharts.isChart == false) { //if is not chart report then call grid function
                    if (document.getElementById('GridErrorDiv') != undefined && document.getElementById('GridErrorDiv') != null) {
                        document.getElementById('GridErrorDiv').style.display = "none"; //hide error div
                    }
                    filterCharts.gridScope.getGridData(); //using gridscope because getGridData is in different controller

                } else {
                    //if its chart report then call chart function
                    if (document.getElementById('GridErrorDiv') != undefined && document.getElementById('GridErrorDiv') != null) {
                        document.getElementById('GridErrorDiv').style.display = "none"; //hide error div
                    }
                    $scope.plotLineChart(); // chart function

                }

            }


        }



        //=======New datepicker code===========//
        // specify default date range in controller
        $scope.date = {};
        $scope.date.dateRange = null; /*moment().startOf("today");*/
        $scope.today = new Date();

        //Select range options
        $scope.mycallback = "None";
        //whenever use selects an option from daterange picker, this funciton gets called or Done/Apply button is clicked
        $scope.dateRangeChanged = function(fromdate, todate) {

            chartFilterParameters.fromdate = fromdate;
            chartFilterParameters.todate = todate;
            // calling go button on date selection
            $scope.getValue();
        }

        //function called on click of clear button
        $scope.clearDropdowns = function() {
            showLoader();
            //setting all the variables to their default state
            chartFilterParameters.companyid = "";
            chartFilterParameters.fromdate = "";
            chartFilterParameters.todate = "";
            chartFilterParameters.avgormax = "",
                chartFilterParameters.avgormaxname = "",
                chartFilterParameters.numberofdays = "",
                chartFilterParameters.companyname = "",
                chartFilterParameters.insurancename = "",
                chartFilterParameters.noOfDaysValue = "",
                chartFilterParameters.insuranceid = "",
                filterCharts.selectedParentClientId = ""; //used in angular-multiselect.js as a flag for various conditions
            $scope.date.dateRange = null;
            $rootScope.$emit('ams_do_reset', { name: '*' }); //reset text of dropdwns
            var currentpage = window.location.href; //get current page location
            currentpage = currentpage.toLowerCase(); //convert text to lowercase for comparision
            $scope.getFilterData(); //getting values for client dropdown again
            $scope.getAverageDropdownData(); //getting values for average dropdown
            $scope.getPayers(); //getting values for payer dropdown
            $scope.getDays(); //getting values for days drodown
            if (currentpage.indexOf("overview") >= 0 && chartFilterParameters.reportid == 0) {
                //if current page URL is of overview page then call functions of all the reports of overview page
                $scope.plotOdometer();
                $scope.totalClaimsWaitingToBeBilled();
                $scope.medicareDays();
                $scope.totalHoursWorked();
                $scope.averageDaysToBill();
                $scope.totalClaimsWatitingToBeBilled_claimsBilled();
            } else {
                if (filterCharts.isChart == false) { //if report is not in chart view call grid function
                    filterCharts.gridScope.getGridData();
                    // $scope.plotLineChart();
                } else {
                    $scope.plotLineChart(); // if report is in chart  view call chart function

                }
            }
            DatePickerInitialize(''); //reinitialize datepicker
        }

        //when user clicks on chart icon following function is called
        $scope.toggleChartView = function() {
            showLoader();
            filterCharts.isChart = true; //setting chart variable to true
            showChart(); //calling showChart function written in commonFunction.js to show/hide div
            $scope.plotLineChart(); //call chart function to load on view
        }


        //Toggle buttons hover effect
        $scope.rollover = function(my_image) {
            if (my_image == 'chart') {
                document.getElementById('chartButton').style.backgroundImage = 'url("./images/icons/Chart_Click.png")';
                document.getElementById('gridButton').style.backgroundImage = 'url("./images/icons/Grid_Non.png")';
            } else {
                document.getElementById('gridButton').style.backgroundImage = 'url("./images/icons/Grid_Click.png")';
                document.getElementById('chartButton').style.backgroundImage = 'url("./images/icons/Chart_Non.png")';
            }
        }

        $scope.mouseaway = function(my_image) {
            if (filterCharts.isChart == true) {
                document.getElementById('chartButton').style.backgroundImage = 'url("./images/icons/Chart_Click.png")';
                document.getElementById('gridButton').style.backgroundImage = 'url("./images/icons/Grid_Non.png")';
            } else {
                document.getElementById('gridButton').style.backgroundImage = 'url("./images/icons/Grid_Click.png")';
                document.getElementById('chartButton').style.backgroundImage = 'url("./images/icons/Chart_Non.png")';
            }

        }

        //this function is called when user clicks on cancel loader
        $scope.chartRequestCanceller = function() {
            filterCharts.isRequestCancelled = true; //setting this to true to indicate request needs to be cancelled (required for cancelling grid request)
            var currentpage = window.location.href; //get current page url
            currentpage = currentpage.toLowerCase(); //conver value to lower case
            if (currentpage.indexOf("overview") >= 0 && chartFilterParameters.reportid == 0) {
                //if current page is overview than simply hide the loader
                hideLoader();
            } else {
                cancel(); //calling cancel request sub function defined on top for chart report
                filterCharts.gridControllerScope.gridRequestCanceller(); //calling cancel function for grid report
                hideLoader();
            }
        }

        $scope.disableClientDropdownClick = function() {
            if (filterCharts.disableClientDropdownClick) {
                $rootScope.$emit('ams_close', { name: '*' });
            }
        }



    }]) //end of controller
