//=============GRID CONTROLLER============//
try {
    app.controller('FilterController', ['$scope', '$rootScope', '$http', 'isMobileFilter', 'uiGridConstants', 'filterCharts', 'chartFilterParameters', 'httpService', '$state', '$timeout', 'uiGridPinningConstants', '$q', 'getReportHeading', function($scope, $rootScope, $http, isMobileFilter, uiGridConstants, filterCharts, chartFilterParameters, httpService, $state, $timeout, uiGridPinningConstants, $q, getReportHeading) {
        filterCharts.gridControllerScope = $scope; //this controller's scope so that cancel request function can be called from another controller
        filterCharts.canceler = $q.defer(); //initializing cancel request object
        var resolved = false; //variable used for cancelling grid request
        var cancel = function() { //sub function for cancelling grid request
            try {
                filterCharts.canceler.resolve("http call aborted");
            } catch (exception) {
                console.log(exception);
            }
        };


        //==========================BIND GRID REPORTS==============================//

        //Grid variable declaration
        $rootScope.gridApi = { api: "" }; //to store library api functions of ui-grid
        //to set grid properties you use $scope.gridOptions object      
        $scope.gridOptions = {
            enablePinning: true, //setting property to freeze column to true            
            enableFiltering: false, //setting filter property to false
            enableSorting: false, //setting sorting property to false
            enableVerticalScrollbar: 2, //enable vertical scrolling only if needed
            enableHorizontalScrollbar: 2, //enable horizontal scrolling only if needed
            enableColumnResizing: true, //allow column resizing
            enableColumnMenus: false, // disable column menus
            enableGridMenu: false, //disable grid menu
            onRegisterApi: function(gridApi) { //api function to customize grid using library functions
                $rootScope.gridApi.api = gridApi; //store library api functions of ui-grid
                //set column width
                gridApi.core.registerColumnsProcessor(hideIdColumn, 0);
                //for setting default column width when no. of columns are more
                function hideIdColumn(columns) {
                    columns.forEach(function(column) {
                        if (columns.length > 6) {
                            column.width = '16%';
                        }
                    });
                    return columns;
                }
            }
        };

        //Function for binding grid report data
        $scope.getGridData = function() {
            CheckLogin();
            chartFilterParameters.reporttype = 0; //setting reportype to grid for sending parameter in httprequest
            filterCharts.gridScope = $scope; //storing scope of this particular function
            filterCharts.isChart = false; //setting chart view to false

            if (resolved) { //if resolved true then call cancel function to cancel grid http request
                cancel();
            }
            filterCharts.canceler = $q.defer(); //reinitialize the request canceller object so that after cancel, new request can be made.
            resolved = true;
            //clear old data 
            filterCharts.scope.ReportTitle = "";
            filterCharts.scope.DateRange = "";
            
            //Get report Heading
            var promise = getReportHeading.getReport();
            promise.then(function(response) {
              
                if (response.data != null && response.data != undefined && response.data != "\n\n" && response.data != '') {
                    //if received response is not null/undefined/blank then Storing response received from report heading request in different variables

                    filterCharts.scope.ReportTitle = response.data.Table[0].ReportTitle;
                    filterCharts.scope.DateRange = '(' + response.data.Table[0].DateRange + ')';
                } else {
                    //if webservice returned null data
                    console.log("Report heading webservice returned null data");
                }

                //Make HTTP request to get grid report data
                httpService.async(filterCharts.canceler).then(function(response) {
                        try {
                            if (response != undefined) { // if response is undefined
                                if (response.data.hasOwnProperty('errortag')) {
                                    //if webservice encouters some error while fetching data from db show error tag hide grid tag
                                    document.getElementById('displayGrid').style.display = "none";
                                    document.getElementById('GridErrorDiv').style.display = "block";
                                    document.getElementById('GridErrorDiv').innerHTML = response.data.errortag;
                                    hideLoader();
                                } else {
                                    if (response.data.Table[0].Database_Error.trim() === "") {
                                        // if response has database error column and it is blank
                                        if (response.data.Table.length != 0 && response.data != '\n\n' && response.data != null && response.data != undefined) {
                                            // if response is not null hide error div and show grid
                                            document.getElementById('GridErrorDiv').style.display = "none";
                                            document.getElementById('displayChart').style.display = "none";
                                            document.getElementById('displayGrid').style.display = "block"
                                                //store received response to bind grid
                                            $rootScope.gridApi.api.grid.options.data = response.data.Table;
                                            //get columns and store them in following array so that data gets updated on new requests 
                                            var col = [];
                                            angular.forEach($rootScope.gridApi.api.grid.options.data[0], function(value, key) {
                                                    col.push({ name: key });
                                                })
                                                //creating new object of column definitions on each request 
                                            $rootScope.gridApi.api.grid.options.columnDefs = new Array();
                                            //assigning columns to this definition property 
                                            $rootScope.gridApi.api.grid.options.columnDefs = col;

                                            for (var i = 0; i < ($rootScope.gridApi.api.grid.options.columnDefs).length; i++) {
                                                //Hiding database error column
                                                if ($rootScope.gridApi.api.grid.options.columnDefs[i].name == 'Database_Error') {
                                                    $rootScope.gridApi.api.grid.options.columnDefs[i].visible = false
                                                } else {

                                                    //Setting text alignement to right if number/amount , to center if '-', to left if text
                                                    $rootScope.gridApi.api.grid.options.columnDefs[i].cellClass = function(grid, row, col, rowRenderIndex, colRenderIndex) {
                                                        var number = grid.getCellValue(row, col); //get calue of a cell

                                                        try {
                                                            //check if number is in string fomat
                                                            if (typeof(number) == 'string') {
                                                                //if string than remove comma and check that value is a number or not
                                                                var number = number.replace(',', '');
                                                                if (isNaN(Number(number)) == true && number.indexOf('$') == -1) { //if is not a number and is not amount then 
                                                                    if (number == '-') {
                                                                        //return 'alignColumnCenter'; //set text alignment to center
                                                                        return 'alignColumn';
                                                                    } else {
                                                                        return 'alignColumnLeft'; //else set text alignment to left
                                                                    }
                                                                } else {
                                                                    if (col.displayName == "Name") {
                                                                        return 'alignColumnLeft';
                                                                    } else {
                                                                        return 'alignColumn'; //if number or amount set alignment to right
                                                                    }

                                                                }

                                                            } else {
                                                                if (isNaN(number) != true) { // if not a string and is a number then set alignment to right

                                                                    return 'alignColumn';
                                                                }
                                                            }
                                                        } catch (exception) {
                                                            console.log("nan " + exception);

                                                        }

                                                    }

                                                }
                                            }

                                            $rootScope.gridApi.api.grid.refresh(); //refresh grid data


                                            $timeout(function() {
                                                //Pinning(freezing) no. of columns from left
                                                for (var i = 0; i < filterCharts.freezeColumnCount; i++) {
                                                    var col = $rootScope.gridApi.api.grid.columns[i]; //get ith column
                                                    var container = uiGridPinningConstants.container;
                                                    $rootScope.gridApi.api.pinning.pinColumn(col, container.LEFT); //pin column to left
                                                    $rootScope.gridApi.api.core.notifyDataChange(uiGridConstants.dataChange.COLUMN); //notify data change
                                                }
                                                if (navigator.appName == 'Microsoft Internet Explorer' || !!(navigator.userAgent.match(/Trident/) || navigator.userAgent.match(/rv:11/)) || (typeof $.browser !== "undefined" && $.browser.msie == 1)) {
                                                    //if IE
                                                    $rootScope.gridApi.api.core.handleWindowResize();
                                                } else {

                                                    window.dispatchEvent(new Event('resize')); //resize grid
                                                }


                                            }, 10);

                                            hideLoader();
                                        } else {
                                            //if response is null
                                            document.getElementById('displayGrid').style.display = "none";
                                            document.getElementById('GridErrorDiv').style.display = "block";
                                            if (response.data.Table.length === 0) {
                                                //if response length 0
                                                document.getElementById('Nodatatext').innerHTML = "No Data Returned";
                                                hideLoader();
                                            } else {
                                                //if response null/undefined
                                                document.getElementById('Nodatatext').innerHTML = "Something went wrong.Please refresh!";
                                                hideLoader();
                                            }
                                        }
                                    } else {
                                        //if Database_Error column is not empty
                                        document.getElementById('displayGrid').style.display = "none";
                                        document.getElementById('GridErrorDiv').style.display = "block";
                                        document.getElementById('Nodatatext').innerHTML = response.data.Table[0].Database_Error;
                                        hideLoader();
                                    }
                                }
                            } else {
                                if (filterCharts.isRequestCancelled == true) {
                                    //if request was cancelled from loader click display message else display nothing
                                    document.getElementById('displayGrid').style.display = "none";
                                    document.getElementById('Nodatatext').innerHTML = "Loading Cancelled..";
                                    document.getElementById('GridErrorDiv').style.display = "block";
                                    hideLoader();
                                } else {
                                    document.getElementById('displayGrid').style.display = "none";
                                    document.getElementById('displayChart').style.display = "none";
                                    document.getElementById('GridErrorDiv').style.display = "none";
                                    showLoader();
                                }
                            }
                        } catch (exception) {
                            console.log("ExceptionGrid: " + exception);
                            hideLoader();
                        }

                        hideLoader();

                    },
                    function errorCallback(response) {
                        //if HTTP request encountered some error
                        var time = new Date();
                        if (response.statusText != undefined) {
                            var httpErrorObject = {

                                statusCode: response.status,
                                Message2: response.statusText,
                                time: time
                            }
                        } else {
                            var httpErrorObject = {
                                Message2: response,
                                time: time
                            }
                        }
                        //push error message in error object
                        ErrorLog.httpError.push(httpErrorObject);
                        hideLoader();
                    })
            }, function errorCallback(response) {
                //if HTTP request encountered some error
                var time = new Date();
                if (response.statusText != undefined) {
                    var httpErrorObject = {

                        statusCode: response.status,
                        Message2: response.statusText,
                        time: time
                    }
                } else {
                    var httpErrorObject = {
                        Message2: response,
                        time: time
                    }
                }
                //push error message in error object
                ErrorLog.httpError.push(httpErrorObject);
                hideLoader();
            })
        }

        //when user clicks grid icon then disable chart view and enable grid view
        $scope.toggleGridView = function() {
            showLoader();
            filterCharts.isChart = false;
            showGrid(); // function to disable / enable grid view written in common function.js
            filterCharts.scope.getValue();
        }

        //This function gets called from chartController when user clicks on cancel icon from loader
        $scope.gridRequestCanceller = function() {
            if (filterCharts.isChart == false) {
                //if grid report then 
                cancel(); //call function to cancel ongoing request               
                hideLoader();
            }
        }

    }]);
} catch (exception) {
    var caughtException = exception;
    var exceptionObject = {
            Error: caughtException,
            time: new Date()
        }
        //pushing exception in exception object to display relative error message to user.
    ErrorLog.exceptions.push(exceptionObject);
    hideLoader();
}

//=============================================EXPORTING CONTROLLER===============================================//

try {
    app.controller('ExportControl', ['$scope', '$http', 'chartFilterParameters', 'appSettings', 'filterCharts', '$filter', function($scope, $http, chartFilterParameters, appSettings, filterCharts, $filter) {

        //PDF EXPORT FUNCTION

        //base 641 functionss
        $scope.convertImgToBase641 = function(callback, outputFormat) {
            var canvas = document.createElement('CANVAS');
            //next 4 lines to avoid black shadow on pdf if some window is open on browser
            var ctx = canvas.getContext('2d');
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            ctx.fillStyle = "#FFFFFF";
            ctx.fillRect(0, 0, canvas.width, canvas.height);
            var img = new Image;
            img.crossOrigin = 'anonymous';

            img.onload = function() {
                canvas.height = img.height;
                canvas.width = img.width;
                ctx.drawImage(img, 0, 0);



                var imgData = ctx.getImageData(0, 0, canvas.width, canvas.height);
                var data = imgData.data;
                for (var i = 0; i < data.length; i += 4) {
                    if (data[i + 3] < 255) {
                        data[i] = 255;
                        data[i + 1] = 255;
                        data[i + 2] = 255;
                        data[i + 3] = 255;
                    }
                }
                ctx.putImageData(imgData, 0, 0);

                var dataURL = canvas.toDataURL(outputFormat || 'image/png');


                callback.call(this, dataURL);



                // Clean up
                canvas = null;
            };

            img.src = "./images/icons/GlobalITP_BrandingLogo.png";
        }

        //BASE64 function
        $scope.convertImgToBase64 = function(callback, outputFormat) {

            //next 4 lines to avoid black shadow on pdf if some window is open on browser
            var canvas = document.createElement('CANVAS');
            var ctx = canvas.getContext('2d');
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            ctx.fillStyle = "#FFFFFF";
            ctx.fillRect(0, 0, canvas.width, canvas.height);
            var img = new Image;
            img.crossOrigin = 'anonymous';




            img.onload = function() {
                canvas.height = img.height;
                canvas.width = img.width;

                ctx.drawImage(img, 0, 0);



                var imgData = ctx.getImageData(0, 0, canvas.width, canvas.height);
                var data = imgData.data;
                for (var i = 0; i < data.length; i += 4) {
                    if (data[i + 3] < 255) {
                        data[i] = 255;
                        data[i + 1] = 255;
                        data[i + 2] = 255;
                        data[i + 3] = 255;
                    }
                }
                ctx.putImageData(imgData, 0, 0);

                var dataURL = canvas.toDataURL(outputFormat || 'image/png');


                callback.call(this, dataURL);


                // Clean up
                canvas = null;
            };

            img.src = "./images/icons/Revenue Guard_476x80.png";
        }
        var totalSvg;
        var svgdataarray = [];
        var count = 0;


        $scope.printPDFCustom = function(base64img, base64img1) {
            var canvas = document.getElementsByTagName("canvas");

            totalSvg = canvas.length;

            html2canvas(canvas[count], {
                onrendered: function(canvas1) {
                    svgdataarray.push(canvas1.toDataURL('image/jpeg'));
                    count++;
                    if (count < totalSvg)
                        $scope.printPDFCustom(base64img, base64img1);
                    else
                        $scope.CreatePDF(base64img, base64img1);
                }
            });




        }



        $scope.CreatePDF = function(base64img, base64img1) {
            //Getting selected dropdown/datepicker parameters ,to display them in pdf
            var companyname = chartFilterParameters.companyname; //Selected client's name
            var companyid = chartFilterParameters.companyid; //Selected client's id
            var fromdate = chartFilterParameters.fromdate; //Seleted fromdate
            var todate = chartFilterParameters.todate; //Selected todate
            var reportid = chartFilterParameters.reportid; //Selected report's id
            var userid = chartFilterParameters.userid; //logged in user's id
            var avgormax = chartFilterParameters.avgormax; //select avg or max option id
            var avgormaxname = chartFilterParameters.avgormaxname; // selected avg or max option name
            var insuranceid = chartFilterParameters.insuranceid; //selected Payers id
            var insurancename = chartFilterParameters.insurancename; //selected payers name
            var numberofdays = chartFilterParameters.numberofdays; //selected number of days option's id
            var numberofdaysname = chartFilterParameters.numberofdaysname; //selected number of days option's name
            var CompanyidDisplayName = chartFilterParameters.CompanyidDisplayName; // Name to display for cleint dropdown
            var DateDisplayName = chartFilterParameters.DateDisplayName; //Name to display for datepicker
            var InsuranceidDisplayName = chartFilterParameters.InsuranceidDisplayName; //Name to display for payer dropdown
            var NumberOfDaysDisplayName = chartFilterParameters.NumberOfDaysDisplayName; //Name to display for # of days dropdown
            var AvgormaxDisplayName = chartFilterParameters.AvgormaxDisplayName; //Name to display for Average/max dropdown
            var doc = new jsPDF('landscape'); //PDF mode            
            var dataimg = '';
            document.getElementById('div_tbll').innerHTML = ""; //html element on each report page where this pdf will getdownloaded
            var tbll = document.createElement("table");
            //setting style for pdf
            tbll.style.cssText = 'background-color:white;width:100%;font-size: 9px;color: #676565;';
            tbll.setAttribute("cellpadding", "0");
            tbll.setAttribute("cellspacing", "0");

            //PDF if else according to dropdown (filter) 
            var filterHTML = '';
            if (companyid != "Not Available") { //if client dropdown available
                var clientInnerText = document.getElementById('clientFilter').children[0].children[0].innerText; //client filter box html innertext
                clientInnerText = clientInnerText.trim();
                if (companyid === "" || clientInnerText === 'ALL CLIENTS') {
                    companyname = "All Clients"; //if selected client id is -2 or blank
                } //else display following html
                filterHTML = filterHTML + ' <tr><td style="width:15%;border:1px solid #d4d0d0;font-size: 9px;padding-left: 7px;">' + CompanyidDisplayName + '</td>' + '<td style="width:85%;border:1px solid #d4d0d0;padding-left: 7px;border-left: none;"><span id="date_range">' + companyname + '</span></td></tr>';
            }

            //Insurance id for reportid 4 and 5 , if payer dropdown available
            if (insuranceid != "Not Available" && (reportid == 4 || reportid == 5)) {
                var payerInnerText = document.getElementById('payerFilter').children[0].children[0].innerText; //payer filter box html innertext
                payerInnerText = payerInnerText.trim();
                if (insuranceid == "" || payerInnerText === 'ALL PAYERS') {
                    insurancename = "All Payers";
                } //else display following html
                filterHTML = filterHTML + ' <tr><td style="width:15%;border:1px solid #d4d0d0;font-size: 9px;padding-left: 7px;">' + InsuranceidDisplayName + '</td>' + '<td style="width:85%;border:1px solid #d4d0d0;padding-left: 7px;border-left: none;"><span id="date_range">' + insurancename + '</span></td></tr>';
            }

            //Number of days dropdown for report 4 and 5
            if (numberofdays != "Not Available" && (reportid == 4 || reportid == 5)) { // if # of days dropdown available
                if (numberofdaysname == "") { //if selected no. of days id is blank
                    numberofdaysname = "Over 90";
                }
                filterHTML = filterHTML + ' <tr><td style="width:15%;border:1px solid #d4d0d0;font-size: 9px;padding-left: 7px;">' + NumberOfDaysDisplayName + '</td>' + '<td style="width:85%;border:1px solid #d4d0d0;padding-left: 7px;border-left: none;"><span id="date_range">' + numberofdaysname + '</span></td></tr>';
            } // else display following html

            //Average/Maximum dropdown
            if (avgormax != "Not Available" && reportid == 3) {
                if (avgormax == "") { //if selected option id is blank
                    avgormaxname = "Both";
                } //else display following html
                filterHTML = filterHTML + ' <tr><td style="width:15%;border:1px solid #d4d0d0;font-size: 9px;padding-left: 7px;">' + AvgormaxDisplayName + '</td>' + '<td style="width:85%;border:1px solid #d4d0d0;padding-left: 7px;border-left: none;"><span id="date_range">' + avgormaxname + '</span></td></tr>';
            }
            if (chartFilterParameters.fromdate != null && chartFilterParameters.todate != null) {
                //if fromdate and todate both are not null
                if (fromdate == "" && todate == "") {
                    //if fromdate and todate are blank then display dates received from report heading
                    filterHTML = filterHTML + ' <tr><td style="width:15%;border:1px solid #d4d0d0;font-size: 9px;padding-left: 7px;">' + DateDisplayName + '</td>' + '<td style="width:85%;border:1px solid #d4d0d0;padding-left: 7px;border-left: none;"><span id="date_range">(' + filterCharts.dateRange + ')</span></td></tr>';
                } else {
                    //else displaly selected from date and to date
                    filterHTML = filterHTML + ' <tr><td style="width:15%;border:1px solid #d4d0d0;font-size: 9px;padding-left: 7px;">' + DateDisplayName + '</td>' + '<td style="width:85%;border:1px solid #d4d0d0;padding-left: 7px;border-left: none;"><span id="date_range">(' + fromdate + " - " + todate + ')</span></td></tr>';
                }
            }

            //if fromdate is blank and todate is not blank
            if (chartFilterParameters.fromdate == "" && chartFilterParameters.todate != "") {
                filterHTML = filterHTML + ' <tr><td style="width:15%;border:1px solid #d4d0d0;font-size: 9px;padding-left: 7px;">' + "As of Date" + '</td>' + '<td style="width:85%;border:1px solid #d4d0d0;padding-left: 7px;border-left: none;"><span id="date_range">(' + todate + ')</span></td></tr>';
            }
            /*filterHTML = ' <tr><td style="width:15%;border:1px solid #d4d0d0;font-size: 9px;padding-left: 7px;">Date Range</td>' + '<td style="width:85%;border:1px solid #d4d0d0;padding-left: 7px;border-left: none;"><span id="date_range">12/25/2016 - 12/27/2016</span></td></tr>' + ' <tr><td style="width:15%;border:1px solid #d4d0d0;font-size: 9px;padding-left: 7px;">Client Name</td>' + '<td style="width:85%;border:1px solid #d4d0d0;padding-left: 7px;border-left: none;"><span id="client_name">Company1,Company2,Company3</span></td></tr>' + ' <tr><td style="width:15%;border:1px solid #d4d0d0;font-size: 9px;padding-left: 7px;">Insurance Name</td>' + '<td style="width:85%;border:1px solid #d4d0d0;padding-left: 7px;border-left: none;"><span id="insurance_name">Insurance1,Insurance2,Insurance3</span></td></tr>';*/

            tbll.innerHTML = filterHTML; //assigninig filterhmtl set in above conditions to innerhtml to create pdf

            document.getElementById('div_tbll').appendChild(tbll);
            var canvas = document.getElementsByTagName("canvas");

            document.getElementById('div_tbll').style.display = 'block';
            var dataimg2 = '';
            html2canvas([document.getElementById('div_tbll')], {
                onrendered: function(canvas2) {
                    dataimg2 = canvas2.toDataURL('image/jpeg');

                    for (var i = 0; i < canvas.length; i++) {
                        try {
                            canvas[count].style.border = '1px solid #fff';
                            canvas[count].style.backgroundColor = '#fff';
                            html2canvas(canvas[count], {
                                onrendered: function(canvas1) {
                                    canvas1.style.border = '1px solid #fff';
                                    canvas1.style.backgroundColor = '#fff';
                                    dataimg = canvas1.toDataURL('image/jpeg');
                                    doc.setFontSize(13);
                                    doc.setTextColor(80);
                                    doc.setFontType("bold");
                                    doc.addImage(base64img, 'JPEG', 10, 5, 60, 15);
                                    doc.setLineWidth(0.1);
                                    doc.line(10, 25, 280, 25);
                                    if (i == 0 || i == 1)
                                        doc.addImage(dataimg2, 'JPEG', 10, 40, 0, 0);
                                    doc.setTextColor(9, 103, 123);
                                    doc.setFontSize(15);
                                    doc.text(10, 35, document.getElementById("reportHeader").children[0].children[0].innerText);
                                    doc.setFontSize(12);
                                    doc.setFontSize(7);
                                    doc.setTextColor(120);
                                    doc.line(10, 25, 280, 25);


                                    if (i == 0 || i == 1)
                                        doc.addImage(dataimg, 'JPEG', 20, 80, 250, 90);
                                    else
                                        doc.addImage(dataimg, 'JPEG', 20, 60, 250, 90);

                                    doc.setDrawColor(255);
                                    doc.setLineWidth(0.5);
                                    doc.line(270, 80, 270, 170);
                                    doc.line(270, 80, 270, 170);
                                    doc.setDrawColor(255);
                                    doc.setLineWidth(0.5);
                                    doc.line(20, 80, 270, 80);
                                    doc.line(10, 170, 280, 170);



                                    doc.setDrawColor(120);


                                    doc.setFontSize(12);
                                    doc.setFontSize(7);
                                    doc.setTextColor(120);

                                    doc.line(10, 195, 280, 195);
                                    doc.setFontSize(11);
                                    doc.setTextColor(100);
                                    doc.setFontType("normal");
                                    doc.text(10, 203, document.getElementById("leftFooterSpan").innerText);


                                    doc.setFontSize(11);
                                    doc.setTextColor(100);
                                    doc.setFontType("normal");
                                    doc.text(240, 203, document.getElementById("rightFooterSpan").innerText);



                                    if (i < canvas.length - 1)
                                        doc.addPage();
                                    if (i == (canvas.length)) {
                                        $scope.saveDoc(doc);
                                    }

                                }
                            });
                        } catch (e) {
                            console.log(e.toString());
                        }
                    }
                }
            });

            document.getElementById('div_tbll').style.display = 'none'; //hiding pdf tag from report display

            svgdataarray.splice(0, svgdataarray.length);
            svgdataarray = [];

        }

        $scope.saveDoc = function(doc) { //save pdf on system 
            var out = doc.output('save', document.getElementById("reportHeader").children[0].innerText + '.pdf');
        }

        $scope.printPdf = function() { //make pdf
            document.getElementById('iframeExport').src = "";
            $scope.convertImgToBase641(function(base64img1) { $scope.convertImgToBase64(function(base64img) { $scope.CreatePDF(base64img, base64img1) }, "image/jpeg") }, "image/jpeg");
            return false;
        }


        //EXCEL EXPORT FUNCTION

        $scope.printExcel = function() {
            //Getting selected dropdown/datepicker parameters ,to display them in pdf
            var Servicetype = chartFilterParameters.ServiceType; //selected report's service type
            var filename = document.getElementById("reportHeader").children[0].children[0].innerText; //name to be given to excel file
            var companyid = chartFilterParameters.companyid; //selected client id
            var fromdate = chartFilterParameters.fromdate; //selected from date
            var todate = chartFilterParameters.todate; //selected to date
            var reportid = chartFilterParameters.reportid; //selected report's id
            var userid = chartFilterParameters.userid; //logged in user's id
            var avgormax = chartFilterParameters.avgormax; //selected avg/max option's id
            var insuranceid = chartFilterParameters.insuranceid; //selected payer's id
            var numberofdays = chartFilterParameters.numberofdays; // selected # of days id
            var CompanyidDisplayName = chartFilterParameters.CompanyidDisplayName; // Name to display for client dropdown
            var DateDisplayName = chartFilterParameters.DateDisplayName; //Name to display for DatePicker dropdown
            var InsuranceidDisplayName = chartFilterParameters.InsuranceidDisplayName; //Name to display for Payer dropdown
            var NumberOfDaysDisplayName = chartFilterParameters.NumberOfDaysDisplayName; //Name to display for # of days dropdown
            var AvgormaxDisplayName = chartFilterParameters.AvgormaxDisplayName; //Name to display for average/max dropdwn
            if (fromdate == "" && todate == "") {
                //If fromdate and todate are null then display dates from daterange received in report heading   
                var daterange = chartFilterParameters.dateRange;
                if (reportid != 4) {
                    daterange = daterange.split(" - ");
                    fromdate = daterange[0];
                    todate = daterange[1];
                    fromdate = $filter('date')(new Date(fromdate), "MM/dd/yyyy");
                    todate = $filter('date')(new Date(todate), "MM/dd/yyyy");
                    document.getElementById('iframeExport').src = appSettings.excelExportUrl + "Servicetype=" + Servicetype + "&FileName=" + filename + "&companyid=" + companyid + "&reporttype=0&fromdate=" + fromdate + "&todate=" + todate + "&reportid=" + reportid + "&userid=" + userid + "&avgormax=" + avgormax + "&insuranceid=" + insuranceid + "&numberofdays=" + numberofdays + "&CompanyidDisplayName=" + CompanyidDisplayName + "&DateDisplayName=" + DateDisplayName + "&InsuranceidDisplayName=" + InsuranceidDisplayName + "&NumberOfDaysDisplayName=" + NumberOfDaysDisplayName + "&AvgormaxDisplayName=" + AvgormaxDisplayName;
                } else {
                    //if report id is 4 convert As of date received in daterange to  MM/dd/yyyy format 
                    var convertDate = (daterange.toLowerCase()).replace('as of ', '');
                    convertDate = new Date(convertDate);
                    fromdate = $filter('date')(convertDate, "MM/dd/yyyy");
                    todate = $filter('date')(convertDate, "MM/dd/yyyy");
                    document.getElementById('iframeExport').src = appSettings.excelExportUrl + "Servicetype=" + Servicetype + "&FileName=" + filename + "&companyid=" + companyid + "&reporttype=0&fromdate=" + fromdate + "&todate=" + todate + "&reportid=" + reportid + "&userid=" + userid + "&avgormax=" + avgormax + "&insuranceid=" + insuranceid + "&numberofdays=" + numberofdays + "&CompanyidDisplayName=" + CompanyidDisplayName + "&DateDisplayName=" + DateDisplayName + "&InsuranceidDisplayName=" + InsuranceidDisplayName + "&NumberOfDaysDisplayName=" + NumberOfDaysDisplayName + "&AvgormaxDisplayName=" + AvgormaxDisplayName;
                }
            } else {
                //if dates are not null then 
                document.getElementById('iframeExport').src = appSettings.excelExportUrl + "Servicetype=" + Servicetype + "&FileName=" + filename + "&companyid=" + companyid + "&reporttype=0&fromdate=" + fromdate + "&todate=" + todate + "&reportid=" + reportid + "&userid=" + userid + "&avgormax=" + avgormax + "&insuranceid=" + insuranceid + "&numberofdays=" + numberofdays + "&CompanyidDisplayName=" + CompanyidDisplayName + "&DateDisplayName=" + DateDisplayName + "&InsuranceidDisplayName=" + InsuranceidDisplayName + "&NumberOfDaysDisplayName=" + NumberOfDaysDisplayName + "&AvgormaxDisplayName=" + AvgormaxDisplayName;
            }
        }
    }]);

} catch (exception) {
    console.log("Exception: " + exception);
}


//=============================================EXPORTING CONTROLLER===============================================//

try {
    app.controller('ExportControl', ['$scope', '$http', 'chartFilterParameters', 'appSettings', 'filterCharts', '$filter', function($scope, $http, chartFilterParameters, appSettings, filterCharts, $filter) {

        //PDF EXPORT FUNCTION

        //base 641 functionss
        $scope.convertImgToBase641 = function(callback, outputFormat) {
            var canvas = document.createElement('CANVAS');
            //next 4 lines to avoid black shadow on pdf if some window is open on browser
            var ctx = canvas.getContext('2d');
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            ctx.fillStyle = "#FFFFFF";
            ctx.fillRect(0, 0, canvas.width, canvas.height);
            var img = new Image;
            img.crossOrigin = 'anonymous';

            img.onload = function() {
                canvas.height = img.height;
                canvas.width = img.width;
                ctx.drawImage(img, 0, 0);



                var imgData = ctx.getImageData(0, 0, canvas.width, canvas.height);
                var data = imgData.data;
                for (var i = 0; i < data.length; i += 4) {
                    if (data[i + 3] < 255) {
                        data[i] = 255;
                        data[i + 1] = 255;
                        data[i + 2] = 255;
                        data[i + 3] = 255;
                    }
                }
                ctx.putImageData(imgData, 0, 0);

                var dataURL = canvas.toDataURL(outputFormat || 'image/png');


                callback.call(this, dataURL);



                // Clean up
                canvas = null;
            };

            img.src = "./images/icons/GlobalITP_BrandingLogo.png";
        }

        //BASE64 function
        $scope.convertImgToBase64 = function(callback, outputFormat) {

            //next 4 lines to avoid black shadow on pdf if some window is open on browser
            var canvas = document.createElement('CANVAS');
            var ctx = canvas.getContext('2d');
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            ctx.fillStyle = "#FFFFFF";
            ctx.fillRect(0, 0, canvas.width, canvas.height);
            var img = new Image;
            img.crossOrigin = 'anonymous';




            img.onload = function() {
                canvas.height = img.height;
                canvas.width = img.width;

                ctx.drawImage(img, 0, 0);



                var imgData = ctx.getImageData(0, 0, canvas.width, canvas.height);
                var data = imgData.data;
                for (var i = 0; i < data.length; i += 4) {
                    if (data[i + 3] < 255) {
                        data[i] = 255;
                        data[i + 1] = 255;
                        data[i + 2] = 255;
                        data[i + 3] = 255;
                    }
                }
                ctx.putImageData(imgData, 0, 0);

                var dataURL = canvas.toDataURL(outputFormat || 'image/png');


                callback.call(this, dataURL);


                // Clean up
                canvas = null;
            };

            img.src = "./images/icons/Revenue Guard_476x80.png";
        }
        var totalSvg;
        var svgdataarray = [];
        var count = 0;


        $scope.printPDFCustom = function(base64img, base64img1) {
            var canvas = document.getElementsByTagName("canvas");

            totalSvg = canvas.length;

            html2canvas(canvas[count], {
                onrendered: function(canvas1) {
                    svgdataarray.push(canvas1.toDataURL('image/jpeg'));
                    count++;
                    if (count < totalSvg)
                        $scope.printPDFCustom(base64img, base64img1);
                    else
                        $scope.CreatePDF(base64img, base64img1);
                }
            });




        }



        $scope.CreatePDF = function(base64img, base64img1) {
            //Getting selected dropdown/datepicker parameters ,to display them in pdf
            var companyname = chartFilterParameters.companyname; //Selected client's name
            var companyid = chartFilterParameters.companyid; //Selected client's id
            var fromdate = chartFilterParameters.fromdate; //Seleted fromdate
            var todate = chartFilterParameters.todate; //Selected todate
            var reportid = chartFilterParameters.reportid; //Selected report's id
            var userid = chartFilterParameters.userid; //logged in user's id
            var avgormax = chartFilterParameters.avgormax; //select avg or max option id
            var avgormaxname = chartFilterParameters.avgormaxname; // selected avg or max option name
            var insuranceid = chartFilterParameters.insuranceid; //selected Payers id
            var insurancename = chartFilterParameters.insurancename; //selected payers name
            var numberofdays = chartFilterParameters.numberofdays; //selected number of days option's id
            var numberofdaysname = chartFilterParameters.numberofdaysname; //selected number of days option's name
            var CompanyidDisplayName = chartFilterParameters.CompanyidDisplayName; // Name to display for cleint dropdown
            var DateDisplayName = chartFilterParameters.DateDisplayName; //Name to display for datepicker
            var InsuranceidDisplayName = chartFilterParameters.InsuranceidDisplayName; //Name to display for payer dropdown
            var NumberOfDaysDisplayName = chartFilterParameters.NumberOfDaysDisplayName; //Name to display for # of days dropdown
            var AvgormaxDisplayName = chartFilterParameters.AvgormaxDisplayName; //Name to display for Average/max dropdown
            var doc = new jsPDF('landscape'); //PDF mode            
            var dataimg = '';
            document.getElementById('div_tbll').innerHTML = ""; //html element on each report page where this pdf will getdownloaded
            var tbll = document.createElement("table");
            //setting style for pdf
            tbll.style.cssText = 'background-color:white;width:100%;font-size: 9px;color: #676565;';
            tbll.setAttribute("cellpadding", "0");
            tbll.setAttribute("cellspacing", "0");

            //PDF if else according to dropdown (filter) 
            var filterHTML = '';
            if (companyid != "Not Available") { //if client dropdown available
                var clientInnerText = document.getElementById('clientFilter').children[0].children[0].innerText; //client filter box html innertext
                clientInnerText = clientInnerText.trim();
                if (companyid === "" || clientInnerText === 'ALL CLIENTS') {
                    companyname = "All Clients"; //if selected client id is -2 or blank
                } //else display following html
                filterHTML = filterHTML + ' <tr><td style="width:15%;border:1px solid #d4d0d0;font-size: 9px;padding-left: 7px;">' + CompanyidDisplayName + '</td>' + '<td style="width:85%;border:1px solid #d4d0d0;padding-left: 7px;border-left: none;"><span id="date_range">' + companyname + '</span></td></tr>';
            }

            //Insurance id for reportid 4 and 5 , if payer dropdown available
            if (insuranceid != "Not Available" && (reportid == 4 || reportid == 5)) {
                var payerInnerText = document.getElementById('payerFilter').children[0].children[0].innerText; //payer filter box html innertext
                payerInnerText = payerInnerText.trim();
                if (insuranceid == "" || payerInnerText === 'ALL PAYERS') {
                    insurancename = "All Payers";
                } //else display following html
                filterHTML = filterHTML + ' <tr><td style="width:15%;border:1px solid #d4d0d0;font-size: 9px;padding-left: 7px;">' + InsuranceidDisplayName + '</td>' + '<td style="width:85%;border:1px solid #d4d0d0;padding-left: 7px;border-left: none;"><span id="date_range">' + insurancename + '</span></td></tr>';
            }

            //Number of days dropdown for report 4 and 5
            if (numberofdays != "Not Available" && (reportid == 4 || reportid == 5)) { // if # of days dropdown available
                if (numberofdaysname == "") { //if selected no. of days id is blank
                    numberofdaysname = "Over 90";
                }
                filterHTML = filterHTML + ' <tr><td style="width:15%;border:1px solid #d4d0d0;font-size: 9px;padding-left: 7px;">' + NumberOfDaysDisplayName + '</td>' + '<td style="width:85%;border:1px solid #d4d0d0;padding-left: 7px;border-left: none;"><span id="date_range">' + numberofdaysname + '</span></td></tr>';
            } // else display following html

            //Average/Maximum dropdown
            if (avgormax != "Not Available" && reportid == 3) {
                if (avgormax == "") { //if selected option id is blank
                    avgormaxname = "Both";
                } //else display following html
                filterHTML = filterHTML + ' <tr><td style="width:15%;border:1px solid #d4d0d0;font-size: 9px;padding-left: 7px;">' + AvgormaxDisplayName + '</td>' + '<td style="width:85%;border:1px solid #d4d0d0;padding-left: 7px;border-left: none;"><span id="date_range">' + avgormaxname + '</span></td></tr>';
            }
            if (chartFilterParameters.fromdate != null && chartFilterParameters.todate != null) {
                //if fromdate and todate both are not null
                if (fromdate == "" && todate == "") {
                    //if fromdate and todate are blank then display dates received from report heading
                    filterHTML = filterHTML + ' <tr><td style="width:15%;border:1px solid #d4d0d0;font-size: 9px;padding-left: 7px;">' + DateDisplayName + '</td>' + '<td style="width:85%;border:1px solid #d4d0d0;padding-left: 7px;border-left: none;"><span id="date_range">(' + filterCharts.dateRange + ')</span></td></tr>';
                } else {
                    //else displaly selected from date and to date
                    filterHTML = filterHTML + ' <tr><td style="width:15%;border:1px solid #d4d0d0;font-size: 9px;padding-left: 7px;">' + DateDisplayName + '</td>' + '<td style="width:85%;border:1px solid #d4d0d0;padding-left: 7px;border-left: none;"><span id="date_range">(' + fromdate + " - " + todate + ')</span></td></tr>';
                }
            }

            //if fromdate is blank and todate is not blank
            if (chartFilterParameters.fromdate == "" && chartFilterParameters.todate != "") {
                filterHTML = filterHTML + ' <tr><td style="width:15%;border:1px solid #d4d0d0;font-size: 9px;padding-left: 7px;">' + "As of Date" + '</td>' + '<td style="width:85%;border:1px solid #d4d0d0;padding-left: 7px;border-left: none;"><span id="date_range">(' + todate + ')</span></td></tr>';
            }
            /*filterHTML = ' <tr><td style="width:15%;border:1px solid #d4d0d0;font-size: 9px;padding-left: 7px;">Date Range</td>' + '<td style="width:85%;border:1px solid #d4d0d0;padding-left: 7px;border-left: none;"><span id="date_range">12/25/2016 - 12/27/2016</span></td></tr>' + ' <tr><td style="width:15%;border:1px solid #d4d0d0;font-size: 9px;padding-left: 7px;">Client Name</td>' + '<td style="width:85%;border:1px solid #d4d0d0;padding-left: 7px;border-left: none;"><span id="client_name">Company1,Company2,Company3</span></td></tr>' + ' <tr><td style="width:15%;border:1px solid #d4d0d0;font-size: 9px;padding-left: 7px;">Insurance Name</td>' + '<td style="width:85%;border:1px solid #d4d0d0;padding-left: 7px;border-left: none;"><span id="insurance_name">Insurance1,Insurance2,Insurance3</span></td></tr>';*/

            tbll.innerHTML = filterHTML; //assigninig filterhmtl set in above conditions to innerhtml to create pdf

            document.getElementById('div_tbll').appendChild(tbll);
            var canvas = document.getElementsByTagName("canvas");

            document.getElementById('div_tbll').style.display = 'block';
            var dataimg2 = '';
            html2canvas([document.getElementById('div_tbll')], {
                onrendered: function(canvas2) {
                    dataimg2 = canvas2.toDataURL('image/jpeg');

                    for (var i = 0; i < canvas.length; i++) {
                        try {
                            canvas[count].style.border = '1px solid #fff';
                            canvas[count].style.backgroundColor = '#fff';
                            html2canvas(canvas[count], {
                                onrendered: function(canvas1) {
                                    canvas1.style.border = '1px solid #fff';
                                    canvas1.style.backgroundColor = '#fff';
                                    dataimg = canvas1.toDataURL('image/jpeg');
                                    doc.setFontSize(13);
                                    doc.setTextColor(80);
                                    doc.setFontType("bold");
                                    doc.addImage(base64img, 'JPEG', 10, 5, 60, 15);
                                    doc.setLineWidth(0.1);
                                    doc.line(10, 25, 280, 25);
                                    if (i == 0 || i == 1)
                                        doc.addImage(dataimg2, 'JPEG', 10, 40, 0, 0);
                                    doc.setTextColor(9, 103, 123);
                                    doc.setFontSize(15);
                                    doc.text(10, 35, document.getElementById("reportHeader").children[0].children[0].innerText);
                                    doc.setFontSize(12);
                                    doc.setFontSize(7);
                                    doc.setTextColor(120);
                                    doc.line(10, 25, 280, 25);


                                    if (i == 0 || i == 1)
                                        doc.addImage(dataimg, 'JPEG', 20, 80, 250, 90);
                                    else
                                        doc.addImage(dataimg, 'JPEG', 20, 60, 250, 90);

                                    doc.setDrawColor(255);
                                    doc.setLineWidth(0.5);
                                    doc.line(270, 80, 270, 170);
                                    doc.line(270, 80, 270, 170);
                                    doc.setDrawColor(255);
                                    doc.setLineWidth(0.5);
                                    doc.line(20, 80, 270, 80);
                                    doc.line(10, 170, 280, 170);



                                    doc.setDrawColor(120);


                                    doc.setFontSize(12);
                                    doc.setFontSize(7);
                                    doc.setTextColor(120);

                                    doc.line(10, 195, 280, 195);
                                    doc.setFontSize(11);
                                    doc.setTextColor(100);
                                    doc.setFontType("normal");
                                    doc.text(10, 203, document.getElementById("leftFooterSpan").innerText);


                                    doc.setFontSize(11);
                                    doc.setTextColor(100);
                                    doc.setFontType("normal");
                                    doc.text(240, 203, document.getElementById("rightFooterSpan").innerText);



                                    if (i < canvas.length - 1)
                                        doc.addPage();
                                    if (i == (canvas.length)) {
                                        $scope.saveDoc(doc);
                                    }

                                }
                            });
                        } catch (e) {
                            console.log(e.toString());
                        }
                    }
                }
            });

            document.getElementById('div_tbll').style.display = 'none'; //hiding pdf tag from report display

            svgdataarray.splice(0, svgdataarray.length);
            svgdataarray = [];

        }

        $scope.saveDoc = function(doc) { //save pdf on system 
            var out = doc.output('save', document.getElementById("reportHeader").children[0].innerText + '.pdf');
        }

        $scope.printPdf = function() { //make pdf
            document.getElementById('iframeExport').src = "";
            $scope.convertImgToBase641(function(base64img1) { $scope.convertImgToBase64(function(base64img) { $scope.CreatePDF(base64img, base64img1) }, "image/jpeg") }, "image/jpeg");
            return false;
        }


        //EXCEL EXPORT FUNCTION

        $scope.printExcel = function() {
            //Getting selected dropdown/datepicker parameters ,to display them in pdf
            var Servicetype = chartFilterParameters.ServiceType; //selected report's service type
            var filename = document.getElementById("reportHeader").children[0].children[0].innerText; //name to be given to excel file
            var companyid = chartFilterParameters.companyid; //selected client id
            var fromdate = chartFilterParameters.fromdate; //selected from date
            var todate = chartFilterParameters.todate; //selected to date
            var reportid = chartFilterParameters.reportid; //selected report's id
            var userid = chartFilterParameters.userid; //logged in user's id
            var avgormax = chartFilterParameters.avgormax; //selected avg/max option's id
            var insuranceid = chartFilterParameters.insuranceid; //selected payer's id
            var numberofdays = chartFilterParameters.numberofdays; // selected # of days id
            var CompanyidDisplayName = chartFilterParameters.CompanyidDisplayName; // Name to display for client dropdown
            var DateDisplayName = chartFilterParameters.DateDisplayName; //Name to display for DatePicker dropdown
            var InsuranceidDisplayName = chartFilterParameters.InsuranceidDisplayName; //Name to display for Payer dropdown
            var NumberOfDaysDisplayName = chartFilterParameters.NumberOfDaysDisplayName; //Name to display for # of days dropdown
            var AvgormaxDisplayName = chartFilterParameters.AvgormaxDisplayName; //Name to display for average/max dropdwn
            if (fromdate == "" && todate == "") {
                //If fromdate and todate are null then display dates from daterange received in report heading   
                var daterange = chartFilterParameters.dateRange;
                if (reportid != 4) {
                    daterange = daterange.split(" - ");
                    fromdate = daterange[0];
                    todate = daterange[1];
                    fromdate = $filter('date')(new Date(fromdate), "MM/dd/yyyy");
                    todate = $filter('date')(new Date(todate), "MM/dd/yyyy");
                    document.getElementById('iframeExport').src = appSettings.excelExportUrl + "Servicetype=" + Servicetype + "&FileName=" + filename + "&companyid=" + companyid + "&reporttype=0&fromdate=" + fromdate + "&todate=" + todate + "&reportid=" + reportid + "&userid=" + userid + "&avgormax=" + avgormax + "&insuranceid=" + insuranceid + "&numberofdays=" + numberofdays + "&CompanyidDisplayName=" + CompanyidDisplayName + "&DateDisplayName=" + DateDisplayName + "&InsuranceidDisplayName=" + InsuranceidDisplayName + "&NumberOfDaysDisplayName=" + NumberOfDaysDisplayName + "&AvgormaxDisplayName=" + AvgormaxDisplayName;
                } else {
                    //if report id is 4 convert As of date received in daterange to  MM/dd/yyyy format 
                    var convertDate = (daterange.toLowerCase()).replace('as of ', '');
                    convertDate = new Date(convertDate);
                    fromdate = $filter('date')(convertDate, "MM/dd/yyyy");
                    todate = $filter('date')(convertDate, "MM/dd/yyyy");
                    document.getElementById('iframeExport').src = appSettings.excelExportUrl + "Servicetype=" + Servicetype + "&FileName=" + filename + "&companyid=" + companyid + "&reporttype=0&fromdate=" + fromdate + "&todate=" + todate + "&reportid=" + reportid + "&userid=" + userid + "&avgormax=" + avgormax + "&insuranceid=" + insuranceid + "&numberofdays=" + numberofdays + "&CompanyidDisplayName=" + CompanyidDisplayName + "&DateDisplayName=" + DateDisplayName + "&InsuranceidDisplayName=" + InsuranceidDisplayName + "&NumberOfDaysDisplayName=" + NumberOfDaysDisplayName + "&AvgormaxDisplayName=" + AvgormaxDisplayName;
                }
            } else {
                //if dates are not null then 
                document.getElementById('iframeExport').src = appSettings.excelExportUrl + "Servicetype=" + Servicetype + "&FileName=" + filename + "&companyid=" + companyid + "&reporttype=0&fromdate=" + fromdate + "&todate=" + todate + "&reportid=" + reportid + "&userid=" + userid + "&avgormax=" + avgormax + "&insuranceid=" + insuranceid + "&numberofdays=" + numberofdays + "&CompanyidDisplayName=" + CompanyidDisplayName + "&DateDisplayName=" + DateDisplayName + "&InsuranceidDisplayName=" + InsuranceidDisplayName + "&NumberOfDaysDisplayName=" + NumberOfDaysDisplayName + "&AvgormaxDisplayName=" + AvgormaxDisplayName;
            }
        }
    }]);

} catch (exception) {
    console.log("Exception: " + exception);
}
