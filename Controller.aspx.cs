﻿using Elmah;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;
using Newtonsoft.Json;
using System.Collections;
using System.Web.Script.Serialization;
using System.Net.Mail;
using System.Net.Mime;
using System.Net;
using Newtonsoft.Json.Linq;

public partial class Controller : System.Web.UI.Page
{
    class Data
    {
        public string Name { get; set; }
        public string Startdate { get; set; }
        public string Enddate { get; set; }

    }
    protected void GetJsonValues()
    {
        //List<Data> data = new List<Data>();

        //data.Add(new Data { Name = "Json O' Brien", Startdate = "", Enddate = "" });
        //data.Add(new Data { Name = "Name_Sername", Startdate = "", Enddate = "" });
        //data.Add(new Data { Name = "name-surname", Startdate = "", Enddate = "" });
        //data.Add(new Data { Name = "Hello'", Startdate = "", Enddate = "" });
        //data.Add(new Data { Name = "jhdfhj", Startdate = "", Enddate = "" });
        //var json = "";
        //try { json = JsonConvert.SerializeObject(data); }
        //catch (Exception ex) { json = ex.ToString(); }
        //Response.Write(json);
        //Response.Flush();
        //HttpContext.Current.ApplicationInstance.CompleteRequest();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        //GetJsonValues();
        //return;
        //GetResponse("getclients");
        //return;

        //string[] AuthenticateDomain = System.Configuration.ConfigurationSettings.AppSettings["Domain"].ToString().Split(',');
        //string currentDomain = Request.UrlReferrer.Host;
        //int avail = 0;
        //for (int i = 0; i < AuthenticateDomain.Length; i++)
        //{
        //    if (currentDomain.ToLower().Contains(AuthenticateDomain[i].ToString().ToLower()))
        //    {
        //        avail = 1;
        //        break;
        //    }
        //}

        // if (!string.IsNullOrEmpty(Request["Servicetype"]) && avail==1)
        if (!string.IsNullOrEmpty(Request["Servicetype"]))

            GetResponse(Request["Servicetype"]);
        //  GetResponse("getaveragedaystobill");

    }


    private void SendEmail(string errors, string subject, string emailids)
    {


        System.IO.StringWriter tw = new System.IO.StringWriter();
        MailMessage mm = new MailMessage();
        mm.From = new MailAddress(("notify@altamm.com").Trim(), ("notify@altamm.com"));
        string[] mailto = ("arindom@viriminfotech.com").Split(',');
        if (emailids == "")
        {
            for (int i = 0; i < mailto.Length; i++)
            {
                if (mailto[i] != "")
                {

                    mm.To.Add(new MailAddress(mailto[i].ToString().Trim()));
                }
            }
        }
        else
        {
            string[] mailcc = emailids.Split(',');
            for (int i = 0; i < mailcc.Length; i++)
            {
                if (mailcc[i] != "")
                {

                    mm.To.Add(new MailAddress(mailcc[i].ToString().Trim()));
                }
            }

            for (int i = 0; i < mailto.Length; i++)
            {
                if (mailto[i] != "")
                {

                    mm.Bcc.Add(new MailAddress(mailto[i].ToString().Trim()));
                }
            }

        }





        string html = errors; //sb.ToString();



        AlternateView avHtml = AlternateView.CreateAlternateViewFromString
     (html, null, MediaTypeNames.Text.Html);




        mm.Subject = subject;// System.Configuration.ConfigurationSettings.AppSettings["subject"].ToString();
        mm.Body = html;
        mm.IsBodyHtml = true;
        mm.AlternateViews.Add(avHtml);

        SmtpClient smtp = new SmtpClient();
        smtp.Host = "mail.altamm.com";
        smtp.EnableSsl = false;
        NetworkCredential NetworkCred = new NetworkCredential();
        NetworkCred.UserName = "notify@altamm.com";
        NetworkCred.Password = "1Gl0b@L!2016";
        smtp.UseDefaultCredentials = true;
        smtp.Credentials = NetworkCred;
        smtp.Port = 587;
        smtp.Send(mm);





    }

    private void GetResponse(string Servicetype)
    {
        DBConnect objConnect = new DBConnect();
        string convertedstring = string.Empty;
        string companyid = ((Request["companyid"] != null)) ? ((Request["companyid"].ToString().ToLower() != "not available") ? Request["companyid"] : "") : "";
        int reporttype = (!string.IsNullOrEmpty(Request["reporttype"])) ? ((Request["reporttype"].ToString().ToLower() != "not available") ? Convert.ToInt32(Request["reporttype"]) : 0) : 0;
        string fromdate = ((Request["fromdate"] != null)) ? ((Request["fromdate"].ToString().ToLower() != "not available") ? Request["fromdate"] : "") : "";
        string todate = ((Request["todate"] != null)) ? ((Request["todate"].ToString().ToLower() != "not available") ? Request["todate"] : "") : "";
        int reportid = (!string.IsNullOrEmpty(Request["reportid"])) ? ((Request["reportid"].ToString().ToLower() != "not available") ? Convert.ToInt32(Request["reportid"]) : 0) : 0;
        int userid = (!string.IsNullOrEmpty(Request["userid"])) ? ((Request["userid"].ToString().ToLower() != "not available") ? Convert.ToInt32(Request["userid"]) : 0) : 0;
        string avgormax = ((Request["avgormax"] != null)) ? ((Request["avgormax"].ToString().ToLower() != "not available") ? Request["avgormax"] : "") : "";
        string insuranceid = ((Request["insuranceid"] != null)) ? ((Request["insuranceid"].ToString().ToLower() != "not available") ? Request["insuranceid"] : "") : "";
        string numberofdays = ((Request["numberofdays"] != null)) ? ((Request["numberofdays"].ToString().ToLower() != "not available") ? Request["numberofdays"] : "") : "";
        int isreport = (!string.IsNullOrEmpty(Request["isreport"]) ? Convert.ToInt32(Request["isreport"].ToString()) : 0);

        // int isreport = 0;

        //string msg = "New _ companyid : " + companyid + "  reporttype: " + reporttype.ToString() + " fromdate : " + fromdate+Environment.NewLine;
        //msg = msg + "todate : " + todate + " reportid : " + reportid.ToString() + " userid : " + userid.ToString() + " avgormax : " + avgormax + Environment.NewLine;
        //msg = msg + "insuranceid : " + insuranceid + " numberofdays : " + numberofdays + " isreport : " + Request["isreport"] + " Servicetype : " + Servicetype;
        //SendEmail(msg, "Post Data", "arindom@viriminfotech.com");

        if (Servicetype.Trim().ToLower() == ("getAverageDaysToBill").ToLower())
        {
            try
            {
                DataSet ds = objConnect.getAverageDaysToBill(companyid, reporttype, fromdate, todate, reportid, userid, isreport);
                //DataSet ds = objConnect.getAverageDaysToBill("", 1, "12/25/2016", "12/25/2016", 139, 1,0);
                if (ds != null)
                {
                    if (ds.Tables.Count > 0)
                    {

                        convertedstring = (reporttype == 0) ? JsonConvert.SerializeObject(ds) : FetchChartJSON(ds);
                        //  convertedstring = FetchChartJSON(ds);
                    }
                    else
                        convertedstring = "{\"errortag\": \"Some Exception Occured\"}";
                }
                else
                    convertedstring = "{\"errortag\": \"Some Exception Occured\"}";
            }

            catch (Exception ex)
            {
                convertedstring = "{\"errortag\": \"Some Exception Occured\"}";
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
        }
        else if (Servicetype.Trim().ToLower() == ("getOdometerValues").ToLower())
        {
            try
            {

                //DataSet ds = objConnect.getOdometerValues("", 1, "", "", 2, 139, 0);
                DataSet ds = objConnect.getOdometerValues(companyid, reporttype, fromdate, todate, reportid, userid, isreport);
                // DataSet ds = objConnect.getAverageDaysToBill("", 1, "12/25/2016", "12/25/2016", 1, 1);
                if (ds != null)
                {
                    if (ds.Tables.Count > 0)
                    {

                        // convertedstring = (reporttype == 0) ? JsonConvert.SerializeObject(ds) : FetchChartJSON(ds);
                        // convertedstring = JsonConvert.SerializeObject(ds);

                        string returntext = "{";
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            if (ds.Tables[0].Rows[i]["Title"].ToString().ToLower().Contains("charges"))
                                returntext = returntext + "\"ChargesPerRun\": {";
                            else if (ds.Tables[0].Rows[i]["Title"].ToString().ToLower().Contains("revenue"))
                                returntext = returntext + "\"RevenuePerRun\": {";
                            else if (ds.Tables[0].Rows[i]["Title"].ToString().ToLower().Contains("days"))
                                returntext = returntext + "\"DaysinAR\": {";
                            returntext = returntext + "\"title\": \"" + ds.Tables[0].Rows[i]["Title"].ToString() + "\",";
                            //Start condition add to hendle null value by sumit soni on 03 feb 2017
                            returntext = returntext + "\"value\": " + ((ds.Tables[0].Rows[i]["Value"] == null || ds.Tables[0].Rows[i]["Value"].ToString().Trim() == "") ? "null" : ds.Tables[0].Rows[i]["Value"].ToString()) + ",";
                            returntext = returntext + "\"upperLimit\": " + ((ds.Tables[0].Rows[i]["UpperLimit"] == null || ds.Tables[0].Rows[i]["UpperLimit"].ToString().Trim() == "") ? "null" : ds.Tables[0].Rows[i]["UpperLimit"].ToString()) + ",";
                            returntext = returntext + "\"lowerLimit\": " + ((ds.Tables[0].Rows[i]["LowerLimit"] == null || ds.Tables[0].Rows[i]["LowerLimit"].ToString().Trim() == "") ? "null" : ds.Tables[0].Rows[i]["LowerLimit"].ToString()) + ",";
                            returntext = returntext + "\"ranges\": [{";
                            returntext = returntext + "\"min\": " + ((ds.Tables[0].Rows[i]["LowerLimit"] == null || ds.Tables[0].Rows[i]["LowerLimit"].ToString().Trim() == "") ? "null" : ds.Tables[0].Rows[i]["LowerLimit"].ToString()) + ",";
                            returntext = returntext + "\"max\": " + ((ds.Tables[0].Rows[i]["TargetValue"] == null || ds.Tables[0].Rows[i]["TargetValue"].ToString().Trim() == "") ? "null" : ds.Tables[0].Rows[i]["TargetValue"].ToString()) + "},{";
                            returntext = returntext + "\"min\": " + ((ds.Tables[0].Rows[i]["TargetValue"] == null || ds.Tables[0].Rows[i]["TargetValue"].ToString().Trim() == "") ? "null" : ds.Tables[0].Rows[i]["TargetValue"].ToString()) + ",";
                            returntext = returntext + "\"max\": " + ((ds.Tables[0].Rows[i]["UpperLimit"] == null || ds.Tables[0].Rows[i]["UpperLimit"].ToString().Trim() == "") ? "null" : ds.Tables[0].Rows[i]["UpperLimit"].ToString()) + "}]},";
                            //Start condition add to hendle null value by sumit soni on 03 feb 2017

                        }


                        //returntext = returntext + "\"RevenuePerRun\": {";
                        //returntext = returntext + "\"title\": \"" + ds.Tables[0].Rows[1]["Title"].ToString() + "\",";
                        //returntext = returntext + "\"value\": \"" + ds.Tables[0].Rows[1]["Value"].ToString() + "\",";
                        //returntext = returntext + "\"upperLimit\": \"" + ds.Tables[0].Rows[1]["UpperLimit"].ToString() + "\",";
                        //returntext = returntext + "\"lowerLimit\": \"" + ds.Tables[0].Rows[1]["LowerLimit"].ToString() + "\",";
                        //returntext = returntext + "\"ranges\": [{";
                        //returntext = returntext + "\"min\": " + ds.Tables[0].Rows[1]["LowerLimit"].ToString() + ",";
                        //returntext = returntext + "\"max\": " + ds.Tables[0].Rows[1]["TargetValue"].ToString() + "},{";
                        //returntext = returntext + "\"min\": " + ds.Tables[0].Rows[1]["TargetValue"].ToString() + ",";
                        //returntext = returntext + "\"max\": " + ds.Tables[0].Rows[1]["UpperLimit"].ToString() + "}]},";


                        //returntext = returntext + "\"DaysinAR\": {";
                        //returntext = returntext + "\"title\": \"" + ds.Tables[0].Rows[2]["Title"].ToString() + "\",";
                        //returntext = returntext + "\"value\": \"" + ds.Tables[0].Rows[2]["Value"].ToString() + "\",";
                        //returntext = returntext + "\"upperLimit\": \"" + ds.Tables[0].Rows[2]["UpperLimit"].ToString() + "\",";
                        //returntext = returntext + "\"lowerLimit\": \"" + ds.Tables[0].Rows[2]["LowerLimit"].ToString() + "\",";
                        //returntext = returntext + "\"ranges\": [{";
                        //returntext = returntext + "\"min\": " + ds.Tables[0].Rows[2]["LowerLimit"].ToString() + ",";
                        //returntext = returntext + "\"max\": " + ds.Tables[0].Rows[2]["TargetValue"].ToString() + "},{";
                        //returntext = returntext + "\"min\": " + ds.Tables[0].Rows[2]["TargetValue"].ToString() + ",";
                        //returntext = returntext + "\"max\": " + ds.Tables[0].Rows[2]["UpperLimit"].ToString() + "}]},";
                        returntext = returntext + "\"Database_Error\":\"" + ds.Tables[0].Rows[0]["Database_Error"].ToString() +"\",";
                        returntext = returntext + "\"ReportTitle\":\"" + ds.Tables[1].Rows[0]["ReportTitle"].ToString() + "\"}";

                        // var jsondata= Newtonsoft.Json.JsonConvert.DeserializeObject(returntext);


                        convertedstring = returntext;
                    }
                    else
                        convertedstring = "{\"errortag\": \"Some Exception Occured\"}";
                }
                else 
                    convertedstring = "{\"errortag\": \"Some Exception Occured\"}";
            }

            catch (Exception ex)
            {
                convertedstring = "{\"errortag\": \"Some Exception Occured\"}";
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
        }
        else if (Servicetype.Trim().ToLower() == ("GetReportName").ToLower())
        {
            try
            {
                DataSet ds = objConnect.GetReportName(companyid, reporttype, fromdate, todate, reportid, userid, avgormax, numberofdays, insuranceid);
                if (ds != null)
                {
                    if (ds.Tables.Count > 0)
                    {

                        convertedstring = JsonConvert.SerializeObject(ds);

                    }
                    else
                        convertedstring = "{\"errortag\": \"Some Exception Occured\"}";
                }
                else
                    convertedstring = "{\"errortag\": \"Some Exception Occured\"}";
            }

            catch (Exception ex)
            {
                convertedstring = "{\"errortag\": \"Some Exception Occured\"}";
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
        }
        else if (Servicetype.Trim().ToLower() == ("getClaimsWaitingToBeBilled").ToLower())
        {
            try
            {
                //DataSet ds = objConnect.getClaimsWaitingToBeBilled("", 1, "", "", 2, 139, 0);

                DataSet ds = objConnect.getClaimsWaitingToBeBilled(companyid, reporttype, fromdate, todate, reportid, userid, isreport);
                if (ds != null)
                {
                    if (ds.Tables.Count > 0)
                    {

                        convertedstring = (reporttype == 0) ? JsonConvert.SerializeObject(ds) : FetchChartJSON(ds);

                    }
                    else
                        convertedstring = "{\"errortag\": \"Some Exception Occured\"}";
                }
                else
                {
                    convertedstring = "{\"errortag\": \"Some Exception Occured\"}";
                }
            }

            catch (Exception ex)
            {
                convertedstring = "{\"errortag\": \"Some Exception Occured\"}";
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
        }
        else if (Servicetype.Trim().ToLower() == ("GetDateOfServiceVsDateOfUpload").ToLower())
        {
            try
            {
                DataSet ds = objConnect.GetDateOfServiceVsDateOfUpload(companyid, reporttype, fromdate, todate, reportid, userid, avgormax);
                if (ds != null)
                {
                    if (ds.Tables.Count > 0)
                    {

                        convertedstring = (reporttype == 0) ? JsonConvert.SerializeObject(ds) : FetchChartJSON(ds);

                    }

                    else
                        convertedstring = "{\"errortag\": \"Some Exception Occured\"}";
                }
                else
                    convertedstring = "{\"errortag\": \"Some Exception Occured\"}";
            }

            catch (Exception ex)
            {
                convertedstring = "{\"errortag\": \"Some Exception Occured\"}";
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
        }
        else if (Servicetype.Trim().ToLower() == ("Getavgormax").ToLower())
        {
            try
            {
                DataSet ds = objConnect.Getavgormax(companyid, reporttype, fromdate, todate, reportid, userid, avgormax);
                if (ds != null)
                {
                    if (ds.Tables.Count > 0)
                    {
                        string StrJson = "";
                        List<TreeNodes> objlisttree = new List<TreeNodes>();
                        for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
                        {
                            TreeNodes objtree = new TreeNodes();
                            objtree.text = ds.Tables[0].Rows[i]["Name"].ToString();
                            objtree.value = (ds.Tables[0].Rows[i]["Name"].ToString());
                            objtree.id = Convert.ToInt32(ds.Tables[0].Rows[i]["Id"].ToString());
                            objtree.parentId = ds.Tables[0].Rows[i]["ParentId"].ToString();
                            objtree.open = true;
                            objtree.ischecked = true;
                            objlisttree.Add(objtree);
                        }
                        if (objlisttree.Count > 0)
                        {

                            StrJson = "   [{";
                            StrJson += "\"id\":" + objlisttree[0].id + ",";


                            StrJson += "\"text\":\"" + objlisttree[0].text + "\",";
                            StrJson += "\"value\":\"" + objlisttree[0].text + "\",";
                            StrJson += "\"checked\":" + "true" + ",";
                            StrJson += "\"open\":" + "true" + ",";

                            StrJson += getNodeChildren(objlisttree[0].id.ToString(), objlisttree);

                            StrJson += "}]";
                        }

                        // convertedstring = JsonConvert.SerializeObject(ds);
                        convertedstring = StrJson;

                    }
                    else
                        convertedstring = "{\"errortag\": \"Some Exception Occured\"}";
                }
                else
                    convertedstring = "{\"errortag\": \"Some Exception Occured\"}";
            }

            catch (Exception ex)
            {
                convertedstring = "{\"errortag\": \"Some Exception Occured\"}";
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
        }
        else if (Servicetype.Trim().ToLower() == ("getTotalOutstandingClaims").ToLower())
        {
            try
            {
                DataSet ds = objConnect.getTotalOutstandingClaims(companyid, reporttype, fromdate, todate, reportid, userid, numberofdays, insuranceid);
                if (ds != null)
                {
                    if (ds.Tables.Count > 0)
                    {

                        convertedstring = (reporttype == 0) ? JsonConvert.SerializeObject(ds) : FetchChartJSON(ds);

                    }
                    else
                        convertedstring = "{\"errortag\": \"Some Exception Occured\"}";
                }
                else
                    convertedstring = "{\"errortag\": \"Some Exception Occured\"}";
            }

            catch (Exception ex)
            {
                convertedstring = "{\"errortag\": \"Some Exception Occured\"}";
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
        }
        else if (Servicetype.Trim().ToLower() == ("GetInsurances").ToLower())
        {
            try
            {
                DataSet ds = objConnect.GetInsurances(companyid, reporttype, fromdate, todate, reportid, userid, avgormax, insuranceid);
                if (ds != null)
                {
                    if (ds.Tables.Count > 0)
                    {


                        string StrJson = "";
                        List<TreeNodes> objlisttree = new List<TreeNodes>();
                        for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
                        {
                            TreeNodes objtree = new TreeNodes();
                            objtree.text = ds.Tables[0].Rows[i]["Name"].ToString();
                            objtree.value = (ds.Tables[0].Rows[i]["Name"].ToString());
                            objtree.id = Convert.ToInt32(ds.Tables[0].Rows[i]["Id"].ToString());
                            objtree.parentId = ds.Tables[0].Rows[i]["ParentId"].ToString();
                            objtree.open = true;
                            objtree.ischecked = true;
                            objlisttree.Add(objtree);
                        }
                        if (objlisttree.Count > 0)
                        {

                            StrJson = "";
                            foreach (var tree in objlisttree)
                            {
                                if (Convert.ToInt32(tree.parentId) < 0)
                                {
                                    if (StrJson == "")
                                    {
                                        StrJson = "[{";
                                    }
                                    else
                                    {
                                        StrJson += ",{";
                                    }

                                    //StrJson += "\"id\":" + objlisttree[0].id + ",";


                                    //StrJson += "\"text\":\"" + objlisttree[0].text + "\",";
                                    //StrJson += "\"value\":\"" + objlisttree[0].text + "\",";
                                    //StrJson += "\"checked\":\"" + "true" + "\",";
                                    //StrJson += "\"open\":\"" + "true" + "\",";


                                    //StrJson += getNodeChildren(objlisttree[0].id.ToString(), objlisttree);

                                    StrJson += "\"id\":" + tree.id + ",";


                                    StrJson += "\"text\":\"" + tree.text + "\",";
                                    StrJson += "\"value\":\"" + tree.text + "\",";
                                    StrJson += "\"checked\":" + "true" + ",";
                                    StrJson += "\"open\":" + "true" + "";


                                    // StrJson += getNodeChildren(objlisttree[0].id.ToString(), objlisttree);

                                    StrJson += ((objlisttree.Where(w => w.parentId == tree.id.ToString()).Count() > 0) ? "," + getNodeChildren(tree.id.ToString(), objlisttree) : "");
                                    StrJson += "}";
                                }
                            }

                            StrJson += "]";
                        }

                        // convertedstring = JsonConvert.SerializeObject(ds);
                        convertedstring = StrJson;

                    }
                    else
                        convertedstring = "{\"errortag\": \"Some Exception Occured\"}";
                }
                else
                    convertedstring = "{\"errortag\": \"Some Exception Occured\"}";
            }

            catch (Exception ex)
            {
                convertedstring = "{\"errortag\": \"Some Exception Occured\"}";
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
        }
        else if (Servicetype.Trim().ToLower() == ("getclients").ToLower())
        {
            try
            {
                //DataSet ds = objConnect.getclients(139);               
                DataSet ds = objConnect.getclients(userid);
                if (ds != null)
                {
                    if (ds.Tables.Count > 0)
                    {


                        string StrJson = "";
                        List<TreeNodes> objlisttree = new List<TreeNodes>();
                        for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
                        {
                            TreeNodes objtree = new TreeNodes();
                            objtree.text = ds.Tables[0].Rows[i]["Name"].ToString();
                            objtree.value = (ds.Tables[0].Rows[i]["Name"].ToString());
                            objtree.id = Convert.ToInt32(ds.Tables[0].Rows[i]["Id"].ToString());
                            objtree.parentId = ds.Tables[0].Rows[i]["ParentId"].ToString();
                            objtree.open = true;
                            objtree.ischecked = true;
                            objlisttree.Add(objtree);
                        }
                        if (objlisttree.Count > 0)
                        {

                            StrJson = "   [{";
                            StrJson += "\"id\":" + objlisttree[0].id + ",";


                            StrJson += "\"text\":\"" + objlisttree[0].text.Replace("\"", "\\\"") + "\",";
                            StrJson += "\"value\":\"" + objlisttree[0].text.Replace("\"", "\\\"") + "\",";
                            StrJson += "\"checked\":" + "true" + ",";
                            StrJson += "\"open\":" + "true" + ",";


                            StrJson += getNodeChildren(objlisttree[0].id.ToString(), objlisttree);

                            StrJson += "}]";
                        }
                        //StrJson = JsonConvert.SerializeObject(ds.Tables[0]);

                        // convertedstring = JsonConvert.SerializeObject(ds);
                        convertedstring = StrJson;

                    }
                    else
                        convertedstring = "{\"errortag\": \"Some Exception Occured\"}";
                }
                else
                    convertedstring = "{\"errortag\": \"Some Exception Occured\"}";
            }

            catch (Exception ex)
            {
                convertedstring = "{\"errortag\": \"Some Exception Occured\"}";
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
        }
        else if (Servicetype.Trim().ToLower() == ("GetNumberOfDays").ToLower())
        {
            try
            {
                DataSet ds = objConnect.GetNumberOfDays(companyid, reporttype, fromdate, todate, reportid, userid, avgormax, numberofdays, insuranceid);
                if (ds != null)
                {
                    if (ds.Tables.Count > 0)
                    {


                        string StrJson = "";
                        // List<TreeNodes> objlisttree = new List<TreeNodes>();
                        StrJson = "";
                        for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
                        {

                            if (StrJson == "")
                            {
                                StrJson = "[{";
                            }
                            else
                            {
                                StrJson += ",{";
                            }
                            StrJson += "\"id\":" + ds.Tables[0].Rows[i]["Id"].ToString() + ",";


                            StrJson += "\"text\":\"" + ds.Tables[0].Rows[i]["Name"].ToString() + "\",";
                            StrJson += "\"value\":\"" + ds.Tables[0].Rows[i]["Name"].ToString() + "\",";
                            StrJson += "\"checked\":" + "true" + ",";
                            StrJson += "\"open\":" + "true" + "";


                            StrJson += "}";
                        }

                        StrJson += "]";

                        // convertedstring = JsonConvert.SerializeObject(ds);
                        convertedstring = StrJson;

                    }
                    else
                        convertedstring = "{\"errortag\": \"Some Exception Occured\"}";
                }
                else
                    convertedstring = "{\"errortag\": \"Some Exception Occured\"}";
            }

            catch (Exception ex)
            {
                convertedstring = "{\"errortag\": \"Some Exception Occured\"}";
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
        }
        else if (Servicetype.Trim().ToLower() == ("getPercentOutstandingClaims").ToLower())
        {
            try
            {
                DataSet ds = objConnect.getPercentOutstandingClaims(companyid, reporttype, fromdate, todate, reportid, userid, numberofdays, insuranceid, isreport);
                if (ds != null)
                {
                    if (ds.Tables.Count > 0)
                    {

                        convertedstring = (reporttype == 0) ? JsonConvert.SerializeObject(ds) : FetchChartJSON(ds);

                    }
                    else
                        convertedstring = "{\"errortag\": \"Some Exception Occured\"}";
                }
                else
                    convertedstring = "{\"errortag\": \"Some Exception Occured\"}";
            }

            catch (Exception ex)
            {
                convertedstring = "{\"errortag\": \"Some Exception Occured\"}";
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
        }
        else if (Servicetype.Trim().ToLower() == ("getTotalHoursWorked").ToLower())
        {
            try
            {
                DataSet ds = objConnect.getTotalHoursWorked(companyid, reporttype, fromdate, todate, reportid, userid, numberofdays, insuranceid, isreport);
                if (ds != null)
                {
                    if (ds.Tables.Count > 0)
                    {

                        convertedstring = (reporttype == 0) ? JsonConvert.SerializeObject(ds) : FetchChartJSON(ds);

                    }
                    else
                        convertedstring = "{\"errortag\": \"Some Exception Occured\"}";
                }
                else
                    convertedstring = "{\"errortag\": \"Some Exception Occured\"}";
            }

            catch (Exception ex)
            {
                convertedstring = "{\"errortag\": \"Some Exception Occured\"}";
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
        }
        else if (Servicetype.Trim().ToLower() == ("getTotalClaimsWaitingToBeBilledVsTotalClaimsBilled").ToLower())
        {
            try
            {
                DataSet ds = objConnect.getTotalClaimsWaitingToBeBilledVsTotalClaimsBilled(companyid, reporttype, fromdate, todate, reportid, userid, numberofdays, insuranceid, isreport);
                if (ds != null)
                {
                    if (ds.Tables.Count > 0)
                    {

                        convertedstring = (reporttype == 0) ? JsonConvert.SerializeObject(ds) : FetchChartJSON(ds);

                    }
                    else
                        convertedstring = "{\"errortag\": \"Some Exception Occured\"}";
                }
                else
                    convertedstring = "{\"errortag\": \"Some Exception Occured\"}";
            }

            catch (Exception ex)
            {
                convertedstring = "{\"errortag\": \"Some Exception Occured\"}";
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
        }
        else if (Servicetype.Trim().ToLower() == ("getClaimsBilledPerClient").ToLower())
        {
            try
            {
                DataSet ds = objConnect.getClaimsBilledPerClient(companyid, reporttype, fromdate, todate, reportid, userid);
                // DataSet ds = objConnect.getAverageDaysToBill("", 1, "12/25/2016", "12/25/2016", 1, 1);
                if (ds != null)
                {
                    if (ds.Tables.Count > 0)
                    {

                        convertedstring = (reporttype == 0) ? JsonConvert.SerializeObject(ds) : FetchChartJSON(ds);
                        //  convertedstring = FetchChartJSON(ds);
                    }
                    else
                        convertedstring = "{\"errortag\": \"Some Exception Occured\"}";
                }
                else
                    convertedstring = "{\"errortag\": \"Some Exception Occured\"}";
            }

            catch (Exception ex)
            {
                convertedstring = "{\"errortag\": \"Some Exception Occured\"}";
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
        }
        else if (Servicetype.Trim().ToLower() == ("getAverageDaysOutstandingTrending").ToLower())
        {
            try
            {
                DataSet ds = objConnect.getAverageDaysOutstandingTrending(companyid, reporttype, fromdate, todate, reportid, userid);
                // DataSet ds = objConnect.getAverageDaysToBill("", 1, "12/25/2016", "12/25/2016", 1, 1);
                if (ds != null)
                {
                    if (ds.Tables.Count > 0)
                    {

                        convertedstring = (reporttype == 0) ? JsonConvert.SerializeObject(ds) : FetchChartJSON(ds);
                        //  convertedstring = FetchChartJSON(ds);
                    }
                    else
                        convertedstring = "{\"errortag\": \"Some Exception Occured\"}";
                }
                else
                    convertedstring = "{\"errortag\": \"Some Exception Occured\"}";
            }

            catch (Exception ex)
            {
                convertedstring = "{\"errortag\": \"Some Exception Occured\"}";
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
        }
        else if (Servicetype.Trim().ToLower() == ("getMedicarelastPayment").ToLower())
        {
            try
            {
                DataSet ds = objConnect.getMedicarelastPayment(companyid, reporttype, fromdate, todate, reportid, userid);
                // DataSet ds = objConnect.getAverageDaysToBill("", 1, "12/25/2016", "12/25/2016", 1, 1);
                if (ds != null)
                {
                    if (ds.Tables.Count > 0)
                    {

                        convertedstring = (reporttype == 0) ? JsonConvert.SerializeObject(ds) : FetchChartJSON(ds);
                        //  convertedstring = FetchChartJSON(ds);
                    }
                    else
                        convertedstring = "{\"errortag\": \"Some Exception Occured\"}";
                }
                else
                    convertedstring = "{\"errortag\": \"Some Exception Occured\"}";
            }

            catch (Exception ex)
            {
                convertedstring = "{\"errortag\": \"Some Exception Occured\"}";
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
        }
        else if (Servicetype.Trim().ToLower() == ("getAverageCashCollectedPerTrip").ToLower())
        {
            try
            {
                DataSet ds = objConnect.getAverageCashCollectedPerTrip(companyid, reporttype, fromdate, todate, reportid, userid);
                // DataSet ds = objConnect.getAverageDaysToBill("", 1, "12/25/2016", "12/25/2016", 1, 1);
                if (ds != null)
                {
                    if (ds.Tables.Count > 0)
                    {

                        convertedstring = (reporttype == 0) ? JsonConvert.SerializeObject(ds) : FetchChartJSON(ds);
                        //  convertedstring = FetchChartJSON(ds);
                    }
                    else
                        convertedstring = "{\"errortag\": \"Some Exception Occured\"}";
                }
                else
                    convertedstring = "{\"errortag\": \"Some Exception Occured\"}";
            }

            catch (Exception ex)
            {
                convertedstring = "{\"errortag\": \"Some Exception Occured\"}";
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
        }
        else if (Servicetype.Trim().ToLower() == ("getVolumeMonthlyTrending").ToLower())
        {
            try
            {
                DataSet ds = objConnect.getVolumeMonthlyTrending(companyid, reporttype, fromdate, todate, reportid, userid);
                // DataSet ds = objConnect.getAverageDaysToBill("", 1, "12/25/2016", "12/25/2016", 1, 1);
                if (ds != null)
                {
                    if (ds.Tables.Count > 0)
                    {

                        convertedstring = (reporttype == 0) ? JsonConvert.SerializeObject(ds) : FetchChartJSON(ds);
                        //  convertedstring = FetchChartJSON(ds);
                    }
                    else
                        convertedstring = "{\"errortag\": \"Some Exception Occured\"}";
                }
                else
                    convertedstring = "{\"errortag\": \"Some Exception Occured\"}";
            }

            catch (Exception ex)
            {
                convertedstring = "{\"errortag\": \"Some Exception Occured\"}";
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
        }
        else if (Servicetype.Trim().ToLower() == ("getReports").ToLower())
        {
            try
            {
                DataSet ds = objConnect.getReports(userid);
                // DataSet ds = objConnect.getAverageDaysToBill("", 1, "12/25/2016", "12/25/2016", 1, 1);
                if (ds != null)
                {
                    if (ds.Tables.Count > 0)
                    {

                        convertedstring = JsonConvert.SerializeObject(ds);
                        //  convertedstring = FetchChartJSON(ds);
                    }
                    else
                        convertedstring = "{\"errortag\": \"Some Exception Occured\"}";
                }
                else
                    convertedstring = "{\"errortag\": \"Some Exception Occured\"}";
            }

            catch (Exception ex)
            {
                convertedstring = "{\"errortag\": \"Some Exception Occured\"}";
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
        }

        Response.Write(convertedstring);
        Response.Flush();
        HttpContext.Current.ApplicationInstance.CompleteRequest();
    }


    public static string getNodeChildren(string ParentId, List<TreeNodes> lstTree)
    {
        var child = lstTree.Where(e => e.parentId == ParentId).ToList();
        string StrJson = "";
        foreach (var tree in child)
        {
            if (StrJson == "")
            {
                StrJson = "\"children\":[";
            }
            else
            {
                StrJson += ",";
            }
            StrJson += "{";
            StrJson += "\"id\":\"" + tree.id + "\",";
            StrJson += "\"value\":\"" + tree.text.Replace("\\", "\\\\").Replace("\"", "\\\"") + "\",";
            StrJson += "\"text\":\"" + tree.text.Replace("\\", "\\\\").Replace("\"", "\\\"") + "\",";

            //if (lstTree.Where(w => w.parentId == tree.parentId).Count() == 0)
            //{
            //if (tree.ischeck == 1)
            //{
            StrJson += "\"open\":true,";
            StrJson += "\"checked\":true";
            //}
            //}
            StrJson += ((lstTree.Where(w => w.parentId == tree.id.ToString()).Count() > 0) ? "," + getNodeChildren(tree.id.ToString(), lstTree) : "");



            StrJson += "}";
        }
        if (StrJson != "")
            StrJson += "]";
        return StrJson;
    }


    public string FetchChartJSON(DataSet dst)
    {
        List<string> st_Labels = new List<string>();
        List<string> st_Series = new List<string>();

        DataView view = new DataView(dst.Tables[0]);
        DataTable distinctValues = view.ToTable(true, "series");
        DataRow[] Row_Distinct = null;

        Row_Distinct = distinctValues.Select();
        st_Series = Enumerable.Select(Row_Distinct.AsEnumerable(), vendor => vendor["series"].ToString()).ToList();

        /*  for (int j = 0; j < distinctValues.Rows.Count; j++)
          {
              string str = distinctValues.Rows[j]["series"].ToString();
              st_Series.Add(str);
          }*/

        DataRow[] Rows_Datadt = null;
        DataRow[] Data_Dt = null;
        ChartData p = new ChartData();
        List<List<string>> data_j = new List<List<string>>();

        if (st_Series.Count != 0)
        {
            Rows_Datadt = dst.Tables[0].Select("[series]='" + st_Series[0].ToString() + "'");

            for (int i = 0; i < st_Series.Count; i++)
            {



                Data_Dt = dst.Tables[0].Select("[series]='" + st_Series[i].ToString().Replace("'", "''") + "'");
                //Data_Dt = dst.Tables[0].Select("[series]='" + st_Series[i].ToString() + "'");                

                List<string> st_Data = new List<string>();

                for (int j = 0; j < Data_Dt.Length; j++)
                {
                    string s = ((Data_Dt[j]["Value"] == null || Data_Dt[j]["Value"].ToString().Trim() == "") ? null : Data_Dt[j]["Value"].ToString());
                    //string s = ( Data_Dt[j]["value"] == null) ? null : Data_Dt[j]["value"].ToString();
                    st_Data.Add(s);


                }
                data_j.Add(st_Data);
                p.data = data_j;


            }
        }

        string Database_Error = "";
        if (Rows_Datadt != null)
        {
            st_Labels = Enumerable.Select(Rows_Datadt.AsEnumerable(), vendor => vendor["labels"].ToString()).ToList();
            Database_Error = Enumerable.Select(Rows_Datadt.AsEnumerable(), vendor => vendor["Database_Error"].ToString()).FirstOrDefault();
        }

        p.labels = st_Labels;
        p.series = st_Series;
        p.Database_Error = Database_Error;
        if (dst.Tables.Count > 1 && dst.Tables[1].Columns.Count == 1)
            p.TotalRevenue = dst.Tables[1].Rows[0][0].ToString();
        else
            if (dst.Tables.Count > 2 && dst.Tables[2].Columns.Count == 1)
                p.TotalRevenue = dst.Tables[2].Rows[0][0].ToString();
            else
                p.TotalRevenue = "";

        if (dst.Tables.Count > 1 && dst.Tables[1].Columns.Count > 1)
        {
            p.ReportTitle = dst.Tables[1].Rows[0]["ReportTitle"].ToString();
            p.XAxisLabel = dst.Tables[1].Rows[0]["XAxisLabel"].ToString();
            p.YAxisLabel = dst.Tables[1].Rows[0]["YAxisLabel"].ToString();
            p.DateRange = dst.Tables[1].Rows[0]["DateRange"].ToString();
            p.ChartType = dst.Tables[1].Rows[0]["ChartType"].ToString();
            p.ToolTipFormat = dst.Tables[1].Rows[0]["ToolTipFormat"].ToString();
        }
        else if (dst.Tables.Count > 1 && dst.Tables[2].Columns.Count > 1)
        {
            p.ReportTitle = dst.Tables[2].Rows[0]["ReportTitle"].ToString();
            p.XAxisLabel = dst.Tables[2].Rows[0]["XAxisLabel"].ToString();
            p.YAxisLabel = dst.Tables[2].Rows[0]["YAxisLabel"].ToString();
            p.DateRange = dst.Tables[2].Rows[0]["DateRange"].ToString();
            p.ChartType = dst.Tables[2].Rows[0]["ChartType"].ToString();
            p.ToolTipFormat = dst.Tables[2].Rows[0]["ToolTipFormat"].ToString();
        }
        else
        {
            p.ReportTitle = "";
            p.XAxisLabel = "";
            p.YAxisLabel = "";
            p.DateRange = "";
            p.ChartType = "";
            p.ToolTipFormat = "";
        }

        

        //string returnJSON = "";
        //if (p.ChartType.ToLower() == "pie")
        //    returnJSON = JsonConvert.SerializeObject(p).Replace("[[", "[").Replace("]]", "]");
        //else

        //    returnJSON = JsonConvert.SerializeObject(p);
        //return returnJSON;
        //return JsonConvert.SerializeObject(p).Replace("[[", "[").Replace("]]", "]");
        return (p.ChartType.ToLower() != "pie") ? JsonConvert.SerializeObject(p) : JsonConvert.SerializeObject(p).Replace("[[", "[").Replace("]]", "]");
    }


    public class ChartData
    {

        public List<string> labels { get; set; }
        public List<string> series { get; set; }
        public List<List<string>> data { get; set; }
        public string TotalRevenue { get; set; }
        public string ReportTitle { get; set; }
        public string XAxisLabel { get; set; }
        public string YAxisLabel { get; set; }
        public string DateRange { get; set; }
        public string ChartType { get; set; }
        public string ToolTipFormat { get; set; }
        public string Database_Error { get; set; }
    }
}


public class TreeNodes
{
    public string text { get; set; }
    public string value { get; set; }
    public int id { get; set; }

    public bool open { get; set; }
    public bool ischecked { get; set; }
    public string parentId { get; set; }
}