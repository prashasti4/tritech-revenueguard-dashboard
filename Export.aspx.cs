﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.xml;
using iTextSharp.text.pdf;
using System.Xml.XPath;
using System.Xml.Xsl;
using System.Data;
using System.ComponentModel;
using System.Web.UI.HtmlControls;
using System.Collections;
using System.Text;

using ClosedXML.Excel;
using System.Net.Mail;
using System.Net;
using System.Text.RegularExpressions;
using System.Net.Mime;
using Elmah;
using System.Data.SqlClient;
using Microsoft.Win32;
using Newtonsoft.Json;
using System.Timers;
using System.Security.Cryptography;


public partial class Export : System.Web.UI.Page
{
    //private void SendEmail(string errors, string subject, string emailids)
    //{
       
        
    //        System.IO.StringWriter tw = new System.IO.StringWriter();
    //        MailMessage mm = new MailMessage();
    //        mm.From = new MailAddress(("notify@altamm.com").Trim(), ("notify@altamm.com"));
    //        string[] mailto = ("arindom@viriminfotech.com").Split(',');
    //        if (emailids == "")
    //        {
    //            for (int i = 0; i < mailto.Length; i++)
    //            {
    //                if (mailto[i] != "")
    //                {

    //                    mm.To.Add(new MailAddress(mailto[i].ToString().Trim()));
    //                }
    //            }
    //        }
    //        else
    //        {
    //            string[] mailcc = emailids.Split(',');
    //            for (int i = 0; i < mailcc.Length; i++)
    //            {
    //                if (mailcc[i] != "")
    //                {

    //                    mm.To.Add(new MailAddress(mailcc[i].ToString().Trim()));
    //                }
    //            }

    //            for (int i = 0; i < mailto.Length; i++)
    //            {
    //                if (mailto[i] != "")
    //                {

    //                    mm.Bcc.Add(new MailAddress(mailto[i].ToString().Trim()));
    //                }
    //            }

    //        }





    //        string html = errors; //sb.ToString();



    //        AlternateView avHtml = AlternateView.CreateAlternateViewFromString
    //     (html, null, MediaTypeNames.Text.Html);

            


    //        mm.Subject = subject;// System.Configuration.ConfigurationSettings.AppSettings["subject"].ToString();
    //        mm.Body = html;
    //        mm.IsBodyHtml = true;
    //        mm.AlternateViews.Add(avHtml);

    //        SmtpClient smtp = new SmtpClient();
    //        smtp.Host = "mail.altamm.com";
    //        smtp.EnableSsl = false;
    //        NetworkCredential NetworkCred = new NetworkCredential();
    //        NetworkCred.UserName = "notify@altamm.com";
    //        NetworkCred.Password = "1Gl0b@L!2016";
    //        smtp.UseDefaultCredentials = true;
    //        smtp.Credentials = NetworkCred;
    //        smtp.Port = 587;
    //        smtp.Send(mm);


       


    //}




    protected void Page_Load(object sender, EventArgs e)
    {
        //string a = Request.Form.ToString();
        //string error = "URL :       Authority : "+Request.Url.Authority.ToString()+" Absolute URL : "+ Request.Url.IsAbsoluteUri + "   Host : "+ Request.Url.Host+Environment.NewLine;
        //error = error + "URL Referrer :       Authority : " + Request.UrlReferrer.Authority.ToString() + " Absolute URL : " + Request.UrlReferrer.IsAbsoluteUri + "   Host : " + Request.UrlReferrer.Host;
        //SendEmail(error, "test", "");

        //string[] AuthenticateDomain = System.Configuration.ConfigurationSettings.AppSettings["Domain"].ToString().Split(',');
        //string currentDomain = Request.UrlReferrer.Host;
        //int avail = 0;
        //for (int i = 0; i < AuthenticateDomain.Length; i++)
        //{
        //    if (currentDomain.ToLower().Contains(AuthenticateDomain[i].ToString().ToLower()))
        //    {
        //        avail = 1;
        //        break;
        //    }
        //}

       // if (!string.IsNullOrEmpty(Request["Servicetype"]) && avail == 1)
        if (!string.IsNullOrEmpty(Request["Servicetype"]))
        {
            DBConnect objConnect = new DBConnect();
            string convertedstring = string.Empty;

            string Servicetype=(!string.IsNullOrEmpty(Request["Servicetype"])) ? ((Request["Servicetype"].ToString().ToLower() != "not available") ? Request["Servicetype"] : null) : null;
            string companyid = ((Request["companyid"]!=null)) ? ((Request["companyid"].ToString().ToLower() != "not available") ? Request["companyid"] : null) : null;
            int reporttype = (!string.IsNullOrEmpty(Request["reporttype"])) ? ((Request["reporttype"].ToString().ToLower() != "not available") ? Convert.ToInt32(Request["reporttype"]) : 0) : 0;
            string fromdate = ((Request["fromdate"] != null)) ? ((Request["fromdate"].ToString().ToLower() != "not available") ? Request["fromdate"] : null) : null;
            string todate = ((Request["todate"] != null)) ? ((Request["todate"].ToString().ToLower() != "not available") ? Request["todate"] : null) : null;
            int reportid = (!string.IsNullOrEmpty(Request["reportid"])) ? ((Request["reportid"].ToString().ToLower() != "not available") ? Convert.ToInt32(Request["reportid"]) : 0) : 0;
            int userid = (!string.IsNullOrEmpty(Request["userid"])) ? ((Request["userid"].ToString().ToLower() != "not available") ? Convert.ToInt32(Request["userid"]) : 0) : 0;
            string avgormax = ((Request["avgormax"] != null)) ? ((Request["avgormax"].ToString().ToLower() != "not available") ? Request["avgormax"] : null) : null;
            string insuranceid = ((Request["insuranceid"] != null)) ? ((Request["insuranceid"].ToString().ToLower() != "not available") ? Request["insuranceid"] : null) : null;
            string numberofdays = ((Request["numberofdays"] != null)) ? ((Request["numberofdays"].ToString().ToLower() != "not available") ? Request["numberofdays"] : null) : null;
            string InsuranceidDisplayName = (!string.IsNullOrEmpty(Request["InsuranceidDisplayName"])) ? ((Request["InsuranceidDisplayName"].ToString().ToLower() != "not available") ? Request["InsuranceidDisplayName"] : "") : "";
            string CompanyidDisplayName = (!string.IsNullOrEmpty(Request["CompanyidDisplayName"])) ? ((Request["CompanyidDisplayName"].ToString().ToLower() != "not available") ? Request["CompanyidDisplayName"] : "") : "";
            string DateDisplayName = (!string.IsNullOrEmpty(Request["DateDisplayName"])) ? ((Request["DateDisplayName"].ToString().ToLower() != "not available") ? Request["DateDisplayName"] : "") : "";
            string NumberOfDaysDisplayName = (!string.IsNullOrEmpty(Request["NumberOfDaysDisplayName"])) ? ((Request["NumberOfDaysDisplayName"].ToString().ToLower() != "not available") ? Request["NumberOfDaysDisplayName"] : "") : "";
            string AvgormaxDisplayName = (!string.IsNullOrEmpty(Request["AvgormaxDisplayName"])) ? ((Request["AvgormaxDisplayName"].ToString().ToLower() != "not available") ? Request["AvgormaxDisplayName"] : "") : "";

            //string InsuranceidDisplayName = (Request["InsuranceidDisplayName"] != null) ? Request["InsuranceidDisplayName"] : "";
            //string CompanyidDisplayName = (Request["CompanyidDisplayName"] != null) ? Request["CompanyidDisplayName"] : "";
            //string DateDisplayName = (Request["DateDisplayName"] != null) ? Request["DateDisplayName"] : "";
            //string NumberOfDaysDisplayName = (Request["NumberOfDaysDisplayName"] != null) ? Request["NumberOfDaysDisplayName"] : "";
            //string AvgormaxDisplayName = (Request["AvgormaxDisplayName"] != null) ? Request["AvgormaxDisplayName"] : "";
            int isreport = (!string.IsNullOrEmpty(Request["isreport"]) ? Convert.ToInt32(Request["isreport"].ToString()) : 0);
            string ReportName = "";
            int BoldCount = 0;
           // int isreport = 0;

            if (Servicetype!= null)
            {
                DataSet ds = objConnect.GetReportName(companyid, reporttype, fromdate, todate, reportid, userid, avgormax, numberofdays, insuranceid);
                if (ds != null)
                {
                    if (ds.Tables.Count > 0)
                    {

                        ReportName = ds.Tables[0].Rows[0]["ReportTitle"].ToString();
                        BoldCount = Convert.ToInt32(ds.Tables[0].Rows[0]["BoldCount"].ToString());

                    }
                }
            }


            if (Servicetype.ToString().Trim().ToLower() == ("getAverageDaysToBill").ToLower())
            {
                try
                {
                    DataSet ds = objConnect.getAverageDaysToBill(companyid, reporttype, fromdate, todate, reportid, userid,isreport);
                    // DataSet ds = objConnect.getAverageDaysToBill("", 1, "12/25/2016", "12/25/2016", 1, 1);
                    if (ds != null)
                    {
                        if (ds.Tables.Count > 0)
                        {

                            ExportToExcel(ReportName,BoldCount, ds, fromdate, todate, (companyid != null) ? objConnect.fetchCommaSeperatedCompanyName(companyid) : companyid, (insuranceid != null) ? objConnect.fetchCommaSeperatedInsuranceName(insuranceid) : insuranceid, numberofdays, avgormax, DateDisplayName, CompanyidDisplayName, InsuranceidDisplayName, NumberOfDaysDisplayName, AvgormaxDisplayName);
                        }
                    }
                }

                catch (Exception ex)
                {
                  //  ErrorSignal.FromCurrentContext().Raise(ex);
                }
            }
            else if (Servicetype.Trim().ToLower() == ("GetReportName").ToLower())
            {
                try
                {
                    DataSet ds = objConnect.GetReportName(companyid, reporttype, fromdate, todate, reportid, userid, avgormax, numberofdays, insuranceid);
                    if (ds != null)
                    {
                        if (ds.Tables.Count > 0)
                        {

                            convertedstring = JsonConvert.SerializeObject(ds);

                        }
                    }
                }

                catch (Exception ex)
                {
                    ErrorSignal.FromCurrentContext().Raise(ex);
                }
            }
            else if (Servicetype.ToString().Trim().ToLower() == ("getClaimsWaitingToBeBilled").ToLower())
            {
                try
                {
                    DataSet ds = objConnect.getClaimsWaitingToBeBilled(companyid, reporttype, fromdate, todate,reportid, userid,isreport);
                    // DataSet ds = objConnect.getAverageDaysToBill("", 1, "12/25/2016", "12/25/2016", 1, 1);
                    if (ds != null)
                    {
                        if (ds.Tables.Count > 0)
                        {

                            ExportToExcel(ReportName,BoldCount, ds, fromdate, todate, (companyid != null) ? objConnect.fetchCommaSeperatedCompanyName(companyid) : companyid, (insuranceid != null) ? objConnect.fetchCommaSeperatedInsuranceName(insuranceid) : insuranceid, numberofdays, avgormax, DateDisplayName, CompanyidDisplayName, InsuranceidDisplayName, NumberOfDaysDisplayName, AvgormaxDisplayName);
                        }
                    }
                }

                catch (Exception ex)
                {
                    //  ErrorSignal.FromCurrentContext().Raise(ex);
                }
            }
            else if (Servicetype.ToString().Trim().ToLower() == ("GetDateOfServiceVsDateOfUpload").ToLower())
            {
                try
                {
                    DataSet ds = objConnect.GetDateOfServiceVsDateOfUpload(companyid, Convert.ToInt32(reporttype), fromdate, todate, Convert.ToInt32(reportid), Convert.ToInt32(userid), avgormax);
                    // DataSet ds = objConnect.getAverageDaysToBill("", 1, "12/25/2016", "12/25/2016", 1, 1);
                    if (ds != null)
                    {
                        if (ds.Tables.Count > 0)
                        {

                            ExportToExcel(ReportName,BoldCount, ds, fromdate, todate, (companyid != null) ? objConnect.fetchCommaSeperatedCompanyName(companyid) : companyid, (insuranceid != null) ? objConnect.fetchCommaSeperatedInsuranceName(insuranceid) : insuranceid, numberofdays, avgormax, DateDisplayName, CompanyidDisplayName, InsuranceidDisplayName, NumberOfDaysDisplayName, AvgormaxDisplayName);
                        }
                    }
                }

                catch (Exception ex)
                {
                    //  ErrorSignal.FromCurrentContext().Raise(ex);
                }
            }
            else if (Servicetype.ToString().Trim().ToLower() == ("getTotalOutstandingClaims").ToLower())
            {
                try
                {
                    DataSet ds = objConnect.getTotalOutstandingClaims(companyid, Convert.ToInt32(reporttype), fromdate, todate, Convert.ToInt32(reportid), Convert.ToInt32(userid), numberofdays, insuranceid);
                    // DataSet ds = objConnect.getAverageDaysToBill("", 1, "12/25/2016", "12/25/2016", 1, 1);
                    if (ds != null)
                    {
                        if (ds.Tables.Count > 0)
                        {

                            ExportToExcel(ReportName,BoldCount, ds, fromdate, todate, (companyid != null) ? objConnect.fetchCommaSeperatedCompanyName(companyid) : companyid, (insuranceid != null) ? objConnect.fetchCommaSeperatedInsuranceName(insuranceid) : insuranceid, numberofdays, avgormax, DateDisplayName, CompanyidDisplayName, InsuranceidDisplayName, NumberOfDaysDisplayName, AvgormaxDisplayName);
                        }
                    }
                }

                catch (Exception ex)
                {
                    //  ErrorSignal.FromCurrentContext().Raise(ex);
                }
            }
            else if (Servicetype.ToString().Trim().ToLower() == ("getPercentOutstandingClaims").ToLower())
            {
                try
                {
                    DataSet ds = objConnect.getPercentOutstandingClaims(companyid, Convert.ToInt32(reporttype), fromdate, todate, Convert.ToInt32(reportid), Convert.ToInt32(userid), numberofdays, insuranceid,isreport);
                    // DataSet ds = objConnect.getAverageDaysToBill("", 1, "12/25/2016", "12/25/2016", 1, 1);
                    if (ds != null)
                    {
                        if (ds.Tables.Count > 0)
                        {

                            ExportToExcel(ReportName,BoldCount, ds, fromdate, todate, (companyid != null) ? objConnect.fetchCommaSeperatedCompanyName(companyid) : companyid, (insuranceid != null) ? objConnect.fetchCommaSeperatedInsuranceName(insuranceid) : insuranceid, numberofdays, avgormax, DateDisplayName, CompanyidDisplayName, InsuranceidDisplayName, NumberOfDaysDisplayName, AvgormaxDisplayName);
                        }
                    }
                }

                catch (Exception ex)
                {
                    //  ErrorSignal.FromCurrentContext().Raise(ex);
                }
            }
            else if (Servicetype.ToString().Trim().ToLower() == ("getTotalClaimsWaitingToBeBilledVsTotalClaimsBilled").ToLower())
            {
                try
                {
                    DataSet ds = objConnect.getTotalClaimsWaitingToBeBilledVsTotalClaimsBilled(companyid, Convert.ToInt32(reporttype), fromdate, todate, Convert.ToInt32(reportid), Convert.ToInt32(userid), numberofdays, insuranceid, isreport);
                    // DataSet ds = objConnect.getAverageDaysToBill("", 1, "12/25/2016", "12/25/2016", 1, 1);
                    if (ds != null)
                    {
                        if (ds.Tables.Count > 0)
                        {

                            ExportToExcel(ReportName,BoldCount, ds, fromdate, todate, (companyid != null) ? objConnect.fetchCommaSeperatedCompanyName(companyid) : companyid, (insuranceid != null) ? objConnect.fetchCommaSeperatedInsuranceName(insuranceid) : insuranceid, numberofdays, avgormax, DateDisplayName, CompanyidDisplayName, InsuranceidDisplayName, NumberOfDaysDisplayName, AvgormaxDisplayName);
                        }
                    }
                }

                catch (Exception ex)
                {
                    //  ErrorSignal.FromCurrentContext().Raise(ex);
                }
            }
            else if (Servicetype.ToString().Trim().ToLower() == ("getTotalHoursWorked").ToLower())
            {
                try
                {
                    DataSet ds = objConnect.getTotalHoursWorked(companyid, Convert.ToInt32(reporttype), fromdate, todate, Convert.ToInt32(reportid), Convert.ToInt32(userid), numberofdays, insuranceid, isreport);
                    // DataSet ds = objConnect.getAverageDaysToBill("", 1, "12/25/2016", "12/25/2016", 1, 1);
                    if (ds != null)
                    {
                        if (ds.Tables.Count > 0)
                        {

                            ExportToExcel(ReportName,BoldCount, ds, fromdate, todate, (companyid != null) ? objConnect.fetchCommaSeperatedCompanyName(companyid) : companyid, (insuranceid != null) ? objConnect.fetchCommaSeperatedInsuranceName(insuranceid) : insuranceid, numberofdays, avgormax, DateDisplayName, CompanyidDisplayName, InsuranceidDisplayName, NumberOfDaysDisplayName, AvgormaxDisplayName);
                        }
                    }
                }

                catch (Exception ex)
                {
                    //  ErrorSignal.FromCurrentContext().Raise(ex);
                }
            }
            else if (Servicetype.ToString().Trim().ToLower() == ("getClaimsBilledPerClient").ToLower())
            {
                try
                {
                    DataSet ds = objConnect.getClaimsBilledPerClient(companyid, Convert.ToInt32(reporttype), fromdate, todate, Convert.ToInt32(reportid), Convert.ToInt32(userid));
                    // DataSet ds = objConnect.getAverageDaysToBill("", 1, "12/25/2016", "12/25/2016", 1, 1);
                    if (ds != null)
                    {
                        if (ds.Tables.Count > 0)
                        {

                            ExportToExcel(ReportName,BoldCount, ds, fromdate, todate, (companyid != null) ? objConnect.fetchCommaSeperatedCompanyName(companyid) : companyid, (insuranceid != null) ? objConnect.fetchCommaSeperatedInsuranceName(insuranceid) : insuranceid, numberofdays, avgormax, DateDisplayName, CompanyidDisplayName, InsuranceidDisplayName, NumberOfDaysDisplayName, AvgormaxDisplayName);
                        }
                    }
                }

                catch (Exception ex)
                {
                    //  ErrorSignal.FromCurrentContext().Raise(ex);
                }
            }
            else if (Servicetype.ToString().Trim().ToLower() == ("getAverageDaysOutstandingTrending").ToLower())
            {
                try
                {
                    DataSet ds = objConnect.getAverageDaysOutstandingTrending(companyid, Convert.ToInt32(reporttype), fromdate, todate, Convert.ToInt32(reportid), Convert.ToInt32(userid));
                    // DataSet ds = objConnect.getAverageDaysToBill("", 1, "12/25/2016", "12/25/2016", 1, 1);
                    if (ds != null)
                    {
                        if (ds.Tables.Count > 0)
                        {

                            ExportToExcel(ReportName,BoldCount, ds, fromdate, todate, (companyid != null) ? objConnect.fetchCommaSeperatedCompanyName(companyid) : companyid, (insuranceid != null) ? objConnect.fetchCommaSeperatedInsuranceName(insuranceid) : insuranceid, numberofdays, avgormax, DateDisplayName, CompanyidDisplayName, InsuranceidDisplayName, NumberOfDaysDisplayName, AvgormaxDisplayName);
                        }
                    }
                }

                catch (Exception ex)
                {
                    //  ErrorSignal.FromCurrentContext().Raise(ex);
                }
            }
            else if (Servicetype.ToString().Trim().ToLower() == ("getMedicarelastPayment").ToLower())
            {
                try
                {
                    DataSet ds = objConnect.getMedicarelastPayment(companyid, Convert.ToInt32(reporttype), fromdate, todate, Convert.ToInt32(reportid), Convert.ToInt32(userid));
                    // DataSet ds = objConnect.getAverageDaysToBill("", 1, "12/25/2016", "12/25/2016", 1, 1);
                    if (ds != null)
                    {
                        if (ds.Tables.Count > 0)
                        {

                            ExportToExcel(ReportName,BoldCount, ds, fromdate, todate, (companyid != null) ? objConnect.fetchCommaSeperatedCompanyName(companyid) : companyid, (insuranceid != null) ? objConnect.fetchCommaSeperatedInsuranceName(insuranceid) : insuranceid, numberofdays, avgormax, DateDisplayName, CompanyidDisplayName, InsuranceidDisplayName, NumberOfDaysDisplayName, AvgormaxDisplayName);
                        }
                    }
                }

                catch (Exception ex)
                {
                    //  ErrorSignal.FromCurrentContext().Raise(ex);
                }
            }
            else if (Servicetype.ToString().Trim().ToLower() == ("getAverageCashCollectedPerTrip").ToLower())
            {
                try
                {
                    DataSet ds = objConnect.getAverageCashCollectedPerTrip(companyid, Convert.ToInt32(reporttype), fromdate, todate, Convert.ToInt32(reportid), Convert.ToInt32(userid));
                    // DataSet ds = objConnect.getAverageDaysToBill("", 1, "12/25/2016", "12/25/2016", 1, 1);
                    if (ds != null)
                    {
                        if (ds.Tables.Count > 0)
                        {

                            ExportToExcel(ReportName,BoldCount, ds, fromdate, todate, (companyid != null) ? objConnect.fetchCommaSeperatedCompanyName(companyid) : companyid, (insuranceid != null) ? objConnect.fetchCommaSeperatedInsuranceName(insuranceid) : insuranceid, numberofdays, avgormax, DateDisplayName, CompanyidDisplayName, InsuranceidDisplayName, NumberOfDaysDisplayName, AvgormaxDisplayName);
                        }
                    }
                }

                catch (Exception ex)
                {
                    //  ErrorSignal.FromCurrentContext().Raise(ex);
                }
            }
            else if (Servicetype.ToString().Trim().ToLower() == ("getVolumeMonthlyTrending").ToLower())
            {
                try
                {
                    DataSet ds = objConnect.getVolumeMonthlyTrending(companyid, Convert.ToInt32(reporttype), fromdate, todate, Convert.ToInt32(reportid), Convert.ToInt32(userid));
                    // DataSet ds = objConnect.getAverageDaysToBill("", 1, "12/25/2016", "12/25/2016", 1, 1);
                    if (ds != null)
                    {
                        if (ds.Tables.Count > 0)
                        {

                            ExportToExcel(ReportName,BoldCount, ds, fromdate, todate, (companyid != null) ? objConnect.fetchCommaSeperatedCompanyName(companyid) : companyid, (insuranceid != null) ? objConnect.fetchCommaSeperatedInsuranceName(insuranceid) : insuranceid, numberofdays, avgormax, DateDisplayName, CompanyidDisplayName, InsuranceidDisplayName, NumberOfDaysDisplayName, AvgormaxDisplayName);
                        }
                    }
                }

                catch (Exception ex)
                {
                    //  ErrorSignal.FromCurrentContext().Raise(ex);
                }
            }
        }
    }


    public DataSet fetch_filter_data(string fromdate, string todate, string companyname, string insurancename, string noofdays, string avgofmax, string DateDisplayName, string CompanyidDisplayName, string InsuranceidDisplayName, string NumberOfDaysDisplayName, string AvgormaxDisplayName)
    {
        DataSet dst_filter = new DataSet();
        DataTable dt_filter = new DataTable();
    


        dt_filter.Columns.Add("name");
        dt_filter.Columns.Add("value");
        dt_filter.Columns.Add("value1");
        dt_filter.Columns.Add("value2");
        dt_filter.Columns.Add("value3");

       


        if (fromdate != null && todate != null)
            dt_filter.Rows.Add(DateDisplayName, fromdate + " - " + todate, "", "", "");
        else if (fromdate == null && todate != null)
            dt_filter.Rows.Add(DateDisplayName, "As of " + todate, "", "", "");

        if (CompanyidDisplayName != null && CompanyidDisplayName!="")
            dt_filter.Rows.Add(CompanyidDisplayName, companyname, "", "", "");
        if (InsuranceidDisplayName != null && InsuranceidDisplayName!="")
            dt_filter.Rows.Add(InsuranceidDisplayName, insurancename, "", "", "");
        if (NumberOfDaysDisplayName != null && NumberOfDaysDisplayName!="")
            dt_filter.Rows.Add(NumberOfDaysDisplayName, noofdays, "", "", "");
        if (AvgormaxDisplayName != null && AvgormaxDisplayName!="")
            dt_filter.Rows.Add(AvgormaxDisplayName, avgofmax, "", "", "");

       
        dst_filter.Tables.Add(dt_filter);
        return dst_filter;
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }


    private int ExportToExcel(string FileName,int BoldCount, DataSet dst, string fromdate, string todate, string companyname, string insurancename, string noofdays, string avgofmax, string DateDisplayName, string CompanyidDisplayName, string InsuranceidDisplayName, string NumberOfDaysDisplayName, string AvgormaxDisplayName)
    {
        try
        {
            for (int i = 0; i < dst.Tables.Count; i++)
            {
                for (int j = 0; j < dst.Tables[i].Columns.Count; j++)
                {
                    if (dst.Tables[i].Columns[j].ColumnName.ToLower().Contains("hidden_"))
                    {
                        dst.Tables[i].Columns.Remove(dst.Tables[i].Columns[j].ColumnName);
                    }
                    if (dst.Tables[i].Columns[j].ColumnName.ToLower().Contains("database_error"))
                    {
                        dst.Tables[i].Columns.Remove(dst.Tables[i].Columns[j].ColumnName);
                    }
                }
            }


         
           // string file_name1 = FileName.Split('|')[1].ToString().Replace(" ", "_") + DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff").Replace("/", "_").Replace(":", "_").Replace(" ", "_").Replace(".", "_");
            string file_name1 = FileName.Replace(" ", "_") + DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff").Replace("/", "_").Replace(":", "_").Replace(" ", "_").Replace(".", "_");
            string file_name = file_name1 + ".xlsx";
            System.IO.StringWriter tw = new System.IO.StringWriter();
            System.Web.UI.HtmlTextWriter hw = new System.Web.UI.HtmlTextWriter(tw);

           // int totalHeader = FileName.Split('|').Count();


            using (XLWorkbook workbook = new XLWorkbook())
            {
                try
                {
                    var worksheet = workbook.Worksheets.Add("Grid Report");
                   // worksheet.Tables.FirstOrDefault().ShowAutoFilter = false;


                    Font ColFont = FontFactory.GetFont(FontFactory.HELVETICA, 22, Font.BOLD, new Color(9, 103, 123));
                    string report_header = FileName;

                    int rowIndex = 1;
                    DataSet dst_filter = new DataSet();
                    //for (int i = 2; i < totalHeader - 2; i++)
                    //{


                        dst_filter = fetch_filter_data(fromdate, todate,companyname, insurancename, noofdays, avgofmax,DateDisplayName, CompanyidDisplayName, InsuranceidDisplayName, NumberOfDaysDisplayName, AvgormaxDisplayName);
                        //  

                    //}

                    worksheet.Range("A" + (rowIndex).ToString() + ":" + FetchExcelColumnName(dst.Tables[0].Columns.Count) + (rowIndex).ToString()).Merge();
                    worksheet.Cell(rowIndex, 1).Value = report_header;

                    worksheet.Cell(rowIndex, 1).Style.Border.RightBorder = XLBorderStyleValues.Thin;
                    worksheet.Cell(rowIndex, 1).Style.Border.LeftBorder = XLBorderStyleValues.Thin;
                    worksheet.Cell(rowIndex, 1).Style.Border.TopBorder = XLBorderStyleValues.Thin;
                    worksheet.Cell(rowIndex, 1).Style.Border.BottomBorder = XLBorderStyleValues.Thin;

                    worksheet.Cell(rowIndex, 1).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Left);
                    worksheet.Cell(rowIndex, 1).Style.Font.Bold = true;
                    worksheet.Cell(rowIndex, 1).Style.Font.SetFontSize(22);
                    rowIndex = rowIndex + 1;

                    for (int filter_cnt = 0; filter_cnt < dst_filter.Tables[0].Rows.Count; filter_cnt++)
                    {
                        int columnNo = 1;
                        for (int filter_cnt1 = 0; filter_cnt1 < dst_filter.Tables[0].Columns.Count; filter_cnt1++)
                        {
                            if (filter_cnt1 == 1)
                                worksheet.Range("B" + (rowIndex).ToString() + ":" + FetchExcelColumnName(dst.Tables[0].Columns.Count) + (rowIndex).ToString()).Merge();
                            worksheet.Cell(rowIndex, columnNo).Value = dst_filter.Tables[0].Rows[filter_cnt][filter_cnt1].ToString();

                            worksheet.Cell(rowIndex, columnNo).Style.Border.RightBorder = XLBorderStyleValues.Thin;
                            worksheet.Cell(rowIndex, columnNo).Style.Border.LeftBorder = XLBorderStyleValues.Thin;
                            worksheet.Cell(rowIndex, columnNo).Style.Border.TopBorder = XLBorderStyleValues.Thin;
                            worksheet.Cell(rowIndex, columnNo).Style.Border.BottomBorder = XLBorderStyleValues.Thin;

                            worksheet.Cell(rowIndex, columnNo).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Left);


                            columnNo++;
                        }

                        rowIndex++;

                    }
                    rowIndex++;

                    int row = rowIndex+1;
                    for (int cnt = 0; cnt < dst.Tables.Count; cnt++)
                    {
                        //if (cnt == 0)
                        //    worksheet.Cell(rowIndex, 1).Value = dst.Tables[1].Rows[0]["Reportheading"].ToString();
                        //else if (cnt == 2)
                        //    worksheet.Cell(rowIndex, 1).Value = dst.Tables[3].Rows[0]["Reportheading"].ToString();
                        //else if (cnt == 4)
                        //    worksheet.Cell(rowIndex, 1).Value = dst.Tables[5].Rows[0]["Reportheading"].ToString();
                        //else if (cnt == 6)
                        //    worksheet.Cell(rowIndex, 1).Value = dst.Tables[7].Rows[0]["Reportheading"].ToString();
                        //worksheet.Cell(rowIndex, 1).Style.Font.Bold = true;
                        //worksheet.Cell(rowIndex, 1).Style.Font.SetFontSize(18);

                        //rowIndex++;



                        //if (totalHeader > 4)
                         //   rowIndex = rowIndex + 1;
                        //else
                        //    rowIndex = rowIndex + 2;
                        int colIndex = 0;
                        //for (int col = 1; col < dst.Tables[cnt].Columns.Count + 1; col++)
                        //{
                        //    if (col == 1)
                        //        worksheet.Cell(rowIndex, col).Value = dst.Tables[cnt + 1].Rows[0]["Bottomheading.ToString();
                        //    else
                        //        worksheet.Cell(rowIndex, col).Value = dst.Tables[cnt].Columns[colIndex].ToString();
                        //    worksheet.Cell(rowIndex, col).Style.Border.RightBorder = XLBorderStyleValues.Thin;
                        //    worksheet.Cell(rowIndex, col).Style.Border.LeftBorder = XLBorderStyleValues.Thin;
                        //    worksheet.Cell(rowIndex, col).Style.Border.TopBorder = XLBorderStyleValues.Thin;
                        //    worksheet.Cell(rowIndex, col).Style.Border.BottomBorder = XLBorderStyleValues.Thin;
                        //    worksheet.Cell(rowIndex, col).Style.Font.Bold = true;
                        //    worksheet.Cell(rowIndex, col).Style.Alignment.ShrinkToFit = true;
                        //    worksheet.Cell(rowIndex, col).Style.Alignment.JustifyLastLine = true;
                        //    worksheet.Cell(rowIndex, col).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center);
                        //    if (col == 1)
                        //        worksheet.Cell(rowIndex, col).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Left);
                        //    else
                        //        worksheet.Cell(rowIndex, col).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right);
                        //    worksheet.Columns().AdjustToContents();
                        //    colIndex++;
                        //}




                        int column_No = 1;
                        for (int j = 0; j < dst.Tables[cnt].Columns.Count; j++)
                        {
                            try
                            {
                                //if (j == 0)
                                //    worksheet.Cell(row, column_No).Value = dst.Tables[cnt + 1].Rows[0]["Bottomheading.ToString();
                                //else
                                //string a = dst.Tables[cnt].Columns[j].ToString();
                                //worksheet.Cell(row, column_No).SetDataType(XLCellValues.Text);
                                //    worksheet.Cell(row, column_No).Value = dst.Tables[cnt].Columns[j].ToString();
                                //    worksheet.Cell(row, column_No).SetDataType(XLCellValues.Text);
                                    worksheet.Cell(row, column_No).SetValue<string>(dst.Tables[cnt].Columns[j].ToString().Replace("<br>",Environment.NewLine));
                                worksheet.Cell(row, column_No).Style.Border.RightBorder = XLBorderStyleValues.Thin;
                                worksheet.Cell(row, column_No).Style.Border.LeftBorder = XLBorderStyleValues.Thin;
                                worksheet.Cell(row, column_No).Style.Border.TopBorder = XLBorderStyleValues.Thin;
                                worksheet.Cell(row, column_No).Style.Border.BottomBorder = XLBorderStyleValues.Thin;
                                worksheet.Cell(row, column_No).Style.Font.Bold = true;
                                worksheet.Cell(row, column_No).Style.Alignment.ShrinkToFit = true;
                                worksheet.Cell(row, column_No).Style.Alignment.JustifyLastLine = true;
                                worksheet.Cell(row, column_No).Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center);

                                if (j == 0)
                                    worksheet.Cell(row, column_No).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Left);
                                else
                                    worksheet.Cell(row, column_No).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right);

                                column_No++;
                            }
                            catch (Exception ex)
                            {
                                string msg = ex.ToString();
                            }
                        }
                        row++;



                        for (int i = 0; i < dst.Tables[cnt].Rows.Count; i++)
                        {
                            int columnNo = 1;
                            for (int j = 0; j < dst.Tables[cnt].Columns.Count; j++)
                            {
                                try
                                {
                                    worksheet.Cell(row, columnNo).SetValue<string>(dst.Tables[cnt].Rows[i][j].ToString());
                                   // worksheet.Cell(row, columnNo).Value = dst.Tables[cnt].Rows[i][j].ToString();

                                    worksheet.Cell(row, columnNo).Style.Border.RightBorder = XLBorderStyleValues.Thin;
                                    worksheet.Cell(row, columnNo).Style.Border.LeftBorder = XLBorderStyleValues.Thin;
                                    worksheet.Cell(row, columnNo).Style.Border.TopBorder = XLBorderStyleValues.Thin;
                                    worksheet.Cell(row, columnNo).Style.Border.BottomBorder = XLBorderStyleValues.Thin;

                                    if (columnNo == 1)
                                        worksheet.Cell(row, columnNo).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Left);
                                    else
                                        worksheet.Cell(row, columnNo).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right);

                                    if (i >= (dst.Tables[cnt].Rows.Count - BoldCount))
                                        worksheet.Cell(row, columnNo).Style.Font.Bold = true;
                                    columnNo++;
                                }
                                catch (Exception ex)
                                {
                                    string msg = ex.ToString();
                                }
                            }
                            row++;
                        }
                        rowIndex = row + 1;
                        row = rowIndex + 2;
                        cnt++;
                    }
                    worksheet.Columns().AdjustToContents();

                   

                    Response.Clear();
                    Response.Buffer = true;
                    Response.Charset = "";
                    Response.ClearHeaders();
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.AddHeader("content-disposition", "attachment; filename=\"" + file_name + "\"");
                }
                catch (Exception ex)
                {
                    ErrorSignal.FromCurrentContext().Raise(ex);
                }

                
                    using (MemoryStream MyMemoryStream = new MemoryStream())
                    {
                        workbook.SaveAs(MyMemoryStream);
                        MyMemoryStream.WriteTo(Response.OutputStream);
                        Response.Flush();
                        Context.ApplicationInstance.CompleteRequest();
                        Response.End();

                    }
               
                


            }


        }
        catch (Exception ex)
        {
            //string vbCrLf = "\r\n";
            //Common.SaveTextToFile("------" + vbCrLf + ex.Message.ToString() + vbCrLf + ex.Source + vbCrLf + ex.StackTrace.ToString() + vbCrLf + ex.TargetSite.ToString() + vbCrLf + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString() + "------");

        }

        return 1;
    }



    public string FetchExcelColumnName(int columnNo)
    {
        string ColumnName = "";
        if (columnNo == 1)
            ColumnName = "A";
        else if (columnNo == 2)
            ColumnName = "B";
        else if (columnNo == 3)
            ColumnName = "C";
        else if (columnNo == 4)
            ColumnName = "D";
        else if (columnNo == 5)
            ColumnName = "E";
        else if (columnNo == 6)
            ColumnName = "F";
        else if (columnNo == 7)
            ColumnName = "G";
        else if (columnNo == 8)
            ColumnName = "H";
        else if (columnNo == 9)
            ColumnName = "I";
        else if (columnNo == 10)
            ColumnName = "J";
        else if (columnNo == 11)
            ColumnName = "K";
        else if (columnNo == 12)
            ColumnName = "L";
        else if (columnNo == 13)
            ColumnName = "M";
        else if (columnNo == 14)
            ColumnName = "N";
        else if (columnNo == 15)
            ColumnName = "O";
        else if (columnNo == 16)
            ColumnName = "P";
        else if (columnNo == 17)
            ColumnName = "Q";
        else if (columnNo == 18)
            ColumnName = "R";
        else if (columnNo == 19)
            ColumnName = "S";
        else if (columnNo == 20)
            ColumnName = "T";
        else if (columnNo == 21)
            ColumnName = "U";
        else if (columnNo == 22)
            ColumnName = "V";
        else if (columnNo == 23)
            ColumnName = "W";
        else if (columnNo == 24)
            ColumnName = "X";
        else if (columnNo == 25)
            ColumnName = "Y";
        else if (columnNo == 26)
            ColumnName = "Z";
        else if (columnNo == 27)
            ColumnName = "AA";
        else if (columnNo == 28)
            ColumnName = "AB";
        else if (columnNo == 29)
            ColumnName = "AC";
        else if (columnNo == 30)
            ColumnName = "AD";
        else if (columnNo == 31)
            ColumnName = "AE";
        else if (columnNo == 32)
            ColumnName = "AF";
        else if (columnNo == 33)
            ColumnName = "AG";
        else if (columnNo == 34)
            ColumnName = "AH";
        else if (columnNo == 35)
            ColumnName = "AI";
        else if (columnNo == 36)
            ColumnName = "AJ";
        else if (columnNo == 37)
            ColumnName = "AK";
        else if (columnNo == 38)
            ColumnName = "AL";
        else if (columnNo == 39)
            ColumnName = "AM";
        else if (columnNo == 40)
            ColumnName = "AN";
        else if (columnNo == 41)
            ColumnName = "AO";
        else if (columnNo == 42)
            ColumnName = "AP";
        else if (columnNo == 43)
            ColumnName = "AQ";
        else if (columnNo == 44)
            ColumnName = "AR";
        else if (columnNo == 45)
            ColumnName = "AS";
        else if (columnNo == 46)
            ColumnName = "AT";
        else if (columnNo == 47)
            ColumnName = "AU";

        return ColumnName;
    }


}