﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Security.Cryptography;
using System.IO;

/// <summary>
/// Summary description for AppearenceSettingRepo
/// </summary>
public class AppearenceSettingRepo
{
    public AppearenceSettingRepo()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    //Modified date 31 oct 2013
    public DataSet GetMiscSetting(string username)
    {
        DataSet ds = null;
        try
        {
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@username", SqlDbType.VarChar, 100);
            param[0].Value = username;
            ds = SqlHelper.ExecuteDataset(SqlHelper.GetConnectionString(), CommandType.StoredProcedure, "getsettings", param);
        }
        catch (Exception ex)
        {
            string vbCrLf = "\r\n";
          //  Common.SaveTextToFile("------" + vbCrLf + ex.Message.ToString() + vbCrLf + ex.Source + vbCrLf + ex.StackTrace.ToString() + vbCrLf + ex.TargetSite.ToString() + vbCrLf + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString() + "------");
            ds = null;
        }
        return ds;
    }

    public int InsertSettings(string inputxml)
    {
        SqlParameter[] param = new SqlParameter[2];
        param[0] = new SqlParameter("@xml", SqlDbType.VarChar, 1000000);
        param[0].Value = inputxml;
        param[1] = new SqlParameter("@result", SqlDbType.Int);
        param[1].Direction = ParameterDirection.Output;
        SqlHelper.ExecuteNonQuery(SqlHelper.GetConnectionString(), CommandType.StoredProcedure, "setsettings", param);
        return int.Parse(param[1].Value.ToString());
    }

    public DataSet getFacilitiesForSAutoID(string SAutoID, out string error)
    {
        DataSet ds = new DataSet();
        error = "error";
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@SAutoID", SqlDbType.VarChar, 200);
        param[0].Value = SAutoID;
        param[1] = new SqlParameter("@facilityid", SqlDbType.VarChar, 100000);
        param[1].Direction = ParameterDirection.Output;
        param[2] = new SqlParameter("@error", SqlDbType.VarChar, 100000);
        param[2].Direction = ParameterDirection.Output;


        ds = SqlHelper.ExecuteDataset(SqlHelper.GetnewConnectionString(), CommandType.StoredProcedure, "getFacilitiesForSAutoID", param);
        error = param[2].Value.ToString();
        return ds;
    }

    //public string getLogin(string username, string password, out string error, int Bypasslogin)
    //{
    //    error = "";
    //    SqlParameter[] param = new SqlParameter[5];
    //    param[0] = new SqlParameter("@username", SqlDbType.VarChar, 200);
    //    param[0].Value = username;
    //    param[1] = new SqlParameter("@password", SqlDbType.VarChar, 200);
    //    param[1].Value = password;
    //    param[2] = new SqlParameter("@facilityid", SqlDbType.VarChar, 100000);
    //    param[2].Direction = ParameterDirection.Output;
    //    param[3] = new SqlParameter("@error", SqlDbType.VarChar, 100000);
    //    param[3].Direction = ParameterDirection.Output;
    //    param[4] = new SqlParameter("@Bypasslogin", SqlDbType.Int);
    //    param[4].Value = Bypasslogin;


    //    SqlHelper.ExecuteNonQuery(SqlHelper.getLoginConnection(), CommandType.StoredProcedure, "getFacilitiesForUsername", param);
    //    error = param[3].Value.ToString();
    //    return param[2].Value.ToString();
    //}
    //public string getFacilities(string username, string password, out string error, int Bypasslogin)
    //{
    //    error = "";
    //    SqlParameter[] param = new SqlParameter[3];
    //    param[0] = new SqlParameter("@username", SqlDbType.VarChar, 200);
    //    param[0].Value = username;
    //    param[1] = new SqlParameter("@facilityid", SqlDbType.VarChar, 100000);
    //    param[1].Direction = ParameterDirection.Output;
    //    param[2] = new SqlParameter("@error", SqlDbType.VarChar, 100000);
    //    param[2].Direction = ParameterDirection.Output;
        

    //    SqlHelper.ExecuteNonQuery(SqlHelper.getLoginConnection(), CommandType.StoredProcedure, "getFacilitiesForUsername", param);
    //    error = param[2].Value.ToString();
    //    return param[1].Value.ToString();
    //}


    public string getFacilities(string SAutoID, out string error)
    {
        error = "error";
        try
        {
            
            SqlParameter[] param = new SqlParameter[3];
            param[0] = new SqlParameter("@SAutoID", SqlDbType.VarChar, 200);
            param[0].Value = SAutoID;
            param[1] = new SqlParameter("@facilityid", SqlDbType.VarChar, 100000);
            param[1].Direction = ParameterDirection.Output;
            param[2] = new SqlParameter("@error", SqlDbType.VarChar, 100000);
            param[2].Direction = ParameterDirection.Output;


            SqlHelper.ExecuteNonQuery(SqlHelper.GetnewConnectionString(), CommandType.StoredProcedure, "getFacilitiesForSAutoID", param);
            error = param[2].Value.ToString();
            return param[1].Value.ToString();
        }
        catch (Exception ex)
        {
           
            string vbCrLf = "\r\n";
          //  Common.SaveTextToFile("------" + vbCrLf + ex.Message.ToString() + vbCrLf + ex.Source + vbCrLf + ex.StackTrace.ToString() + vbCrLf + ex.TargetSite.ToString() + vbCrLf + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString() + "------");
            return "error";
        }
    }

    public void ChangePassword(string username, string password, string currentPassword, out string error)
    {
        SqlParameter[] param = new SqlParameter[4];
        error = "";
        try
        {
            param[0] = new SqlParameter("@username ", SqlDbType.VarChar, 100);
            param[0].Value = username;
            param[1] = new SqlParameter("@password ", SqlDbType.VarChar, 100);
            param[1].Value = password;
            param[2] = new SqlParameter("@currentpassword ", SqlDbType.VarChar, 100);
            param[2].Value = currentPassword;
            param[3] = new SqlParameter("@error", SqlDbType.VarChar, 100000);
            param[3].Direction = ParameterDirection.Output;

            SqlHelper.ExecuteNonQuery(SqlHelper.GetnewConnectionString(), CommandType.StoredProcedure, "ChangePassword ", param);

            error = param[3].Value.ToString();
        }
        catch (Exception ex)
        {

            string vbCrLf = "\r\n";
          //  Common.SaveTextToFile("------" + vbCrLf + ex.Message.ToString() + vbCrLf + ex.Source + vbCrLf + ex.StackTrace.ToString() + vbCrLf + ex.TargetSite.ToString() + vbCrLf + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString() + "------");
            
        }
    }


    public DataTable GetNewLogins(string SubID)
    {
        DataTable dt = null;
        try
        {
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@SubID", SqlDbType.VarChar, 100);
            param[0].Value = SubID;
            dt = SqlHelper.ExecuteDataset(SqlHelper.GetnewConnectionString(), CommandType.StoredProcedure, "retriveLoginDtls", param).Tables[0];
        }
        catch (Exception ex)
        {
            string vbCrLf = "\r\n";
          //  Common.SaveTextToFile("------" + vbCrLf + ex.Message.ToString() + vbCrLf + ex.Source + vbCrLf + ex.StackTrace.ToString() + vbCrLf + ex.TargetSite.ToString() + vbCrLf + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString() + "------");
            dt = null;
        }
        return dt;
    }

    public DataSet getLogin(string username, string password, string ipaddress, string rqstURL, out int ipattempt, out int invaliduserattempt, out int isadmin)
    {
        DataSet ds = new DataSet();
        ipattempt = 0;
        invaliduserattempt = 0;
        isadmin = 0;
        try
        {
            ds = null;
            SqlParameter[] param = new SqlParameter[7];
            param[0] = new SqlParameter("@username", SqlDbType.VarChar, 100);
            param[0].Value = username;
            param[1] = new SqlParameter("@password", SqlDbType.VarChar, 100);
            param[1].Value = password;
            param[2] = new SqlParameter("@url", SqlDbType.VarChar);
            param[2].Value = rqstURL;
            param[3] = new SqlParameter("@ip_address", SqlDbType.VarChar, 100);
            param[3].Value = ipaddress;
            param[4] = new SqlParameter("@IPAttempt", SqlDbType.Int);
            param[4].Direction = ParameterDirection.Output;
            param[5] = new SqlParameter("@userattempt", SqlDbType.Int);
            param[5].Direction = ParameterDirection.Output;

            param[6] = new SqlParameter("@isadmin", SqlDbType.Int);
            param[6].Direction = ParameterDirection.Output;

            //  ds = SqlHelper.ExecuteDataset(SqlHelper.GetnewConnectionString(), CommandType.StoredProcedure, "logins_user_ip_block", param);
            ds = SqlHelper.ExecuteDataset(SqlHelper.GetConnectionString(), CommandType.StoredProcedure, "RoleManagement_logins", param);
            Int32.TryParse(param[4].Value.ToString(), out ipattempt);
            Int32.TryParse(param[5].Value.ToString(), out invaliduserattempt);
            Int32.TryParse(param[6].Value.ToString(), out isadmin);
            //ipattempt = Convert.ToInt32(param[4].Value.ToString());
            //invaliduserattempt = Convert.ToInt32(param[5].Value.ToString());
            //isadmin = Convert.ToInt32(param[6].Value.ToString());

        }
        catch (Exception ex)
        {

            ds = null;
        }
        return ds;
    }
    public string Decryptdata(string encryptpwd)
    {
        string EncryptionKey = "MAKV2SPBNI99212";
        byte[] cipherBytes = Convert.FromBase64String(encryptpwd);
        using (Aes encryptor = Aes.Create())
        {
            Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
            encryptor.Key = pdb.GetBytes(32);
            encryptor.IV = pdb.GetBytes(16);
            using (MemoryStream ms = new MemoryStream())
            {
                using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                {
                    cs.Write(cipherBytes, 0, cipherBytes.Length);
                    cs.Close();
                }
                encryptpwd = Encoding.Unicode.GetString(ms.ToArray());
            }
        }
        return encryptpwd;
    }

    public string Encryptdata(string password)
    {
        string strmsg = string.Empty;
        byte[] encode = new byte[password.Length];
        encode = System.Text.Encoding.UTF8.GetBytes(password);
        strmsg = Convert.ToBase64String(encode);
        return strmsg;
    }



   // [setFeedback](@name varchar(500)='',@emailId varchar(200)='',@PhoneNumber varchar(50)='',@title varchar(500)='',
//@desc varchar(1000)='',@attachmentPath varchar(max)='',@result int =1 out,@practiceid varchar(10)='',@os varchar(100)='',@browser varchar(100)='',@time datetime='' )
    public int setFeedback(string name, string emailId, string PhoneNumber, string title, string desc, string attachmentPath, string practiceid, string os , string browser)
    {
        int result = 1;
        try
        {
            SqlParameter[] param = new SqlParameter[10];
            param[0] = new SqlParameter("@name", SqlDbType.VarChar, 500);
            param[0].Value = name;
            param[1] = new SqlParameter("@emailId", SqlDbType.VarChar, 200);
            param[1].Value = emailId;
            param[2] = new SqlParameter("@PhoneNumber", SqlDbType.VarChar, 50);
            param[2].Value = PhoneNumber;
            param[3] = new SqlParameter("@title", SqlDbType.VarChar, 500);
            param[3].Value = title;
            param[4] = new SqlParameter("@desc", SqlDbType.VarChar, 10000);
            param[4].Value = desc;
            param[5] = new SqlParameter("@attachmentPath", SqlDbType.VarChar,10000000);
            param[5].Value = attachmentPath;

            param[6]=new SqlParameter("@result",SqlDbType.Int);
            param[6].Direction=ParameterDirection.Output;

            param[7] = new SqlParameter("@practiceid", SqlDbType.VarChar,10);
            param[7].Value = practiceid;

            param[8] = new SqlParameter("@os", SqlDbType.VarChar, 100);
            param[8].Value = os;

            param[9] = new SqlParameter("@browser", SqlDbType.VarChar, 100);
            param[9].Value = browser;

       

            SqlHelper.ExecuteNonQuery(SqlHelper.GetConnectionString(), CommandType.StoredProcedure, "setFeedback", param);
            result = Convert.ToInt32(param[6].Value);
        }
        catch (Exception ex)
        {
            string vbCrLf = "\r\n";
          //  Common.SaveTextToFile("------" + vbCrLf + ex.Message.ToString() + vbCrLf + ex.Source + vbCrLf + ex.StackTrace.ToString() + vbCrLf + ex.TargetSite.ToString() + vbCrLf + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString() + "------");
            
        }
        return result;
    }

    public string getThemeIDfromURL(string url)
    {
        string theme = "";
        try
        {
            SqlParameter[] param = new SqlParameter[2];
            param[0] = new SqlParameter("@URl", SqlDbType.VarChar, 100000);
            param[0].Value = url;
            param[1] = new SqlParameter("@themeid", SqlDbType.VarChar, 100);
            param[1].Direction = ParameterDirection.Output;

            SqlHelper.ExecuteNonQuery(SqlHelper.GetnewConnectionString(), CommandType.StoredProcedure, "getThemeIDfromURL", param);

            theme = Convert.ToString(param[1].Value);
        }
        catch (Exception ex)
        {
            string vbCrLf = "\r\n";
           // Common.SaveTextToFile("------" + vbCrLf + ex.Message.ToString() + vbCrLf + ex.Source + vbCrLf + ex.StackTrace.ToString() + vbCrLf + ex.TargetSite.ToString() + vbCrLf + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString() + "------");
        }
        return theme;
    }

    public static string MD5Hash(string text)
    {
        MD5 md5 = new MD5CryptoServiceProvider();

        //compute hash from the bytes of text
        md5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(text));

        //get hash result after compute it
        byte[] result = md5.Hash;

        StringBuilder strBuilder = new StringBuilder();
        for (int i = 0; i < result.Length; i++)
        {
            //change it into 2 hexadecimal digits
            //for each byte
            strBuilder.Append(result[i].ToString("x2"));
        }

        return strBuilder.ToString();
    }


    public DataSet getloginsforhashing()
    {

        return SqlHelper.ExecuteDataset(SqlHelper.GetnewConnectionString(), CommandType.StoredProcedure, "getloginsforhashing");
    }

    public string setloginsforhashing(int id, string password)
    {
        string theme = "";
        try
        {
            SqlParameter[] param = new SqlParameter[2];
            param[0] = new SqlParameter("@password", SqlDbType.VarChar, 200);
            param[0].Value = password;
            param[1] = new SqlParameter("@id", SqlDbType.Int);
            param[1].Value = id;

            SqlHelper.ExecuteNonQuery(SqlHelper.GetnewConnectionString(), CommandType.StoredProcedure, "setloginsforhashing", param);

            theme = Convert.ToString(param[1].Value);
        }
        catch (Exception ex)
        {
            string vbCrLf = "\r\n";
           // Common.SaveTextToFile("------" + vbCrLf + ex.Message.ToString() + vbCrLf + ex.Source + vbCrLf + ex.StackTrace.ToString() + vbCrLf + ex.TargetSite.ToString() + vbCrLf + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString() + "------");
        }
        return theme;
    }
  

}