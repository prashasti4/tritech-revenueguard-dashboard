﻿using Elmah;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;
using Newtonsoft.Json;

/// <summary>
/// Summary description for TritechWebService
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class TritechWebService : System.Web.Services.WebService {
    DBConnect objConnect = new DBConnect();
    public TritechWebService () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

   

    //[WebMethod]
    //// Average Days To Bill
    //public string AverageDaysToBill(string companyid, string reporttype, string fromdate, string todate, string reportid, string SAutoId, string CAutoId)
    //{
    //    string convertedstring = string.Empty;
    //    try
    //    {

    //       // DataSet ds = objConnect.getAverageDaysToBill(companyid, Convert.ToInt32(reporttype), fromdate, todate, Convert.ToInt32(reportid), Convert.ToInt32(SAutoId), Convert.ToInt32(CAutoId));
    //        DataSet ds = objConnect.getAverageDaysToBill(companyid, Convert.ToInt32(reporttype), fromdate, todate, Convert.ToInt32(reportid), Convert.ToInt32(SAutoId));
    //        if (ds != null)
    //        {
    //            if (ds.Tables.Count > 0)
    //            {

    //                convertedstring = (reporttype == "0") ? JsonConvert.SerializeObject(ds) : FetchChartJSON(ds);
    //            }
    //        }
            
    //    }
    //    catch (Exception ex)
    //    {
    //        ErrorSignal.FromCurrentContext().Raise(ex);
    //    }
    //    return convertedstring;
    //}

   

    //[WebMethod]
    //// Waiting to Be Billed
    //public string WaitingClaimsVsBilledClaims(string companyid, string reporttype, string fromdate, string todate, string reportid, string SAutoId, string CAutoId)
    //{
    //    string convertedstring = string.Empty;
    //    try
    //    {

    //        DataSet ds = objConnect.getWaitingClaimsVsBilledClaims(companyid, Convert.ToInt32(reporttype), fromdate, todate, Convert.ToInt32(reportid), Convert.ToInt32(SAutoId), Convert.ToInt32(CAutoId));
    //        if (ds != null)
    //        {
    //            if (ds.Tables.Count > 0)
    //            {

    //                convertedstring = (reporttype == "0") ? JsonConvert.SerializeObject(ds) : FetchChartJSON(ds);
    //            }
    //        }

    //    }
    //    catch (Exception ex)
    //    {
    //        ErrorSignal.FromCurrentContext().Raise(ex);
    //    }
    //    return convertedstring;
    //}

    //[WebMethod]
    ////  Date of Service vs. Date of Upload
    //public string DOSvsDOU(string companyid, string reporttype, string fromdate, string todate, string reportid, string SAutoId, string CAutoId, string DaysFilter)
    //{
    //    string convertedstring = string.Empty;
    //    try
    //    {

    //        DataSet ds = objConnect.getDOSvsDOU(companyid, Convert.ToInt32(reporttype), fromdate, todate, Convert.ToInt32(reportid), Convert.ToInt32(SAutoId), Convert.ToInt32(CAutoId),Convert.ToInt32(DaysFilter));
    //        if (ds != null)
    //        {
    //            if (ds.Tables.Count > 0)
    //            {

    //                convertedstring = (reporttype == "0") ? JsonConvert.SerializeObject(ds) : FetchChartJSON(ds);
    //            }
    //        }

    //    }
    //    catch (Exception ex)
    //    {
    //        ErrorSignal.FromCurrentContext().Raise(ex);
    //    }
    //    return convertedstring;
    //}



    //[WebMethod]
    ////  A/R - Total Outstanding Claims
    //public string TotalOutstandingClaims(string companyid, string reporttype, string fromdate, string todate, string reportid, string SAutoId, string CAutoId, string DaysOver, string Payer)
    //{
    //    string convertedstring = string.Empty;
    //    try
    //    {

    //        DataSet ds = objConnect.getTotalOutstandingClaims(companyid, Convert.ToInt32(reporttype), fromdate, todate, Convert.ToInt32(reportid), Convert.ToInt32(SAutoId), Convert.ToInt32(CAutoId), Convert.ToInt32(DaysOver), Payer);
    //        if (ds != null)
    //        {
    //            if (ds.Tables.Count > 0)
    //            {

    //                convertedstring = (reporttype == "0") ? JsonConvert.SerializeObject(ds) : FetchChartJSON(ds);
    //            }
    //        }

    //    }
    //    catch (Exception ex)
    //    {
    //        ErrorSignal.FromCurrentContext().Raise(ex);
    //    }
    //    return convertedstring;
    //}



    //[WebMethod]
    ////  A/R - Percent Outstanding Claims
    //public string PercentOutstandingClaims(string companyid, string reporttype, string fromdate, string todate, string reportid, string SAutoId, string CAutoId, string DaysOver, string Payer)
    //{
    //    string convertedstring = string.Empty;
    //    try
    //    {

    //        DataSet ds = objConnect.getPercentOutstandingClaims(companyid, Convert.ToInt32(reporttype), fromdate, todate, Convert.ToInt32(reportid), Convert.ToInt32(SAutoId), Convert.ToInt32(CAutoId), Convert.ToInt32(DaysOver), Payer);
    //        if (ds != null)
    //        {
    //            if (ds.Tables.Count > 0)
    //            {

    //                convertedstring = (reporttype == "0") ? JsonConvert.SerializeObject(ds) : FetchChartJSON(ds);
    //            }
    //        }

    //    }
    //    catch (Exception ex)
    //    {
    //        ErrorSignal.FromCurrentContext().Raise(ex);
    //    }
    //    return convertedstring;
    //}

    //[WebMethod]
    ////  Claims Billed Month per Client 
    //public string ClaimsBilledPerClient(string companyid, string reporttype, string fromdate, string todate, string reportid, string SAutoId, string CAutoId)
    //{
    //    string convertedstring = string.Empty;
    //    try
    //    {

    //        DataSet ds = objConnect.getClaimsBilledPerClient(companyid, Convert.ToInt32(reporttype), fromdate, todate, Convert.ToInt32(reportid), Convert.ToInt32(SAutoId), Convert.ToInt32(CAutoId));
    //        if (ds != null)
    //        {
    //            if (ds.Tables.Count > 0)
    //            {

    //                convertedstring = (reporttype == "0") ? JsonConvert.SerializeObject(ds) : FetchChartJSON(ds);
    //            }
    //        }

    //    }
    //    catch (Exception ex)
    //    {
    //        ErrorSignal.FromCurrentContext().Raise(ex);
    //    }
    //    return convertedstring;
    //}

    //[WebMethod]
    ////  Average Days Outstanding Trending
    //public string AverageDaysOutstandingTrending(string companyid, string reporttype, string fromdate, string todate, string reportid, string SAutoId, string CAutoId)
    //{
    //    string convertedstring = string.Empty;
    //    try
    //    {

    //        DataSet ds = objConnect.getAverageDaysOutstandingTrending(companyid, Convert.ToInt32(reporttype), fromdate, todate, Convert.ToInt32(reportid), Convert.ToInt32(SAutoId), Convert.ToInt32(CAutoId));
    //        if (ds != null)
    //        {
    //            if (ds.Tables.Count > 0)
    //            {

    //                convertedstring = (reporttype == "0") ? JsonConvert.SerializeObject(ds) : FetchChartJSON(ds);
    //            }
    //        }

    //    }
    //    catch (Exception ex)
    //    {
    //        ErrorSignal.FromCurrentContext().Raise(ex);
    //    }
    //    return convertedstring;
    //}

    //[WebMethod]
    ////  Medicare Last Payment
    //public string MedicarelastPayment(string companyid, string reporttype, string fromdate, string todate, string reportid, string SAutoId, string CAutoId)
    //{
    //    string convertedstring = string.Empty;
    //    try
    //    {

    //        DataSet ds = objConnect.getMedicarelastPayment(companyid, Convert.ToInt32(reporttype), fromdate, todate, Convert.ToInt32(reportid), Convert.ToInt32(SAutoId), Convert.ToInt32(CAutoId));
    //        if (ds != null)
    //        {
    //            if (ds.Tables.Count > 0)
    //            {

    //                convertedstring = (reporttype == "0") ? JsonConvert.SerializeObject(ds) : FetchChartJSON(ds);
    //            }
    //        }

    //    }
    //    catch (Exception ex)
    //    {
    //        ErrorSignal.FromCurrentContext().Raise(ex);
    //    }
    //    return convertedstring;
    //}


    //[WebMethod]
    ////  Average Cash Collected Per Trip
    //public string AverageCashCollectedPerTrip(string companyid, string reporttype, string fromdate, string todate, string reportid, string SAutoId, string CAutoId)
    //{
    //    string convertedstring = string.Empty;
    //    try
    //    {

    //        DataSet ds = objConnect.getAverageCashCollectedPerTrip(companyid, Convert.ToInt32(reporttype), fromdate, todate, Convert.ToInt32(reportid), Convert.ToInt32(SAutoId), Convert.ToInt32(CAutoId));
    //        if (ds != null)
    //        {
    //            if (ds.Tables.Count > 0)
    //            {

    //                convertedstring = (reporttype == "0") ? JsonConvert.SerializeObject(ds) : FetchChartJSON(ds);
    //            }
    //        }

    //    }
    //    catch (Exception ex)
    //    {
    //        ErrorSignal.FromCurrentContext().Raise(ex);
    //    }
    //    return convertedstring;
    //}


    //[WebMethod]
    ////  Volume Monthly Trending
    //public string VolumeMonthlyTrending(string companyid, string reporttype, string fromdate, string todate, string reportid, string SAutoId, string CAutoId)
    //{
    //    string convertedstring = string.Empty;
    //    try
    //    {

    //        DataSet ds = objConnect.getVolumeMonthlyTrending(companyid, Convert.ToInt32(reporttype), fromdate, todate, Convert.ToInt32(reportid), Convert.ToInt32(SAutoId), Convert.ToInt32(CAutoId));
    //        if (ds != null)
    //        {
    //            if (ds.Tables.Count > 0)
    //            {

    //                convertedstring = (reporttype == "0") ? JsonConvert.SerializeObject(ds) : FetchChartJSON(ds);
    //            }
    //        }

    //    }
    //    catch (Exception ex)
    //    {
    //        ErrorSignal.FromCurrentContext().Raise(ex);
    //    }
    //    return convertedstring;
    //}

    //// Generate Chart JSON
    //public string FetchChartJSON(DataSet dst)
    //{
    //    string json = "";
    //    try
    //    {
    //        //json = JsonConvert.SerializeObject(dst);
    //        //json = json.Substring(0, json.Length - 2) + "]";
    //        //string series = "";
    //        //int cnt = 1;
    //        //for (int i = 0; i < dst.Tables.Count; i++)
    //        //{
    //        //    series = getseriesfromtable(dst.Tables[i], "series" + cnt.ToString());
    //        //    json = json + "," + (((i+1)!= dst.Tables.Count)? series.Substring(0, series.Length - 1):series);
    //        //    i++;
    //        //}

    //        if (dst.Tables.Count > 0)
    //        {
    //            json = json + "{";
    //            for (int i = 0; i < dst.Tables.Count; i++)
    //            {
    //                if(i==0)
    //                    json = json + "\"Table\":{";
    //                else
    //                    json = json + ",\"Table+"+i.ToString()+"\":{";
    //                string[] columnNames = dst.Tables[i].Columns.Cast<DataColumn>()
    //                                     .Select(x => x.ColumnName)
    //                                     .ToArray();

    //                 var distinctLabels = dst.Tables[i].AsEnumerable()
    //                .Select(s=> new {
    //                    label = s.Field<string>(columnNames[0].ToString()),                           
    //                 }).ToList();
    //              //  .Distinct().ToList();

    //                 json = json + "\"labels\": [";

    //                 int cnt = 0;
    //                foreach(var label in distinctLabels)
    //                {
    //                    if (cnt==0)
    //                    {
    //                        json = json + "\"" + label.label.ToString() + "\"";
    //                    }
    //                    else
    //                    {
    //                        json = json + ",\"" + label.label.ToString() + "\"";
    //                    }

    //                    cnt++;
    //                }
    //                json = json + "],";



    //                json = json + "\"series\": [";
    //                for (int j = 1; j < columnNames.Length;j++)
    //                {
    //                    if(j== 1)
    //                    {
    //                        json = json + "\"" + columnNames[j].ToString() + "\"";
    //                    }
    //                    else
    //                    {
    //                        json = json + ",\"" + columnNames[j].ToString() + "\"";
    //                    }
    //                }
    //                json = json + "],";



    //                json = json + "\"data\": [";
    //                for (int j = 1; j < columnNames.Length; j++)
    //                {
    //                    var ColumnDatas = dst.Tables[i].AsEnumerable()
    //               .Select(s => new
    //               {
    //                   ColumnData = s.Field<string>(columnNames[j].ToString()),
    //               }).ToList();
    //               //.Distinct().ToList();
    //                    if (j == 1)
    //                    {
    //                        json = json + "[";
    //                    }
    //                    else
    //                    {
    //                        json = json + ",[";
    //                    }
    //                    int cnt1 = 0;
    //                    foreach (var ColumnData in ColumnDatas)
    //                    {
                            
    //                            if (cnt1 == 0)
    //                            {
    //                                json = json + "\"" + ColumnData.ColumnData.ToString() + "\"";
    //                            }
    //                            else
    //                            {
    //                                json = json + ",\"" + ColumnData.ColumnData.ToString() + "\"";
    //                            }
                           
    //                        cnt1++;

    //                    }

    //                    json = json + "]";
    //                }
    //                json = json + "]";




    //                json = json + "}";
    //            }
    //            json = json + "}";
    //        }

    //    }
    //    catch (Exception ex)
    //    {
    //        ErrorSignal.FromCurrentContext().Raise(ex);
    //    }

    //    return json;
    //}

    //// Generate Chart Series
    //private string getseriesfromtable(DataTable dt, string seriesname)
    //{
    //    string[] seriescol = new string[dt.Columns.Count];
    //    for (int i = 0; i < dt.Columns.Count; i++)
    //    {
    //        seriescol[i] = dt.Columns[i].ColumnName;
    //    }

    //    string series = "\"" + seriesname + "\":[";
    //    for (int i = 3; i < seriescol.Length; i++)
    //    {
    //        series += "{ \"valueField\":\"" + seriescol[i].ToString();
          
    //        if (i == seriescol.Length - 1) // for last column 
    //        {

    //            series += "\", \"name\":\"" + seriescol[i].ToString() + "\"}";
    //        }
    //        else
    //        {
    //            //if (!string.IsNullOrEmpty(Request["dtype"]) && i == 3)
    //            //    series += "\", \"type\":\"area";
    //            series += "\", \"name\":\"" + seriescol[i].ToString() + "\"},";
    //        }
    //    }
    //    series += "]}";

    //    return series;

    //}
    
}
