﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

/// <summary>
/// Summary description for ChartDataClass
/// </summary>
/// 
public class TreeList
{
    public string Name { get; set; }
    public int Id { get; set; }
    public int parentId { get; set; }
    public int LEVEL_DEPTH { get; set; }
    public int ischeck { get; set; }

}

public class TreeCarrier
{
    public string Name { get; set; }
    public string Insgrpid { get; set; }
    public int ischeck { get; set; }

    public string parentId { get; set; }
    public int LEVEL_DEPTH { get; set; }
}

public class ChartDataClass
{
    AppearenceSettingRepo objappRepo = new AppearenceSettingRepo();
    public ChartDataClass()
    {
        //
        // TODO: Add constructor logic here
        //
    }



    public DataSet getGraphdata(string practiceid, int branchid, string InsGrpId, int reportid, string fromdate, string todate, string previousdata, string deficiencycode, string providercode, string procedureName, string cAutoId)
    {
        DataSet ds = new DataSet();
        SqlParameter[] param = new SqlParameter[12];
        string medlink = objappRepo.Decryptdata(HttpContext.Current.Request.Cookies["SM_FACILITY"]["Uname"].ToString());
        try
        {
            param[0] = new SqlParameter("@practiceid", SqlDbType.VarChar);
            param[0].Value = practiceid;
            param[1] = new SqlParameter("@branchid", SqlDbType.Int);
            param[1].Value = branchid;
            param[2] = new SqlParameter("@InsGrpId", SqlDbType.VarChar);
            param[2].Value = InsGrpId;
            param[3] = new SqlParameter("@reportid", SqlDbType.Int);
            param[3].Value = reportid;

            param[4] = new SqlParameter("@fromdate", SqlDbType.DateTime);
            if (fromdate.ToString().Trim() != "")
                param[4].Value = Convert.ToDateTime(fromdate);
            else
                param[4].Value = DBNull.Value;


            param[5] = new SqlParameter("@todate", SqlDbType.DateTime);
            if (todate.ToString().Trim() != "")
                param[5].Value = Convert.ToDateTime(todate);
            else
                param[5].Value = DBNull.Value;


            param[6] = new SqlParameter("@previousdata", SqlDbType.VarChar);
            param[6].Value = previousdata;

            param[7] = new SqlParameter("@isClientSide", SqlDbType.Int);
            param[7].Value = 0;

            param[8] = new SqlParameter("@deficiencycode", SqlDbType.VarChar);
            param[8].Value = deficiencycode;

            param[9] = new SqlParameter("@providercode", SqlDbType.VarChar);
            param[9].Value = providercode;

            param[10] = new SqlParameter("@isMedlink ", SqlDbType.Int);
            param[10].Value = medlink == "MedlinkDemo" ? 1 : 0;

            param[11] = new SqlParameter("@Cautoid", SqlDbType.VarChar, 100);
            param[11].Value = cAutoId;


            ds = SqlHelper.ExecuteDataset(SqlHelper.GetConnectionString(), CommandType.StoredProcedure, procedureName, param);

            return ds;
        }
        catch (Exception ex)
        {
            // Common.SaveTextToFile(ex.ToString());
            return ds;
        }
        finally
        {
            ds = null;
            param = null;
        }
    }

    public DataSet getAgedTrialBalance(string practiceid, int branchid, string InsGrpId, int reportid, string fromdate, string todate, string previousdata, string procedureName, int pageSize, int pageNo, string cAutoId)
    {
        DataSet ds = new DataSet();
        SqlParameter[] param = new SqlParameter[11];
        string medlink = objappRepo.Decryptdata(HttpContext.Current.Request.Cookies["SM_FACILITY"]["Uname"].ToString());
        try
        {
            param[0] = new SqlParameter("@practiceid", SqlDbType.VarChar);
            param[0].Value = practiceid;
            param[1] = new SqlParameter("@branchid", SqlDbType.Int);
            param[1].Value = branchid;
            param[2] = new SqlParameter("@InsGrpId", SqlDbType.VarChar);
            param[2].Value = InsGrpId;
            param[3] = new SqlParameter("@reportid", SqlDbType.Int);
            param[3].Value = reportid;

            param[4] = new SqlParameter("@fromdate", SqlDbType.DateTime);
            if (fromdate.ToString().Trim() != "")
                param[4].Value = Convert.ToDateTime(fromdate);
            else
                param[4].Value = DBNull.Value;


            param[5] = new SqlParameter("@todate", SqlDbType.DateTime);
            if (todate.ToString().Trim() != "")
                param[5].Value = Convert.ToDateTime(todate);
            else
                param[5].Value = DBNull.Value;


            param[6] = new SqlParameter("@previousdata", SqlDbType.VarChar);
            param[6].Value = previousdata;

            param[7] = new SqlParameter("@pageSize", SqlDbType.Int);
            param[7].Value = pageSize;

            param[8] = new SqlParameter("@pageNo", SqlDbType.Int);
            param[8].Value = pageNo;

            param[9] = new SqlParameter("@isMedlink ", SqlDbType.Int);
            param[9].Value = medlink == "MedlinkDemo" ? 1 : 0;

            param[10] = new SqlParameter("@Cautoid", SqlDbType.VarChar, 100);
            param[10].Value = cAutoId;


            ds = SqlHelper.ExecuteDataset(SqlHelper.GetConnectionString(), CommandType.StoredProcedure, procedureName, param);

            return ds;
        }
        catch (Exception ex)
        {
            // Common.SaveTextToFile(ex.ToString());
            return ds;
        }
        finally
        {
            ds = null;
            param = null;
        }
    }

    public DataSet getBranchfordropdown(string practiceid, string fromdate, string todate)
    {
        DataSet ds = new DataSet();
        SqlParameter[] param = new SqlParameter[3];
        try
        {
            param[0] = new SqlParameter("@practiceid", SqlDbType.VarChar);
            param[0].Value = practiceid;
            param[1] = new SqlParameter("@fromdate", SqlDbType.DateTime);
            if (fromdate.ToString().Trim() != "")
                param[1].Value = Convert.ToDateTime(fromdate);
            else
                param[1].Value = DBNull.Value;

            param[2] = new SqlParameter("@todate", SqlDbType.DateTime);
            if (todate.ToString().Trim() != "")
                param[2].Value = Convert.ToDateTime(todate);
            else
                param[2].Value = DBNull.Value;

            ds = SqlHelper.ExecuteDataset(SqlHelper.GetConnectionString(), CommandType.StoredProcedure, "getBranchfordropdown", param);
            return ds;
        }
        catch (Exception ex)
        {
            // Common.SaveTextToFile(ex.ToString());
            return ds;
        }
        finally
        {
            ds = null;
        }
    }


    public DataSet getCarrierfordropdown(string practiceid)
    {
        DataSet ds = new DataSet();
        SqlParameter[] param = new SqlParameter[1];
        try
        {
            param[0] = new SqlParameter("@practiceid", SqlDbType.VarChar);
            param[0].Value = practiceid;

            ds = SqlHelper.ExecuteDataset(SqlHelper.GetConnectionString(), CommandType.StoredProcedure, "getCarrierfordropdown", param);
            return ds;
        }
        catch (Exception ex)
        {
            // Common.SaveTextToFile(ex.ToString());
            return ds;
        }
        finally
        {
            ds = null;
            param = null;
        }
    }

    public DataSet getreportnamefordropdown(string practiceid)
    {
        DataSet ds = new DataSet();
        SqlParameter[] param = new SqlParameter[4];
        string CAutoID = objappRepo.Decryptdata(HttpContext.Current.Request.Cookies["CAutoID"].Value);
        string SAutoID = objappRepo.Decryptdata(HttpContext.Current.Request.Cookies["SM_USER"].Value);
        string ClientMasterID = HttpContext.Current.Request.Cookies["ClientMasterID"] != null ? HttpContext.Current.Request.Cookies["ClientMasterID"].Value : "1";
        try
        {
            param[0] = new SqlParameter("@practiceid", SqlDbType.VarChar);
            param[0].Value = practiceid;
            param[1] = new SqlParameter("@cautoid", SqlDbType.Int);
            param[1].Value = Convert.ToInt32(CAutoID);
            param[2] = new SqlParameter("@sautoid", SqlDbType.Int);
            param[2].Value = Convert.ToInt32(SAutoID); ;
            param[3] = new SqlParameter("@clientmasterid", SqlDbType.Int);
            param[3].Value = Convert.ToInt32(ClientMasterID); ;
            // ds = SqlHelper.ExecuteDataset(SqlHelper.GetConnectionString(), CommandType.StoredProcedure, "getreportnamefordropdown",param);
            ds = SqlHelper.ExecuteDataset(SqlHelper.GetConnectionString(), CommandType.StoredProcedure, "getmenus", param);
            return ds;
        }
        catch (Exception ex)
        {
            // Common.SaveTextToFile(ex.ToString());
            return ds;
        }
        finally
        {
            ds = null;

        }
    }

    public static string getTreeData(string fromdate, string todate, string ids, string reportid)
    {
        //getFilter(@fromdate datetime='2013/01/01', @todate datetime='2013/01/01')

        string StrJson = "";
        try
        {
            //DataSet dst = SqlHelper.ExecuteDataset(SqlHelper.getconnection(), CommandType.StoredProcedure, "SSP_GET_HIERARCHY_NEW");
            DataSet dst;
            string sqlStoreprocedure = "";
            SqlParameter[] arparam = new SqlParameter[4];
            //arparam[0] = new SqlParameter("@fromdate", fromdate);
            //arparam[1] = new SqlParameter("@todate", todate);
            //arparam[2] = new SqlParameter("@ids", ids);
            //arparam[3] = new SqlParameter("@reportid", reportid);

            arparam[0] = new SqlParameter("@fromdate", SqlDbType.DateTime);
            if (fromdate != "")
                arparam[0].Value = fromdate;
            else
                arparam[0].Value = DBNull.Value;
            arparam[1] = new SqlParameter("@todate", SqlDbType.DateTime);
            if (todate != "")
                arparam[1].Value = todate;
            else
                arparam[1].Value = DBNull.Value; ;
            arparam[2] = new SqlParameter("@ids", SqlDbType.VarChar, 1000);
            arparam[2].Value = ids;
            arparam[3] = new SqlParameter("@reportid", SqlDbType.Int);
            arparam[3].Value = reportid;




            dst = SqlHelper.ExecuteDataset(SqlHelper.GetConnectionString(), CommandType.StoredProcedure, "getFilter", arparam);


            List<TreeList> objlisttree = new List<TreeList>();
            for (int i = 0; i <= dst.Tables[0].Rows.Count - 1; i++)
            {
                TreeList objtree = new TreeList();
                objtree.Name = dst.Tables[0].Rows[i]["Name"].ToString();
                objtree.Id = Convert.ToInt32(dst.Tables[0].Rows[i]["Id"].ToString());
                objtree.parentId = Convert.ToInt32(dst.Tables[0].Rows[i]["parentId"].ToString());
                objtree.LEVEL_DEPTH = Convert.ToInt32(dst.Tables[0].Rows[i]["LEVEL_DEPTH"].ToString());
                objtree.ischeck = Convert.ToInt32(dst.Tables[0].Rows[i]["ischeck"].ToString());
                objlisttree.Add(objtree);
            }
            objlisttree = objlisttree.OrderBy(e => e.LEVEL_DEPTH).ToList();

            if (objlisttree.Count > 0)
            {
                StrJson = "   [{";
                StrJson += "\"id\":\"" + objlisttree[0].Id + "\",";

                StrJson += "\"text\":\"" + objlisttree[0].Name + "\",";
                if (objlisttree[0].ischeck == 1)
                {
                    StrJson += "\"checked\":\"true\",";
                }
                StrJson += getChildren(objlisttree[0].Id, objlisttree.Where(e => e.Id != objlisttree[0].Id).ToList());

                StrJson += "}]";
            }

        }

        catch (Exception ex)
        {
            // Common.SaveTextToFile(ex.ToString());
            return "";
        }
        return StrJson;
    }

    public static string getChildren(int ParentId, List<TreeList> lstTree)
    {
        var child = lstTree.Where(e => e.parentId == ParentId).ToList();
        string StrJson = "";
        foreach (var tree in child)
        {
            if (StrJson == "")
            {
                StrJson = "\"children\":[";
            }
            else
            {
                StrJson += ",";
            }
            StrJson += "{";
            StrJson += "\"id\":\"" + tree.Id + "\",";
            if (tree.ischeck == 1)
            {
                StrJson += "\"checked\":\"true\",";
            }
            StrJson += "\"text\":\"" + tree.Name + "\"";

            StrJson += ((lstTree.Where(e => e.parentId == tree.Id).ToList().Count > 0) ? "," + getChildren(tree.Id, lstTree.Where(e => e.Id != tree.Id).ToList()) : "");
            StrJson += "}";
        }
        if (StrJson != "")
            StrJson += "]";
        return StrJson;
    }
    public static string getCarrierDorpdown(string practiceid, string selelctIDs, int reportid, string FromDate, string toDate, string procedureName, string filterid)
    {

        string StrJson = "";
        try
        {
            string ids = selelctIDs.TrimStart(',');
            DataSet dst;
            string sqlStoreprocedure = "";
            List<SqlParameter> param = new List<SqlParameter>();
            var param1 = new SqlParameter("@practiceid", SqlDbType.VarChar);
            param1.Value = practiceid;
            param.Add(param1);
            param1 = new SqlParameter("@ids", SqlDbType.VarChar);
            param1.Value = ids;
            param.Add(param1);
            param1 = new SqlParameter("@reportid", SqlDbType.Int);
            param1.Value = reportid;
            param.Add(param1);
            param1 = new SqlParameter("@fromdate", SqlDbType.DateTime);
            if (FromDate != "")
                param1.Value = Convert.ToDateTime(FromDate);
            else
                param1.Value = DBNull.Value;
            param.Add(param1);
            param1 = new SqlParameter("@todate", SqlDbType.DateTime);
            if (toDate != "")
                param1.Value = Convert.ToDateTime(toDate);
            else
                param1.Value = DBNull.Value;
            param.Add(param1);

            if (filterid != "")
            {
                param1 = new SqlParameter("@previousdata ", SqlDbType.VarChar, 200);
                param1.Value = filterid;
                param.Add(param1);
            }

            dst = SqlHelper.ExecuteDataset(SqlHelper.GetConnectionString(), CommandType.StoredProcedure, procedureName, param.ToArray());


            List<TreeCarrier> objlisttree = new List<TreeCarrier>();
            for (int i = 0; i <= dst.Tables[0].Rows.Count - 1; i++)
            {
                TreeCarrier objtree = new TreeCarrier();
                objtree.Name = dst.Tables[0].Rows[i]["Name"].ToString();
                objtree.Insgrpid = (dst.Tables[0].Rows[i]["Insgrpid"].ToString());
                objtree.ischeck = Convert.ToInt32(dst.Tables[0].Rows[i]["ischeck"].ToString());
                objtree.parentId = dst.Tables[0].Rows[i]["ParentId"].ToString();
                objlisttree.Add(objtree);
            }
            if (objlisttree.Count > 0)
            {

                StrJson = "   [{";
                StrJson += "\"id\":" + objlisttree[0].Insgrpid + ",";
                //if (objlisttree[0].ischeck == 1)
                //{
                //    StrJson += "\"checked\":\"true\",";
                //}

                StrJson += "\"text\":\"" + objlisttree[0].Name + "\",";
                //StrJson = "   [{";
                //StrJson += "\"id\":\"\",";

                //StrJson += "\"text\":\" ALL CARRIERS \",";
                //if (objlisttree[0].ischeck == 1)
                //{

                //    StrJson += "\"checked\":\"true\",";
                //}
                StrJson += getCarrierChildren(objlisttree[0].Insgrpid, objlisttree);

                StrJson += "}]";
            }

        }

        catch (Exception ex)
        {
            // Common.SaveTextToFile(ex.ToString());
            return "";
        }
        return StrJson;
    }


    public static string getCarrierChildren(string ParentId, List<TreeCarrier> lstTree)
    {
        var child = lstTree.Where(e => e.parentId == ParentId).ToList();
        string StrJson = "";
        foreach (var tree in child)
        {
            if (StrJson == "")
            {
                StrJson = "\"children\":[";
            }
            else
            {
                StrJson += ",";
            }
            StrJson += "{";
            StrJson += "\"id\":\"" + tree.Insgrpid + "\",";

            if (lstTree.Where(w => w.parentId == tree.Insgrpid).Count() == 0)
            {
                if (tree.ischeck == 1)
                {
                    StrJson += "\"checked\":\"true\",";
                }
            }
            //else
            //{
            //    StrJson += "\"checked\":\"false\",";
            //}
            StrJson += "\"text\":\"" + tree.Name + "\"";

            StrJson += ((lstTree.Where(w => w.parentId == tree.Insgrpid).Count() > 0) ? "," + getCarrierChildren(tree.Insgrpid, lstTree) : "");
            StrJson += "}";
        }
        if (StrJson != "")
            StrJson += "]";
        return StrJson;
    }



    public DataSet bindPivotGrid()
    {
        DataSet ds = new DataSet();
        try
        {
            ds = SqlHelper.ExecuteDataset(SqlHelper.GetConnectionString(), CommandType.StoredProcedure, "getGraphdata");
            return ds;
        }
        catch (Exception ex)
        {
            // Common.SaveTextToFile(ex.ToString());
            return ds;
        }
        finally
        {
            ds = null;
        }
    }

    public DataSet GetReportParam(string practiceid)
    {
        DataSet ds = new DataSet();
        try
        {

            SqlParameter param = new SqlParameter("@practiceid", SqlDbType.VarChar);
            param.Value = practiceid;


            ds = SqlHelper.ExecuteDataset(SqlHelper.GetConnectionString(), CommandType.StoredProcedure, "getreportparams", param);
            return ds;
        }
        catch (Exception ex)
        {
            // Common.SaveTextToFile(ex.ToString());
            return ds;
        }
        finally
        {
            ds = null;
        }
    }

    public DataSet GetReportParamAssignments(string practiceid, int reportparamid)
    {
        DataSet ds = new DataSet();
        try
        {
            SqlParameter[] param = new SqlParameter[2];
            param[0] = new SqlParameter("@practiceid", SqlDbType.VarChar);
            param[0].Value = practiceid;

            param[1] = new SqlParameter("@reportparamid", SqlDbType.Int);
            param[1].Value = reportparamid;

            ds = SqlHelper.ExecuteDataset(SqlHelper.GetConnectionString(), CommandType.StoredProcedure, "getReportParamAssignments", param);
            return ds;
        }
        catch (Exception ex)
        {
            // Common.SaveTextToFile(ex.ToString());
            return ds;
        }
        finally
        {

        }
    }
    public void SetReportParamAssignments(string practiceid, int reportParamid, string ProcedureIds, ref int result)
    {

        try
        {

            SqlParameter[] param = new SqlParameter[4];
            param[0] = new SqlParameter("@practiceid", SqlDbType.VarChar);
            param[0].Value = practiceid;


            param[1] = new SqlParameter("@reportParamid", SqlDbType.Int);
            param[1].Value = reportParamid;

            param[2] = new SqlParameter("@ProcedureIds", SqlDbType.VarChar, 2000);
            param[2].Value = ProcedureIds;

            param[3] = new SqlParameter("@result", SqlDbType.VarChar, 50);
            param[3].Direction = ParameterDirection.Output;


            SqlHelper.ExecuteNonQuery(SqlHelper.GetConnectionString(), CommandType.StoredProcedure, "setReportParamAssignments", param);
            result = Convert.ToInt32(param[3].Value);

        }
        catch (Exception ex)
        {
            // Common.SaveTextToFile(ex.ToString());

        }
        finally
        {

        }
    }
    public DataSet getpaymentsummary(string practiceid, int branchid, string InsGrpId, int reportid, string fromdate, string todate, string previousdata)
    {
        DataSet ds = new DataSet();
        SqlParameter[] param = new SqlParameter[7];
        try
        {
            param[0] = new SqlParameter("@practiceid", SqlDbType.VarChar);
            param[0].Value = practiceid;
            param[1] = new SqlParameter("@branchid", SqlDbType.Int);
            param[1].Value = branchid;
            param[2] = new SqlParameter("@InsGrpId", SqlDbType.VarChar, 10);
            param[2].Value = InsGrpId;
            param[3] = new SqlParameter("@reportid", SqlDbType.Int);
            param[3].Value = reportid;

            param[4] = new SqlParameter("@fromdate", SqlDbType.DateTime);
            if (fromdate.ToString().Trim() != "")
                param[4].Value = Convert.ToDateTime(fromdate);
            else
                param[4].Value = DBNull.Value;


            param[5] = new SqlParameter("@todate", SqlDbType.DateTime);
            if (todate.ToString().Trim() != "")
                param[5].Value = Convert.ToDateTime(todate);
            else
                param[5].Value = DBNull.Value;


            param[6] = new SqlParameter("@previousdata", SqlDbType.VarChar);
            param[6].Value = previousdata;

            ds = SqlHelper.ExecuteDataset(SqlHelper.GetConnectionString(), CommandType.StoredProcedure, "getpaymentsummary", param);
            return ds;
        }
        catch (Exception ex)
        {
            // Common.SaveTextToFile(ex.ToString());

            return ds;
        }
        finally
        {
            ds = null;
            param = null;
        }
    }
    public DataSet GetReportMemoParam(string practiceid)
    {
        DataSet ds = new DataSet();
        try
        {

            SqlParameter param = new SqlParameter("@practiceid", SqlDbType.VarChar);
            param.Value = practiceid;


            ds = SqlHelper.ExecuteDataset(SqlHelper.GetConnectionString(), CommandType.StoredProcedure, "getreportmemoparams", param);
            return ds;
        }
        catch (Exception ex)
        {
            // Common.SaveTextToFile(ex.ToString());

            return ds;
        }
        finally
        {
            ds = null;
        }
    }
    public DataSet GetReportMemoParamAssignments(string practiceid, int reportparamid)
    {
        DataSet ds = new DataSet();
        try
        {
            SqlParameter[] param = new SqlParameter[2];
            param[0] = new SqlParameter("@practiceid", SqlDbType.VarChar);
            param[0].Value = practiceid;

            param[1] = new SqlParameter("@reportparamid", SqlDbType.Int);
            param[1].Value = reportparamid;

            ds = SqlHelper.ExecuteDataset(SqlHelper.GetConnectionString(), CommandType.StoredProcedure, "getReportmemoParamAssignments", param);
            return ds;
        }
        catch (Exception ex)
        {
            // Common.SaveTextToFile(ex.ToString());

            return ds;
        }
        finally
        {

        }
    }
    public void SetReportMemoParamAssignments(string practiceid, int reportParamid, string ProcedureIds, ref int result)
    {

        try
        {

            SqlParameter[] param = new SqlParameter[4];
            param[0] = new SqlParameter("@practiceid", SqlDbType.VarChar);
            param[0].Value = practiceid;


            param[1] = new SqlParameter("@reportParamid", SqlDbType.Int);
            param[1].Value = reportParamid;

            param[2] = new SqlParameter("@ProcedureIds", SqlDbType.VarChar, 2000);
            param[2].Value = ProcedureIds;

            param[3] = new SqlParameter("@result", SqlDbType.VarChar, 50);
            param[3].Direction = ParameterDirection.Output;

            SqlHelper.ExecuteNonQuery(SqlHelper.GetConnectionString(), CommandType.StoredProcedure, "setreportMemoParamAssignments", param);
            result = Convert.ToInt32(param[3].Value);
        }
        catch (Exception ex)
        {
            // Common.SaveTextToFile(ex.ToString());


        }
        finally
        {

        }
    }
    public DataSet bindReportDashboard(string practiceid, string proceduceName)
    {
        DataSet ds = new DataSet();
        SqlParameter[] param = new SqlParameter[2];
        try
        {
            param[0] = new SqlParameter("@practiceid", SqlDbType.VarChar);
            param[0].Value = practiceid;

            param[1] = new SqlParameter("@isClientSide", SqlDbType.Int);
            param[1].Value = 0;

            ds = SqlHelper.ExecuteDataset(SqlHelper.GetConnectionString(), CommandType.StoredProcedure, proceduceName, param);
            return ds;
        }
        catch (Exception ex)
        {
            // Common.SaveTextToFile(ex.ToString());

            return ds;
        }
        finally
        {
            ds = null;
            param = null;
        }
    }
    public DataSet getDrilldownPmtReport(string practiceid, string month, int grp, string fromdate, string todate)
    {
        DataSet ds = new DataSet();
        try
        {
            SqlParameter[] param = new SqlParameter[5];
            param[0] = new SqlParameter("@practiceid", SqlDbType.VarChar);
            param[0].Value = practiceid;

            param[1] = new SqlParameter("@month", SqlDbType.VarChar, 50);
            param[1].Value = month;

            param[2] = new SqlParameter("@grp", SqlDbType.Int);
            param[2].Value = grp;

            param[3] = new SqlParameter("@fromdate", SqlDbType.DateTime);
            if (fromdate.ToString().Trim() != "")
                param[3].Value = Convert.ToDateTime(fromdate);
            else
                param[3].Value = DBNull.Value;


            param[4] = new SqlParameter("@todate", SqlDbType.DateTime);
            if (todate.ToString().Trim() != "")
                param[4].Value = Convert.ToDateTime(todate);
            else
                param[4].Value = DBNull.Value;


            ds = SqlHelper.ExecuteDataset(SqlHelper.GetConnectionString(), CommandType.StoredProcedure, "getDrilldownPmtReport", param);
            return ds;
        }
        catch (Exception ex)
        {
            // Common.SaveTextToFile(ex.ToString());

            return ds;
        }
        finally
        {

        }
    }

    // Get Dashboard From DataBase
    // Get Dashboard From DataBase
    public DataSet getDashboardDate(string practiceid, string cAutoId)
    {
        DataSet ds = new DataSet();
        SqlParameter[] param = new SqlParameter[3];
        string datestring = "";
        string medlink = objappRepo.Decryptdata(HttpContext.Current.Request.Cookies["SM_FACILITY"]["Uname"].ToString());
        try
        {
            param[0] = new SqlParameter("@practiceid", SqlDbType.VarChar);
            param[0].Value = practiceid;

            param[1] = new SqlParameter("@isMedlink ", SqlDbType.Int);
            param[1].Value = medlink == "MedlinkDemo" ? 1 : 0;

            param[2] = new SqlParameter("@Cautoid", SqlDbType.VarChar, 100);
            param[2].Value = cAutoId;

            ds = SqlHelper.ExecuteDataset(SqlHelper.GetConnectionString(), CommandType.StoredProcedure, "GetDashboardDate", param);
            //if (ds.Tables.Count > 0)
            //{
            //    if (ds.Tables[0].Rows.Count > 0)
            //    {
            //        datestring = ds.Tables[0].Rows[0]["DateString"].ToString().Trim();
            //    }
            //}
            //return datestring;
            return ds;
        }
        catch (Exception ex)
        {
            // Common.SaveTextToFile(ex.ToString());
            return null;
        }
        finally
        {
            ds = null;
            param = null;
            datestring = null;
        }
    }

    // For Deficiency Run Summary & Detail
    public DataSet getDeficiencyRuns(string practiceid, string fromdate, string todate, string deficiencycode, string InsGrpId, string providercode, string procedureName, string cAutoId)
    {
        DataSet ds = new DataSet();
        SqlParameter[] param = new SqlParameter[8];
        string medlink = objappRepo.Decryptdata(HttpContext.Current.Request.Cookies["SM_FACILITY"]["Uname"].ToString());
        try
        {
            param[0] = new SqlParameter("@practiceid", SqlDbType.VarChar);
            param[0].Value = practiceid;

            param[1] = new SqlParameter("@fromdate", SqlDbType.DateTime);
            if (fromdate.ToString().Trim() != "")
                param[1].Value = Convert.ToDateTime(fromdate);
            else
                param[1].Value = DBNull.Value;


            param[2] = new SqlParameter("@todate", SqlDbType.DateTime);
            if (todate.ToString().Trim() != "")
                param[2].Value = Convert.ToDateTime(todate);
            else
                param[2].Value = DBNull.Value;

            param[3] = new SqlParameter("@InsGrpId", SqlDbType.VarChar, 8000);
            param[3].Value = InsGrpId;

            param[4] = new SqlParameter("@deficiencycode", SqlDbType.VarChar);
            param[4].Value = deficiencycode;

            param[5] = new SqlParameter("@providercode", SqlDbType.VarChar);
            param[5].Value = providercode;

            param[6] = new SqlParameter("@isMedlink ", SqlDbType.Int);
            param[6].Value = medlink == "MedlinkDemo" ? 1 : 0;

            param[7] = new SqlParameter("@Cautoid", SqlDbType.VarChar, 100);
            param[7].Value = cAutoId;


            ds = SqlHelper.ExecuteDataset(SqlHelper.GetConnectionString(), CommandType.StoredProcedure, procedureName, param);

            return ds;
        }
        catch (Exception ex)
        {
            // Common.SaveTextToFile(ex.ToString());
            return ds;
        }
        finally
        {
            ds = null;
            param = null;
        }
    }

    // For Insurance Aged Trial Balance By Payer Group Pop-up
    public DataSet getPayers(string practiceid, string fromdate, string todate, int reportid, string PayerGroup, string previousdata)
    {
        DataSet ds = new DataSet();
        SqlParameter[] param = new SqlParameter[6];
        try
        {
            param[0] = new SqlParameter("@practiceid", SqlDbType.VarChar);
            param[0].Value = practiceid;

            param[1] = new SqlParameter("@fromdate", SqlDbType.DateTime);
            if (fromdate.ToString().Trim() != "")
                param[1].Value = Convert.ToDateTime(fromdate);
            else
                param[1].Value = DBNull.Value;


            param[2] = new SqlParameter("@todate", SqlDbType.DateTime);
            if (todate.ToString().Trim() != "")
                param[2].Value = Convert.ToDateTime(todate);
            else
                param[2].Value = DBNull.Value;

            param[3] = new SqlParameter("@reportid", SqlDbType.Int);
            param[3].Value = reportid;

            param[4] = new SqlParameter("@PayerGroup", SqlDbType.VarChar);
            param[4].Value = PayerGroup;

            param[5] = new SqlParameter("@previousdata", SqlDbType.VarChar);
            param[5].Value = previousdata;



            ds = SqlHelper.ExecuteDataset(SqlHelper.GetConnectionString(), CommandType.StoredProcedure, "getPayers", param);

            return ds;
        }
        catch (Exception ex)
        {
            // Common.SaveTextToFile(ex.ToString());
            return ds;
        }
        finally
        {
            ds = null;
            param = null;
        }
    }

    // For Insurance Aged Trial Balance By Payer Group Drill Down Chart 
    public DataSet getAgedTrialBalanceByPayer(string practiceid, int branchid, string InsGrpId, int reportid, string fromdate, string todate, string previousdata, string procedureName, string cAutoId)
    {
        DataSet ds = new DataSet();
        SqlParameter[] param = new SqlParameter[9];
        string medlink = objappRepo.Decryptdata(HttpContext.Current.Request.Cookies["SM_FACILITY"]["Uname"].ToString());
        try
        {
            param[0] = new SqlParameter("@practiceid", SqlDbType.VarChar);
            param[0].Value = practiceid;
            param[1] = new SqlParameter("@branchid", SqlDbType.Int);
            param[1].Value = branchid;
            param[2] = new SqlParameter("@InsGrpId", SqlDbType.VarChar);
            param[2].Value = InsGrpId;
            param[3] = new SqlParameter("@reportid", SqlDbType.Int);
            param[3].Value = reportid;

            param[4] = new SqlParameter("@fromdate", SqlDbType.DateTime);
            if (fromdate.ToString().Trim() != "")
                param[4].Value = Convert.ToDateTime(fromdate);
            else
                param[4].Value = DBNull.Value;


            param[5] = new SqlParameter("@todate", SqlDbType.DateTime);
            if (todate.ToString().Trim() != "")
                param[5].Value = Convert.ToDateTime(todate);
            else
                param[5].Value = DBNull.Value;


            param[6] = new SqlParameter("@previousdata", SqlDbType.VarChar);
            param[6].Value = previousdata;

            param[7] = new SqlParameter("@isMedlink ", SqlDbType.Int);
            param[7].Value = medlink == "MedlinkDemo" ? 1 : 0;

            param[8] = new SqlParameter("@Cautoid", SqlDbType.VarChar, 100);
            param[8].Value = cAutoId;

            ds = SqlHelper.ExecuteDataset(SqlHelper.GetConnectionString(), CommandType.StoredProcedure, procedureName, param);
            return ds;
        }
        catch (Exception ex)
        {
            // Common.SaveTextToFile(ex.ToString());

            return ds;
        }
        finally
        {
            ds = null;
            param = null;
        }
    }

    // Get Target Value list acc to parameter
    public DataSet getTargetValue(string practiceid, int id, int year)
    {
        DataSet ds = new DataSet();
        SqlParameter[] param = new SqlParameter[3];
        try
        {
            param[0] = new SqlParameter("@practiceid", SqlDbType.VarChar);
            param[0].Value = practiceid;
            param[1] = new SqlParameter("@id", SqlDbType.Int);
            param[1].Value = id;
            param[2] = new SqlParameter("@year", SqlDbType.Int);
            param[2].Value = year;

            ds = SqlHelper.ExecuteDataset(SqlHelper.GetConnectionString(), CommandType.StoredProcedure, "getTargetValue", param);
            return ds;
        }
        catch (Exception ex)
        {
            // Common.SaveTextToFile(ex.ToString());

            return ds;
        }
        finally
        {
            ds = null;
        }
    }


    // For get Report Name of Grid
    public DataSet getReportName(string practiceid, int id, string fromdate, string todate, string type, int date, string cAutoId)
    {
        DataSet ds = new DataSet();
        SqlParameter[] param = new SqlParameter[8];
        string medlink = objappRepo.Decryptdata(HttpContext.Current.Request.Cookies["SM_FACILITY"]["Uname"].ToString());
        try
        {
            param[0] = new SqlParameter("@practiceid", SqlDbType.VarChar);
            param[0].Value = practiceid;
            param[1] = new SqlParameter("@reportid", SqlDbType.Int);
            param[1].Value = id;
            param[2] = new SqlParameter("@fromdate", SqlDbType.DateTime);

            if (fromdate.ToString().Trim() != "")
                param[2].Value = Convert.ToDateTime(fromdate);
            else
                param[2].Value = DBNull.Value;


            param[3] = new SqlParameter("@todate", SqlDbType.DateTime);
            if (todate.ToString().Trim() != "")
                param[3].Value = Convert.ToDateTime(todate);
            else
                param[3].Value = DBNull.Value;

            param[4] = new SqlParameter("@type", SqlDbType.VarChar, 100);
            param[4].Value = type;

            param[5] = new SqlParameter("@date", SqlDbType.Int);
            param[5].Value = date;

            param[6] = new SqlParameter("@isMedlink ", SqlDbType.Int);
            param[6].Value = medlink == "MedlinkDemo" ? 1 : 0;

            param[7] = new SqlParameter("@Cautoid", SqlDbType.VarChar, 100);
            param[7].Value = cAutoId;

            ds = SqlHelper.ExecuteDataset(SqlHelper.GetConnectionString(), CommandType.StoredProcedure, "getReportName", param);
            return ds;
        }
        catch (Exception ex)
        {
            // Common.SaveTextToFile(ex.ToString());

            return ds;
        }
        finally
        {
            ds = null;
        }
    }

    // Trage Name List for Dropdown
    public DataSet getTargetValueMaster()
    {
        DataSet ds = new DataSet();

        try
        {

            ds = SqlHelper.ExecuteDataset(SqlHelper.GetConnectionString(), CommandType.StoredProcedure, "getTargetValueMaster");
            return ds;
        }
        catch (Exception ex)
        {
            // Common.SaveTextToFile(ex.ToString());

            return ds;
        }
        finally
        {
            ds = null;
        }
    }

    /// <summary>
    /// For Target setting insert
    /// </summary>
    /// <param name="dt"></param>
    public void setTargetValue(DataTable dt)
    {
        SqlParameter[] param = new SqlParameter[1];
        try
        {

            param[0] = new SqlParameter("@TVP", SqlDbType.Structured);
            param[0].Value = dt;
            SqlHelper.ExecuteNonQuery(SqlHelper.GetConnectionString(), CommandType.StoredProcedure, "setTargetValue", param);

        }
        catch (Exception ex)
        {
            // Common.SaveTextToFile(ex.ToString());


        }
        finally
        {
        }
    }

    // For Analysis By Transaction Date DrillDown Report
    public DataSet getAnalysisByTransactionDateDrillDown(string practiceid, string InsGrpId, int reportid, string fromdate, string todate, string type, int date)
    {
        DataSet ds = new DataSet();
        SqlParameter[] param = new SqlParameter[7];
        try
        {
            param[0] = new SqlParameter("@practiceid", SqlDbType.VarChar);
            param[0].Value = practiceid;

            param[1] = new SqlParameter("@InsGrpId", SqlDbType.VarChar, 8000);
            param[1].Value = InsGrpId;
            param[2] = new SqlParameter("@reportid", SqlDbType.Int);
            param[2].Value = reportid;

            param[3] = new SqlParameter("@fromdate", SqlDbType.DateTime);
            if (fromdate.ToString().Trim() != "")
                param[3].Value = Convert.ToDateTime(fromdate);
            else
                param[3].Value = DBNull.Value;


            param[4] = new SqlParameter("@todate", SqlDbType.DateTime);
            if (todate.ToString().Trim() != "")
                param[4].Value = Convert.ToDateTime(todate);
            else
                param[4].Value = DBNull.Value;


            param[5] = new SqlParameter("@type", SqlDbType.VarChar);
            param[5].Value = type;

            param[6] = new SqlParameter("@date", SqlDbType.Int);
            param[6].Value = date;

            ds = SqlHelper.ExecuteDataset(SqlHelper.GetConnectionString(), CommandType.StoredProcedure, "getAnalysisByTransactionDateDrillDown", param);
            return ds;
        }
        catch (Exception ex)
        {
            // Common.SaveTextToFile(ex.ToString());

            return ds;
        }
        finally
        {
            ds = null;
            param = null;
        }
    }



    public DataSet getLocationFilter(string fromdate, string todate, string ids, string reportid)
    {
        try
        {

            DataSet dst;
            SqlParameter[] arparam = new SqlParameter[4];

            arparam[0] = new SqlParameter("@fromdate", SqlDbType.DateTime);
            if (fromdate != "")
                arparam[0].Value = fromdate;
            else
                arparam[0].Value = DBNull.Value;
            arparam[1] = new SqlParameter("@todate", SqlDbType.DateTime);
            if (todate != "")
                arparam[1].Value = todate;
            else
                arparam[1].Value = DBNull.Value; ;
            arparam[2] = new SqlParameter("@ids", SqlDbType.VarChar, 8000);
            arparam[2].Value = ids;
            arparam[3] = new SqlParameter("@reportid", SqlDbType.Int);
            arparam[3].Value = reportid;

            return SqlHelper.ExecuteDataset(SqlHelper.GetConnectionString(), CommandType.StoredProcedure, "getFilter", arparam);
        }
        catch (Exception ex)
        {
            // Common.SaveTextToFile(ex.ToString());
            return null;
        }
    }


    public DataSet getlocationsBasedTextReport(string practiceid, string InsGrpId, string fromdate, string todate, int reportid, string previousdata)
    {
        try
        {

            SqlParameter[] param = new SqlParameter[6];

            param[0] = new SqlParameter("@practiceID", SqlDbType.VarChar);
            param[0].Value = practiceid;

            param[1] = new SqlParameter("@InsGrpId", SqlDbType.VarChar);
            param[1].Value = InsGrpId;
            param[2] = new SqlParameter("@reportid", SqlDbType.Int);
            param[2].Value = reportid;

            param[3] = new SqlParameter("@fromdate", SqlDbType.DateTime);
            if (fromdate.ToString().Trim() != "")
                param[3].Value = Convert.ToDateTime(fromdate);
            else
                param[3].Value = DBNull.Value;


            param[4] = new SqlParameter("@todate", SqlDbType.DateTime);
            if (todate.ToString().Trim() != "")
                param[4].Value = Convert.ToDateTime(todate);
            else
                param[4].Value = DBNull.Value;
            param[5] = new SqlParameter("@previousdata", SqlDbType.VarChar);
            param[5].Value = previousdata;

            return SqlHelper.ExecuteDataset(SqlHelper.GetConnectionString(), CommandType.StoredProcedure, "getlocationsBasedTextReport", param);
        }
        catch (Exception ex)
        {
            // Common.SaveTextToFile(ex.ToString());
            return null;
        }
    }


    public DataSet getCustomFiscalYearforDropDown(string practiceid)
    {
        try
        {

            DataSet dst;
            SqlParameter[] arparam = new SqlParameter[1];


            arparam[0] = new SqlParameter("@practiceid", SqlDbType.VarChar);
            arparam[0].Value = practiceid;

            return SqlHelper.ExecuteDataset(SqlHelper.GetConnectionString(), CommandType.StoredProcedure, "getCustomFiscalYearforDropDown", arparam);
        }
        catch (Exception ex)
        {
            // Common.SaveTextToFile(ex.ToString());
            return null;
        }
    }

    public DataSet getsetCustomFiscalYearPracticeid(long SAutoId)
    {
        try
        {
            SqlParameter[] arparam = new SqlParameter[1];


            arparam[0] = new SqlParameter("@SAutoId", SqlDbType.BigInt);
            arparam[0].Value = SAutoId;

            return SqlHelper.ExecuteDataset(SqlHelper.GetConnectionString(), CommandType.StoredProcedure, "getsetCustomFiscalYearPracticeid", arparam);
        }
        catch (Exception ex)
        {
            // Common.SaveTextToFile(ex.ToString());
            return null;
        }
    }


    public void setCustomFiscalYearPracticeid(DataTable dt)
    {
        SqlParameter[] param = new SqlParameter[1];
        try
        {

            param[0] = new SqlParameter("@TVP", SqlDbType.Structured);
            param[0].Value = dt;
            SqlHelper.ExecuteNonQuery(SqlHelper.GetConnectionString(), CommandType.StoredProcedure, "setCustomFiscalYearPracticeid", param);

        }
        catch (Exception ex)
        {
            // Common.SaveTextToFile(ex.ToString());
        }
        finally
        {
        }
    }


    public DataSet getFeedback()
    {
        //SqlParameter[] param = new SqlParameter[4];
        try
        {

            // [getFeedback](@name varchar(300)='',@emailId varchar(300)='',@fromdate datetime='',@todate datetime='')
            //param[0] = new SqlParameter("@name", SqlDbType.VarChar,300);
            //param[0].Value = "";
            //param[1] = new SqlParameter("@emailId", SqlDbType.VarChar, 300);
            //param[1].Value = "";
            //param[2] = new SqlParameter("@fromdate", SqlDbType.DateTime);
            //param[2].Value = DBNull.Value;
            //param[2] = new SqlParameter("@todate", SqlDbType.DateTime);
            //param[2].Value = DBNull.Value;
            return SqlHelper.ExecuteDataset(SqlHelper.GetConnectionString(), CommandType.StoredProcedure, "getFeedback");
        }
        catch (Exception ex)
        {
            // Common.SaveTextToFile(ex.ToString());
            return null;
        }

    }

    public string getFacilitiesForDropdown(string practiceids)
    {
        string StrJson = "";
        string SAutoID = objappRepo.Decryptdata(HttpContext.Current.Request.Cookies["SM_USER"].Value);
        string CAutoID = objappRepo.Decryptdata(HttpContext.Current.Request.Cookies["CAutoID"].Value);
        DataSet dst = new DataSet();
        try
        {
            SqlParameter[] param = new SqlParameter[3];
            param[0] = new SqlParameter("@SAutoID", SqlDbType.VarChar, 200);
            param[0].Value = SAutoID;
            param[1] = new SqlParameter("@practiceids", SqlDbType.VarChar);
            param[1].Value = practiceids;
            param[2] = new SqlParameter("@CAutoID", SqlDbType.VarChar, 200);
            param[2].Value = CAutoID;

            dst = SqlHelper.ExecuteDataset(SqlHelper.GetConnectionString(), CommandType.StoredProcedure, "getFacilitiesForDropdown", param);

            List<TreeCarrier> objlisttree = new List<TreeCarrier>();
            for (int i = 0; i <= dst.Tables[0].Rows.Count - 1; i++)
            {
                TreeCarrier objtree = new TreeCarrier();
                objtree.Name = dst.Tables[0].Rows[i]["practicename"].ToString().Trim();
                objtree.Insgrpid = (dst.Tables[0].Rows[i]["practiceid"].ToString().Trim());
                objtree.ischeck = Convert.ToInt32(dst.Tables[0].Rows[i]["ischeck"].ToString());
                objtree.parentId = dst.Tables[0].Rows[i]["parentid"].ToString().Trim();
                objtree.LEVEL_DEPTH = Convert.ToInt32(dst.Tables[0].Rows[i]["level_depth"].ToString());
                objlisttree.Add(objtree);
            }
            if (objlisttree.Count > 0)
            {

                StrJson = "   [{";
                StrJson += "\"id\":" + objlisttree[0].Insgrpid + ",";
                StrJson += "\"text\":\"" + objlisttree[0].Name + "\",";
                StrJson += getCarrierChildren(objlisttree[0].Insgrpid, objlisttree);

                StrJson += "}]";
            }
        }
        catch (Exception ex)
        {
            // Common.SaveTextToFile(ex.ToString());
            StrJson = "[]";
        }
        return StrJson;
    }



    public DataSet getCPTCodesByState(string practiceid, int branchid, string InsGrpId, int reportid, string fromdate, string todate, string previousdata, string locationids)
    {
        DataSet ds = new DataSet();
        SqlParameter[] param = new SqlParameter[8];
        string medlink = objappRepo.Decryptdata(HttpContext.Current.Request.Cookies["SM_FACILITY"]["Uname"].ToString());
        try
        {
            param[0] = new SqlParameter("@practiceid", SqlDbType.VarChar);
            param[0].Value = practiceid;
            param[1] = new SqlParameter("@branchid", SqlDbType.Int);
            param[1].Value = branchid;
            param[2] = new SqlParameter("@InsGrpId", SqlDbType.VarChar);
            param[2].Value = InsGrpId;
            param[3] = new SqlParameter("@reportid", SqlDbType.Int);
            param[3].Value = reportid;

            param[4] = new SqlParameter("@fromdate", SqlDbType.DateTime);
            if (fromdate.ToString().Trim() != "")
                param[4].Value = Convert.ToDateTime(fromdate);
            else
                param[4].Value = DBNull.Value;


            param[5] = new SqlParameter("@todate", SqlDbType.DateTime);
            if (todate.ToString().Trim() != "")
                param[5].Value = Convert.ToDateTime(todate);
            else
                param[5].Value = DBNull.Value;


            param[6] = new SqlParameter("@previousdata", SqlDbType.VarChar);
            param[6].Value = previousdata;


            param[7] = new SqlParameter("@locationids", SqlDbType.VarChar, 8000);
            param[7].Value = locationids;



            ds = SqlHelper.ExecuteDataset(SqlHelper.GetConnectionString(), CommandType.StoredProcedure, "getCPTCodesByState", param);

            return ds;
        }
        catch (Exception ex)
        {
            // Common.SaveTextToFile(ex.ToString());
            return ds;
        }
        finally
        {
            ds = null;
            param = null;
        }
    }





    #region Projection Reports

    public DataSet getOtherSystemCharges(string typeID)
    {
        SqlParameter[] param = new SqlParameter[1];
        try
        {
            //param[0] = new SqlParameter("@year", SqlDbType.VarChar, 10);
            //param[0].Value = Year;
            if (typeID == "2")
                return SqlHelper.ExecuteDataset(SqlHelper.GetConnectionString(), CommandType.StoredProcedure, "getOtherSystemCharges");
            else
                return SqlHelper.ExecuteDataset(SqlHelper.GetConnectionString(), CommandType.StoredProcedure, "getOtherSystemPayments");
        }
        catch (Exception ex)
        {
            // Common.SaveTextToFile(ex.ToString());
            return null;
        }

    }



    public DataSet getAllClients()
    {

        try
        {
            return SqlHelper.ExecuteDataset(SqlHelper.GetConnectionString(), CommandType.StoredProcedure, "getAllClients");
        }
        catch (Exception ex)
        {
            // Common.SaveTextToFile(ex.ToString());
            return null;
        }

    }


    public void setOtherSystemCharges(DataTable dt, string typeId)
    {
        SqlParameter[] param = new SqlParameter[1];
        try
        {

            param[0] = new SqlParameter("@TVP", SqlDbType.Structured);
            param[0].Value = dt;

            if (typeId == "2")
                SqlHelper.ExecuteNonQuery(SqlHelper.GetConnectionString(), CommandType.StoredProcedure, "setOtherSystemCharges", param);
            else
                SqlHelper.ExecuteNonQuery(SqlHelper.GetConnectionString(), CommandType.StoredProcedure, "setOtherSystemPayments", param);

        }
        catch (Exception ex)
        {
            // Common.SaveTextToFile(ex.ToString());


        }
        finally
        {
        }
    }


    public DataSet getProjectedRPR()
    {
        SqlParameter[] param = new SqlParameter[1];
        try
        {
            //param[0] = new SqlParameter("@year", SqlDbType.VarChar, 10);
            //param[0].Value = Year;

            return SqlHelper.ExecuteDataset(SqlHelper.GetConnectionString(), CommandType.StoredProcedure, "getProjectedRPR");
        }
        catch (Exception ex)
        {
            // Common.SaveTextToFile(ex.ToString());
            return null;
        }

    }

    public void setProjectedRPR(DataTable dt)
    {
        SqlParameter[] param = new SqlParameter[1];
        try
        {

            param[0] = new SqlParameter("@TVP", SqlDbType.Structured);
            param[0].Value = dt;
            SqlHelper.ExecuteNonQuery(SqlHelper.GetConnectionString(), CommandType.StoredProcedure, "setProjectedRPR", param);


        }
        catch (Exception ex)
        {
            // Common.SaveTextToFile(ex.ToString());


        }
        finally
        {
        }
    }

    public DataSet getProjectionChargesPayments(string date, string procedureName)
    {
        SqlParameter[] param = new SqlParameter[1];
        try
        {

            param[0] = new SqlParameter("@date", SqlDbType.DateTime);
            if (date != "")
                param[0].Value = Convert.ToDateTime(date);
            else
                param[0].Value = DBNull.Value;
            return SqlHelper.ExecuteDataset(SqlHelper.GetConnectionString(), CommandType.StoredProcedure, procedureName, param);


        }
        catch (Exception ex)
        {
            // Common.SaveTextToFile(ex.ToString());
            return null;

        }

    }


    public DataSet getActualVsBenchmark(string fromdate, string todate)
    {
        SqlParameter[] param = new SqlParameter[2];
        try
        {

            param[0] = new SqlParameter("@fromdate", SqlDbType.DateTime);
            if (fromdate != "")
                param[0].Value = Convert.ToDateTime(fromdate);
            else
                param[0].Value = DBNull.Value;
            param[1] = new SqlParameter("@todate", SqlDbType.DateTime);
            if (todate != "")
                param[1].Value = Convert.ToDateTime(todate);
            else
                param[1].Value = DBNull.Value;

            return SqlHelper.ExecuteDataset(SqlHelper.GetConnectionString(), CommandType.StoredProcedure, "getActualVsBenchmark", param);


        }
        catch (Exception ex)
        {
            // Common.SaveTextToFile(ex.ToString());
            return null;

        }

    }

    public void SetClients(DataTable dt)
    {
        SqlParameter[] param = new SqlParameter[1];
        try
        {

            param[0] = new SqlParameter("@TVP", SqlDbType.Structured);
            param[0].Value = dt;
            SqlHelper.ExecuteNonQuery(SqlHelper.GetConnectionString(), CommandType.StoredProcedure, "SetClients", param);


        }
        catch (Exception ex)
        {
            // Common.SaveTextToFile(ex.ToString());


        }
        finally
        {
        }
    }



    #endregion

    #region User Management
    public string GetUsersForCAutoId(int CAutoId)
    {
        string result = "";
        DataSet ds = new DataSet();
        SqlParameter[] param = new SqlParameter[1];
        try
        {
            param[0] = new SqlParameter("@CAutoId", SqlDbType.Int);
            param[0].Value = CAutoId;

            ds = SqlHelper.ExecuteDataset(SqlHelper.GetConnectionString(), CommandType.StoredProcedure, "RoleManagement_GetUsersForCAutoId", param);

            if (ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    result = "{\"Table\":[";
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {

                        for (int j = 0; j < ds.Tables[0].Columns.Count; j++)
                        {

                            if (j == 0)
                                result += "{ \"" + ds.Tables[0].Columns[j].ColumnName + "\":\"" + ds.Tables[0].Rows[i][j].ToString();
                            else if (j + 1 == ds.Tables[0].Columns.Count)
                            {
                                if (i + 1 == ds.Tables[0].Rows.Count)
                                    result += "\", \"" + ds.Tables[0].Columns[j].ColumnName + "\":\"" + ds.Tables[0].Rows[i][j].ToString() + "\"}";
                                else
                                    result += "\", \"" + ds.Tables[0].Columns[j].ColumnName + "\":\"" + ds.Tables[0].Rows[i][j].ToString() + "\"},";
                            }
                            else
                                result += "\", \"" + ds.Tables[0].Columns[j].ColumnName + "\":\"" + ds.Tables[0].Rows[i][j].ToString();
                        }


                    }
                    result += "]}";
                }
            }
        }
        catch (Exception ex)
        {
            result = "";
        }
        return result;
    }


    public string GetFacilitiesNameForCAutoId(int ParentCAutoId, int ChildCAutoId)
    {
        string result = "";
        SqlParameter[] param = new SqlParameter[2];
        try
        {
            DataSet ds = new DataSet();
            param[0] = new SqlParameter("@ParentCAutoId", SqlDbType.Int);
            param[0].Value = ParentCAutoId;
            param[1] = new SqlParameter("@ChildCAutoId", SqlDbType.Int);
            param[1].Value = ChildCAutoId;

            ds = SqlHelper.ExecuteDataset(SqlHelper.GetConnectionString(), CommandType.StoredProcedure, "RoleManagement_GetFacilitiesNameForCAutoId", param);

            if (ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    result = "{\"Table\":[";
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {

                        for (int j = 0; j < ds.Tables[0].Columns.Count; j++)
                        {

                            if (j == 0)
                                result += "{ \"" + ds.Tables[0].Columns[j].ColumnName + "\":\"" + ds.Tables[0].Rows[i][j].ToString();
                            else if (j + 1 == ds.Tables[0].Columns.Count)
                            {
                                if (i + 1 == ds.Tables[0].Rows.Count)
                                    result += "\", \"" + ds.Tables[0].Columns[j].ColumnName + "\":\"" + ds.Tables[0].Rows[i][j].ToString() + "\"}";
                                else
                                    result += "\", \"" + ds.Tables[0].Columns[j].ColumnName + "\":\"" + ds.Tables[0].Rows[i][j].ToString() + "\"},";
                            }
                            else
                                result += "\", \"" + ds.Tables[0].Columns[j].ColumnName + "\":\"" + ds.Tables[0].Rows[i][j].ToString();
                        }

                    }
                    result += "]}";
                }
            }

        }
        catch (Exception ex)
        {
            result = "";
        }
        return result;
    }

    // Check User Name is Exist or Not
    public string CheckUsername(string username)
    {
        string error = "error";
        SqlParameter[] param = new SqlParameter[2];
        try
        {
            param[0] = new SqlParameter("@username", SqlDbType.VarChar, 100);
            param[0].Value = username;

            param[1] = new SqlParameter("@errormsg", SqlDbType.VarChar, 250);
            param[1].Direction = ParameterDirection.Output;

            SqlHelper.ExecuteNonQuery(SqlHelper.GetConnectionString(), CommandType.StoredProcedure, "RoleManagement_CheckUsername", param);
            error = param[1].Value.ToString();
        }
        catch (Exception ex)
        {
            error = "error";
        }
        return error;
    }
    // Set Facilities and User with Login Password
    public int setFacilitiesNameForCAutoId(int ParentCAutoId, int ChildCAutoId, string practiceids, string userName, string Password, int isActive, ref
 string error, string SAutoID, int RoleId, int Showallfacilities)
    {
        error = "error";
        SqlParameter[] param = new SqlParameter[11];
        try
        {
            param[0] = new SqlParameter("@ParentCAutoId", SqlDbType.Int);
            param[0].Value = ParentCAutoId;

            param[1] = new SqlParameter("@ChildCAutoId", SqlDbType.Int);
            param[1].Value = ChildCAutoId;

            param[2] = new SqlParameter("@practiceids", SqlDbType.VarChar);
            param[2].Value = practiceids;

            param[3] = new SqlParameter("@username", SqlDbType.VarChar, 100);
            param[3].Value = userName;

            param[4] = new SqlParameter("@password", SqlDbType.VarChar, 100);
            param[4].Value = Password;

            param[5] = new SqlParameter("@isactive", SqlDbType.Int);
            param[5].Value = isActive;

            param[6] = new SqlParameter("@result", SqlDbType.Int);
            param[6].Direction = ParameterDirection.Output;

            param[7] = new SqlParameter("@errormsg", SqlDbType.VarChar, 250);
            param[7].Direction = ParameterDirection.Output;

            param[8] = new SqlParameter("@parentSautoid", SqlDbType.VarChar, 100);
            param[8].Value = SAutoID;

            param[9] = new SqlParameter("@MenuRoleid", SqlDbType.Int);
            param[9].Value = RoleId;

            param[10] = new SqlParameter("@Showallfacilities", SqlDbType.Int);
            param[10].Value = Showallfacilities;



            SqlHelper.ExecuteNonQuery(SqlHelper.GetConnectionString(), CommandType.StoredProcedure, "RoleManagement_setFacilitiesForCAutoId", param);
            error = param[7].Value.ToString();
            return int.Parse(param[6].Value.ToString());
        }
        catch (Exception ex)
        {
            error = "error";
            return 1;
        }

    }



    //RoleManagement_ResetPassWordForCAutoId(@CAutoId int=-1,@password varchar(100)='',@result int =-1 output)
    public int ResetPassWordForCAutoId(int CAutoId, string password)
    {
        int result = 1;
        SqlParameter[] param = new SqlParameter[3];
        try
        {
            param[0] = new SqlParameter("@CAutoId", SqlDbType.Int);
            param[0].Value = CAutoId;

            param[1] = new SqlParameter("@password", SqlDbType.VarChar, 100);
            param[1].Value = password;

            param[2] = new SqlParameter("@result", SqlDbType.Int);
            param[2].Direction = ParameterDirection.Output;

            SqlHelper.ExecuteNonQuery(SqlHelper.GetConnectionString(), CommandType.StoredProcedure, "RoleManagement_ResetPassWordForCAutoId", param);
            result = int.Parse(param[2].Value.ToString());
        }
        catch (Exception ex)
        {
            result = 1;
        }
        return result;
    }

    // [RoleManagement_getMenuRole](@Roleid int=-1,@CAutoid int=-1,@IsGloballyVisible int =0, int =0,@SAutoid int =-1,@status int=-1)
    //[RoleManagement_getMenuRoleDetails](@Roleid int=-1,@CAutoid int=-1,@IsGloballyVisible int =0,@SAutoid int =-1 ,@status int=-1)
    public DataSet GetMenuRole(int Roleid, int CAutoId, int IsGloballyVisible, int SAutoID, int status, string ProcedureName)
    {
        DataSet ds = new DataSet();
        SqlParameter[] param = new SqlParameter[5];
        try
        {
            param[0] = new SqlParameter("@Roleid", SqlDbType.Int);
            param[0].Value = Roleid;

            param[1] = new SqlParameter("@CAutoid", SqlDbType.Int);
            param[1].Value = CAutoId;

            param[2] = new SqlParameter("@IsGloballyVisible", SqlDbType.Int);
            param[2].Value = IsGloballyVisible;

            param[3] = new SqlParameter("@SAutoID", SqlDbType.Int);
            param[3].Value = SAutoID;

            param[4] = new SqlParameter("@status", SqlDbType.Int);
            param[4].Value = status;

            ds = SqlHelper.ExecuteDataset(SqlHelper.GetConnectionString(), CommandType.StoredProcedure, ProcedureName, param);
        }
        catch (Exception ex)
        {

        }
        return ds;
    }


    //  [RoleManagement_setMenuRole](@Roleid int=-1,@CAutoid int=-1,@IsGloballyVisible int =0,@RoleName varchar(150),@isactive int =0,@isPhysician varchar(250),@isAmbulance varchar(250),@result int =-1 output,@outroleid int =-1 output, @errormsg varchar(250) output )
    public int SetMenuRole(int Roleid, int CAutoId, int IsGloballyVisible, string RoleName, string isAmbulance, string isPhysician, int isActive, ref
 string error)
    {
        error = "error";
        SqlParameter[] param = new SqlParameter[10];
        try
        {
            param[0] = new SqlParameter("@Roleid", SqlDbType.Int);
            param[0].Value = Roleid;

            param[1] = new SqlParameter("@CAutoid", SqlDbType.Int);
            param[1].Value = CAutoId;

            param[2] = new SqlParameter("@IsGloballyVisible", SqlDbType.Int);
            param[2].Value = IsGloballyVisible;


            param[3] = new SqlParameter("@RoleName", SqlDbType.VarChar, 150);
            param[3].Value = RoleName;

            param[4] = new SqlParameter("@isPhysician", SqlDbType.VarChar, 250);
            param[4].Value = isPhysician;

            param[5] = new SqlParameter("@isAmbulance", SqlDbType.VarChar, 250);
            param[5].Value = isAmbulance;

            param[6] = new SqlParameter("@isactive", SqlDbType.Int);
            param[6].Value = isActive;

            param[7] = new SqlParameter("@result", SqlDbType.Int);
            param[7].Direction = ParameterDirection.Output;

            param[8] = new SqlParameter("@outroleid", SqlDbType.Int);
            param[8].Direction = ParameterDirection.Output;

            param[9] = new SqlParameter("@errormsg", SqlDbType.VarChar, 250);
            param[9].Direction = ParameterDirection.Output;


            SqlHelper.ExecuteNonQuery(SqlHelper.GetConnectionString(), CommandType.StoredProcedure, "RoleManagement_setMenuRole", param);
            error = param[9].Value.ToString();
            return int.Parse(param[7].Value.ToString());
        }
        catch (Exception ex)
        {
            error = ex.Message;
            return 1;
        }

    }
    //RoleManagement_CheckRoleName (@RoleName varchar(150),@errormsg varchar(100)='' output)
    // Check Role Name is Exist or Not
    public string CheckRoleName(string RoleName, int RoleId)
    {
        string error = "error";
        SqlParameter[] param = new SqlParameter[3];
        try
        {
            param[0] = new SqlParameter("@RoleName", SqlDbType.VarChar, 100);
            param[0].Value = RoleName;

            param[1] = new SqlParameter("@RoleId", SqlDbType.Int);
            param[1].Value = RoleId;

            param[2] = new SqlParameter("@errormsg", SqlDbType.VarChar, 100);
            param[2].Direction = ParameterDirection.Output;

            SqlHelper.ExecuteNonQuery(SqlHelper.GetConnectionString(), CommandType.StoredProcedure, "RoleManagement_CheckRoleName", param);
            error = param[2].Value.ToString();
        }
        catch (Exception ex)
        {
            error = ex.Message;
        }
        return error;
    }
    #endregion


    #region Scheduler



    // Add or Update Schedule Details
    public void AddScheduleDetails(string report_id, string email_to, string report_type, string event_id, int email_id, DateTime startdate, string starttime, DateTime enddate, string endtime, string timezone, string repeat, int Repeattypeid, string endtimesummary, string @repeatesummary, string Timezonedisplay, string user_id, string facility_id, int isrepeat)
    {

        try
        {

            SqlParameter[] param = new SqlParameter[18];
            param[0] = new SqlParameter("@report_id", SqlDbType.VarChar);
            param[0].Value = report_id;


            param[1] = new SqlParameter("@email_to", SqlDbType.VarChar);
            param[1].Value = email_to;

            param[2] = new SqlParameter("@report_type", SqlDbType.VarChar);
            param[2].Value = report_type;

            param[3] = new SqlParameter("@event_id", SqlDbType.VarChar);
            param[3].Value = event_id;

            param[4] = new SqlParameter("@email_id", SqlDbType.VarChar);
            param[4].Value = email_id;

            param[5] = new SqlParameter("@start_date", SqlDbType.VarChar);
            param[5].Value = startdate;

            param[6] = new SqlParameter("@start_time", SqlDbType.VarChar);
            param[6].Value = starttime;

            param[7] = new SqlParameter("@end_date", SqlDbType.VarChar);
            param[7].Value = enddate;

            param[8] = new SqlParameter("@end_time", SqlDbType.VarChar);
            param[8].Value = endtime;

            param[9] = new SqlParameter("@timezone", SqlDbType.VarChar, 200);
            param[9].Value = timezone;

            param[10] = new SqlParameter("@repeat", SqlDbType.VarChar);
            param[10].Value = repeat;

            param[11] = new SqlParameter("@Repeattypeid", SqlDbType.Int);
            param[11].Value = Repeattypeid;


            param[12] = new SqlParameter("@endtimesummary", SqlDbType.VarChar, -1);
            param[12].Value = endtimesummary;


            param[13] = new SqlParameter("@repeatesummary", SqlDbType.VarChar, -1);
            param[13].Value = repeatesummary;

            param[14] = new SqlParameter("@Timezonedisplay", SqlDbType.VarChar, -1);
            param[14].Value = Timezonedisplay;

            param[15] = new SqlParameter("@userid", SqlDbType.VarChar);
            param[15].Value = user_id;

            param[16] = new SqlParameter("@facilityid", SqlDbType.VarChar);
            param[16].Value = facility_id;

            param[17] = new SqlParameter("@isrepeat", SqlDbType.Int);
            param[17].Value = isrepeat;


            SqlHelper.ExecuteNonQuery(SqlHelper.GetConnectionString(), CommandType.StoredProcedure, "EmailScheculer_AddScheduleDetails", param);


        }
        catch (Exception ex)
        {
            // Common.SaveTextToFile(ex.ToString());


        }
        finally
        {

        }
    }




    // Fetch Event Details
    public DataSet fetch_event_details(string event_id)
    {
        DataSet ds = new DataSet();
        SqlParameter[] param = new SqlParameter[1];

        try
        {
            param[0] = new SqlParameter("@event_id", SqlDbType.VarChar);
            param[0].Value = event_id;


            ds = SqlHelper.ExecuteDataset(SqlHelper.GetConnectionString(), CommandType.StoredProcedure, "EmailScheculer_fetch_event_details", param);
            return ds;
        }
        catch (Exception ex)
        {
            // Common.SaveTextToFile(ex.ToString());
            return ds;
        }
        finally
        {
            ds = null;

        }
    }

    // Fetch All Schedules
    public DataSet fetch_all_schedules(int pageno, int pagesize, out int totalCount)
    {
        DataSet ds = new DataSet();
        SqlParameter[] param = new SqlParameter[3];

        try
        {

            param[0] = new SqlParameter("@pageSize", SqlDbType.Int);
            param[0].Value = pagesize;

            param[1] = new SqlParameter("@pageNo", SqlDbType.Int);
            param[1].Value = pageno;

            param[2] = new SqlParameter("@totalCount", SqlDbType.Int);
            param[2].Direction = ParameterDirection.Output;



            ds = SqlHelper.ExecuteDataset(SqlHelper.GetConnectionString(), CommandType.StoredProcedure, "EmailScheculer_fetch_all_schedules", param);
            totalCount = Convert.ToInt32(param[2].Value.ToString());

            return ds;
        }
        catch (Exception ex)
        {
            // Common.SaveTextToFile("Function Name : fetch_all_schedules, Page Name : ChartDataClass, Error Message : " + ex.ToString());
            totalCount = 0;

            ds = null;
            return ds;
        }
        finally
        {
            ds = null;

        }
    }



    // Fetch All Email IDs for Auto Complete DropDown
    public DataSet GetSchedulingEmails(string str)
    {
        DataSet ds = new DataSet();
        SqlParameter[] param = new SqlParameter[1];

        try
        {

            param[0] = new SqlParameter("@str", SqlDbType.VarChar);
            param[0].Value = str;

            ds = SqlHelper.ExecuteDataset(SqlHelper.GetConnectionString(), CommandType.StoredProcedure, "getschedulingemails", param);
            return ds;
        }
        catch (Exception ex)
        {
            // Common.SaveTextToFile(ex.ToString());
            return ds;
        }
        finally
        {
            ds = null;

        }
    }

    // Add Email IDs in scheduling_emails table if Current Email ID is not available in the table
    public int SetSchedulingEmails(string email, out string error)
    {

        SqlParameter[] param = new SqlParameter[3];
        int result = -1;
        try
        {
            param[0] = new SqlParameter("@email", SqlDbType.VarChar, 200);
            param[0].Value = email;

            param[1] = new SqlParameter("@error", SqlDbType.VarChar, 200);
            param[1].Direction = ParameterDirection.Output;

            param[2] = new SqlParameter("@id", SqlDbType.Int);
            param[2].Direction = ParameterDirection.Output;

            SqlHelper.ExecuteNonQuery(SqlHelper.GetConnectionString(), CommandType.StoredProcedure, "setschedulingemails", param);

            error = param[1].Value.ToString();

            return Convert.ToInt32(param[2].Value);
        }
        catch (Exception ex)
        {
            // Common.SaveTextToFile(ex.ToString());
            error = ex.Message;
            return result;
        }

    }

    // Add Email Transaction Details After Post From Google Calender API
    public void AddEmailTransaction(string event_id, string SAutoID, string guid)
    {

        SqlParameter[] param = new SqlParameter[3];

        try
        {
            param[0] = new SqlParameter("@event_id", SqlDbType.VarChar, 500);
            param[0].Value = event_id;

            param[1] = new SqlParameter("@SAutoID", SqlDbType.VarChar, 500);
            param[1].Value = SAutoID;

            param[2] = new SqlParameter("@guid", SqlDbType.VarChar, 500);
            param[2].Value = guid;


            SqlHelper.ExecuteNonQuery(SqlHelper.GetConnectionString(), CommandType.StoredProcedure, "EmailScheculer_AddEmailTransaction", param);

        }
        catch (Exception ex)
        {
            // Common.SaveTextToFile(ex.ToString());

        }
        finally
        {


        }
    }

    // Fetch Master Details For Email Scheduleing Page
    public DataSet Fetch_Master_Details(int report_id)
    {

        DataSet ds = new DataSet();

        SqlParameter[] param = new SqlParameter[1];

        try
        {
            param[0] = new SqlParameter("@report_id", SqlDbType.Int);
            param[0].Value = report_id;
            ds = SqlHelper.ExecuteDataset(SqlHelper.GetConnectionString(), CommandType.StoredProcedure, "EmailScheculer_fetch_master_details", param);
            return ds;
        }
        catch (Exception ex)
        {
            // Common.SaveTextToFile(ex.ToString());
            return ds;
        }
        finally
        {
            ds = null;

        }
    }

    // Check Duplicate Schedule Details
    public DataSet check_duplicate_schedule(string report_id, string email_to, string report_type, string event_id, int email_id, DateTime startdate, string starttime, DateTime enddate, string endtime, string timezone, string repeat, int Repeattypeid, string endtimesummary, string @repeatesummary, string Timezonedisplay, string user_id, string facility_id, int isrepeat, int emailid)
    {
        DataSet dst = new DataSet();
        try
        {

            SqlParameter[] param = new SqlParameter[16];
            param[0] = new SqlParameter("@report_id", SqlDbType.VarChar);
            param[0].Value = report_id;


            param[1] = new SqlParameter("@email_to", SqlDbType.VarChar);
            param[1].Value = email_to;

            param[2] = new SqlParameter("@report_type", SqlDbType.VarChar);
            param[2].Value = report_type;

            param[3] = new SqlParameter("@start_date", SqlDbType.VarChar);
            param[3].Value = startdate;

            param[4] = new SqlParameter("@start_time", SqlDbType.VarChar);
            param[4].Value = starttime;

            param[5] = new SqlParameter("@end_date", SqlDbType.VarChar);
            param[5].Value = enddate;

            param[6] = new SqlParameter("@end_time", SqlDbType.VarChar);
            param[6].Value = endtime;

            param[7] = new SqlParameter("@timezone", SqlDbType.VarChar, 200);
            param[7].Value = timezone;

            param[8] = new SqlParameter("@repeat", SqlDbType.VarChar);
            param[8].Value = repeat;

            param[9] = new SqlParameter("@Repeattypeid", SqlDbType.Int);
            param[9].Value = Repeattypeid;


            param[10] = new SqlParameter("@endtimesummary", SqlDbType.VarChar, -1);
            param[10].Value = endtimesummary;


            param[11] = new SqlParameter("@repeatesummary", SqlDbType.VarChar, -1);
            param[11].Value = repeatesummary;

            param[12] = new SqlParameter("@Timezonedisplay", SqlDbType.VarChar, -1);
            param[12].Value = Timezonedisplay;


            param[13] = new SqlParameter("@facilityid", SqlDbType.VarChar);
            param[13].Value = facility_id;

            param[14] = new SqlParameter("@isrepeat", SqlDbType.Int);
            param[14].Value = isrepeat;

            param[15] = new SqlParameter("@email_id", SqlDbType.Int);
            param[15].Value = email_id;


            dst = SqlHelper.ExecuteDataset(SqlHelper.GetConnectionString(), CommandType.StoredProcedure, "EmailScheculer_check_duplicate_schedule", param);

            return dst;
        }
        catch (Exception ex)
        {
            // Common.SaveTextToFile("Function Name : AddScheduleDetails, Page Name : ChartDataClass, Error Message : " + ex.ToString());
            dst = null;
            return dst;

        }
        finally
        {

        }
    }


    // Delete Email ID
    public DataSet DeleteSchedulingEmails(string emailid)
    {
        DataSet ds = new DataSet();
        SqlParameter[] param = new SqlParameter[1];

        try
        {

            param[0] = new SqlParameter("@emailid", SqlDbType.VarChar);
            param[0].Value = emailid;

            ds = SqlHelper.ExecuteDataset(SqlHelper.GetConnectionString(), CommandType.StoredProcedure, "EmailScheculer_deleteschedulingemails", param);
            return ds;
        }
        catch (Exception ex)
        {
            // Common.SaveTextToFile("Function Name : deleteschedulingemails, Page Name : ChartDataClass, Error Message : " + ex.ToString());
            return ds;
        }
        finally
        {
            ds = null;

        }
    }


    #endregion

    #region Login_Block_Unblock


    // Fetch block type master details
    public DataSet block_type_master()
    {

        DataSet ds = new DataSet();
        SqlParameter[] param = new SqlParameter[1];

        try
        {

            ds = SqlHelper.ExecuteDataset(SqlHelper.GetConnectionString(), CommandType.StoredProcedure, "RoleManagement_IP_getblocktypemaster");
            return ds;
        }
        catch (Exception ex)
        {
            // Common.SaveTextToFile(ex.ToString());
            return ds;
        }
        finally
        {
            ds = null;

        }
    }

    // Login Block Details
    public DataSet login_block_details(int block_type_id, string SAutoID, string CAutoID, string url, int isactive)
    {

        DataSet ds = new DataSet();
        SqlParameter[] param = new SqlParameter[5];

        try
        {
            param[0] = new SqlParameter("@block_type_id", SqlDbType.Int);
            param[0].Value = block_type_id;
            param[1] = new SqlParameter("@Sautoid", SqlDbType.VarChar);
            param[1].Value = SAutoID;
            param[2] = new SqlParameter("@cautoid", SqlDbType.VarChar);
            param[2].Value = CAutoID;
            param[3] = new SqlParameter("@url", SqlDbType.VarChar);
            param[3].Value = url;
            param[4] = new SqlParameter("@isactive", SqlDbType.Int);
            param[4].Value = isactive;

            ds = SqlHelper.ExecuteDataset(SqlHelper.GetConnectionString(), CommandType.StoredProcedure, "RoleManagement_IP_getloginblockdetails", param);
            return ds;
        }
        catch (Exception ex)
        {
            // Common.SaveTextToFile(ex.ToString());
            return ds;
        }
        finally
        {
            ds = null;

        }
    }

    // // Update block details and change status
    public DataSet update_block_details(string block_data)
    {

        DataSet ds = new DataSet();
        SqlParameter[] param = new SqlParameter[1];

        try
        {
            param[0] = new SqlParameter("@block_data", SqlDbType.NVarChar);
            param[0].Value = block_data;

            ds = SqlHelper.ExecuteDataset(SqlHelper.GetConnectionString(), CommandType.StoredProcedure, "RoleManagement_IP_setLoginidIPBlockRel", param);
            return ds;
        }
        catch (Exception ex)
        {
            // Common.SaveTextToFile(ex.ToString());
            return ds;
        }
        finally
        {
            ds = null;

        }
    }

    #endregion

    #region User bypass Login
    //[getPassowordForCAutoID](@Cautoid int,@username varchar(100)='',@password varchar(200)='' output)
    public string GetPassowordForCAutoID(int Cautoid, string username)
    {
        string password = "";
        try
        {
            SqlParameter[] param = new SqlParameter[3];
            param[0] = new SqlParameter("@Cautoid", SqlDbType.Int);
            param[0].Value = Cautoid;
            param[1] = new SqlParameter("@username", SqlDbType.VarChar, 100);
            param[1].Value = username;

            param[2] = new SqlParameter("@password", SqlDbType.VarChar, 200);
            param[2].Direction = ParameterDirection.Output;

            SqlHelper.ExecuteDataset(SqlHelper.GetConnectionString(), CommandType.StoredProcedure, "getPassowordForCAutoID", param);

            password = param[2].Value.ToString();
        }
        catch (Exception ex)
        {

            // Common.SaveTextToFile(ex.ToString());
        }
        return password;
    }
    #endregion

}