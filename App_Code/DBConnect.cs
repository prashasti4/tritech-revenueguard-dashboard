﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Elmah;

/// <summary>
/// Summary description for DBConnect
/// </summary>
public class DBConnect
{
	public DBConnect()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    // Report Header
    public DataSet GetReportName(string companyid, int reporttype, string fromdate, string todate, int reportid, int userid, string avgormax, string numberofdays, string insuranceid)
    {
        DataSet ds = new DataSet();
        SqlParameter[] param = new SqlParameter[9];

        try
        {
            param[0] = new SqlParameter("@companyid", SqlDbType.VarChar, -1);
            param[0].Value = companyid;
            param[1] = new SqlParameter("@reporttype", SqlDbType.Int);
            param[1].Value = reporttype;
            param[2] = new SqlParameter("@reportid", SqlDbType.Int);
            param[2].Value = reportid;

            param[3] = new SqlParameter("@fromdate", SqlDbType.DateTime);
            if (fromdate.ToString().Trim() != "")
                param[3].Value = Convert.ToDateTime(fromdate);
            else
                param[3].Value = DBNull.Value;


            param[4] = new SqlParameter("@todate", SqlDbType.DateTime);
            if (todate.ToString().Trim() != "")
                param[4].Value = Convert.ToDateTime(todate);
            else
                param[4].Value = DBNull.Value;


            param[5] = new SqlParameter("@userid", SqlDbType.Int);
            param[5].Value = userid;


            param[6] = new SqlParameter("@avgormax", SqlDbType.VarChar, -1);
            param[6].Value = avgormax;
            param[7] = new SqlParameter("@numberofdays", SqlDbType.VarChar, -1);
            param[7].Value = numberofdays;
            param[8] = new SqlParameter("@insuranceid", SqlDbType.VarChar, -1);
            param[8].Value = insuranceid;



            ds = SqlHelper.ExecuteDataset(SqlHelper.GetConnectionString(), CommandType.StoredProcedure, "GetReportName", param);

            return ds;
        }
        catch (Exception ex)
        {
            ErrorSignal.FromCurrentContext().Raise(ex);
            return ds;
        }
        finally
        {
            ds = null;
            param = null;
        }
    }

    // Average Days To Bill
    public DataSet getAverageDaysToBill(string companyid, int reporttype, string fromdate, string todate, int reportid, int userid, int isreport)
    {
        DataSet ds = new DataSet();
        SqlParameter[] param = new SqlParameter[7];
       
        try
        {
            param[0] = new SqlParameter("@companyid", SqlDbType.VarChar, -1);
            param[0].Value = companyid;
            param[1] = new SqlParameter("@reporttype", SqlDbType.Int);
            param[1].Value = reporttype;
            param[2] = new SqlParameter("@reportid", SqlDbType.Int);
            param[2].Value = reportid;
           
            param[3] = new SqlParameter("@fromdate", SqlDbType.DateTime);
            if (fromdate.ToString().Trim() != "")
                param[3].Value = Convert.ToDateTime(fromdate);
            else
                param[3].Value = DBNull.Value;


            param[4] = new SqlParameter("@todate", SqlDbType.DateTime);
            if (todate.ToString().Trim() != "")
                param[4].Value = Convert.ToDateTime(todate);
            else
                param[4].Value = DBNull.Value;


            param[5] = new SqlParameter("@userid", SqlDbType.Int);
            param[5].Value = userid;

            param[6] = new SqlParameter("@isreport", SqlDbType.Int);
            param[6].Value = isreport;

            ds = SqlHelper.ExecuteDataset(SqlHelper.GetConnectionString(), CommandType.StoredProcedure, "getAverageDaysToBill", param);

            return ds;
        }
        catch (Exception ex)
        {
            ErrorSignal.FromCurrentContext().Raise(ex);
            return ds;
        }
        finally
        {
            ds = null;
            param = null;
        }
    }

    // Average Days To Bill
    public DataSet getOdometerValues(string companyid, int reporttype, string fromdate, string todate, int reportid, int userid, int isreport)
    {
        DataSet ds = new DataSet();
        SqlParameter[] param = new SqlParameter[7];

        try
        {
            param[0] = new SqlParameter("@companyid", SqlDbType.VarChar, -1);
            param[0].Value = companyid;
            param[1] = new SqlParameter("@reporttype", SqlDbType.Int);
            param[1].Value = reporttype;
            param[2] = new SqlParameter("@reportid", SqlDbType.Int);
            param[2].Value = reportid;

            param[3] = new SqlParameter("@fromdate", SqlDbType.DateTime);
            if (fromdate.ToString().Trim() != "")
                param[3].Value = Convert.ToDateTime(fromdate);
            else
                param[3].Value = DBNull.Value;


            param[4] = new SqlParameter("@todate", SqlDbType.DateTime);
            if (todate.ToString().Trim() != "")
                param[4].Value = Convert.ToDateTime(todate);
            else
                param[4].Value = DBNull.Value;


            param[5] = new SqlParameter("@userid", SqlDbType.Int);
            param[5].Value = userid;

            param[6] = new SqlParameter("@isreport", SqlDbType.Int);
            param[6].Value = isreport;

            ds = SqlHelper.ExecuteDataset(SqlHelper.GetConnectionString(), CommandType.StoredProcedure, "getOdometerValues", param);

            return ds;
        }
        catch (Exception ex)
        {
            ErrorSignal.FromCurrentContext().Raise(ex);
            return ds;
        }
        finally
        {
            ds = null;
            param = null;
        }
    }


    // Waiting to Be Billed
    public DataSet getClaimsWaitingToBeBilled(string companyid, int reporttype, string fromdate, string todate, int reportid, int userid, int isreport)
    {
        DataSet ds = new DataSet();
        SqlParameter[] param = new SqlParameter[7];

        try
        {
            param[0] = new SqlParameter("@companyid", SqlDbType.VarChar, -1);
            param[0].Value = companyid;
            param[1] = new SqlParameter("@reporttype", SqlDbType.Int);
            param[1].Value = reporttype;
            param[2] = new SqlParameter("@reportid", SqlDbType.Int);
            param[2].Value = reportid;

            param[3] = new SqlParameter("@fromdate", SqlDbType.DateTime);
            if (fromdate.ToString().Trim() != "")
                param[3].Value = Convert.ToDateTime(fromdate);
            else
                param[3].Value = DBNull.Value;


            param[4] = new SqlParameter("@todate", SqlDbType.DateTime);
            if (todate.ToString().Trim() != "")
                param[4].Value = Convert.ToDateTime(todate);
            else
                param[4].Value = DBNull.Value;


            param[5] = new SqlParameter("@userid", SqlDbType.Int);
            param[5].Value = userid;


            param[6] = new SqlParameter("@isreport", SqlDbType.Int);
            param[6].Value = isreport;





            ds = SqlHelper.ExecuteDataset(SqlHelper.GetConnectionString(), CommandType.StoredProcedure, "getClaimsWaitingToBeBilled", param);

            return ds;
        }
        catch (Exception ex)
        {
            
             ErrorSignal.FromCurrentContext().Raise(ex);
            return ds;
        }
        finally
        {
            ds = null;
            param = null;
        }
    }


    // Date of Service vs. Date of Upload
    public DataSet GetDateOfServiceVsDateOfUpload(string companyid, int reporttype, string fromdate, string todate, int reportid, int userid, string avgormax)
    {
        DataSet ds = new DataSet();
        SqlParameter[] param = new SqlParameter[7];

        try
        {
            param[0] = new SqlParameter("@companyid", SqlDbType.VarChar, -1);
            param[0].Value = companyid;
            param[1] = new SqlParameter("@reporttype", SqlDbType.Int);
            param[1].Value = reporttype;
            param[2] = new SqlParameter("@reportid", SqlDbType.Int);
            param[2].Value = reportid;

            param[3] = new SqlParameter("@fromdate", SqlDbType.DateTime);
            if (fromdate.ToString().Trim() != "")
                param[3].Value = Convert.ToDateTime(fromdate);
            else
                param[3].Value = DBNull.Value;


            param[4] = new SqlParameter("@todate", SqlDbType.DateTime);
            if (todate.ToString().Trim() != "")
                param[4].Value = Convert.ToDateTime(todate);
            else
                param[4].Value = DBNull.Value;


            param[5] = new SqlParameter("@userid", SqlDbType.Int);
            param[5].Value = userid;


            param[6] = new SqlParameter("@avgormax", SqlDbType.VarChar, -1);
            param[6].Value = userid;

            ds = SqlHelper.ExecuteDataset(SqlHelper.GetConnectionString(), CommandType.StoredProcedure, "GetDateOfServiceVsDateOfUpload", param);

            return ds;
        }
        catch (Exception ex)
        {
            
             ErrorSignal.FromCurrentContext().Raise(ex);
            return ds;
        }
        finally
        {
            ds = null;
            param = null;
        }
    }

    // Date of Service vs. Date of Upload
    public DataSet Getavgormax(string companyid, int reporttype, string fromdate, string todate, int reportid, int userid, string avgormax)
    {
        DataSet ds = new DataSet();
        SqlParameter[] param = new SqlParameter[7];

        try
        {
            param[0] = new SqlParameter("@companyid", SqlDbType.VarChar, -1);
            param[0].Value = companyid;
            param[1] = new SqlParameter("@reporttype", SqlDbType.Int);
            param[1].Value = reporttype;
            param[2] = new SqlParameter("@reportid", SqlDbType.Int);
            param[2].Value = reportid;

            param[3] = new SqlParameter("@fromdate", SqlDbType.DateTime);
            if (fromdate.ToString().Trim() != "")
                param[3].Value = Convert.ToDateTime(fromdate);
            else
                param[3].Value = DBNull.Value;


            param[4] = new SqlParameter("@todate", SqlDbType.DateTime);
            if (todate.ToString().Trim() != "")
                param[4].Value = Convert.ToDateTime(todate);
            else
                param[4].Value = DBNull.Value;


            param[5] = new SqlParameter("@userid", SqlDbType.Int);
            param[5].Value = userid;


            param[6] = new SqlParameter("@avgormax", SqlDbType.VarChar, -1);
            param[6].Value = userid;

            ds = SqlHelper.ExecuteDataset(SqlHelper.GetConnectionString(), CommandType.StoredProcedure, "Getavgormax", param);

            return ds;
        }
        catch (Exception ex)
        {

            ErrorSignal.FromCurrentContext().Raise(ex);
            return ds;
        }
        finally
        {
            ds = null;
            param = null;
        }
    }


    // A/R - Total Outstanding Claims
    public DataSet getTotalOutstandingClaims(string companyid, int reporttype, string fromdate, string todate, int reportid, int userid, string noofdays, string insuranceid)
    {
        DataSet ds = new DataSet();
        SqlParameter[] param = new SqlParameter[8];

        try
        {
            param[0] = new SqlParameter("@companyid", SqlDbType.VarChar, -1);
            param[0].Value = companyid;
            param[1] = new SqlParameter("@reporttype", SqlDbType.Int);
            param[1].Value = reporttype;
            param[2] = new SqlParameter("@reportid", SqlDbType.Int);
            param[2].Value = reportid;

            param[3] = new SqlParameter("@fromdate", SqlDbType.DateTime);
            if (fromdate.ToString().Trim() != "")
                param[3].Value = Convert.ToDateTime(fromdate);
            else
                param[3].Value = DBNull.Value;


            param[4] = new SqlParameter("@todate", SqlDbType.DateTime);
            if (todate.ToString().Trim() != "")
                param[4].Value = Convert.ToDateTime(todate);
            else
                param[4].Value = DBNull.Value;


            param[5] = new SqlParameter("@numberofdays", SqlDbType.VarChar, -1);
            param[5].Value = noofdays;

            param[6] = new SqlParameter("@insuranceid", SqlDbType.VarChar, -1);
            param[6].Value = insuranceid;


            param[7] = new SqlParameter("@userid", SqlDbType.Int);
            param[7].Value = userid;



            ds = SqlHelper.ExecuteDataset(SqlHelper.GetConnectionString(), CommandType.StoredProcedure, "getTotalOutstandingClaims", param);

            return ds;
        }
        catch (Exception ex)
        {
            
            ErrorSignal.FromCurrentContext().Raise(ex);
            return ds;
        }
        finally
        {
            ds = null;
            param = null;
        }
    }

    // Insurance Dropdown
    public DataSet GetInsurances(string companyid, int reporttype, string fromdate, string todate, int reportid, int userid, string avgormax, string insuranceid)
    {
        DataSet ds = new DataSet();
        SqlParameter[] param = new SqlParameter[8];

        try
        {
            param[0] = new SqlParameter("@companyid", SqlDbType.VarChar, -1);
            param[0].Value = companyid;
            param[1] = new SqlParameter("@reporttype", SqlDbType.Int);
            param[1].Value = reporttype;
            param[2] = new SqlParameter("@reportid", SqlDbType.Int);
            param[2].Value = reportid;

            param[3] = new SqlParameter("@fromdate", SqlDbType.DateTime);
            if (fromdate.ToString().Trim() != "")
                param[3].Value = Convert.ToDateTime(fromdate);
            else
                param[3].Value = DBNull.Value;


            param[4] = new SqlParameter("@todate", SqlDbType.DateTime);
            if (todate.ToString().Trim() != "")
                param[4].Value = Convert.ToDateTime(todate);
            else
                param[4].Value = DBNull.Value;


            param[5] = new SqlParameter("@avgormax", SqlDbType.VarChar, -1);
            param[5].Value = avgormax;

            param[6] = new SqlParameter("@insuranceid", SqlDbType.VarChar, -1);
            param[6].Value = insuranceid;


            param[7] = new SqlParameter("@userid", SqlDbType.Int);
            param[7].Value = userid;



            ds = SqlHelper.ExecuteDataset(SqlHelper.GetConnectionString(), CommandType.StoredProcedure, "GetInsurances", param);

            return ds;
        }
        catch (Exception ex)
        {

            ErrorSignal.FromCurrentContext().Raise(ex);
            return ds;
        }
        finally
        {
            ds = null;
            param = null;
        }
    }

    // Client Dropdown
    public DataSet getclients(int userid)
    {
        DataSet ds = new DataSet();
        SqlParameter[] param = new SqlParameter[1];

        try
        {
            

            param[0] = new SqlParameter("@userid", SqlDbType.Int);
            param[0].Value = userid;



            ds = SqlHelper.ExecuteDataset(SqlHelper.GetConnectionString(), CommandType.StoredProcedure, "getclients", param);

            return ds;
        }
        catch (Exception ex)
        {

            ErrorSignal.FromCurrentContext().Raise(ex);
            return ds;
        }
        finally
        {
            ds = null;
            param = null;
        }
    }

    // No. of Days Dropdown
    public DataSet GetNumberOfDays(string companyid, int reporttype, string fromdate, string todate, int reportid, int userid, string avgormax, string numberofdays, string insuranceid)
    {
        DataSet ds = new DataSet();
        SqlParameter[] param = new SqlParameter[9];

        try
        {
            param[0] = new SqlParameter("@companyid", SqlDbType.VarChar, -1);
            param[0].Value = companyid;
            param[1] = new SqlParameter("@reporttype", SqlDbType.Int);
            param[1].Value = reporttype;
            param[2] = new SqlParameter("@reportid", SqlDbType.Int);
            param[2].Value = reportid;

            param[3] = new SqlParameter("@fromdate", SqlDbType.DateTime);
            if (fromdate.ToString().Trim() != "")
                param[3].Value = Convert.ToDateTime(fromdate);
            else
                param[3].Value = DBNull.Value;


            param[4] = new SqlParameter("@todate", SqlDbType.DateTime);
            if (todate.ToString().Trim() != "")
                param[4].Value = Convert.ToDateTime(todate);
            else
                param[4].Value = DBNull.Value;


            param[5] = new SqlParameter("@userid", SqlDbType.Int);
            param[5].Value = userid;


            param[6] = new SqlParameter("@avgormax", SqlDbType.VarChar, -1);
            param[6].Value = avgormax;
            param[7] = new SqlParameter("@numberofdays", SqlDbType.VarChar, -1);
            param[7].Value = numberofdays;
            param[8] = new SqlParameter("@insuranceid", SqlDbType.VarChar, -1);
            param[8].Value = insuranceid;




            ds = SqlHelper.ExecuteDataset(SqlHelper.GetConnectionString(), CommandType.StoredProcedure, "GetNumberOfDays", param);

            return ds;
        }
        catch (Exception ex)
        {

            ErrorSignal.FromCurrentContext().Raise(ex);
            return ds;
        }
        finally
        {
            ds = null;
            param = null;
        }
    }


    // A/R - Percent Outstanding Claims
    public DataSet getPercentOutstandingClaims(string companyid, int reporttype, string fromdate, string todate, int reportid, int userid, string noofdays, string insuranceid, int isreport)
    {
        DataSet ds = new DataSet();
        SqlParameter[] param = new SqlParameter[9];

        try
        {
            param[0] = new SqlParameter("@companyid", SqlDbType.VarChar, -1);
            param[0].Value = companyid;
            param[1] = new SqlParameter("@reporttype", SqlDbType.Int);
            param[1].Value = reporttype;
            param[2] = new SqlParameter("@reportid", SqlDbType.Int);
            param[2].Value = reportid;

            param[3] = new SqlParameter("@fromdate", SqlDbType.DateTime);
            if (fromdate.ToString().Trim() != "")
                param[3].Value = Convert.ToDateTime(fromdate);
            else
                param[3].Value = DBNull.Value;


            param[4] = new SqlParameter("@todate", SqlDbType.DateTime);
            if (todate.ToString().Trim() != "")
                param[4].Value = Convert.ToDateTime(todate);
            else
                param[4].Value = DBNull.Value;


            param[5] = new SqlParameter("@numberofdays", SqlDbType.VarChar, -1);
            param[5].Value = noofdays;

            param[6] = new SqlParameter("@insuranceid", SqlDbType.VarChar, -1);
            param[6].Value = insuranceid;


            param[7] = new SqlParameter("@userid", SqlDbType.Int);
            param[7].Value = userid;


            param[8] = new SqlParameter("@isreport", SqlDbType.Int);
            param[8].Value = isreport;

            ds = SqlHelper.ExecuteDataset(SqlHelper.GetConnectionString(), CommandType.StoredProcedure, "getPercentOutstandingClaims", param);

            return ds;
        }
        catch (Exception ex)
        {
            
            ErrorSignal.FromCurrentContext().Raise(ex);
            return ds;
        }
        finally
        {
            ds = null;
            param = null;
        }
    }


    // Total hours worked
    public DataSet getTotalHoursWorked(string companyid, int reporttype, string fromdate, string todate, int reportid, int userid, string noofdays, string insuranceid, int isreport)
    {
        DataSet ds = new DataSet();
        SqlParameter[] param = new SqlParameter[9];

        try
        {
            param[0] = new SqlParameter("@companyid", SqlDbType.VarChar, -1);
            param[0].Value = companyid;
            param[1] = new SqlParameter("@reporttype", SqlDbType.Int);
            param[1].Value = reporttype;
            param[2] = new SqlParameter("@reportid", SqlDbType.Int);
            param[2].Value = reportid;

            param[3] = new SqlParameter("@fromdate", SqlDbType.DateTime);
            if (fromdate.ToString().Trim() != "")
                param[3].Value = Convert.ToDateTime(fromdate);
            else
                param[3].Value = DBNull.Value;


            param[4] = new SqlParameter("@todate", SqlDbType.DateTime);
            if (todate.ToString().Trim() != "")
                param[4].Value = Convert.ToDateTime(todate);
            else
                param[4].Value = DBNull.Value;


            param[5] = new SqlParameter("@numberofdays", SqlDbType.VarChar, -1);
            param[5].Value = noofdays;

            param[6] = new SqlParameter("@insuranceid", SqlDbType.VarChar, -1);
            param[6].Value = insuranceid;


            param[7] = new SqlParameter("@userid", SqlDbType.Int);
            param[7].Value = userid;


            param[8] = new SqlParameter("@isreport", SqlDbType.Int);
            param[8].Value = isreport;

            ds = SqlHelper.ExecuteDataset(SqlHelper.GetConnectionString(), CommandType.StoredProcedure, "getTotalHoursWorked", param);

            return ds;
        }
        catch (Exception ex)
        {

            ErrorSignal.FromCurrentContext().Raise(ex);
            return ds;
        }
        finally
        {
            ds = null;
            param = null;
        }
    }


    // Total Claims Waiting To Be Billed Vs Total Claims Billed
    public DataSet getTotalClaimsWaitingToBeBilledVsTotalClaimsBilled(string companyid, int reporttype, string fromdate, string todate, int reportid, int userid, string noofdays, string insuranceid, int isreport)
    {
        DataSet ds = new DataSet();
        SqlParameter[] param = new SqlParameter[9];

        try
        {
            param[0] = new SqlParameter("@companyid", SqlDbType.VarChar, -1);
            param[0].Value = companyid;
            param[1] = new SqlParameter("@reporttype", SqlDbType.Int);
            param[1].Value = reporttype;
            param[2] = new SqlParameter("@reportid", SqlDbType.Int);
            param[2].Value = reportid;

            param[3] = new SqlParameter("@fromdate", SqlDbType.DateTime);
            if (fromdate.ToString().Trim() != "")
                param[3].Value = Convert.ToDateTime(fromdate);
            else
                param[3].Value = DBNull.Value;


            param[4] = new SqlParameter("@todate", SqlDbType.DateTime);
            if (todate.ToString().Trim() != "")
                param[4].Value = Convert.ToDateTime(todate);
            else
                param[4].Value = DBNull.Value;


            param[5] = new SqlParameter("@numberofdays", SqlDbType.VarChar, -1);
            param[5].Value = noofdays;

            param[6] = new SqlParameter("@insuranceid", SqlDbType.VarChar, -1);
            param[6].Value = insuranceid;


            param[7] = new SqlParameter("@userid", SqlDbType.Int);
            param[7].Value = userid;


            param[8] = new SqlParameter("@isreport", SqlDbType.Int);
            param[8].Value = isreport;

            ds = SqlHelper.ExecuteDataset(SqlHelper.GetConnectionString(), CommandType.StoredProcedure, "getTotalClaimsWaitingToBeBilledVsTotalClaimsBilled", param);

            return ds;
        }
        catch (Exception ex)
        {

            ErrorSignal.FromCurrentContext().Raise(ex);
            return ds;
        }
        finally
        {
            ds = null;
            param = null;
        }
    }


    // Claims Billed Month per Client 
    public DataSet getClaimsBilledPerClient(string companyid, int reporttype, string fromdate, string todate, int reportid, int userid)
    {
        DataSet ds = new DataSet();
        SqlParameter[] param = new SqlParameter[6];

        try
        {
            param[0] = new SqlParameter("@companyid", SqlDbType.VarChar, -1);
            param[0].Value = companyid;
            param[1] = new SqlParameter("@reporttype", SqlDbType.Int);
            param[1].Value = reporttype;
            param[2] = new SqlParameter("@reportid", SqlDbType.Int);
            param[2].Value = reportid;

            param[3] = new SqlParameter("@fromdate", SqlDbType.DateTime);
            if (fromdate.ToString().Trim() != "")
                param[3].Value = Convert.ToDateTime(fromdate);
            else
                param[3].Value = DBNull.Value;


            param[4] = new SqlParameter("@todate", SqlDbType.DateTime);
            if (todate.ToString().Trim() != "")
                param[4].Value = Convert.ToDateTime(todate);
            else
                param[4].Value = DBNull.Value;


            param[5] = new SqlParameter("@userid", SqlDbType.Int);
            param[5].Value = userid;




            ds = SqlHelper.ExecuteDataset(SqlHelper.GetConnectionString(), CommandType.StoredProcedure, "getClaimsBilledPerClient", param);

            return ds;
        }
        catch (Exception ex)
        {
            
             ErrorSignal.FromCurrentContext().Raise(ex);
            return ds;
        }
        finally
        {
            ds = null;
            param = null;
        }
    }


    // Average Days Outstanding Trending
    public DataSet getAverageDaysOutstandingTrending(string companyid, int reporttype, string fromdate, string todate, int reportid, int userid)
    {
        DataSet ds = new DataSet();
        SqlParameter[] param = new SqlParameter[6];

        try
        {
            param[0] = new SqlParameter("@companyid", SqlDbType.VarChar, -1);
            param[0].Value = companyid;
            param[1] = new SqlParameter("@reporttype", SqlDbType.Int);
            param[1].Value = reporttype;
            param[2] = new SqlParameter("@reportid", SqlDbType.Int);
            param[2].Value = reportid;

            param[3] = new SqlParameter("@fromdate", SqlDbType.DateTime);
            if (fromdate.ToString().Trim() != "")
                param[3].Value = Convert.ToDateTime(fromdate);
            else
                param[3].Value = DBNull.Value;


            param[4] = new SqlParameter("@todate", SqlDbType.DateTime);
            if (todate.ToString().Trim() != "")
                param[4].Value = Convert.ToDateTime(todate);
            else
                param[4].Value = DBNull.Value;


            param[5] = new SqlParameter("@userid", SqlDbType.Int);
            param[5].Value = userid;




            ds = SqlHelper.ExecuteDataset(SqlHelper.GetConnectionString(), CommandType.StoredProcedure, "getAverageDaysOutstandingTrending", param);

            return ds;
        }
        catch (Exception ex)
        {
            
             ErrorSignal.FromCurrentContext().Raise(ex);
            return ds;
        }
        finally
        {
            ds = null;
            param = null;
        }
    }


    // Medicare Last Payment
    public DataSet getMedicarelastPayment(string companyid, int reporttype, string fromdate, string todate, int reportid, int userid)
    {
        DataSet ds = new DataSet();
        SqlParameter[] param = new SqlParameter[6];

        try
        {
            param[0] = new SqlParameter("@companyid", SqlDbType.VarChar, -1);
            param[0].Value = companyid;
            param[1] = new SqlParameter("@reporttype", SqlDbType.Int);
            param[1].Value = reporttype;
            param[2] = new SqlParameter("@reportid", SqlDbType.Int);
            param[2].Value = reportid;

            param[3] = new SqlParameter("@fromdate", SqlDbType.DateTime);
            if (fromdate.ToString().Trim() != "")
                param[3].Value = Convert.ToDateTime(fromdate);
            else
                param[3].Value = DBNull.Value;


            param[4] = new SqlParameter("@todate", SqlDbType.DateTime);
            if (todate.ToString().Trim() != "")
                param[4].Value = Convert.ToDateTime(todate);
            else
                param[4].Value = DBNull.Value;


            param[5] = new SqlParameter("@userid", SqlDbType.Int);
            param[5].Value = userid;




            ds = SqlHelper.ExecuteDataset(SqlHelper.GetConnectionString(), CommandType.StoredProcedure, "getMedicarelastPayment", param);

            return ds;
        }
        catch (Exception ex)
        {
           
             ErrorSignal.FromCurrentContext().Raise(ex);
            return ds;
        }
        finally
        {
            ds = null;
            param = null;
        }
    }

    // Average Cash Collected Per Trip
    public DataSet getAverageCashCollectedPerTrip(string companyid, int reporttype, string fromdate, string todate, int reportid, int userid)
    {
        DataSet ds = new DataSet();
        SqlParameter[] param = new SqlParameter[6];

        try
        {
            param[0] = new SqlParameter("@companyid", SqlDbType.VarChar, -1);
            param[0].Value = companyid;
            param[1] = new SqlParameter("@reporttype", SqlDbType.Int);
            param[1].Value = reporttype;
            param[2] = new SqlParameter("@reportid", SqlDbType.Int);
            param[2].Value = reportid;

            param[3] = new SqlParameter("@fromdate", SqlDbType.DateTime);
            if (fromdate.ToString().Trim() != "")
                param[3].Value = Convert.ToDateTime(fromdate);
            else
                param[3].Value = DBNull.Value;


            param[4] = new SqlParameter("@todate", SqlDbType.DateTime);
            if (todate.ToString().Trim() != "")
                param[4].Value = Convert.ToDateTime(todate);
            else
                param[4].Value = DBNull.Value;


            param[5] = new SqlParameter("@userid", SqlDbType.Int);
            param[5].Value = userid;





            ds = SqlHelper.ExecuteDataset(SqlHelper.GetConnectionString(), CommandType.StoredProcedure, "getAverageCashCollectedPerTrip", param);

            return ds;
        }
        catch (Exception ex)
        {
            // Common.SaveTextToFile(ex.ToString());
            // ErrorSignal.FromCurrentContext().Raise(ex);
            return ds;
        }
        finally
        {
            ds = null;
            param = null;
        }
    }

    // Volume Monthly Trending
    public DataSet getVolumeMonthlyTrending(string companyid, int reporttype, string fromdate, string todate, int reportid, int userid)
    {
        DataSet ds = new DataSet();
        SqlParameter[] param = new SqlParameter[6];

        try
        {
            param[0] = new SqlParameter("@companyid", SqlDbType.VarChar, -1);
            param[0].Value = companyid;
            param[1] = new SqlParameter("@reporttype", SqlDbType.Int);
            param[1].Value = reporttype;
            param[2] = new SqlParameter("@reportid", SqlDbType.Int);
            param[2].Value = reportid;

            param[3] = new SqlParameter("@fromdate", SqlDbType.DateTime);
            if (fromdate.ToString().Trim() != "")
                param[3].Value = Convert.ToDateTime(fromdate);
            else
                param[3].Value = DBNull.Value;


            param[4] = new SqlParameter("@todate", SqlDbType.DateTime);
            if (todate.ToString().Trim() != "")
                param[4].Value = Convert.ToDateTime(todate);
            else
                param[4].Value = DBNull.Value;


            param[5] = new SqlParameter("@userid", SqlDbType.Int);
            param[5].Value = userid;




            ds = SqlHelper.ExecuteDataset(SqlHelper.GetConnectionString(), CommandType.StoredProcedure, "getVolumeMonthlyTrending", param);

            return ds;
        }
        catch (Exception ex)
        {
             
            ErrorSignal.FromCurrentContext().Raise(ex);
            return ds;
        }
        finally
        {
            ds = null;
            param = null;
        }
    }

    // Fetch Menu
    public DataSet getReports(int userid)
    {
        DataSet ds = new DataSet();
        SqlParameter[] param = new SqlParameter[1];

        try
        {
            
            param[0] = new SqlParameter("@userid", SqlDbType.Int);
            param[0].Value = userid;




            ds = SqlHelper.ExecuteDataset(SqlHelper.GetConnectionString(), CommandType.StoredProcedure, "getReports", param);

            return ds;
        }
        catch (Exception ex)
        {

            ErrorSignal.FromCurrentContext().Raise(ex);
            return ds;
        }
        finally
        {
            ds = null;
            param = null;
        }
    }



    public string fetchCommaSeperatedCompanyName(string companyids)
    {
        string facility_names = "";
        ChartDataClass objChrtDtClass = new ChartDataClass();
        try
        {
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@companyids", SqlDbType.VarChar, -1);
            param[0].Value = companyids;

            if (companyids != "")
                facility_names = SqlHelper.ExecuteDataset(SqlHelper.GetConnectionString(), CommandType.Text, "select dbo.GetCompanyNames(" + "'" + companyids + "'" + ")").Tables[0].Rows[0][0].ToString();
            else
                facility_names = "All Clients";
        }
        catch (Exception ex)
        {
            //string vbCrLf = "\r\n";
            //Common.SaveTextToFile("------" + vbCrLf + ex.Message.ToString() + vbCrLf + ex.Source + vbCrLf + ex.StackTrace.ToString() + vbCrLf + ex.TargetSite.ToString() + vbCrLf + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString() + "------");
            ErrorSignal.FromCurrentContext().Raise(ex);
            facility_names = "";
        }
        return facility_names;
    }


    public string fetchCommaSeperatedInsuranceName(string insuranceids)
    {
        string facility_names = "";
        ChartDataClass objChrtDtClass = new ChartDataClass();
        try
        {
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@insuranceids", SqlDbType.VarChar, -1);
            param[0].Value = insuranceids;
            if (insuranceids != "")
                facility_names = SqlHelper.ExecuteDataset(SqlHelper.GetConnectionString(), CommandType.Text, "select dbo.GetInsuranceNames(" + "'" + insuranceids + "'" + ")").Tables[0].Rows[0][0].ToString();
            else
                facility_names = "All Payers";
        }
        catch (Exception ex)
        {
            //string vbCrLf = "\r\n";
            //Common.SaveTextToFile("------" + vbCrLf + ex.Message.ToString() + vbCrLf + ex.Source + vbCrLf + ex.StackTrace.ToString() + vbCrLf + ex.TargetSite.ToString() + vbCrLf + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString() + "------");
            ErrorSignal.FromCurrentContext().Raise(ex);
            facility_names = "";
        }
        return facility_names;
    }
   

}