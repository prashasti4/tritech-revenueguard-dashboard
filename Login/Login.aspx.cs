﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Data;
using System.Configuration;
using System.Security.Cryptography;
using System.Text;
using System.IO;

public partial class Login : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
       // redirectOnLogin();
        if (!IsPostBack)
        {
          
            clearcookies();
            //if (Request.Cookies["SM_URL"] != null)
            //{
            //    if (!Request.Url.ToString().Contains("localhost"))
            //        Response.Redirect(Request.Cookies["SM_URL"].Value);
            //    else
            //        Response.Redirect("../facilities/facilities.aspx");
            //}
           
            
            //lblError.Visible = false;

            //ClsCrypto cl = new ClsCrypto("MAKV2SPBNI99212");
            //string a = cl.Encrypt("test");
        }
    }
    protected void btnsubmit_Click(object sender, EventArgs e)
    {
        Loginvalidation();
    }
    protected void btnReset_Click(object sender, EventArgs e)
    {
        tbl_captcha.Visible = false;
        lblError.Text = "";
        ClientScript.RegisterStartupScript(GetType(), "id", "setTimeout('LoginPosition();',500);", true);
    }
    protected bool Loginvalidation()
    {
        //string strError = "<ul>";
        string strError = "";
        if (txtusername.Value.ToString().Trim() == "")
        {
            strError += "<p>" + "Enter Username." + "</p>";
        }
        if (txtpassword.Value.ToString().Trim() == "")
        {
            strError += "<p>" + "Enter Password." + "</p>";
        }

        if (txtusername.Value.ToString().Trim() != "" && txtpassword.Value.ToString().Trim() != "")
        {
            try
            {

                string error = "Incorrect Credentials or Account Disabled.";
                AppearenceSettingRepo objrepo = new AppearenceSettingRepo();
                string password = AppearenceSettingRepo.MD5Hash(txtpassword.Value);

               
                               string ipaddress;
                                   ipaddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
                                   if (ipaddress == "" || ipaddress == null)
                                       ipaddress = Request.ServerVariables["REMOTE_ADDR"];
                                   int cnt = 1;
                                   if (tbl_captcha.Visible == true)
                                   {
                                       if (!recaptcha.IsValid)
                                       {
                                           cnt = 0;
                                       }
                                   }
                                   if (cnt == 1)
                                   {
                                       string rqstURL=Request.Url.Authority.ToLower().Replace(":" + Request.Url.Port.ToString(), "");
                   
                                       int ipattempt=0;
                                       int invaliduserattempt=0;
                                       int isadmin=0;
                                       DataSet ds = new DataSet();
                                       ds = objrepo.getLogin(txtusername.Value.ToString(), password, ipaddress, rqstURL, out ipattempt, out invaliduserattempt, out isadmin );

                            

            //    DataSet ds = objrepo.getLogin(txtusername.Value.ToString(), password);
                DataTable login = ds.Tables[0];
                //if (error == "")
                //    ViewState["facilityids"] = facilityids;
                if (login.Rows.Count != 0)
                {
                    if (login.Columns.Count != 1)
                    {
                        Session["username"] = txtusername.Value;
                        if (login.Rows[0][3].ToString() == "D")
                        {
                            strError += "<p>Account has been disabled.</p>";
                        }
                        else
                        {
                            string uri = login.Rows[0][2].ToString();
                            //if (uri.ToLower().Contains("amb") || uri.ToLower().Contains(Request.Url.Authority.ToString().Replace(":" + Request.Url.Port.ToString(), "")) || Request.Url.ToString().Contains("localhost"))
                            //{
                                Session["UserName"] = txtusername.Value;
                                HttpCookie myCookie = new HttpCookie("SM_USER");

                                string str_pass = Encryptdata(login.Rows[0][1].ToString());
                                myCookie.Value = str_pass;
                                // myCookie.Domain = ".eonbi.com";
                                Response.Cookies.Add(myCookie);

                               
                                     HttpCookie isAdmincheck = new HttpCookie("isAdmincheck");
                                           isAdmincheck.Value = Encryptdata(isadmin.ToString());
                                           Response.Cookies.Add(isAdmincheck);
                              
                                     

                                HttpCookie UrlCookie = new HttpCookie("SM_URL");
                                UrlCookie.Value = login.Rows[0]["URL"].ToString();
                                // UrlCookie.Domain = ".eonbi.com";
                                Response.Cookies.Add(UrlCookie);


                                HttpCookie CautoIDCookie = new HttpCookie("CAutoID");

                                string str_CAuto_ID = Encryptdata(login.Rows[0]["CAuto_ID"].ToString());
                                CautoIDCookie.Value = str_CAuto_ID;
                                // myCookie.Domain = ".eonbi.com";
                                Response.Cookies.Add(CautoIDCookie);



                                HttpCookie SAuto_IDCookie = new HttpCookie("SAuto_ID");

                                string str_SAuto_ID = Encryptdata(login.Rows[0]["SAuto_ID"].ToString());
                                SAuto_IDCookie.Value = str_SAuto_ID;
                                // myCookie.Domain = ".eonbi.com";
                                Response.Cookies.Add(SAuto_IDCookie);

                                HttpCookie Role_IDCookie = new HttpCookie("Role_ID");

                                string str_Role_ID = Encryptdata(login.Rows[0]["Role_ID"].ToString());
                                Role_IDCookie.Value = str_Role_ID;
                                // myCookie.Domain = ".eonbi.com";
                                Response.Cookies.Add(Role_IDCookie);

                                ClsCrypto ccpt = new ClsCrypto("MAKV2SPBNI99212");

                                HttpCookie UnameCookie = new HttpCookie("Uname");

                                string str_Uname = Encryptdata(txtusername.Value.Trim());
                                UnameCookie.Value = str_Uname;
                                // myCookie.Domain = ".eonbi.com";
                                Response.Cookies.Add(UnameCookie);

                                HttpCookie UnmCookie = new HttpCookie("Unm");

                                string str_Unm = ccpt.Encrypt(txtusername.Value.Trim());
                                UnmCookie.Value = str_Unm;
                                // myCookie.Domain = ".eonbi.com";
                                Response.Cookies.Add(UnmCookie);

                                HttpCookie UserIDCookie = new HttpCookie("UserID");

                                string str_UserID = ccpt.Encrypt(login.Rows[0]["Auto_ID"].ToString());
                                UserIDCookie.Value = str_UserID;
                                // myCookie.Domain = ".eonbi.com";
                                Response.Cookies.Add(UserIDCookie);



                                HttpCookie ClientsCountCookie = new HttpCookie("ClientsCount");

                                string str_ClientsCount = ccpt.Encrypt(login.Rows[0]["ClientsCount"].ToString());
                                ClientsCountCookie.Value = str_ClientsCount;
                                // myCookie.Domain = ".eonbi.com";
                                Response.Cookies.Add(ClientsCountCookie);
                                Response.Redirect(login.Rows[0]["URL"].ToString());

                                //if (txtusername.Value.ToLower() != "admin_user")
                                //    Response.Redirect("../Admin/RoleManagement.aspx");
                                //else
                                //    Response.Redirect("../#/report/home.report.overview");
                                //else
                                //    Response.Redirect(login.Rows[0][2].ToString());
                            //}
                            //else
                            //    strError += "<p>Incorrect Credentials or Account Disabled.</p>";
                        }
                    }
                    else
                    {
                        strError += "<p>" + login.Rows[0][0].ToString() + "</p>";

                        if ((invaliduserattempt == 2) || (ipattempt >= 2 && ipattempt <= 4))
                        {
                            tbl_captcha.Visible = true;
                            ClientScript.RegisterStartupScript(GetType(), "id", "setTimeout('LoginPosition();',500);", true);
                        }
                        else
                        {
                            tbl_captcha.Visible = false;
                        }   

                    }
                }
                else
                    strError += "<p>" + error + "</p>";
               
                }
                            else
                            {
                                strError += "<p>The verification code is incorrect.</p>";

                            }  
            }
            catch (Exception ex)
            {

            }
        }
        //strError += "</ul>";
        strError += "";
       // if (strError.ToString().Trim() != "<ul></ul>")
        if (strError.ToString().Trim() != "<p></p>")
        {
            lblError.Visible = true;
            lblError.Text = strError.ToString().Trim();
            return false;
        }
        else
        {
            return true;
        }

    }

    private void clearcookies()
    {
        if (Request.Cookies["SM_USER"] != null)
        {
            HttpCookie SM_USER = Request.Cookies["SM_USER"];
            SM_USER.Expires = DateTime.Now.AddDays(-1d);
            Response.Cookies.Add(SM_USER);
        }
        if (Request.Cookies["isAdmincheck"] != null)
        {
            HttpCookie isAdmincheck = Request.Cookies["isAdmincheck"];
            isAdmincheck.Expires = DateTime.Now.AddDays(-1d);
            Response.Cookies.Add(isAdmincheck);
        }
        if (Request.Cookies["SM_URL"] != null)
        {
            HttpCookie SM_URL = Request.Cookies["SM_URL"];
            SM_URL.Expires = DateTime.Now.AddDays(-1d);
            Response.Cookies.Add(SM_URL);
        }
        if (Request.Cookies["CAutoID"] != null)
        {
            HttpCookie CAutoID = Request.Cookies["CAutoID"];
            CAutoID.Expires = DateTime.Now.AddDays(-1d);
            Response.Cookies.Add(CAutoID);
        }
        if (Request.Cookies["SAuto_ID"] != null)
        {
            HttpCookie SAuto_ID = Request.Cookies["SAuto_ID"];
            SAuto_ID.Expires = DateTime.Now.AddDays(-1d);
            Response.Cookies.Add(SAuto_ID);
        }
        if (Request.Cookies["Role_ID"] != null)
        {
            HttpCookie Role_ID = Request.Cookies["Role_ID"];
            Role_ID.Expires = DateTime.Now.AddDays(-1d);
            Response.Cookies.Add(Role_ID);
        }
        if (Request.Cookies["Uname"] != null)
        {
            HttpCookie Uname = Request.Cookies["Uname"];
            Uname.Expires = DateTime.Now.AddDays(-1d);
            Response.Cookies.Add(Uname);
        }
        if (Request.Cookies["active_menu"] != null)
        {
            HttpCookie active_menu = Request.Cookies["active_menu"];
            active_menu.Expires = DateTime.Now.AddDays(-1d);
            Response.Cookies.Add(active_menu);
        }
        if (Request.Cookies["UserID"] != null)
        {
            HttpCookie UserID = Request.Cookies["UserID"];
            UserID.Expires = DateTime.Now.AddDays(-1d);
            Response.Cookies.Add(UserID);
        }
        if (Request.Cookies["ClientsCount"] != null)
        {
            HttpCookie ClientsCount = Request.Cookies["ClientsCount"];
            ClientsCount.Expires = DateTime.Now.AddDays(-1d);
            Response.Cookies.Add(ClientsCount);
        }

        if (Request.Cookies["Unm"] != null)
        {
            HttpCookie Unm = Request.Cookies["Unm"];
            Unm.Expires = DateTime.Now.AddDays(-1d);
            Response.Cookies.Add(Unm);
        }

        if (Request.Cookies["reportid"] != null)
        {
            HttpCookie reportid = Request.Cookies["reportid"];
            reportid.Expires = DateTime.Now.AddDays(-1d);
            Response.Cookies.Add(reportid);
        }

        if (Request.Cookies["service"] != null)
        {
            HttpCookie service = Request.Cookies["service"];
            service.Expires = DateTime.Now.AddDays(-1d);
            Response.Cookies.Add(service);
        }

        if (Request.Cookies["active_menu"] != null)
        {
            HttpCookie active_menu = Request.Cookies["active_menu"];
            active_menu.Expires = DateTime.Now.AddDays(-1d);
            Response.Cookies.Add(active_menu);
        }
 
    }

    //protected void btnReset_Click(object sender, EventArgs e)
    //{
    //    txtusername.Value = "";
    //    lblError.Visible = false;
    //    lblError.Text = "";
    //}

    /// <summary>
    /// Created By Vaibhav Kale
    /// Encrypt url with username, password and facility
    /// </summary>
    /// <param name="password"></param>
    /// <returns></returns>
    private string Encryptdata(string password)
    {
        string EncryptionKey = "MAKV2SPBNI99212";
        byte[] clearBytes = Encoding.Unicode.GetBytes(password);
        using (Aes encryptor = Aes.Create())
        {
            Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
            encryptor.Key = pdb.GetBytes(32);
            encryptor.IV = pdb.GetBytes(16);
            using (MemoryStream ms = new MemoryStream())
            {
                using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                {
                    cs.Write(clearBytes, 0, clearBytes.Length);
                    cs.Close();
                }
                password = Convert.ToBase64String(ms.ToArray());
            }
        }
        return password;
    }


    private void redirectOnLogin()
    {
        string RedirecUrl = ConfigurationManager.AppSettings["LoginUrl"].ToString();
        string userAgent = Request.UserAgent;
        Uri test = new Uri(Request.Url.ToString());
        if (test.Host != "localhost" && !userAgent.Contains("iPad") && !userAgent.Contains("iPhone"))
            Response.Redirect(RedirecUrl);
    }


    public class ClsCrypto
    {
        private RijndaelManaged myRijndael = new RijndaelManaged();
        private int iterations;
        private byte[] salt;

        public ClsCrypto(string strPassword)
        {
            myRijndael.BlockSize = 128;
            myRijndael.KeySize = 128;
            myRijndael.IV = HexStringToByteArray("e84ad660c4721ae0e84ad660c4721ae0");

            myRijndael.Padding = PaddingMode.PKCS7;
            myRijndael.Mode = CipherMode.CBC;
            iterations = 1000;
            salt = System.Text.Encoding.UTF8.GetBytes("insight123resultxyz");
            myRijndael.Key = GenerateKey(strPassword);
        }

        public string Encrypt(string strPlainText)
        {
            byte[] strText = new System.Text.UTF8Encoding().GetBytes(strPlainText);
            ICryptoTransform transform = myRijndael.CreateEncryptor();
            byte[] cipherText = transform.TransformFinalBlock(strText, 0, strText.Length);

            return Convert.ToBase64String(cipherText);
        }

        public static byte[] HexStringToByteArray(string strHex)
        {
            dynamic r = new byte[strHex.Length / 2];
            for (int i = 0; i <= strHex.Length - 1; i += 2)
            {
                r[i / 2] = Convert.ToByte(Convert.ToInt32(strHex.Substring(i, 2), 16));
            }
            return r;
        }

        private byte[] GenerateKey(string strPassword)
        {
            Rfc2898DeriveBytes rfc2898 = new Rfc2898DeriveBytes(System.Text.Encoding.UTF8.GetBytes(strPassword), salt, iterations);

            return rfc2898.GetBytes(128 / 8);
        }
    }

}