﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/TriTechMaster.master" AutoEventWireup="true"
    CodeFile="ChangePassword.aspx.cs" Inherits="ChangePassword" %>

<asp:Content ID="Content1" ContentPlaceHolderID="pagehead" runat="Server">
    <link rel="stylesheet" type="text/css" href="../Admin/css/drag.css" />
    <link rel="stylesheet" type="text/css" href="../Admin/css/jefbar/facilities.css?03042014" />
    <script src="../Admin/js/jquery-2.1.4.min.js"></script>
    <link href="../Admin/css/bootstrap.min.css" rel="stylesheet" />
     <script src="../Admin/js/bootstrap.min.js"></script>
    <link href="../Admin/css/bootstrap-dialog.min.css" rel="stylesheet" />
        <script src="../Admin/js/bootstrap-dialog.min.js"></script>
   <link href="../Admin/css/jquery.dataTables.min.css" rel="stylesheet" />
    <script src="../Admin/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../jqtransformplugin/jquery.jqtransform.js"></script>
    <script type="text/javascript" src="../Admin/js/DraggableModalpopup.js"></script>
    <style type="text/css">
        /*   .marslogo
        {
            display: none;
        }
        
        #mpusername
        {
            display: none;
        }
        #todaysdate
        {
            display: none;
        }
    */
        #changefacility
        {
            display: none;
        }
        .modal
        {
            /*border: solid 6px rgba(0,0,0,0.5);*/
            width: auto;
            height: auto;
            display: none;
            position: absolute;
            left: 40%;
            /*top: 35%;*/
            top: 28%;
            z-index: 10001;
            margin: -25px 0 0 -50px;
            border-radius: 5px 5px 5px 5px;
            background-color: #ffffff;
            -moz-background-clip: padding;
            -webkit-background-clip: padding;
            background-clip: padding-box;
        }
        
        .overlay-style
        {
            background: none repeat scroll 0 0 #444444; /*background: rgba(40, 40, 40, .7);*/
            height: 100%;
            left: 0;
            opacity: 0.7;
            position: fixed;
            top: 0;
            width: 100%;
            height: 100%;
            z-index: 10000;
            display: none;
        }
        .jqTransformInputWrapper
        {
            background: transparent url(../jqtransformplugin/img/input/oldinput_text_left.gif) no-repeat left top;
            height: 31px;
            padding: 0px;
            float: left;
        }
        .jqTransformInputInner
        {
            background: transparent url(../jqtransformplugin/img/input/oldinput_text_right.gif) no-repeat top right;
            padding: 0px;
            margin: 0px;
        }
        .ClassSpan
        {
            color: #201F35;
            font-weight: normal;
            padding-right: 2px;
            text-transform: capitalize;
            display: inline-block;
            font-size: 14px;
            line-height: 30px;
            margin: 0;
            padding-right: 2px;
            text-transform: capitalize;
            width: 190px;
        }
        .jqTransformSafari .jqTransformInputInner
        {
            margin: 0px 0 0 0px;
        }
        
        .jqTransformInputInner div input:hover
        {
            cursor: text;
            border: 0 none !important;
        }
        .jqTransformInputInner div input:focus
        {
            outline: none;
        }
        .facility
        {
            color: #7E7975;
            font-size: 13px;
        }
          @media (max-width:420px)
       {
         .facility
        {
            color: #7E7975;
            font-size: 11px;
        }
        
        table
        {
            width:100%
        }  
       }
       .errorul
       {
           list-style-type:disc; 
           margin-left: -7%;
       }
    </style>
    <script type="text/javascript">


        var keylist = "abcdefghijklmnopqrstuvwxyz0123456789*&^%$#@!"
        var temp = ''

        function generatepass() {
            temp = ''
            for (i = 0; i < 10; i++)
                temp += keylist.charAt(Math.floor(Math.random() * keylist.length))
            return temp
        }

        function populateform() {
            $('#txtpwd').val(generatepass());
            return false;
        }

        function openpassowrdgenerator() {
            //document.getElementById('divpwd').style.display = 'block';
            $('#divpwd').fadeIn(500);
            $('#divoverlay').show();
            populateform();
            return false;
        }

        function closegenrator() {
            //document.getElementById('divpwd').style.display = 'none';
            $('#divpwd').fadeOut(500);
            $('#divoverlay').hide();
            return false;
        }
        function Usepassword() {
            if ($('#chk').is(":checked")) {
                var pwd = $('#txtpwd').val();
                $('#ContentHolder_txtNewPassword').val(pwd);
                $('#ContentHolder_txtRepassword').val(pwd);
                $('#divpwd').fadeOut(500);
                $('#divoverlay').hide();
            }
            return false;
        }

        $(document).ready(function () {
            //$('.lbl1').focus(function () {
            //    $('#ContentHolder_lblError').text('');
            //    var prm = Sys.WebForms.PageRequestManager.getInstance();
            //    prm.add_endRequest(function () {
            //        BindControlEvents();
            //    });
            //});
            $('.innerView').css({ 'position': 'relative', 'overflow-y': 'auto' });
            $('#sidebar').show();
            $('#licp').hide();
            $('#lihr').hide();
            BindControlEvents();

            $('#btngenerator').click(function () {
                $('#divpwd').fadeIn(500);
                $('#divoverlay').show();
                populateform();
                return false;

            });

            $('#btngenrate').click(function () {
                populateform();
            });

            $('#btnusepass').click(function () {
                Usepassword();
            });

            $('#btncancel').click(function () {
                closegenrator();
            });


        });
        function BindControlEvents() {
            //$("#lfrom").removeClass("jqtransformdone");
            //$('#lfrom').jqTransform({ imgPath: 'jqtransformplugin/img/' });
            //$(".jqTransformSelectWrapper ul li a").click(function () {
            //    //ShowLoadingDiv();
            //});
        }

        
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentHolder" runat="Server">
    <%--<asp:updatepanel ID="up" runat="server">
                <ContentTemplate>--%>
    <div align="center">
        <div id="lfrom">
            <span style="vertical-align: middle; display: none;" class="log-in">
                <img src="../Admin/images/maintenance.jpg" alt="Down" />
            </span>
            <div class="facility" id="ctl00">
                <%--<h1 style="height: 40px; text-align: center;">
                    <span style="vertical-align: middle;" class="log-in">Change Password</span>
                </h1>--%>
                    <div class="panel-heading" style="    border-color: #e9e9e9 !important;text-align:left;">
                                <span id="span_report_heading2">
                                    <strong>
                                        <span id="div_management_header" style="    font-weight: bold;    font-size: 14px;">Change Password</span>
                                    </strong>
                                    </span>
                        </div>
                <div>
                    <table>
                        <tr>
                            <td align="left" style="text-align: left;" colspan="2">
                                <asp:Label ID="lblError" runat="server" ForeColor="Red"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" style="margin-right:2px;">
                                Current Password
                            </td>
                            <td style="padding-left:10px;">
                                <asp:TextBox ID="txtpassword" runat="server" TextMode="Password" CssClass="lbl1"
                                    ValidationGroup="save" MaxLength="25"></asp:TextBox>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" style="margin-right:2px;">
                                New Password
                            </td>
                            <td style="padding-left:10px;">
                                <asp:TextBox ID="txtNewPassword" runat="server" TextMode="Password" CssClass="lbl1"
                                    ValidationGroup="save" MaxLength="25"></asp:TextBox>&nbsp; <span title="Generate Password"
                                        id="btngenerator" style="cursor: pointer; font-size: 20px;"><i class="icon-key">
                                        </i></span>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" style="margin-right:2px;">
                                Confirm Password
                            </td>
                            <td style="padding-left:10px;">
                                <asp:TextBox ID="txtRepassword" runat="server" TextMode="Password" CssClass="lbl1"
                                    ValidationGroup="save"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td align="left" style="padding-left:8px;">
                                <asp:LinkButton ID="lnkSave" ValidationGroup="save" runat="server" class="cancgo filterButton" style=" border: 1px solid rgb(18,68,4);
        background-color: rgb(18,68,4);

    height: 26px;
    width: 65px;
    border-radius: 30px;
    border-radius: 4px !important;margin-right:0;"
                                    Text="Submit" OnClick="btnCreate_Click"></asp:LinkButton>
                                &nbsp;
                                <asp:LinkButton ID="lnkCancel" ValidationGroup="save" runat="server"  class="cancgo filterButton" style=" border: 1px solid rgb(18,68,4);
        background-color: rgb(18,68,4);

    height: 26px;
    width: 65px;
    border-radius: 30px;
    border-radius: 4px !important;margin-left:0;"
                                    Text="Clear"></asp:LinkButton>
                                <%--  <asp:Button ID="btnCreate" CssClass="canccompare" runat="server" Text="Submit" ValidationGroup="save"
                                    OnClick="btnCreate_Click" />--%>
                                <%--  <asp:ValidationSummary ID="vs" runat="server" ValidationGroup="save" ShowMessageBox="true" ShowSummary="false" />--%>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div id="divpwd" class="modal" style="display: none; width: 330px;overflow:hidden;-webkit-box-shadow: 0 5px 15px rgba(0,0,0,0.5);
    box-shadow: 0 5px 15px rgba(0,0,0,0.5);">
        <div style="z-index: 610; border-radius: 8px 8px 8px 8px;">
            <div style="background: none repeat scroll 0 0 rgb(18,68,4); z-index: 1000; position: relative;
                padding: 2px; border-bottom: 1px solid rgb(18,68,4);     height: 49px; border-radius: 3px 3px 0px 0px;">
                <table width="100%" height="100%">
                    <tbody>
                        <tr>
                            <td valign="middle" style="vertical-align: middle; font-weight: bold; color: rgb(255, 255, 255);
                                text-shadow: 0px 1px 1px rgba(0, 0, 0, 0.5); padding-left: 10px; font-size: 13px;
                                cursor: move;">
                                Password Generator
                            </td>
                            <td width="20" align="center" style="vertical-align: middle; font-family: Arial,Helvetica,sans-serif;
                                color: rgb(170, 170, 170); cursor: pointer;">
                                <div title="Close Window" class="close-wizs" onclick="closegenrator();" style="display:none;">
                                    <span style="color: #000000;"><i class="icon-remove-circle" style="font-size: 20px;
                                        margin-right: 0;"></i></span>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="window-content-wrapper">
                <table width="100%" cellpadding="0" cellspacing="10">
                    <tr>
                        <td>
                            <input type="text" id="txtpwd" style="width: 100px;height:26px;" />   <input type="button" id="btngenrate" value="Generate Password" class="canccompare filterButton"
                                style="border: 1px solid rgb(18,68,4);
        background-color: rgb(18,68,4);

    height: 26px;
    width: 150px;
    border-radius: 30px;
    border-radius: 4px !important;" />
                        </td>
                    </tr>
                   <%-- <tr>
                        <td>
                          
                        </td>
                    </tr>--%>
                    <tr>
                        <td>
                            <input type="checkbox" id="chk" />
                            I have copied this password in a safe place.
                        </td>
                    </tr>
                </table>
            </div>
            <div class="window-buttons-wrapper" style="padding: 0px; display: inline-block; width: 100%;
                border-top: 1px solid rgb(255, 255, 255); border-top: 1px solid #e5e5e5;
                z-index: 999; position: relative; text-align: right; border-radius: 0px 0px 3px 3px;">
                <div style="padding: 6px; height: 45px;">
                    <input type="button" id="btnusepass" value="Use Password" class="canccompare filterButton" style="border: 1px solid rgb(18,68,4);
        background-color: rgb(18,68,4);

    height: 26px;
    width: 110px;
    border-radius: 30px;
    border-radius: 4px !important;" />
                    <input type="button" id="btncancel" value="Cancel" class="canccompare filterButton" style="border: 1px solid rgb(18,68,4);
       background-color: rgb(18,68,4);

    height: 26px;
    width: 60px;
    border-radius: 30px;
    border-radius: 4px !important;" />
                </div>
            </div>
        </div>
    </div>
    <div id="divoverlay" class="overlay-style">
    </div>
    <%--</ContentTemplate>
                </asp:updatepanel>--%>
</asp:Content>
