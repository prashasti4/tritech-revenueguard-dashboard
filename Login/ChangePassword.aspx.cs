﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections;

public partial class ChangePassword : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
        //if (Request.Cookies["SM_FACILITY"] == null)
        //    Response.Redirect("~/Login/Login.aspx");
        //if (!IsPostBack)
        //{
        //    if (Request.UrlReferrer != null)
        //    {
        //        lnkCancel.PostBackUrl = Request.UrlReferrer.ToString();
        //        ViewState["url"] = Request.UrlReferrer.ToString();
        //    }
        //    else
        //        lnkCancel.PostBackUrl = "../Facilities/facilities.aspx";
        //}
    }

    protected void btnCreate_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            AppearenceSettingRepo objrepo = new AppearenceSettingRepo();
            string username = "";
            username = objrepo.Decryptdata(Request.Cookies["SM_FACILITY"]["Uname"]).ToString().Split('|')[0];

            string strError = "<ul>";
            if (txtpassword.Text.ToString().Trim() == "")
            {
                strError += "<li>" + "Enter Current Password." + "</li>";
            }
            if (txtNewPassword.Text.ToString().Trim() == "")
            {
                strError += "<li>" + "Enter New Password." + "</li>";
            }
            if (txtRepassword.Text.ToString().Trim() == "")
            {
                strError += "<li>" + "Enter Confirm Password." + "</li>";
            }
            if (txtNewPassword.Text.ToString().Trim() != "" && txtRepassword.Text.ToString().Trim() != "")
            {
                if (txtNewPassword.Text.ToString().Trim() != txtRepassword.Text.ToString().Trim())
                {
                    strError += "<li>" + "New Password and Confirm Password should be same." + "</li>";
                }
                else if (!IsValidPassword(txtNewPassword.Text))
                {
                    strError += "<li style='list-style:none;'>" + "Password must contain at least two of the following:" + "</li>";
                    strError += "<ul class='errorul' ><li>Alphabets</li>";
                    strError += "<li>Numbers</li>";
                    strError += "<li>Special characters</li></ul>";
                }
            }
            
            if (strError == "<ul>")
            {
                string password = AppearenceSettingRepo.MD5Hash(txtpassword.Text);
                string newpassword = AppearenceSettingRepo.MD5Hash(txtNewPassword.Text);
                string error = "Error";
                //string facility = "";
                //facility = objrepo.getLogin(username, password, out error,1);
                //if (error == "")
                //{
                lblError.Text = "";
                objrepo.ChangePassword(username, newpassword, password, out error);
                if (error == "")
                {
                    if (ViewState["url"] != null)
                        Response.Redirect(ViewState["url"].ToString());
                    else
                        Response.Redirect("../Facilities/facilities.aspx");
                }
                else
                {
                    strError += "<li>" + error + "</li>";

                }
            }

            lblError.Text = strError + "</ul>";
        }

    }

    static bool IsLetter(char c)
    {
        return (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z');
    }

    static bool IsDigit(char c)
    {
        return c >= '0' && c <= '9';
    }

    static bool IsSymbol(char c)
    {
        return c > 32 && c < 127 && !IsDigit(c) && !IsLetter(c);
    }

    static bool IsValidPassword(string password)
    {
        return
           (password.Any(c => IsLetter(c)) &&
           password.Any(c => IsDigit(c))) || (password.Any(c => IsLetter(c)) &&
           password.Any(c => IsSymbol(c))) ||(password.Any(c => IsDigit(c)) &&
           password.Any(c => IsSymbol(c)));
    }


}