﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/TriTechMaster.master" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="Login" %>
<%@ Register TagPrefix="recaptcha" Namespace="Recaptcha" Assembly="Recaptcha" %>

<asp:Content ID="Content1" ContentPlaceHolderID="pagehead" runat="Server">
    <%--<link rel="stylesheet" href="css/styleeonbi.css" type="text/css" media="all" />--%>
    <script src="../Admin/js/jquery-1.8.2.js"></script>
   <%-- <script type="text/javascript" src="https://mydashboard.ambulancerevenue.com/js/jquery-1.8.2.js"></script>--%>

    <style md-theme-style="">md-bottom-sheet.md-default-theme.md-list md-list-item , md-bottom-sheet.md-list md-list-item {    color: rgba(0,0,0,0.87);}md-bottom-sheet.md-default-theme .md-subheader , md-bottom-sheet .md-subheader {    color: rgba(0,0,0,0.87);}.md-button.md-default-theme.md-primary , .md-button.md-primary {  color: rgb(18,68,4);}.md-button.md-default-theme.md-primary.md-raised, .md-button.md-primary.md-raised, .md-button.md-default-theme.md-primary.md-fab , .md-button.md-primary.md-fab {    color: rgba(0,0,0,0.87);    background-color: rgb(18,68,4);}.md-button.md-default-theme.md-primary.md-raised:not([disabled]) md-icon, .md-button.md-primary.md-raised:not([disabled]) md-icon, .md-button.md-default-theme.md-primary.md-fab:not([disabled]) md-icon , .md-button.md-primary.md-fab:not([disabled]) md-icon {      color: rgba(0,0,0,0.87);}.md-button.md-default-theme.md-primary.md-raised:not([disabled]):hover, .md-button.md-primary.md-raised:not([disabled]):hover, .md-button.md-default-theme.md-primary.md-fab:not([disabled]):hover , .md-button.md-primary.md-fab:not([disabled]):hover {      background-color: #195c06;}.md-button.md-default-theme.md-primary.md-raised:not([disabled]).md-focused, .md-button.md-primary.md-raised:not([disabled]).md-focused, .md-button.md-default-theme.md-primary.md-fab:not([disabled]).md-focused , .md-button.md-primary.md-fab:not([disabled]).md-focused {      background-color: #195c06;}.md-button.md-default-theme.md-primary:not([disabled]) md-icon , .md-button.md-primary:not([disabled]) md-icon {    color: rgb(18,68,4);}._md a.md-default-theme:not(.md-button).md-primary , ._md a:not(.md-button).md-primary {  color: rgb(18,68,4);}._md a.md-default-theme:not(.md-button).md-primary:hover , ._md a:not(.md-button).md-primary:hover {    color: rgb(37,140,212);}md-card.md-default-theme .md-card-image , md-card .md-card-image {    border-radius: 2px 2px 0 0;}md-card.md-default-theme md-card-header md-card-header-text .md-subhead , md-card md-card-header md-card-header-text .md-subhead {    color: rgba(0,0,0,0.54);}md-card.md-default-theme md-card-title md-card-title-text:not(:only-child) .md-subhead , md-card md-card-title md-card-title-text:not(:only-child) .md-subhead {    color: rgba(0,0,0,0.54);}md-checkbox.md-default-theme .md-ink-ripple , md-checkbox .md-ink-ripple {  color: rgba(0,0,0,0.54);}md-checkbox.md-default-theme:not(.md-checked) .md-icon , md-checkbox:not(.md-checked) .md-icon {  border-color: rgba(0,0,0,0.54);}md-checkbox.md-default-theme:not([disabled]).md-primary .md-ripple , md-checkbox:not([disabled]).md-primary .md-ripple {  color: #195c06;}md-checkbox.md-default-theme:not([disabled]).md-primary.md-checked .md-ripple , md-checkbox:not([disabled]).md-primary.md-checked .md-ripple {  color: rgb(117,117,117);}md-checkbox.md-default-theme:not([disabled]).md-primary .md-ink-ripple , md-checkbox:not([disabled]).md-primary .md-ink-ripple {  color: rgba(0,0,0,0.54);}md-checkbox.md-default-theme:not([disabled]).md-primary.md-checked .md-ink-ripple , md-checkbox:not([disabled]).md-primary.md-checked .md-ink-ripple {  color: rgba(76,163,224,0.87);}md-checkbox.md-default-theme:not([disabled]).md-primary:not(.md-checked) .md-icon , md-checkbox:not([disabled]).md-primary:not(.md-checked) .md-icon {  border-color: rgba(0,0,0,0.54);}md-checkbox.md-default-theme:not([disabled]).md-primary.md-checked .md-icon , md-checkbox:not([disabled]).md-primary.md-checked .md-icon {  background-color: rgba(76,163,224,0.87);}md-checkbox.md-default-theme:not([disabled]).md-primary.md-checked.md-focused .md-container:before , md-checkbox:not([disabled]).md-primary.md-checked.md-focused .md-container:before {  background-color: rgba(76,163,224,0.26);}md-checkbox.md-default-theme:not([disabled]).md-primary.md-checked .md-icon:after , md-checkbox:not([disabled]).md-primary.md-checked .md-icon:after {  border-color: rgba(0,0,0,0.87);}md-checkbox.md-default-theme:not([disabled]).md-primary .md-indeterminate[disabled] .md-container , md-checkbox:not([disabled]).md-primary .md-indeterminate[disabled] .md-container {  color: rgba(0,0,0,0.38);}md-checkbox.md-default-theme[disabled]:not(.md-checked) .md-icon , md-checkbox[disabled]:not(.md-checked) .md-icon {  border-color: rgba(0,0,0,0.38);}md-checkbox.md-default-theme[disabled] .md-icon:after , md-checkbox[disabled] .md-icon:after {  border-color: rgba(0,0,0,0.38);}md-checkbox.md-default-theme[disabled] .md-label , md-checkbox[disabled] .md-label {  color: rgba(0,0,0,0.38);}md-chips.md-default-theme .md-chips , md-chips .md-chips {  box-shadow: 0 1px rgba(0,0,0,0.12);}md-chips.md-default-theme .md-chips.md-focused , md-chips .md-chips.md-focused {    box-shadow: 0 2px rgb(18,68,4);}md-chips.md-default-theme .md-chips .md-chip-input-container input , md-chips .md-chips .md-chip-input-container input {    color: rgba(0,0,0,0.87);}md-chips.md-default-theme .md-chips .md-chip-input-container input::-webkit-input-placeholder , md-chips .md-chips .md-chip-input-container input::-webkit-input-placeholder {      color: rgba(0,0,0,0.38);}md-chips.md-default-theme .md-chips .md-chip-input-container input:-moz-placeholder , md-chips .md-chips .md-chip-input-container input:-moz-placeholder {      color: rgba(0,0,0,0.38);}md-chips.md-default-theme .md-chips .md-chip-input-container input::-moz-placeholder , md-chips .md-chips .md-chip-input-container input::-moz-placeholder {      color: rgba(0,0,0,0.38);}md-chips.md-default-theme .md-chips .md-chip-input-container input:-ms-input-placeholder , md-chips .md-chips .md-chip-input-container input:-ms-input-placeholder {      color: rgba(0,0,0,0.38);}md-chips.md-default-theme .md-chips .md-chip-input-container input::-webkit-input-placeholder , md-chips .md-chips .md-chip-input-container input::-webkit-input-placeholder {      color: rgba(0,0,0,0.38);}md-chips.md-default-theme md-chip.md-focused , md-chips md-chip.md-focused {    background: rgb(18,68,4);    color: rgba(0,0,0,0.87);}md-chips.md-default-theme md-chip.md-focused md-icon , md-chips md-chip.md-focused md-icon {      color: rgba(0,0,0,0.87);}.md-default-theme .md-calendar-date.md-calendar-date-today .md-calendar-date-selection-indicator ,  .md-calendar-date.md-calendar-date-today .md-calendar-date-selection-indicator {  border: 1px solid rgb(18,68,4);}.md-default-theme .md-calendar-date.md-calendar-date-today.md-calendar-date-disabled ,  .md-calendar-date.md-calendar-date-today.md-calendar-date-disabled {  color: rgba(76,163,224,0.6);}.md-default-theme .md-calendar-date.md-calendar-selected-date .md-calendar-date-selection-indicator,  .md-calendar-date.md-calendar-selected-date .md-calendar-date-selection-indicator,.md-default-theme .md-calendar-date.md-focus.md-calendar-selected-date .md-calendar-date-selection-indicator ,  .md-calendar-date.md-focus.md-calendar-selected-date .md-calendar-date-selection-indicator {  background: rgb(18,68,4);  color: rgba(0,0,0,0.87);  border-color: transparent;}/** Theme styles for mdDatepicker. */.md-default-theme .md-datepicker-input ,  .md-datepicker-input {  color: rgba(0,0,0,0.87);}.md-default-theme .md-datepicker-input::-webkit-input-placeholder ,  .md-datepicker-input::-webkit-input-placeholder {    color: rgba(0,0,0,0.38);}.md-default-theme .md-datepicker-input:-moz-placeholder ,  .md-datepicker-input:-moz-placeholder {    color: rgba(0,0,0,0.38);}.md-default-theme .md-datepicker-input::-moz-placeholder ,  .md-datepicker-input::-moz-placeholder {    color: rgba(0,0,0,0.38);}.md-default-theme .md-datepicker-input:-ms-input-placeholder ,  .md-datepicker-input:-ms-input-placeholder {    color: rgba(0,0,0,0.38);}.md-default-theme .md-datepicker-input::-webkit-input-placeholder ,  .md-datepicker-input::-webkit-input-placeholder {    color: rgba(0,0,0,0.38);}.md-default-theme .md-datepicker-input-container ,  .md-datepicker-input-container {  border-bottom-color: rgba(0,0,0,0.12);}.md-default-theme .md-datepicker-input-container.md-datepicker-focused ,  .md-datepicker-input-container.md-datepicker-focused {    border-bottom-color: rgb(18,68,4);}.md-default-theme .md-datepicker-triangle-button .md-datepicker-expand-triangle ,  .md-datepicker-triangle-button .md-datepicker-expand-triangle {  border-top-color: rgba(0,0,0,0.38);}.md-default-theme .md-datepicker-triangle-button:hover .md-datepicker-expand-triangle ,  .md-datepicker-triangle-button:hover .md-datepicker-expand-triangle {  border-top-color: rgba(0,0,0,0.54);}.md-default-theme .md-datepicker-open .md-datepicker-calendar-icon ,  .md-datepicker-open .md-datepicker-calendar-icon {  color: rgb(18,68,4);}md-dialog.md-default-theme.md-content-overflow .md-actions, md-dialog.md-content-overflow .md-actions, md-dialog.md-default-theme.md-content-overflow md-dialog-actions , md-dialog.md-content-overflow md-dialog-actions {    border-top-color: rgba(0,0,0,0.12);}md-divider.md-default-theme , md-divider {  border-top-color: rgba(0,0,0,0.12);}.layout-row > md-divider.md-default-theme, .layout-row > md-divider,.layout-xs-row > md-divider.md-default-theme, .layout-xs-row > md-divider, .layout-gt-xs-row > md-divider.md-default-theme,  .layout-gt-xs-row > md-divider,.layout-sm-row > md-divider.md-default-theme, .layout-sm-row > md-divider, .layout-gt-sm-row > md-divider.md-default-theme,  .layout-gt-sm-row > md-divider,.layout-md-row > md-divider.md-default-theme, .layout-md-row > md-divider, .layout-gt-md-row > md-divider.md-default-theme,  .layout-gt-md-row > md-divider,.layout-lg-row > md-divider.md-default-theme, .layout-lg-row > md-divider, .layout-gt-lg-row > md-divider.md-default-theme,  .layout-gt-lg-row > md-divider,.layout-xl-row > md-divider.md-default-theme , .layout-xl-row > md-divider {  border-right-color: rgba(0,0,0,0.12);}md-icon.md-default-theme , md-icon {  color: rgba(0,0,0,0.54);}md-icon.md-default-theme.md-primary , md-icon.md-primary {    color: rgb(18,68,4);}md-input-container.md-default-theme .md-input , md-input-container .md-input {  color: rgba(0,0,0,0.87);  border-color: rgba(0,0,0,0.12);}md-input-container.md-default-theme .md-input::-webkit-input-placeholder , md-input-container .md-input::-webkit-input-placeholder {    color: rgba(0,0,0,0.38);}md-input-container.md-default-theme .md-input:-moz-placeholder , md-input-container .md-input:-moz-placeholder {    color: rgba(0,0,0,0.38);}md-input-container.md-default-theme .md-input::-moz-placeholder , md-input-container .md-input::-moz-placeholder {    color: rgba(0,0,0,0.38);}md-input-container.md-default-theme .md-input:-ms-input-placeholder , md-input-container .md-input:-ms-input-placeholder {    color: rgba(0,0,0,0.38);}md-input-container.md-default-theme .md-input::-webkit-input-placeholder , md-input-container .md-input::-webkit-input-placeholder {    color: rgba(0,0,0,0.38);}md-input-container.md-default-theme > md-icon , md-input-container > md-icon {  color: rgba(0,0,0,0.87);}md-input-container.md-default-theme label, md-input-container label,md-input-container.md-default-theme .md-placeholder , md-input-container .md-placeholder {  color: rgba(0,0,0,0.38);}md-input-container.md-default-theme:not(.md-input-focused):not(.md-input-invalid) label.md-required:after , md-input-container:not(.md-input-focused):not(.md-input-invalid) label.md-required:after {  color: rgba(0,0,0,0.54);}md-input-container.md-default-theme .md-input-messages-animation .md-char-counter, md-input-container .md-input-messages-animation .md-char-counter, md-input-container.md-default-theme .md-input-message-animation .md-char-counter , md-input-container .md-input-message-animation .md-char-counter {    color: rgba(0,0,0,0.87);}md-input-container.md-default-theme:not(.md-input-invalid).md-input-has-value label , md-input-container:not(.md-input-invalid).md-input-has-value label {  color: rgba(0,0,0,0.54);}md-input-container.md-default-theme:not(.md-input-invalid).md-input-focused .md-input, md-input-container:not(.md-input-invalid).md-input-focused .md-input, md-input-container.md-default-theme:not(.md-input-invalid).md-input-resized .md-input , md-input-container:not(.md-input-invalid).md-input-resized .md-input {  border-color: rgb(18,68,4);}md-input-container.md-default-theme:not(.md-input-invalid).md-input-focused label, md-input-container:not(.md-input-invalid).md-input-focused label,md-input-container.md-default-theme:not(.md-input-invalid).md-input-focused md-icon , md-input-container:not(.md-input-invalid).md-input-focused md-icon {  color: rgb(18,68,4);}md-list.md-default-theme md-list-item.md-2-line .md-list-item-text h3, md-list md-list-item.md-2-line .md-list-item-text h3, md-list.md-default-theme md-list-item.md-2-line .md-list-item-text h4, md-list md-list-item.md-2-line .md-list-item-text h4,md-list.md-default-theme md-list-item.md-3-line .md-list-item-text h3, md-list md-list-item.md-3-line .md-list-item-text h3,md-list.md-default-theme md-list-item.md-3-line .md-list-item-text h4 , md-list md-list-item.md-3-line .md-list-item-text h4 {  color: rgba(0,0,0,0.87);}md-list.md-default-theme md-list-item.md-2-line .md-list-item-text p, md-list md-list-item.md-2-line .md-list-item-text p,md-list.md-default-theme md-list-item.md-3-line .md-list-item-text p , md-list md-list-item.md-3-line .md-list-item-text p {  color: rgba(0,0,0,0.54);}md-list.md-default-theme md-list-item > md-icon , md-list md-list-item > md-icon {  color: rgba(0,0,0,0.54);}md-list.md-default-theme md-list-item > md-icon.md-highlight , md-list md-list-item > md-icon.md-highlight {    color: rgb(18,68,4);}md-menu-bar.md-default-theme > button.md-button , md-menu-bar > button.md-button {  color: rgba(0,0,0,0.54);  border-radius: 2px;}md-toolbar.md-default-theme.md-menu-toolbar md-toolbar-filler , md-toolbar.md-menu-toolbar md-toolbar-filler {    background-color: rgb(18,68,4);    color: rgba(255,255,255,0.87);}md-nav-bar.md-default-theme .md-button._md-nav-button.md-unselected , md-nav-bar .md-button._md-nav-button.md-unselected {  color: rgba(0,0,0,0.54);}md-progress-circular.md-default-theme path , md-progress-circular path {  stroke: rgb(18,68,4);}md-progress-linear.md-default-theme .md-container , md-progress-linear .md-container {  background-color: rgb(163,208,239);}md-progress-linear.md-default-theme .md-bar , md-progress-linear .md-bar {  background-color: rgb(18,68,4);}md-radio-button.md-default-theme .md-off , md-radio-button .md-off {  border-color: rgba(0,0,0,0.54);}md-radio-group.md-default-theme:not([disabled]) .md-primary .md-on, md-radio-group:not([disabled]) .md-primary .md-on, md-radio-group.md-default-theme:not([disabled]).md-primary .md-on, md-radio-group:not([disabled]).md-primary .md-on,md-radio-button.md-default-theme:not([disabled]) .md-primary .md-on, md-radio-button:not([disabled]) .md-primary .md-on,md-radio-button.md-default-theme:not([disabled]).md-primary .md-on , md-radio-button:not([disabled]).md-primary .md-on {  background-color: rgba(76,163,224,0.87);}md-radio-group.md-default-theme:not([disabled]) .md-primary .md-checked .md-off, md-radio-group:not([disabled]) .md-primary .md-checked .md-off, md-radio-group.md-default-theme:not([disabled]) .md-primary.md-checked .md-off, md-radio-group:not([disabled]) .md-primary.md-checked .md-off, md-radio-group.md-default-theme:not([disabled]).md-primary .md-checked .md-off, md-radio-group:not([disabled]).md-primary .md-checked .md-off, md-radio-group.md-default-theme:not([disabled]).md-primary.md-checked .md-off, md-radio-group:not([disabled]).md-primary.md-checked .md-off,md-radio-button.md-default-theme:not([disabled]) .md-primary .md-checked .md-off, md-radio-button:not([disabled]) .md-primary .md-checked .md-off,md-radio-button.md-default-theme:not([disabled]) .md-primary.md-checked .md-off, md-radio-button:not([disabled]) .md-primary.md-checked .md-off,md-radio-button.md-default-theme:not([disabled]).md-primary .md-checked .md-off, md-radio-button:not([disabled]).md-primary .md-checked .md-off,md-radio-button.md-default-theme:not([disabled]).md-primary.md-checked .md-off , md-radio-button:not([disabled]).md-primary.md-checked .md-off {  border-color: rgba(76,163,224,0.87);}md-radio-group.md-default-theme:not([disabled]) .md-primary .md-checked .md-ink-ripple, md-radio-group:not([disabled]) .md-primary .md-checked .md-ink-ripple, md-radio-group.md-default-theme:not([disabled]) .md-primary.md-checked .md-ink-ripple, md-radio-group:not([disabled]) .md-primary.md-checked .md-ink-ripple, md-radio-group.md-default-theme:not([disabled]).md-primary .md-checked .md-ink-ripple, md-radio-group:not([disabled]).md-primary .md-checked .md-ink-ripple, md-radio-group.md-default-theme:not([disabled]).md-primary.md-checked .md-ink-ripple, md-radio-group:not([disabled]).md-primary.md-checked .md-ink-ripple,md-radio-button.md-default-theme:not([disabled]) .md-primary .md-checked .md-ink-ripple, md-radio-button:not([disabled]) .md-primary .md-checked .md-ink-ripple,md-radio-button.md-default-theme:not([disabled]) .md-primary.md-checked .md-ink-ripple, md-radio-button:not([disabled]) .md-primary.md-checked .md-ink-ripple,md-radio-button.md-default-theme:not([disabled]).md-primary .md-checked .md-ink-ripple, md-radio-button:not([disabled]).md-primary .md-checked .md-ink-ripple,md-radio-button.md-default-theme:not([disabled]).md-primary.md-checked .md-ink-ripple , md-radio-button:not([disabled]).md-primary.md-checked .md-ink-ripple {  color: rgba(76,163,224,0.87);}md-radio-group.md-default-theme:not([disabled]) .md-primary .md-container .md-ripple, md-radio-group:not([disabled]) .md-primary .md-container .md-ripple, md-radio-group.md-default-theme:not([disabled]).md-primary .md-container .md-ripple, md-radio-group:not([disabled]).md-primary .md-container .md-ripple,md-radio-button.md-default-theme:not([disabled]) .md-primary .md-container .md-ripple, md-radio-button:not([disabled]) .md-primary .md-container .md-ripple,md-radio-button.md-default-theme:not([disabled]).md-primary .md-container .md-ripple , md-radio-button:not([disabled]).md-primary .md-container .md-ripple {  color: #195c06;}md-radio-group.md-default-theme[disabled], md-radio-group[disabled],md-radio-button.md-default-theme[disabled] , md-radio-button[disabled] {  color: rgba(0,0,0,0.38);}md-radio-group.md-default-theme[disabled] .md-container .md-off, md-radio-group[disabled] .md-container .md-off,  md-radio-button.md-default-theme[disabled] .md-container .md-off ,   md-radio-button[disabled] .md-container .md-off {    border-color: rgba(0,0,0,0.38);}md-radio-group.md-default-theme[disabled] .md-container .md-on, md-radio-group[disabled] .md-container .md-on,  md-radio-button.md-default-theme[disabled] .md-container .md-on ,   md-radio-button[disabled] .md-container .md-on {    border-color: rgba(0,0,0,0.38);}md-radio-group.md-default-theme.md-primary .md-checked:not([disabled]) .md-ink-ripple, md-radio-group.md-primary .md-checked:not([disabled]) .md-ink-ripple, md-radio-group.md-default-theme .md-checked:not([disabled]).md-primary .md-ink-ripple , md-radio-group .md-checked:not([disabled]).md-primary .md-ink-ripple {  color: rgba(76,163,224,0.26);}md-radio-group.md-default-theme .md-checked.md-primary .md-ink-ripple , md-radio-group .md-checked.md-primary .md-ink-ripple {  color: '{{warn-color-0.26}}';}md-radio-group.md-default-theme.md-focused:not(:empty).md-primary .md-checked .md-container:before, md-radio-group.md-focused:not(:empty).md-primary .md-checked .md-container:before,md-radio-group.md-default-theme.md-focused:not(:empty) .md-checked.md-primary .md-container:before , md-radio-group.md-focused:not(:empty) .md-checked.md-primary .md-container:before {  background-color: rgba(76,163,224,0.26);}md-input-container:not(.md-input-focused):not(.md-input-invalid) md-select.md-default-theme .md-select-value span:first-child:after , md-input-container:not(.md-input-focused):not(.md-input-invalid) md-select .md-select-value span:first-child:after {  color: rgba(0,0,0,0.38);}md-input-container.md-input-focused:not(.md-input-has-value) md-select.md-default-theme .md-select-value , md-input-container.md-input-focused:not(.md-input-has-value) md-select .md-select-value {  color: rgb(18,68,4);}md-input-container.md-input-focused:not(.md-input-has-value) md-select.md-default-theme .md-select-value.md-select-placeholder , md-input-container.md-input-focused:not(.md-input-has-value) md-select .md-select-value.md-select-placeholder {    color: rgb(18,68,4);}md-input-container.md-input-invalid md-select.md-default-theme.md-no-underline .md-select-value , md-input-container.md-input-invalid md-select.md-no-underline .md-select-value {  border-bottom-color: transparent !important;}md-select.md-default-theme .md-select-value , md-select .md-select-value {  border-bottom-color: rgba(0,0,0,0.12);}md-select.md-default-theme .md-select-value.md-select-placeholder , md-select .md-select-value.md-select-placeholder {    color: rgba(0,0,0,0.38);}md-select.md-default-theme.md-no-underline .md-select-value , md-select.md-no-underline .md-select-value {  border-bottom-color: transparent !important;}md-select.md-default-theme.ng-invalid.ng-touched.md-no-underline .md-select-value , md-select.ng-invalid.ng-touched.md-no-underline .md-select-value {  border-bottom-color: transparent !important;}md-select.md-default-theme:not([disabled]):focus .md-select-value , md-select:not([disabled]):focus .md-select-value {  border-bottom-color: rgb(18,68,4);  color: rgba(0,0,0,0.87);}md-select.md-default-theme:not([disabled]):focus .md-select-value.md-select-placeholder , md-select:not([disabled]):focus .md-select-value.md-select-placeholder {    color: rgba(0,0,0,0.87);}md-select.md-default-theme:not([disabled]):focus.md-no-underline .md-select-value , md-select:not([disabled]):focus.md-no-underline .md-select-value {  border-bottom-color: transparent !important;}md-select.md-default-theme[disabled] .md-select-value , md-select[disabled] .md-select-value {  color: rgba(0,0,0,0.38);}md-select.md-default-theme[disabled] .md-select-value.md-select-placeholder , md-select[disabled] .md-select-value.md-select-placeholder {    color: rgba(0,0,0,0.38);}md-select-menu.md-default-theme md-content md-option[selected] , md-select-menu md-content md-option[selected] {      color: rgb(18,68,4);}md-select-menu.md-default-theme md-content md-option[selected]:focus , md-select-menu md-content md-option[selected]:focus {        color: #195c06;}.md-checkbox-enabled.md-default-theme .md-ripple , .md-checkbox-enabled .md-ripple {  color: #195c06;}.md-checkbox-enabled.md-default-theme .md-ink-ripple , .md-checkbox-enabled .md-ink-ripple {  color: rgba(0,0,0,0.54);}.md-checkbox-enabled.md-default-theme[selected] .md-ink-ripple , .md-checkbox-enabled[selected] .md-ink-ripple {  color: rgba(76,163,224,0.87);}.md-checkbox-enabled.md-default-theme:not(.md-checked) .md-icon , .md-checkbox-enabled:not(.md-checked) .md-icon {  border-color: rgba(0,0,0,0.54);}.md-checkbox-enabled.md-default-theme[selected] .md-icon , .md-checkbox-enabled[selected] .md-icon {  background-color: rgba(76,163,224,0.87);}.md-checkbox-enabled.md-default-theme[selected].md-focused .md-container:before , .md-checkbox-enabled[selected].md-focused .md-container:before {  background-color: rgba(76,163,224,0.26);}.md-checkbox-enabled.md-default-theme[selected] .md-icon:after , .md-checkbox-enabled[selected] .md-icon:after {  border-color: rgba(0,0,0,0.87);}.md-checkbox-enabled.md-default-theme .md-indeterminate[disabled] .md-container , .md-checkbox-enabled .md-indeterminate[disabled] .md-container {  color: rgba(0,0,0,0.38);}md-slider.md-default-theme.md-primary .md-focus-ring , md-slider.md-primary .md-focus-ring {  background-color: rgba(141,197,235,0.38);}md-slider.md-default-theme.md-primary .md-track.md-track-fill , md-slider.md-primary .md-track.md-track-fill {  background-color: rgb(18,68,4);}md-slider.md-default-theme.md-primary .md-thumb:after , md-slider.md-primary .md-thumb:after {  border-color: rgb(18,68,4);  background-color: rgb(18,68,4);}md-slider.md-default-theme.md-primary .md-sign , md-slider.md-primary .md-sign {  background-color: rgb(18,68,4);}md-slider.md-default-theme.md-primary .md-sign:after , md-slider.md-primary .md-sign:after {    border-top-color: rgb(18,68,4);}md-slider.md-default-theme.md-primary[md-vertical] .md-sign:after , md-slider.md-primary[md-vertical] .md-sign:after {  border-top-color: transparent;  border-left-color: rgb(18,68,4);}md-slider.md-default-theme.md-primary .md-thumb-text , md-slider.md-primary .md-thumb-text {  color: rgba(0,0,0,0.87);}md-slider.md-default-theme[disabled] .md-thumb:after , md-slider[disabled] .md-thumb:after {  border-color: transparent;}md-slider-container[disabled] > *:first-child:not(md-slider),md-slider-container[disabled] > *:last-child:not(md-slider) {  color: rgba(0,0,0,0.38);}.md-subheader.md-default-theme.md-primary , .md-subheader.md-primary {    color: rgb(18,68,4);}md-switch.md-default-theme.md-checked.md-primary .md-ink-ripple , md-switch.md-checked.md-primary .md-ink-ripple {  color: rgb(18,68,4);}md-switch.md-default-theme.md-checked.md-primary .md-thumb , md-switch.md-checked.md-primary .md-thumb {  background-color: rgb(18,68,4);}md-switch.md-default-theme.md-checked.md-primary .md-bar , md-switch.md-checked.md-primary .md-bar {  background-color: rgba(76,163,224,0.5);}md-switch.md-default-theme.md-checked.md-primary.md-focused .md-thumb:before , md-switch.md-checked.md-primary.md-focused .md-thumb:before {  background-color: rgba(76,163,224,0.26);}md-tabs.md-default-theme .md-paginator md-icon , md-tabs .md-paginator md-icon {  color: rgb(18,68,4);}md-tabs.md-default-theme .md-tab , md-tabs .md-tab {  color: rgba(0,0,0,0.54);}md-tabs.md-default-theme .md-tab[disabled], md-tabs .md-tab[disabled], md-tabs.md-default-theme .md-tab[disabled] md-icon , md-tabs .md-tab[disabled] md-icon {    color: rgba(0,0,0,0.38);}md-tabs.md-default-theme .md-tab.md-active, md-tabs .md-tab.md-active, md-tabs.md-default-theme .md-tab.md-active md-icon, md-tabs .md-tab.md-active md-icon, md-tabs.md-default-theme .md-tab.md-focused, md-tabs .md-tab.md-focused, md-tabs.md-default-theme .md-tab.md-focused md-icon , md-tabs .md-tab.md-focused md-icon {    color: rgb(18,68,4);}md-tabs.md-default-theme .md-tab.md-focused , md-tabs .md-tab.md-focused {    background: rgba(76,163,224,0.1);}md-tabs.md-default-theme.md-primary > md-tabs-wrapper , md-tabs.md-primary > md-tabs-wrapper {  background-color: rgb(18,68,4);}md-tabs.md-default-theme.md-primary > md-tabs-wrapper > md-tabs-canvas > md-pagination-wrapper > md-tab-item:not([disabled]) , md-tabs.md-primary > md-tabs-wrapper > md-tabs-canvas > md-pagination-wrapper > md-tab-item:not([disabled]) {    color: rgb(163,208,239);}md-tabs.md-default-theme.md-primary > md-tabs-wrapper > md-tabs-canvas > md-pagination-wrapper > md-tab-item:not([disabled]).md-active, md-tabs.md-primary > md-tabs-wrapper > md-tabs-canvas > md-pagination-wrapper > md-tab-item:not([disabled]).md-active, md-tabs.md-default-theme.md-primary > md-tabs-wrapper > md-tabs-canvas > md-pagination-wrapper > md-tab-item:not([disabled]).md-active md-icon, md-tabs.md-primary > md-tabs-wrapper > md-tabs-canvas > md-pagination-wrapper > md-tab-item:not([disabled]).md-active md-icon, md-tabs.md-default-theme.md-primary > md-tabs-wrapper > md-tabs-canvas > md-pagination-wrapper > md-tab-item:not([disabled]).md-focused, md-tabs.md-primary > md-tabs-wrapper > md-tabs-canvas > md-pagination-wrapper > md-tab-item:not([disabled]).md-focused, md-tabs.md-default-theme.md-primary > md-tabs-wrapper > md-tabs-canvas > md-pagination-wrapper > md-tab-item:not([disabled]).md-focused md-icon , md-tabs.md-primary > md-tabs-wrapper > md-tabs-canvas > md-pagination-wrapper > md-tab-item:not([disabled]).md-focused md-icon {      color: rgba(0,0,0,0.87);}md-tabs.md-default-theme.md-primary > md-tabs-wrapper > md-tabs-canvas > md-pagination-wrapper > md-tab-item:not([disabled]).md-focused , md-tabs.md-primary > md-tabs-wrapper > md-tabs-canvas > md-pagination-wrapper > md-tab-item:not([disabled]).md-focused {      background: rgba(0,0,0,0.1);}md-toolbar > md-tabs.md-default-theme > md-tabs-wrapper , md-toolbar > md-tabs > md-tabs-wrapper {  background-color: rgb(18,68,4);}md-toolbar > md-tabs.md-default-theme > md-tabs-wrapper > md-tabs-canvas > md-pagination-wrapper > md-tab-item:not([disabled]) , md-toolbar > md-tabs > md-tabs-wrapper > md-tabs-canvas > md-pagination-wrapper > md-tab-item:not([disabled]) {    color: rgb(163,208,239);}md-toolbar > md-tabs.md-default-theme > md-tabs-wrapper > md-tabs-canvas > md-pagination-wrapper > md-tab-item:not([disabled]).md-active, md-toolbar > md-tabs > md-tabs-wrapper > md-tabs-canvas > md-pagination-wrapper > md-tab-item:not([disabled]).md-active, md-toolbar > md-tabs.md-default-theme > md-tabs-wrapper > md-tabs-canvas > md-pagination-wrapper > md-tab-item:not([disabled]).md-active md-icon,  md-toolbar > md-tabs > md-tabs-wrapper > md-tabs-canvas > md-pagination-wrapper > md-tab-item:not([disabled]).md-active md-icon, md-toolbar > md-tabs.md-default-theme > md-tabs-wrapper > md-tabs-canvas > md-pagination-wrapper > md-tab-item:not([disabled]).md-focused,  md-toolbar > md-tabs > md-tabs-wrapper > md-tabs-canvas > md-pagination-wrapper > md-tab-item:not([disabled]).md-focused, md-toolbar > md-tabs.md-default-theme > md-tabs-wrapper > md-tabs-canvas > md-pagination-wrapper > md-tab-item:not([disabled]).md-focused md-icon ,  md-toolbar > md-tabs > md-tabs-wrapper > md-tabs-canvas > md-pagination-wrapper > md-tab-item:not([disabled]).md-focused md-icon {      color: rgba(0,0,0,0.87);}md-toolbar > md-tabs.md-default-theme > md-tabs-wrapper > md-tabs-canvas > md-pagination-wrapper > md-tab-item:not([disabled]).md-focused , md-toolbar > md-tabs > md-tabs-wrapper > md-tabs-canvas > md-pagination-wrapper > md-tab-item:not([disabled]).md-focused {      background: rgba(0,0,0,0.1);}md-toast.md-default-theme .md-toast-content .md-button.md-highlight.md-primary , md-toast .md-toast-content .md-button.md-highlight.md-primary {        color: rgb(18,68,4);}md-toolbar.md-default-theme:not(.md-menu-toolbar) , md-toolbar:not(.md-menu-toolbar) {  background-color: rgb(18,68,4);  color: rgba(0,0,0,0.87);}md-toolbar.md-default-theme:not(.md-menu-toolbar) md-icon , md-toolbar:not(.md-menu-toolbar) md-icon {    color: rgba(0,0,0,0.87);    fill: rgba(0,0,0,0.87);}md-toolbar.md-default-theme:not(.md-menu-toolbar) .md-button[disabled] md-icon , md-toolbar:not(.md-menu-toolbar) .md-button[disabled] md-icon {    color: rgba(0,0,0,0.26);    fill: rgba(0,0,0,0.26);}</style>


  
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentHolder" runat="Server">

    <style type="text/css">
       
       
     
        .recaptchatable .recaptcha_r1_c1 {
  background: url('../Admin/images/captcha.png?201701190331') 0 -63px no-repeat !important;
  /*width: 268px !important;
  height: 9px !important;*/
}

        .recaptchatable .recaptcha_r2_c1 {
  background: url('../Admin/images/captcha.png?201701190331') -18px 0 no-repeat !important;
  /*width: 9px !important;
  height: 57px !important;*/
}


        .recaptchatable .recaptcha_r2_c2 {
  background: url('../Admin/images/captcha.png?201701190331') -27px 0 no-repeat !important;
  /*width: 9px !important;
  height: 57px !important;*/
}

        .recaptchatable .recaptcha_r3_c1 {
  background: url('../Admin/images/captcha.png?201701190331') 0 0 no-repeat !important;
  /*width: 9px !important;
  height: 63px !important;*/
}

        .recaptchatable .recaptcha_r3_c2 {
  background: url('../Admin/images/captcha.png?201701190331') -11px -64px no-repeat !important;
  /*width: 250px !important;
  height: 6px !important;*/
}

        .recaptchatable .recaptcha_r3_c3 {
  background: url('../Admin/images/captcha.png?201701190331') -9px 0 no-repeat !important;
  /*width: 9px !important;
  height: 63px !important;*/
}

        .recaptchatable .recaptcha_r4_c1 {
  background: url('../Admin/images/captcha.png?201701190331') -43px 0 no-repeat !important;
  /*width: 121px !important;
  height: 49px !important;*/
}

        .recaptchatable .recaptcha_r4_c2 {
  background: url('../Admin/images/captcha.png?201701190331')  -36px 0 no-repeat !important;
  /*width: 7px !important;
  height: 57px !important;*/
}

        .recaptchatable .recaptcha_r4_c4 {
  background: url('../Admin/images/captcha.png?201701190331') -214px 0 no-repeat !important;
  /*width: 97px !important;
  height: 57px !important;*/
}

        .recaptchatable .recaptcha_r7_c1 {
  background: url('../Admin/images/captcha.png?201701190331') -43px -49px no-repeat !important;
  /*width: 121px !important;
  height: 8px !important;*/
}

        .recaptchatable .recaptcha_r8_c1 {
  background: url('../Admin/images/captcha.png?201701190331') -43px -49px no-repeat !important;
  /*width: 25px !important;
  height: 8px !important;*/
}

        .recaptcha_r7_c1{
            display:none;
        }

         .recaptcha_r8_c1{
            display:none;
        }
         #recaptcha_reload {
content:url(../Admin/images/refresh.png?201701190331);
    height: 18px;
    
}
          #recaptcha_switch_audio {
content:url(../Admin/images/audio.png?201701190331);
    height: 18px;
   
}
           #recaptcha_whatsthis {
content:url(../Admin/images/help.png?201701190331);
    height: 18px;
    padding-bottom: 2px !important;
}


           /*#recaptcha_area, #recaptcha_table {
    width: 265px!important;
}

        #recaptcha_area table {
  width: 268px !important;
}

           #recaptcha_image, #recaptcha_challenge_image
           {
               width: 250px !important;
           }
           .recaptcha_input_area, .recaptchatable #recaptcha_response_field
           {
               width: 103px !important;
           }


          .recaptcha_r4_c1 div {
  position: relative !important;
  width: 103px !important;
  height: 42px !important;
  margin-left: 7px !important;
  margin-right: 7px !important;
  background: none !important;
}


 .recaptcha_r4_c1 .recaptcha_input_area #recaptcha_response_field
           {
               width: 103px !important;
           }*/
 .panel-default{
         border: none;
    border-top: none;
    background: none;
    box-shadow: none;
    border-top:none !important;
 }
 .loginBox{
     /*background: #fff;
     border-top: 1px solid #c6c6c6;
    border-bottom: 1px solid #c6c6c6;


    background: #fff;
    border: 1px solid rgba(147, 184, 189,0.8) !important;
    box-shadow: 0pt 2px 5px rgba(105, 108, 109, 0.7), 0px 0px 8px 5px rgba(208, 223, 226, 0.4) inset !important;
        border-radius: 5px !important;*/
        font-family: "Trebuchet MS","Myriad Pro",Arial,sans-serif;
        font-weight: 400;
    font-size: 17px;
    color: #1d3c41;
 }

 .form-2 input[type=text], .form-2 input[type=password] {
    /*font-family: 'Lato' , Calibri, Arial, sans-serif;
    font-size: 13px;
    font-weight: 400;
    display: block;
    width: 100%;
    padding: 5px;
    margin-bottom: 5px;
    margin-left: 0px;
    border: 3px solid #d7d7d7;
    border-radius: 5px;
    -webkit-transition: all 0.3s ease-out;
    -moz-transition: all 0.3s ease-out;
    -ms-transition: all 0.3s ease-out;
    -o-transition: all 0.3s ease-out;
    transition: all 0.3s ease-out;*/

    width: 87%;
    margin-top: 4px;
    padding: 16px 0px 16px 41px;
    border: 1px solid #d2d2d2;
    -webkit-appearance: textfield;
    -webkit-box-sizing: content-box;
    -moz-box-sizing: content-box;
    box-sizing: content-box;
    -webkit-border-radius: 3px;
    -moz-border-radius: 3px;
    border-radius: 3px;
    /*-webkit-box-shadow: 0px 1px 4px 0px rgba(168, 168, 168, 0.2) inset;
    -moz-box-shadow: 0px 1px 4px 0px rgba(168, 168, 168, 0.2) inset;
    box-shadow: 0px 1px 4px 0px rgba(168, 168, 168, 0.2) inset;*/
    -webkit-transition: all 0.2s linear;
    -moz-transition: all 0.2s linear;
    -o-transition: all 0.2s linear;
    transition: all 0.2s linear;
    float: left;
        outline: none;
        height: 14px;
        color:#555;
}
 /*.form-2 input[type=text]:hover, .form-2 input[type=password]:hover {
    
    border: 1px solid rgba(91, 90, 90, 0.7);
	background: rgba(238, 236, 240, 0.2);	
	-webkit-box-shadow: 0px 1px 4px 0px rgba(168, 168, 168, 0.4) inset;
	   -moz-box-shadow: 0px 1px 4px 0px rgba(168, 168, 168, 0.4) inset;
	        box-shadow: 0px 1px 4px 0px rgba(168, 168, 168, 0.4) inset;
}*/

 [class^="icon-"], [class*=" icon-"]
 {
         position: absolute;
   
    padding-top: 18px;
    left: 0;
    padding-left: 19px;
        color: #767676;
    font-size: 20px;
 }

 .login_header{
   
    /*background: -webkit-repeating-linear-gradient(-45deg, rgb(18, 83, 93) , rgb(18, 83, 93) 20px, rgb(64, 111, 118) 20px, rgb(64, 111, 118) 40px, rgb(18, 83, 93) 40px);*/
    /*background: #666;*/
    /*-webkit-text-fill-color: transparent;
    -webkit-background-clip: text;*/

        font-size: 28px;
    color: #767676;
   
    font-family: 'Arial Narrow',Arial,sans-serif;
    font-weight: bold;
    text-align: center;
    margin-top: 0;
       margin-bottom: 10px;
           line-height: 3rem;
}

        .login_header:after {
            content: ' ';
    display: block;
    width: 100%;
    height: 2px;
    margin-top: 10px;
    background: -moz-linear-gradient(left, rgba(147,184,189,0) 0%, rgba(147,184,189,0.8) 20%, rgba(147,184,189,1) 53%, rgba(147,184,189,0.8) 79%, rgba(147,184,189,0) 100%);
    background: -webkit-gradient(linear, left top, right top, color-stop(0%,rgba(147,184,189,0)), color-stop(20%,rgba(147,184,189,0.8)), color-stop(53%,rgba(147,184,189,1)), color-stop(79%,rgba(147,184,189,0.8)), color-stop(100%,rgba(147,184,189,0)));
   
    background: -o-linear-gradient(left, rgba(147,184,189,0) 0%,rgba(147,184,189,0.8) 20%,rgba(147,184,189,1) 53%,rgba(147,184,189,0.8) 79%,rgba(147,184,189,0) 100%);
    background: -ms-linear-gradient(left, rgba(147,184,189,0) 0%,rgba(147,184,189,0.8) 20%,rgba(147,184,189,1) 53%,rgba(147,184,189,0.8) 79%,rgba(147,184,189,0) 100%);
    background: linear-gradient(left, rgba(147,184,189,0) 0%,rgba(147,184,189,0.8) 20%,rgba(147,184,189,1) 53%,rgba(147,184,189,0.8) 79%,rgba(147,184,189,0) 100%);
        }
        [data-icon]:after {
    content: attr(data-icon);
    font-family: 'FontomasCustomRegular';
    color: rgb(106, 159, 171);
    position: absolute;
    left: 10px;
    top: 35px;
	width: 30px;
        margin-bottom: 5px;
}

        .loginBox {
    max-width: 90%;
    /*width: 500px;*/
    /*width:370px;*/
     width:400px;
    vertical-align: middle;
}
        #ContentHolder_lblError p{
                font-size: 12px;
                    line-height: normal;
                        margin: 0px;
        }
        .md-whiteframe-1dp, .md-whiteframe-z1{
            box-shadow:none;
        }
        .md-button{
            min-height:40px;
            font-family:Arial;
            font-weight:normal;
        }

    </style>

    <div align="center">
        <div id="lfrom">
            <div class="form-2" id="ctl00">
                <div id="login_body" class="loginBox md-whiteframe-z1 layout-column" ,="" layout="column"  style="top:20%;left:33%;">
           <%-- <md-toolbar id="login-md_toolbar" class="_md _md-toolbar-transitions">
                
        <span><img src="../Admin/images/icons/Revenue-Guard_Logo.png" style="margin-top:2%;"></span>
            </md-toolbar>--%>
            <md-content class="md-padding _md layout-column" ,="" layout="column" style="padding-left: 12px;
    padding-right: 12px;margin-top: 20px;
    margin-bottom: 20px;
    background: #fff;
    border-radius: 3px;    box-shadow: 0 0 40px #d4d2d2;">
                <%--<h1 style="    height: 40px;
    font-size: 18px;
    font-weight: bold;
    color: #bdb5aa;
    padding-bottom: 8px;
    border-bottom: 1px solid #e9e8e8;
    text-shadow: 0 2px 0 rgba(255,255,255,0.8);
    box-shadow: 0 1px 0 rgba(255,255,255,0.8);
    text-align: center;line-height: 0;">
                    <div style="width: 50px; margin-left: auto; margin-right: auto;">
                        <span style="display: table-cell; vertical-align: middle;    color: #6c6763;font-size:22px;
    padding-right: 2px;    padding-top: 7px;" class="log-in">Login</span>
                    </div>
                </h1>--%>
                <h1 style="font-size: 21px;line-height: 3rem;
    color: #767676;
   
    font-family: 'Arial Narrow',Arial,sans-serif;margin-bottom: 8px;
    margin-top: 8px;
    font-weight: bold;">Login</h1>
                  <asp:Label ForeColor="Red" ID="lblError" Font-Size="12px" Visible="false" runat="server" style="    width: auto;"></asp:Label>
                <md-input-container style="padding-top: 2%;margin-bottom:0px;margin-top:0px;margin-left: 9px;margin-right: 3px;" class="md-input-has-value">
                    <i class="icon-user"></i>
                     
                    <input id="txtusername" runat="server" type="text" style="font-size:14px;font-family:Arial;"  placeholder="Username">

                </md-input-container>
                <md-input-container class="md-input-has-value" style="padding-bottom:0px;margin-bottom: 10px;margin-top:12px;margin-left: 9px;margin-right: 3px;">
                   <i class="icon-lock"></i>
                    <input type="password" id="txtpassword" runat="server" style="font-size:14px;font-family:Arial;"   placeholder="Password">
                </md-input-container>
               
                <table style="" class="clearfix" id="tbl_captcha" runat="server" visible="false">
                        <tr>
                            <td>
                                 <%--<recaptcha:RecaptchaControl ID="recaptcha" runat="server" PublicKey="6LcXRREUAAAAALqY-RlUx7pf42ZevUgjMHTKK9na"
                    PrivateKey="6LcXRREUAAAAAO7oAApS1M7mJz89cDFz1bNdD9U2" />--%>
                                <recaptcha:RecaptchaControl ID="recaptcha" runat="server" PublicKey="6LdY6AMTAAAAANpRoQIgE7_HWyPXwBRwgv4Q5f04"
                    PrivateKey="6LdY6AMTAAAAAPzBQumWrhe1K65lfnG_iMWvMyiN" />
                            </td>
                        </tr>
                    </table>
                <div layout="row" layout-align="center center" style="padding-top:1%;   margin-bottom: 20px;
    margin-top: 20px;" class="layout-align-center-center layout-row">
                   <%-- <button onclick="exportall();" style="display:none;">Export</button>--%>
                    <asp:Button id="Button1" runat="server" OnClientClick="$('.divloading').show();" class="md-raised md-primary md-button md-ink-ripple" Text="Login" 
                        onclick="btnsubmit_Click" style="    color: white;
    width: 42%;margin-right: 25px;" />
                   <%-- <button class="md-raised md-primary md-button md-ink-ripple" type="button" ng-transclude="" style="color:white; width: 45%;" onclick="return Reset();"><span class="ng-scope">Reset</span></button>                 --%>
                 <asp:Button id="btnReset" runat="server" OnClientClick="Reset();" class="md-raised md-primary md-button md-ink-ripple" Text="Reset" 
                        onclick="btnReset_Click"  style="color:white; width: 42%;" />
                </div>
            </md-content>
        </div>

            </div>
        </div>
    </div>
    <div align="center" style="display: none;" class="divloading" id="divLoading">
        <table cellspacing="0" cellpadding="0" style="height: 100%; border-collapse: collapse;"
            class="dxchartsuiLoadingPanel_DevEx">
            <tbody>
                <tr>
                    <td style="" class="dx">
                        <img align="middle" alt="Loading..." src="../Admin/images/Loader_Circular.gif">
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div id="divoverlay" class="overlay-style">
    </div>
    <div id="divMask" class="mask" style="display: none">
    </div>
    <script type="text/javascript">

        function exportall() {

            document.getElementById('iframeExport').src = "../Export.aspx?Servicetype=getaveragedaystobill&FileName=test&companyid=1&reporttype=0&fromdate=12/25/2016&todate=12/25/2016&reportid=1&userid=1&avgormax=1&insuranceid=1&numberofdays=1&CompanyidDisplayName=Clients&DateDisplayName=Date Range&InsuranceidDisplayName=Payer&NumberOfDaysDisplayName=Number of Days&AvgormaxDisplayName=Average/Max";
            // document.getElementById('iframeExport').src = "../Export.aspx?Servicetype=GetDateOfServiceVsDateOfUpload&FileName=Date%20of%20Service%20vs%20Date%20of%20Upload&companyid=1&reporttype=0&fromdate=12/25/2016&todate=12/25/2016&reportid=3&userid=1&avgormax=&insuranceid=Not Available&numberofdays=Not Available";

        }

        function showLoginHelp() {
            var position = $('.form-2').offset();
            $('.topuplogin').css("top", position.top - 10);
            $('.topuplogin').fadeIn('400');

        }
        function hideLoginHelp() {
            $('.topuplogin').fadeOut('400');
        }

        function emptyTextBoxLogin(id) {
            if (document.getElementById(id).value == 'Name' ||
            document.getElementById(id).value == 'Email Id' ||
            document.getElementById(id).value == 'Phone Number' ||
            document.getElementById(id).value == 'Title' ||
            document.getElementById(id).value == 'Description') {
                document.getElementById(id).value = '';

            }
            $('#' + id).css('color', '#000');
            //            if (navigator.userAgent.match(/iPhone/i) != null || navigator.userAgent.match(/iPad/i) != null) {
            //                $('.footer').css('position', 'fixed');
            //            }
        }


        function colortextboxLogin(id) {
            if (document.getElementById(id).value == 'Name' ||
            document.getElementById(id).value == 'Email Id' ||
            document.getElementById(id).value == 'Phone Number' ||
            document.getElementById(id).value == 'Title' ||
            document.getElementById(id).value == 'Description') {
                $('#' + id).css('color', '#808080');
            }
            else
                $('#' + id).css('color', '#000');
        }

        function resetTextBoxLogin() {
            if (document.getElementById('txtNamelogin').value == '')
            { document.getElementById('txtNamelogin').value = 'Name'; }
            else if (document.getElementById('txtEmaillogin').value == '')
            { document.getElementById('txtEmaillogin').value = 'Email Id'; }
            else if (document.getElementById('txtContactNologin').value == '')
            { document.getElementById('txtContactNologin').value = 'Phone Number'; }
            else if (document.getElementById('txtTitlelogin').value == '')
            { document.getElementById('txtTitlelogin').value = 'Title'; }
            else if (document.getElementById('txtDesclogin').value == '')
            { document.getElementById('txtDesclogin').value = 'Description'; }
        }

        function resetTextBoxLoginForContactusLogin() {
            $('#divErrorContactUsLogin').hide();

            document.getElementById('txtNamelogin').value = 'Name';

            document.getElementById('txtEmaillogin').value = 'Email Id';

            document.getElementById('txtContactNologin').value = 'Phone Number';

            document.getElementById('txtTitlelogin').value = 'Title';

            document.getElementById('txtDesclogin').value = 'Description';

            $('.topup input[type="text"]').css('color', '#808080');

            $('#uploadfilelogin').val('');
            files = undefined;
        }

        function LoginPosition() {
            $('#login_body').css({ 'top': (($(window).height() - $('#login_body').height() - 10 - $('#header').height() - $('#footer').height()) / 2.3), 'position': 'absolute' });
            $('#login_body').css({ 'left': (($(window).width() - $('#login_body').width()) / 2), 'float': 'none' });
            
        }
        var files;
        $(document).ready(function () {
            $('.divloading').show();
            
            $('#sidebar').hide();

            setTimeout("LoginPosition();", 50);
            //  IPhoneStyle();

            if (navigator.userAgent.match(/iPhone/i) != null) {
                $('.footer').css('z-index', '-1');
            }



            $('#uploadfilelogin').change(function (e) {
                files = e.target.files;
                for (var key in files) {
                    if (files[key].name != undefined) {
                        var filename = files[key].name;
                        var ext = filename.split('.')[1].toString();
                        if (ext != "jpg" && ext != "jpeg" && ext != "png" && ext != "pdf" && ext != "xls" && ext != "xlsx") {
                            $('#divErrorContactUsLogin').show();
                            $('#divErrorContactUsLogin').html('<ul><li>Invalid File(s) Extension. </li></ul>')
                            $(this).val('');
                            files = undefined;
                            break;
                        }
                        else {
                            $('#divErrorContactUsLogin').hide();
                        }

                    }
                }

            });
            $('.divloading').hide();
        });

        function NumericValidation(eventObj) {
            var keycode;

            if (eventObj.keyCode) //For IE
                keycode = eventObj.keyCode;
            else if (eventObj.Which)
                keycode = eventObj.Which;  // For FireFox
            else
                keycode = eventObj.charCode; // Other Browser

            if (keycode != 8 && keycode != 9) //if the key is the backspace key
            {
                if (keycode < 48 || keycode > 57) //if not a number
                    return false; // disable key press
                else
                    return true; // enable key press
            }
        }



        function Reset() {
            $('#ContentHolder_lblError').text('');
            $('#ContentHolder_txtusername').val('');
            $('#ContentHolder_txtpassword').val('');
            if (document.getElementById("ContentHolder_lblError") !== null) {
                $('#ContentHolder_lblError')[0].innerHTML = '';
            }

        }
    </script>
</asp:Content>
