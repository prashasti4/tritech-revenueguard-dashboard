﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using System.Net.NetworkInformation;
using System.Net;
using System.Configuration;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.Text;
using System.Security.Cryptography;

public partial class Reportdata : System.Web.UI.Page
{
    int resizeWiidth, resizeHeight, x, x1, y, y1;
    protected void Page_Load(object sender, EventArgs e)
    {
        // if (Session["facilities"] != null)

        Response.ClearHeaders();
        Response.ClearContent();
        Response.Clear();

        Response.Cache.SetCacheability(HttpCacheability.NoCache);

        {
            if (!String.IsNullOrEmpty(Request["sp"]))
                getdeshboard(Request["sp"]);
            if (!String.IsNullOrEmpty(Request["UB"]))
                getUserBlock(Request["UB"]);
            else if (!string.IsNullOrEmpty(Request["con"]))
                CheckNetwork();
            else if (!string.IsNullOrEmpty(Request["proc"]))
                GetAssignmentPageProcedure(Request["proc"]);
            if (!string.IsNullOrEmpty(Request["file"]))
                PostFacilityImage();
            if (!string.IsNullOrEmpty(Request["del"]))
                RemoveImage(Request["pid"]);
        }
        // else
        // {
        //   Response.Write("{\"error\":[{\"error\":\"<script>window.location.href='../Login/Login.aspx'</script>\"}]}");
        // }
    }

    private void getUserBlock(string procedurename)
    {
        DataSet ds = new DataSet();
        ChartDataClass objchart = new ChartDataClass();
        AppearenceSettingRepo objrepo = new AppearenceSettingRepo();
        string CAutoID = objrepo.Decryptdata(Request.Cookies["CAutoID"].Value);
        // Fetch block type master details
        if (procedurename == "fetch_block_type_master_details")
        {

            ds = objchart.block_type_master();

            Response.Write(JsonConvert.SerializeObject(ds));
        }
        // Login Block Details
        else if (procedurename == "login_block_details")
        {
            string SAutoID = objrepo.Decryptdata(Request.Cookies["SM_USER"].Value);
            string rqstURL = Request.Url.Authority.ToLower().Replace(":" + Request.Url.Port.ToString(), "");
            ds = objchart.login_block_details(Convert.ToInt32(Request["bid"].ToString()), SAutoID, CAutoID, rqstURL, Convert.ToInt32(Request["isactive"].ToString()));
            Response.Write(JsonConvert.SerializeObject(ds));
        }
        // Update block details and change status
        else if (procedurename == "update_block_details")
        {

            ds = objchart.update_block_details(Request["block_data"].ToString());
            Response.Write(JsonConvert.SerializeObject(ds));
        }
    }

    private void getdeshboard(string procedurename)
    {
      
        try
        {



            AppearenceSettingRepo objrepo = new AppearenceSettingRepo();
            DataSet ds = new DataSet();
            ChartDataClass objchart = new ChartDataClass();
            string facilitiesid = Request.Cookies["SM_FACILITY"]!=null?objrepo.Decryptdata(Request.Cookies["SM_FACILITY"]["facilities"].ToString()).Split('-')[0]:"";
           // string Uname = objrepo.Decryptdata(Request.Cookies["SM_FACILITY"]["Uname"].ToString());
            string Uname = objrepo.Decryptdata(Request.Cookies["Uname"].ToString());
            string CAutoID = objrepo.Decryptdata(Request.Cookies["CAutoID"].Value);

            facilitiesid = string.IsNullOrEmpty(Request["fid"]) ? facilitiesid : Request["fid"]; 

            //Report Menu
            if (procedurename == "reportmenu")
            {
                ds = objchart.getreportnamefordropdown(facilitiesid);
                Response.Write(JsonConvert.SerializeObject(ds));

            } // Branch Drop Down
            else if (procedurename == "Branchddl")
            {
                string fdate = Request["frmDate"];
                string tdate = Request["todate"];
                ds = objchart.getBranchfordropdown(facilitiesid, fdate, tdate);
                Response.Write(JsonConvert.SerializeObject(ds));

            } // Date & company Name
            else if (procedurename == "getDashboardDate")
            {
                ds = objchart.getDashboardDate(facilitiesid, CAutoID);
                Response.Write(JsonConvert.SerializeObject(ds));
            } // Grid Report Title
            else if (procedurename == "getReportName")
            {
                string fdate = Request["FromDate"];
                string tdate = Request["toDate"];
                int rpt = int.Parse(Request["rpt"]);
                string type = Request["type"];
                int date = int.Parse(Request["date"]);
                ds = objchart.getReportName(facilitiesid, rpt, fdate, tdate, type, date, CAutoID);
                Response.Write(JsonConvert.SerializeObject(ds));
            } // Analysis By Transaction Date DrillDown
            else if (procedurename == "getAnalysisByTransactionDateDrillDown")
            {
                string fdate = Request["FromDate"];
                string tdate = Request["toDate"];
                int id = int.Parse(Request["id"]);
                string type = Request["type"];
                int date = int.Parse(Request["date"]);
                ds = objchart.getAnalysisByTransactionDateDrillDown(facilitiesid, Request["carrier"], id, fdate, tdate, type, date);
                Session["DataTable"] = ds.Tables[0];
                Response.Write(JsonConvert.SerializeObject(ds));
            }

                // Payer List For Popup
            else if (procedurename == "getPayers")
            {
                string fdate = Request["FromDate"];
                string tdate = Request["toDate"];
                int rpt = int.Parse(Request["rpt"]);
                string payergrp = Request["payergrp"];
                string predata = Request["predata"];
                ds = objchart.getPayers(facilitiesid, fdate, tdate, rpt, payergrp, predata);
                Response.Write(JsonConvert.SerializeObject(ds));
            } // Financial Summary Drill Down
            else if (!string.IsNullOrEmpty(Request["grp"]))
            {
                int grp = int.Parse(Request["grp"]);
                string month = Request["month"];
                string fdate = Request["FromDate"];
                string tdate = Request["toDate"];
                ds = objchart.getDrilldownPmtReport(facilitiesid, month, grp, fdate,tdate);
                Response.Write(JsonConvert.SerializeObject(ds));
            } // Deficiency Grid Report
            else if (!string.IsNullOrEmpty(Request["defType"]))
            {
                ds = objchart.getDeficiencyRuns(facilitiesid, Request["FromDate"], Request["toDate"], Request["deficiencycode"], Request["carrier"], Request["providercode"], procedurename, CAutoID);
                Session["DataTable"] = ds.Tables[0];
                Response.Write(JsonConvert.SerializeObject(ds));
            }
            else if (procedurename == "getLocationFilter")
            {
                ds = objchart.getLocationFilter(Request["FromDate"], Request["toDate"], "", Request["id"]);
                Response.Write(JsonConvert.SerializeObject(ds));
            }
            else if (procedurename == "getlocationsBasedTextReport")
            {
                ds = objchart.getlocationsBasedTextReport(facilitiesid, Request["carrier"], Request["FromDate"], Request["toDate"], Convert.ToInt32(Request["id"]), Request["predata"]);
                Session["DataTable"] = ds.Tables[0];
                Response.Write(JsonConvert.SerializeObject(ds));
            }
            
            // Add or Update Schedule Details
            else if (procedurename == "AddScheduleDetails")
            {
                string SAutoID = objrepo.Decryptdata(Request.Cookies["SM_USER"].Value);
                DataSet dstt = new DataSet();
                    DateTime enddate1 = Convert.ToDateTime("01/01/1900");
                    if (Request["enddate"].ToString() != "")
                    {
                        enddate1 = Convert.ToDateTime(Request["enddate"].ToString());
                    }
                    dstt = objchart.check_duplicate_schedule("0", Request["email_to"], Request["email_type"], "", Convert.ToInt32(Request["emailid"].ToString()), Convert.ToDateTime(Request["startdate"].ToString()), Request["starttime"], enddate1, Request["starttime"], Request["timezone"], Request["recursionrule"], Convert.ToInt32(Request["Repeattypeid"]), Request["endsummary"], Request["repeat"], Request["Timezonedisplay"].ToString().Replace("||", "&"), SAutoID, facilitiesid,  Convert.ToInt32(Request["isrepeat"].ToString()), Convert.ToInt32(Request["emailid"].ToString()));

                    if (dstt.Tables[0].Rows[0][0].ToString() == "0")
                    {
                        string postData = "";
                        string eventid = "";

                        if (Request["emailid"] == "-1")
                            postData = postData + "event=create";
                        else
                        {
                            postData = postData + "event=update";
                            postData = postData + "&eventid=" + Request["eventid"];
                        }


                        postData = postData + "&name=" + Request["name"];

                        postData = postData + "&starttime=" + Request["startdate"] + " " + Request["starttime"];
                        postData = postData + "&endtime=" + Request["enddate"] + " " + Request["starttime"];
                        postData = postData + "&timezone=" + Request["timezone"];

                        postData = postData + "&recursion=" + Request["recursionrule"];
                        // Call Google API Code for Save and Update Event Details in Google Calender and Get Event ID in Response
                        eventid = PostForm(postData);
                        //eventid = "123";
                       
                        DateTime enddate = Convert.ToDateTime("01/01/1900");
                        if (Request["enddate"].ToString() != "")
                        {
                            enddate = Convert.ToDateTime(Request["enddate"].ToString());
                        }

                        if (eventid.ToString().Trim().ToLower() == "true")
                        {
                            objchart.AddScheduleDetails(Request["report_id"], Request["email_to"], Request["email_type"], eventid, Convert.ToInt32(Request["emailid"].ToString()), Convert.ToDateTime(Request["startdate"].ToString()), Request["starttime"], enddate, Request["starttime"], Request["timezone"], Request["recursionrule"], Convert.ToInt32(Request["Repeattypeid"]), Request["endsummary"], Request["repeat"], Request["Timezonedisplay"].ToString().Replace("||", "&"), SAutoID, facilitiesid, Convert.ToInt32(Request["isrepeat"].ToString()));
                            string result = "{\"result\":[{\"id\":\"" + "" + "\",\"error\":\"" + "" + "\"}]}";
                            Response.Write(JsonConvert.SerializeObject(result));
                        }
                        else
                        {
                            if (eventid.ToString().Substring(0, 5).ToString().Trim() == "false")
                            {
                                string result = "{\"result\":[{\"id\":\"" + "" + "\",\"error\":\"" + "generateerror" + "\"}]}";
                                Response.Write(JsonConvert.SerializeObject(result));
                            }
                            else
                            {
                                objchart.AddScheduleDetails(Request["report_id"], Request["email_to"], Request["email_type"], eventid, Convert.ToInt32(Request["emailid"].ToString()), Convert.ToDateTime(Request["startdate"].ToString()), Request["starttime"], enddate, Request["starttime"], Request["timezone"], Request["recursionrule"], Convert.ToInt32(Request["Repeattypeid"]), Request["endsummary"], Request["repeat"], Request["Timezonedisplay"].ToString().Replace("||", "&"), SAutoID, facilitiesid, Convert.ToInt32(Request["isrepeat"].ToString()));
                                string result = "{\"result\":[{\"id\":\"" + "" + "\",\"error\":\"" + "" + "\"}]}";
                                Response.Write(JsonConvert.SerializeObject(result));
                            }
                        }
                    }
                    else
                    {
                        string result = "{\"result\":[{\"id\":\"" + "" + "\",\"error\":\"" + "repeat" + "\"}]}";
                        Response.Write(JsonConvert.SerializeObject(result));
                    }
            }
            // Fetch All Schedules
            else if (procedurename == "fetch_all_schedules")
            {

                try
                {
                    int pageno = int.Parse(Request["start"]);

                    // if (int.Parse(Request["start"]) > 0)
                    // {
                    pageno = int.Parse(Request["start"]) / int.Parse(Request["limit"]) + 1;
                    int totalcount = 0;

                    ds = objchart.fetch_all_schedules(pageno, Convert.ToInt32(Request["limit"].ToString()), out totalcount);
                    // string json = JsonConvert.SerializeObject(ds).Replace("]}", "],\"totalCount\":" + ds.Tables[0].Rows.Count + "}");
                    string json = JsonConvert.SerializeObject(ds).Replace("{\"Table\"", "{\"totalCount\":\"" + totalcount + "\",\"Table\"");

                    // result = result.ToString().Replace("{\"Table\"", "{\"totalCount\":15,\"Table\"");
                    Response.Write(json);
                }
                catch (Exception ex)
                {

                }
            }
            // Delete Schedule
            else if (procedurename == "delete_schedule")
            {

                string postData = "";
                string eventid = "";

                postData = postData + "event=delete";
                postData = postData + "&eventid=" + Request["eventid"];

                eventid = PostForm(postData);

                objchart.AddScheduleDetails("", "", "", eventid, Convert.ToInt32(Request["emailid"].ToString()), Convert.ToDateTime("01/01/1900"), "", Convert.ToDateTime("01/01/1900"), "", "", "", 0, "", "", "", "", "",0);
                //objchart.delete_schedule(Convert.ToInt32(Request["emailid"].ToString()));
                string result = "{\"result\":[{\"id\":\"" + "" + "\",\"error\":\"" + "" + "\"}]}";
                Response.Write(JsonConvert.SerializeObject(result));
            }
            // Delete Email ID
            else if (procedurename == "DeleteSchedulingEmails")
            {
                try
                {
                    string emailid = Request["emailid"];
                    ds = objchart.DeleteSchedulingEmails(emailid);
                    string result = "{\"result\":[{\"id\":\"" + "" + "\",\"error\":\"" + "" + "\"}]}";
                    Response.Write(JsonConvert.SerializeObject(result));
                }
                catch (Exception ex)
                {

                }
            }
            // Fetch All Email IDs for Auto Complete DropDown
            else if (procedurename == "getschedulingemail")
            {
                string str = Request["term"];
                ds = objchart.GetSchedulingEmails(str);
                string result = (JsonConvert.SerializeObject(ds.Tables[0]));
                Response.Write(result);
            }

            // Add Email IDs in scheduling_emails table if Current Email ID is not available in the table
            else if (procedurename == "setschedulingemail")
            {
                string str = Request["email"];
                string error = "error";
                int id = objchart.SetSchedulingEmails(str, out error);
                string result = "{\"result\":[{\"id\":\"" + id + "\",\"error\":\"" + error + "\"}]}";
                Response.Write(result);
            }
            // Fetch Master Details For Email Scheduleing Page
            else if (procedurename == "fetch_master_details")
            {

                int report_id = 0;
                if (!string.IsNullOrEmpty(Request["rid"]))
                {
                    report_id = Convert.ToInt32(Request["rid"].ToString());
                }
                ds = objchart.Fetch_Master_Details(report_id);
                Response.Write(JsonConvert.SerializeObject(ds));
            }
            else
            {

                if (!string.IsNullOrEmpty(Request["id"]))
                {
                    if (Request["id"] == "18") // Finencial Summary 
                        ds = objchart.getpaymentsummary(facilitiesid, int.Parse(Request["branch"]), Request["carrier"], Convert.ToInt32(Request["id"]), Request["FromDate"], Request["toDate"], Request["predata"]);
                    else if(Request["id"] == "31") // CPT Report 
                        ds = objchart.getCPTCodesByState(facilitiesid, int.Parse(Request["branch"]), Request["carrier"], Convert.ToInt32(Request["id"]), Request["FromDate"], Request["toDate"], Request["predata"],  Request["location"]); 
                    else if (Request["payer"] == "payer")
                        ds = objchart.getAgedTrialBalanceByPayer(facilitiesid, int.Parse(Request["branch"]), Request["carrier"], Convert.ToInt32(Request["id"]), Request["FromDate"], Request["toDate"], Request["predata"], procedurename, CAutoID);
                    else
                        ds = objchart.getGraphdata(facilitiesid, int.Parse(Request["branch"]), Request["carrier"], Convert.ToInt32(Request["id"]), Request["FromDate"], Request["toDate"], Request["predata"], Request["deficiencycode"], Request["providercode"], procedurename, CAutoID);

                }
                else
                {
                    ds = objchart.bindReportDashboard(facilitiesid, procedurename);
                }

                var json = JsonConvert.SerializeObject(ds);



                if (ds.Tables.Count > 0)
                {
                    Session["DataTable"] = ds.Tables[0];
                    if (Request["id"] == "27")
                        Session["DataSet"] = ds;
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        //if (Request["id"] == "26" || Request["id"] == "24")
                        if (Request["id"] == "22" || Request["id"] == "24")
                        {
                            string series1 = getseriesfromtable(ds.Tables[0], "series1");
                            string series2 = getseriesfromtable(ds.Tables[3], "series2");
                            //if (Request["id"] == "24")
                            //{
                            //    //string series3 = getseriesfromtable(ds.Tables[2], "series3");
                            //    //json = json.Substring(0, json.Length - 2) + "]," + series1.Substring(0, series1.Length - 1) + "," + series2.Substring(0, series1.Length - 1) + "," + series3;
                            //    json = json.Substring(0, json.Length - 2) + "]," + series1.Substring(0, series1.Length - 1) + "," + series2;
                            //}
                            //else
                            json = json.Substring(0, json.Length - 2) + "]," + series1.Substring(0, series1.Length - 1) + "," + series2;

                        }
                        else
                        {
                            string series = getseriesfromtable(ds.Tables[0], "series");

                            json = json.Substring(0, json.Length - 2) + "]," + series;
                        }
                        Response.Write(json);
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(Request["id"]))
                            Response.Write("{\"error\":[{\"error\":\"error\"}]}");

                        else
                            Response.Write(json);
                    }


                }
                else if (NetworkInterface.GetIsNetworkAvailable())
                    Response.Write("{\"error\":[{\"error\":\"error\"}]}");
                else
                    Response.Write("{\"speed\":[{\"speed\":\"0\"}]}");
            }



        }
        catch (Exception ex)
        {
            string vbCrLf = "\r\n";
          //  Common.SaveTextToFile("------" + vbCrLf + ex.Message.ToString() + vbCrLf + ex.Source + vbCrLf + ex.StackTrace.ToString() + vbCrLf + ex.TargetSite.ToString() + vbCrLf + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString() + "------");
            Response.Write("{\"error\":[{\"error\":\"" + ex.Message + "\"}]}");
        }

        Response.Flush();
        HttpContext.Current.ApplicationInstance.CompleteRequest();


    }



    private string PostForm(string postData)
    {
        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(System.Configuration.ConfigurationManager.AppSettings["googlecalendarposturl"].ToString());
        request.Method = "POST";
        request.ContentType = "application/x-www-form-urlencoded";
        //  string postData = "home=Cosby&favorite+flavor=flies";
        byte[] bytes = Encoding.UTF8.GetBytes(postData);
        request.ContentLength = bytes.Length;

        Stream requestStream = request.GetRequestStream();
        requestStream.Write(bytes, 0, bytes.Length);

        WebResponse response = request.GetResponse();
        Stream stream = response.GetResponseStream();
        StreamReader reader = new StreamReader(stream);

        var result = reader.ReadToEnd();
        stream.Dispose();
        reader.Dispose();

        return result.ToString();
    }

    private string getseriesfromtable(DataTable dt, string seriesname)
    {
        string[] seriescol = new string[dt.Columns.Count];
        for (int i = 0; i < dt.Columns.Count; i++)
        {
            seriescol[i] = dt.Columns[i].ColumnName;
        }

        string series = "\"" + seriesname + "\":[";
        for (int i = 3; i < seriescol.Length; i++)
        {
            series += "{ \"valueField\":\"" + seriescol[i].ToString();
            //if (Request["id"] == "22")
            //{
            //    if (seriescol[i].ToLower()=="charges")
            //    series += "\", \"stack\":\"charges";
            //    else
            //        series += "\", \"stack\":\"not";
            //}

            if (i == seriescol.Length - 1)
            {
                if (!string.IsNullOrEmpty(Request["dtype"]))
                    series += "\", \"type\":\"area";
                series += "\", \"name\":\"" + seriescol[i].ToString() + "\"}";
            }
            else
            {
                series += "\", \"name\":\"" + seriescol[i].ToString() + "\"},";
            }
        }
        series += "]}";

        return series;

    }


    private void CheckNetwork()
    {
        if (NetworkInterface.GetIsNetworkAvailable())
        {


            NetworkInterface[] obj = NetworkInterface.GetAllNetworkInterfaces();
            long speed = 0;
            foreach (NetworkInterface item in obj)
            {
                if (item.OperationalStatus == OperationalStatus.Down)
                { speed = item.Speed / 1024; }
            }

            Response.Write("{\"speed\":[{\"speed\":\"" + speed + "\"}]}");
        }
        else
        {
            Response.Write("{\"speed\":[{\"speed\":\"0\"}]}");
        }


        Response.Flush();
        HttpContext.Current.ApplicationInstance.CompleteRequest();

    }

    private void GetAssignmentPageProcedure(string procedureName)
    {
        string results = "";

        if (procedureName == "GetUsersForCAutoId")
        {
            ChartDataClass objrepo = new ChartDataClass();
            AppearenceSettingRepo objappRepo = new AppearenceSettingRepo();
            string CAutoID = objappRepo.Decryptdata(Request.Cookies["CAutoID"].Value);
            results = objrepo.GetUsersForCAutoId(int.Parse(CAutoID));

        }// GetFascilities for Popup
        else if (procedureName == "GetFacilitiesNameForCAutoId")
        {
            AppearenceSettingRepo objrepo = new AppearenceSettingRepo();
            string parentAutoID = objrepo.Decryptdata(Request.Cookies["CAutoID"].Value);
            string childAutoID = Request["cid"];
            ChartDataClass objchart = new ChartDataClass();
            results = objchart.GetFacilitiesNameForCAutoId(int.Parse(parentAutoID), int.Parse(childAutoID));
        }

        // Check User Name
        else if (procedureName == "CheckUsername")
        {

            string userName = Request["uname"];
            ChartDataClass objchart = new ChartDataClass();
            string error = objchart.CheckUsername(userName);
            results = "{\"result\":[{\"msg\":\"" + error + "\"}]}";
        }

        if (procedureName == "setFacilitiesNameForCAutoId")
        {
            AppearenceSettingRepo objrepo = new AppearenceSettingRepo();
            string parentAutoID = objrepo.Decryptdata(Request.Cookies["CAutoID"].Value);
            string SAutoID = objrepo.Decryptdata(Request.Cookies["SM_USER"].Value);
            string PracticeID = Request["autoid"];
            string userName = Request["username"];
            string password = !string.IsNullOrEmpty(Request["pwd"]) ? AppearenceSettingRepo.MD5Hash(Request["pwd"]) : "";
            string childAutoID = Request["cautoid"];
            string isActive = Request["isactive"];
            string RoleId = Request["roleid"];
            ChartDataClass objchart = new ChartDataClass();
            string error = "error";
            int Showallfacilities = Convert.ToInt32(Request["allfsc"]);
            int status = objchart.setFacilitiesNameForCAutoId(int.Parse(parentAutoID), int.Parse(childAutoID), PracticeID, userName, password, int.Parse(isActive), ref error, SAutoID, Convert.ToInt32(RoleId), Showallfacilities);
            results = "{\"result\":[{\"msg\":\"" + error + "\",\"status\":\"" + status + "\"}]}";
        }

        if (procedureName == "ResetPassWordForCAutoId")
        {
            AppearenceSettingRepo objrepo = new AppearenceSettingRepo();
            string childAutoID = Request["cid"];
            string password = AppearenceSettingRepo.MD5Hash(Request["pwd"]);
            string error = "error";
            ChartDataClass objchart = new ChartDataClass();
            error = objchart.ResetPassWordForCAutoId(int.Parse(childAutoID), password).ToString();
            results = "{\"result\":[{\"msg\":\"" + error + "\"}]}";
        }

        if (procedureName == "getFacilitiesForSAutoID")
        {

            AppearenceSettingRepo objRepo = new AppearenceSettingRepo();
            string SAutoID = objRepo.Decryptdata(Request.Cookies["SM_USER"].Value);
            string error = "Error";
            DataSet ds = objRepo.getFacilitiesForSAutoID(SAutoID, out error);
            results = ConvetTableToJSON(ds.Tables[0]);
        }

        // For Role Grid
        if (procedureName == "RoleManagement_getMenuRoleDetails" || procedureName == "RoleManagement_getMenuRole")
        {

            AppearenceSettingRepo objRepo = new AppearenceSettingRepo();
            string CAutoID = objRepo.Decryptdata(Request.Cookies["CAutoID"].Value);
            string SAutoID = objRepo.Decryptdata(Request.Cookies["SM_USER"].Value);
            string Roleid = Request["rid"];
            string isGlobal = Request["isGlobal"];
            // string isDetail = Request["isDetail"];
            string status = Request["status"];
            ChartDataClass objchart = new ChartDataClass();
            DataSet ds = objchart.GetMenuRole(Convert.ToInt32(Roleid), Convert.ToInt32(CAutoID), Convert.ToInt32(isGlobal), Convert.ToInt32(SAutoID), Convert.ToInt32(status), procedureName);
            results = ConvetTableToJSON(ds);
        }


        // For Role Menu Saving
        if (procedureName == "SetMenuRole")
        {

            AppearenceSettingRepo objRepo = new AppearenceSettingRepo();
            string CAutoID = objRepo.Decryptdata(Request.Cookies["CAutoID"].Value);
            string SAutoID = objRepo.Decryptdata(Request.Cookies["SM_USER"].Value);
            string Roleid = Request["rid"];
            int isGlobal = Convert.ToInt32(Request["isGlobal"]);
            string RoleName = Request["rName"];
            string Ambulance = Request["amb"];
            string Physician = Request["phy"];
            int isActive = Convert.ToInt32(Request["isActive"]);
            string Error = "Error";
            ChartDataClass objchart = new ChartDataClass();
            int status = objchart.SetMenuRole(Convert.ToInt32(Roleid), Convert.ToInt32(CAutoID), isGlobal, RoleName, Ambulance, Physician, isActive, ref Error);
            results = "{\"result\":[{\"msg\":\"" + Error + "\",\"status\":\"" + status + "\"}]}";
        }

        // Check User Name
        else if (procedureName == "CheckRoleName")
        {

            string RoleName = Request["rName"];
            int RoleId = Convert.ToInt32(Request["rid"]);
            ChartDataClass objchart = new ChartDataClass();
            string error = objchart.CheckRoleName(RoleName, RoleId);
            results = "{\"result\":[{\"msg\":\"" + error + "\"}]}";
        }

        if (procedureName == "GetLoginBypass")
        {
            string userName = Request["username"];
            string Auto_ID = Request["Auto_ID"];
            int childAutoID = Convert.ToInt32(Request["cautoid"]);
            string ClientsCount = Request["ClientsCount"];
            //int status = GetLoginBypass(childAutoID, userName);

            //results = "{\"result\":[{\"status\":\"" + status + "\"}]}";

            HttpCookie UnmCookie = new HttpCookie("Unm");
            ClsCrypto ccpt = new ClsCrypto("MAKV2SPBNI99212");

            string str_Unm = ccpt.Encrypt(userName);
            UnmCookie.Value = str_Unm;
            // myCookie.Domain = ".eonbi.com";
            Response.Cookies.Add(UnmCookie);



            HttpCookie UserIDCookie = new HttpCookie("UserID");

            string str_UserID = ccpt.Encrypt(Auto_ID);
            UserIDCookie.Value = str_UserID;
            // myCookie.Domain = ".eonbi.com";
            Response.Cookies.Add(UserIDCookie);



            HttpCookie ClientsCountCookie = new HttpCookie("ClientsCount");

            string str_ClientsCount = ccpt.Encrypt(ClientsCount);
            ClientsCountCookie.Value = str_ClientsCount;
            // myCookie.Domain = ".eonbi.com";
            Response.Cookies.Add(ClientsCountCookie);

            results = "{\"result\":[{\"status\":\"" + "0" + "\"}]}";

        }

        Response.Write(results);
        Response.Flush();
        HttpContext.Current.ApplicationInstance.CompleteRequest();
    }


    private string ConvetTableToJSON(DataTable dt)
    {
        string result = "";
        if (dt.Rows.Count > 0)
        {
            result += "{\"" + dt.TableName + "\":[";
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                for (int j = 0; j < dt.Columns.Count; j++)
                {
                    if (j == 0)
                    {
                        result += "{ \"" + dt.Columns[j].ColumnName + "\":\"" + dt.Rows[i][j].ToString();
                        result += "\", \"imagurl\":\"" + getImageUrl(dt.Rows[i]["ImagePracticeID"].ToString());
                    }
                    else if (j + 1 == dt.Columns.Count)
                    {
                        if (i + 1 == dt.Rows.Count)
                        {
                            result += "\", \"" + dt.Columns[j].ColumnName + "\":\"" + dt.Rows[i][j].ToString() + "\"}";
                        }
                        else
                        {
                            result += "\", \"" + dt.Columns[j].ColumnName + "\":\"" + dt.Rows[i][j].ToString() + "\"},";
                        }
                    }
                    else
                        result += "\", \"" + dt.Columns[j].ColumnName + "\":\"" + dt.Rows[i][j].ToString();
                }
            }
            result += "]}";
        }
        return result;
    }

    private string getImageUrl(string PrecticeID)
    {
        AppearenceSettingRepo objRepo = new AppearenceSettingRepo();
        string config = objRepo.Decryptdata(ConfigurationManager.AppSettings["clientcss"]);
        string path = Server.MapPath("~/images/facilitylogos/" + config + "/");
        if (!File.Exists(path + PrecticeID + ".jpg"))
            return "../images/facilitylogos/" + config + "/facility2.jpg";
        else
            return "../images/facilitylogos/" + config + "/" + PrecticeID + ".jpg";
    }

    private void PostFacilityImage()
    {
        string base64String = Request["file"];
        AppearenceSettingRepo objrepo = new AppearenceSettingRepo();
        string config = objrepo.Decryptdata(ConfigurationManager.AppSettings["clientcss"]);
        string filePath = Server.MapPath("~/images/facilitylogos/" + config);
        filePath += "\\" + Request["pid"] + ".jpg";
        SaveByteArrayAsImage(filePath, base64String);

    }

    private void SaveByteArrayAsImage(string fullOutputPath, string base64String)
    {
        try
        {
            byte[] imageBytes = Convert.FromBase64String(base64String);
            MemoryStream ms = new MemoryStream(imageBytes, 0,
              imageBytes.Length);

            // Convert byte[] to Image
            ms.Write(imageBytes, 0, imageBytes.Length);
            System.Drawing.Image image = System.Drawing.Image.FromStream(ms, true);
            image.Save(fullOutputPath);


            resizeHeight = int.Parse(Request["tH"]);
            resizeWiidth = int.Parse(Request["tW"]);
            x = int.Parse(Request["x"]);
            x1 = int.Parse(Request["x1"]);
            y = int.Parse(Request["y"]);
            y1 = int.Parse(Request["y1"]);

            int width = x1 - x;
            int height = y1 - y;

            //Resize Image 
            System.Drawing.Image resizedImage = ResizeImage(fullOutputPath, resizeWiidth, resizeHeight, ImageFormat.Jpeg);

            //Crop Image 
            System.Drawing.Image cropedImage = CropImage(resizedImage, width, height, x, y, x1, y1, ImageFormat.Jpeg);
            resizedImage.Dispose();

            cropedImage.Save(fullOutputPath);

            cropedImage.Dispose();

            Response.Write("{\"result\":[{\"msg\":\"\"}]}");
        }
        catch (Exception e)
        {
            Response.Write("{\"result\":[{\"msg\":\"" + e.Message + "\"}]}");
        }

        Response.Flush();
        HttpContext.Current.ApplicationInstance.CompleteRequest();
    }



    private void RemoveImage(string PrecticeId)
    {
        try
        {
            AppearenceSettingRepo objrepo = new AppearenceSettingRepo();
            string config = objrepo.Decryptdata(ConfigurationManager.AppSettings["clientcss"]);
            string filePath = Server.MapPath("~/images/facilitylogos/" + config + "/" + PrecticeId + ".jpg");
            FileInfo file = new FileInfo(filePath);
            if (file.Exists)
            {
                file.Delete();
                Response.Write("{\"result\":[{\"msg\":\"\"}]}");
            }
            else
                Response.Write("{\"result\":[{\"msg\":\"Image dose not Exists.\"}]}");
        }
        catch (Exception ex)
        {
            Response.Write("{\"result\":[{\"msg\":\"" + ex.Message + "\"}]}");
        }


        Response.Flush();
        HttpContext.Current.ApplicationInstance.CompleteRequest();
    }

    /// <param name="targetWidth">Width of the target</param>
    /// <param name="targetHeight">Height of the target</param>
    /// <param name="x1">The position x1</param>
    /// <param name="y1">The position y1</param>
    /// <param name="x2">The position x2</param>
    /// <param name="y2">The position y2</param>
    /// <param name="imageFormat">The image format</param>
    /// <returns>A cropped and resized image</returns>
    public System.Drawing.Image CropImage(System.Drawing.Image image, int targetWidth, int targetHeight, int x1, int y1, int x2, int y2, ImageFormat imageFormat)
    {

        //System.Drawing.Image image = System.Drawing.Image.FromFile(filePath);

        var bmp = new Bitmap(targetWidth, targetHeight);
        Graphics g = Graphics.FromImage(bmp);

        g.InterpolationMode = InterpolationMode.HighQualityBicubic;
        g.SmoothingMode = SmoothingMode.HighQuality;
        g.PixelOffsetMode = PixelOffsetMode.HighQuality;
        g.CompositingQuality = CompositingQuality.HighQuality;

        int width = x2 - x1;
        int height = y2 - y1;

        g.DrawImage(image, new Rectangle(0, 0, targetWidth, targetHeight), x1, y1, width, height, GraphicsUnit.Pixel);

        var memStream = new MemoryStream();


        bmp.Save(memStream, imageFormat);
        image.Dispose();
        bmp.Dispose();
        g.Dispose();
        return System.Drawing.Image.FromStream(memStream);

    }

    /// <param name="filePath">get file from path to resize</param>
    /// <param name="targetWidth">New Width </param>
    /// <param name="targetHeight">New Height</param>
    /// <param name="imageFormat">Format of Image</param>
    /// <returns></returns>
    private System.Drawing.Image ResizeImage(string filePath, int targetWidth, int targetHeight, ImageFormat imageFormat)
    {
        try
        {
            System.Drawing.Image image = System.Drawing.Image.FromFile(filePath);

            float AspectRatio = (float)image.Size.Width / (float)image.Size.Height;

            Bitmap bitMAP1 = new Bitmap(targetWidth, targetHeight);
            Graphics imgGraph = Graphics.FromImage(bitMAP1);
            imgGraph.CompositingQuality = CompositingQuality.HighQuality;
            imgGraph.SmoothingMode = SmoothingMode.HighQuality;
            imgGraph.InterpolationMode = InterpolationMode.HighQualityBicubic;
            var imgDimesions = new Rectangle(0, 0, targetWidth, targetHeight);
            imgGraph.DrawImage(image, imgDimesions);
            var memStream = new MemoryStream();
            bitMAP1.Save(memStream, imageFormat);

            bitMAP1.Dispose();
            imgGraph.Dispose();
            image.Dispose();

            return System.Drawing.Image.FromStream(memStream);
        }
        catch (Exception ex)
        {
            return null;
        }

    }

    private string ConvetTableToJSON(DataSet ds)
    {

        string result = "{";
        if (ds.Tables.Count > 0)
        {
            for (int z = 0; z < ds.Tables.Count; z++)
            {
                result += "\"" + ds.Tables[z].TableName + "\":[";
                for (int i = 0; i < ds.Tables[z].Rows.Count; i++)
                {

                    for (int j = 0; j < ds.Tables[z].Columns.Count; j++)
                    {

                        if (j == 0)
                            result += "{ \"" + ds.Tables[z].Columns[j].ColumnName + "\":\"" + ds.Tables[z].Rows[i][j].ToString();
                        else if (j + 1 == ds.Tables[z].Columns.Count)
                        {
                            if (i + 1 == ds.Tables[z].Rows.Count)
                                result += "\", \"" + ds.Tables[z].Columns[j].ColumnName + "\":\"" + ds.Tables[z].Rows[i][j].ToString() + "\"}";
                            else
                                result += "\", \"" + ds.Tables[z].Columns[j].ColumnName + "\":\"" + ds.Tables[z].Rows[i][j].ToString() + "\"},";
                        }
                        else
                            result += "\", \"" + ds.Tables[z].Columns[j].ColumnName + "\":\"" + ds.Tables[z].Rows[i][j].ToString();
                    }

                }
                if (z < ds.Tables.Count - 1)
                    result += "],";
                else
                    result += "]}";
            }
        }

        return result;

    }

    //By pass login to show users view for Admin
    private int GetLoginBypass(int CAutoId, string userName)
    {
        int result = 1;
        try
        {
            ChartDataClass objchart = new ChartDataClass();
            // Get password from user cautoid and username
            string password = objchart.GetPassowordForCAutoID(CAutoId, userName);

            if (password != "")
            {
                int ipattempt = 0;
                int invaliduserattempt = 0;
                int isadmin = 0;
                string rqstURL = Request.Url.Authority.ToLower().Replace(":" + Request.Url.Port.ToString(), "");
                DataSet ds = new DataSet();
                AppearenceSettingRepo objrepo = new AppearenceSettingRepo();

                // login with new user's username and password 
                ds = objrepo.getLogin(userName, password, "", rqstURL, out ipattempt, out invaliduserattempt, out isadmin);

                DataTable login = ds.Tables[0];
                if (login.Rows.Count > 0)
                {
                    // Save User SAutoID in OldSM_User Cookie
                    HttpCookie OldSM_USER = new HttpCookie("OldSM_USER");
                    OldSM_USER["SM_USER"] = Request.Cookies["SM_USER"].Value;
                    OldSM_USER["isAdmincheck"] = Request.Cookies["isAdmincheck"].Value;
                    Response.Cookies.Add(OldSM_USER);


                    // Replace SAutoID Cookie wth new user SautoID
                    HttpCookie myCookie = new HttpCookie("SM_USER");
                    string str_pass = objrepo.Encryptdata(login.Rows[0][1].ToString());
                    myCookie.Value = str_pass;
                    Response.Cookies.Add(myCookie);

                    HttpCookie isAdmincheck = new HttpCookie("isAdmincheck");
                    isAdmincheck.Value = isadmin.ToString();
                    Response.Cookies.Add(isAdmincheck);

                    result = 0;
                }
            }
        }
        catch (Exception ex)
        {
          //  Common.SaveTextToFile(ex.Message);
        }

        return result;
    }


    public class ClsCrypto
    {
        private RijndaelManaged myRijndael = new RijndaelManaged();
        private int iterations;
        private byte[] salt;

        public ClsCrypto(string strPassword)
        {
            myRijndael.BlockSize = 128;
            myRijndael.KeySize = 128;
            myRijndael.IV = HexStringToByteArray("e84ad660c4721ae0e84ad660c4721ae0");

            myRijndael.Padding = PaddingMode.PKCS7;
            myRijndael.Mode = CipherMode.CBC;
            iterations = 1000;
            salt = System.Text.Encoding.UTF8.GetBytes("insight123resultxyz");
            myRijndael.Key = GenerateKey(strPassword);
        }

        public string Encrypt(string strPlainText)
        {
            byte[] strText = new System.Text.UTF8Encoding().GetBytes(strPlainText);
            ICryptoTransform transform = myRijndael.CreateEncryptor();
            byte[] cipherText = transform.TransformFinalBlock(strText, 0, strText.Length);

            return Convert.ToBase64String(cipherText);
        }

        public static byte[] HexStringToByteArray(string strHex)
        {
            dynamic r = new byte[strHex.Length / 2];
            for (int i = 0; i <= strHex.Length - 1; i += 2)
            {
                r[i / 2] = Convert.ToByte(Convert.ToInt32(strHex.Substring(i, 2), 16));
            }
            return r;
        }

        private byte[] GenerateKey(string strPassword)
        {
            Rfc2898DeriveBytes rfc2898 = new Rfc2898DeriveBytes(System.Text.Encoding.UTF8.GetBytes(strPassword), salt, iterations);

            return rfc2898.GetBytes(128 / 8);
        }
    }

}

