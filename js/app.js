try {
    //declaring module and including dependencies

    var app = angular.module('app', ['ngAnimate', 'ui.router', 'ngMaterial', 'ngAria', 'chart.js', 'ngStorage', 'ui.grid', 'ui.grid.pinning', 'ui.grid.edit', 'ui.grid.cellNav', 'ui.grid.grouping', 'ui.grid.exporter',
        'ui.grid.selection', 'ui.grid.infiniteScroll', 'ui.grid.pagination', "angular-multi-select", 'ngRadialGauge', 'ngMessages', 'ui.grid.resizeColumns', 'ui.grid.pinning','ui.grid.autoResize'
    ])

    //for changing theme
    app.config(function($mdThemingProvider) {
        var customParameters = customizeTheme();
        var customPrimary = customParameters.customPrimary;
        $mdThemingProvider.definePalette('customPrimary',
            customPrimary);
        $mdThemingProvider.theme(customParameters.theme)
            .primaryPalette(customParameters.primaryPalleteColor, {
                'default': customParameters.hue
            })

    })

    //set constants to be used throughout the application
    app.value('appSettings', {
        //webservice url & proxypage to be used throughout the applicatio       
        requestedUrl: 'http://mydashboard.revenueguard.com/Controller.aspx',
        proxyPage: "reportCurl.php",
        //excel export url
        excelExportUrl: 'http://mydashboard.revenueguard.com/Export.aspx?',
        //Global chart settings
        defaultFontColor: '#868585',
        defaultFontSize: 14,
        defaultFontFamily: 'Arial',
        //variables for chart properties to avoid overwriting of properties
        lineChartProperty: "",
        pieChartProperty: "",
        barChartProperty: "",
        overviewMedicareOverReportChartProperty: "",
        overviewTotalHoursReportChartProperty: "",
        overviewAverageReport: "",
        overviewClaimsDifferenceReport: ""
    })

    //loading data from config jsons and assigning to constant varaiables to avoid overwriting of properties
    app.run(function($http, filterCharts, appSettings) {
        $http.get('chartSettings/lineProperty.json').success(function(response) {
            appSettings.lineChartProperty = response;
        })
        $http.get('chartSettings/pieProperty.json').success(function(response) {
            appSettings.pieChartProperty = response;
        })
        $http.get('chartSettings/barProperty.json').success(function(response) {
            appSettings.barChartProperty = response;
        })
        $http.get('chartSettings/lineProperty.json').success(function(response) {
            appSettings.overviewMedicareOverReportChartProperty = response;
        })
        $http.get('chartSettings/lineProperty.json').success(function(response) {
            appSettings.overviewTotalHoursReportChartProperty = response;
        })
        $http.get('chartSettings/lineProperty.json').success(function(response) {
            appSettings.overviewAverageReport = response;
        })
        $http.get('chartSettings/lineProperty.json').success(function(response) {
            appSettings.overviewClaimsDifferenceReport = response;
        })
    })



    //===================HTTP REQUEST TO GET REPORT HEADING=============================//
    //we call this factory function as promise to synchronize other requests
    try {
        app.factory('getReportHeading', function($http, chartFilterParameters, appSettings, ErrorLog) {
            return {
                getReport: function() {
                    //Prepare HTTP request parameters
                    var config = {
                        method: 'POST',
                        url: appSettings.proxyPage,
                        data: {
                            requester: "ChartCtrl",
                            requestedUrl: appSettings.requestedUrl,
                            ServiceType: 'GetReportName',
                            companyid: chartFilterParameters.companyid,
                            reporttype: chartFilterParameters.reporttype,
                            fromdate: chartFilterParameters.fromdate,
                            todate: chartFilterParameters.todate,
                            reportid: chartFilterParameters.reportid,
                            userid: chartFilterParameters.userid,
                            avgormax: "",
                            numberofdays: "",
                            insuranceid: ""
                        }
                    };

                    var request = $http(config); //request object

                    return request; //return request object

                }

            }
        })
    } catch (exception) {
        var caughtException = exception;
        var exceptionObject = {
                Error: caughtException,
                time: new Date()
            }
            //pushing exception in exception object to display relative error message.
        ErrorLog.exceptions.push(exceptionObject);
        console.log(ErrorLog.exceptions);
        hideLoader();
    }

//==================HTTP REQUEST SERIVCE FOR OVERVIEW PAGE==========================//
    try {
        app.factory('getOverviewData', function($http, chartFilterParameters, appSettings, ErrorLog) {
            return {
                getOverviewChartData: function(servicetype) {
                    var servicetype = servicetype; //get service type of report which is making request    
                    //Prepare HTTP request parameters                
                    var config = {
                        method: 'POST',
                        url: appSettings.proxyPage,
                        data: {
                            requester: "ChartCtrl",
                            requestedUrl: appSettings.requestedUrl,
                            ServiceType: servicetype,
                            companyid: chartFilterParameters.companyid,
                            reporttype: 1,
                            fromdate: chartFilterParameters.fromdate,
                            todate: chartFilterParameters.todate,
                            reportid: chartFilterParameters.reportid,
                            userid: chartFilterParameters.userid,
                            avgormax: "",
                            numberofdays: "",
                            insuranceid: "",
                            isreport: 1
                        }
                    };
                   
                    var request = $http(config); //make request object

                    return request; //return request object

                }

            }
        })
    } catch (exception) {
        var caughtException = exception;
        var exceptionObject = {
            Error: caughtException,
            time: new Date()
        }
        ErrorLog.exceptions.push(exceptionObject);
        hideLoader();
    }

} catch (exception) {
    var caughtException = exception;
    var exceptionObject = {
        Error: caughtException,
        time: new Date()
    }
     //pushing exception in exception object to display relative error message.
        ErrorLog.exceptions.push(exceptionObject);
        console.log(ErrorLog.exceptions);
    hideLoader();
}
