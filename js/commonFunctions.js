function customizeTheme() {
    //this function is called from app.js from theme provider method
    //customize dashboard theme. enter color codes in customPrimary property of customParameters json object
    var customParameters = {
        "theme": "default",
        "primaryPalleteColor": "customPrimary",
        "accentPalette": "purple",
        "warnPalette": "customPrimary",
        "backgroundPalette": "grey",
        "hue": "600",
        "customPrimary": {
           '50': '#3ad40e',
        '100': '#33bc0c',
        '200': '#2da40b',
        '300': '#268c09',
        '400': '#1f7408',
        '500': '195c06',
        '600': '#124404',
        '700': '#0c2c03',
        '800': '#051401',
        '900': '#000000',
        'A100': '#40ec0f',
        'A200': '#51f124',
        'A400': '#64f23c',
        'A700': '#000000'
        }
    }


    return customParameters;
}


function setAlignment() {
    var windowWidth = window.innerWidth;
    var innerWidth = windowWidth - 2;
    var marginLeft = windowWidth - (windowWidth * (82 / 100)) + 'px';   
    angular.element(document.querySelector(".innerView")).css('width', windowWidth);
    angular.element(document.querySelector("#indexView")).css('minWidth', innerWidth);
   
}


//show loader
function showLoader() {
    document.getElementById('divmask').style.display = "block";
    document.getElementById('divloading').style.display = "block";
}

//hide loader
function hideLoader() {
    document.getElementById('divmask').style.display = "none";
    document.getElementById('divloading').style.display = "none";
}


//Align chart title
function positionChartTitle() {
    var canvasBaseWidth = document.getElementById('canvasBase').style.width;
    canvasBaseWidth = canvasBaseWidth.replace('%', '');
    var position = canvasBaseWidth / 2;  
    document.getElementById("chartTitle").style.marginLeft = position;
}

//detect platform is desktop or mobile
function isWeb() {
    var windowWidth = window.innerWidth;
    if (windowWidth >= 729) {
       
        return true;
    } else {       
        canvas = document.getElementById('canvas');
        canvas.width = window.innerWidth;
        canvas.height = window.innerWidth - 20;
        return false;
    }
}

//hide filter icon in desktop
function hideFilter() {
    var menuBarHeight=window.innerHeight-160;
    var height = isNaN(window.innerHeight) ? window.clientHeight : menuBarHeight;
    //document.getElementById('nav').style.height=menuBarHeight+'px';
    if (window.innerWidth <= 729) {
        document.getElementById('subHeader').style.display = "none";
    }

}

function setHomeViewMargin() {  
//not needed anymore but called somewhere in code so haven't removed from here
}

//show filter icon in mobile
function showFilter() {
    if (window.innerWidth <= 729) {
        document.getElementById('subHeader').style.display = "block";
    }
}

//toggle to chart view from grid view
function showChart() {    
 
    document.getElementById('chartButton').style.backgroundImage = 'url("./images/icons/Chart_Click.png")';
    document.getElementById('gridButton').style.backgroundImage = 'url("./images/icons/Grid_Non.png")';
    document.getElementById('exportExcel').style.display = "none";
    document.getElementById('exportPdf').style.display = "block";
    document.getElementById('displayGrid').style.display = "none";
    document.getElementById('GridErrorDiv').style.display = "none";   
    document.getElementById('displayChart').style.display = "block";
}

//toggle to grid view from chart view
function showGrid() {
    document.getElementById('displayChart').style.display = "none";
    document.getElementById('gridButton').style.backgroundImage = 'url("./images/icons/Grid_Click.png")';
    document.getElementById('chartButton').style.backgroundImage = 'url("./images/icons/Chart_Non.png")';
    document.getElementById('exportPdf').style.display = "none";
    document.getElementById('exportExcel').style.display = "block";   
    document.getElementById('displayGrid').style.display = "block";   


}

//Decrypt cookie
function DecryptData(encryptData) {


    try {
        //Creating the Vector Key
        var iv = CryptoJS.enc.Hex.parse('e84ad660c4721ae0e84ad660c4721ae0');
        //Encoding the Password in from UTF8 to byte array
        var Pass = CryptoJS.enc.Utf8.parse('MAKV2SPBNI99212');
        //var Pass = "MAKV2SPBNI99212";
        //Encoding the Salt in from UTF8 to byte array
        var Salt = CryptoJS.enc.Utf8.parse("insight123resultxyz");
        //Creating the key in PBKDF2 format to be used during the decryption
        var key128Bits1000Iterations = CryptoJS.PBKDF2(Pass.toString(CryptoJS.enc.Utf8), Salt, { keySize: 128 / 32, iterations: 1000 });
        //Enclosing the test to be decrypted in a CipherParams object as supported by the CryptoJS libarary
        var cipherParams = CryptoJS.lib.CipherParams.create({
            ciphertext: CryptoJS.enc.Base64.parse(encryptData)
        });

        //Decrypting the string contained in cipherParams using the PBKDF2 key
        // var decrypted = CryptoJS.AES.decrypt(cipherParams, "", { mode: CryptoJS.mode.CBC, iv: iv, padding: CryptoJS.pad.Pkcs7 });
        var decrypted = CryptoJS.AES.decrypt(cipherParams, key128Bits1000Iterations, { mode: CryptoJS.mode.CBC, iv: iv, padding: CryptoJS.pad.Pkcs7 });
        var decryptElement = decrypted.toString(CryptoJS.enc.Utf8);
        return decryptElement;
    
    }
    //Malformed UTF Data due to incorrect password
    catch (err) {
        return "";
    }
}

//create cookie
function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();

    document.cookie = cname + "=" + cvalue + "; " + expires;
    //(cvalue);

}

//getcookie data
function getCookies(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
    }
}

//getcookie data
function FetchCookieData(cname) {
    var cookieValue = getCookies(cname);
    cookieValue = DecryptData(cookieValue);
    return cookieValue;
}


//check if user logged  in or not
function CheckLogin() {

    var checkCookie = FetchCookieData('UserID');
    if (checkCookie != '' && checkCookie != null) {} else {
      //window.location.href = "login/login.aspx";
        window.location.href = '#/login';
    }

}


//on page reload excel download displays undefined so click menu and store servicetype and id of active tab 
function UpdateFilterParameter() {   
    //alert("function");
    var currenturl = window.location.href;
    //side-menu

    if (document.getElementsByClassName('nav').length > 0) {
 
        if (document.getElementsByClassName('nav').nav.children.length > 0) {

            for (var i = 0; i < document.getElementsByClassName('nav').nav.children.length; i++) {
   
                var isActive = document.getElementsByClassName('nav').nav.children[i].classList.contains('active');
               

                if (isActive == true) {

                    document.getElementsByClassName('nav').nav.children[i].click();


                    break;
                }else{
            //click first menu if no active tab          
                    if(i==document.getElementsByClassName('nav').nav.children.length-1){
                                
                        if((document.getElementsByClassName('nav').nav.children[0].innerText).toLowerCase()!='logout')
   
                        document.getElementsByClassName('nav').nav.children[0].click();
                    }
            }
        }
       
    }
}
}
//clear cookie data
function ClearCookie() {
    setCookie('reportid', '', -1);
    setCookie('service', '', -1);
    setCookie('active_menu', '', -1);
}


//return appropriate currency symbol
function getCurrencySign(currency) {
    var currency = currency;
    switch (currency) {
        case 'dollar':
            return '$';
        case 'euro':
            return '€';
        case 'yen':
            return '¥';
        case 'pound':
            return '£';
        case 'won':
            return '₩';
        case 'percent':
            return '%';
        case 'number':
            return '';
    }
}



function DatePickerInitialize()
{
  $('#txtDateRange').daterangepicker({
                format: 'MM/DD/YYYY',
                startDate: moment().subtract(29, 'days'),
                endDate: moment(),
                minDate: '01/01/1880',
                maxDate: '12/31/2090',
                showDropdowns: true,
                showWeekNumbers: true,
                timePicker: false,
                timePickerIncrement: 1,
                timePicker12Hour: true,
                ranges: {
                   
                    'LAST MONTH': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                    'MONTH TO DATE': [moment().startOf('month'), moment()],
                    'YEAR TO DATE': [moment().startOf('year'), moment()]
                    //'THIS MONTH': [moment().startOf('month'), moment().endOf('month')],

                },
                opens: 'right',
                drops: 'down',
                buttonClasses: ['btn', 'btn-sm'],
                applyClass: 'btn-primary',
                cancelClass: 'btn-default',
                separator: ' - ',
                locale: {
                    applyLabel: 'Done',
                    cancelLabel: 'Cancel',
                    fromLabel: 'From',
                    toLabel: 'To',
                    customRangeLabel: 'DATE RANGE',
                    daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                    monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                    firstDay: 1
                }
            }, function (start, end, label) {
                $('#txtDateRange').val(start.format('MM/DD/YYYY') + ' - ' + end.format('MM/DD/YYYY'));
            });

            $('#txtDate').daterangepicker({
                format: 'MM/DD/YYYY',
                startDate: moment().subtract(29, 'days'),
                endDate: moment(),
                minDate: '01/01/1880',
                showDropdowns: true,
                showWeekNumbers: true,
                timePicker: false,
                timePickerIncrement: 1,
                timePicker12Hour: true,
             
                opens: 'right',
                drops: 'down',
                buttonClasses: ['btn', 'btn-sm'],
                applyClass: 'btn-primary',
                cancelClass: 'btn-default',
                separator: ' - ',
                singleDatePicker: true,
                showDropdowns: true,
                locale: {
                    applyLabel: 'Done',
                    cancelLabel: 'Cancel',
                    fromLabel: 'From',
                    toLabel: 'To',
                    customRangeLabel: 'DATE RANGE',
                    daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                    monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                    firstDay: 1
                }
            }, function (start, end, label) {
                $('#txtDate').val(start.format('MM/DD/YYYY'));
            });

}


//called on click of checkbox in multiselect dropdown
function storeIds(item) {

    if(item!=undefined && item!=null){
        
        //for average dropdown
        if(item.id==-1 && item.$ams_children_leafs!=0 && ((item.text).toLowerCase()).indexOf('top')==-1){
          //  if id is -1 , has children and text does not has top then don't do anything
        }
        else{
            if(item.id!=-1 && item.$ams_parents_id==-1 && (((item.text).toLowerCase()).indexOf('average')!=-1 || ((item.text).toLowerCase()).indexOf('maximum')!=-1)){
             //   if id is not -1, parent id is -1 and text is either avg or max then do nothing 
            }else{
         
       if(!item.hasOwnProperty('children')){
          //if selected item does not has property children

        //for client dropdown

        if(item.$ams_parents_id[0]=='-1' || item.$ams_parents_id[0]=='0'){
       //if selected item's parent id is -1 or 0 (leaf selected)
       //alert((item.text).toLowerCase());
            return 'none';
        }
        if(item.id==0 && ((item.text).toLowerCase()=='other')){
             //if item id is 0 then it is other means disable check
            return true;
        }

        if(item.id=='-1' && ((item.text).toLowerCase()).indexOf('top')!=-1){
          //if item id is -1 and text has top means top revenue clients
            return 'top';
        }
        if(item.id=='-2' && (item.text).toLowerCase()=='all clients'){
           //if item id is -2 and text is all clients
            return 'all';
        }

//for Payers dropdown
      if(item.id==0 && ((item.text).toLowerCase()).indexOf('top')==-1){
       //if item id is 0 and text does not has top then multipayers
           return 'payerTop';
        }
           if(item.id=='-1' && (item.text).toLowerCase()=='all payors'){
        //item id is -1 and text is all payers

            return 'all';
        }
    }
}
}
}
}

//Break x-axis labels
function formatLabel(str, maxwidth){
    var sections = [];
    var words = str.split(" ");
    var temp = "";

    words.forEach(function(item, index){
        if(temp.length > 0)
        {
            var concat = temp + ' ' + item;

            if(concat.length > maxwidth){
                sections.push(temp);
                temp = "";
            }
            else{
                if(index == (words.length-1))
                {
                    sections.push(concat);
                    return;
                }
                else{
                    temp = concat;
                    return;
                }
            }
        }

        if(index == (words.length-1))
        {
            sections.push(item);
            return;
        }

        if(item.length < maxwidth) {
            temp = item;
        }
        else {
            sections.push(item);
        }

    });

    return sections;
}